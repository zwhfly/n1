/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.systemui.statusbar.phone;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.keyguard.KeyguardStatusView;
import com.android.systemui.BatteryMeterView;
import com.android.systemui.FontSizeUtils;
import com.android.systemui.R;
import com.android.systemui.qs.QSPanel;
import com.android.systemui.qs.QSTile;
import com.android.systemui.statusbar.policy.BatteryController;
import com.android.systemui.statusbar.policy.NextAlarmController;
import com.android.systemui.statusbar.policy.UserInfoController;
import com.mediatek.xlog.Xlog;

import java.text.NumberFormat;

//M; tymy StatusBarHeader import begin
import java.util.Calendar;
import java.util.Date;
import java.io.File;
import android.telephony.TelephonyManager;
import android.content.BroadcastReceiver;
import java.text.SimpleDateFormat;
import android.provider.Settings;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Handler;
import java.io.File;
import android.net.Uri;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.PorterDuffXfermode;
import android.graphics.Paint;
import android.graphics.Color;
//M; tymy StatusBarHeader import end
/**
 * The view to manage the header area in the expanded status bar.
 */
public class StatusBarHeaderView extends RelativeLayout implements View.OnClickListener,
        BatteryController.BatteryStateChangeCallback, NextAlarmController.NextAlarmChangeCallback {

    /// M: For debug
    static final String TAG = "StatusBarHeaderView";
    public static final boolean DEBUG = true;

    private boolean mExpanded;
    private boolean mListening;

    private ViewGroup mSystemIconsContainer;
    private View mSystemIconsSuperContainer;
    private View mDateGroup;
    private View mClock;
    private TextView mTime;
    private TextView mAmPm;
    private MultiUserSwitch mMultiUserSwitch;
    private ImageView mMultiUserAvatar;
    private TextView mDateCollapsed;
    private TextView mDateExpanded;
    private LinearLayout mSystemIcons;
    private View mSignalCluster;
    private View mSettingsButton;
    private View mQsDetailHeader;
    private TextView mQsDetailHeaderTitle;
    private Switch mQsDetailHeaderSwitch;
    private ImageView mQsDetailHeaderProgress;
    private TextView mEmergencyCallsOnly;
    private TextView mBatteryLevel;
    private TextView mAlarmStatus;

    private boolean mShowEmergencyCallsOnly;
    private boolean mAlarmShowing;
    private AlarmManager.AlarmClockInfo mNextAlarm;

    private int mCollapsedHeight;
    private int mExpandedHeight;

    private int mMultiUserExpandedMargin;
    private int mMultiUserCollapsedMargin;

    private int mClockMarginBottomExpanded;
    private int mClockMarginBottomCollapsed;
    private int mMultiUserSwitchWidthCollapsed;
    private int mMultiUserSwitchWidthExpanded;

    private int mClockCollapsedSize;
    private int mClockExpandedSize;

    /**
     * In collapsed QS, the clock and avatar are scaled down a bit post-layout to allow for a nice
     * transition. These values determine that factor.
     */
    private float mClockCollapsedScaleFactor;
    private float mAvatarCollapsedScaleFactor;

    private ActivityStarter mActivityStarter;
    private BatteryController mBatteryController;
    private NextAlarmController mNextAlarmController;
    private QSPanel mQSPanel;


    private final Rect mClipBounds = new Rect();

    private boolean mCaptureValues;
    private boolean mSignalClusterDetached;
    private final LayoutValues mCollapsedValues = new LayoutValues();
    private final LayoutValues mExpandedValues = new LayoutValues();
    private final LayoutValues mCurrentValues = new LayoutValues();

    private float mCurrentT;
    private boolean mShowingDetail;

  //M; tymy StatusBarHeader view_statement begin
    private RelativeLayout mOriginalHeader;
    private RelativeLayout mIuniHeader;   
    private ImageView accountIcon;
    private TextView accountName;
    //private TextView accountDate;
    private TextView simTv;   
    private String oldSycDistanceTime="";   
    private boolean isAirPlaneMode;
    private String operator = null;
  //M; tymy StatusBarHeader view_statement end
    
    public StatusBarHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mSystemIconsSuperContainer = findViewById(R.id.system_icons_super_container);
        mSystemIconsContainer = (ViewGroup) findViewById(R.id.system_icons_container);
        mSystemIconsSuperContainer.setOnClickListener(this);
        mDateGroup = findViewById(R.id.date_group);
        mClock = findViewById(R.id.clock);
        mTime = (TextView) findViewById(R.id.time_view);
        mAmPm = (TextView) findViewById(R.id.am_pm_view);
        mMultiUserSwitch = (MultiUserSwitch) findViewById(R.id.multi_user_switch);
        mMultiUserAvatar = (ImageView) findViewById(R.id.multi_user_avatar);
        mDateCollapsed = (TextView) findViewById(R.id.date_collapsed);
        mDateExpanded = (TextView) findViewById(R.id.date_expanded);
        mSettingsButton = findViewById(R.id.settings_button);
        mSettingsButton.setOnClickListener(this);
        mQsDetailHeader = findViewById(R.id.qs_detail_header);
        mQsDetailHeader.setAlpha(0);
        mQsDetailHeaderTitle = (TextView) mQsDetailHeader.findViewById(android.R.id.title);
        mQsDetailHeaderSwitch = (Switch) mQsDetailHeader.findViewById(android.R.id.toggle);
        mQsDetailHeaderProgress = (ImageView) findViewById(R.id.qs_detail_header_progress);
        mEmergencyCallsOnly = (TextView) findViewById(R.id.header_emergency_calls_only);
        mBatteryLevel = (TextView) findViewById(R.id.battery_level);
        mAlarmStatus = (TextView) findViewById(R.id.alarm_status);
        mAlarmStatus.setOnClickListener(this);
        mSignalCluster = findViewById(R.id.signal_cluster);
        mSystemIcons = (LinearLayout) findViewById(R.id.system_icons);
      //M; tymy StatusBarHeader view_definition begin
        mOriginalHeader=(RelativeLayout)findViewById(R.id.header_original);
        mIuniHeader=(RelativeLayout)findViewById(R.id.header_iuni);
      //M; tymy StatusBarHeader view_definition end
        
      //M; tymy StatusBarHeader layout_switch begin
        if(PhoneStatusBar.IUNIStyle){
        	mOriginalHeader.setVisibility(View.GONE);
        	mIuniHeader.setVisibility(View.VISIBLE);
        	
        	accountIcon=(ImageView)findViewById(R.id.account_icon);
        	accountName=(TextView)findViewById(R.id.account_name);
        	//accountDate=(TextView)findViewById(R.id.account_date);
        	simTv=(TextView)findViewById(R.id.simTv);
        	
        	
        }else{
      //M; tymy StatusBarHeader layout_switch end
        	mOriginalHeader.setVisibility(View.VISIBLE);
        	mIuniHeader.setVisibility(View.GONE);
        }
        	              
        loadDimens();
        updateVisibilities();
        updateClockScale();
        updateAvatarScale();
        addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right,
                    int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if ((right - left) != (oldRight - oldLeft)) {
                    // width changed, update clipping
                    setClipping(getHeight());
                }
                boolean rtl = getLayoutDirection() == LAYOUT_DIRECTION_RTL;
                mTime.setPivotX(rtl ? mTime.getWidth() : 0);
                mTime.setPivotY(mTime.getBaseline());
                updateAmPmTranslation();
            }
        });
        setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRect(mClipBounds);
            }
        });
        requestCaptureValues();
        
      //M; tymy StatusBarHeader view_initialization begin
        if(PhoneStatusBar.IUNIStyle){
        	
        	final IntentFilter filter = new IntentFilter();
        	//M:pgm register the TIME_TICK broadcast begin
        	 filter.addAction(Intent.ACTION_TIME_TICK);
        	 //M:pgm register the TIME_TICK broadcast end
        	filter.addAction("android.intent.action.SIM_STATE_CHANGED");
        	filter.addAction("android.intent.action.LOCALE_CHANGED");
            filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            getContext().registerReceiver(mReceiver, filter);
        	
        	updateCarriers(getContext());        	
        	initAccountProviderObserver();       	
        }
      //M; tymy StatusBarHeader view_initialization end
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (mCaptureValues) {
            if (mExpanded) {
                captureLayoutValues(mExpandedValues);
            } else {
                captureLayoutValues(mCollapsedValues);
            }
            mCaptureValues = false;
            updateLayoutValues(mCurrentT);
        }
        mAlarmStatus.setX(mDateGroup.getLeft() + mDateCollapsed.getRight());
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        FontSizeUtils.updateFontSize(mBatteryLevel, R.dimen.battery_level_text_size);
        FontSizeUtils.updateFontSize(mEmergencyCallsOnly,
                R.dimen.qs_emergency_calls_only_text_size);
        FontSizeUtils.updateFontSize(mDateCollapsed, R.dimen.qs_date_collapsed_size);
        FontSizeUtils.updateFontSize(mDateExpanded, R.dimen.qs_date_collapsed_size);
        FontSizeUtils.updateFontSize(mAlarmStatus, R.dimen.qs_date_collapsed_size);
        FontSizeUtils.updateFontSize(this, android.R.id.title, R.dimen.qs_detail_header_text_size);
        FontSizeUtils.updateFontSize(this, android.R.id.toggle, R.dimen.qs_detail_header_text_size);
        FontSizeUtils.updateFontSize(mAmPm, R.dimen.qs_time_collapsed_size);
        FontSizeUtils.updateFontSize(this, R.id.empty_time_view, R.dimen.qs_time_expanded_size);

        mEmergencyCallsOnly.setText(com.android.internal.R.string.emergency_calls_only);

        mClockCollapsedSize = getResources().getDimensionPixelSize(R.dimen.qs_time_collapsed_size);
        mClockExpandedSize = getResources().getDimensionPixelSize(R.dimen.qs_time_expanded_size);
        mClockCollapsedScaleFactor = (float) mClockCollapsedSize / (float) mClockExpandedSize;

        updateClockScale();
        updateClockCollapsedMargin();
    }

    private void updateClockCollapsedMargin() {
        Resources res = getResources();
        int padding = res.getDimensionPixelSize(R.dimen.clock_collapsed_bottom_margin);
        int largePadding = res.getDimensionPixelSize(
                R.dimen.clock_collapsed_bottom_margin_large_text);
        float largeFactor = (MathUtils.constrain(getResources().getConfiguration().fontScale, 1.0f,
                FontSizeUtils.LARGE_TEXT_SCALE) - 1f) / (FontSizeUtils.LARGE_TEXT_SCALE - 1f);
        mClockMarginBottomCollapsed = Math.round((1 - largeFactor) * padding + largeFactor * largePadding);
        requestLayout();
    }

    private void requestCaptureValues() {
        mCaptureValues = true;
        requestLayout();
    }

    private void loadDimens() {
        mCollapsedHeight = getResources().getDimensionPixelSize(R.dimen.status_bar_header_height);
      //M; tymy StatusBarHeader layout_height begin
        if(PhoneStatusBar.IUNIStyle){
        	mExpandedHeight = getResources().getDimensionPixelSize(
                    R.dimen.status_bar_header_height);
        }else{
      //M; tymy StatusBarHeader layout_height begin
        mExpandedHeight = getResources().getDimensionPixelSize(
                R.dimen.status_bar_header_height_expanded);
        }
        mMultiUserExpandedMargin =
                getResources().getDimensionPixelSize(R.dimen.multi_user_switch_expanded_margin);
        mMultiUserCollapsedMargin =
                getResources().getDimensionPixelSize(R.dimen.multi_user_switch_collapsed_margin);
        mClockMarginBottomExpanded =
                getResources().getDimensionPixelSize(R.dimen.clock_expanded_bottom_margin);
        updateClockCollapsedMargin();
        mMultiUserSwitchWidthCollapsed =
                getResources().getDimensionPixelSize(R.dimen.multi_user_switch_width_collapsed);
        mMultiUserSwitchWidthExpanded =
                getResources().getDimensionPixelSize(R.dimen.multi_user_switch_width_expanded);
        mAvatarCollapsedScaleFactor =
                getResources().getDimensionPixelSize(R.dimen.multi_user_avatar_collapsed_size)
                / (float) mMultiUserAvatar.getLayoutParams().width;
        mClockCollapsedSize = getResources().getDimensionPixelSize(R.dimen.qs_time_collapsed_size);
        mClockExpandedSize = getResources().getDimensionPixelSize(R.dimen.qs_time_expanded_size);
        mClockCollapsedScaleFactor = (float) mClockCollapsedSize / (float) mClockExpandedSize;

    }

    public void setActivityStarter(ActivityStarter activityStarter) {
        mActivityStarter = activityStarter;
    }

    public void setBatteryController(BatteryController batteryController) {
        mBatteryController = batteryController;
        ((BatteryMeterView) findViewById(R.id.battery)).setBatteryController(batteryController);
    }

    public void setNextAlarmController(NextAlarmController nextAlarmController) {
        mNextAlarmController = nextAlarmController;
    }

    public int getCollapsedHeight() {
        return mCollapsedHeight;
    }

    public int getExpandedHeight() {
        return mExpandedHeight;
    }

    public void setListening(boolean listening) {
        if (listening == mListening) {
            return;
        }
        mListening = listening;
        updateListeners();
    }

    public void setExpanded(boolean expanded) {
        boolean changed = expanded != mExpanded;
        mExpanded = expanded;
        if (changed) {
            updateEverything();
        }
    }

    public void updateEverything() {
        updateHeights();
        updateVisibilities();
        updateSystemIconsLayoutParams();
        updateClickTargets();
        updateMultiUserSwitch();
        updateClockScale();
        updateAvatarScale();
        updateClockLp();
        requestCaptureValues();
    }

    private void updateHeights() {
        int height = mExpanded ? mExpandedHeight : mCollapsedHeight;
        ViewGroup.LayoutParams lp = getLayoutParams();
        if (lp.height != height) {
            lp.height = height;
            setLayoutParams(lp);
        }
    }

    private void updateVisibilities() {
        if (DEBUG) {
            Xlog.d(TAG, "updateVisibilities: " +
                mExpanded + ", " +
                mAlarmShowing + ", " +
                mShowingDetail + ", " +
                mShowEmergencyCallsOnly);
        }
        mDateCollapsed.setVisibility(mExpanded && mAlarmShowing ? View.VISIBLE : View.INVISIBLE);
        mDateExpanded.setVisibility(mExpanded && mAlarmShowing ? View.INVISIBLE : View.VISIBLE);
        mAlarmStatus.setVisibility(mExpanded && mAlarmShowing ? View.VISIBLE : View.INVISIBLE);
        mSettingsButton.setVisibility(mExpanded ? View.VISIBLE : View.INVISIBLE);
        mQsDetailHeader.setVisibility(mExpanded && mShowingDetail? View.VISIBLE : View.INVISIBLE);
        if (mSignalCluster != null) {
            updateSignalClusterDetachment();
        }
        mEmergencyCallsOnly.setVisibility(mExpanded && mShowEmergencyCallsOnly ? VISIBLE : GONE);
        mBatteryLevel.setVisibility(mExpanded ? View.VISIBLE : View.GONE);
    }

    private void updateSignalClusterDetachment() {
        boolean detached = mExpanded;
        if (detached != mSignalClusterDetached) {
            if (detached) {
                getOverlay().add(mSignalCluster);
            } else {
                reattachSignalCluster();
            }
        }
        mSignalClusterDetached = detached;
    }

    private void reattachSignalCluster() {
        getOverlay().remove(mSignalCluster);
        mSystemIcons.addView(mSignalCluster, 1);
    }

    private void updateSystemIconsLayoutParams() {
        RelativeLayout.LayoutParams lp = (LayoutParams) mSystemIconsSuperContainer.getLayoutParams();
        int rule = mExpanded
                ? mSettingsButton.getId()
                : mMultiUserSwitch.getId();
        if (rule != lp.getRules()[RelativeLayout.START_OF]) {
            lp.addRule(RelativeLayout.START_OF, rule);
            mSystemIconsSuperContainer.setLayoutParams(lp);
        }
    }

    private void updateListeners() {
        if (mListening) {
            mBatteryController.addStateChangedCallback(this);
            mNextAlarmController.addStateChangedCallback(this);
        } else {
            mBatteryController.removeStateChangedCallback(this);
            mNextAlarmController.removeStateChangedCallback(this);
        }
    }

    private void updateAvatarScale() {
        if (mExpanded) {
            mMultiUserAvatar.setScaleX(1f);
            mMultiUserAvatar.setScaleY(1f);
        } else {
            mMultiUserAvatar.setScaleX(mAvatarCollapsedScaleFactor);
            mMultiUserAvatar.setScaleY(mAvatarCollapsedScaleFactor);
        }
    }

    private void updateClockScale() {
        mTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mExpanded
                ? mClockExpandedSize
                : mClockCollapsedSize);
        mTime.setScaleX(1f);
        mTime.setScaleY(1f);
        updateAmPmTranslation();
    }

    private void updateAmPmTranslation() {
        boolean rtl = getLayoutDirection() == LAYOUT_DIRECTION_RTL;
        mAmPm.setTranslationX((rtl ? 1 : -1) * mTime.getWidth() * (1 - mTime.getScaleX()));
    }

    @Override
    public void onBatteryLevelChanged(int level, boolean pluggedIn, boolean charging) {
        String percentage = NumberFormat.getPercentInstance().format((double) level / 100.0);
        mBatteryLevel.setText(percentage);
    }

    @Override
    public void onPowerSaveChanged() {
        // could not care less
    }

    @Override
    public void onNextAlarmChanged(AlarmManager.AlarmClockInfo nextAlarm) {
        mNextAlarm = nextAlarm;
        if (nextAlarm != null) {
            mAlarmStatus.setText(KeyguardStatusView.formatNextAlarm(getContext(), nextAlarm));
        }
        mAlarmShowing = nextAlarm != null;
        updateEverything();
        requestCaptureValues();
    }

    private void updateClickTargets() {
        mMultiUserSwitch.setClickable(mExpanded);
        mMultiUserSwitch.setFocusable(mExpanded);
        mSystemIconsSuperContainer.setClickable(mExpanded);
        mSystemIconsSuperContainer.setFocusable(mExpanded);
        mAlarmStatus.setClickable(mNextAlarm != null && mNextAlarm.getShowIntent() != null);
    }

    private void updateClockLp() {
        int marginBottom = mExpanded
                ? mClockMarginBottomExpanded
                : mClockMarginBottomCollapsed;
        LayoutParams lp = (LayoutParams) mDateGroup.getLayoutParams();
        if (marginBottom != lp.bottomMargin) {
            lp.bottomMargin = marginBottom;
            mDateGroup.setLayoutParams(lp);
        }
    }

    private void updateMultiUserSwitch() {
        int marginEnd;
        int width;
        if (mExpanded) {
            marginEnd = mMultiUserExpandedMargin;
            width = mMultiUserSwitchWidthExpanded;
        } else {
            marginEnd = mMultiUserCollapsedMargin;
            width = mMultiUserSwitchWidthCollapsed;
        }
        MarginLayoutParams lp = (MarginLayoutParams) mMultiUserSwitch.getLayoutParams();
        if (marginEnd != lp.getMarginEnd() || lp.width != width) {
            lp.setMarginEnd(marginEnd);
            lp.width = width;
            mMultiUserSwitch.setLayoutParams(lp);
        }
    }

    public void setExpansion(float t) {
        if (!mExpanded) {
            t = 0f;
        }
        mCurrentT = t;
        float height = mCollapsedHeight + t * (mExpandedHeight - mCollapsedHeight);
        if (height < mCollapsedHeight) {
            height = mCollapsedHeight;
        }
        if (height > mExpandedHeight) {
            height = mExpandedHeight;
        }
        setClipping(height);
        updateLayoutValues(t);
    }

    private void updateLayoutValues(float t) {
        if (mCaptureValues) {
            return;
        }
        mCurrentValues.interpoloate(mCollapsedValues, mExpandedValues, t);
        applyLayoutValues(mCurrentValues);
    }

    private void setClipping(float height) {
        mClipBounds.set(getPaddingLeft(), 0, getWidth() - getPaddingRight(), (int) height);
        setClipBounds(mClipBounds);
        invalidateOutline();
    }

    public void setUserInfoController(UserInfoController userInfoController) {
        userInfoController.addListener(new UserInfoController.OnUserInfoChangedListener() {
            @Override
            public void onUserInfoChanged(String name, Drawable picture) {
                mMultiUserAvatar.setImageDrawable(picture);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == mSettingsButton) {
            startSettingsActivity();
        } else if (v == mSystemIconsSuperContainer) {
            startBatteryActivity();
        } else if (v == mAlarmStatus && mNextAlarm != null) {
            PendingIntent showIntent = mNextAlarm.getShowIntent();
            if (showIntent != null) {
                mActivityStarter.startPendingIntentDismissingKeyguard(showIntent);
            }
        }
    }

    private void startSettingsActivity() {
        mActivityStarter.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS),
                true /* dismissShade */);
    }

    private void startBatteryActivity() {
        mActivityStarter.startActivity(new Intent(Intent.ACTION_POWER_USAGE_SUMMARY),
                true /* dismissShade */);
    }

    public void setQSPanel(QSPanel qsp) {
        mQSPanel = qsp;
        if (mQSPanel != null) {
            mQSPanel.setCallback(mQsPanelCallback);
        }
        mMultiUserSwitch.setQsPanel(qsp);
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return true;
    }

    public void setShowEmergencyCallsOnly(boolean show) {
        if (DEBUG) {
            Xlog.d(TAG, "setShowEmergencyCallsOnly: show= " + show +
                ", mShowEmergencyCallsOnly= " + mShowEmergencyCallsOnly +
                ", mExpanded= " + mExpanded);
        }

        boolean changed = show != mShowEmergencyCallsOnly;
        if (changed) {
            mShowEmergencyCallsOnly = show;
            if (mExpanded) {
                updateEverything();
                requestCaptureValues();
            }
        }
    }

    @Override
    protected void dispatchSetPressed(boolean pressed) {
        // We don't want that everything lights up when we click on the header, so block the request
        // here.
    }

    private void captureLayoutValues(LayoutValues target) {
        target.timeScale = mExpanded ? 1f : mClockCollapsedScaleFactor;
        target.clockY = mClock.getBottom();
        target.dateY = mDateGroup.getTop();
        target.emergencyCallsOnlyAlpha = getAlphaForVisibility(mEmergencyCallsOnly);
        target.alarmStatusAlpha = getAlphaForVisibility(mAlarmStatus);
        target.dateCollapsedAlpha = getAlphaForVisibility(mDateCollapsed);
        target.dateExpandedAlpha = getAlphaForVisibility(mDateExpanded);
        target.avatarScale = mMultiUserAvatar.getScaleX();
        target.avatarX = mMultiUserSwitch.getLeft() + mMultiUserAvatar.getLeft();
        target.avatarY = mMultiUserSwitch.getTop() + mMultiUserAvatar.getTop();
        if (getLayoutDirection() == LAYOUT_DIRECTION_LTR) {
            target.batteryX = mSystemIconsSuperContainer.getLeft()
                    + mSystemIconsContainer.getRight();
        } else {
            target.batteryX = mSystemIconsSuperContainer.getLeft()
                    + mSystemIconsContainer.getLeft();
        }
        target.batteryY = mSystemIconsSuperContainer.getTop() + mSystemIconsContainer.getTop();
        target.batteryLevelAlpha = getAlphaForVisibility(mBatteryLevel);
        target.settingsAlpha = getAlphaForVisibility(mSettingsButton);
        target.settingsTranslation = mExpanded
                ? 0
                : mMultiUserSwitch.getLeft() - mSettingsButton.getLeft();
        target.signalClusterAlpha = mSignalClusterDetached ? 0f : 1f;
        target.settingsRotation = !mExpanded ? 90f : 0f;
    }

    private float getAlphaForVisibility(View v) {
        return v == null || v.getVisibility() == View.VISIBLE ? 1f : 0f;
    }

    private void applyAlpha(View v, float alpha) {
        if (v == null || v.getVisibility() == View.GONE) {
            return;
        }
        if (alpha == 0f) {
            v.setVisibility(View.INVISIBLE);
        } else {
            v.setVisibility(View.VISIBLE);
            v.setAlpha(alpha);
        }
    }

    private void applyLayoutValues(LayoutValues values) {
        mTime.setScaleX(values.timeScale);
        mTime.setScaleY(values.timeScale);
        mClock.setY(values.clockY - mClock.getHeight());
        mDateGroup.setY(values.dateY);
        mAlarmStatus.setY(values.dateY - mAlarmStatus.getPaddingTop());
        mMultiUserAvatar.setScaleX(values.avatarScale);
        mMultiUserAvatar.setScaleY(values.avatarScale);
        mMultiUserAvatar.setX(values.avatarX - mMultiUserSwitch.getLeft());
        mMultiUserAvatar.setY(values.avatarY - mMultiUserSwitch.getTop());
        if (getLayoutDirection() == LAYOUT_DIRECTION_LTR) {
            mSystemIconsSuperContainer.setX(values.batteryX - mSystemIconsContainer.getRight());
        } else {
            mSystemIconsSuperContainer.setX(values.batteryX - mSystemIconsContainer.getLeft());
        }
        mSystemIconsSuperContainer.setY(values.batteryY - mSystemIconsContainer.getTop());
        if (mSignalCluster != null && mExpanded) {
            if (getLayoutDirection() == LAYOUT_DIRECTION_LTR) {
                mSignalCluster.setX(mSystemIconsSuperContainer.getX()
                        - mSignalCluster.getWidth());
            } else {
                mSignalCluster.setX(mSystemIconsSuperContainer.getX()
                        + mSystemIconsSuperContainer.getWidth());
            }
            mSignalCluster.setY(
                    mSystemIconsSuperContainer.getY() + mSystemIconsSuperContainer.getHeight()/2
                            - mSignalCluster.getHeight()/2);
        } else if (mSignalCluster != null) {
            mSignalCluster.setTranslationX(0f);
            mSignalCluster.setTranslationY(0f);
        }
        mSettingsButton.setTranslationY(mSystemIconsSuperContainer.getTranslationY());
        mSettingsButton.setTranslationX(values.settingsTranslation);
        mSettingsButton.setRotation(values.settingsRotation);
        applyAlpha(mEmergencyCallsOnly, values.emergencyCallsOnlyAlpha);
        if (!mShowingDetail) {
            // Otherwise it needs to stay invisible
            applyAlpha(mAlarmStatus, values.alarmStatusAlpha);
        }
        applyAlpha(mDateCollapsed, values.dateCollapsedAlpha);
        applyAlpha(mDateExpanded, values.dateExpandedAlpha);
        applyAlpha(mBatteryLevel, values.batteryLevelAlpha);
        applyAlpha(mSettingsButton, values.settingsAlpha);
        applyAlpha(mSignalCluster, values.signalClusterAlpha);
        if (!mExpanded) {
            mTime.setScaleX(1f);
            mTime.setScaleY(1f);
        }
        updateAmPmTranslation();
    }

    /**
     * Captures all layout values (position, visibility) for a certain state. This is used for
     * animations.
     */
    private static final class LayoutValues {

        float dateExpandedAlpha;
        float dateCollapsedAlpha;
        float emergencyCallsOnlyAlpha;
        float alarmStatusAlpha;
        float timeScale = 1f;
        float clockY;
        float dateY;
        float avatarScale;
        float avatarX;
        float avatarY;
        float batteryX;
        float batteryY;
        float batteryLevelAlpha;
        float settingsAlpha;
        float settingsTranslation;
        float signalClusterAlpha;
        float settingsRotation;

        public void interpoloate(LayoutValues v1, LayoutValues v2, float t) {
            timeScale = v1.timeScale * (1 - t) + v2.timeScale * t;
            clockY = v1.clockY * (1 - t) + v2.clockY * t;
            dateY = v1.dateY * (1 - t) + v2.dateY * t;
            avatarScale = v1.avatarScale * (1 - t) + v2.avatarScale * t;
            avatarX = v1.avatarX * (1 - t) + v2.avatarX * t;
            avatarY = v1.avatarY * (1 - t) + v2.avatarY * t;
            batteryX = v1.batteryX * (1 - t) + v2.batteryX * t;
            batteryY = v1.batteryY * (1 - t) + v2.batteryY * t;
            settingsTranslation = v1.settingsTranslation * (1 - t) + v2.settingsTranslation * t;

            float t1 = Math.max(0, t - 0.5f) * 2;
            settingsRotation = v1.settingsRotation * (1 - t1) + v2.settingsRotation * t1;
            emergencyCallsOnlyAlpha =
                    v1.emergencyCallsOnlyAlpha * (1 - t1) + v2.emergencyCallsOnlyAlpha * t1;

            float t2 = Math.min(1, 2 * t);
            signalClusterAlpha = v1.signalClusterAlpha * (1 - t2) + v2.signalClusterAlpha * t2;

            float t3 = Math.max(0, t - 0.7f) / 0.3f;
            batteryLevelAlpha = v1.batteryLevelAlpha * (1 - t3) + v2.batteryLevelAlpha * t3;
            settingsAlpha = v1.settingsAlpha * (1 - t3) + v2.settingsAlpha * t3;
            dateExpandedAlpha = v1.dateExpandedAlpha * (1 - t3) + v2.dateExpandedAlpha * t3;
            dateCollapsedAlpha = v1.dateCollapsedAlpha * (1 - t3) + v2.dateCollapsedAlpha * t3;
            alarmStatusAlpha = v1.alarmStatusAlpha * (1 - t3) + v2.alarmStatusAlpha * t3;
        }
    }

    private final QSPanel.Callback mQsPanelCallback = new QSPanel.Callback() {
        private boolean mScanState;

        @Override
        public void onToggleStateChanged(final boolean state) {
            post(new Runnable() {
                @Override
                public void run() {
                    handleToggleStateChanged(state);
                }
            });
        }

        @Override
        public void onShowingDetail(final QSTile.DetailAdapter detail) {
            post(new Runnable() {
                @Override
                public void run() {
                    handleShowingDetail(detail);
                }
            });
        }

        @Override
        public void onScanStateChanged(final boolean state) {
            post(new Runnable() {
                @Override
                public void run() {
                    handleScanStateChanged(state);
                }
            });
        }

        private void handleToggleStateChanged(boolean state) {
            mQsDetailHeaderSwitch.setChecked(state);
        }

        private void handleScanStateChanged(boolean state) {
            if (mScanState == state) return;
            mScanState = state;
            final Animatable anim = (Animatable) mQsDetailHeaderProgress.getDrawable();
            if (state) {
                mQsDetailHeaderProgress.animate().alpha(1f);
                anim.start();
            } else {
                mQsDetailHeaderProgress.animate().alpha(0f);
                anim.stop();
            }
        }

        private void handleShowingDetail(final QSTile.DetailAdapter detail) {
            final boolean showingDetail = detail != null;
            transition(mClock, !showingDetail);
            transition(mDateGroup, !showingDetail);
            if (mAlarmShowing) {
                transition(mAlarmStatus, !showingDetail);
            }
            transition(mQsDetailHeader, showingDetail);
            mShowingDetail = showingDetail;
            if (showingDetail) {
                mQsDetailHeaderTitle.setText(detail.getTitle());
                final Boolean toggleState = detail.getToggleState();
                if (toggleState == null) {
                    mQsDetailHeaderSwitch.setVisibility(INVISIBLE);
                    mQsDetailHeader.setClickable(false);
                } else {
                    mQsDetailHeaderSwitch.setVisibility(VISIBLE);
                    mQsDetailHeaderSwitch.setChecked(toggleState);
                    mQsDetailHeader.setClickable(true);
                    mQsDetailHeader.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            detail.setToggleState(!mQsDetailHeaderSwitch.isChecked());
                        }
                    });
                }
            } else {
                mQsDetailHeader.setClickable(false);
            }
        }

        private void transition(final View v, final boolean in) {
            if (in) {
                v.bringToFront();
                v.setVisibility(VISIBLE);
            }
            if (v.hasOverlappingRendering()) {
                v.animate().withLayer();
            }
            v.animate()
                    .alpha(in ? 1 : 0)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            if (!in) {
                                v.setVisibility(INVISIBLE);
                            }
                        }
                    })
                    .start();
        }
    };
    
  //M; tymy StatusBarHeader init_fun begin
    private AccountProviderObserver mAccountProviderObserver;
    
    public void initAccountProviderObserver() {
    	mAccountProviderObserver = new AccountProviderObserver(new Handler());
    	mAccountProviderObserver.observe();
    	mAccountProviderObserver.update();
    }
    
    class AccountProviderObserver extends ContentObserver {
    	Uri uri;
    	AccountProviderObserver(Handler handler) {
            super(handler);
            uri = Uri.parse("content://com.aurora.account.accountprovider/account_info");
        }
        
        void observe() {
            mContext.getContentResolver().registerContentObserver(uri,
            false, this);
        }
        
        @Override
        public void onChange(boolean selfChange) {
            update();
        }
        
        public void update() {
//        	String[] projection = {"nick", "iconPath", "lastSyncFinishedTime", "hasLogin"}; 
        	Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
        	String currTime = getShortTime();
        	if(cursor != null && cursor.moveToFirst()){
        		String nickName = cursor.getString(cursor.getColumnIndex("nick"));
        		String iconPathName = cursor.getString(cursor.getColumnIndex("iconPath"));
        		long lastSyncFinishedTimeName = cursor.getLong(cursor.getColumnIndex("lastSyncFinishedTime"));
        		boolean hasLoginName = cursor.getInt(cursor.getColumnIndex("hasLogin")) == 1 ? true : false;
        		if(hasLoginName){
        			Bitmap bm = null;
        			File file = new File(iconPathName);
                    if (file.exists()) {
                            bm = BitmapFactory.decodeFile(iconPathName);
                            //The picture shows the ImageView                           
                            accountIcon.setImageBitmap(loadLocalCacheIcon(bm));                          
                    }else{
                    	accountIcon.setImageResource(R.drawable.aurora_account_default);
                    }
 
                    if(nickName.contains("&nbsp;")){
                    	nickName=nickName.replace("&nbsp;", " ");
                    }

                    //M;pgm set currenttime begin
        		   // accountName.setText(nickName);currTime
                    accountName.setText(currTime);
                    //M;pgm set currenttime end
        		    String sycDate = getDistanceTime(getDateTime(lastSyncFinishedTimeName));
        		    if(!"error".equals(sycDate) && sycDate != null && !"".equals(sycDate)){
        		    	if(!oldSycDistanceTime.equals(sycDate)){
        		    		oldSycDistanceTime = sycDate;
        		    		if(lastSyncFinishedTimeName == 0){
        		    			//accountDate.setVisibility(View.GONE);
        		    		}else{
        		    		    //accountDate.setVisibility(View.VISIBLE);
        		    		}
            		    	//accountDate.setText(mContext.getString(
                	        //        R.string.account_syn_date, sycDate));
        		    	}
        		    }else{
        		    	//accountDate.setVisibility(View.GONE);
        		    }
        		}else{
        			accountIcon.setImageResource(R.drawable.aurora_account_default);
        			   //M;pgm set currenttime begin
        			//accountName.setText(R.string.account_load);
                     accountName.setText(currTime);
                     //M;pgm set currenttime end
        		
        			//accountDate.setVisibility(View.GONE);
        		}
        	}else{
        		accountIcon.setImageResource(R.drawable.aurora_account_default);
        		  //M;pgm set currenttime begin
    			//accountName.setText(R.string.account_load);
                 accountName.setText(currTime);
                 //M;pgm set currenttime end
    			//accountDate.setVisibility(View.GONE);
        	}
        }
    }
    
    public String getDistanceTime(String dataTime) {
		if (dataTime.equals("0")){
			return getResourceString(0);
		}
		SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date begin = null, end = null;
		try {
			end = dfs.parse(getCurrentDateTime(dfs));
			begin = dfs.parse(dataTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if ((null != begin) && (null != end)) {
			long between = (end.getTime() - begin.getTime()) / 1000;// Divided by 1000 is to be converted into seconds
			long day = between / (24 * 3600);
			long hour = between % (24 * 3600) / 3600;
			long minute = between % 3600 / 60;
			long second = between % 60;
			long month = day / 30;
			long year = month / 12;
			String times = "";
			if (year >= 1) {
				times = dataTime.substring(2, 10);
			} else if (month >= 1) {
				times = month + getResourceString(6);
			} else if (day > 0) {
				times = day + getResourceString(5);
			} else if (hour > 0) {
				times = hour + getResourceString(4);
			} else if (minute > 0) {
				times = minute + getResourceString(3);
			} else if (second > 0) {
				times = getResourceString(1);
			} else if (second == 0) {
				times = getResourceString(1);
			}
			return times;
		} else {
			return "error";
		}
	}
    
    private String getCurrentDateTime(SimpleDateFormat dfs){
    	Date curDate = new Date(System.currentTimeMillis());
    	return dfs.format(curDate);
    }
    
    private String getDateTime(long time){
    	SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date sycDate = new Date(time);
    	return dfs.format(sycDate);
    }
    
    //M;pgm get short time begin
    private String getShortTime(){
    	SimpleDateFormat dfs = new SimpleDateFormat("HH:mm");
    	Date sycDate = new Date(System.currentTimeMillis());
    	return dfs.format(sycDate);
    }
    //M;pgm get short time end
    
    private String getResourceString(int i){
    	switch(i){
    	case 0:
    		return mContext.getString(
                    R.string.account_never);
    	case 1:
    		return mContext.getString(
                    R.string.account_just);
    	case 2:
    		return mContext.getString(
                    R.string.account_second);
    	case 3:
    		return mContext.getString(
                    R.string.account_minute);
    	case 4:
    		return mContext.getString(
                    R.string.account_hour);
    	case 5:
    		return mContext.getString(
                    R.string.account_days);
    	case 6:
    		return mContext.getString(
                    R.string.account_months);
    	default:
    		return null;
    	}
    }
    
    public static Bitmap clipCircleBitmap(Bitmap bitmap, int diameter) {
        if (bitmap == null) {
            return bitmap;
        }
        Bitmap output = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output); 
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        final RectF rect = new RectF(0f, 0f, diameter, diameter);
        paint.setColor(Color.WHITE);
        canvas.drawOval(rect, paint);      
        paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, null, rect, paint);
        return output;
    }
    
    public static Bitmap loadLocalCacheIcon(Bitmap bm) {
        if (bm != null) {
            return clipCircleBitmap(bm, bm.getWidth());
        } else {
        	return null;
        }
    }	
	//M:xiejun android 5.0 must change the slot_id to phone_id  getting sim card info begin
	private static int getSlotId(int slot){
		return android.telephony.SubscriptionManager.getSubIdUsingPhoneId(slot);
	}
	//M:xiejun android 5.0 must change the slot_id to phone_id  getting sim card info begin
	private void updateCarriers(Context mContext) {

		isAirPlaneMode=(1==Settings.System.getInt(mContext.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0))?true:false;
		
		TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		String operator1 = null;
		String IMSI1 = tm.getSubscriberId(getSlotId(0));
		String operator2 = null;
		String IMSI2 = tm.getSubscriberId(getSlotId(1));
		
		try {
			if (IMSI1 == null || "".equals(IMSI1)) {
				operator1 = mContext.getResources().getString(R.string.nosim1);
			} else {
				if (IMSI1.startsWith("46000") || IMSI1.startsWith("46002") || IMSI1.startsWith("46007")) {
					operator1 = mContext.getResources().getString(
							R.string.China_Mobile);
				} else if (IMSI1.startsWith("46001")) {
					operator1 = mContext.getResources().getString(
							R.string.China_Unicom);
				} else if (IMSI1.startsWith("46003")) {
					operator1 = mContext.getResources().getString(
							R.string.China_Telecom);
				}
			}
		} catch (Exception localException) {
			localException.printStackTrace();
		}
		
		try {
			if (IMSI2 == null || "".equals(IMSI2)) {
				operator2 = mContext.getResources().getString(R.string.nosim2);
			} else {
				if (IMSI2.startsWith("46000") || IMSI2.startsWith("46002") || IMSI2.startsWith("46007")) {
					operator2 = mContext.getResources().getString(
							R.string.China_Mobile);
				} else if (IMSI2.startsWith("46001")) {
					operator2 = mContext.getResources().getString(
							R.string.China_Unicom);
				} else if (IMSI2.startsWith("46003")) {
					operator2 = mContext.getResources().getString(
							R.string.China_Telecom);
				}
			}
		} catch (Exception localException) {
			localException.printStackTrace();
		}
		
		if (isAirPlaneMode) {
			operator = mContext.getResources().getString(
					R.string.airplanemode);
		} else {
			if ((mContext.getResources().getString(R.string.nosim1)).equals(operator1)
					&& (mContext.getResources().getString(R.string.nosim2)).equals(operator2)) {
				operator = operator1 + "  " + operator2;
			} else if (!(mContext.getResources().getString(R.string.nosim1)).equals(operator1)
					&& (mContext.getResources().getString(R.string.nosim2)).equals(operator2)) {
				operator = operator1;
			} else if ((mContext.getResources().getString(R.string.nosim1)).equals(operator1)
					&& !(mContext.getResources().getString(R.string.nosim2)).equals(operator2)) {
				operator = operator2;
			} else if (!(mContext.getResources().getString(R.string.nosim1)).equals(operator1)
					&& !(mContext.getResources().getString(R.string.nosim2)).equals(operator2)) {
				operator = operator1 + "  " + operator2;
			}
		}
		if(simTv!=null){
			   simTv.setText(operator);
		}else{
			((TextView)findViewById(R.id.simTv)).setText(operator);
		}
		
	}

	//M:pgm update the time when header is expand begin 
	private void updateClockTime()
	{
		//if(mExpanded) 
		   accountName.setText(getShortTime());
	}
	//M:pgm update the time when header is expand begin 
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "received a broadcast action =="+intent.getAction());
			if (PhoneStatusBar.IUNIStyle) {
				if (intent.getAction().equals("android.intent.action.SIM_STATE_CHANGED")) {
					updateCarriers(context);
				} else if (intent.getAction().equals("android.intent.action.LOCALE_CHANGED")) {					
					updateCarriers(context);
					mAccountProviderObserver.update();
				} else if (intent.getAction().equals("android.intent.action.AIRPLANE_MODE")) {
					updateCarriers(context);
				}
				//M:pgm get the TIME_TICK broadcast begin 
				else if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
					updateClockTime();
					}
				//M:pgm get the TIME_TICK broadcast end
			}
		}
	};
  //M; tymy StatusBarHeader init_fun end
    
}

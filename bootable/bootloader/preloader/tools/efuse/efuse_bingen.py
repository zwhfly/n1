import binascii
import os.path
import sys
from xml.dom import minidom
import argparse
import re
import base64
import subprocess
import os
import hashlib


def convert_to_int_value(val) :
    if val.lower() == "true" :
        return 1
    if val.lower() == "false" :
        return 0
    if (val == 1) or (val == "1") :
        return 1
    return 0


def convert_to_double_efuse_int_value(val) :
    if val.lower() == "true" :
        return 1
    if val.lower() == "false" :
        return 0
    if (val == 11) or (val == "11") or (val == 10) or (val == "10") or (val == 01) or (val == "01") :
        return 1
    return 0


def writeBitValueAndOneHexStringToFile(fstream, lstBitValue, hexString) :
    """
        Notice: The priority of bit input(lstBitValue) is higher than hex input(hexString) if hexString overlaps lstBitValue
    """
    
    result = 0;
    priority_mask = 0xFFFFFFFF
    for bit_number, value in lstBitValue :
        if value == 1 :
            result = result + (2 ** bit_number) #transform bit to decimal
        priority_mask = priority_mask & ~(2 ** bit_number)
    
    hexString2Decimal = int(hexString, 16)  #transform hex to decimal
    result = (hexString2Decimal & priority_mask) | result #merge
    
    plain_hex_string = str(hex(result))[2:]  #result is decimal, transform to hex
    plain_hex_string = plain_hex_string.zfill(8)  #padding 0 at right to 8 digits
    lstHexArr = re.findall('..', plain_hex_string)
        
    fstream.write(chr(int(lstHexArr[3], 16)))
    fstream.write(chr(int(lstHexArr[2], 16)))
    fstream.write(chr(int(lstHexArr[1], 16)))
    fstream.write(chr(int(lstHexArr[0], 16)))
        

def writeBitValueToFile(fstream, lstBitValue) :
    result = 0;
    for bit_number, value in lstBitValue :
        if value == 1 :
            result = result + (2 ** bit_number)
    
    plain_hex_string = str(hex(result))[2:]  #result is decimal
    plain_hex_string = plain_hex_string.zfill(8)
    lstHexArr = re.findall('..', plain_hex_string)
        
    fstream.write(chr(int(lstHexArr[3], 16)))
    fstream.write(chr(int(lstHexArr[2], 16)))
    fstream.write(chr(int(lstHexArr[1], 16)))
    fstream.write(chr(int(lstHexArr[0], 16)))
    

def get_file_sha256hexdigest(file_name):
    
    hash_result = ""
    
    with open(file_name) as f:
        m = hashlib.sha256()
        m.update(f.read())
        hash_result = m.hexdigest()
        
    return hash_result


def writeHexStringToFile(fstream, hexString) :    
    plain_hex_string = hexString.ljust(8, '0')  #padding 0 at right to 8 digits
    lstHexArr = re.findall('..', plain_hex_string)
        
    fstream.write(chr(int(lstHexArr[0], 16)))
    fstream.write(chr(int(lstHexArr[1], 16)))
    fstream.write(chr(int(lstHexArr[2], 16)))
    fstream.write(chr(int(lstHexArr[3], 16)))
        
    
def main():
    
    print("***************************************************************************")
    print("************************ MediaTek EFUSE XML Parser ************************")
    print("****************************** version 1.1.5 ******************************")
    print("***************************************************************************")
    
    parser = argparse.ArgumentParser(description='MediaTek EFUSE XML Parser')
    parser.add_argument('--file', '-f',
                        required=True,
                        help='Provide the xml file')
    parser.add_argument('--output_bin_name', '-o',
                        required=False,
                        default='xml_output.bin',
                        help='Provide output file name')
    parser.add_argument('--key_hash', '-k',
                        required=True,
                        help='Provide the file name path of key hash')
    """
    parser.add_argument('--input_bin', '-i',
                        required=True,
                        help='Provide input bin file name for SBC_KEY_HASH')
    """
    
    args = parser.parse_args()
    
    if not os.path.isfile(args.file) :
        print "[Error] xml file not exist!!"
        sys.exit()
        
    xml_file = minidom.parse(args.file)
    
    EFUSE_KEY1 = "51682454"
    EFUSE_KEY2 = "8825512D"
    
    #Parsing XML to variable
    
    EFUSE_ac_key = xml_file.getElementsByTagName('ac-key')[0].childNodes[0].data
    EFUSE_usb_vid = xml_file.getElementsByTagName('usb-id')[0].attributes["vid"].value 
    EFUSE_usb_pid = xml_file.getElementsByTagName('usb-id')[0].attributes["pid"].value
    
    EFUSE_Disable_NAND_boot_speedup = convert_to_int_value(xml_file.getElementsByTagName('common-ctrl')[0].attributes["Disable_NAND_boot_speedup"].value)
    EFUSE_USB_download_type = convert_to_int_value(xml_file.getElementsByTagName('common-ctrl')[0].attributes["USB_download_type"].value)
    EFUSE_Disable_NAND_boot = convert_to_int_value(xml_file.getElementsByTagName('common-ctrl')[0].attributes["Disable_NAND_boot"].value)
    EFUSE_Disable_EMMC_boot = convert_to_int_value(xml_file.getElementsByTagName('common-ctrl')[0].attributes["Disable_EMMC_boot"].value)
    EFUSE_Enable_SW_JTAG_CON = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_SW_JTAG_CON"].value)
    EFUSE_Enable_Root_Cert = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_Root_Cert"].value)
    EFUSE_Enable_ACC = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_ACC"].value)
    EFUSE_Enable_ACK = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_ACK"].value)
    EFUSE_Enable_SLA = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_SLA"].value)
    EFUSE_Enable_DAA = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_DAA"].value)
    EFUSE_Enable_SBC = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Enable_SBC"].value)
    EFUSE_Disable_JTAG = convert_to_int_value(xml_file.getElementsByTagName('secure-ctrl')[0].attributes["Disable_JTAG"].value)
    
    EFUSE_C_SEC_CTRL1 = "00"
    EFUSE_C_CTRL1 = "00"
    EFUSE_DISABLE_EFUSE = 0
    
    if xml_file.getElementsByTagName('cust-secure-ctrl') :
        EFUSE_C_SEC_CTRL1 = xml_file.getElementsByTagName('cust-secure-ctrl')[0].attributes["c_sec_ctrl"].value
        
    if xml_file.getElementsByTagName('cust-common-ctrl') :
        EFUSE_C_CTRL1 = xml_file.getElementsByTagName('cust-common-ctrl')[0].attributes["c_ctrl"].value
        EFUSE_DISABLE_EFUSE = convert_to_double_efuse_int_value(xml_file.getElementsByTagName('cust-common-ctrl')[0].attributes["DISABLE_EFUSE_BLOW"].value)
    
    EFUSE_com_ctrl_lock = convert_to_int_value(xml_file.getElementsByTagName('common-lock')[0].attributes["com_ctrl_lock"].value)
    EFUSE_usb_id_lock = convert_to_int_value(xml_file.getElementsByTagName('common-lock')[0].attributes["usb_id_lock"].value)
    EFUSE_sec_attr_lock = convert_to_int_value(xml_file.getElementsByTagName('secure-lock')[0].attributes["sec_attr_lock"].value)
    EFUSE_ackey_lock = convert_to_int_value(xml_file.getElementsByTagName('secure-lock')[0].attributes["ackey_lock"].value)
    EFUSE_sbc_pubk_hash_lock = convert_to_int_value(xml_file.getElementsByTagName('secure-lock')[0].attributes["sbc_pubk_hash_lock"].value)
    
    
    print "EFUSE_ac_key = " + str(EFUSE_ac_key)
    print "EFUSE_usb_vid = " + str(EFUSE_usb_vid)
    print "EFUSE_usb_pid = " + str(EFUSE_usb_pid)
    print "EFUSE_Disable_NAND_boot_speedup = " + str(EFUSE_Disable_NAND_boot_speedup)
    print "EFUSE_USB_download_type = " + str(EFUSE_USB_download_type)
    print "EFUSE_Disable_NAND_boot = " + str(EFUSE_Disable_NAND_boot)
    print "EFUSE_Disable_EMMC_boot = " + str(EFUSE_Disable_EMMC_boot)
    print "EFUSE_Enable_SW_JTAG_CON = " + str(EFUSE_Enable_SW_JTAG_CON)
    print "EFUSE_Enable_Root_Cert = " + str(EFUSE_Enable_Root_Cert)
    print "EFUSE_Enable_ACC = " + str(EFUSE_Enable_ACC)
    print "EFUSE_Enable_ACK = " + str(EFUSE_Enable_ACK)
    print "EFUSE_Enable_SLA = " + str(EFUSE_Enable_SLA)
    print "EFUSE_Enable_DAA = " + str(EFUSE_Enable_DAA)
    print "EFUSE_Enable_SBC = " + str(EFUSE_Enable_SBC)
    print "EFUSE_Disable_JTAG = " + str(EFUSE_Disable_JTAG)
    print "EFUSE_C_SEC_CTRL1 = " + str(EFUSE_C_SEC_CTRL1)
    print "EFUSE_DISABLE_EFUSE = " + str(EFUSE_DISABLE_EFUSE)
    print "EFUSE_C_CTRL1 = " + str(EFUSE_C_CTRL1)
    print "EFUSE_com_ctrl_lock = " + str(EFUSE_com_ctrl_lock)
    print "EFUSE_usb_id_lock = " + str(EFUSE_usb_id_lock)
    print "EFUSE_sec_attr_lock = " + str(EFUSE_sec_attr_lock)
    print "EFUSE_ackey_lock = " + str(EFUSE_ackey_lock)
    print "EFUSE_sbc_pubk_hash_lock = " + str(EFUSE_sbc_pubk_hash_lock)

    if len(EFUSE_ac_key) <> 32 :
        print "Error: The length of AC Key is not 32!"
        sys.exit()
    
    """
    if not os.path.isfile(args.input_bin) :
        print "Input bin file not exist!!"
        sys.exit()
        
    if not os.path.isfile("getKeyHash.sh") :
        print "SBCKeyGenerator not exist!!"
        sys.exit()
    
    if os.path.isfile("gfh.bin") :
        os.remove("gfh.bin")
    
    if os.path.isfile(args.key_hash) :
        os.remove(args.key_hash)
    
    print("-----------------------------------------------")
    print("Generating SBC_PUBK_HASH ...")
    subprocess.call(['./getKeyHash.sh', args.input_bin], shell=False)
    print("-----------------------------------------------")
    """
    
    print("-----------------------------------------------")
    
    print("Loading SBC_PUBK_HASH from key hash file: " + args.key_hash)
    
    if not os.path.isfile(args.key_hash) :
        print "[Error] " + args.key_hash + " is not generated from getKeyHash.sh for SBC_Key_Hash!!"
        sys.exit()
    
    EFUSE_SBC_PUBK_HASH = 0
    
    with open(args.key_hash, 'r') as f:
        EFUSE_SBC_PUBK_HASH = f.read()
    
    EFUSE_SBC_PUBK_HASH = EFUSE_SBC_PUBK_HASH.strip()
    
    if EFUSE_SBC_PUBK_HASH == "" :
        print "SBC_PUBK_HASH is not generated"
        sys.exit()
    
    if len(EFUSE_SBC_PUBK_HASH) <> 64 :
        print "[Error] SBC_PUBK_HASH is not in length 64"
        sys.exit()
        
    print "EFUSE_SBC_PUBK_HASH = " + EFUSE_SBC_PUBK_HASH
    
    print("-----------------------------------------------")

    with open(args.output_bin_name, "wb") as f :
        writeHexStringToFile(f, EFUSE_KEY1)  #0x0
        writeHexStringToFile(f, EFUSE_KEY2)  #0x4
        writeBitValueToFile(f, [(0, 0)])  #0x8
        writeHexStringToFile(f, EFUSE_ac_key[0:8])  #0xC
        writeHexStringToFile(f, EFUSE_ac_key[8:16]) #0x10
        
        writeHexStringToFile(f, EFUSE_ac_key[16:24])  #0x14
        writeHexStringToFile(f, EFUSE_ac_key[24:32])  #0x18
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[0:8])  #0x1C
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[8:16]) #0x20
        
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[16:24])  #0x24
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[24:32])  #0x28
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[32:40])  #0x2C
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[40:48])  #0x30
        
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[48:56])  #0x34
        writeHexStringToFile(f, EFUSE_SBC_PUBK_HASH[56:64])  #0x38
        writeHexStringToFile(f, EFUSE_usb_pid)  #0x3C  (only 4 digits, auto padding to 8 digits in writeHexStringToFile)
        writeHexStringToFile(f, EFUSE_usb_vid)  #0x40  (only 4 digits, auto padding to 8 digits in writeHexStringToFile)
        
        writeBitValueToFile(f, [(0, EFUSE_Disable_EMMC_boot), (1, EFUSE_Disable_NAND_boot), (2, EFUSE_USB_download_type), (4, EFUSE_Disable_NAND_boot_speedup)])  #0x44
        writeBitValueToFile(f, [(0, EFUSE_Disable_JTAG), (1, EFUSE_Enable_SBC), (2, EFUSE_Enable_DAA), (3, EFUSE_Enable_SLA),(4, EFUSE_Enable_ACK), (5, EFUSE_Enable_ACC), (6, EFUSE_Enable_SW_JTAG_CON), (7, EFUSE_Enable_Root_Cert)])  #0x48
        writeHexStringToFile(f, EFUSE_C_SEC_CTRL1)  #0x4C    (only 2 digits, auto padding to 8 digits in writeHexStringToFile)
        
        if EFUSE_DISABLE_EFUSE == 1 :
            writeBitValueAndOneHexStringToFile(f, [(7, 1)], EFUSE_C_CTRL1)  #0x50
        else :
            writeBitValueAndOneHexStringToFile(f, [(7, 0)], EFUSE_C_CTRL1)  #0x50   #[IMPORTANT] You MUST still set bit 7 to value 0 as a mask because the priority of bit input(lstBitValue) is higher than hex input(hexString).
            
        writeBitValueToFile(f, [(0, EFUSE_sbc_pubk_hash_lock), (1, EFUSE_ackey_lock), (2, EFUSE_sec_attr_lock)])  #0x54
        writeBitValueToFile(f, [(1, EFUSE_usb_id_lock), (2, EFUSE_com_ctrl_lock)])  #0x58
        
        #extends to 512 bytes file siz
        for i in range(97) : #0x5C
            writeBitValueToFile(f, [(0, 0)])
    
    bin_file_size_before_hash = os.path.getsize(args.output_bin_name)
    
    print
    sha256_hash = get_file_sha256hexdigest(args.output_bin_name)
    print("Image file(" + str(bin_file_size_before_hash) + " bytes) sha256 hash: " + sha256_hash)
    
    with open(args.output_bin_name, "ab") as f :
        writeHexStringToFile(f, sha256_hash[0:8])  #0x1E0
        writeHexStringToFile(f, sha256_hash[8:16]) #0x1E4
        writeHexStringToFile(f, sha256_hash[16:24])  #0x1E8
        writeHexStringToFile(f, sha256_hash[24:32])  #0x1EC
        writeHexStringToFile(f, sha256_hash[32:40])  #0x1F0
        writeHexStringToFile(f, sha256_hash[40:48])  #0x1F4
        writeHexStringToFile(f, sha256_hash[48:56])  #0x1F8
        writeHexStringToFile(f, sha256_hash[56:64])  #0x1FC
        
        print("Append sha256 hash to bin: Done!")
    
    bin_file_size = os.path.getsize(args.output_bin_name)
       
    print
    print("[Success] Write to bin: " + os.path.abspath(args.output_bin_name) + " (size: " + str(bin_file_size) + " bytes)")
    print
    

if __name__ == '__main__':
    main()


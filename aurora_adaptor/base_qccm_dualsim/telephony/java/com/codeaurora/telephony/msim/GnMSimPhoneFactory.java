package com.codeaurora.telephony.msim;
import com.codeaurora.telephony.msim.MSimPhoneFactory;
import com.android.internal.telephony.Phone;
public class GnMSimPhoneFactory
{
	public static int getDefaultSubscription() 
	{
		
		return MSimPhoneFactory.getDefaultSubscription();
		
		}
	public static Phone getPhone(int subscription)
	{
		
		return MSimPhoneFactory.getPhone(subscription);
		
		}
	
	
	}
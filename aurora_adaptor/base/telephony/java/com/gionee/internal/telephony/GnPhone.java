package com.gionee.internal.telephony;

import android.os.RemoteException;
import android.os.ServiceManager;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
/**
 * GnPhone replace for the com.android.internal.telephony.Phone
 * @author guoyangxu
 *
 */
public class GnPhone {
    public static ITelephony phone = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
    
    public static String STATE_KEY = com.android.internal.telephony.PhoneConstants.STATE_KEY;
    public static String FEATURE_ENABLE_MMS = com.android.internal.telephony.Phone.FEATURE_ENABLE_MMS;
    
    public static String REASON_NO_SUCH_PDP = com.android.internal.telephony.PhoneConstants.REASON_NO_SUCH_PDP;
    public static String REASON_APN_FAILED = com.android.internal.telephony.Phone.REASON_APN_FAILED;
    public static String REASON_RADIO_TURNED_OFF = com.android.internal.telephony.Phone.REASON_RADIO_TURNED_OFF;
    public static String REASON_VOICE_CALL_ENDED = com.android.internal.telephony.Phone.REASON_VOICE_CALL_ENDED;
    public static String APN_TYPE_MMS = com.android.internal.telephony.PhoneConstants.APN_TYPE_MMS;
    public static Object APN_TYPE_ALL = com.android.internal.telephony.PhoneConstants.APN_TYPE_ALL;
    
    /**
     * Return codes for <code>enableApnType()</code>
     */
    public static final int APN_ALREADY_ACTIVE     = com.android.internal.telephony.PhoneConstants.APN_ALREADY_ACTIVE;
    public static final int APN_REQUEST_STARTED    = com.android.internal.telephony.PhoneConstants.APN_REQUEST_STARTED;
    public static final int APN_TYPE_NOT_AVAILABLE = com.android.internal.telephony.PhoneConstants.APN_TYPE_NOT_AVAILABLE;
    public static final int APN_REQUEST_FAILED     = com.android.internal.telephony.PhoneConstants.APN_REQUEST_FAILED;
    public static final int APN_ALREADY_INACTIVE   = com.android.internal.telephony.PhoneConstants.APN_ALREADY_INACTIVE;
    
    // Used for preferred network type
    // Note NT_* substitute RILConstants.NETWORK_MODE_* above the Phone
    public static int NT_MODE_WCDMA_PREF   = RILConstants.NETWORK_MODE_WCDMA_PREF;
    public static int NT_MODE_GSM_ONLY     = RILConstants.NETWORK_MODE_GSM_ONLY;
    public static int NT_MODE_WCDMA_ONLY   = RILConstants.NETWORK_MODE_WCDMA_ONLY;
    public static int NT_MODE_GSM_UMTS     = RILConstants.NETWORK_MODE_GSM_UMTS;

    public static int NT_MODE_CDMA         = RILConstants.NETWORK_MODE_CDMA;

    public static int NT_MODE_CDMA_NO_EVDO = RILConstants.NETWORK_MODE_CDMA_NO_EVDO;
    public static int NT_MODE_EVDO_NO_CDMA = RILConstants.NETWORK_MODE_EVDO_NO_CDMA;
    public static int NT_MODE_GLOBAL       = RILConstants.NETWORK_MODE_GLOBAL;

    public static int NT_MODE_LTE_ONLY     = RILConstants.NETWORK_MODE_LTE_ONLY;
    public static int PREFERRED_NT_MODE    = RILConstants.PREFERRED_NETWORK_MODE;

    /**
     * SIM ID for GEMINI
     */
    public static final int GEMINI_SIM_1 = PhoneConstants.GEMINI_SIM_1;
    public static final int GEMINI_SIM_2 = PhoneConstants.GEMINI_SIM_2;
    public static final int GEMINI_SIP_CALL = PhoneConstants.GEMINI_SIP_CALL; //MTK added for SIP call
    public static String GEMINI_SIM_ID_KEY = "simId";//PhoneConstants.GEMINI_SIM_ID_KEY;
    public static String MULTI_SIM_ID_KEY = PhoneConstants.MULTI_SIM_ID_KEY;
    public static String GEMINI_DEFAULT_SIM_PROP = PhoneConstants.GEMINI_DEFAULT_SIM_PROP;
    
    public static String APN_TYPE_DM = PhoneConstants.APN_TYPE_DM;
    public static String APN_TYPE_WAP = PhoneConstants.APN_TYPE_WAP;
    public static String APN_TYPE_NET = PhoneConstants.APN_TYPE_NET;
    public static String APN_TYPE_CMMAIL = PhoneConstants.APN_TYPE_CMMAIL;
    public static String APN_TYPE_TETHERING = PhoneConstants.APN_TYPE_TETHERING;
	
    /** UNKNOWN, invalid value */
    public static final int SIM_INDICATOR_UNKNOWN = PhoneConstants.SIM_INDICATOR_UNKNOWN;
    
    /** ABSENT, no SIM/USIM card inserted for this phone */
    public static final int SIM_INDICATOR_ABSENT = PhoneConstants.SIM_INDICATOR_ABSENT;
    
    /** RADIOOFF,  has SIM/USIM inserted but not in use . */
    public static final int SIM_INDICATOR_RADIOOFF = PhoneConstants.SIM_INDICATOR_RADIOOFF;
    
    /** LOCKED,  has SIM/USIM inserted and the SIM/USIM has been locked. */
    public static final int SIM_INDICATOR_LOCKED = PhoneConstants.SIM_INDICATOR_LOCKED;
    
    /** INVALID : has SIM/USIM inserted and not be locked but failed to register to the network. */
    public static final int SIM_INDICATOR_INVALID = PhoneConstants.SIM_INDICATOR_INVALID;
    
    /** SEARCHING : has SIM/USIM inserted and SIM/USIM state is Ready and is searching for network. */
    public static final int SIM_INDICATOR_SEARCHING = PhoneConstants.SIM_INDICATOR_SEARCHING; 
    
    /** NORMAL = has SIM/USIM inserted and in normal service(not roaming and has no data connection). */
    public static final int SIM_INDICATOR_NORMAL = PhoneConstants.SIM_INDICATOR_NORMAL;
    
    /** ROAMING : has SIM/USIM inserted and in roaming service(has no data connection). */  
    public static final int SIM_INDICATOR_ROAMING = PhoneConstants.SIM_INDICATOR_ROAMING;
    
    /** CONNECTED : has SIM/USIM inserted and in normal service(not roaming) and data connected. */
    public static final int SIM_INDICATOR_CONNECTED = PhoneConstants.SIM_INDICATOR_CONNECTED;
    
    /** ROAMINGCONNECTED = has SIM/USIM inserted and in roaming service(not roaming) and data connected.*/
    public static final int SIM_INDICATOR_ROAMINGCONNECTED = PhoneConstants.SIM_INDICATOR_ROAMINGCONNECTED;
    
    public static final String GEMINI_DEFAULT_SIM_MODE = Phone.GEMINI_DEFAULT_SIM_MODE;

    public static final String GEMINI_GPRS_TRANSFER_TYPE = Phone.GEMINI_GPRS_TRANSFER_TYPE;
    
    public static boolean isTestIccCard() {
        //return phone.isTestIccCard();
        return false;
    }
    
    public static boolean isSimInsert() {       
        try {
           return phone.hasIccCard();
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }    
    }
}

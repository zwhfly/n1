package gionee.telephony;

import java.util.ArrayList;
import java.util.List;
import android.telephony.SmsMemoryStatus;
import gionee.telephony.GnSmsMemoryStatus;
import android.app.PendingIntent;
import android.telephony.SmsManager;

public class GnSmsManager {

    private static final GnSmsManager sInstance = new GnSmsManager();
    
    /**
     * Get the default instance of the SmsManager
     *
     * @return the default instance of the SmsManager
     */
    public static GnSmsManager getDefault() {
        return sInstance;
    }
    
    /**
     * Set the last Incoming Sms SIM Id
     * This function is used for FTA test only
     * 
     * @param simId the sim ID where the last incoming SMS comes from 
     *
     * {@hide}
     */
    public void setLastIncomingSmsSimId(int simId) {
    	SmsManager.getDefault().setLastIncomingSmsSimId(simId);
    }
    
    public void setSmsMemoryStatus (boolean state) {
        SmsManager.getDefault().setSmsMemoryStatus(state);
    }
    
    public void sendMultipartTextMessageWithEncodingType(SmsManager smsManager,
            String dest, String serviceCenter, ArrayList<String> messages,
            int codingType, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
        smsManager.sendMultipartTextMessageWithEncodingType(dest, serviceCenter, messages, 
                codingType, sentIntents, deliveryIntents);
    }

    public SmsMemoryStatus getSmsSimMemoryStatus() {
        return SmsManager.getDefault().getSmsSimMemoryStatus();
    }
    
    public SmsMemoryStatus getSmsSimMemoryStatus(int simId) {
        return SmsManager.getDefault().getSmsSimMemoryStatus();
    }

    public int copyTextMessageToIccCard(String scAddress, String mAddress,
            List<String> messages, int smsStatus, long timeStamp) {
        return SmsManager.getDefault().copyTextMessageToIccCard(scAddress,
                mAddress, messages, smsStatus, timeStamp);
    }
    
    /**
     * copyTextMessageToIccCard is make for mtk compile pass.
     * @param scAddress
     * @param mAddress
     * @param messages
     * @param smsStatus
     * @param timeStamp
     * @return
     */
    public boolean copyTextMessageToIccCard(String scAddress, String mAddress,
            String messages, int smsStatus, long timeStamp) {
        return false;
    }
}

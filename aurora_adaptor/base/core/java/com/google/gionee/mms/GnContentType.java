package com.google.gionee.mms;

import java.util.ArrayList;
import com.google.android.mms.ContentType;
public class GnContentType {

    public static final String TEXT_PLAIN        = ContentType.TEXT_PLAIN;
    public static final String TEXT_HTML         = ContentType.TEXT_HTML;
    public static final String TEXT_VCALENDAR    = ContentType.TEXT_VCALENDAR;
    public static final String TEXT_VCARD        = ContentType.TEXT_VCARD;
    public static final String TEXT_TS           = ContentType.TEXT_TS;
    
    public static final String IMAGE_UNSPECIFIED = ContentType.IMAGE_UNSPECIFIED;
    public static final String IMAGE_JPEG        = ContentType.IMAGE_JPEG;
    public static final String IMAGE_JPG         = ContentType.IMAGE_JPG;
    public static final String IMAGE_GIF         = ContentType.IMAGE_GIF;
    public static final String IMAGE_WBMP        = ContentType.IMAGE_WBMP;
    public static final String IMAGE_PNG         = ContentType.IMAGE_PNG;
    public static final String IMAGE_BMP         = ContentType.IMAGE_BMP;
    
    public static final String AUDIO_UNSPECIFIED = ContentType.AUDIO_UNSPECIFIED;
    public static final String AUDIO_AAC         = ContentType.AUDIO_AAC;
    public static final String AUDIO_AAC_MP4     = "audio/aac_mp4";
    public static final String AUDIO_QCELP       = "audio/qcelp";
    public static final String AUDIO_EVRC        = "audio/evrc";
    public static final String AUDIO_AMR         = ContentType.AUDIO_AMR;
    public static final String AUDIO_IMELODY     = ContentType.AUDIO_IMELODY;
    public static final String AUDIO_MID         = ContentType.AUDIO_MID;
    public static final String AUDIO_MIDI        = ContentType.AUDIO_MIDI;
    public static final String AUDIO_MP3         = ContentType.AUDIO_MP3;
    public static final String AUDIO_MPEG3       = ContentType.AUDIO_MPEG3;
    public static final String AUDIO_MPEG        = ContentType.AUDIO_MPEG;
    public static final String AUDIO_MPG         = ContentType.AUDIO_MPG;
    public static final String AUDIO_MP4         = ContentType.AUDIO_MP4;
    public static final String AUDIO_X_MID       = ContentType.AUDIO_X_MID;
    public static final String AUDIO_X_MIDI      = ContentType.AUDIO_X_MIDI;
    public static final String AUDIO_X_MP3       = ContentType.AUDIO_X_MP3;
    public static final String AUDIO_X_MPEG3     = ContentType.AUDIO_X_MPEG3;
    public static final String AUDIO_X_MPEG      = ContentType.AUDIO_X_MPEG;
    public static final String AUDIO_X_MPG       = ContentType.AUDIO_X_MPG;
    public static final String AUDIO_3GPP        = ContentType.AUDIO_3GPP;
    public static final String AUDIO_OGG         = ContentType.AUDIO_OGG;
    public static final String AUDIO_VORBIS      = ContentType.AUDIO_VORBIS;
    
    public static final String VIDEO_UNSPECIFIED = ContentType.VIDEO_UNSPECIFIED;
    public static final String VIDEO_3GPP        = ContentType.VIDEO_3GPP;
    public static final String VIDEO_3G2         = ContentType.VIDEO_3G2;
    public static final String VIDEO_H263        = ContentType.VIDEO_H263;
    public static final String VIDEO_MP4         = ContentType.VIDEO_MP4;
    public static final String VIDEO_TS          = ContentType.VIDEO_TS;
    
    private static final ArrayList<String> sRestrictedContentTypes = new ArrayList<String>();
    
    static {
        sRestrictedContentTypes.add(IMAGE_JPEG);
        sRestrictedContentTypes.add(IMAGE_JPG);
        sRestrictedContentTypes.add(IMAGE_WBMP);        
        sRestrictedContentTypes.add(IMAGE_GIF);                
        sRestrictedContentTypes.add(TEXT_PLAIN);                        
        sRestrictedContentTypes.add(TEXT_TS);  
        sRestrictedContentTypes.add(AUDIO_MID);                                
        sRestrictedContentTypes.add(AUDIO_MIDI);
        sRestrictedContentTypes.add(AUDIO_AMR);        
        sRestrictedContentTypes.add(AUDIO_VORBIS);        
        sRestrictedContentTypes.add(AUDIO_X_MID);                
        sRestrictedContentTypes.add(AUDIO_X_MIDI);
        sRestrictedContentTypes.add(VIDEO_3GPP);        
        sRestrictedContentTypes.add(VIDEO_3G2);                
        sRestrictedContentTypes.add(VIDEO_MP4);                        
        sRestrictedContentTypes.add(VIDEO_TS); 
    }
    
    private GnContentType() {
    }
    
    public static boolean isRestrictedType(String contentType) {
        return (null != contentType) && sRestrictedContentTypes.contains(contentType);
    }
}

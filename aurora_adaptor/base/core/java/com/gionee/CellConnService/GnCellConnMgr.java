package com.gionee.CellConnService;

import android.content.Context;
import com.mediatek.CellConnService.CellConnMgr;

public class GnCellConnMgr {
    public static final int RESULT_OK = 0;
    public static final int RESULT_WAIT = 1;
    public static final int RESULT_ABORT = 2;
    public static final int RESULT_STATE_NORMAL = 4;

    public static final int REQUEST_TYPE_SIMLOCK = 302;
    public static final int REQUEST_TYPE_FDN = 304;
    public static final int REQUEST_TYPE_ROAMING = 306;
    
    private static CellConnMgr mCellConnMgr;

    public GnCellConnMgr(Runnable mServiceComplete) {
        // TODO Auto-generated constructor stub
        
        mCellConnMgr = new CellConnMgr(mServiceComplete);
    }

    public GnCellConnMgr() {
        // TODO Auto-generated constructor stub
        
        mCellConnMgr = new CellConnMgr();
    }

    public void register(Context applicationContext) {
        // TODO Auto-generated method stub

        mCellConnMgr.register(applicationContext);
    }

    public int getResult() {
        // TODO Auto-generated method stub
        
        return mCellConnMgr.getResult();
    }

    public static String resultToString(int result) {
        // TODO Auto-generated method stub
        
        return mCellConnMgr.resultToString(result);
    }

    public int handleCellConn(int slotId, int req) {
        // TODO Auto-generated method stub
        
        return mCellConnMgr.handleCellConn(slotId, req);
    }

    public void unregister() {
        // TODO Auto-generated method stub

        mCellConnMgr.unregister();
    }

    public int handleCellConn(int slotId, int requestType, Runnable runnable) {
        // TODO Auto-generated method stub
        
        return mCellConnMgr.handleCellConn(slotId, requestType, runnable);
    }

    public int getPreferSlot() {
        // TODO Auto-generated method stub
        
        return mCellConnMgr.getPreferSlot();
    }

}

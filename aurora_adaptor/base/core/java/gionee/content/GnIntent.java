package gionee.content;

import android.content.Intent;

public class GnIntent {

    public static final String ACTION_VOICE_CALL_DEFAULT_SIM_CHANGED = Intent.ACTION_VOICE_CALL_DEFAULT_SIM_CHANGED;
    public static final String ACTION_SMS_DEFAULT_SIM_CHANGED = Intent.ACTION_SMS_DEFAULT_SIM_CHANGED;
    public static final String ACTION_DATA_DEFAULT_SIM_CHANGED = Intent.ACTION_DATA_DEFAULT_SIM_CHANGED;
    public static final String ACTION_ROAMING_REMINDER_SETTING_CHANGED = Intent.ACTION_ROAMING_REMINDER_SETTING_CHANGED;
    public static final String ACTION_SIM_SETTINGS_INFO_CHANGED = Intent.SIM_SETTINGS_INFO_CHANGED;
    public static final String ACTION_DUAL_SIM_MODE_CHANGED = Intent.ACTION_DUAL_SIM_MODE_CHANGED;
    public static final String ACTION_EXTRA_DUAL_SIM_MODE = Intent.EXTRA_DUAL_SIM_MODE;
    public static final String ACTION_THEME_CHANGED = "android.intent.action.THEME_CHANGED";
    public static final String ACTION_FLIP_OVER = "android.intent.action.FLIP_OVER";

    /**
     * Broadcast Action:  Request the media scanner to scan a dir and add it to the media database.
     * The path to the dir is contained in the Intent.mData field.
     * @hide
     */
    public static final String ACTION_MEDIA_SCANNER_SCAN_DIR = "android.intent.action.MEDIA_SCANNER_SCAN_DIR";
}

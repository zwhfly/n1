package gionee.provider;

import java.util.HashSet;
import java.util.List;

import com.android.internal.telephony.ITelephony;
import com.google.android.mms.util.SqliteWrapper;
import android.app.Activity;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.BaseColumns;
import android.provider.Telephony.BaseMmsColumns;
import android.provider.Telephony.TextBasedSmsColumns;

import android.provider.Telephony;
import android.telephony.SmsMessage;
import java.util.ArrayList;
import java.util.List;
import android.text.TextUtils;
import android.content.ContentUris;
import android.database.DatabaseUtils;
import gionee.telephony.GnCbSmsMessage;

public class GnTelephony {
    /**
     * MTK platform is sim_id
     * Qulcomm platform is sub_id
     * 
     */
    public static String GN_SIM_ID = "sim_id";

    public static class CbSms implements BaseColumns, TextBasedCbSmsColumns {

        public static Uri CONTENT_URI = Telephony.CbSms.CONTENT_URI;
        public static Uri ADDRESS_URI = Telephony.CbSms.ADDRESS_URI;
        public static String DEFAULT_SORT_ORDER = Telephony.CbSms.DEFAULT_SORT_ORDER;
        
        public static class CanonicalAddressesColumns {

            public static String ADDRESS = Telephony.CbSms.CanonicalAddressesColumns.ADDRESS;

        }
        
        public static class Conversations {

            public static Uri CONTENT_URI = Telephony.CbSms.Conversations.CONTENT_URI;
            public static String MESSAGE_COUNT = Telephony.CbSms.Conversations.MESSAGE_COUNT;
            public static String SNIPPET = Telephony.CbSms.Conversations.SNIPPET;
            public static String ADDRESS_ID = Telephony.CbSms.Conversations.ADDRESS_ID;

        }

        public static class CbChannel {
            public static Uri CONTENT_URI = Telephony.CbSms.CbChannel.CONTENT_URI;

            public static String NUMBER = Telephony.CbSms.CbChannel.NUMBER;

            public static String NAME = Telephony.CbSms.CbChannel.NAME;
			
			public static String ENABLE = Telephony.CbSms.CbChannel.ENABLE;
        }
        
        public static class Intents {
            public static String CB_SMS_RECEIVED_ACTION = Telephony.CbSms.Intents.CB_SMS_RECEIVED_ACTION;
            
            /**
             * Read the PDUs out of an {@link #CB_SMS_RECEIVED_ACTION}.
             *
             * @param intent the intent to read from
             * @return an array of CbSmsMessages for the PDUs
             */
            public static GnCbSmsMessage[] getMessagesFromIntent(
                    Intent intent) {

                
                return null;
            }
        }

    }
    
    /**
     * Base columns for tables that contain text based CbSMSs.
     */
    public interface TextBasedCbSmsColumns {

        /**
         * The SIM ID which indicated which SIM the CbSMS comes from
         * Reference to Telephony.SIMx
         * <P>Type: INTEGER</P>
         */
        public static String SIM_ID = GN_SIM_ID;

        /**
         * The channel ID of the message
         * which is the message identifier defined in the Spec. 3GPP TS 23.041
         * <P>Type: INTEGER</P>
         */
        public static String CHANNEL_ID = Telephony.TextBasedCbSmsColumns.CHANNEL_ID;

        /**
         * The date the message was sent
         * <P>Type: INTEGER (long)</P>
         */
        public static String DATE = Telephony.TextBasedCbSmsColumns.DATE;

        /**
         * Has the message been read
         * <P>Type: INTEGER (boolean)</P>
         */
        public static String READ = Telephony.TextBasedCbSmsColumns.READ;

        /**
         * The body of the message
         * <P>Type: TEXT</P>
         */
        public static String BODY = Telephony.TextBasedCbSmsColumns.BODY;
        
        /**
         * The thread id of the message
         * <P>Type: INTEGER</P>
         */
        public static String THREAD_ID = Telephony.TextBasedCbSmsColumns.THREAD_ID;

        /**
         * Indicates whether this message has been seen by the user. The "seen" flag will be
         * used to figure out whether we need to throw up a statusbar notification or not.
         */
        public static String SEEN = Telephony.TextBasedCbSmsColumns.SEEN;

        /**
         * Has the message been locked?
         * <P>Type: INTEGER (boolean)</P>
         */
        public static String LOCKED = Telephony.TextBasedCbSmsColumns.LOCKED;
    }

    public static class Sms implements TextBasedSmsColumns{

        public static String _ID = Telephony.Sms._ID;
        public static String THREAD_ID = Telephony.Sms.THREAD_ID;
        public static String ADDRESS = Telephony.Sms.ADDRESS;
        public static String BODY = Telephony.Sms.BODY;
        public static String STATUS = Telephony.Sms.STATUS;
        public static Uri CONTENT_URI = Telephony.Sms.CONTENT_URI;
        public static int STATUS_PENDING = Telephony.Sms.STATUS_PENDING;
        public static String TYPE = Telephony.Sms.TYPE;
        public static int MESSAGE_TYPE_SENT = Telephony.Sms.MESSAGE_TYPE_SENT;
        public static int MESSAGE_TYPE_OUTBOX = Telephony.Sms.MESSAGE_TYPE_OUTBOX;
        public static int MESSAGE_TYPE_QUEUED = Telephony.Sms.MESSAGE_TYPE_QUEUED;
        public static int MESSAGE_TYPE_FAILED = Telephony.Sms.MESSAGE_TYPE_FAILED;
        public static int STATUS_FAILED = Telephony.Sms.STATUS_FAILED;
        public static String PROTOCOL = Telephony.Sms.PROTOCOL;
        public static String SIM_ID = GN_SIM_ID;
        public static String ERROR_CODE = Telephony.Sms.ERROR_CODE;
        public static String DATE = Telephony.Sms.DATE;
        public static String READ = Telephony.Sms.READ;
        public static String LOCKED = Telephony.Sms.LOCKED;
        public static String SERVICE_CENTER = Telephony.Sms.SERVICE_CENTER;
        public static String STAR = "star";
        public static String DATE_SENT = "date_sent";
        
        /**
         * Indicates whether this message has been seen by the user. The "seen" flag will be
         * used to figure out whether we need to throw up a statusbar notification or not.
         */
        public static String SEEN = Telephony.Sms.SEEN;
        public static boolean moveMessageToFolder(Context context, Uri uri,
                int messageTypeSent, int error) {
            return Telephony.Sms.moveMessageToFolder(context, uri, messageTypeSent, error);
        }

        public static class Inbox {

            public static String ADDRESS = Telephony.Sms.Inbox.ADDRESS;
            public static String DATE = Telephony.Sms.Inbox.DATE;
            public static String PROTOCOL = Telephony.Sms.Inbox.PROTOCOL;
            public static String READ = Telephony.Sms.Inbox.READ;
            public static String SIM_ID = android.provider.Telephony.Sms.Inbox.SIM_ID;
            public static String SEEN = Telephony.Sms.Inbox.SEEN;
            public static String SUBJECT = Telephony.Sms.Inbox.SUBJECT;
            public static String REPLY_PATH_PRESENT = Telephony.Sms.Inbox.REPLY_PATH_PRESENT;
            public static String SERVICE_CENTER = Telephony.Sms.Inbox.SERVICE_CENTER;
            public static String BODY = Telephony.Sms.Inbox.BODY;
            public static Uri CONTENT_URI = Telephony.Sms.Inbox.CONTENT_URI;
            public static Uri addMessage(ContentResolver mContentResolver, String address2,
                    String body2, String subject, Long date2, boolean b, int mSimId) {
                // TODO Auto-generated method stub
                return Telephony.Sms.Inbox.addMessage(mContentResolver, address2, body2, subject, date2, b);

            }
            public static Uri addMessage(ContentResolver mContentResolver, String address2,
                    String body2, String subject, Long date2, boolean b) {
                return Telephony.Sms.Inbox.addMessage(mContentResolver, address2, body2, subject, date2, b);
            }
            
            public static String THREAD_ID = Telephony.Sms.Inbox.THREAD_ID;
            
            public static Uri addMessage(ContentResolver mContentResolver,
                    String address2, String body2, String subject2,
                    String serviceCenter, Long date2, boolean b) {
                return Telephony.Sms.Inbox.addMessage(mContentResolver, address2,
                        body2, subject2, serviceCenter, date2, b);
            }
			//Gionee guoyx 20130225 modified for CR00772795 begin
            public static Uri addMessage(ContentResolver Content,
                    String address, String body, String subject,
                    String serviceCenter, Long date, boolean read, int simId) {
                return Telephony.Sms.Inbox.addMessage(Content, address,
                        body, subject, serviceCenter, date, read, simId);
            }
			//Gionee guoyx 20130225 modified for CR00772795 end

        }
        public static class Sent {

            public static Uri addMessage(ContentResolver mContentResolver, String address,
                    String body, String subject, Long date, int mSimId) {
                return Telephony.Sms.Sent.addMessage(mContentResolver, address, body, subject, date);
            }

            public static Uri addMessage(ContentResolver mContentResolver, String address,
                    String body, String subject, Long date) {
                return Telephony.Sms.Sent.addMessage(mContentResolver, address, body, subject, date);
            }

            public static Uri addMessage(ContentResolver mContentResolver,
                    String address, String body, String subject,
                    String serviceCenter, Long date) {
                return Telephony.Sms.Sent.addMessage(mContentResolver, address, body,
                        subject, serviceCenter, date);

            }

            //Gionee guoyx 20130225 modified for CR00772795 begin
            public static Uri addMessage(ContentResolver Content,
                    String address, String body, String subject,
                    String serviceCenter, Long date, int simId) {
                return Telephony.Sms.Sent.addMessage(Content, address, body,
                        subject, serviceCenter, date, simId);
            }
			//Gionee guoyx 20130225 modified for CR00772795 end

        }

        public static class Intents {

            public static SmsMessage[] getMessagesFromIntent(Intent intent) {
                SmsMessage msg[] = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                return msg;
            }
            public static String SMS_CB_RECEIVED_ACTION =
            		Telephony.Sms.Intents.SMS_CB_RECEIVED_ACTION;
            
            public static String SMS_EMERGENCY_CB_RECEIVED_ACTION =
            		Telephony.Sms.Intents.SMS_EMERGENCY_CB_RECEIVED_ACTION;
        }

        public static boolean isOutgoingFolder(int mBoxId) {
            // TODO Auto-generated method stub
            return Telephony.Sms.isOutgoingFolder(mBoxId);
        }
        
        public static Cursor query(ContentResolver cr, String[] projection,
                String where, String orderBy) {
            return Telephony.Sms.query(cr, projection, where, orderBy);
        }

    }

    public static class Mms implements BaseMmsColumns {

        public static String THREAD_ID = Telephony.Mms.THREAD_ID;
        public static String READ = Telephony.Mms.READ;
        public static String SIM_ID = Telephony.Mms.SIM_ID;
        public static String SEND_DATE = "send_date";
        public static String MESSAGE_ID = Telephony.Mms.MESSAGE_ID;
        public static Uri CONTENT_URI = Telephony.Mms.CONTENT_URI;
        public static String CONTENT_LOCATION = Telephony.Mms.CONTENT_LOCATION;
        public static String MESSAGE_TYPE = Telephony.Mms.MESSAGE_TYPE;
        public static String LOCKED = Telephony.Mms.LOCKED;
        public static String MESSAGE_SIZE = Telephony.Mms.MESSAGE_SIZE;
        public static String DATE = Telephony.Mms.DATE;
        public static String RESPONSE_STATUS =Telephony.Mms.RESPONSE_STATUS;
        public static String SUBJECT = Telephony.Mms.SUBJECT;
        public static String SUBJECT_CHARSET = Telephony.Mms.SUBJECT_CHARSET;
        public static String MESSAGE_BOX = Telephony.Mms.MESSAGE_BOX;
        public static String DELIVERY_REPORT = Telephony.Mms.DELIVERY_REPORT;
        public static String READ_REPORT = Telephony.Mms.READ_REPORT;
        public static String STAR = "star";//android.provider.Telephony.Mms.THREAD_ID;
        public static final int MESSAGE_BOX_INBOX = Telephony.Mms.MESSAGE_BOX_INBOX;
        public static final int MESSAGE_BOX_DRAFTS = Telephony.Mms.MESSAGE_BOX_DRAFTS;
        public static final int MESSAGE_BOX_OUTBOX = Telephony.Mms.MESSAGE_BOX_OUTBOX;
        public static String _ID = Telephony.Mms._ID;
        public static String DATE_SENT = Telephony.Mms.DATE_SENT;
        public static String SERVICE_CENTER = Telephony.Mms.SERVICE_CENTER;
        public static boolean isPhoneNumber(String mAddress) {
            // TODO Auto-generated method stub
            return Telephony.Mms.isPhoneNumber(mAddress);
        }
        public static boolean isEmailAddress(String mAddress) {
            // TODO Auto-generated method stub
            return Telephony.Mms.isEmailAddress(mAddress);
        }

        public static class Inbox {

            public static Uri CONTENT_URI = Telephony.Mms.Inbox.CONTENT_URI;

        }

        public static class Part {

            public static String _DATA = Telephony.Mms.Part._DATA;

        }
        
        public static class ScrapSpace {
            /**
             * The content:// style URL for this table
             */
            public static Uri CONTENT_URI = Telephony.Mms.ScrapSpace.CONTENT_URI;

            /**
             * This is the scrap file we use to store the media attachment when the user
             * chooses to capture a photo to be attached . We pass {#link@Uri} to the Camera app,
             * which streams the captured image to the uri. Internally we write the media content
             * to this file. It's named '.temp.jpg' so Gallery won't pick it up.
             */
            public static String SCRAP_FILE_PATH = Telephony.Mms.ScrapSpace.SCRAP_FILE_PATH;
        }
        
        public static class Draft {
            public static Uri
                    CONTENT_URI = Telephony.Mms.Draft.CONTENT_URI;
            
            public static String DEFAULT_SORT_ORDER = Telephony.Mms.Draft.DEFAULT_SORT_ORDER;
        }
        
        public static class Outbox {
            /**
             * The content:// style URL for this table
             */
            public static Uri
                    CONTENT_URI = Telephony.Mms.Outbox.CONTENT_URI;

            /**
             * The default sort order for this table
             */
            public static String DEFAULT_SORT_ORDER = Telephony.Mms.Outbox.DEFAULT_SORT_ORDER;
        }

    }
    public static class MmsSms {
        /**
         * The column to distinguish SMS &amp; MMS messages in query results.
         */
        public static String TYPE_DISCRIMINATOR_COLUMN =
                "transport_type";
        public static class PendingMessages {

            public static String SIM_ID = android.provider.Telephony.MmsSms.PendingMessages.SIM_ID;
            public static String _ID = Telephony.MmsSms.PendingMessages._ID;
            public static Uri CONTENT_URI = Telephony.MmsSms.PendingMessages.CONTENT_URI;
            public static String MSG_TYPE = Telephony.MmsSms.PendingMessages.MSG_TYPE;
            public static String MSG_ID = Telephony.MmsSms.PendingMessages.MSG_ID;
            public static String ERROR_TYPE = Telephony.MmsSms.PendingMessages.ERROR_TYPE;
            public static String RETRY_INDEX = Telephony.MmsSms.PendingMessages.RETRY_INDEX;
            public static String PROTO_TYPE = Telephony.MmsSms.PendingMessages.PROTO_TYPE;
            public static String ERROR_CODE = Telephony.MmsSms.PendingMessages.ERROR_CODE;
            public static String DUE_TIME = Telephony.MmsSms.PendingMessages.DUE_TIME;
            public static String LAST_TRY = Telephony.MmsSms.PendingMessages.LAST_TRY;

        }

        public static class WordsTable {

            public static String ID = Telephony.MmsSms.WordsTable.ID;
            public static String INDEXED_TEXT = Telephony.MmsSms.WordsTable.INDEXED_TEXT;
            public static String SOURCE_ROW_ID = Telephony.MmsSms.WordsTable.SOURCE_ROW_ID;
            public static String TABLE_ID = Telephony.MmsSms.WordsTable.TABLE_ID;

        }

        public static Uri CONTENT_URI_QUICKTEXT = Telephony.MmsSms.CONTENT_URI_QUICKTEXT;
    }
    public static class Threads{
        public static String _ID = Telephony.Threads._ID;
        public static String MESSAGE_COUNT = Telephony.Threads.MESSAGE_COUNT;
        public static Uri CONTENT_URI = Telephony.Threads.CONTENT_URI;
        public static final int WAPPUSH_THREAD = android.provider.Telephony.Threads.WAPPUSH_THREAD;
        public static final int CELL_BROADCAST_THREAD = android.provider.Telephony.Threads.CELL_BROADCAST_THREAD;
        public static Uri OBSOLETE_THREADS_URI = Telephony.Threads.OBSOLETE_THREADS_URI;
        public static String SNIPPET = Telephony.Threads.SNIPPET;
        public static String DATE = Telephony.Threads.DATE;
        public static String RECIPIENT_IDS = Telephony.Threads.RECIPIENT_IDS;
        public static String SNIPPET_CHARSET = Telephony.Threads.SNIPPET_CHARSET;
        public static String READ = Telephony.Threads.READ;
        public static String ERROR = Telephony.Threads.ERROR;
        public static String HAS_ATTACHMENT = Telephony.Threads.HAS_ATTACHMENT;
        public static String TYPE = Telephony.Threads.TYPE;
        public static String SIM_ID = GN_SIM_ID;
        public static int BROADCAST_THREAD = 1;
        public static String READCOUNT = "readcount";
        public static String STATUS = "status";
        public static String ENCRYPTION = "encryption";
        public static long getOrCreateThreadId(Context context, HashSet<String> recipients) {
            // TODO Auto-generated method stub
            return Telephony.Threads.getOrCreateThreadId(context, recipients);
        }
        
        public static long getOrCreateThreadId(Context context, String recipients) {
            // TODO Auto-generated method stub
            return Telephony.Threads.getOrCreateThreadId(context, recipients);
        }
        //Gionee guoyx 20130312 add for CR00779734 begin
        public static final int IP_MESSAGE_GUIDE_THREAD = 10;
        //Gionee guoyx 20130312 add for CR00779734 end
    }

    public static class Carriers {

        public static String TYPE = Telephony.Carriers.TYPE;
        public static String MMSC = Telephony.Carriers.MMSC;
        public static String MMSPROXY = Telephony.Carriers.MMSPROXY;
        public static String MMSPORT = Telephony.Carriers.MMSPORT;
        public static String APN = Telephony.Carriers.APN;
        public static Uri CONTENT_URI = Telephony.Carriers.CONTENT_URI;
        public static String NUMERIC = Telephony.Carriers.NUMERIC;
        public static String MCC = Telephony.Carriers.MCC;
        public static String MNC = Telephony.Carriers.MNC;
        public static String NAME = Telephony.Carriers.NAME;
        public static String USER = Telephony.Carriers.USER;
        public static String SERVER = Telephony.Carriers.SERVER;
        public static String PASSWORD = Telephony.Carriers.PASSWORD;
        public static String PORT = Telephony.Carriers.PORT;
        public static String PROXY = Telephony.Carriers.PROXY;
        public static String AUTH_TYPE = Telephony.Carriers.AUTH_TYPE;
        public static String ROAMING_PROTOCOL = Telephony.Carriers.ROAMING_PROTOCOL;
        public static Uri CONTENT_URI_DM = Telephony.Carriers.CONTENT_URI_DM;
        public static String _ID = Telephony.Carriers._ID;
        public static String CURRENT = Telephony.Carriers.CURRENT;
        
        public static String PROTOCOL = Telephony.Carriers.PROTOCOL;
        public static String BEARER = Telephony.Carriers.BEARER;
        public static String CARRIER_ENABLED = Telephony.Carriers.CARRIER_ENABLED;
        public static String OMACPID = Telephony.Carriers.OMACPID;
        public static String NAPID = Telephony.Carriers.NAPID;
        public static String PROXYID = Telephony.Carriers.PROXYID;
        public static String SOURCE_TYPE = Telephony.Carriers.SOURCE_TYPE;
        public static String CSD_NUM = Telephony.Carriers.CSD_NUM;
        
        public static String SPN = "spn";
        public static String IMSI = "imsi";
        public static String PNN = "pnn";
 
        public static class SIM1Carriers {
            public static Uri CONTENT_URI =
                Uri.parse("content://telephony/carriers_sim1"); 
        }
        
        public static class SIM2Carriers {
            public static Uri CONTENT_URI =
                Uri.parse("content://telephony/carriers_sim2"); 
        }

        
        public static class GeminiCarriers {

            public static Uri CONTENT_URI = Telephony.Carriers.GeminiCarriers.CONTENT_URI;
            public static Uri CONTENT_URI_DM = Telephony.Carriers.GeminiCarriers.CONTENT_URI_DM;

        }

    }

    public static class WapPush {

        public static String DEFAULT_SORT_ORDER = Telephony.WapPush.DEFAULT_SORT_ORDER;
        public static Uri CONTENT_URI = Telephony.WapPush.CONTENT_URI;
        public static Uri CONTENT_URI_SI = Telephony.WapPush.CONTENT_URI_SI;
        public static Uri CONTENT_URI_SL = Telephony.WapPush.CONTENT_URI_SL;
        public static Uri CONTENT_URI_THREAD = Telephony.WapPush.CONTENT_URI_THREAD;

        public static String THREAD_ID = Telephony.WapPush.THREAD_ID;
        public static String ADDR = Telephony.WapPush.ADDR;
        public static String SERVICE_ADDR = Telephony.WapPush.SERVICE_ADDR;
        public static String READ = Telephony.WapPush.READ;
        public static String DATE = Telephony.WapPush.DATE;
        public static String TYPE = Telephony.WapPush.TYPE;
        public static String SIID = Telephony.WapPush.SIID;
        public static String URL = Telephony.WapPush.URL;
        public static String CREATE = Telephony.WapPush.CREATE;
        public static String EXPIRATION = Telephony.WapPush.EXPIRATION;
        public static String ACTION = Telephony.WapPush.ACTION;
        public static String TEXT = Telephony.WapPush.TEXT;
        public static String SIM_ID = Telephony.WapPush.SIM_ID;
        public static String LOCKED = Telephony.WapPush.LOCKED;
        public static String ERROR = Telephony.WapPush.ERROR;
        public static int TYPE_SL = Telephony.WapPush.TYPE_SL;
        public static int TYPE_SI = Telephony.WapPush.TYPE_SI;
        public static String _ID= Telephony.WapPush._ID;
        public static String SEEN = Telephony.WapPush.SEEN;
        
        public static int STATUS_SEEN = Telephony.WapPush.STATUS_SEEN;
        public static int STATUS_UNSEEN = Telephony.WapPush.STATUS_UNSEEN;
        
        public static int STATUS_READ = Telephony.WapPush.STATUS_READ;
        public static int STATUS_UNREAD = Telephony.WapPush.STATUS_UNREAD;
        public static int STATUS_LOCKED = Telephony.WapPush.STATUS_LOCKED;
        public static int STATUS_UNLOCKED = Telephony.WapPush.STATUS_UNLOCKED;

        // Gionee Linfeng Jia 2011-06-12 added for CR00272690 start
        public static int STATUS_STAR = 1;
        public static int STATUS_UNSTAR = 0;
        
    }
	
    public static class GprsInfo {

        public static Uri CONTENT_URI = Telephony.GprsInfo.CONTENT_URI;
        public static String SIM_ID = Telephony.GprsInfo.SIM_ID;
        public static String GPRS_IN = Telephony.GprsInfo.GPRS_IN;
        public static String GPRS_OUT = Telephony.GprsInfo.GPRS_OUT;

        public static String _ID = Telephony.GprsInfo._ID;
    }

  //MTK-START [mtk04070][111121][ALPS00093395]MTK added
    public static class SimInfo implements BaseColumns{
        public static Uri CONTENT_URI = 
        		Telephony.SimInfo.CONTENT_URI;
        
        public static String DEFAULT_SORT_ORDER = Telephony.SimInfo.DEFAULT_SORT_ORDER;
        
        /**
         * <P>Type: TEXT</P>
         */
        public static String ICC_ID = Telephony.SimInfo.ICC_ID;
        /**
         * <P>Type: TEXT</P>
         */
        public static String DISPLAY_NAME = Telephony.SimInfo.DISPLAY_NAME;
        public static int DEFAULT_NAME_MIN_INDEX = Telephony.SimInfo.DEFAULT_NAME_MIN_INDEX;
        public static int DEFAULT_NAME_MAX_INDEX= Telephony.SimInfo.DEFAULT_NAME_MAX_INDEX;
//        public static final int DEFAULT_NAME_RES = com.mediatek.internal.R.string.new_sim;
        public static int DEFAULT_NAME_RES = Telephony.SimInfo.DEFAULT_NAME_RES;
        /**
         * <P>Type: TEXT</P>
         */
        public static String NUMBER = Telephony.SimInfo.NUMBER;
        
        /**
         * 0:none, 1:the first four digits, 2:the last four digits.
         * <P>Type: INTEGER</P>
         */
        public static String DISPLAY_NUMBER_FORMAT = Telephony.SimInfo.DISPLAY_NUMBER_FORMAT;
        public static final int DISPLAY_NUMBER_NONE = 0;//Telephony.SimInfo.DISPLAY_NUMBER_NONE;
        public static final int DISPLAY_NUMBER_FIRST = 1;//Telephony.SimInfo.DISPLAY_NUMBER_FIRST;
        public static final int DISPLAY_NUMBER_LAST = 2;//Telephony.SimInfo.DISPLAY_NUMBER_LAST;
    // gionee wangyaohui 20120615 modify for CR00616244 begin
        // public static final int DISLPAY_NUMBER_DEFAULT = DISPLAY_NUMBER_FIRST;
        public static final int DISLPAY_NUMBER_DEFAULT = DISPLAY_NUMBER_NONE;
    // gionee wangyaohui 20120615 modify for CR00616244 end 
        
        /**
         * Eight kinds of colors. 0-3 will represent the eight colors.
         * Default value: any color that is not in-use.
         * <P>Type: INTEGER</P>
         */
        public static String COLOR = Telephony.SimInfo.COLOR;
        public static int COLOR_1 = Telephony.SimInfo.COLOR_1;
        public static int COLOR_2 = Telephony.SimInfo.COLOR_2;
        public static int COLOR_3 = Telephony.SimInfo.COLOR_3;
        public static int COLOR_4 = Telephony.SimInfo.COLOR_4;
        public static int COLOR_5 = 5;
        public static int COLOR_6 = 6;
        public static int COLOR_7 = 7;
        public static int COLOR_8 = 8;
        public static int COLOR_DEFAULT = COLOR_1;
        
        /**
         * 0: Don't allow data when roaming, 1:Allow data when roaming
         * <P>Type: INTEGER</P>
         */
        public static String DATA_ROAMING = Telephony.SimInfo.DATA_ROAMING;
        public static int DATA_ROAMING_ENABLE = Telephony.SimInfo.DATA_ROAMING_ENABLE;
        public static int DATA_ROAMING_DISABLE = Telephony.SimInfo.DATA_ROAMING_DISABLE;
        public static int DATA_ROAMING_DEFAULT = DATA_ROAMING_DISABLE;
        
        /**
         * <P>Type: INTEGER</P>
         */
        public static String SLOT = Telephony.SimInfo.SLOT;
        public static int SLOT_NONE = Telephony.SimInfo.SLOT_NONE;
        
        public static int ERROR_GENERAL = Telephony.SimInfo.ERROR_GENERAL;
        public static int ERROR_NAME_EXIST = Telephony.SimInfo.ERROR_NAME_EXIST;
        
        /**
         * <p>Type: INTEGER<p>
         */
        public static String WAP_PUSH = Telephony.SimInfo.WAP_PUSH;
        public static int WAP_PUSH_DEFAULT = Telephony.SimInfo.WAP_PUSH_DEFAULT;
        public static int WAP_PUSH_DISABLE = Telephony.SimInfo.WAP_PUSH_DISABLE;
        public static int WAP_PUSH_ENABLE = Telephony.SimInfo.WAP_PUSH_ENABLE;
        
        
    }
    
    public static int[] SIMBackgroundRes = Telephony.SIMBackgroundRes;
    
    public static class SIMInfo {
        public long mSimId;
        public String mICCId;
        public static int mSimContactPhotoRes = 0;

        public String mDisplayName = "";
        public String mNumber = "";
        public int mDispalyNumberFormat = SimInfo.DISLPAY_NUMBER_DEFAULT;
        public int mColor;
        public int mDataRoaming = SimInfo.DATA_ROAMING_DEFAULT;
        public int mSlot = SimInfo.SLOT_NONE;
        public int mSimBackgroundRes = SIMBackgroundRes[SimInfo.COLOR_DEFAULT];
        public int mWapPush = -1;
        private SIMInfo() {
        }
        
        public static class ErrorCode {
            public static int ERROR_GENERAL = -1;
            public static int ERROR_NAME_EXIST = -2;
        }
        private static SIMInfo fromCursor(Cursor cursor) {
            SIMInfo info = new SIMInfo();
            info.mSimId = cursor.getLong(cursor.getColumnIndexOrThrow(SimInfo._ID));
            info.mICCId = cursor.getString(cursor.getColumnIndexOrThrow(SimInfo.ICC_ID));
            info.mDisplayName = cursor.getString(cursor.getColumnIndexOrThrow(SimInfo.DISPLAY_NAME));
            info.mNumber = cursor.getString(cursor.getColumnIndexOrThrow(SimInfo.NUMBER));
            info.mDispalyNumberFormat = cursor.getInt(cursor.getColumnIndexOrThrow(SimInfo.DISPLAY_NUMBER_FORMAT));
            info.mColor = cursor.getInt(cursor.getColumnIndexOrThrow(SimInfo.COLOR));
            info.mDataRoaming = cursor.getInt(cursor.getColumnIndexOrThrow(SimInfo.DATA_ROAMING));
            info.mSlot = cursor.getInt(cursor.getColumnIndexOrThrow(SimInfo.SLOT));
            int size = SIMBackgroundRes.length;
            if (info.mColor >= 0 && info.mColor < size) {
                info.mSimBackgroundRes = SIMBackgroundRes[info.mColor];
            }
            info.mWapPush = cursor.getInt(cursor.getColumnIndexOrThrow(SimInfo.WAP_PUSH));
            return info;
        }
        
        /**
         * 
         * @param ctx
         * @return the array list of Current SIM Info
         */
        public static List<SIMInfo> getInsertedSIMList(Context ctx) {
            ArrayList<SIMInfo> simList = new ArrayList<SIMInfo>();
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    null, SimInfo.SLOT + "!=" + SimInfo.SLOT_NONE, null, "slot");
            try {
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        simList.add(SIMInfo.fromCursor(cursor));
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return simList;
        	//return Telephony.SIMInfo.getInsertedSIMList(ctx);
        }
        
        /**
         * 
         * @param ctx
         * @return array list of all the SIM Info include what were used before
         */
        public static List<SIMInfo> getAllSIMList(Context ctx) {
            ArrayList<SIMInfo> simList = new ArrayList<SIMInfo>();
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    null, null, null, null);
            try {
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        simList.add(SIMInfo.fromCursor(cursor));
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return simList;
        	//return Telephony.SIMInfo.getAllSIMList(ctx);
        }
        
        /**
         * 
         * @param ctx
         * @param SIMId the unique SIM id
         * @return SIM-Info, maybe null
         */
        public static SIMInfo getSIMInfoById(Context ctx, long SIMId) {
            if (SIMId <= 0 ) return null;
            Cursor cursor = ctx.getContentResolver().query(ContentUris.withAppendedId(SimInfo.CONTENT_URI, SIMId), 
                    null, null, null, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return SIMInfo.fromCursor(cursor);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return null;
        	//return Telephony.SIMInfo.getSIMInfoById(ctx, SIMId);
        }
        
        /**
         * 
         * @param ctx
         * @param SIMName the Name of the SIM Card
         * @return SIM-Info, maybe null
         */
        public static SIMInfo getSIMInfoByName(Context ctx, String SIMName) {
            if (SIMName == null) return null;
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    null, SimInfo.DISPLAY_NAME + "=?", new String[]{SIMName}, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return SIMInfo.fromCursor(cursor);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return null;
        	//return Telephony.SIMInfo.getSIMInfoByName(ctx, SIMName);
        }
        
        /**
         * @param ctx
         * @param cardSlot
         * @return The SIM-Info, maybe null
         */
        public static SIMInfo getSIMInfoBySlot(Context ctx, int cardSlot) {
            if (cardSlot < 0) return null;
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    null, SimInfo.SLOT + "=?", new String[]{String.valueOf(cardSlot)}, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return SIMInfo.fromCursor(cursor);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return null;
        	//return Telephony.SIMInfo.getSIMInfoBySlot(ctx, cardSlot);
        }
        
        /**
         * @param ctx
         * @param iccid 
         * @return The SIM-Info, maybe null
         */
        public static SIMInfo getSIMInfoByICCId(Context ctx, String iccid) {
            if (iccid == null) return null;
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    null, SimInfo.ICC_ID + "=?", new String[]{iccid}, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return SIMInfo.fromCursor(cursor);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return null;
        	//return Telephony.SIMInfo.getSIMInfoByICCId(ctx, iccid);
        }
        
        /**
         * @param ctx
         * @param SIMId
         * @return the slot of the SIM Card, -1 indicate that the SIM card is missing
         */
        public static int getSlotById(Context ctx, long SIMId) {
            if (SIMId <= 0 ) return SimInfo.SLOT_NONE;
            Cursor cursor = ctx.getContentResolver().query(ContentUris.withAppendedId(SimInfo.CONTENT_URI, SIMId), 
                    new String[]{SimInfo.SLOT}, null, null, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return cursor.getInt(0);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return  SimInfo.SLOT_NONE;
        	//return Telephony.SIMInfo.getSlotById(ctx, SIMId);
        }
        
        /**
         * @param ctx
         * @param SIMName
         * @return the slot of the SIM Card, -1 indicate that the SIM card is missing
         */
        public static int getSlotByName(Context ctx, String SIMName) {
            if (SIMName == null) return SimInfo.SLOT_NONE;
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
                    new String[]{SimInfo.SLOT}, SimInfo.DISPLAY_NAME + "=?", new String[]{SIMName}, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        return cursor.getInt(0);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            return SimInfo.SLOT_NONE;
        	//return Telephony.SIMInfo.getSlotByName(ctx, SIMName);
        }
        
        /**
         * @param ctx
         * @return current SIM Count
         */
        public static int getInsertedSIMCount(Context ctx) {
//            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
//                    null, SimInfo.SLOT + "!=" + SimInfo.SLOT_NONE, null, null);
//            try {
//                if (cursor != null) {
//                    return cursor.getCount();
//                }
//            } finally {
//                if (cursor != null) {
//                    cursor.close();
//                }
//            }
//            return 0;
        	return Telephony.SIMInfo.getInsertedSIMCount(ctx);
        }
        
        /**
         * @param ctx
         * @return the count of all the SIM Card include what was used before
         */
        public static int getAllSIMCount(Context ctx) {
//            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, 
//                    null, null, null, null);
//            try {
//                if (cursor != null) {
//                    return cursor.getCount();
//                }
//            } finally {
//                if (cursor != null) {
//                    cursor.close();
//                }
//            }
//            return 0;
        	return Telephony.SIMInfo.getAllSIMCount(ctx);
        }
        
        /**
         * set display name by SIM ID
         * @param ctx
         * @param displayName
         * @param SIMId
         * @return -1 means general error, -2 means the name is exist. >0 means success
         */
        public static int setDisplayName(Context ctx, String displayName, long SIMId) {
        	return Telephony.SIMInfo.setDisplayName(ctx, displayName, SIMId);
        }
        
        /**
         * @param ctx
         * @param number
         * @param SIMId
         * @return >0 means success
         */
        public static int setNumber(Context ctx, String number, long SIMId) {
        	return Telephony.SIMInfo.setNumber(ctx, number, SIMId);
        }
        
        /**
         * 
         * @param ctx
         * @param color
         * @param SIMId
         * @return >0 means success
         */
        public static int setColor(Context ctx, int color, long SIMId) {
        	return Telephony.SIMInfo.setColor(ctx, color, SIMId);
        }
        
        /**
         * set the format.0: none, 1: the first four digits, 2: the last four digits.
         * @param ctx
         * @param format
         * @param SIMId
         * @return >0 means success
         */
        public static int setDispalyNumberFormat(Context ctx, int format, long SIMId) {
        	return Telephony.SIMInfo.setDispalyNumberFormat(ctx, format, SIMId);
        }
        
        /**
         * set data roaming.0:Don't allow data when roaming, 1:Allow data when roaming
         * @param ctx
         * @param roaming
         * @param SIMId
         * @return >0 means success
         */
        public static int setDataRoaming(Context ctx, int roaming, long SIMId) {
        	return Telephony.SIMInfo.setDataRoaming(ctx, roaming, SIMId);
        }
        
        /**
         * set the wap push flag
         * @return >0 means success
         */
        public static int setWAPPush(Context ctx, int enable, long SIMId) {
        	return Telephony.SIMInfo.setWAPPush(ctx, enable, SIMId);
        }
        /**
         * Insert the ICC ID and slot if needed
         * @param ctx
         * @param ICCId
         * @param slot
         * @return
         */
        public static Uri insertICCId(Context ctx, String ICCId, int slot) {
        	return Telephony.SIMInfo.insertICCId(ctx, ICCId, slot);
        }
        
        public static int setDefaultName(Context ctx, long simId, String name) {
        	           if (simId <= 0)
                return ErrorCode.ERROR_GENERAL;
            String default_name = ctx.getString(SimInfo.DEFAULT_NAME_RES);
            ContentResolver resolver = ctx.getContentResolver();
            Uri uri = ContentUris.withAppendedId(SimInfo.CONTENT_URI, simId);
            if (name != null) {
                int result = setDisplayName(ctx, name, simId);
                if (result > 0) {
                    return result;
                }
            }
            int index = getAppropriateIndex(ctx, simId, name);
            String suffix = getSuffixFromIndex(index);
            ContentValues value = new ContentValues(1);
            String display_name = (name == null ? default_name + " " + suffix : name + " " + suffix);
            value.put(SimInfo.DISPLAY_NAME, display_name);
            return ctx.getContentResolver().update(uri, value, null, null);
        }
        
        private static String getSuffixFromIndex(int index) {
            if (index < 10) {
                return "0" + index;
            } else {
                return String.valueOf(index);
            }
            
        }
        private static int getAppropriateIndex(Context ctx, long simId, String name) {
            String default_name = ctx.getString(SimInfo.DEFAULT_NAME_RES);
            StringBuilder sb = new StringBuilder(SimInfo.DISPLAY_NAME + " LIKE ");
            if (name == null) {
                DatabaseUtils.appendEscapedSQLString(sb, default_name + '%');
            } else {
                DatabaseUtils.appendEscapedSQLString(sb, name + '%');
            }
            sb.append(" AND (");
            sb.append(SimInfo._ID + "!=" + simId);
            sb.append(")");
            
            Cursor cursor = ctx.getContentResolver().query(SimInfo.CONTENT_URI, new String[]{SimInfo._ID, SimInfo.DISPLAY_NAME},
                    sb.toString(), null, SimInfo.DISPLAY_NAME);
            ArrayList<Long> array = new ArrayList<Long>();
            int index = SimInfo.DEFAULT_NAME_MIN_INDEX;
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String display_name = cursor.getString(1);
                    
                    if (display_name != null) {
                        int length = display_name.length();
                        if (length >= 2) {
                            String sub = display_name.substring(length -2);
                            if (TextUtils.isDigitsOnly(sub)) {
                                long value = Long.valueOf(sub);
                                array.add(value);
                            }
                        }
                    }
                }
                cursor.close();
            }
            for (int i = SimInfo.DEFAULT_NAME_MIN_INDEX; i <= SimInfo.DEFAULT_NAME_MAX_INDEX; i++) {
                if (array.contains((long)i)) {
                    continue;
                } else {
                    index = i;
                    break;
                }
            }
            return index;
        }
    }
    
    
    public static class CdmaCallOptions implements BaseColumns {
        /**
         * The content:// style URL for this table
         */
        public static Uri CONTENT_URI =
            Uri.parse("content://cdma/calloption");

        /**
         * The default sort order for this table
         */
        public static String DEFAULT_SORT_ORDER = "name ASC";

        public static String NAME = "name";

        public static String MCC = "mcc";

        public static String MNC = "mnc";

        public static String NUMERIC = "numeric";

        public static String NUMBER = "number";

        public static String TYPE = "type";

        public static String CATEGORY = "category";

        public static String STATE = "state";

    }
    
    /**
     * record the avoid CDMA current system network log
     *
     */
    public static class AvoidNetWork implements BaseColumns {
        /**
         * The content:// style URL for this table
         */
        public static Uri CONTENT_URI =
                Uri.parse("content://cdma/avoid_net");

        /**
         * The default sort order for this table
         */
        public static String MCC = "mcc";

        public static String MNC = "mnc";

    }
}

    
    
    




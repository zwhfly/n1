package gionee.drm;

public class GnDrmStore {

    /**
     * defines the drm extra key & value.
     * @hide
     */
    public static class DrmExtra {
        public static final String EXTRA_DRM_LEVEL =
                "android.intent.extra.drm_level";
        public static final int DRM_LEVEL_FL = 1;
        public static final int DRM_LEVEL_SD = 2;
        public static final int DRM_LEVEL_ALL = 4;
    }
}

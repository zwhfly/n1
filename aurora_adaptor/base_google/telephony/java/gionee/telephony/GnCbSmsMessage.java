package gionee.telephony;

//import com.android.internal.telephony.gsm.GsmCbSmsMessage;
import com.android.internal.telephony.gsm.GsmSmsCbMessage;

public class GnCbSmsMessage {

    public GsmSmsCbMessage mWrappedMessage;
    
    /**
     * @return the text of message
     */
    public String getMessageBody()
    {
        return null;
//        return mWrappedMessage.getMessageBody();
    }
    
    /**
     * @return SIM ID, but now it doesn't return a valid value
     */
    public int getMessageSimId() {
        return -1;
    }
    
    /**
     * @return message id
     */
    public int getMessageID() {
//        return mWrappedMessage.getMessageID();
        return -1;
    }
}

package com.gionee.internal.telephony;

import android.content.Context;
import android.os.ServiceManager;
import android.os.IBinder;
import java.util.List;
import com.android.internal.telephony.IIccPhoneBook;
import com.android.internal.telephony.UsimGroup;

public class GnIIccPhoneBook {
    
    public static boolean removeUSIMGroupById(IIccPhoneBook icc, int groupId) {
        return false;
    }
    
    public static List<UsimGroup> getUsimGroups(IIccPhoneBook icc) {
        return null;
    }
    
    public static int insertUSIMGroup(IIccPhoneBook icc, String grpName) {
        return -1;
    }
    
    public static int updateUSIMGroup(IIccPhoneBook icc, int nGasId, String grpName) {
        return -1;
    }
    
    public static boolean addContactToGroup(IIccPhoneBook icc, int adnIndex, int grpIndex) {
        return false;
    }
    
    public static boolean removeContactFromGroup(IIccPhoneBook icc, int adnIndex, int grpIndex) {
        return false;
    }
    
    public static int getUSIMGrpMaxNameLen(IIccPhoneBook icc) {
        return -1;
    }
    
    public static int getUSIMGrpMaxCount(IIccPhoneBook icc) {
        return -1;
    }
}


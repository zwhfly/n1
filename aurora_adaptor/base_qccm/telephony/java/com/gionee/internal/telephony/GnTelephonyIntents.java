package com.gionee.internal.telephony;

public class GnTelephonyIntents {


    /**
     * Broadcast Action: New SIM detected.
     * The intent will have the following extra values:</p>
     * <ul>
     *	 <li><em>SIMCount</em> - available SIM count.	"1" for one SIM, "2" for two SIMs</li>
     * </ul>  
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     * 
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
   public static final String ACTION_NEW_SIM_DETECTED
		= "android.intent.action.NEW_SIM_DETECTED";
	
    /**
     * Broadcast Action: default SIM removed.
     * The intent will have the following extra values:</p>
     * <ul>
     *	 <li><em>SIMCount</em> - available SIM count.	"1" for one SIM, "2" for two SIMs</li>
     * </ul>  
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     * 
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
   public static final String ACTION_DEFAULT_SIM_REMOVED
		= "android.intent.action.DEFAULT_SIM_REMOVED";
    /**
      * Broadcast Action: sim indicator state changed.
      * The intent will have the following extra values:</p>
      * <ul>
      *   <li><em>slotId</em> - specify the slot in which the SIM indicator state changed.
      *    int : 0 for slot1, 1 for slot 2</li>
      * <li><em>state</em> - the new state   
      * </ul>  
      *
      * <p class="note">
      * Requires the READ_PHONE_STATE permission.
      * 
      * <p class="note">This is a protected intent that can only be sent
      * by the system.
     */
    public static final String ACTION_SIM_INDICATOR_STATE_CHANGED
    	= "android.intent.action.SIM_INDICATOR_STATE_CHANGED";
    
    /**
     * Broadcast Action: sim slot id has been updated into Sim Info database.
     * The intent will have the following extra values:</p>
     * <ul>
     *   <li><em>slotId</em> - specify the slot in which the SIM indicator state changed.
     *    int : 0 for slot1, 1 for slot 2</li>
     * <li><em>state</em> - the new state   
     * </ul>  
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     * 
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
    public static final String ACTION_SIM_INFO_UPDATE
    	= "android.intent.action.SIM_INFO_UPDATE";

    /**
      * Broadcast Action: Radio off from normal state.
      * The intent will have the following extra values:</p>
      * <ul>
      *   <li><em>slotId</em> - specify the slot in which the SIM indicator state changed.
      *    int : 0 for slot1, 1 for slot 2</li>
      * </ul>  
      *
      * <p class="note">
      * Requires the READ_PHONE_STATE permission.
      * 
      * <p class="note">This is a protected intent that can only be sent
      * by the system.
     */
    public static final String ACTION_RADIO_OFF
         = "android.intent.action.RADIO_OFF";

    public static String INTENT_KEY_ICC_SLOT = "slotId";
    public static String INTENT_KEY_ICC_STATE = "state";

    public static final String ACTION_SIM_INSERTED_STATUS
        = "android.intent.action.SIM_INSERTED_STATUS";

    public static final String ACTION_SIM_NAME_UPDATE
        = "android.intent.action.SIM_NAME_UPDATE";

}

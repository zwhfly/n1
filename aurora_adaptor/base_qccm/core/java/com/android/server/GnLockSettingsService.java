package com.android.server;
import java.lang.reflect.Method;
import android.os.Build; 
import com.android.internal.widget.ILockSettings;
public class GnLockSettingsService{


  public static void setLockPattern(ILockSettings iservice,byte[] hash,String pattern, int userId)
  {
        if(Build.VERSION.SDK_INT<19)
        {
          try{

              Class<?> spp=Class.forName("com.android.internal.widget.ILockSettings");
              Method method=spp.getMethod("setLockPattern",byte[].class,int.class);
              method.invoke(iservice,hash,userId);
          }catch(Exception e){}

       }
       else
       {
          try{

              Class<?> spp=Class.forName("com.android.internal.widget.ILockSettings");
              Method method=spp.getMethod("setLockPattern",String.class,int.class);
              method.invoke(iservice,pattern,userId);
          }catch(Exception e){}

       }



  }

  public static boolean checkPattern(ILockSettings iservice,byte[] hash,String pattern, int userId)
  {
        if(Build.VERSION.SDK_INT<19)
        {
          try{

              Class<?> spp=Class.forName("com.android.internal.widget.ILockSettings");
              Method method=spp.getMethod("checkPattern",byte[].class,int.class);
              return ((Boolean)method.invoke(iservice,hash,userId)).booleanValue();
          }catch(Exception e){}

       }
       else
       {
          try{

              Class<?> spp=Class.forName("com.android.internal.widget.ILockSettings");
              Method method=spp.getMethod("checkPattern",String.class,int.class);
              return ((Boolean)method.invoke(iservice,pattern,userId)).booleanValue();
          }catch(Exception e){}

       }

       return true;

  }


}

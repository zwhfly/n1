package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
/**
*add this class for Quclocomm compile pass.
*/
public class SmsMemoryStatus
  implements Parcelable
{
  public int mUsed;
  public int mTotal;
  public static final Parcelable.Creator<SmsMemoryStatus> CREATOR = new Parcelable.Creator()
  {
    public SmsMemoryStatus createFromParcel(Parcel source)
    {
      int used = source.readInt();
      int total = source.readInt();
      return new SmsMemoryStatus(used, total);
    }

    public SmsMemoryStatus[] newArray(int size) {
      return new SmsMemoryStatus[size];
    }
  };

  public SmsMemoryStatus()
  {
    this.mUsed = 0;
    this.mTotal = 0;
  }

  public SmsMemoryStatus(int used, int total) {
    this.mUsed = used;
    this.mTotal = total;
  }

  public int getUsed() {
    return this.mUsed;
  }

  public int getTotal() {
    return this.mTotal;
  }

  public int getUnused() {
    return this.mTotal - this.mUsed;
  }

  public void reset() {
    this.mUsed = 0;
    this.mTotal = 0;
  }

  public int describeContents() {
    return 0;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.mUsed);
    dest.writeInt(this.mTotal);
  }
}

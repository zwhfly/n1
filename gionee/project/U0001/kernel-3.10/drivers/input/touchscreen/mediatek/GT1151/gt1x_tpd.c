/* drivers/input/touchscreen/gt1x_tpd.c
 *
 * 2010 - 2014 Goodix Technology.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be a reference
 * to you, when you are integrating the GOODiX's CTP IC into your system,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * Version: 1.0   
 * Revision Record: 
 *      V1.0:  first release. 2014/09/28.
 *
 */

#include "gt1x_tpd_custom.h"
#include "gt1x_generic.h"

#if TPD_SUPPORT_I2C_DMA
#include <linux/dma-mapping.h>
#endif

#if GTP_ICS_SLOT_REPORT
#include <linux/input/mt.h>
#endif

extern struct tpd_device *tpd;

static int tpd_flag = 0;
int tpd_halt = 0;
static int tpd_eint_mode = 1;
static struct task_struct *thread = NULL;
static int tpd_polling_time = 50;
extern u8 is_reseting ;    //patch
static DECLARE_WAIT_QUEUE_HEAD(waiter);
DEFINE_MUTEX(i2c_access);

//Gionee BSP1 yaoyc 20150608 add for CR01478730 begin
#if TPD_IGNORE_LARGETOUCH_BY_DRIVER
#define GTP_REG_FRAME_NUM 0x8268 
static u8 largetouch_flag=0;  
#endif
//Gionee BSP1 yaoyc 20150608 add for CR01478730 end


#if TPD_HAVE_BUTTON
static int tpd_keys_local[TPD_KEY_COUNT] = TPD_KEYS;
static int tpd_keys_dim_local[TPD_KEY_COUNT][4] = TPD_KEYS_DIM;
#endif

#if (defined(TPD_WARP_START) && defined(TPD_WARP_END))
static int tpd_wb_start_local[TPD_WARP_CNT] = TPD_WARP_START;
static int tpd_wb_end_local[TPD_WARP_CNT] = TPD_WARP_END;
#endif

#if (defined(TPD_HAVE_CALIBRATION) && !defined(TPD_CUSTOM_CALIBRATION))
static int tpd_def_calmat_local[8] = TPD_CALIBRATION_MATRIX;
#endif

static void tpd_eint_interrupt_handler(void);
static int tpd_event_handler(void *unused);
static int tpd_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int tpd_i2c_detect(struct i2c_client *client, struct i2c_board_info *info);
static int tpd_i2c_remove(struct i2c_client *client);

#ifndef MT6572
extern void mt65xx_eint_set_hw_debounce(kal_uint8 eintno, kal_uint32 ms);
extern kal_uint32 mt65xx_eint_set_sens(kal_uint8 eintno, kal_bool sens);
extern void mt65xx_eint_registration(kal_uint8 eintno, kal_bool Dbounce_En, kal_bool ACT_Polarity, void (EINT_FUNC_PTR) (void), kal_bool auto_umask);
#endif

#define GTP_DRIVER_NAME  "gt1x"
static const struct i2c_device_id tpd_i2c_id[] = { {GTP_DRIVER_NAME, 0}, {} };
static unsigned short force[] = { 0, GTP_I2C_ADDRESS, I2C_CLIENT_END, I2C_CLIENT_END };
static const unsigned short *const forces[] = { force, NULL };

//static struct i2c_client_address_data addr_data = { .forces = forces,};
static struct i2c_board_info __initdata i2c_tpd = { I2C_BOARD_INFO(GTP_DRIVER_NAME, (GTP_I2C_ADDRESS >> 1)) };

static struct i2c_driver tpd_i2c_driver = {
	.probe = tpd_i2c_probe,
	.remove = tpd_i2c_remove,
	.detect = tpd_i2c_detect,
	.driver.name = GTP_DRIVER_NAME,
	.id_table = tpd_i2c_id,
	.address_list = (const unsigned short *)forces,
};

//#define TPD_SUSPEND_HOTKNOT
#ifdef TPD_SUSPEND_HOTKNOT
struct mutex tp_suspend_mutex;
#endif

#if TPD_SUPPORT_I2C_DMA
static u8 *gpDMABuf_va = NULL;
static dma_addr_t gpDMABuf_pa = 0;
struct mutex dma_mutex;

static s32 i2c_dma_write_mtk(u16 addr, u8 * buffer, s32 len)
{
	s32 ret = 0;
	s32 pos = 0;
	s32 transfer_length;
	u16 address = addr;
	struct i2c_msg msg = {
		.flags = !I2C_M_RD,
		.ext_flag = (gt1x_i2c_client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG),
		.addr = (gt1x_i2c_client->addr & I2C_MASK_FLAG),
		.timing = I2C_MASTER_CLOCK,
		.buf = (u8 *) gpDMABuf_pa,
	};

	mutex_lock(&dma_mutex);
	while (pos != len) {
		if (len - pos > (IIC_DMA_MAX_TRANSFER_SIZE - GTP_ADDR_LENGTH)) {
			transfer_length = IIC_DMA_MAX_TRANSFER_SIZE - GTP_ADDR_LENGTH;
		} else {
			transfer_length = len - pos;
		}

		gpDMABuf_va[0] = (address >> 8) & 0xFF;
		gpDMABuf_va[1] = address & 0xFF;
		memcpy(&gpDMABuf_va[GTP_ADDR_LENGTH], &buffer[pos], transfer_length);

		msg.len = transfer_length + GTP_ADDR_LENGTH;

		ret = i2c_transfer(gt1x_i2c_client->adapter, &msg, 1);
		if (ret != 1) {
			GTP_INFO("I2c Transfer error! (%d)", ret);
			ret = ERROR_IIC;
			break;
		}
		ret = 0;
		pos += transfer_length;
		address += transfer_length;
	}
	mutex_unlock(&dma_mutex);
	return ret;
}

static s32 i2c_dma_read_mtk(u16 addr, u8 * buffer, s32 len)
{
	s32 ret = ERROR;
	s32 pos = 0;
	s32 transfer_length;
	u16 address = addr;
	u8 addr_buf[GTP_ADDR_LENGTH] = { 0 };
	struct i2c_msg msgs[2] = {
		{
		 .flags = 0,	//!I2C_M_RD,
		 .addr = (gt1x_i2c_client->addr & I2C_MASK_FLAG),
		 .timing = I2C_MASTER_CLOCK,
		 .len = GTP_ADDR_LENGTH,
		 .buf = addr_buf,
		 },
		{
		 .flags = I2C_M_RD,
		 .ext_flag = (gt1x_i2c_client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG),
		 .addr = (gt1x_i2c_client->addr & I2C_MASK_FLAG),
		 .timing = I2C_MASTER_CLOCK,
		 .buf = (u8 *) gpDMABuf_pa,
		 },
	};
	mutex_lock(&dma_mutex);
	while (pos != len) {
		if (len - pos > IIC_DMA_MAX_TRANSFER_SIZE) {
			transfer_length = IIC_DMA_MAX_TRANSFER_SIZE;
		} else {
			transfer_length = len - pos;
		}

		msgs[0].buf[0] = (address >> 8) & 0xFF;
		msgs[0].buf[1] = address & 0xFF;
		msgs[1].len = transfer_length;

		ret = i2c_transfer(gt1x_i2c_client->adapter, msgs, 2);
		if (ret != 2) {
			GTP_ERROR("I2C Transfer error! (%d)", ret);
			ret = ERROR_IIC;
			break;
		}
		ret = 0;
		memcpy(&buffer[pos], gpDMABuf_va, transfer_length);
		pos += transfer_length;
		address += transfer_length;
	};
	mutex_unlock(&dma_mutex);
	return ret;
}

#else

static s32 i2c_write_mtk(u16 addr, u8 * buffer, s32 len)
{
	s32 ret;

	struct i2c_msg msg = {
		.flags = 0,
		.addr = (gt1x_i2c_client->addr & I2C_MASK_FLAG) | (I2C_ENEXT_FLAG),	//remain
		.timing = I2C_MASTER_CLOCK,
	};

	ret = _do_i2c_write(&msg, addr, buffer, len);
	return ret;
}

static s32 i2c_read_mtk(u16 addr, u8 * buffer, s32 len)
{
	int ret;
	u8 addr_buf[GTP_ADDR_LENGTH] = { (addr >> 8) & 0xFF, addr & 0xFF };

	struct i2c_msg msgs[2] = {
		{
		 .addr = ((gt1x_i2c_client->addr & I2C_MASK_FLAG) | (I2C_ENEXT_FLAG)),
		 .flags = 0,
		 .buf = addr_buf,
		 .len = GTP_ADDR_LENGTH,
		 .timing = I2C_MASTER_CLOCK},
		{
		 .addr = ((gt1x_i2c_client->addr & I2C_MASK_FLAG) | (I2C_ENEXT_FLAG)),
		 .flags = I2C_M_RD,
		 .timing = I2C_MASTER_CLOCK},
	};

	ret = _do_i2c_read(msgs, addr, buffer, len);
	return ret;
}
#endif /* TPD_SUPPORT_I2C_DMA */

/**
 * @return: return 0 if success, otherwise return a negative number
 *          which contains the error code.
 */
s32 gt1x_i2c_read(u16 addr, u8 * buffer, s32 len)
{
#if TPD_SUPPORT_I2C_DMA
	return i2c_dma_read_mtk(addr, buffer, len);
#else
	return i2c_read_mtk(addr, buffer, len);
#endif
}

/**
 * @return: return 0 if success, otherwise return a negative number
 *          which contains the error code.
 */
s32 gt1x_i2c_write(u16 addr, u8 * buffer, s32 len)
{
#if TPD_SUPPORT_I2C_DMA
	return i2c_dma_write_mtk(addr, buffer, len);
#else
	return i2c_write_mtk(addr, buffer, len);
#endif
}

#ifdef TPD_REFRESH_RATE
/*******************************************************
Function:
    Write refresh rate

Input:
    rate: refresh rate N (Duration=5+N ms, N=0~15)

Output:
    Executive outcomes.0---succeed.
*******************************************************/
static u8 gt1x_set_refresh_rate(u8 rate)
{
	u8 buf[1] = { rate };

	if (rate > 0xf) {
		GTP_ERROR("Refresh rate is over range (%d)", rate);
		return ERROR_VALUE;
	}

	GTP_INFO("Refresh rate change to %d", rate);
	return gt1x_i2c_write(GTP_REG_REFRESH_RATE, buf, sizeof(buf));
}

/*******************************************************
Function:
    Get refresh rate

Output:
    Refresh rate or error code
*******************************************************/
static u8 gt1x_get_refresh_rate(void)
{
	int ret;
	u8 buf[1] = { 0x00 };
	ret = gt1x_i2c_read(GTP_REG_REFRESH_RATE, buf, sizeof(buf));
	if (ret < 0)
		return ret;

	GTP_INFO("Refresh rate is %d", buf[0]);
	return buf[0];
}

//=============================================================
static ssize_t show_refresh_rate(struct device *dev, struct device_attribute *attr, char *buf)
{
	int ret = gt1x_get_refresh_rate();
	if (ret < 0)
		return 0;
	else
		return sprintf(buf, "%d\n", ret);
}

static ssize_t store_refresh_rate(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
	//u32 rate = 0;
	gt1x_set_refresh_rate(simple_strtoul(buf, NULL, 16));
	return size;
}

static DEVICE_ATTR(tpd_refresh_rate, 0664, show_refresh_rate, store_refresh_rate);

static struct device_attribute *gt9xx_attrs[] = {
	&dev_attr_tpd_refresh_rate,
};
#endif
//=============================================================

static int tpd_i2c_detect(struct i2c_client *client, struct i2c_board_info *info)
{
	strcpy(info->type, "mtk-tpd");
	return 0;
}

static int tpd_power_on(void)
{
	gt1x_power_switch(SWITCH_ON);

	gt1x_select_addr();
	msleep(10);

	if (gt1x_get_chip_type() != 0) {
		return -1;
	}

	if (gt1x_reset_guitar() != 0) {
		return -1;
	}

	return 0;
}

void gt1x_irq_enable(void)
{
	mt_eint_unmask(CUST_EINT_TOUCH_PANEL_NUM);
}

void gt1x_irq_disable(void)
{
	mt_eint_mask(CUST_EINT_TOUCH_PANEL_NUM);
}

void gt1x_power_switch(s32 state)
{
	GTP_GPIO_OUTPUT(GTP_RST_PORT, 0);
	GTP_GPIO_OUTPUT(GTP_INT_PORT, 0);
	msleep(10);
    printk("Power switch on! state = %d\n",state);
	switch (state) {
	case SWITCH_ON:
		printk("Power switch on!");
		//hwPowerOn(MT6328_POWER_LDO_VCAM_IO, VOL_1800, "synaptics_tp");
		//msleep(100);

		mt_set_gpio_mode(GPIO_CTP_EN_PIN, GPIO_MODE_GPIO);
                //mt_set_gpio_pull_enable(GPIO_CTP_EN_PIN, 0);
		mt_set_gpio_dir(GPIO_CTP_EN_PIN, GPIO_DIR_OUT);
		mt_set_gpio_out(GPIO_CTP_EN_PIN, GPIO_OUT_ONE);
		

		break;
	case SWITCH_OFF:
		GTP_DEBUG("Power switch off!");
		mt_set_gpio_mode(GPIO_CTP_EN_PIN, GPIO_CTP_EN_PIN_M_GPIO);
		mt_set_gpio_dir(GPIO_CTP_EN_PIN, GPIO_DIR_OUT);
		mt_set_gpio_out(GPIO_CTP_EN_PIN, GPIO_OUT_ZERO);

		break;
	default:
		GTP_ERROR("Invalid power switch command!");
		break;
	}
}

#if GTP_FACTORY_CHECK
u16 node_value[SENSOR_A_PORT_SIZE];
static int goodix_factory_check(void)
{
	int ret = 0;
	char temp_data[sizeof(node_value)];
	u16 *p = &node_value[0];
	//int driver_num = 0, sensor_num = 0;
	int i, j;
	//int size = (sizeof(node_value))/2;
	int size = SENSOR_A_PORT_SIZE;
	int retry = 20;
	unsigned char status = 0;
	int rawdata_size = SENSOR_A_PORT_SIZE*2;
	
	ret = gt1x_send_cfg(gt1x_config_factory_check, gt1x_cfg_length);
	if(ret < 0) {
		printk("%s: CTP  i2c_read_bytes error!gt1x_config_factory_check[0] = %d\n", __func__,gt1x_config_factory_check[0]);
		goto err;
	}
	mdelay(300);
	ret = gt1x_send_cmd(1, 0);
	mdelay(20);
	gt1x_rawdiff_mode = 1;
	gt1x_i2c_write(GTP_READ_COOR_ADDR,&status,1);
	
	while(retry > 0){
		gt1x_i2c_read(GTP_READ_COOR_ADDR,&status,1);
		printk("%s, CTP status = 0x%x\n",__func__, status);
		if(status & 0x80){
			break;
		}
		msleep(100);
		retry--;
	}
	printk("%s, CTP sizeof(node) = %d\n",__func__,rawdata_size );
	ret = gt1x_i2c_read(0xb798, temp_data, rawdata_size);
	if(ret < 0) {
		GTP_DEBUG("%s: CTP i2c_read_bytes error! ", __func__);
		goto err;
	}

	for (i = 0, j = 0; i < rawdata_size; i += 2, j++) {
		p[j] = temp_data[i] << 8 | temp_data[i+1];
    }
	// yaoyc - check AA arae raw data 
	for (i = 0; i < SENSOR_NUM*DRIVER_NUM; i++) {
		if(p[i] < CTP_NODE_MIN_VALUE) {
			printk("%s: p[%d]: %d, CTP_NODE_MIN_VALUE: %d == > p[%d] < CTP_NODE_MIN_VALUE", __func__, i, p[i], CTP_NODE_MIN_VALUE, i);
			goto err;
		}
		if(p[i] > CTP_NODE_MAX_VALUE) {
			printk("%s: p[%d]: %d, CTP_NODE_MAX_VALUE: %d == > p[%d] > CTP_NODE_MAX_VALUE", __func__, i, p[i], CTP_NODE_MIN_VALUE, i);
			goto err;
		}
	}

	//yaoyc - check touch key raw data 
	for(i = SENSOR_NUM*DRIVER_NUM; i < size; i ++ ) {
		if(p[i] < CTP_TOUCH_KEY_MIN_VALUE){
			printk("%s:p[%d]: %d, CTP_TOUCH_KEY_MIN_VALUE: %d == > p[%d] < CTP_TOUCH_KEY_MIN_VALUE ",
					__func__, i, p[i], CTP_TOUCH_KEY_MIN_VALUE, i);
			goto err;
		}
		if(p[i] > CTP_TOUCH_KEY_MAX_VALUE) {
			printk("%s:p[%d]: %d, CTP_TOUCH_KEY_MAX_VALUE: %d == > p[%d] < CTP_TOUCH_KEY_MIN_VALUE ",
					__func__, i, p[i], CTP_TOUCH_KEY_MAX_VALUE, i);
			goto err;
		}
	}

	ret = gt1x_send_cfg(gt1x_config, gt1x_cfg_length);
	if(ret < 0) {
		printk("%s: CTP  i2c_read_bytes error! ", __func__);
		goto err;
	}
	gt1x_rawdiff_mode = 0;
	return 0;

err:

	ret = gt1x_send_cfg(gt1x_config, gt1x_cfg_length);
	if(ret < 0) {
		GTP_ERROR("%s:CTP  i2c_read_bytes error! ", __func__);
		//goto err;
	}
	gt1x_rawdiff_mode = 0;

	return -1;
}

static ssize_t show_factory_check(struct device *dev, struct device_attribute *attr, char *buf)
{
	int ret; 
	

	ret = goodix_factory_check();
	if(ret < 0) {
		ret = 0;
		return sprintf(buf, "%d\n", ret);
	}
	else {
		ret = 1;
		return sprintf(buf, "%d\n", ret);
	}
}

static ssize_t show_factory_check_value(struct device *dev, struct device_attribute *attr, char *buf)
{
	int i;
	char *ptr = buf;

	u16 *p = &node_value[0];

    	int size = SENSOR_A_PORT_SIZE;

	for (i = 0; i < size; i++) {
		ptr += sprintf(ptr, "%d ,", p[i]);
		if( (i % (SENSOR_NUM)) == (SENSOR_NUM - 1))
			ptr += sprintf(ptr, "\\\n");
		
	}
	/*
	for (i = size+SENSOR_NUM-3; i<size+SENSOR_NUM;i++)
		ptr += sprintf(ptr, "%d ,", p[i]);
	*/	
	ptr += sprintf(ptr, "\n");

	return (ptr - buf);	
}

static DEVICE_ATTR(factory_check, (S_IWUSR|S_IRUGO), show_factory_check, NULL);
static DEVICE_ATTR(factory_check_value, (S_IWUSR|S_IRUGO), show_factory_check_value, NULL);
#endif

#if GTP_SMART_COVER
static int gt1x_smart_cover_update_state(unsigned char enable);
#endif

static unsigned int hall_key_state = 1;
static unsigned char glove_switch = 0;
void gt1x_hall_key_callback(int state)
{
	printk("%s, state = %d, glove_switch = %d\n",__func__, state,glove_switch);
	
#if GTP_SMART_COVER
	hall_key_state = state;

	if(state == 1) { //hall key open
		if(glove_switch) {
			//mdelay(5);
			gt1x_smart_cover_update_state(true);
		}
	}else if(state == 0){  //hall key close
		if(glove_switch) {
			gt1x_smart_cover_update_state(false);
		}
	}
#endif

	return;
}

EXPORT_SYMBOL(gt1x_hall_key_callback);

#if GTP_SMART_COVER
struct smart_cover_device *gt1x_sc_dev;

static int gt1x_smart_cover_update_state(unsigned char enable)
{

	int ret = 0;
	struct smart_cover_device *device = gt1x_sc_dev;

	GTP_DEBUG(" CTP gt1x_smart_cover_update_state enter\n");
	
	if (!device) {
		return -ENODEV;
	}
        printk("%s, enable = %d\n",__func__,enable);
        GTP_DEBUG(" CTP gt1x glove state device->suspended = %d,enable = %d\n",device->suspended, enable);
	if(!device->suspended) {
		if(enable) {  /* near by*/
			ret = gt1x_send_cfg(gt1x_config_smart_factory, gt1x_cfg_length);
		} else {
	#if GTP_CHARGER_SWITCH
			gt1x_charger_config(1);
	#else
			ret = gt1x_send_cfg(gt1x_config, gt1x_cfg_length);
	#endif
		}
		
	} else {
		GTP_DEBUG("TP is suspended, do nothing.");
	}
	return ret;
}

static ssize_t smart_cover_show(struct device *dev, 
					struct device_attribute *attr, char *buf)
{
	struct smart_cover_device *device = gt1x_sc_dev;

	if (!device) {
		return -ENODEV;
	}
	
	return sprintf(buf, "%d\n", glove_switch);
}

static ssize_t smart_cover_store(struct device *dev,
				struct device_attribute *attr, const char *buf, size_t count)
{
	struct smart_cover_device *device = gt1x_sc_dev;
	int rt;	
	unsigned long val;	
	
	GTP_DEBUG("CTP smart_cover_store enter\n");	
	
	if (!device) {	
		return -ENODEV;
	}

	if (!device->enabled) {	
		return -1;
	} 
	
	rt = strict_strtoul(buf, 10, &val);	
	if(rt != 0){			
		return rt;	
	}
	
	glove_switch = (val > 0)?0x01:0x00;

	if( (0x00 == glove_switch) || (hall_key_state == 0)) {
		gt1x_smart_cover_update_state(false);
	} else if((0x01 == glove_switch) && (hall_key_state == 1)) {
		gt1x_smart_cover_update_state(true);
	} else {
		GTP_DEBUG("Invalid argument!");
	}
	return count;
}

static DEVICE_ATTR(glove_enable, 0664, smart_cover_show, smart_cover_store);

int gt1x_smart_cover_init(void) 
{

	int ret = 0;
	
	if (!gt1x_sc_dev) {
		gt1x_sc_dev = kzalloc(sizeof(struct smart_cover_device), GFP_KERNEL);
		if (!gt1x_sc_dev) {
			return -ENOMEM;
		}
	}

	gt1x_sc_dev->enabled = 1;   

	return ret;
}

void gt1x_smart_cover_deinit(void)
{
    if (!gt1x_sc_dev) {
        return;
    }

    kfree(gt1x_sc_dev);
}
#endif

#if DOUBLE_CLICK_WAKE
int wake_switch = 0;
static ssize_t tp_wake_switch_show(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	return sprintf(buf, "%d\n", wake_switch);
}

static ssize_t tp_wake_switch_write(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t size)
{
	int rt;
	unsigned long val;
       unsigned char test_wake_flag = 0, device_ctrl = 0;
       
	rt = strict_strtoul(buf, 10, &val); 
	if(rt != 0){
		pr_err("%s, invalid value\n", __func__);
		return rt;
	}
	wake_switch = val;
	printk("%s, %d\n", __func__, wake_switch);

	return size;
}

static DEVICE_ATTR(double_wake, 0664, tp_wake_switch_show, tp_wake_switch_write);
#endif
//Gionee BSP1 yaoyc 20150616 add for iuni gesture function begin
#if GN_IUNI_GESTURE_TYPE
int gesture_c_switch = 0;
static ssize_t tp_gesture_c_switch_show(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	return sprintf(buf, "%d\n", gesture_c_switch);
}

static ssize_t tp_gesture_c_switch_write(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t size)
{
	int rt;
	unsigned long val;
       
	rt = strict_strtoul(buf, 10, &val); 
	if(rt != 0){
		pr_err("%s, invalid value\n", __func__);
		return rt;
	}
	gesture_c_switch = val;
	printk("%s, %d\n", __func__, gesture_c_switch);
	if(gesture_c_switch)
		gesture_enabled = 1;
	else{
		gesture_enabled = gesture_for_music_switch?1:0;
	}
	return size;
}
static DEVICE_ATTR(gesture_c, 0664, tp_gesture_c_switch_show, tp_gesture_c_switch_write);


int gesture_for_music_switch = 0;
static ssize_t tp_gesture_for_music_switch_show(struct device *dev,
		struct device_attribute *attr,
		char *buf)
{
	return sprintf(buf, "%d\n", gesture_for_music_switch);
}

static ssize_t tp_gesture_for_music_switch_write(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t size)
{
	int rt;
	unsigned long val;
       
	rt = strict_strtoul(buf, 10, &val); 
	if(rt != 0){
		pr_err("%s, invalid value\n", __func__);
		return rt;
	}
	gesture_for_music_switch = val;
	printk("%s, %d\n", __func__, gesture_for_music_switch);
	if(gesture_for_music_switch)
		gesture_enabled = 1;
	else{
		gesture_enabled = gesture_c_switch?1:0;
	}

	return size;
}
static DEVICE_ATTR(gesture_for_music, 0664, tp_gesture_for_music_switch_show, tp_gesture_for_music_switch_write);

#endif
//Gionee BSP1 yaoyc 20150616 add for iuni gesture function end
static s32 tpd_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	s32 err = 0;

#if GTP_HAVE_TOUCH_KEY
	s32 idx = 0;
#endif
	GTP_ERROR("tpd_i2c_probe enter!");

	gt1x_i2c_client = client;

	if (gt1x_init()) {
		/* TP resolution == LCD resolution, no need to match resolution when initialized fail */
		gt1x_abs_x_max = 0;
		gt1x_abs_y_max = 0;
		//Gionee yaoyc 20150408 add for compat tp begin
		//return -1;
		//Gionee yaoyc 20150408 add for compat tp end
	}

	thread = kthread_run(tpd_event_handler, 0, TPD_DEVICE);
	if (IS_ERR(thread)) {
		err = PTR_ERR(thread);
		GTP_INFO(TPD_DEVICE " failed to create kernel thread: %d\n", err);
	}
#if GTP_HAVE_TOUCH_KEY
	for (idx = 0; idx < GTP_MAX_KEY_NUM; idx++) {
		input_set_capability(tpd->dev, EV_KEY, gt1x_touch_key_array[idx]);
	}
#endif

#if GTP_GESTURE_WAKEUP
	input_set_capability(tpd->dev, EV_KEY, KEY_GESTURE);
#endif

#if DOUBLE_CLICK_WAKE
	input_set_capability(tpd->dev, EV_KEY, KEY_DOUBLE_WAKE);
#endif
//Gionee BSP1 yaoyc 20150518 add for support B protocol begin
#if GTP_ICS_SLOT_REPORT
	input_mt_init_slots(tpd->dev,GTP_MAX_TOUCH, 0);
#endif
//Gionee BSP1 yaoyc 20150518 add for support B protocol end

#if GTP_FACTORY_CHECK
	err = platform_device_register(&gn_tp_wake_device);
	if (err) {
		printk("tpd: create gn_tp_wake_device failed\n");
	}

	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_factory_check);
	if(err) {
		printk("tpd: wake_tp_create_attr factory_check failed.\n");
	}

	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_factory_check_value);
	if(err) {
		printk("tpd: wake_tp_create_attr factory_check value failed.\n");
	}
	
#endif

#if GTP_SMART_COVER
	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_glove_enable);
	if (err) 
	{
		printk("tpd: wake_tp_create_attr glove_enable failed.\n");
	}
#endif

#if DOUBLE_CLICK_WAKE
	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_double_wake);
	if(err) {
		printk("tpd: wake_tp_create_attr double_wake failed.\n");
	}
#endif
//Gionee BSP1 yaoyc 20150616 add for iuni gesture function begin
#if GN_IUNI_GESTURE_TYPE
	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_gesture_c);
	if(err) {
		printk("tpd: wake_tp_create_attr double_wake failed.\n");
	}
	err = device_create_file(&(gn_tp_wake_device.dev), &dev_attr_gesture_for_music);
	if(err) {
		printk("tpd: wake_tp_create_attr double_wake failed.\n");
	}

	input_set_capability(tpd->dev, EV_KEY, KEY_GESTURE_C);
	input_set_capability(tpd->dev, EV_KEY, KEY_GESTURE_O);
	input_set_capability(tpd->dev, EV_KEY, KEY_GESTURE_LEFT);
	input_set_capability(tpd->dev, EV_KEY, KEY_GESTURE_RIGHT);
#endif
//Gionee BSP1 yaoyc 20150616 add for iuni gesture function end

	GTP_GPIO_AS_INT(GTP_INT_PORT);

	msleep(50);


	mt_eint_set_hw_debounce(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_DEBOUNCE_CN);
	mt_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, EINTF_TRIGGER_FALLING, tpd_eint_interrupt_handler, 1); 
//	mt_eint_mask(CUST_EINT_TOUCH_PANEL_NUM);

/* interrupt registration */
#if 0
#ifndef MT6589
	if (!gt1x_int_type) {	/*EINTF_TRIGGER */
		mt_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, EINTF_TRIGGER_RISING, tpd_eint_interrupt_handler, 1);
	} else {
		mt_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, EINTF_TRIGGER_FALLING, tpd_eint_interrupt_handler, 1);
	}

#else
	mt65xx_eint_set_sens(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_SENSITIVE);
	mt65xx_eint_set_hw_debounce(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_DEBOUNCE_CN);

	if (!gt1x_int_type) {
		mt65xx_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_DEBOUNCE_EN, CUST_EINT_POLARITY_HIGH, tpd_eint_interrupt_handler, 1);
	} else {
		mt65xx_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_DEBOUNCE_EN, CUST_EINT_POLARITY_LOW, tpd_eint_interrupt_handler, 1);
	}
#endif
#endif
	gt1x_irq_enable();

#if GTP_ESD_PROTECT
	/*  must before auto update */
	gt1x_init_esd_protect();
	gt1x_esd_switch(SWITCH_ON);
#endif

#if GTP_AUTO_UPDATE

	thread = kthread_run(gt1x_auto_update_proc, (void *)NULL, "gt1x_auto_update");
	if (IS_ERR(thread)) {
		err = PTR_ERR(thread);
		GTP_INFO(TPD_DEVICE " ctp failed to create auto-update thread: %d\n", err);
	}
#endif

	tpd_load_status = 1;

	return 0;
}

static void tpd_eint_interrupt_handler(void)
{
	TPD_DEBUG_PRINT_INT;

	tpd_flag = 1;

	wake_up_interruptible(&waiter);
}

void gt1x_touch_down(s32 x, s32 y, s32 size, s32 id)
{
#if GTP_CHANGE_X2Y
	GTP_SWAP(x, y);
#endif
    //x = x*12/11;
    //y = y*128/107;
    //printk("touch_down before : x = %d ,y = %d .\n",x,y);
   // x = GTP_MAX_WIDTH - x;
//	y = GTP_MAX_HEIGHT - y;
	
#if GTP_ICS_SLOT_REPORT
	input_mt_slot(tpd->dev, id);
//Gionee BSP1 yaoyc 20150518 add for support B protocol begin
	input_mt_report_slot_state(tpd->dev,MT_TOOL_FINGER,1);
	input_report_key(tpd->dev, BTN_TOUCH, 1);
	input_report_key(tpd->dev, BTN_TOOL_FINGER, 1);
//Gionee BSP1 yaoyc 20150518 add for support B protocol end
	//input_report_abs(tpd->dev, ABS_MT_PRESSURE, size);
	input_report_abs(tpd->dev, ABS_MT_TOUCH_MAJOR, size);
	input_report_abs(tpd->dev, ABS_MT_TRACKING_ID, id);
	input_report_abs(tpd->dev, ABS_MT_POSITION_X, x);
	input_report_abs(tpd->dev, ABS_MT_POSITION_Y, y);
#else
	input_report_key(tpd->dev, BTN_TOUCH, 1);
	if ((!size) && (!id)) {
		/* for virtual button */
		input_report_abs(tpd->dev, ABS_MT_PRESSURE, 100);
		input_report_abs(tpd->dev, ABS_MT_TOUCH_MAJOR, 100);
	} else {
		input_report_abs(tpd->dev, ABS_MT_PRESSURE, size);
		input_report_abs(tpd->dev, ABS_MT_TOUCH_MAJOR, size);
		input_report_abs(tpd->dev, ABS_MT_TRACKING_ID, id);
	}
	input_report_abs(tpd->dev, ABS_MT_POSITION_X, x);
	input_report_abs(tpd->dev, ABS_MT_POSITION_Y, y);
	input_mt_sync(tpd->dev);
#endif
	printk("GTP-INFO:gt1x down : [%d](%d,%d); %d\n",id,x,y,size);
#ifdef TPD_HAVE_BUTTON
	if (FACTORY_BOOT == get_boot_mode() || RECOVERY_BOOT == get_boot_mode()) {
		tpd_button(x, y, 1);
	}
#endif
}

void gt1x_touch_up(s32 id)
{
//Gionee BSP1 yaoyc 20150518 modify for support B protocol begin
#if GTP_ICS_SLOT_REPORT
	input_mt_slot(tpd->dev, id);
	//input_report_abs(tpd->dev, ABS_MT_TRACKING_ID, -1);
	input_mt_report_slot_state(tpd->dev,MT_TOOL_FINGER,0);
//Gionee BSP1 yaoyc 20150518 modify for support B protocol end
#else
	input_report_key(tpd->dev, BTN_TOUCH, 0);
	input_mt_sync(tpd->dev);
#endif
#ifdef TPD_HAVE_BUTTON
	if (FACTORY_BOOT == get_boot_mode() || RECOVERY_BOOT == get_boot_mode()) {
		tpd_button(0, 0, 0);
	}
#endif
	printk("GTP-INFO:gt1x up : [%d] \n",id);
}

#if GTP_CHARGER_SWITCH
#ifdef MT6573
#define CHR_CON0      (0xF7000000+0x2FA00)
#else
extern kal_bool upmu_is_chr_det(void);
#endif

u32 gt1x_get_charger_status(void)
{
	u32 chr_status = 0;
#ifdef MT6573
	chr_status = *(volatile u32 *)CHR_CON0;
	chr_status &= (1 << 13);
#else /* ( defined(MT6575) || defined(MT6577) || defined(MT6589) ) */
	chr_status = upmu_is_chr_det();
#endif
	return chr_status;
}
#endif

static int tpd_event_handler(void *unused)
{
	u8 finger = 0;
	u8 end_cmd = 0;
	s32 ret = 0;
	u8 point_data[11] = { 0 };
//Gionee BSP1 yaoyc 20150608 add for CR01478730 begin
#if TPD_IGNORE_LARGETOUCH_BY_DRIVER
	u8 frame_data[1] = {0};
#endif
//Gionee BSP1 yaoyc 20150608 add for CR01478730 end
	struct sched_param param = {.sched_priority = RTPM_PRIO_TPD };

	sched_setscheduler(current, SCHED_RR, &param);
	do {
		set_current_state(TASK_INTERRUPTIBLE);

		if (tpd_eint_mode) {
			wait_event_interruptible(waiter, tpd_flag != 0);
			tpd_flag = 0;
		} else {
			GTP_DEBUG("Polling coordinate mode!");
			msleep(tpd_polling_time);
		}

		set_current_state(TASK_RUNNING);
		 if (update_info.status)         //patch
		{
			continue;
		}  

		mutex_lock(&i2c_access);
		/* don't reset before "if (tpd_halt..."  */

#if (GTP_GESTURE_WAKEUP || DOUBLE_CLICK_WAKE)
		ret = gesture_event_handler(tpd->dev);
		if (ret >= 0) {
			gt1x_irq_enable();
			mutex_unlock(&i2c_access);
			continue;
		}
#endif
		if (tpd_halt) {
			mutex_unlock(&i2c_access);
			GTP_DEBUG("return for interrupt after suspend...  ");
			continue;
		}

		/* read coordinates */
		ret = gt1x_i2c_read(GTP_READ_COOR_ADDR, point_data, sizeof(point_data));
		if (ret < 0) {
			GTP_ERROR("I2C transfer error!");
#if !GTP_ESD_PROTECT
			gt1x_power_reset();
#endif
			goto exit_work_func;
		}
		finger = point_data[0];

		/* response to a ic request */
		if (finger == 0x00) {
			gt1x_request_event_handler();
		}

		if ((finger & 0x80) == 0) {
#if HOTKNOT_BLOCK_RW
			if (!hotknot_paired_flag)
#endif
			{
				gt1x_irq_enable();
				mutex_unlock(&i2c_access);
				GTP_ERROR("buffer not ready:0x%02x", finger);
				continue;
			}
		}

//Gionee BSP1 yaoyc 20150608 add for CR01478730 begin
#if TPD_IGNORE_LARGETOUCH_BY_DRIVER
		if ((finger & 0x40) == 0x40) 
		{ 
			GTP_DEBUG("largetouch_enable\n"); 
			largetouch_flag=2; 
			gt1x_touch_up(0); 
			//Gionee BSP1 yaoyc 20150518 add for support B protocol begin
#if GTP_ICS_SLOT_REPORT
			input_report_key(tpd->dev, BTN_TOUCH, 0);
			input_report_key(tpd->dev, BTN_TOOL_FINGER, 0);
#endif
			//Gionee BSP1 yaoyc 20150518 add for support B protocol end
			input_sync(tpd->dev);
			goto exit_work_func; 
		} 
		if((finger & 0x40) == 0&&largetouch_flag>0) //largetouch_flag 
		{ 
			ret = gt1x_i2c_read(GTP_REG_FRAME_NUM, frame_data, sizeof(frame_data)); 
			if(ret==0) { 
				GTP_DEBUG("frame_data : %d\n",frame_data[0]); 
				if(frame_data[0]>0) 
				{ 
					gt1x_touch_up(0); 
					//Gionee BSP1 yaoyc 20150518 add for support B protocol begin
#if GTP_ICS_SLOT_REPORT
					input_report_key(tpd->dev, BTN_TOUCH, 0);
					input_report_key(tpd->dev, BTN_TOOL_FINGER, 0);
#endif
					//Gionee BSP1 yaoyc 20150518 add for support B protocol end
					input_sync(tpd->dev);
					goto exit_work_func; 
				} 
				else 
				{ 
					if(finger==0x80) 
					{ 
						largetouch_flag--; 
					} 
					gt1x_touch_up(0); 
					//Gionee BSP1 yaoyc 20150518 add for support B protocol begin
#if GTP_ICS_SLOT_REPORT
					input_report_key(tpd->dev, BTN_TOUCH, 0);
					input_report_key(tpd->dev, BTN_TOOL_FINGER, 0);
#endif
					//Gionee BSP1 yaoyc 20150518 add for support B protocol end
					input_sync(tpd->dev);
					goto exit_work_func; 

				} 
			} else { 
				GTP_DEBUG("read GTP_REG_FRAME_NUM error ret : %d\n",ret); 
			} 
		} 

#endif
//Gionee BSP1 yaoyc 20150608 add for CR01478730 end

#if HOTKNOT_BLOCK_RW
		ret = hotknot_event_handler(point_data);
		if (!ret) {
			goto exit_work_func;
		}
#endif

#if GTP_PROXIMITY
		ret = gt1x_prox_event_handler(point_data);
		if (ret > 0) {
			goto exit_work_func;
		}
#endif

#if GTP_WITH_STYLUS
		ret = gt1x_touch_event_handler(point_data, tpd->dev, pen_dev);
#else
		ret = gt1x_touch_event_handler(point_data, tpd->dev, NULL);
#endif

//patch
exit_work_func:

		if(!gt1x_rawdiff_mode && (ret >=0 || ret == ERROR_VALUE)) {
			ret = gt1x_i2c_write(GTP_READ_COOR_ADDR, &end_cmd, 1);
			if (ret < 0) {
				GTP_INFO("I2C write end_cmd  error!");
			}
		}
		gt1x_irq_enable();
		mutex_unlock(&i2c_access);

	} while (!kthread_should_stop());

	return 0;
}

int gt1x_debug_proc(u8 * buf, int count)
{
	char mode_str[50] = { 0 };
	int mode;

	sscanf(buf, "%s %d", (char *)&mode_str, &mode);

	/***********POLLING/EINT MODE switch****************/
	if (strcmp(mode_str, "polling") == 0) {
		if (mode >= 10 && mode <= 200) {
			GTP_INFO("Switch to polling mode, polling time is %d", mode);
			tpd_eint_mode = 0;
			tpd_polling_time = mode;
			tpd_flag = 1;
			wake_up_interruptible(&waiter);
		} else {
			GTP_INFO("Wrong polling time, please set between 10~200ms");
		}
		return count;
	}
	if (strcmp(mode_str, "eint") == 0) {
		GTP_INFO("Switch to eint mode");
		tpd_eint_mode = 1;
		return count;
	}
	/**********************************************/
	if (strcmp(mode_str, "switch") == 0) {
		if (mode == 0)	// turn off
			tpd_off();
		else if (mode == 1)	//turn on
			tpd_on();
		else
			GTP_ERROR("error mode :%d", mode);
		return count;
	}

	return -1;
}

static u16 convert_productname(u8 * name)
{
	int i;
	u16 product = 0;
	for (i = 0; i < 4; i++) {
		product <<= 4;
		if (name[i] < '0' || name[i] > '9') {
			product += '*';
		} else {
			product += name[i] - '0';
		}
	}
	return product;
}

static int tpd_i2c_remove(struct i2c_client *client)
{
	gt1x_deinit();

	return 0;
}

static int tpd_local_init(void)
{

#if TPD_SUPPORT_I2C_DMA
	mutex_init(&dma_mutex);
	tpd->dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	gpDMABuf_va = (u8 *) dma_alloc_coherent(&tpd->dev->dev, IIC_DMA_MAX_TRANSFER_SIZE, &gpDMABuf_pa, GFP_KERNEL);
	if (!gpDMABuf_va) {
		GTP_ERROR("Allocate DMA I2C Buffer failed!");
		return -1;
	}
	memset(gpDMABuf_va, 0, IIC_DMA_MAX_TRANSFER_SIZE);
#endif
	if (i2c_add_driver(&tpd_i2c_driver) != 0) {
		GTP_ERROR("unable to add i2c driver.");
		return -1;
	}

	if (tpd_load_status == 0)	// disable auto load touch driver for linux3.0 porting
	{
		GTP_ERROR("add error touch panel driver.");
		i2c_del_driver(&tpd_i2c_driver);
		return -1;
	}
	input_set_abs_params(tpd->dev, ABS_MT_TRACKING_ID, 0, (GTP_MAX_TOUCH - 1), 0, 0);
#if TPD_HAVE_BUTTON
	tpd_button_setting(TPD_KEY_COUNT, tpd_keys_local, tpd_keys_dim_local);	// initialize tpd button data
#endif

#if (defined(TPD_WARP_START) && defined(TPD_WARP_END))
	TPD_DO_WARP = 1;
	memcpy(tpd_wb_start, tpd_wb_start_local, TPD_WARP_CNT * 4);
	memcpy(tpd_wb_end, tpd_wb_start_local, TPD_WARP_CNT * 4);
#endif

#if (defined(TPD_HAVE_CALIBRATION) && !defined(TPD_CUSTOM_CALIBRATION))
	memcpy(tpd_calmat, tpd_def_calmat_local, 8 * 4);
	memcpy(tpd_def_calmat, tpd_def_calmat_local, 8 * 4);
#endif

	// set vendor string
	tpd->dev->id.vendor = 0x00;
	tpd->dev->id.product = convert_productname(gt1x_version.product_id);
	tpd->dev->id.version = (gt1x_version.patch_id >> 8);

	GTP_INFO("end %s, %d\n", __FUNCTION__, __LINE__);
	tpd_type_cap = 1;

#ifdef TPD_SUSPEND_HOTKNOT
	mutex_init(&tp_suspend_mutex);
#endif
	return 0;
}

/* Function to manage low power suspend */
static void tpd_suspend(struct early_suspend *h)
{
	s32 ret = -1;

#ifdef TPD_SUSPEND_HOTKNOT
	GTP_INFO("TPD suspend debug start...");
	mutex_lock(&tp_suspend_mutex);
#endif
	if (is_reseting||update_info.status)      //patch
	{
#ifdef TPD_SUSPEND_HOTKNOT
		mutex_unlock(&tp_suspend_mutex);
#endif	
		return;
	}

#if GTP_HOTKNOT && !HOTKNOT_BLOCK_RW
	u8 buf[1] = { 0 };
#endif
	GTP_INFO("TPD suspend start...");

#if GTP_SMART_COVER   
    if (gt1x_sc_dev) {
        gt1x_sc_dev->suspended = 1;
    }
#endif

#if GTP_PROXIMITY
	if (gt1x_proximity_flag == 1) {
		GTP_INFO("Suspend: proximity is detected!");
#ifdef TPD_SUSPEND_HOTKNOT
		mutex_unlock(&tp_suspend_mutex);
#endif		
		return;
	}
#endif

#if GTP_HOTKNOT
	if (hotknot_enabled) {
#if HOTKNOT_BLOCK_RW
		if (hotknot_paired_flag) {
			GTP_INFO("Suspend: hotknot is paired!");
#ifdef TPD_SUSPEND_HOTKNOT
			mutex_unlock(&tp_suspend_mutex);
#endif
			return;
		}
#else
		gt1x_i2c_read(GTP_REG_HN_PAIRED, buf, sizeof(buf));
		GTP_DEBUG("0x81AA: 0x%02X", buf[0]);
		if (buf[0] == 0x55) {
			GTP_INFO("Suspend: hotknot is paired!");
#ifdef TPD_SUSPEND_HOTKNOT
			mutex_unlock(&tp_suspend_mutex);
#endif			
			return;
		}
#endif
	}
#endif
	tpd_halt = 1;

#if GTP_ESD_PROTECT
	gt1x_esd_switch(SWITCH_OFF);
#endif

#if GTP_CHARGER_SWITCH
	gt1x_charger_switch(SWITCH_OFF);
#endif

	mutex_lock(&i2c_access);

#if (GTP_GESTURE_WAKEUP || DOUBLE_CLICK_WAKE)
	gesture_clear_wakeup_data();
	if (gesture_enabled || wake_switch) {
		gesture_enter_doze();
	} else
#endif
	{
		gt1x_irq_disable();
		ret = gt1x_enter_sleep();
		if (ret < 0) {
			GTP_ERROR("GTP early suspend failed.");
		}
	}

	mutex_unlock(&i2c_access);
#ifdef TPD_SUSPEND_HOTKNOT
	mutex_unlock(&tp_suspend_mutex);
#endif	
	GTP_INFO("TPD suspend end...");	
	msleep(58);
}

/* Function to manage power-on resume */
static void tpd_resume(struct early_suspend *h)
{
	s32 ret = -1;

	GTP_INFO("TPD resume start...");
	if (is_reseting||update_info.status)     //patch
	{
		return;
	}

	
#if GTP_SMART_COVER    
	if (gt1x_sc_dev) {
	    gt1x_sc_dev->suspended = 0;
	}
#endif

#if GTP_PROXIMITY
	if (gt1x_proximity_flag == 1) {
		GTP_INFO("Resume: proximity is on!");
		return;
	}
#endif

#if GTP_HOTKNOT
	if (hotknot_enabled) {
#if HOTKNOT_BLOCK_RW
		if (hotknot_paired_flag) {
			//hotknot_paired_flag = 0;    //jica20150813--4 //
			//hotknot_wakeup_block();
			GTP_INFO("Resume: hotknot is paired!");
			return;
		}
#endif
	}
#endif

	ret = gt1x_wakeup_sleep();
	if (ret < 0) {
		GTP_ERROR("GTP later resume failed.");
	}
#if GTP_HOTKNOT
	if (!hotknot_enabled) {
		gt1x_send_cmd(GTP_CMD_HN_EXIT_SLAVE, 0);
	}
#endif

#if GTP_CHARGER_SWITCH
	gt1x_charger_config(0);
	gt1x_charger_switch(SWITCH_ON);
#endif

	tpd_halt = 0;
	gt1x_irq_enable();

#if GTP_ESD_PROTECT
	gt1x_esd_switch(SWITCH_ON);
#endif

	GTP_INFO("tpd resume end.");
}

static struct tpd_driver_t tpd_device_driver = {
	.tpd_device_name = "gt9xx",
	.tpd_local_init = tpd_local_init,
	.suspend = tpd_suspend,
	.resume = tpd_resume,
#ifdef TPD_HAVE_BUTTON
	.tpd_have_button = 1,
#else
	.tpd_have_button = 0,
#endif
};

void tpd_off(void)
{
	gt1x_power_switch(SWITCH_OFF);
	tpd_halt = 1;
	gt1x_irq_disable();
}

void tpd_on(void)
{
	s32 ret = -1, retry = 0;

	while (retry++ < 5) {
		ret = tpd_power_on();
		if (ret < 0) {
			GTP_ERROR("I2C Power on ERROR!");
		}

		ret = gt1x_send_cfg(gt1x_config, gt1x_cfg_length);
		if (ret == 0) {
			GTP_DEBUG("Wakeup sleep send gt1x_config success.");
			break;
		}
	}
	if (ret < 0) {
		GTP_ERROR("GTP later resume failed.");
	}
	//gt1x_irq_enable();
	tpd_halt = 0;
}

/* called when loaded into kernel */
static int __init tpd_driver_init(void)
{
	GTP_INFO("Goodix touch panel driver init begin");
       // printk("Goodix touch panel driver init.  \n");
	i2c_register_board_info(1, &i2c_tpd, 1);

	if (tpd_driver_add(&tpd_device_driver) < 0) {
		printk("add generic driver failed\n");
	}

	return 0;
}

/* should never be called */
static void __exit tpd_driver_exit(void)
{
	GTP_INFO("MediaTek gt91xx touch panel driver exit\n");
	tpd_driver_remove(&tpd_device_driver);
}

module_init(tpd_driver_init);
module_exit(tpd_driver_exit);

/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "show_animation_common.h"
#include "show_logo_log.h"
#include "fast_charging_common.h"
#include "charging_animation.h"
  
static int charging_low_index = 0;
static int charging_animation_index = 0;
static int version0_charging_index = 0;
static int bits = 0;

#define CHECK_LOGO_BIN_OK  0
#define CHECK_LOGO_BIN_ERROR  -1


static unsigned short  number_pic_addr[(NUMBER_RIGHT - NUMBER_LEFT)*(NUMBER_BOTTOM - NUMBER_TOP)*4] = {0x0}; //addr
static unsigned short  line_pic_addr[(TOP_ANIMATION_RIGHT - TOP_ANIMATION_LEFT)*4] = {0x0};
static unsigned short  percent_pic_addr[(PERCENT_RIGHT - PERCENT_LEFT)*(PERCENT_BOTTOM - PERCENT_TOP)*4] = {0x0};
static unsigned short  top_animation_addr[(TOP_ANIMATION_RIGHT - TOP_ANIMATION_LEFT)*(TOP_ANIMATION_BOTTOM - TOP_ANIMATION_TOP)*4] = {0x0};



/*
 * Check logo.bin address if valid, and get logo related info
 *
 */ 
int check_logo_index_valid(unsigned int index, void * logo_addr, LOGO_PARA_T * logo_info)
{  
    unsigned int *pinfo = (unsigned int*)logo_addr;
    logo_info->logonum = pinfo[0];
    
    LOG_ANIM("[show_animation_common: %s %d]logonum =%d, index =%d\n", __FUNCTION__,__LINE__,logo_info->logonum, index);
    if (index >= logo_info->logonum)
    {
        LOG_ANIM("[show_animation_common: %s %d]unsupported logo, index =%d\n", __FUNCTION__,__LINE__, index);
        return CHECK_LOGO_BIN_ERROR;
    
    }   
   // LOG_ANIM("show_animation_common, pinfo[1]=%d,  pinfo[2+index]=%d, pinfo[3+index]=%d, pinfo[3+index] - pinfo[2+index]= %d, pinfo[1] - pinfo[2+index] =%d \n", 
   //            pinfo[1],pinfo[2+index],pinfo[3+index], pinfo[3+index] - pinfo[2+index],pinfo[1] - pinfo[2+index]); 
    
    if(index < logo_info->logonum - 1)
        logo_info->logolen = pinfo[3+index] - pinfo[2+index];
    else
        logo_info->logolen = pinfo[1] - pinfo[2+index];

    logo_info->inaddr = (unsigned int)logo_addr + pinfo[2+index];
    LOG_ANIM("show_animation_common, in_addr=0x%08x,  logolen=%d\n", 
                logo_info->inaddr,  logo_info->logolen);

    return CHECK_LOGO_BIN_OK;                
}


/*
 * Fill a screen size buffer with logo content 
 *
 */ 
void fill_animation_logo(unsigned int index, void *fill_addr, void * dec_logo_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOGO_PARA_T logo_info;
    int logo_width;
    int logo_height;
    int raw_data_size;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return; 

    raw_data_size = decompress_logo((void*)logo_info.inaddr, dec_logo_addr, logo_info.logolen, phical_screen.fb_size);
    //RECT_REGION_T rect = {0, 0, phical_screen.width, phical_screen.height};   
    logo_width =phical_screen.width;
    logo_height = phical_screen.height;
    if (phical_screen.rotation == 270 || phical_screen.rotation == 90) {
        logo_width =phical_screen.height;
        logo_height = phical_screen.width;
    }
    if (0 == bits) {
        if (raw_data_size == logo_width*logo_height*4) {
            bits = 32;
        } else if (raw_data_size == logo_width*logo_height*2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    
    RECT_REGION_T rect = {0, 0, logo_width, logo_height};   
    
    fill_rect_with_content(fill_addr, rect, dec_logo_addr, phical_screen, bits);

    
}

/*
 * Fill a rectangle size address with special color 
 *
 */ 
void fill_animation_prog_bar(RECT_REGION_T rect_bar,
                       unsigned int fgColor, 
                       unsigned int start_div, unsigned int occupied_div,
                       void *fill_addr, LCM_SCREEN_T phical_screen)
{
    unsigned int div_size  = (rect_bar.bottom - rect_bar.top) / (ANIM_V0_REGIONS);
    unsigned int draw_size = div_size - (ANIM_V0_SPACE_AFTER_REGION);
    
    unsigned int i;
    
    for (i = start_div; i < start_div + occupied_div; ++ i)
    {
        unsigned int draw_bottom = rect_bar.bottom - div_size * i - (ANIM_V0_SPACE_AFTER_REGION);
        unsigned int draw_top    = draw_bottom - draw_size;
        
        RECT_REGION_T rect = {rect_bar.left, draw_top, rect_bar.right, draw_bottom};

        fill_rect_with_color(fill_addr, rect, fgColor, phical_screen);

    }
}


/*
 * Fill a rectangle with logo content 
 *
 */ 
void fill_animation_dynamic(unsigned int index, RECT_REGION_T rect, void *fill_addr, void * dec_logo_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOGO_PARA_T logo_info;
    int raw_data_size;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return; 
                    
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)dec_logo_addr, logo_info.logolen, (rect.right-rect.left)*(rect.bottom-rect.top)*4);

    if (0 == bits) {
        if (raw_data_size == (rect.right-rect.left)*(rect.bottom-rect.top)*4) {
            bits = 32;
        } else if (raw_data_size == (rect.right-rect.left)*(rect.bottom-rect.top)*2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    fill_rect_with_content(fill_addr, rect, dec_logo_addr, phical_screen, bits);            
}


/*
 * Fill a rectangle  with number logo content 
 *
 * number_position: 0~1st number, 1~2nd number 
 */
void fill_animation_number(unsigned int index, unsigned int number_position, void *fill_addr,  void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOG_ANIM("[show_animation_common: %s %d]index= %d, number_position = %d\n",__FUNCTION__,__LINE__, index, number_position);

    LOGO_PARA_T logo_info;
    int raw_data_size;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return;                 

    // draw default number rect,
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)number_pic_addr, logo_info.logolen, number_pic_size);

    //static RECT_REGION_T number_location_rect = {NUMBER_LEFT,NUMBER_TOP,NUMBER_RIGHT,NUMBER_BOTTOM};    
    RECT_REGION_T battery_number_rect = {NUMBER_LEFT + (NUMBER_RIGHT - NUMBER_LEFT)*number_position,
                            NUMBER_TOP,
                            NUMBER_RIGHT + (NUMBER_RIGHT - NUMBER_LEFT)*number_position,
                            NUMBER_BOTTOM};

    if (0 == bits) {
        if (raw_data_size == (NUMBER_RIGHT - NUMBER_LEFT)*(NUMBER_BOTTOM - NUMBER_TOP)*4) {
            bits = 32;
        } else if (raw_data_size == (NUMBER_RIGHT - NUMBER_LEFT)*(NUMBER_BOTTOM - NUMBER_TOP)*2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    fill_rect_with_content(fill_addr, battery_number_rect, number_pic_addr,phical_screen, bits);            
}

/*
 * Fill a line with special color 
 *
 */ 
void fill_animation_line(unsigned int index, unsigned int capacity_grids, void *fill_addr,  void * logo_addr, LCM_SCREEN_T phical_screen)
{    
    LOGO_PARA_T logo_info;
    int raw_data_size;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return; 
                
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)line_pic_addr, logo_info.logolen, line_pic_size);

    if (0 == bits) {
        if (raw_data_size == (TOP_ANIMATION_RIGHT - TOP_ANIMATION_LEFT)*4) {
            bits = 32;
        } else if (raw_data_size == (TOP_ANIMATION_RIGHT - TOP_ANIMATION_LEFT)*2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    RECT_REGION_T rect = {CAPACITY_LEFT, CAPACITY_TOP, CAPACITY_RIGHT, CAPACITY_BOTTOM}; 
    int i = capacity_grids;
    for(; i < CAPACITY_BOTTOM; i++)
    {      
        rect.top = i;
        rect.bottom = i+1;                                             
        fill_rect_with_content(fill_addr, rect, line_pic_addr, phical_screen, bits);

    }
}

//Gionee BSP1 ningyd 20141212 modify for CR01425187 begin
#if defined(CONFIG_GN_BSP_AMIGO_CHARGING_SUPPORT)
#define AMIGO_PIC_START_INDEX 39
#define AMIGO_BLACK_PIC_INDEX AMIGO_PIC_START_INDEX+15

typedef enum
{
	AMIGO_EMPTY_LEVEL=0,     //capacity from 0% to 9%
	AMIGO_10_LEVEL,         //capacity from 10% to 20%
	AMIGO_20_LEVEL,         //capacity from 21% to 30%
	AMIGO_30_LEVEL,         //capacity from 31% to 40%
	AMIGO_40_LEVEL,         //capacity from 41% to 50%
	AMIGO_50_LEVEL,         //capacity from 51% to 60%
	AMIGO_60_LEVEL,         //capacity from 61% to 70%
	AMIGO_70_LEVEL,         //capacity from 71% to 80%
	AMIGO_80_LEVEL,         //capacity from 81% to 90%
	AMIGO_90_LEVEL,         //capacity from 91% to 99%	
	AMIGO_FULL_LEVEL,
	AMIGO_FAILED_LEVEL
}AMIGO_BATTERY_LEVEL;

typedef enum
{
	AMIGO_BATFULL_ANIM = 0,
	AMIGO_BATEMPTY_START_ANIM = 1,
#ifdef CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
	AMIGO_BATE_10_START_ANIM = 2,
	AMIGO_BATE_20_START_ANIM = 3,
	AMIGO_BATE_30_START_ANIM = 4,
	AMIGO_BATE_40_START_ANIM = 5,
	AMIGO_BATE_50_START_ANIM = 6,
	AMIGO_BATE_60_START_ANIM = 7,
	AMIGO_BATE_70_START_ANIM = 8,
	AMIGO_BATE_80_START_ANIM = 9,
	AMIGO_BATE_90_START_ANIM = 10,
#endif	
	AMIGO_BATLOW_START_ANIM = 13,
	AMIGO_BATMID_START_ANIM = 25,
	AMIGO_BATHIGH_START_ANIM = 37,
	AMIGO_BATLOW_PERCENT = 49,
	AMIGO_BATLOW_NUM_0 = 50,
	AMIGO_BATMID_PERCENT = 60,
	AMIGO_BATMID_NUM_0 = 61,
	AMIGO_BATHIGH_PERCENT = 71,
	AMIGO_BATHIGH_NUM_0 = 72
}AMIGO_PIC_OFFSET_MAP;

typedef struct 
{
	unsigned int anim_offset;
	unsigned int num_offset;
	unsigned int percent_offset;
}AMIGO_OFFSET_INFO;

typedef struct 
{
	RECT_REGION_T *rect_num1;
	RECT_REGION_T *rect_num2;
	RECT_REGION_T *rect_percent;
}AMIGO_CAPACITY_LOCATION;

#ifdef CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
#define AMIGO_PIC_WIDTH 720
#define AMIGO_PIC_LEN 1280

#define AMIGO_ANIM_SIZE AMIGO_PIC_WIDTH*AMIGO_PIC_LEN*4
unsigned short amigo_anim_buf[AMIGO_ANIM_SIZE] = {0x0};
RECT_REGION_T amigo_anim_location = {0,0,0+AMIGO_PIC_WIDTH,0+AMIGO_PIC_LEN};

#define IUNI_PIC_WIDTH_LEN 428
#define IUNI_ANIM_SIZE IUNI_PIC_WIDTH_LEN*IUNI_PIC_WIDTH_LEN*4
unsigned short iuni_anim_buf[IUNI_ANIM_SIZE] = {0x0};
RECT_REGION_T iuni_anim_location = {146,426,146+IUNI_PIC_WIDTH_LEN,426+IUNI_PIC_WIDTH_LEN};

#else
#define AMIGO_PIC_WIDTH_LEN 428
#define AMIGO_ANIM_SIZE AMIGO_PIC_WIDTH_LEN*AMIGO_PIC_WIDTH_LEN*4
unsigned short amigo_anim_buf[AMIGO_ANIM_SIZE] = {0x0};
RECT_REGION_T amigo_anim_location = {146,426,146+AMIGO_PIC_WIDTH_LEN,426+AMIGO_PIC_WIDTH_LEN};
#endif
#define AMIGO_NUM_WIDTH 15
#define AMIGO_NUM_LEN 23
#define AMIGO_NUM_SIZE AMIGO_NUM_LEN*AMIGO_NUM_WIDTH*4
unsigned short amigo_num_buf[AMIGO_NUM_SIZE]={0x00};

#define AMIGO_PERCENT_WIDTH_LEN 23
#define AMIGO_PERCENT_SIZE AMIGO_PERCENT_WIDTH_LEN*AMIGO_PERCENT_WIDTH_LEN*4
unsigned short amigo_percent_buf[AMIGO_PERCENT_SIZE]={0x00};

//BatteryCapacity-label's location when Battery level is AMIGO_EMPTY_LEVEL
RECT_REGION_T amigo_Empty_num_location = {339,694,339+AMIGO_NUM_WIDTH,694+AMIGO_NUM_LEN};
RECT_REGION_T amigo_Empty_percent_location = {357,694,357+AMIGO_PERCENT_WIDTH_LEN,694+AMIGO_PERCENT_WIDTH_LEN};

//BatteryCapacity-label's location when Battery level is not AMIGO_EMPTY_LEVEL
RECT_REGION_T amigo_batNotEmpty_num1_location = {331,694,331+AMIGO_NUM_WIDTH,694+AMIGO_NUM_LEN};
RECT_REGION_T amigo_batNotEmpty_num2_location = {348,694,348+AMIGO_NUM_WIDTH,694+AMIGO_NUM_LEN};
RECT_REGION_T amigo_batNotEmpty_percent_location = {365,694,365+AMIGO_PERCENT_WIDTH_LEN,694+AMIGO_PERCENT_WIDTH_LEN};

static int amigo_animation_index = 0;

/*
 * Fill a rectangle  with Amigo Charging animation content  
 */
 //Gionee BSP1 ningyd 20141013 modify for CR01395060 begin
void fill_amigo_logo(unsigned int index, void *fill_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
	LOG_ANIM("[show_animation_common: %s %d]index= %d\n",__FUNCTION__,__LINE__, index);	
    LOGO_PARA_T logo_info;
    int raw_data_size;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return; 
                    
    unsigned short *iuni_logo_addr = malloc(IUNI_ANIM_SIZE);
    memset(iuni_logo_addr,0,IUNI_ANIM_SIZE);
    // draw default number rect,
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)iuni_logo_addr, logo_info.logolen, IUNI_ANIM_SIZE);
    
    if (0 == bits) {
        if (raw_data_size == IUNI_ANIM_SIZE) {
            bits = 32;
        } else if (raw_data_size == IUNI_ANIM_SIZE/2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    
	fill_rect_with_content(fill_addr, iuni_anim_location, iuni_logo_addr, phical_screen,bits);
	free(iuni_logo_addr); 
}
//Gionee BSP1 ningyd 20141013 modify for CR01395060 end

void fill_amigo_animation(unsigned int offset, void *fill_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
	LOG_ANIM("[show_animation_common: %s %d]index= %d\n",__FUNCTION__,__LINE__, offset);	
    LOGO_PARA_T logo_info;
    int raw_data_size;
    unsigned int index = AMIGO_PIC_START_INDEX + offset;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return;

    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)amigo_anim_buf, logo_info.logolen, AMIGO_ANIM_SIZE);
    if (0 == bits) {
        if (raw_data_size == AMIGO_ANIM_SIZE) {
            bits = 32;
        } else if (raw_data_size == AMIGO_ANIM_SIZE/2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    fill_rect_with_content(fill_addr, amigo_anim_location, (unsigned int *)amigo_anim_buf, phical_screen,bits);          
}

void fill_amigo_number(unsigned int offset, RECT_REGION_T *amigo_num_rect, void *fill_addr,  void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOG_ANIM("[show_animation_common: %s %d]index= %d\n",__FUNCTION__,__LINE__, offset);

	if(amigo_num_rect==NULL)
	{
		LOG_ANIM("[show_animation_common] : num_rect null.\n");
		return;
	}

    LOGO_PARA_T logo_info;
    int raw_data_size;
    unsigned int index = AMIGO_PIC_START_INDEX + offset;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return;                 

    // draw default number rect,
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)amigo_num_buf, logo_info.logolen, AMIGO_NUM_SIZE); 
    if (0 == bits) {
        if (raw_data_size == AMIGO_NUM_SIZE) {
            bits = 32;
        } else if (raw_data_size == AMIGO_NUM_SIZE/2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    fill_rect_with_content(fill_addr, *amigo_num_rect, (unsigned int *)amigo_num_buf,phical_screen,bits);            
}

void fill_amigo_percent(unsigned int offset, RECT_REGION_T *amigo_percent_rect, void *fill_addr,  void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOG_ANIM("[show_animation_common: %s %d]index= %d\n",__FUNCTION__,__LINE__, offset);

	if(amigo_percent_rect == NULL)
	{
		LOG_ANIM("[show_animation_common] : percent_rect null.\n");
		return;
	}
	
    LOGO_PARA_T logo_info;
    int raw_data_size;
    unsigned int index = AMIGO_PIC_START_INDEX + offset;
    if(check_logo_index_valid(index, logo_addr, &logo_info) != CHECK_LOGO_BIN_OK)
        return;              

    // draw default number rect,
    raw_data_size = decompress_logo((void*)logo_info.inaddr, (void*)amigo_percent_buf, logo_info.logolen, AMIGO_PERCENT_SIZE); 
    if (0 == bits) {
        if (raw_data_size == AMIGO_PERCENT_SIZE) {
            bits = 32;
        } else if (raw_data_size == AMIGO_PERCENT_SIZE/2) {
            bits = 16;
        } else {
            LOG_ANIM("[show_animation_common: %s %d]Logo data error\n",__FUNCTION__,__LINE__);
            return;
        }
        LOG_ANIM("[show_animation_common: %s %d]bits = %d\n",__FUNCTION__,__LINE__, bits);
    }
    fill_rect_with_content(fill_addr, *amigo_percent_rect, (unsigned int *)amigo_percent_buf,phical_screen,bits);            
}

AMIGO_BATTERY_LEVEL amigo_get_batteryLevel(unsigned int capacity)
{
#ifdef CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
	if(capacity >= 100)
		return AMIGO_FULL_LEVEL;
	if(capacity < 10)
		return AMIGO_EMPTY_LEVEL;
	if(capacity < 20)
		return AMIGO_10_LEVEL;
	if(capacity < 30)
		return AMIGO_20_LEVEL;
	if(capacity < 40)
		return AMIGO_30_LEVEL;	
	if(capacity < 50)
		return AMIGO_40_LEVEL;
	if(capacity < 60)
		return AMIGO_50_LEVEL;
	if(capacity < 70)
		return AMIGO_60_LEVEL;	
	if(capacity < 80)
		return AMIGO_70_LEVEL;	
	if(capacity < 90)
		return AMIGO_80_LEVEL;	
	if(capacity < 100)
		return AMIGO_90_LEVEL;
#else
	if(capacity >= 100)
		return AMIGO_FULL_LEVEL;
	if(capacity < 10)
		return AMIGO_EMPTY_LEVEL;
	if(capacity >= 10 && capacity < 30)
		return AMIGO_LOW_LEVEL;
	if(capacity >= 30 && capacity < 60)
		return AMIGO_MID_LEVEL;
	if(capacity >= 60 && capacity <= 99)
		return AMIGO_HIGH_LEVEL;
#endif	
	return AMIGO_FAILED_LEVEL;
}

AMIGO_CAPACITY_LOCATION amigo_set_BatCapacity_location(AMIGO_BATTERY_LEVEL batLevel)
{
	AMIGO_CAPACITY_LOCATION amigo_capacity_location;
	if(batLevel == AMIGO_FULL_LEVEL)
	{
		amigo_capacity_location.rect_num1 = NULL;
		amigo_capacity_location.rect_num2 = NULL;
		amigo_capacity_location.rect_percent = NULL;
		return amigo_capacity_location;
	}
	if(batLevel == AMIGO_EMPTY_LEVEL)
	{
		amigo_capacity_location.rect_num2 = &amigo_Empty_num_location;
		amigo_capacity_location.rect_num1 = NULL;
		amigo_capacity_location.rect_percent = &amigo_Empty_percent_location;
		return amigo_capacity_location;
	}

	amigo_capacity_location.rect_num1 = &amigo_batNotEmpty_num1_location;
	amigo_capacity_location.rect_num2 = &amigo_batNotEmpty_num2_location;
	amigo_capacity_location.rect_percent = &amigo_batNotEmpty_percent_location;
	return amigo_capacity_location;
}

AMIGO_OFFSET_INFO amigo_set_showing_pictures_offset(AMIGO_BATTERY_LEVEL batLevel)
{
	AMIGO_OFFSET_INFO amigo_offset_info;
	switch(batLevel)
	{
#ifdef CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
		case AMIGO_FULL_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATFULL_ANIM;
			break;
		case AMIGO_90_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_90_START_ANIM;
			break;
		case AMIGO_80_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_80_START_ANIM;
			break;
		case AMIGO_70_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_70_START_ANIM;
			break;
		case AMIGO_60_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_60_START_ANIM;
			break;
		case AMIGO_50_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_50_START_ANIM;
			break;
		case AMIGO_40_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_40_START_ANIM;
			break;
		case AMIGO_30_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_30_START_ANIM;
			break;
		case AMIGO_20_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_20_START_ANIM;
			break;
		case AMIGO_10_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATE_10_START_ANIM;
			break;
		case AMIGO_EMPTY_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATEMPTY_START_ANIM;
			break;
		default :
			LOG_ANIM("[show_animation_common] : Error battery level.\n");
			break;	
#else
		case AMIGO_FULL_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATFULL_ANIM;
			break;
		case AMIGO_EMPTY_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATEMPTY_START_ANIM;
			amigo_offset_info.num_offset = AMIGO_BATLOW_NUM_0;
			amigo_offset_info.percent_offset = AMIGO_BATLOW_PERCENT;
			break;
		case AMIGO_LOW_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATLOW_START_ANIM;
			amigo_offset_info.num_offset = AMIGO_BATLOW_NUM_0;
			amigo_offset_info.percent_offset = AMIGO_BATLOW_PERCENT;
			break;
		case AMIGO_MID_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATMID_START_ANIM;
			amigo_offset_info.num_offset = AMIGO_BATMID_NUM_0;
			amigo_offset_info.percent_offset = AMIGO_BATMID_PERCENT;
			break;
		case AMIGO_HIGH_LEVEL:
			amigo_offset_info.anim_offset = AMIGO_BATHIGH_START_ANIM;
			amigo_offset_info.num_offset = AMIGO_BATHIGH_NUM_0;
			amigo_offset_info.percent_offset = AMIGO_BATHIGH_PERCENT;
			break;
		default :
			LOG_ANIM("[show_animation_common] : Error battery level.\n");
			break;
#endif			
	}
	
	return amigo_offset_info;
}

void amigo_show_charging_animation(unsigned int capacity,void * fill_addr,void * dec_logo_addr,void * logo_addr,LCM_SCREEN_T phical_screen)
{
	AMIGO_BATTERY_LEVEL batLevel = amigo_get_batteryLevel(capacity);
	if(batLevel == AMIGO_FAILED_LEVEL)
	{
		LOG_ANIM("BatteryLevel is Error, Stop showing animations.\n");
		return;
	}	
	
	AMIGO_OFFSET_INFO amigo_offset_info = amigo_set_showing_pictures_offset(batLevel);
	AMIGO_CAPACITY_LOCATION amigo_capacity_location = amigo_set_BatCapacity_location(batLevel);

	//Gionee BSP1 ningyd 20140818 modify for CR01373126 begin
	if(batLevel == AMIGO_FULL_LEVEL)
	{
		fill_amigo_animation(amigo_offset_info.anim_offset,fill_addr,logo_addr,phical_screen);
		return;
	}
	//Gionee BSP1 ningyd 20140818 modify for CR01373126 end
#ifdef CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
	fill_amigo_animation(amigo_offset_info.anim_offset+amigo_animation_index,fill_addr,logo_addr,phical_screen);

	//fill_amigo_number(amigo_offset_info.num_offset + (capacity/10), amigo_capacity_location.rect_num1, fill_addr, logo_addr, phical_screen);
	//fill_amigo_number(amigo_offset_info.num_offset + (capacity%10), amigo_capacity_location.rect_num2, fill_addr, logo_addr, phical_screen);
	//fill_amigo_percent(amigo_offset_info.percent_offset,amigo_capacity_location.rect_percent,fill_addr,logo_addr,phical_screen);

	//amigo_animation_index++ ;
	//if (amigo_animation_index >= 11) amigo_animation_index = 0;

#else
	fill_amigo_animation(amigo_offset_info.anim_offset+amigo_animation_index,fill_addr,logo_addr,phical_screen);
	
    fill_amigo_number(amigo_offset_info.num_offset + (capacity/10), amigo_capacity_location.rect_num1, fill_addr, logo_addr, phical_screen);
	fill_amigo_number(amigo_offset_info.num_offset + (capacity%10), amigo_capacity_location.rect_num2, fill_addr, logo_addr, phical_screen);
    fill_amigo_percent(amigo_offset_info.percent_offset,amigo_capacity_location.rect_percent,fill_addr,logo_addr,phical_screen);
    
    amigo_animation_index++ ;
	u32 i=0;
	for(i=300;i>0;i--);
    if (amigo_animation_index >= 12) amigo_animation_index = 0;
#endif	
}


void amigo_fill_animation_battery_ver_1(unsigned int capacity,void * fill_addr,void * dec_logo_addr,void * logo_addr,LCM_SCREEN_T phical_screen)
{
	fill_animation_logo(AMIGO_BLACK_PIC_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);

	amigo_show_charging_animation(capacity,fill_addr,dec_logo_addr,logo_addr,phical_screen);		
}
#endif
//Gionee BSP1 ningyd 20141212 modify for CR01425187 end
/*
 * Show lod charging animation
 *
 */
void fill_animation_battery_old(unsigned int capacity,  void *fill_addr, void * dec_logo_addr, void * logo_addr,
                       LCM_SCREEN_T phical_screen)
{
    int capacity_grids = 0;
    if (capacity > 100) capacity = 100;
    capacity_grids = (capacity * (ANIM_V0_REGIONS)) / 100;

    if (version0_charging_index < capacity_grids * 2)
        version0_charging_index = capacity_grids * 2;

    if (capacity < 100){
        version0_charging_index > 7? version0_charging_index = capacity_grids * 2 : version0_charging_index++;
    } else {
        version0_charging_index = ANIM_V0_REGIONS * 2;
    }

    fill_animation_logo(ANIM_V0_BACKGROUND_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);

    RECT_REGION_T rect_bar = {bar_rect.left + 1, bar_rect.top + 1, bar_rect.right, bar_rect.bottom};

    fill_animation_prog_bar(rect_bar,
                       (unsigned int)(BAR_OCCUPIED_COLOR), 
                       0,  version0_charging_index/2,
                       fill_addr, phical_screen);                              

    fill_animation_prog_bar(rect_bar,
                      (unsigned int)(BAR_EMPTY_COLOR),
                      version0_charging_index/2, ANIM_V0_REGIONS - version0_charging_index/2,
                      fill_addr, phical_screen); 

}

/*
 * Show new charging animation
 *
 */
void fill_animation_battery_new(unsigned int capacity, void *fill_addr, void * dec_logo_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOG_ANIM("[show_animation_common: %s %d]capacity : %d\n",__FUNCTION__,__LINE__, capacity);
    //Gionee BSP1 ningyd 20140415 modify for CR01357829 begin
    #ifndef CONFIG_GN_BSP_AMIGO_CHARGING_SUPPORT 
    if (capacity >= 100) {
        //show_logo(37); // battery 100
        fill_animation_logo(FULL_BATTERY_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);
     
    } else if (capacity < 10) {
        LOG_ANIM("[show_animation_common: %s %d]charging_low_index = %d\n",__FUNCTION__,__LINE__, charging_low_index);  
        charging_low_index ++ ;
                
        fill_animation_logo(LOW_BAT_ANIM_START_0 + charging_low_index, fill_addr, dec_logo_addr, logo_addr,phical_screen);
        fill_animation_number(NUMBER_PIC_START_0 + capacity, 1, fill_addr, logo_addr, phical_screen);
        fill_animation_dynamic(NUMBER_PIC_PERCENT, percent_location_rect, fill_addr, percent_pic_addr, logo_addr, phical_screen);
        
        if (charging_low_index >= 9) charging_low_index = 0;

    } else {

        unsigned int capacity_grids = 0;
        //static RECT_REGION_T battery_rect = {CAPACITY_LEFT,CAPACITY_TOP,CAPACITY_RIGHT,CAPACITY_BOTTOM};
        capacity_grids = CAPACITY_BOTTOM - (CAPACITY_BOTTOM - CAPACITY_TOP) * (capacity - 10) / 90;
        LOG_ANIM("[show_animation_common: %s %d]capacity_grids : %d,charging_animation_index = %d\n",__FUNCTION__,__LINE__, capacity_grids,charging_animation_index);   

        //background 
        fill_animation_logo(ANIM_V1_BACKGROUND_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);
        
        fill_animation_line(ANIM_LINE_INDEX, capacity_grids, fill_addr,  logo_addr, phical_screen);
        fill_animation_number(NUMBER_PIC_START_0 + (capacity/10), 0, fill_addr, logo_addr, phical_screen);
        fill_animation_number(NUMBER_PIC_START_0 + (capacity%10), 1, fill_addr, logo_addr, phical_screen);
        fill_animation_dynamic(NUMBER_PIC_PERCENT, percent_location_rect, fill_addr, percent_pic_addr, logo_addr, phical_screen);                
        
        
         if (capacity <= 90)
         {
            RECT_REGION_T top_animation_rect = {TOP_ANIMATION_LEFT, capacity_grids - (TOP_ANIMATION_BOTTOM - TOP_ANIMATION_TOP), TOP_ANIMATION_RIGHT, capacity_grids};
            //top_animation_rect.bottom = capacity_grids;
            //top_animation_rect.top = capacity_grids - top_animation_height;
            charging_animation_index++;        
            //show_animation_dynamic(15 + charging_animation_index, top_animation_rect, top_animation_addr);
            fill_animation_dynamic(BAT_ANIM_START_0 + charging_animation_index, top_animation_rect, fill_addr, top_animation_addr, logo_addr, phical_screen);  
            
            if (charging_animation_index >= 9) charging_animation_index = 0;
         }
    }

    #else
		amigo_fill_animation_battery_ver_1(capacity,fill_addr,dec_logo_addr,logo_addr,phical_screen);

	#endif
   //Gionee BSP1 ningyd 20140415 modify for CR01357829 end
}

/*
 * Show wireless charging animation
 * total 29 logo:from 39 ~ 68 
 * less(0<10): 50-53 , low(<30):54-57 ,middle(<60):58-61 , high():62-75 , o:66, full:67,num (0-9):39-48, %:49
 *
 */

 void fill_animation_battery_wireless_charging(unsigned int capacity, void *fill_addr, void * dec_logo_addr, void * logo_addr, LCM_SCREEN_T phical_screen)
{
    LOG_ANIM("[show_animation_common: %s %d]capacity : %d\n",__FUNCTION__,__LINE__, capacity);
//    RECT_REGION_T wireless_bgd_rect = {0, 0, phical_screen.width, phical_screen.height};
    
    charging_low_index >= 3? charging_low_index = 0:charging_low_index++;
    LOG_ANIM("[show_animation_common: %s %d]charging_low_index = %d\n",__FUNCTION__,__LINE__, charging_low_index); 
    
    if (capacity >= 100) {
         // battery 100
        fill_animation_logo(V2_BAT_100_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);
    } else if (capacity <= 0) {
        fill_animation_logo(V2_BAT_0_INDEX, fill_addr, dec_logo_addr, logo_addr,phical_screen);
    } else {
        int bg_index = V2_BAT_0_10_START_INDEX; //capacity > 0 && capacity < 10
        if (capacity >= 10 && capacity < 40) {
            bg_index = V2_BAT_10_40_START_INDEX;               
        } else if (capacity >= 40 && capacity < 80) {            
            bg_index = V2_BAT_40_80_START_INDEX;
        } else if (capacity >= 80 && capacity < 100) {      
            bg_index = V2_BAT_80_100_START_NDEX;
        }        
        fill_animation_logo(bg_index + charging_low_index, fill_addr, dec_logo_addr, logo_addr,phical_screen);    
        RECT_REGION_T tmp_rect = {(int)phical_screen.width * 4/10, 
                        (int) phical_screen.height * 1/6,
                        (int)phical_screen.width* 5/10,
                        (int)phical_screen.height*16/60};  
        unsigned short tmp_num_addr[(int)phical_screen.width * phical_screen.height/100]; //addr
                                     
        if (capacity >= 10) {
            LOG_ANIM("[show_animation_common: %s %d]tmp_rect left = %d, right = %d,top = %d,bottom = %d,\n",__FUNCTION__,__LINE__, 
                        tmp_rect.left,tmp_rect.right,tmp_rect.top,tmp_rect.bottom);                         
            fill_animation_dynamic(V2_NUM_START_0_INDEX + (capacity/10), tmp_rect, fill_addr, tmp_num_addr, logo_addr, phical_screen);
            tmp_rect.left += (int)phical_screen.width /10;
            tmp_rect.right += (int)phical_screen.width /10;
        }
        
        LOG_ANIM("[show_animation_common: %s %d]tmp_rect left = %d, right = %d,top = %d,bottom = %d,\n",__FUNCTION__,__LINE__, 
                tmp_rect.left,tmp_rect.right,tmp_rect.top,tmp_rect.bottom);                  
        fill_animation_dynamic(V2_NUM_START_0_INDEX + (capacity%10), tmp_rect, fill_addr, tmp_num_addr, logo_addr, phical_screen);                   
        
        tmp_rect.left += (int)phical_screen.width /10;
        tmp_rect.right += (int)phical_screen.width /10;

        LOG_ANIM("[show_animation_common: %s %d]tmp_rect left = %d, right = %d,top = %d,bottom = %d,\n",__FUNCTION__,__LINE__, 
                        tmp_rect.left,tmp_rect.right,tmp_rect.top,tmp_rect.bottom);                                         
        fill_animation_dynamic(V2_NUM_PERCENT_INDEX, tmp_rect, fill_addr, tmp_num_addr, logo_addr, phical_screen);  
        
    }
}

/*
 * Pump charging aniamtion 
 * index 39: 100%, 40~45 : animation logo, 46~55: number logo, 56: % logo
 *
 */
void fill_animation_battery_fast_charging(unsigned int capacity, void *fill_addr, void * dec_logo_addr, void * logo_addr, LCM_SCREEN_T phical_screen, int draw_anim_mode)
{
    int display_width = phical_screen.width;
    int display_height = phical_screen.height;
    int curr_left = 0;
    int num_width = 0;
    int num_height = 0;
    int top_margin_height = 0;
    if(draw_anim_mode == DRAW_ANIM_MODE_FB){
	if (0 == strncmp(MTK_LCM_PHYSICAL_ROTATION, "90", 2) ||
	     0 == strncmp(MTK_LCM_PHYSICAL_ROTATION, "270", 3)) {
	     display_width = phical_screen.height;
	     display_height = phical_screen.width;
	    }
	}
    num_width = LOGO_NUM_WIDTH(display_width);
    num_height = LOGO_NUM_HEIGHT(display_height);
    top_margin_height = LOGO_TOP_MARGIN(display_height);
    LOG_ANIM("[show_animation_common: %s %d]capacity : %d, num_width:%d, num_height:%d, top_margin_height:%d\n", __FUNCTION__,__LINE__, capacity, num_width, num_height, top_margin_height);

    charging_low_index >= 5? charging_low_index = 0:charging_low_index++;
    LOG_ANIM("[show_animation_common: %s %d]charging_low_index = %d\n",__FUNCTION__,__LINE__, charging_low_index);

    LOG_ANIM("[show_animation_common: %s %d]capacity : %d\n",__FUNCTION__,__LINE__, capacity);
    if (capacity <= 0) {
        return;
    } else if (capacity >= 100) {
        fill_animation_logo(FAST_CHARGING_BAT_100_INDEX, fill_addr, dec_logo_addr, logo_addr, phical_screen);
    } else {
        fill_animation_logo(FAST_CHARGING_BAT_START_0_INDEX + charging_low_index, fill_addr, dec_logo_addr, logo_addr, phical_screen);

        curr_left = (display_width - num_width * 2) >> 1;
        unsigned short tmp_num_addr[(int)phical_screen.width * phical_screen.height/50];
		
        if (capacity > 10) {
            curr_left = (display_width - num_width * 3) >> 1;
            RECT_REGION_T tmp_rect_1 = {curr_left, top_margin_height, curr_left + num_width, top_margin_height + num_height};
            LOG_ANIM("[show_animation_common: %s %d]capacity = %d, show 1nd num : %d\n",__FUNCTION__,__LINE__, capacity, capacity/10);
            fill_animation_dynamic(FAST_CHARGING_NUM_START_0_INDEX + (capacity/10), tmp_rect_1, fill_addr, tmp_num_addr, logo_addr, phical_screen);
            curr_left += num_width;
        }
        RECT_REGION_T tmp_rect_2 = {curr_left, top_margin_height, curr_left + num_width, top_margin_height + num_height};
        LOG_ANIM("[show_animation_common: %s %d]capacity = %d, show 2st num : %d\n",__FUNCTION__,__LINE__, capacity, capacity%10);
        fill_animation_dynamic(FAST_CHARGING_NUM_START_0_INDEX + (capacity%10), tmp_rect_2, fill_addr, tmp_num_addr, logo_addr, phical_screen);

        curr_left += num_width;
        RECT_REGION_T tmp_rect_3 = {curr_left, top_margin_height, curr_left + num_width, top_margin_height + num_height};
        LOG_ANIM("[show_animation_common: %s %d]show percent \n",__FUNCTION__,__LINE__);
        fill_animation_dynamic(FAST_CHARGING_NUM_PERCENT_INDEX, tmp_rect_3, fill_addr, tmp_num_addr, logo_addr, phical_screen);
    }
	
}

/*
 * Show charging animation by version
 *
 */
void fill_animation_battery_by_ver(unsigned int capacity,void *fill_addr, void * dec_logo_addr, void * logo_addr,
                        LCM_SCREEN_T phical_screen, int version)
{
    LOG_ANIM("[show_animation_common: %s %d]version : %d\n",__FUNCTION__,__LINE__, version);
    switch (version)
    {
        case VERION_OLD_ANIMATION:
            fill_animation_battery_old(capacity, fill_addr, dec_logo_addr, logo_addr, phical_screen);
            
            break;
        case VERION_NEW_ANIMATION:
            fill_animation_battery_new(capacity, fill_addr, dec_logo_addr, logo_addr, phical_screen);
            
            break;
        case VERION_WIRELESS_CHARGING_ANIMATION:
            fill_animation_battery_wireless_charging(capacity, fill_addr, dec_logo_addr, logo_addr, phical_screen);
            
            break;            
        default:
            fill_animation_battery_old(capacity, fill_addr, dec_logo_addr, logo_addr, phical_screen);
            
            break;   
    }                     
}                       

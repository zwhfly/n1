/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTRepeatElement.java
**
** Description:
**     Java IVTRepeatElement class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Represents a Repeat element within a Timeline effect in an IVT buffer.
 */
public class IVTRepeatElement extends IVTElement
{
    private int        repeatCount;
    private int        duration;

    /**
     * Initializes this IVT Repeat element.
     *
     * @param   time            Time in milliseconds of the IVT Repeat element
     *                          within the Timeline effect.
     * @param   repeatCount     Number of times to repeat the events occurring
     *                          between <code>time</code> and
     *                          <code>time + duration</code> within the Timeline
     *                          effect.
     * @param   duration        Length of the interval in milliseconds over
     *                          which to repeat events.
     */
    public IVTRepeatElement(int time, int repeatCount, int duration)
    {
        super(ImmVibeAPI.VIBE_ELEMTYPE_REPEAT, time);
        this.repeatCount = repeatCount;
        this.duration = duration;
    }

    /**
     * Sets the repeat count of this IVT Repeat element.
     *
     * @param   repeatCount     Number of times to repeat the events occurring
     *                          between <code>time</code> and
     *                          <code>time + duration</code> within the Timeline
     *                          effect.
     */
    public void setRepeatCount(int repeatCount)
    {
        this.repeatCount = repeatCount;
    }

    /**
     * Gets the repeat count of this IVT Repeat element.
     *
     * @return                  Number of times to repeat the events occurring
     *                          between <code>time</code> and
     *                          <code>time + duration</code> within the Timeline
     *                          effect.
     */
    public int getRepeatCount()
    {
        return repeatCount;
    }

    /**
     * Sets the duration of this IVT Repeat element.
     *
     * @param   duration        Length of the interval in milliseconds over
     *                          which to repeat events.
     */
    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    /**
     * Gets the duration of this IVT Repeat element.
     *
     * @return                  Length of the interval in milliseconds over
     *                          which to repeat events.
     */
    public int getDuration()
    {
        return duration;
    }

    public int[] getBuffer()
    {
        int[] retVal = new int[4];

        retVal[0] = getType();
        retVal[1] = getTime();
        retVal[2] = repeatCount;
        retVal[3] = duration;

        return retVal;
    }
}

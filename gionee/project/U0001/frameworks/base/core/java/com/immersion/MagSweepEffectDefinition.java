/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     MagSweepEffectDefinition.java
**
** Description:
**     Java MagSweepEffectDefinition class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Encapsulates the parameters of a MagSweep effect.
 */
public class MagSweepEffectDefinition
{
    private int duration;
    private int magnitude;
    private int style;
    private int attackTime;
    private int attackLevel;
    private int fadeTime;
    private int fadeLevel;
    private int actuatorIndex;

    /**
     * Initializes this MagSweep effect with the given parameters.
     *
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link ImmVibe#VIBE_TIME_INFINITE}.
     *                          For a finite duration, the effect duration goes
     *                          from zero to the value returned by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     * @param   style           Style of the effect. The effect style must be
     *                          one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_STRONG}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SHARP}</li>
     *                              <li>{@link ImmVibe#VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     Attack level of the effect. The attack level
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       Fade level of the effect. The fade level goes
     *                          from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public MagSweepEffectDefinition(int duration,
                                    int magnitude,
                                    int style,
                                    int attackTime,
                                    int attackLevel,
                                    int fadeTime,
                                    int fadeLevel,
                                    int actuatorIndex)
    {
        setDuration(duration);
        setMagnitude(magnitude);
        setStyle(style);
        setAttackTime(attackTime);
        setAttackLevel(attackLevel);
        setFadeTime(fadeTime);
        setFadeLevel(fadeLevel);
        setActuatorIndex(actuatorIndex);
    }

    /**
     * Sets the duration of this MagSweep effect definition.
     *
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link ImmVibe#VIBE_TIME_INFINITE}.
     *                          For a finite duration, the effect duration goes
     *                          from zero to the value returned by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     */
    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    /**
     * Gets the duration of this MagSweep effect definition.
     *
     * @return                  Duration of the effect in milliseconds. If
     *                          the effect duration is infinite, this method
     *                          returns
     *                          {@link ImmVibe#VIBE_TIME_INFINITE}.
     *                          For a finite duration, this method returns a
     *                          value from zero to the value returned by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     */
    public int getDuration()
    {
        return duration;
    }

    /**
     * Sets the magnitude of this MagSweep effect definition.
     *
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public void setMagnitude(int magnitude)
    {
        this.magnitude = magnitude;
    }

    /**
     * Gets the magnitude of this MagSweep effect definition.
     *
     * @return                  Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public int getMagnitude()
    {
        return magnitude;
    }

    /**
     * Sets the style of this MagSweep effect definition.
     *
     * @param   style           Style of the effect. The effect style must be
     *                          one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_STRONG}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SHARP}</li>
     *                              <li>{@link ImmVibe#VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     */
    public void setStyle(int style)
    {
        this.style = style;
    }

    /**
     * Gets the style of this MagSweep effect definition.
     *
     * @return                  Style of the effect. The effect style will be
     *                          one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_STRONG}</li>
     *                              <li>{@link ImmVibe#VIBE_STYLE_SHARP}</li>
     *                              <li>{@link ImmVibe#VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     */
    public int getStyle()
    {
        return style;
    }

    /**
     * Sets the attack time of this MagSweep effect definition.
     *
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     */
    public void setAttackTime(int attackTime)
    {
        this.attackTime = attackTime;
    }

    /**
     * Gets the attack time of this MagSweep effect definition.
     *
     * @return                  Attack time of the effect in milliseconds. The
     *                          attack time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     */
    public int getAttackTime()
    {
        return attackTime;
    }

    /**
     * Sets the attack level of this MagSweep effect definition.
     *
     * @param   attackLevel     Attack level of the effect. The attack level
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public void setAttackLevel(int attackLevel)
    {
        this.attackLevel = attackLevel;
    }

    /**
     * Gets the attack level of this MagSweep effect definition.
     *
     * @return                  Attack level of the effect. The attack level
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public int getAttackLevel()
    {
        return attackLevel;
    }

    /**
     * Sets the fade time of this MagSweep effect definition.
     *
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     */
    public void setFadeTime(int fadeTime)
    {
        this.fadeTime = fadeTime;
    }

    /**
     * Gets the fade time of this MagSweep effect definition.
     *
     * @return                  Fade time of the effect in milliseconds. The
     *                          fade time goes from zero to the value returned
     *                          by
     *                          {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                          for the
     *                          {@link ImmVibe#VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     */
    public int getFadeTime()
    {
        return fadeTime;
    }

    /**
     * Sets the fade level of this MagSweep effect definition.
     *
     * @param   fadeLevel       Fade level of the effect. The fade level goes
     *                          from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public void setFadeLevel(int fadeLevel)
    {
        this.fadeLevel = fadeLevel;
    }

    /**
     * Gets the fade level of this MagSweep effect definition.
     *
     * @return                  Fade level of the effect. The fade level goes
     *                          from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive.
     */
    public int getFadeLevel()
    {
        return fadeLevel;
    }

    /**
     * Sets the actuator index of this MagSweep effect definition.
     *
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public void setActuatorIndex(int actuatorIndex)
    {
        this.actuatorIndex = actuatorIndex;
    }

    /**
     * Gets the actuator index of this MagSweep effect definition.
     *
     * @return                  Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public int getActuatorIndex()
    {
        return actuatorIndex;
    }
}

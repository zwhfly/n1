/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTPeriodicElement.java
**
** Description:
**     Java IVTPeriodicElement class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Represents a Periodic effect within a Timeline effect in an IVT buffer.
 */
public class IVTPeriodicElement extends IVTElement
{
    private PeriodicEffectDefinition definition;

    /**
     * Initializes this IVT Periodic element.
     *
     * @param   time            Time in milliseconds of the IVT Periodic
     *                          element within the Timeline effect.
     * @param   definition      Definition of this IVT Periodic element.
     */
    public IVTPeriodicElement(int time, PeriodicEffectDefinition definition)
    {
        super(ImmVibeAPI.VIBE_ELEMTYPE_PERIODIC, time);
        this.definition = definition;
    }

    /**
     * Sets the definition of this IVT Periodic element.
     *
     * @param   definition      Definition of this IVT Periodic element.
     */
    public void setDefinition(PeriodicEffectDefinition definition)
    {
        this.definition = definition;
    }

    /**
     * Gets the definition of this IVT Periodic element.
     *
     * @return                  Definition of this IVT Periodic element.
     */
    public PeriodicEffectDefinition getDefinition()
    {
        return definition;
    }

    public int[] getBuffer()
    {
        int[] retVal = new int[11];

        retVal[0] = getType();
        retVal[1] = getTime();
        retVal[2] = definition.getDuration();
        retVal[3] = definition.getPeriod();
        retVal[4] = definition.getMagnitude();
        retVal[5] = definition.getStyleAndWaveType();
        retVal[6] = definition.getAttackTime();
        retVal[7] = definition.getAttackLevel();
        retVal[8] = definition.getFadeTime();
        retVal[9] = definition.getFadeLevel();
        retVal[10] = definition.getActuatorIndex();

        return retVal;
    }
}

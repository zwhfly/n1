/*
** =============================================================================
** Copyright (c) 2009-2014  Immersion Corporation.  All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**      ImmVibeAPI.java
**
** Description:
**      Java ImmVibeAPI interface declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Java interface to the C API. The methods of this interface are not intended
 * be directly used by applications. Applications should use higher-level
 * classes starting with the {@link Device} class. The methods of this interface
 * are used internally by the {@link Device} class and related classes.
 */
public interface ImmVibeAPI
{
    /*
    ** Device capability types used with getDeviceCapabilityBool,
    ** getDeviceCapabilityInt32, or getDeviceCapabilityString.
    */

    /**
     * Device capability type to get the device category.
     * Used with {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     * The return value will be one of
     *  <ul>
     *      <li>{@link #VIBE_DEVICECATEGORY_VIRTUAL}</li>
     *      <li>{@link #VIBE_DEVICECATEGORY_EMBEDDED}</li>
     *      <li>{@link #VIBE_DEVICECATEGORY_TETHERED}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_DEVICE_CATEGORY = 0;

    /**
     * Device capability type to get the maximum number of nested repeat bars
     * supported for Timeline effects. Any repeat bars nested beyond this level
     * will be played only once. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_NESTED_REPEATS = 1;

    /**
     * Device capability type to get the number of actuators present on the
     * device. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_NUM_ACTUATORS = 2;

    /**
     * Device capability type to get the acutator type. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     * The return value will be one of
     *  <ul>
     *      <li>{@link #VIBE_DEVACTUATORTYPE_BLDC}</li>
     *      <li>{@link #VIBE_DEVACTUATORTYPE_LRA}</li>
     *      <li>{@link #VIBE_DEVACTUATORTYPE_PIEZO}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_ACTUATOR_TYPE = 3;

    /**
     * Device capability type to get the number of effect slots present on the
     * device. The number of effect slots represents the maximum number of
     * simple effects that may play simultaneously. If an attempt is made to
     * play more than this number of effects at the same time, some of the
     * simple effects will not play. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_NUM_EFFECT_SLOTS = 4;

    /**
     * Device capability type to get the supported effect styles. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value will be a bitwise ORing of
     *  <ul>
     *      <li>{@link #VIBE_STYLE_SMOOTH_SUPPORT}</li>
     *      <li>{@link #VIBE_STYLE_STRONG_SUPPORT}</li>
     *      <li>{@link #VIBE_STYLE_SHARP_SUPPORT}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_SUPPORTED_STYLES = 5;

    /**
     * Device capability type to get the minimum period for Periodic effects.
     * Used with {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MIN_PERIOD = 6;

    /**
     * Device capability type to get the maximum period for Periodic effects.
     * Used with {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_PERIOD = 7;

    /**
     * Device capability type to get the maximum finite duration in milliseconds
     * for simple effects. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION = 8;

    /**
     * Device capability type to get the supported effect types. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value will be a bitwise ORing of
     *  <ul>
     *      <li>{@link #VIBE_MAGSWEEP_EFFECT_SUPPORT}</li>
     *      <li>{@link #VIBE_PERIODIC_EFFECT_SUPPORT}</li>
     *      <li>{@link #VIBE_TIMELINE_EFFECT_SUPPORT}</li>
     *      <li>{@link #VIBE_STREAMING_EFFECT_SUPPORT}</li>
     *      <li>{@link #VIBE_WAVEFORM_EFFECT_SUPPORT}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS = 9;

    /**
     * Device capability type to get the device name. Used with
     * {@link #getDeviceCapabilityString getDeviceCapabilityString}. The maximum
     * device name length will be {@link #VIBE_MAX_DEVICE_NAME_LENGTH}.
     */
    final public static int VIBE_DEVCAPTYPE_DEVICE_NAME = 10;

    /**
     * Device capability type to get the maximum attack time or fade time in
     * milliseconds for effect envelopes of simple effects. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME = 11;

    /**
     * Device capability type to get the API version number. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value format is Ox<i>MMNNSPBB</i>, where
     *  <ul>
     *      <li><i>MM</i> is the API major version number</li>
     *      <li><i>NN</i> is the minor version number</li>
     *      <li><i>SP</i> is the special build number (typically 0)</li>
     *      <li><i>BB</i> is the build number</li>
     *  </ul>
     * For example, for 0x034001F the version number is 3.4.31.0.
     */
    final public static int VIBE_DEVCAPTYPE_APIVERSIONNUMBER = 12;

    /**
     * Device capability type to get the maximum size in bytes for IVT files
     * that can be played on a tethered device. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_IVT_SIZE_TETHERED = 13;

    /**
     * @deprecated Device capability type to get the maximum size in bytes for
     * IVT files that can be played on a non-tethered device. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. As of
     * version 3.1, the Player dynamically allocates memory for IVT data;
     * therefore, this device capability type is deprecated but retained for
     * backward compatibility.
     */
    final public static int VIBE_DEVCAPTYPE_MAX_IVT_SIZE = 14;

    /**
     * Device capability type to get the API edition level. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value will be one of
     *  <ul>
     *      <li>{@link #VIBE_EDITION_3000}</li>
     *      <li>{@link #VIBE_EDITION_4000}</li>
     *      <li>{@link #VIBE_EDITION_5000}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_EDITION_LEVEL = 15;

    /**
     * Device capability type to get the supported wave types. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value will be a bitwise ORing of
     *  <ul>
     *      <li>{@link #VIBE_WAVETYPE_SQUARE_SUPPORT}</li>
     *      <li>{@link #VIBE_WAVETYPE_TRIANGLE_SUPPORT}</li>
     *      <li>{@link #VIBE_WAVETYPE_SINE_SUPPORT}</li>
     *      <li>{@link #VIBE_WAVETYPE_SAWTOOTHUP_SUPPORT}</li>
     *      <li>{@link #VIBE_WAVETYPE_SAWTOOTHDOWN_SUPPORT}</li>
     *  </ul>
     */
    final public static int VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES = 16;

    /**
     * Device capability type to get the handset index. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}. The return
     * value will always be zero on embedded devices. In Windows, where multiple
     * tethered handsets may be present, the return value indicates the handset
     * to which a device, or actuator belongs.
     */
    final public static int VIBE_DEVCAPTYPE_HANDSET_INDEX = 17;

    /**
     * Device capability type to get the supported features. Used with
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}.
     * The return value will be a bitwise ORing of
     *  <ul>
     *       <li>{@link #VIBE_FEATURE_PAUSE_RESUME_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_COMPOSITE_DEVICE_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_MULTIPLE_ACTUATOR_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_HIGH_DEFINITION_ACTUATOR_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_PLAY_APPEND_WAVEFORM_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_ENHANCED_ACTUATOR_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_ENHANCED_WAVEFORM_SUPPORT}</li>
     *       <li>{@link #VIBE_FEATURE_XIVT_SUPPORT}</li>
     *  </ul>
     */
    public static final int VIBE_DEVCAPTYPE_SUPPORTED_FEATURES = 18;

    /*
    ** Device property types used with getDevicePropertyBool,
    ** getDevicePropertyInt32, getDevicePropertyString, setDevicePropertyBool,
    ** setDevicePropertyInt32, setDevicePropertyString.
    */

    /**
     * Device property type to set the OEM licence key associated with a device
     * handle. Used with
     * {@link #setDevicePropertyString setDevicePropertyString}. This property
     * is write-only. It can be set to an OEM license key issued by Immersion in
     * order to use higher priority levels not normally available to
     * applications, or to set the master strength. On devices with API version
     * 3.2 or lower, applications must always set this property in order to
     * unlock the ability to play effects.
     */
    final public static int VIBE_DEVPROPTYPE_LICENSE_KEY = 0;

    /**
     * Device property type to get/set the priority of effects associated with a
     * device handle. Used with
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} and
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32}.
     * Different applications can use different priorities on the same device.
     * The priority determines which application's effects are played when
     * multiple applications attempt to play effects at the same time. The
     * default priority is {@link #VIBE_MIN_DEVICE_PRIORITY}. Priority values
     * range from {@link #VIBE_MIN_DEVICE_PRIORITY} to
     * {@link #VIBE_MAX_DEV_DEVICE_PRIORITY}, or from
     * {@link #VIBE_MIN_DEVICE_PRIORITY} to
     * {@link #VIBE_MAX_OEM_DEVICE_PRIORITY} after calling
     * {@link #setDevicePropertyString setDevicePropertyString} with the
     * {@link #VIBE_DEVPROPTYPE_LICENSE_KEY} device property type to set an OEM
     * license key.
     */
    final public static int VIBE_DEVPROPTYPE_PRIORITY = 1;

    /**
     * Device property type to enable/disable effects associated with a device
     * handle. When this property is set to true, the Player immediately stops
     * any playing effects and ignores subsequent requests to play effects. When
     * this property is false, the Player honors requests to play effects. Used
     * with {@link #getDevicePropertyBool getDevicePropertyBool} and
     * {@link #setDevicePropertyBool setDevicePropertyBool}.
     */
    final public static int VIBE_DEVPROPTYPE_DISABLE_EFFECTS = 2;

    /**
     * Device property type to get/set the overall strength for all effects
     * associated with a device handle. Used with
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} and
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32}. The strength
     * varies from {@link #VIBE_MIN_MAGNITUDE} (equivalent to mute) to
     * {@link #VIBE_MAX_MAGNITUDE} (full strength). The default value is
     * {@link #VIBE_MAX_MAGNITUDE}. The strength only applies to the device
     * instance passed to
     * {{@link #getDevicePropertyInt32 getDevicePropertyInt32} or
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32}, not to other
     * device instances held by the same or a different application. Modifying
     * the strength does not affect currently playing effects, only effects
     * played or modified after calling
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32} to use a new
     * strength.
     */
    final public static int VIBE_DEVPROPTYPE_STRENGTH = 3;

    /**
     * Device property type to get/set the overall master strength for all
     * effects associated with all device handles. Used with
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} and
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32}.
     * The master strength varies from {@link #VIBE_MIN_MAGNITUDE} (equivalent
     * to mute) to {@link #VIBE_MAX_MAGNITUDE} (full strength). The default
     * value is {@link #VIBE_MAX_MAGNITUDE}. The master strength applies to all
     * effects on the device, including effects from other applications.
     * Modifying the master strength immediately affects currently playing
     * effects and subsequently played or modified effects. Before changing the
     * master strength, applications must call
     * {@link #setDevicePropertyString setDevicePropertyString} with the
     * {@link #VIBE_DEVPROPTYPE_LICENSE_KEY} property to set an OEM license key,
     * and {@link #setDevicePropertyInt32 setDevicePropertyInt32} with the
     * {@link #VIBE_DEVPROPTYPE_PRIORITY} device property type to associate the
     * maximum OEM priority ({@link #VIBE_MAX_OEM_DEVICE_PRIORITY}) with the
     * device handle.
     */
    final public static int VIBE_DEVPROPTYPE_MASTERSTRENGTH = 4;


    /*
    ** Device categories returned by getDeviceCapabilityInt32 for the
    ** VIBE_DEVCAPTYPE_DEVICE_CATEGORY device capability.
    */

    /**
     * @deprecated IFC device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_IFC = 0;

    /**
     * @deprecated Impulse device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_IMPULSE = 1;

    /**
     * Virtual device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_VIRTUAL = 2;

    /**
     * Embedded device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_EMBEDDED = 3;

    /**
     * Tethered device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_TETHERED = 4;

    /**
     * @deprecated Immersion USB device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_IMMERSION_USB = 5;

    /**
     * Composite device category. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_CATEGORY} device capability.
     */
    final public static int VIBE_DEVICECATEGORY_COMPOSITE = 6;


    /*
    ** Actuator types returned by getDeviceCapabilityInt32 for the
    * VIBE_DEVCAPTYPE_ACTUATOR_TYPE device capability.
    */

    /**
     * Eccentric Rotating Mass (ERM) actuator type. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_ACTUATOR_TYPE} device capability.
     */
    final public static int VIBE_DEVACTUATORTYPE_ERM = 0;

    /**
     * Bush-Less Direct Current (BLDC) actuator type. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_ACTUATOR_TYPE} device capability.
     */
    final public static int VIBE_DEVACTUATORTYPE_BLDC = 1;

    /**
     * Linear Resonant (LR) actuator type. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_ACTUATOR_TYPE} device capability.
     */
    final public static int VIBE_DEVACTUATORTYPE_LRA = 2;

    /**
     * Piezo-electric actuator type. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_ACTUATOR_TYPE} device capability.
     */
    final public static int VIBE_DEVACTUATORTYPE_PIEZO = 4;

    /**
     * @deprecated Piezo-electric actuator type. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_ACTUATOR_TYPE} device capability. As of version
     * 3.4.52, this value has been replaced by
     * {@link #VIBE_DEVACTUATORTYPE_PIEZO}.
     */
    final public static int VIBE_DEVACTUATORTYPE_PIEZO_WAVE = 4;


    /**
     * Effect style mask. Effect styles are 4 bits and may be bitwise ORed with
     * other flags.
     */
    final public static int VIBE_STYLE_MASK = 0x0F;


    /*
    ** Effect styles used with playMagSweepEffect,
    ** playPeriodicEffect, modifyPlayingMagSweepEffect, and
    ** modifyPlayingPeriodicEffect.
    */

    /**
     * Smooth effect style. Used with
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_STYLE_SMOOTH = 0;

    /**
     * Strong effect style. Used with
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_STYLE_STRONG = 1;

    /**
     * Sharp effect style. Used with
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_STYLE_SHARP = 2;

    /**
     * Default effect style. Used with
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_DEFAULT_STYLE = VIBE_STYLE_STRONG;


    /**
     * Effect style support mask. Effect style support bit flags occupy 16 bits
     * and may be bitwise ORed with other flags.
     */
    final public static int VIBE_STYLE_SUPPORT_MASK = 0x0000FFFF;


    /*
    ** Effect style support bit flags returned by getDeviceCapabilityInt32 for
    ** the VIBE_DEVCAPTYPE_SUPPORTED_STYLES device capability type.
    */

    /**
     * Smooth effect style support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_STYLES} device capability type.
     */
    final public static int VIBE_STYLE_SMOOTH_SUPPORT = (1 << VIBE_STYLE_SMOOTH);

    /**
     * Strong effect style support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_STYLES} device capability type.
     */
    final public static int VIBE_STYLE_STRONG_SUPPORT = (1 << VIBE_STYLE_STRONG);

    /**
     * Sharp effect style support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_STYLES} device capability type.
     */
    final public static int VIBE_STYLE_SHARP_SUPPORT = (1 << VIBE_STYLE_SHARP);


    /**
     * Periodic effect wave type shift. Periodic effect wave types are 4 bits
     * and may be bitwise ORed with other flags.
     */
    final public static int VIBE_WAVETYPE_SHIFT = 4;

    /**
     * Periodic effect wave type mask. Periodic effect wave types are 4 bits
     * and may be bitwise ORed with other flags.
     */
    final public static int VIBE_WAVETYPE_MASK = 0xF0;


    /*
    ** Periodic effect wave types used with playPeriodicEffect and
    ** modifyPlayingPeriodicEffect.
    */

    /**
     * Square wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_WAVETYPE_SQUARE = (1 << VIBE_WAVETYPE_SHIFT);

    /**
     * Triangle wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_WAVETYPE_TRIANGLE = (2 << VIBE_WAVETYPE_SHIFT);

    /**
     * Sine wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_WAVETYPE_SINE = (3 << VIBE_WAVETYPE_SHIFT);

    /**
     * Sawtooth up wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_WAVETYPE_SAWTOOTHUP = (4 << VIBE_WAVETYPE_SHIFT);

    /**
     * Sawtooth down wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_WAVETYPE_SAWTOOTHDOWN = (5 << VIBE_WAVETYPE_SHIFT);

    /**
     * Default wave type. Used with
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_DEFAULT_WAVETYPE = VIBE_WAVETYPE_SQUARE;


    /**
     * Wave type support mask. Effect wave type support bit flags occupy
     * 16 bits and may be bitwise ORed with other flags.
     */
    final public static int VIBE_WAVETYPE_SUPPORT_MASK = 0xFFFF0000;


    /*
    ** Wave type support bit flags returned by getDeviceCapabilityInt32 for the
    ** VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES device capability.
    */

    /**
     * Square wave type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES} device capability type.
     */
    final public static int VIBE_WAVETYPE_SQUARE_SUPPORT = (0x10000 << (VIBE_WAVETYPE_SQUARE >> VIBE_WAVETYPE_SHIFT));

    /**
     * Triangle wave type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES} device capability type.
     */
    final public static int VIBE_WAVETYPE_TRIANGLE_SUPPORT = (0x10000 << (VIBE_WAVETYPE_TRIANGLE >> VIBE_WAVETYPE_SHIFT));

    /**
     * Sine wave type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES} device capability type.
     */
    final public static int VIBE_WAVETYPE_SINE_SUPPORT = (0x10000 << (VIBE_WAVETYPE_SINE >> VIBE_WAVETYPE_SHIFT));

    /**
     * Sawtooth up wave type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES} device capability type.
     */
    final public static int VIBE_WAVETYPE_SAWTOOTHUP_SUPPORT = (0x10000 << (VIBE_WAVETYPE_SAWTOOTHUP >> VIBE_WAVETYPE_SHIFT));

    /**
     * Sawtooth down wave type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_WAVE_TYPES} device capability type.
     */
    final public static int VIBE_WAVETYPE_SAWTOOTHDOWN_SUPPORT = (0x10000 << (VIBE_WAVETYPE_SAWTOOTHDOWN >> VIBE_WAVETYPE_SHIFT));


    /*
    ** Effect types returned by getIVTEffectType.
    */

    /**
     * Periodic effect type. Returned by
     * {@link #getIVTEffectType getIVTEffectType}.
     */
    final public static int VIBE_EFFECT_TYPE_PERIODIC = 0;

    /**
     * MagSweep effect type. Returned by
     * {@link #getIVTEffectType getIVTEffectType}.
     */
    final public static int VIBE_EFFECT_TYPE_MAGSWEEP = 1;

    /**
     * Timeline effect type. Returned by
     * {@link #getIVTEffectType getIVTEffectType}.
     */
    final public static int VIBE_EFFECT_TYPE_TIMELINE = 2;

    /**
     * Streaming effect type.
     */
    final public static int VIBE_EFFECT_TYPE_STREAMING = 3;

    /**
     * Waveform effect type. Returned by
     * {@link #getIVTEffectType getIVTEffectType}.
     */
    final public static int VIBE_EFFECT_TYPE_WAVEFORM = 4;

    /**
     * Interpolated effect type. Returned by
     * {@link #getIVTEffectType getIVTEffectType}.
     */
	final public static int VIBE_EFFECT_TYPE_INTERPOLATED = 5;

    /*
    ** Effect type support bit flags returned by getDeviceCapabilityInt32 for
    *  the ** VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS device capability type.
    */

    /**
     * Periodic effect type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS} device capability type.
     */
    final public static int VIBE_PERIODIC_EFFECT_SUPPORT = (1 << VIBE_EFFECT_TYPE_PERIODIC);

    /**
     * MagSweep effect type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS} device capability type.
     */
    final public static int VIBE_MAGSWEEP_EFFECT_SUPPORT = (1 << VIBE_EFFECT_TYPE_MAGSWEEP);

    /**
     * Timeline effect type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS} device capability type.
     */
    final public static int VIBE_TIMELINE_EFFECT_SUPPORT = (1 << VIBE_EFFECT_TYPE_TIMELINE);

    /**
     * Streaming effect type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS} device capability type.
     */
    final public static int VIBE_STREAMING_EFFECT_SUPPORT = (1 << VIBE_EFFECT_TYPE_STREAMING);

    /**
     * Waveform effect type support bit flag. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_EFFECTS} device capability type.
     */
    final public static int VIBE_WAVEFORM_EFFECT_SUPPORT = (1 << VIBE_EFFECT_TYPE_WAVEFORM);


    /*
    ** API edition levels returned by getDeviceCapabilityInt32 for the
    ** VIBE_DEVCAPTYPE_EDITION_LEVEL device capability.
    */

    /**
     * 3000 series edition level. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     */
    final public static int VIBE_EDITION_3000 = 3000;

    /**
     * 4000 series edition level. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     */
    final public static int VIBE_EDITION_4000 = 4000;

    /**
     * 5000 series edition level. Returned by
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} for the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     */
    final public static int VIBE_EDITION_5000 = 5000;


    /*
    ** Device priority levels returned by getDevicePropertyInt32 or used with
    ** setDevicePropertyInt32 for the VIBE_DEVPROPTYPE_PRIORITY device property.
    */

    /**
     * Minimum device priority. Returned by
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} or used with
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32} for the
     * {@link #VIBE_DEVPROPTYPE_PRIORITY} device property type.
     */
    final public static int VIBE_MIN_DEVICE_PRIORITY = 0x0;

    /**
     * Maximum device priority for 3rd-party applications. Returned by
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} or used with
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32} for the
     * {@link #VIBE_DEVPROPTYPE_PRIORITY} device property type.
     */
    final public static int VIBE_MAX_DEV_DEVICE_PRIORITY = 0x7;

    /**
     * Maximum device priority for OEM applications. Returned by
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} or used with
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32} for the
     * {@link #VIBE_DEVPROPTYPE_PRIORITY} device property type. This priority
     * level can be set only after calling
     * {@link #setDevicePropertyString setDevicePropertyString} with the
     * {@link #VIBE_DEVPROPTYPE_LICENSE_KEY} device property type to set an OEM
     * license key.
     */
    final public static int VIBE_MAX_OEM_DEVICE_PRIORITY = 0xF;

    /**
     * Default device priority. Returned by
     * {@link #getDevicePropertyInt32 getDevicePropertyInt32} or used with
     * {@link #setDevicePropertyInt32 setDevicePropertyInt32} for the
     * {@link #VIBE_DEVPROPTYPE_PRIORITY} device property type.
     */
    final public static int VIBE_DEVPRIORITY_DEFAULT = VIBE_MIN_DEVICE_PRIORITY;

    /**
     * @deprecated This value has been replaced by
     * {@link #VIBE_MAX_DEV_DEVICE_PRIORITY} and {@link #VIBE_MAX_OEM_DEVICE_PRIORITY}
     * to be used by third-party developers and device manufacturers, respectively.
     */
    final public static int VIBE_MAX_DEVICE_PRIORITY = VIBE_MAX_OEM_DEVICE_PRIORITY;

    /*
    ** Device states returned by getDeviceState.
    */

    /**
     * Device state indicating that the device is attached to the system.
     * Returned by {@link #getDeviceState getDeviceState}.
     */
    final public static int VIBE_DEVICESTATE_ATTACHED = (1 << 0);

    /**
     * Device state indicating that the device is busy playing effects.
     * Returned by {@link #getDeviceState getDeviceState}.
     */
    final public static int VIBE_DEVICESTATE_BUSY = (1 << 1);


    /*
    ** Effect states returned by getEffectState.
    */

    /**
     * Effect state indicating that the effect is not playing and not paused.
     * Returned by {@link #getEffectState getEffectState}.
     */
    final public static int VIBE_EFFECT_STATE_NOT_PLAYING = 0;

    /**
     * Effect state indicating that the effect is playing. Returned by
     * {@link #getEffectState getEffectState}.
     */
    final public static int VIBE_EFFECT_STATE_PLAYING = 1;

    /**
     * Effect state indicating that the effect is paused. Returned by
     * {@link #getEffectState getEffectState}.
     */
    final public static int VIBE_EFFECT_STATE_PAUSED = 2;


    /*
    ** IVT element types.
    */

    /**
     * Periodic effect element of a Timeline effect.
     */
    final public static int VIBE_ELEMTYPE_PERIODIC = 0;

    /**
     * MagSweep effect element of a Timeline effect.
     */
    final public static int VIBE_ELEMTYPE_MAGSWEEP = 1;

    /**
     * Repeat Repeat element of a Timeline effect.
     */
    final public static int VIBE_ELEMTYPE_REPEAT = 2;

    /**
     * Waveform effect element of a Timeline effect.
     */
    final public static int VIBE_ELEMTYPE_WAVEFORM = 3;


    /*
    ** Effect indexes in the API's built-in IVT file.
    */

    /**
     * Effect index for the short-on-short-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a short
     * vibration followed by a short rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_SHORT_ON_SHORT_OFF = 0;

    /**
     * Effect index for the short-on-medium-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a short
     * vibration followed by a medium rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_SHORT_ON_MEDIUM_OFF = 1;

    /**
     * Effect index for the short-on-long-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a short
     * vibration followed by a long rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_SHORT_ON_LONG_OFF = 2;

    /**
     * Effect index for the medium-on-short-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a medium
     * vibration followed by a short rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_MEDIUM_ON_SHORT_OFF = 3;

    /**
     * Effect index for the medium-on-medium-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a medium
     * vibration followed by a medium rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_MEDIUM_ON_MEDIUM_OFF = 4;

    /**
     * Effect index for the medium-on-long-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a medium
     * vibration followed by a long rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_MEDIUM_ON_LONG_OFF = 5;

    /**
     * Effect index for the long-on-short-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a long
     * vibration followed by a short rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_LONG_ON_SHORT_OFF = 6;

    /**
     * Effect index for the long-on-medium-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a long
     * vibration followed by a medium rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_LONG_ON_MEDIUM_OFF = 7;

    /**
     * Effect index for the long-on-long-off effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a long
     * vibration followed by a long rest, repeated indefinitely.
     */
    final public static int VIBE_BUILTINEFFECT_LONG_ON_LONG_OFF = 8;

    /**
     * Effect index for the short effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a short
     * vibration once.
     */
    final public static int VIBE_BUILTINEFFECT_SHORT = 9;

    /**
     * Effect index for the medium effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a medium
     * vibration once.
     */
    final public static int VIBE_BUILTINEFFECT_MEDIUM = 10;

    /**
     * Effect index for the long effect in the API's built-in IVT
     * file. Used with {@link #playIVTEffect playIVTEffect} when playing effects
     * from the IVT file returned by
     * {@link #getBuiltInEffects getBuiltInEffects}. This effect plays a long
     * vibration once.
     */
    final public static int VIBE_BUILTINEFFECT_LONG = 11;


    /*
    ** Indices.
    */

    /**
     * Invalid index. Used to initialize device and effect indices, for example.
     */
    final public static int VIBE_INVALID_INDEX = -1;


    /*
    ** Handles.
    */

    /**
     * Invalid effect handle. Used to initialize an effect handle, for example.
     */
    final public static int VIBE_INVALID_EFFECT_HANDLE_VALUE = -1;

    /**
     * Invalid device handle. Used to initialize a device handle, for example.
     */
    final public static int VIBE_INVALID_DEVICE_HANDLE_VALUE = -1;


    /*
    ** Force magnitudes.
    */

    /**
     * Maximum force magnitude. Used, for example, to play an effect with maximum force when calling
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_MAX_MAGNITUDE = 10000;

    /**
     * Minimum force magnitude.
     */
    final public static int VIBE_MIN_MAGNITUDE = 0;


    /*
    ** Temporal constants.
    */

    /**
     * Infinite time. Used, for example, to play an effect of indefinite duration when calling
     *  <ul>
     *      <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingMagSweepEffect modifyPlayingMagSweepEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_TIME_INFINITE = Integer.MAX_VALUE;

    /**
     * Microsecond period bit flag. Used to specify the period in microseconds when calling
     *  <ul>
     *      <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *      <li>{@link #modifyPlayingPeriodicEffect modifyPlayingPeriodicEffect}</li>
     *  </ul>
     */
    final public static int VIBE_PERIOD_RESOLUTION_MICROSECOND = 0x80000000;

    /**
     * Infinite repeat count. Used with
     * {@link #playIVTEffectRepeat playIVTEffectRepeat}.
     */
    final public static int VIBE_REPEAT_COUNT_INFINITE = 255;


    /*
    ** String lengths.
    */

    /**
     * Maximum device name length. Device names returned by
     * {@link #getDeviceCapabilityString getDeviceCapabilityString} for the
     * {@link #VIBE_DEVCAPTYPE_DEVICE_NAME} device capability type will not
     * exceed this length.
     */
    final public static int VIBE_MAX_DEVICE_NAME_LENGTH = 64;

    /**
     * Maximum effect name length. Effect names returned by
     * {@link #getIVTEffectName getIVTEffectName} will not exceed this length.
     */
    final public static int VIBE_MAX_EFFECT_NAME_LENGTH = 128;

    /**
     * Maximum device capability string length. String device capabilities
     * returned by {@link #getDeviceCapabilityString getDeviceCapabilityString}
     * will not exceed this length.
     */
    final public static int VIBE_MAX_CAPABILITY_STRING_LENGTH = 64;

    /**
     * Maximum device property string length. String device properties
     * returned by {@link #getDevicePropertyString getDevicePropertyString} or
     * set with {@link #setDevicePropertyString setDevicePropertyString} will
     * not exceed this length.
     */
    final public static int VIBE_MAX_PROPERTY_STRING_LENGTH = 64;


    /*
    ** Miscellaneous constants.
    */

    /**
     * Maximum streaming sample size. Used with
     * {@link #playStreamingSample playStreamingSample}.
     */
    final public static int VIBE_MAX_STREAMING_SAMPLE_SIZE = 255;

    /**
     * Maximum number of actuators that can be supported by a composite device.
     * Used with {@link #openCompositeDevice openCompositeDevice}.
     */
    final public static int VIBE_MAX_LOGICAL_DEVICE_COUNT = 16;


    /*
     ** Return status codes.
     */

    /**
     * No error.
     */
    final public static int VIBE_S_SUCCESS = 0;

    /**
     * False.
     */
    final public static int VIBE_S_FALSE = 0;

    /**
     * True.
     */
    final public static int VIBE_S_TRUE = 1;

    /**
     * The effect is not playing.
     */
    final public static int VIBE_W_NOT_PLAYING = 1;

    /**
     * The device priority is lower than that of currently playing effects
     * belonging to another device instance.
     */
    final public static int VIBE_W_INSUFFICIENT_PRIORITY = 2;

    /**
     * Effects playing on this device have been disabled.
     */
    final public static int VIBE_W_EFFECTS_DISABLED = 3;

    /**
     * The effect is not paused.
     */
    final public static int VIBE_W_NOT_PAUSED = 4;

    /**
     * The TouchSense Player API has already been initialized. This status code
     * is not returned by API functions.
     */
    final public static int VIBE_E_ALREADY_INITIALIZED = -1;

    /**
     * The TouchSense Player API has not been initialized.
     */
    final public static int VIBE_E_NOT_INITIALIZED = -2;

    /**
     * One or more parameters passed to the TouchSense Player API function call
     * are invalid.
     */
    final public static int VIBE_E_INVALID_ARGUMENT = -3;

    /**
     * Generic error.
     */
    final public static int VIBE_E_FAIL = -4;

    /**
     * The effect type is incompatible with the TouchSense Player API function
     * call.
     */
    final public static int VIBE_E_INCOMPATIBLE_EFFECT_TYPE = -5;

    /**
     * The capability type is incompatible with the TouchSense Player API
     * function call.
     */
    final public static int VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE = -6;

    /**
     * The property type is incompatible with the TouchSense Player API function
     * call.
     */
    final public static int VIBE_E_INCOMPATIBLE_PROPERTY_TYPE = -7;

    /**
     * Access to the instance of the device is locked until a valid license
     * key is provided. Use Device.setPropertyString to set the license key.
     * This return status code is obsolete as of TouchSense Player API version
     * 3.3.
     */
    final public static int VIBE_E_DEVICE_NEEDS_LICENSE = -8;

    /**
     * The TouchSense Player API function cannot allocate memory to complete the
     * request.
     */
    final public static int VIBE_E_NOT_ENOUGH_MEMORY = -9;

    /**
     * The TouchSense Player Service is not running. Re-install the TouchSense
     * Player API to restore the default settings. This is available in the
     * Windows prototyping environment only.
     */
    final public static int VIBE_E_SERVICE_NOT_RUNNING = -10;

    /**
     * Not enough priority to achieve the request (insufficient license key priority).
     */
    final public static int VIBE_E_INSUFFICIENT_PRIORITY = -11;

    /**
     * The TouchSense Player Service is busy and could not complete the
     * requested function call. This is available in the Windows prototyping
     * environment only.
     */
    final public static int VIBE_E_SERVICE_BUSY = -12;

    /**
     * The TouchSense Player API function is not supported in the edition level
     * of the TouchSense Player. For example, certain functions are available
     * only on the TouchSense Player - 4000 Series or TouchSense Player - 5000
     * Series, and not on the TouchSense Player - 3000 Series.
     */
    final public static int VIBE_E_NOT_SUPPORTED = -13;

    /**
     ** featuresupport Feature Support Flags
     **
     **   Feature support bit flags returned by
     **   {@link #getCapabilityInt32} for the
     **   {@link #VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability.
     */

    /**
     * Effect pause-resume feature support flag.
     */
    final public static int VIBE_FEATURE_PAUSE_RESUME_SUPPORT = 0x01;

    /**
     * Composite device feature support flag.
     */
    final public static int VIBE_FEATURE_COMPOSITE_DEVICE_SUPPORT = 0x02;

    /**
     * Multiple actuator support flag.
     */
    final public static int VIBE_FEATURE_MULTIPLE_ACTUATOR_SUPPORT  = 0x04;

    /**
     * High-definition actuator support flag.
     */
    final public static int VIBE_FEATURE_HIGH_DEFINITION_ACTUATOR_SUPPORT = 0x08;

    /**
     * Append waveform feature support flag.
    */
    final public static int VIBE_FEATURE_PLAY_APPEND_WAVEFORM_SUPPORT = 0x10;

    /**
     * Enhanced actuator feature support flag.
     */
    final public static int VIBE_FEATURE_ENHANCED_ACTUATOR_SUPPORT = 0x20;

    /**
     * Enhanced waveform feature support flag.
     */
    final public static int VIBE_FEATURE_ENHANCED_WAVEFORM_SUPPORT = 0x40;

    /**
     * X-IVT feature support flag.
     */
    final public static int VIBE_FEATURE_XIVT_SUPPORT = 0x80;

    /**
     * Maximum interpolant value.
     *       Used with {@link Device#playIVTInterpolatedEffect},
     *       {@link EffectHandle#modifyPlayingInterpolatedEffectInterpolant} to
     *       interpolate at the last key frame.
     */
    final public static int VIBE_MAX_INTERPOLANT = 10000;

    /**
     * Enhanced Waveform Data Formats
     *       Used with {@link #playEnhancedWaveformEffect},
     *       {@link #appendEnhancedWaveformEffect},
     *       and {@link #replaceEnhancedWaveformEffect}.
     */

    /**
     * 8-bit shift-128 PCM data.
     */
    final public static int VIBE_WAVEFORMAT_PCM8 = 0;

    /**
     * 16-bit two's complement PCM data.
     */
    public static final int  VIBE_WAVEFORMAT_PCM16 = 1;

    /**
     * 8-bit unsigned waveform data.
     */
    public static final int VIBE_WAVEFORMAT_U8 = 2;

    /*
     ** Secure mode
     */
    public static final int VIBE_SECURITY_MODE_NONE = 0;
    public static final int VIBE_SECURITY_MODE_AES128CTR = 1;

    /**
      ** Initial waveform data size
      */
    public static final int VIBE_MIN_WAVE_DATA_SIZE = 16;

    /*
    ** API
    */

    /**
     * Initializes the API.
     * <p>
     * {@link #initialize initialize} must be called before other API functions.
     * Every call to {@link #initialize initialize} should have a matching call
     * to {@link #terminate terminate}. Normally, client applications call
     * {@link #initialize initialize} during application startup and call
     * {@link #terminate terminate} during application shutdown.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The Java API version is incompatible
     *                                  with (greater than) the underlying C API
     *                                  version.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error initializing the API.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_NOT_RUNNING"</dt>
     *                              <dd>The Player Service is not running.
     *                                  You should re-install the API to restore
     *                                  the default settings.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void initialize();

    /**
     * Closes down the API.
     * <p>
     * {@link #terminate terminate} must be called after other API functions.
     * Every call to {@link #initialize initialize} should have a matching call
     * to {@link #terminate terminate}. Normally, client applications call
     * {@link #initialize initialize} during application startup and call
     * {@link #terminate terminate} during application shutdown.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error closing down the API.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void terminate();


    /*
    ** Devices.
    */

    /**
     * Gets the number of available devices.
     * <p>
     * On platforms such as Windows that support a dynamic device configuration,
     * the list of supported devices is determined during the call to
     * {@link #initialize initialize} by enumerating the available device
     * drivers. The list of supported devices includes attached devices but may
     * also include devices that are not physically connected.
     * {@link #getDeviceCount getDeviceCount} returns at least one device, a
     * virtual generic device, if no physical device is present.
     * <p>
     * On embedded systems where the device configuration is static, a virtual
     * generic device is not provided. The device count corresponds with the
     * actual number of devices that are present on the embedded system.
     *
     * @return                  Number of available devices that are supported
     *                          by the API.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device count.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getDeviceCount();

    /**
     * Gets the status bits of a device.
     *
     * @param   deviceIndex     Index of the device for which to get the status
     *                          bits. The index of the device must be greater
     *                          than or equal to zero and less than the number
     *                          of devices returned by
     *                          {@link #getDeviceCount getDeviceCount}.
     * @return                  Status bits of the device, bitwise ORing of
    *                           <ul>
     *                              <li>{@link #VIBE_DEVICESTATE_ATTACHED}</li>
     *                              <li>{@link #VIBE_DEVICESTATE_BUSY}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getDeviceCount getDeviceCount}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device state.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getDeviceState(int deviceIndex);

    /**
     * Gets a boolean capability of a device.
     * <p>
     * This function is provided for future use. The devices supported by the
     * current version of the API do not have boolean capabilities.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getDeviceCount getDeviceCount}.
     * @param   devCapType      Device capability type of the boolean capability
     *                          to get.
     * @return                  Requested boolean capability of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getDeviceCount getDeviceCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a boolean capability of
     *                                  the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean getDeviceCapabilityBool(int deviceIndex, int devCapType);

    /**
     * Gets a 32-bit integer capability of a device.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getDeviceCount getDeviceCount}.
     * @param   devCapType      Device capability type of the 32-bit integer
     *                          capability to get.
     * @return                  Requested 32-bit integer capability of the
     *                          device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getDeviceCount getDeviceCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a 32-bit integer
     *                                  capability of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getDeviceCapabilityInt32(int deviceIndex, int devCapType);

    /**
     * Gets a string capability of a device.
     *
     * @param   deviceIndex     Index of the device for which to get a boolean
     *                          capability. The index of the device must be
     *                          greater than or equal to zero and less than the
     *                          number of devices returned by
     *                          {@link #getDeviceCount getDeviceCount}.
     * @param   devCapType      Device capability type of the string capability
     *                          to get.
     * @return                  Requested string capability of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getDeviceCount getDeviceCount}.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_CAPABILITY_TYPE"</dt>
     *                              <dd>The <code>devCapType</code> parameter
     *                                  does not specify a string capability of
     *                                  the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public String getDeviceCapabilityString(int deviceIndex, int devCapType);

    /**
     * Opens and initializes a device representing a specified actuator.
     * <p>
     * Effects played on this device can play only on the specified actuator.
     * <p>
     * Every call to {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} should have a matching
     * call to {@link #closeDevice closeDevice} for the same device. Normally,
     * client applications call {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} during application
     * startup and call {@link #closeDevice closeDevice} during application
     * shutdown.
     *
     * @param   deviceIndex     Index of the device to open. The index of the
     *                          device must be greater than or equal to zero and
     *                          less than the number of devices returnd by
     *                          {@link #getDeviceCount getDeviceCount}.
     * @return                  Handle to the open device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceIndex</code> parameter
     *                                  is negative, or greater than or equal to
     *                                  the number of devices returned by
     *                                  {@link #getDeviceCount getDeviceCount}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error opening the device.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int openDevice(int deviceIndex);

    /**
     * Opens and initializes a composite device that supports playing different
     * effects simultaneously on different actuators.
     * <p>
     * Effects may specify the index of an actuator on which to play the
     * effect. When played on a composite device, the effects are rendered on
     * the actuator corresponding to the specified actuator index.
     * <p>
     * Every call to {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} should have a matching
     * call to {@link #closeDevice closeDevice} for the same device. Normally,
     * client applications call {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} during application
     * startup and call {@link #closeDevice closeDevice} during application
     * shutdown.
     *
     * @param   numDevice       Number of actuators to use. The
     *                          <code>numDevice</code> parameter must be
     *                          greater than zero and less than or equal to
     *                          {@link #VIBE_MAX_LOGICAL_DEVICE_COUNT}. To use
     *                          all available actuators, set
     *                          <code>numDevice</code> to the number returned
     *                          by {@link #getDeviceCount getDeviceCount}.
     * @return                  Handle to the open composite device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>numDevice</code> parameter is
     *                                  less than or equal to zero, or greater
     *                                  than
     *                                  {@link #VIBE_MAX_LOGICAL_DEVICE_COUNT}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error opening the composite device.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>The tethered handset does not support
     *                                  this function (Windows-only).</dd>
     *                          </dl>
     */
    public int openCompositeDevice(int numDevice);

    /**
     * Closes a device previously opened with a successful call to
     * {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice}.
     * <p>
     * Every call to {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} should have a matching
     * call to {@link #closeDevice closeDevice} for the same device. Normally,
     * client applications call {@link #openDevice openDevice} or
     * {@link #openCompositeDevice openCompositeDevice} during application
     * startup and call {@link #closeDevice closeDevice} during application
     * shutdown.
     *
     * @param   deviceHandle    Handle to the device to close. The handle to the
     *                          device must have been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error closing the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void closeDevice(int deviceHandle);

    /**
     * Gets a boolean property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to get a boolean
     *                          property. The handle to the device must have
     *                          been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the boolean property to get.
     * @return                  Requested boolean property of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a boolean property of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public boolean getDevicePropertyBool(int deviceHandle, int devPropType);

    /**
     * Sets a boolean property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to set a boolean
     *                          property. The handle to the device must have
     *                          been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the boolean property to set.
     * @param   devPropValue    Value of the boolean property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a boolean property of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setDevicePropertyBool(int deviceHandle,
                                      int devPropType,
                                      boolean devPropValue);

    /**
     * Gets a 32-bit integer property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to get a 32-bit
     *                          integer property. The handle to the device must
     *                          have been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the 32-bit integer property to
     *                          get.
     * @return                  Requested 32-bit integer property of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a 32-bit integer property of the
     *                                  device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getDevicePropertyInt32(int deviceHandle, int devPropType);

    /**
     * Sets a 32-bit integer property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to set a 32-bit
     *                          integer property. The handle to the device must
     *                          have been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the 32-bit integer  property to
     *                          set.
     * @param   devPropValue    Value of the 32-bit integer property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a 32-bit integer property of the
     *                                  device.</dd>
     *                              <dt>"VIBE_E_INSUFFICIENT_PRIORITY"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies the
     *                                  {@link #VIBE_DEVPROPTYPE_MASTERSTRENGTH}
     *                                  property type but the maximum OEM
     *                                  priority
     *                                  ({@link #VIBE_MAX_OEM_DEVICE_PRIORITY})
     *                                  has not been associated with the device
     *                                  handle.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setDevicePropertyInt32(int deviceHandle,
                                       int devPropType,
                                       int devPropValue);

    /**
     * Gets a string property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to get a string
     *                          property. The handle to the device must have
     *                          been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the string property to get.
     * @return                  Requested string property of the device.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a string property of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public String getDevicePropertyString(int deviceHandle, int devPropType);

    /**
     * Sets a string property of an open device.
     *
     * @param   deviceHandle    Handle to the device for which to set a string
     *                          property. The handle to the device must have
     *                          been obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   devPropType     Property type of the string property to set.
     * @param   devPropValue    Value of the string property to set.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>devPropType</code> parameter
     *                                          specifies the
     *                                          {@link #VIBE_DEVPROPTYPE_LICENSE_KEY}
     *                                          property type and the given
     *                                          license key is invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error setting the device property.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_PROPERTY_TYPE"</dt>
     *                              <dd>The <code>devPropType</code> parameter
     *                                  specifies an invalid property type for
     *                                  a string property of the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void setDevicePropertyString(int deviceHandle,
                                        int devPropType,
                                        String devPropValue);

    public int getDeviceKernelParameter(int deviceIndex,
                                        int kernelParamID);

    /*
    ** Effect Player.
    */

    /**
     * Plays an effect defined in IVT data.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   ivt             IVT data containing the definition of the effect
     *                          to play. Use
     *                          {@link #getBuiltInEffects getBuiltInEffects} to
     *                          access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int playIVTEffect(int deviceHandle, byte ivt[], int effectIndex);

    /**
     * Repeatedly plays a Timeline effect defined in IVT data.
     * <p>
     * This function repeats only Timeline effects. If the given effect index
     * refers to a simple effect,
     * {@link #playIVTEffectRepeat playIVTEffectRepeat} ignores the
     * <code>repeat</code> parameter and plays the simple effect once. In that
     * case, {@link #playIVTEffectRepeat playIVTEffectRepeat} behaves like
     * {@link #playIVTEffect playIVTEffect}. To determine the type of an IVT
     * effect, call {@link #getIVTEffectType getIVTEffectType}.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   ivt             IVT data containing the definition of the effect
     *                          to play. Use
     *                          {@link #getBuiltInEffects getBuiltInEffects} to
     *                          access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link #getIVTEffectCount etIVTEffectCount}.
     * @param   repeat          Number of times to repeat the effect. To play
     *                          the effect indefinitely, set
     *                          <code>repeat</code> to
     *                          {@link #VIBE_REPEAT_COUNT_INFINITE}. To repeat
     *                          the effect a finite number of times, set
     *                          <code>repeat</code> to a value from zero to
     *                          {@link #VIBE_REPEAT_COUNT_INFINITE}<code>&nbsp;-&nbsp;1</code>.
     *                          Setting <code>repeat</code> to zero plays the
     *                          effect once (repeats the effect zero times) and
     *                          is equivalent to calling
     *                          {@link #playIVTEffect playIVTEffect}. To stop
     *                          the effect before it has repeated the requested
     *                          number of times, or to stop an effect that is
     *                          playing indefinitely, call
     *                          {@link #stopPlayingEffect stopPlayingEffect} or
     *                          {@link #stopAllPlayingEffects stopAllPlayingEffects}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int playIVTEffectRepeat(int deviceHandle,
                                   byte ivt[],
                                   int effectIndex,
                                   byte repeat);

    /**
     * Plays a MagSweep effect given the parameters defining the effect.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, the effect duration is clamped to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   style           Style of the effect. The effect style must be
     *                          one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time is clamped to a value from zero to
     *                          the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     Attack level of the effect. The attack level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time is clamped to a value from zero to the
     *                          value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       Fade level of the effect. The fade level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int playMagSweepEffect(int deviceHandle,
                                  int duration,
                                  int magnitude,
                                  int style,
                                  int attackTime,
                                  int attackLevel,
                                  int fadeTime,
                                  int fadeLevel);

    /**
     * Plays a Periodic effect given the parameters defining the effect.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, the effect duration is clamped to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   period          Period of the effect in milliseconds or
     *                          microseconds. If the most significant bit is 0,
     *                          the period is in milliseconds. If the most
     *                          significant bit is 1, the period defined in the
     *                          remaining bits is in microseconds. For code
     *                          clarity, you can OR
     *                          {@link #VIBE_PERIOD_RESOLUTION_MICROSECOND} with
     *                          the period when the period is in microseconds.
     *                          The effect period (in milliseconds) should go
     *                          from the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MIN_PERIOD}
     *                          device capability type to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MAX_PERIOD}
     *                          device capability type, inclusive.
     * @param   styleAndWaveType
     *                          Style and wave type of the effect. The effect
     *                          style and wave type must be one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     *                          possibly bitwise ORed with one of
     *                          <ul>
     *                              <li>{@link #VIBE_WAVETYPE_SQUARE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_TRIANGLE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SINE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHUP}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHDOWN}</li>
     *                              <li>{@link #VIBE_DEFAULT_WAVETYPE}</li>
     *                          </ul>
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time is clamped to a value from zero to
     *                          the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     Attack level of the effect. The attack level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time is clamped to a value from zero to the
     *                          value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       Fade level of the effect. The fade level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int playPeriodicEffect(int deviceHandle,
                                  int duration,
                                  int magnitude,
                                  int period,
                                  int styleAndWaveType,
                                  int attackTime,
                                  int attackLevel,
                                  int fadeTime,
                                  int fadeLevel);

    /**
     * Plays a Waveform effect given the parameters defining the effect.
     * <p>
     * This function is supported only in the 5000 API edition. To get the
     * API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   data            PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   dataSize        Size of the PCM data in bytes. The size must be
     *                          greater than zero. The maximum supported size is
     *                          16 megabytes, or less if limited by platform
     *                          constraints.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     * @param   bitDepth        Bit depth of PCM data, or number of bits per
     *                          sample. The only supported values are 8 and 16;
     *                          that is, the Player supports only 8-bit and
     *                          16-bit PCM data.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive. When set
     *                          to {@link #VIBE_MAX_MAGNITUDE}, the PCM data
     *                          samples are not attenuated. Magnitude values
     *                          less than {@link #VIBE_MAX_MAGNITUDE} serve to
     *                          attenuate the PCM data samples.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int playWaveformEffect(int deviceHandle,
                                  byte data[],
                                  int dataSize,
                                  int sampleRate,
                                  int bitDepth,
                                  int magnitude);

    /**
     * Modifies a playing MagSweep effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing MagSweep effect to modify.
     *                          The handle to the effect must have been obtained
     *                          from
     *                          {@link #playMagSweepEffect playMagSweepEffect},
     *                          {@link #playIVTEffect playIVTEffect}, or
     *                          {@link #playIVTEffectRepeat playIVTEffectRepeat}
     *                          to play the MagSweep effect.
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, the effect duration is clamped to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   style           Style of the effect. The effect style must be
     *                          one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time is clamped to a value from zero to
     *                          the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     Attack level of the effect. The attack level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time is clamped to a value from zero to the
     *                          value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       Fade level of the effect. The fade level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying the effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectHandle</code> parameter
     *                                  does not specify a MagSweep effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingMagSweepEffect(int deviceHandle,
                                            int effectHandle,
                                            int duration,
                                            int magnitude,
                                            int style,
                                            int attackTime,
                                            int attackLevel,
                                            int fadeTime,
                                            int fadeLevel);

    /**
     * Modifies a playing Periodic effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing Periodic effect to modify.
     *                          The handle to the effect must have been obtained
     *                          from
     *                          {@link #playPeriodicEffect playPeriodicEffect},
     *                          {@link #playIVTEffect playIVTEffect}, or
     *                          {@link #playIVTEffectRepeat playIVTEffectRepeat}
     *                          to play the Periodic effect.
     * @param   duration        Duration of the effect in milliseconds. To
     *                          specify an infinite duration, use
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, the effect duration is clamped to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   period          Period of the effect in milliseconds or
     *                          microseconds. If the most significant bit is 0,
     *                          the period is in milliseconds. If the most
     *                          significant bit is 1, the period defined in the
     *                          remaining bits is in microseconds. For code
     *                          clarity, you can OR
     *                          {@link #VIBE_PERIOD_RESOLUTION_MICROSECOND} with
     *                          the period when the period is in microseconds.
     *                          The effect period (in milliseconds) should go
     *                          from the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MIN_PERIOD}
     *                          device capability type to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MAX_PERIOD}
     *                          device capability type, inclusive.
     * @param   styleAndWaveType
     *                          Style and wave type of the effect. The effect
     *                          style and wave type must be one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     *                          possibly bitwise ORed with one of
     *                          <ul>
     *                              <li>{@link #VIBE_WAVETYPE_SQUARE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_TRIANGLE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SINE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHUP}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHDOWN}</li>
     *                              <li>{@link #VIBE_DEFAULT_WAVETYPE}</li>
     *                          </ul>
     * @param   attackTime      Attack time of the effect in milliseconds. The
     *                          attack time is clamped to a value from zero to
     *                          the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     Attack level of the effect. The attack level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        Fade time of the effect in milliseconds. The
     *                          fade time is clamped to a value from zero to the
     *                          value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       Fade level of the effect. The fade level is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying the effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectHandle</code> parameter
     *                                  does not specify a Periodic effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingPeriodicEffect(int deviceHandle,
                                            int effectHandle,
                                            int duration,
                                            int magnitude,
                                            int period,
                                            int styleAndWaveType,
                                            int attackTime,
                                            int attackLevel,
                                            int fadeTime,
                                            int fadeLevel);

    /**
     * Appends PCM data to a playing Waveform effect.
     * <p>
     * If the Waveform effect specified by the <code>effectHandle</code>
     * parameter is no longer playing, this method starts playing a new
     * Waveform effect and is equivalent to
     * {@link #playWaveformEffect playWaveformEffect}.
     * <p>
     * This function is supported only in the 5000 API edition. To get the
     * API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing Waveform effect to which
     *                          to append PCM data. The handle to the effect
     *                          must have been obtained from
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect, or from a previous
     *                          call to
     *                          {@link #appendWaveformEffect appendWaveformEffect}
     *                          to append PCM data to the playing Waveform
     *                          effect. If this parameter represents an invalid
     *                          effect handle, this function starts playing a
     *                          new Waveform effect and is equivalent to
     *                          {@link #playWaveformEffect playWaveformEffect}.
     * @param   data            PCM data to append. This data defines the
     *                          actuator drive signal for the Waveform effect
     *                          and is formatted in the same way as the PCM data
     *                          in a WAV file with the exception that only 8-bit
     *                          and 16-bit mono (not stereo) samples are
     *                          supported.
     * @param   dataSize        Size of the PCM data to append in bytes. The
     *                          size must be greater than zero. The maximum
     *                          supported size is 16 megabytes, or less if
     *                          limited by platform constraints.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second). This value must be the same
     *                          as the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect.
     * @param   bitDepth        Bit depth of PCM data, or number of bits per
     *                          sample. The only supported values are 8 and 16;
     *                          that is, the Player supports only 8-bit and
     *                          16-bit PCM data. This value must be the same as
     *                          the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive. When set
     *                          to {@link #VIBE_MAX_MAGNITUDE}, the PCM data
     *                          samples are not attenuated. Magnitude values
     *                          less than {@link #VIBE_MAX_MAGNITUDE} serve to
     *                          attenuate the PCM data samples. There may be a
     *                          performance penalty when specifying a different
     *                          magnitude from the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect.
     * @return                  Handle to the playing effect. This value may be
     *                          different from the <code>effectHandle</code>
     *                          parameter and must be the value used in
     *                          subsequent calls to this method to append more
     *                          PCM data to the Waveform effect, and also in
     *                          subsequent calls to
     *                          {@link #pausePlayingEffect pausePlayingEffect}
     *                          or {@link #stopPlayingEffect stopPlayingEffect}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The value of the
     *                                          <code>sampleRate</code> or
     *                                          <code>bitDepth</code> parameter
     *                                          does not match the value that
     *                                          was passed to
     *                                          {@link #playWaveformEffect playWaveformEffect}
     *                                          to play the Waveform effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int appendWaveformEffect(int deviceHandle,
                                    int effectHandle,
                                    byte data[],
                                    int dataSize,
                                    int sampleRate,
                                    int bitDepth,
                                    int magnitude);

    /**
     * Retrieves the status of an effect; whether playing, not playing, or paused.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the effect for which to get the
     *                          status. The handle to the effect must have been
     *                          obtained from
     *                          <ul>
     *                              <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *                              <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *                              <li>{@link #playIVTEffect playIVTEffect}</li>
     *                              <li>{@link #playIVTEffectRepeat playIVTEffectRepeat}</li>
     *                              <li>{@link #createStreamingEffect createStreamingEffect}</li>
     *                          </ul>
     * @return                  Status of the effect. The effect status is one of
     *                          <ul>
     *                              <li>{@link #VIBE_EFFECT_STATE_NOT_PLAYING}</li>
     *                              <li>{@link #VIBE_EFFECT_STATE_PLAYING}</li>
     *                              <li>{@link #VIBE_EFFECT_STATE_PAUSED}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of the
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getEffectState(int deviceHandle, int effectHandle);

    /**
     * Pauses a playing effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the effect to pause. The handle to the
     *                          effect must have been obtained from
     *                          <ul>
     *                              <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *                              <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *                              <li>{@link #playWaveformEffect playWaveformEffect}</li>
     *                              <li>{@link #appendWaveformEffect appendWaveformEffect}</li>
     *                              <li>{@link #playIVTEffect playIVTEffect}</li>
     *                              <li>{@link #playIVTEffectRepeat playIVTEffectRepeat}</li>
     *                              <li>{@link #createStreamingEffect createStreamingEffect}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error pausing the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void pausePlayingEffect(int deviceHandle, int effectHandle);

    /**
     * Resumes a paused effect from the point where the effect was paused.
     * <p>
     * Depending on the available slots, it is possible that some simple effects
     * from a paused IVT or Streaming Sample could not be resumed. The API will
     * return success even when it cannot resume all of the simple effects.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the effect to resume. The handle to
     *                          the effect must have been obtained from
     *                          <ul>
     *                              <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *                              <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *                              <li>{@link #playWaveformEffect playWaveformEffect}</li>
     *                              <li>{@link #appendWaveformEffect appendWaveformEffect}</li>
     *                              <li>{@link #playIVTEffect playIVTEffect}</li>
     *                              <li>{@link #playIVTEffectRepeat playIVTEffectRepeat}</li>
     *                              <li>{@link #createStreamingEffect createStreamingEffect}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error resuming the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void resumePausedEffect(int deviceHandle, int effectHandle);

    /**
     * Stops a playing or paused effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the effect to stop. The handle to the
     *                          effect must have been obtained from
     *                          <ul>
     *                              <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *                              <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *                              <li>{@link #playWaveformEffect playWaveformEffect}</li>
     *                              <li>{@link #appendWaveformEffect appendWaveformEffect}</li>
     *                              <li>{@link #playIVTEffect playIVTEffect}</li>
     *                              <li>{@link #playIVTEffectRepeat playIVTEffectRepeat}</li>
     *                              <li>{@link #createStreamingEffect createStreamingEffect}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error stopping the effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void stopPlayingEffect(int deviceHandle, int effectHandle);

    /**
     * Stops all playing and paused effects on a device.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</li>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error stopping all effects.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void stopAllPlayingEffects(int deviceHandle);

    /**
     * Plays an effect defined in IVT data.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   ivt             IVT data containing the definition of the effect
     *                          to play. Use
     *                          {@link #getBuiltInEffects getBuiltInEffects} to
     *                          access built-in IVT effects.
     * @param   effectIndex     Index of the effect to play. The index of the
     *                          effect must be greater than or equal to zero and
     *                          less than the number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @param   interpolant     Initial interpolant value for the interpolated effect.
     *                          The interpolant value must be greater than or equal to 0 and
     *                          less than or equal to {@link ImmVibeAPI#VIBE_MAX_INTERPOLANT}.
     *                          The interpolant value may be subsequently modified by calling
     *                          {@link EffectHandle#modifyPlayingInterpolatedEffectInterpolant}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int playIVTInterpolatedEffect(int deviceHandle, byte ivt[], int effectIndex, int interpolant);

    /**
     * Modifies the playing interpolated effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing Periodic effect to modify.
     *                          The handle to the effect must have been obtained
     *                          from
     *                          {@link #playPeriodicEffect playPeriodicEffect},
     *                          {@link #playIVTEffect playIVTEffect}, or
     *                          {@link #playIVTEffectRepeat playIVTEffectRepeat}
     *                          to play the Periodic effect.
     * @param   interpolant      Interpolant value for the playing interpolated effect with
     *                           {@link Device#playIVTInterpolatedEffect}.
     *                           The interpolant value must be greater than or equal to 0
     *                           and less than or equal to {@link #VIBE_MAX_INTERPOLANT}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The device has been closed by a
     *                                          previous call to
     *                                          {@link Device#close Device.close}.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error modifying this effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>This effect handle does not represent a
     *                                  Periodic effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void modifyPlayingInterpolatedEffectInterpolant(int deviceHandle, int effectHandle, int interpolant);

    /*
    ** Streaming Effect Player.
    */

    /**
     * Creates a Streaming effect.
     * <p>
     * Call {@link #createStreamingEffect createStreamingEffect} to create a new
     * Streaming effect and get a handle to the new effect. Use that effect
     * handle to play Streaming Samples by calling
     * {@link #playStreamingSample playStreamingSample} or
     * {@link #playStreamingSampleWithOffset playStreamingSampleWithOffset}.
     *
     * @param   deviceHandle    Handle to the device on which to create the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @return                  Handle to the created effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>deviceHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  device.</li>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error creating the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int createStreamingEffect(int deviceHandle);

    /**
     * Destroys a Streaming effect.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the Streaming effect to destroy. The
     *                          handle to the effect must have been obtained
     *                          from
     *                          {@link #createStreamingEffect createStreamingEffect}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error destroying the Streaming
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectHandle</code> parameter
     *                                  specifies an invalid handle to a
     *                                  Streaming effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void destroyStreamingEffect(int deviceHandle, int effectHandle);

    /**
     * Plays a Streaming Sample.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the Streaming effect. The handle to
     *                          the effect must have been obtained from
     *                          {@link #createStreamingEffect createStreamingEffect}.
     * @param   streamingSample Streaming Sample data.
     * @param   size            Streaming Sample data size in bytes. The size
     *                          must be greater than zero and less than or equal
     *                          to {@link #VIBE_MAX_STREAMING_SAMPLE_SIZE}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a Streaming
     *                                          effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                      <li>The <code>size</code>
     *                                          parameter less than or equal to
     *                                          zero or greater than
     *                                          {@link #VIBE_MAX_STREAMING_SAMPLE_SIZE}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the Streaming Sample.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void playStreamingSample(int deviceHandle,
                                    int effectHandle,
                                    byte streamingSample[],
                                    int size);

    /**
     * Plays a Streaming Sample with a time offset.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the Streaming effect. The handle to
     *                          the effect must have been obtained from
     *                          {@link #createStreamingEffect createStreamingEffect}.
     * @param   streamingSample Streaming Sample data.
     * @param   size            Streaming Sample data size in bytes. The size
     *                          must be greater than zero and less than or equal
     *                          to
     *                          {@link #VIBE_MAX_STREAMING_SAMPLE_SIZE}.
     * @param   offsetTime      For <code>offsetTime</code> values that are
     *                          greater than zero, playback is delayed for
     *                          <code>offsetTime</code> in milliseconds. For
     *                          <code>offsetTime</code> values that are less
     *                          than zero, sample playback begins in offset time
     *                          in milliseconds into the Streaming Sample.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a Streaming
     *                                          effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                      <li>The <code>size</code>
     *                                          parameter less than or equal to
     *                                          zero or greater than
     *                                          {@link #VIBE_MAX_STREAMING_SAMPLE_SIZE}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the Streaming Sample.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void playStreamingSampleWithOffset(int deviceHandle,
                                              int effectHandle,
                                              byte streamingSample[],
                                              int size,
                                              int offsetTime);


    /*
    ** IVT.
    */

    /**
     * Retrieves the built-in IVT effect data. The built-in IVT data provides a
     * set of effects that can be played by calling
     * {@link #playIVTEffect playIVTEffect} or
     * {@link #playIVTEffectRepeat playIVTEffectRepeat}.
     *
     * @return                  Built-in IVT effect data.
     */
    public byte[] getBuiltInEffects();

    /**
     * Gets the number of effects defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @return                  Number of effects defined in the IVT data.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>ivt</code> parameter is
     *                                  invalid or contains invalid IVT
     *                                  data.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect count.</dd>
     *                          </dl>
     */
    public int getIVTEffectCount(byte ivt[]);

    /**
     * Gets the name of an effect defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @param   effectIndex     Index of the effect for which to get the name.
     *                          The index of the effect must be greater than or
     *                          equal to zero and less than the number of effects
     *                          returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @return                  Name of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect name.</dd>
     *                          </dl>
     */
    public String getIVTEffectName(byte ivt[], int effectIndex);

    /**
     * Gets the index of an effect defined in IVT data given the name of the
     * effect.
     *
     * @param   ivt             IVT data.
     * @param   effectName      Name of the effect for which to get the index.
     * @return                  Index of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>ivt</code> parameter is
     *                                  invalid or contains invalid IVT
     *                                  data.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>An effect with the given name was not
     *                                  found in the IVT data.</dd>
     *                          </dl>
     */
    public int getIVTEffectIndexFromName(byte ivt[], String effectName);

    /**
     * Gets the type of an effect defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @return                  Type of the effect. The effect type is one of
     *                          <ul>
     *                              <li>{@link #VIBE_EFFECT_TYPE_MAGSWEEP}</li>
     *                              <li>{@link #VIBE_EFFECT_TYPE_PERIODIC}</li>
     *                              <li>{@link #VIBE_EFFECT_TYPE_WAVEFORM}</li>
     *                              <li>{@link #VIBE_EFFECT_TYPE_TIMELINE}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect type.</dd>
     *                          </dl>
     */
    public int getIVTEffectType(byte ivt[], int effectIndex);

    /**
     * Gets the duration of an effect defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @return                  Duration of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect duration.</dd>
     *                          </dl>
     */
    public int getIVTEffectDuration(byte ivt[], int effectIndex);

    /**
     * Gets the parameters of a MagSweep effect defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @param   duration        [out] Duration of the effect in milliseconds. If
     *                          the effect duration is infinite,
     *                          <code>duration[0]</code> is set to
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, <code>duration[0]</code> is set to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       [out] Magnitude of the effect. The effect
     *                          magnitude goes from {@link #VIBE_MIN_MAGNITUDE}
     *                          to {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   style           [out] Style of the effect. The effect style is
     *                          one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     * @param   attackTime      [out] Attack time of the effect in milliseconds.
     *                          The attack time goes from zero to the value
     *                          returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     [out] Attack level of the effect. The attack
     *                          level goes from {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        [out] Fade time of the effect in milliseconds.
     *                          The fade time goes from zero to the value
     *                          returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       [out] Fade level of the effect. The fade level
     *                          goes from {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect
     *                                  parameters.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectIndex</code> parameter
     *                                  does not specify a MagSweep effect.</dd>
     *                          </dl>
     */
    public void getIVTMagSweepEffectDefinition(byte ivt[],
                                               int effectIndex,
                                               int duration[],
                                               int magnitude[],
                                               int style[],
                                               int attackTime[],
                                               int attackLevel[],
                                               int fadeTime[],
                                               int fadeLevel[]);

    /**
     * Gets the parameters of a Periodic effect defined in IVT data.
     *
     * @param   ivt             IVT data.
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getIVTEffectCount getIVTEffectCount}.
     * @param   duration        [out] Duration of the effect in milliseconds. If
     *                          the effect duration is infinite,
     *                          <code>duration[0]</code> is set to
     *                          {@link #VIBE_TIME_INFINITE}. For a finite
     *                          duration, <code>duration[0]</code> is set to a
     *                          value from zero to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_EFFECT_DURATION}
     *                          device capability type, inclusive.
     * @param   magnitude       [out] Magnitude of the effect. The effect
     *                          magnitude goes from {@link #VIBE_MIN_MAGNITUDE}
     *                          to {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   period          [out] Period of the effect in milliseconds.
     *                          The effect period goes
     *                          from the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MIN_PERIOD}
     *                          device capability type to the value returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the {@link #VIBE_DEVCAPTYPE_MAX_PERIOD}
     *                          device capability type, inclusive.
     * @param   styleAndWaveType
     *                          [out] Style and wave type of the effect. The
     *                          effect style and wave type are one of
     *                          <ul>
     *                              <li>{@link #VIBE_STYLE_SMOOTH}</li>
     *                              <li>{@link #VIBE_STYLE_STRONG}</li>
     *                              <li>{@link #VIBE_STYLE_SHARP}</li>
     *                              <li>{@link #VIBE_DEFAULT_STYLE}</li>
     *                          </ul>
     *                          bitwise ORed with one of
     *                          <ul>
     *                              <li>{@link #VIBE_WAVETYPE_SQUARE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_TRIANGLE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SINE}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHUP}</li>
     *                              <li>{@link #VIBE_WAVETYPE_SAWTOOTHDOWN}</li>
     *                              <li>{@link #VIBE_DEFAULT_WAVETYPE}</li>
     *                          </ul>
     * @param   attackTime      [out] Attack time of the effect in milliseconds.
     *                          The attack time goes from zero to the value
     *                          returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   attackLevel     [out] Attack level of the effect. The attack
     *                          level goes from {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @param   fadeTime        [out] Fade time of the effect in milliseconds.
     *                          The fade time goes from zero to the value
     *                          returned by
     *                          {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                          for the
     *                          {@link #VIBE_DEVCAPTYPE_MAX_ENVELOPE_TIME}
     *                          device capability type, inclusive.
     * @param   fadeLevel       [out] Fade level of the effect. The fade level
     *                          goes from {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getIVTEffectCount getIVTEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect
     *                                  parameters.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectIndex</code> parameter
     *                                  does not specify a Periodic effect.</dd>
     *                          </dl>
     */
    public void getIVTPeriodicEffectDefinition(byte ivt[],
                                               int effectIndex,
                                               int duration[],
                                               int magnitude[],
                                               int period[],
                                               int styleAndWaveType[],
                                               int attackTime[],
                                               int attackLevel[],
                                               int fadeTime[],
                                               int fadeLevel[]);

    /**
     * Saves an IVT file to persistent storage.
     *
     * @param   ivt             IVT data.
     * @param   pathName        Path name of the file to save.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>pathName</code>
     *                                          parameter is invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error saving the IVT file.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void saveIVTFile(byte ivt[], String pathName);

    /**
     * Removes an IVT file from persistent storage.
     *
     * @param   pathName        Path name of the file to remove.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>pathName</code> parameter
     *                                  is invalid.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error removing the IVT file.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public void deleteIVTFile(String pathName);


    /*
    ** IVT Writer.
    */

    /**
     * Allocates and initializes an IVT buffer.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   bufferSize      Size of the buffer in bytes to allocate.
     * @return                  Initialized buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The size of the buffer is too
     *                                  small.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public byte[] initializeIVTBuffer(int bufferSize);

    /**
     * Gets the size of IVT data.
     * <p>
     * Call this function to determine the size of the IVT data within the
     * buffer. This function may be called at any time but is normally called
     * before saving an IVT file that was changed by calling
     * {@link #insertIVTElement insertIVTElement} or
     * {@link #removeIVTElement removeIVTElement}.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   ivt             IVT data.
     * @return                  Size of the IVT data in bytes. The size of the
     *                          IVT data will be less than or equal to the size
     *                          of the buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>ivt</code> parameter is
     *                                  invalid or contains invalid IVT
     *                                  data.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int getIVTSize(byte ivt[]);

    /**
     * Inserts an element into a Timeline effect in an IVT buffer. The element
     * will be added in chronological order.
     * <p>
     * If the IVT data does not contain a Timeline effect, pass zero for
     * <code>timelineIndex</code> and a Timeline effect will be created to
     * house the element.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   ivt             IVT buffer previously initialized by
     *                          {@link #initializeIVTBuffer initializeIVTBuffer}.
     * @param   timelineIndex   Index of a Timeline effect in which to insert
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     *                          If the IVT data does not contain a Timeline
     *                          effect, pass zero for this parameter and the API
     *                          will create a Timeline effect to house this
     *                          element.
     * @param   element         Parameters of a Periodic effect, MagSweep
     *                          effect, Waveform effect, or Repeat event to
     *                          insert into the Timeline effect.
     * @param   elementData     Extra data for the element. This parameter is
     *                          used only when inserting a Waveform effect and
     *                          provides the effect's PCM data.
     * @return                  Updated buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> buffer
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>ivt</code> buffer
     *                                          is too small.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error inserting the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public byte[] insertIVTElement(byte ivt[],
                                   int timelineIndex,
                                   int[] element,
                                   byte[] elementData);

    /**
     * Removes the element at the specified index from a Timeline effect in an
     * IVT buffer.
     * <p>
     * If the simple effect referenced by this element is not referenced by any
     * other element, the simple effect will also be removed. A simple effect is
     * a MagSweep, Periodic, or Waveform effect.
     * <p>
     * If the element being removed is the only element in a Timeline effect,
     * the Timeline effect will also be removed.
     * <p>
     * Start-times of subsequent elements will not be modified.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   ivt             IVT buffer previously initialized by
     *                          {@link #initializeIVTBuffer initializeIVTBuffer}.
     * @param   timelineIndex   Index of a Timeline effect from which to remove
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     * @param   elementIndex    Index of the element to remove.
     * @return                  Updated buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> buffer
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                      <li>The <code>elementIndex</code>
     *                                          parameter is not a valid index
     *                                          of an element in the Timeline
     *                                          effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error removing the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public byte[] removeIVTElement(byte ivt[],
                                   int timelineIndex,
                                   int elementIndex);

    /**
     * Retrieves the parameters of an element of a Timeline effect in an IVT
     * buffer. The element may be a Periodic effect, or MagSweep effect, a
     * Waveform effect, or a Repeat event.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   ivt             IVT buffer previously initialized by
     *                          {@link #initializeIVTBuffer initializeIVTBuffer}.
     * @param   timelineIndex   Index of a Timeline effect from which to read
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     * @param   elementIndex    Index of the element to retrieve.
     * @return                  Retrieved element parameters.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                      <li>The <code>elementIndex</code>
     *                                          parameter is not a valid index
     *                                          of an element in the Timeline
     *                                          effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error reading the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int[] readIVTElement(byte ivt[],
                                int timelineIndex,
                                int elementIndex);

    /**
     * Retrieves the extra data of an element of a Timeline effect in an IVT
     * buffer. The element should be a Waveform effect, for which this method
     * retrieves the effect's PCM data.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL} device capability type.
     *
     * @param   ivt             IVT buffer previously initialized by
     *                          {@link #initializeIVTBuffer initializeIVTBuffer}.
     * @param   timelineIndex   Index of a Timeline effect from which to read
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     * @param   elementIndex    Index of the element to retrieve.
     * @return                  Retrieved extra data for the element.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>ivt</code> parameter
     *                                          is invalid or contains invalid
     *                                          IVT data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                      <li>The <code>elementIndex</code>
     *                                          parameter is not a valid index
     *                                          of an element in the Timeline
     *                                          effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error reading the extra data for the
     *                                  element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public byte[] readIVTElementData(byte ivt[],
                                     int timelineIndex,
                                     int elementIndex);

    /**
     * Plays the encrypted or plain Waveform effect data given the parameters
     * defining the effect.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link #getCapabilityInt32 getCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   deviceHandle    Handle to the device on which to play the
     *                          effect. The handle to the device must have been
     *                          obtained from
     *                          {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1) {@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data-           |
     *                          ------------------------
     *                          2) {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          The offset should be encoded in native-endian.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     * @param   format          Waveform data format. See valid data format,
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8},
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}
     *                          and {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive. When set
     *                          to {@link #VIBE_MAX_MAGNITUDE}, the PCM data
     *                          samples are not attenuated. Magnitude values
     *                          less than {@link #VIBE_MAX_MAGNITUDE} serve to
     *                          attenuate the PCM data samples.
     * @param   secureMode      The secure mode of the waveform effect data.
     *                          The secure mode can be one of supported modes
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     * @return                  Handle to the playing effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error playing the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int playEnhancedWaveformEffect(int deviceHandle,
                                  byte data[],
                                  int sampleRate,
                                  int format,
                                  int magnitude,
                                  int secureMode);

    /**
     *  Appends the encrypted or plain PCM data in a playing Waveform effect.
     * <p>
     * If the Waveform effect specified by the <code>effectHandle</code>
     * parameter is no longer playing, this method starts playing a new
     * Waveform effect and is equivalent to
     * {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link #getCapabilityInt32 getCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing Waveform effect to which
     *                          to append PCM data. The handle to the effect
     *                          must have been obtained from
     *                          {@link #playWaveformEffect playWaveformEffect},
     *                          or {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}
     *                          to play the Waveform effect, or from a previous
     *                          call to
     *                          {@link #appendWaveformEffect appendWaveformEffect},
     *                          {@link #appendEnhancedWaveformEffect appendEnhancedWaveformEffect},
     *                          or {@link #replaceEnhancedWaveformEffect
     *                          replaceEnhancedWaveformEffect}
     *                          to append PCM data to the playing Waveform
     *                          effect. If this parameter represents an invalid
     *                          effect handle, this function starts playing a
     *                          new Waveform effect and is equivalent to
     *                          {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}.
     * @param   data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1) {@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data-           |
     *                          ------------------------
     *                          2) {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          The offset should be encoded in native-endian.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second). This value must be the same
     *                          as the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect} or
     *                          {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}
     *                          to play the Waveform effect.
     * @param   format          Waveform data format. See valid data format,
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8},
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8} 
     *                          and {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive. When set
     *                          to {@link #VIBE_MAX_MAGNITUDE}, the PCM data
     *                          samples are not attenuated. Magnitude values
     *                          less than {@link #VIBE_MAX_MAGNITUDE} serve to
     *                          attenuate the PCM data samples. There may be a
     *                          performance penalty when specifying a different
     *                          magnitude from the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect.
     * @return                  Handle to the playing effect. This value may be
     *                          different from the <code>effectHandle</code>
     *                          parameter and must be the value used in
     *                          subsequent calls to this method to append more
     *                          PCM data to the Waveform effect, and also in
     *                          subsequent calls to
     *                          {@link #pausePlayingEffect pausePlayingEffect}
     *                          or {@link #stopPlayingEffect stopPlayingEffect}.
     * @param   secureMode      The secure mode of the waveform effect data.
     *                          The secure mode can be one of supported modes
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The value of the
     *                                          <code>sampleRate</code> or
     *                                          <code>bitDepth</code> parameter
     *                                          does not match the value that
     *                                          was passed to
     *                                          {@link #playWaveformEffect playWaveformEffect}
     *                                          to play the Waveform effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int appendEnhancedWaveformEffect(int deviceHandle,
                                    int effectHandle,
                                    byte data[],
                                    int sampleRate,
                                    int format,
                                    int magnitude,
                                    int secureMode);

    /**
     *  Replaces the encrypted or plain PCM data in a playing Waveform effect.
     * <p>
     * If the Waveform effect specified by the <code>effectHandle</code>
     * parameter is no longer playing, this method starts playing a new
     * Waveform effect and is equivalent to
     * {@link #playWaveEnhancedformEffect playEnhancedWaveformEffect}.
     * <p>
     * This function is supported only with Enhanced Waveform feature. To get the
     * the supported feature of the TouchSense Player, call
     * {@link #getCapabilityInt32 getCapabilityInt32} with the
     * {@link #VIBE_DEVCAPTYPE_SUPPORTED_FEATURES} device capability type.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the playing Waveform effect to which
     *                          to append PCM data. The handle to the effect
     *                          must have been obtained from
     *                          {@link #playWaveformEffect playWaveformEffect},
     *                          or {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}
     *                          to play the Waveform effect, or from a previous
     *                          call to
     *                          {@link #appendWaveformEffect appendWaveformEffect},
     *                          {@link #appendEnhancedWaveformEffect appendEnhancedWaveformEffect},
     *                          or {@link #replaceEnhancedWaveformEffect
     *                          replaceEnhancedWaveformEffect}
     *                          to append PCM data to the playing Waveform
     *                          effect. If this parameter represents an invalid
     *                          effect handle, this function starts playing a
     *                          new Waveform effect and is equivalent to
     *                          {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}.
     * @param   data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1) {@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data-           |
     *                          ------------------------
     *                          2) {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          The offset should be encoded in native-endian.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second). This value must be the same
     *                          as the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect} or
     *                          {@link #playEnhancedWaveformEffect playEnhancedWaveformEffect}
     *                          to play the Waveform effect.
     * @param   format          Waveform data format. See valid data format,
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8},
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}
     *                          and {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}.
     * @param   magnitude       Magnitude of the effect. The effect magnitude is
     *                          clamped to a value from
     *                          {@link #VIBE_MIN_MAGNITUDE} to
     *                          {@link #VIBE_MAX_MAGNITUDE}, inclusive. When set
     *                          to {@link #VIBE_MAX_MAGNITUDE}, the PCM data
     *                          samples are not attenuated. Magnitude values
     *                          less than {@link #VIBE_MAX_MAGNITUDE} serve to
     *                          attenuate the PCM data samples. There may be a
     *                          performance penalty when specifying a different
     *                          magnitude from the value that was passed to
     *                          {@link #playWaveformEffect playWaveformEffect}
     *                          to play the Waveform effect.
     * @return                  Handle to the playing effect. This value may be
     *                          different from the <code>effectHandle</code>
     *                          parameter and must be the value used in
     *                          subsequent calls to this method to append more
     *                          PCM data to the Waveform effect, and also in
     *                          subsequent calls to
     *                          {@link #pausePlayingEffect pausePlayingEffect}
     *                          or {@link #stopPlayingEffect stopPlayingEffect}.
     * @param   secureMode      The secure mode of the waveform effect data.
     *                          The secure mode can be one of supported modes
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The value of one or more of the
     *                                          effect parameters is
     *                                          invalid.</li>
     *                                      <li>The value of the
     *                                          <code>sampleRate</code> or
     *                                          <code>bitDepth</code> parameter
     *                                          does not match the value that
     *                                          was passed to
     *                                          {@link #playWaveformEffect playWaveformEffect}
     *                                          to play the Waveform effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error appending PCM data to the effect.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocate memory to
     *                                  complete the request. This happens when
     *                                  the system runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 and 4000 API editions. To get the
     *                                  API edition level, call
     *                                  {@link #getDeviceCapabilityInt32 getDeviceCapabilityInt32}
     *                                  with the
     *                                  {@link #VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int replaceEnhancedWaveformEffect(int deviceHandle,
                                    int effectHandle,
                                    byte data[],
                                    int sampleRate,
                                    int format,
                                    int magnitude,
                                    int secureMode);

    /**
     * Retrieves the remaining duration of an effect; whether playing, not playing, or paused.
     *
     * @param   deviceHandle    Handle to the device associated to the effect.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   effectHandle    Handle to the effect for which to get the
     *                          status. The handle to the effect must have been
     *                          obtained from
     *                          <ul>
     *                              <li>{@link #playMagSweepEffect playMagSweepEffect}</li>
     *                              <li>{@link #playPeriodicEffect playPeriodicEffect}</li>
     *                              <li>{@link #playIVTEffect playIVTEffect}</li>
     *                              <li>{@link #playIVTEffectRepeat playIVTEffectRepeat}</li>
     *                              <li>{@link #createStreamingEffect createStreamingEffect}</li>
     *                          </ul>
     * @return                  Status of the effect. The effect status is one of
     *                          <ul>
     *                              <li>{@link #VIBE_EFFECT_STATE_NOT_PLAYING}</li>
     *                              <li>{@link #VIBE_EFFECT_STATE_PLAYING}</li>
     *                              <li>{@link #VIBE_EFFECT_STATE_PAUSED}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_NOT_INITIALIZED"</dt>
     *                              <dd>The API has not been initialized.
     *                                  {@link #initialize initialize} was not
     *                                  called or threw an exception.</dd>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>The <code>deviceHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to a device.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter specifies an invalid
     *                                          handle to an effect.</li>
     *                                      <li>The <code>effectHandle</code>
     *                                          parameter does not specify a
     *                                          handle to an effect that is
     *                                          associated to the device
     *                                          specified by the
     *                                          <code>deviceHandle</code>
     *                                          parameter.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the status of the
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    public int getEffectRemainingDuration(int deviceHandle, int effectHandle);

     /**
     * Allocates and initializes an IVT buffer from an XIVT buffer.
     * <p>
     *
     * @param   deviceHandle    Handle to the device.
     *                          The handle to the device must have been obtained
     *                          from {@link #openDevice openDevice} or
     *                          {@link #openCompositeDevice openCompositeDevice}.
     * @param   xivt            The XIVT buffer.
     * @return                  An IVT buffer.
     */
    public byte[] loadIVTFromXIVT(int deviceHandle, byte xivt[]);
}

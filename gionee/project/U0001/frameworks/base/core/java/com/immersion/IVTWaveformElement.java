/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTWaveformElement
**
** Description:
**     Java IVTWaveformElement class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Represents a Waveform effect within a Timeline effect in an IVT buffer.
 */
public class IVTWaveformElement extends IVTElement
{
    private WaveformEffectDefinition definition;

    /**
     * Initializes this IVT Waveform element.
     *
     * @param   time            Time in milliseconds of this IVT Waveform
     *                          element within the Timeline effect.
     * @param   definition      Definition of this IVT Waveform element.
     */
    public IVTWaveformElement(int time, WaveformEffectDefinition definition)
    {
        super(ImmVibeAPI.VIBE_ELEMTYPE_WAVEFORM, time);
        this.definition = definition;
    }

    /**
     * Sets the definition of this IVT Waveform element.
     *
     * @param   definition      Definition of this IVT Waveform element.
     */
    public void setDefinition(WaveformEffectDefinition definition)
    {
        this.definition = definition;
    }

    /**
     * Gets the definition of this IVT Waveform element.
     *
     * @return                  Definition of this IVT Waveform element.
     */
    public WaveformEffectDefinition getDefinition()
    {
        return definition;
    }

    public int[] getBuffer()
    {
        int[] retVal = new int[8];

        retVal[0] = getType();
        retVal[1] = getTime();
        retVal[2] = 0;
        retVal[3] = definition.getDataSize();
        retVal[4] = definition.getSampleRate();
        retVal[5] = definition.getBitDepth();
        retVal[6] = definition.getMagnitude();
        retVal[7] = definition.getActuatorIndex();

        return retVal;
    }
}

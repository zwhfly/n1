/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTBuffer.java
**
** Description:
**     Java IVTBuffer class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

import com.immersion.ImmVibe;
import com.immersion.PeriodicEffectDefinition;
import com.immersion.MagSweepEffectDefinition;

/**
 * Stores IVT data.
 */
public class IVTBuffer
{
    private byte[] ivt;

    /**
     * Retrieves the built-in IVT effect data. The built-in IVT data provides a
     * set of effects that can be played by calling
     * {@link Device#playIVTEffect Device.playIVTEffect} or
     * {@link Device#playIVTEffectRepeat Device.playIVTEffectRepeat}.
     *
     * @return                  Built-in IVT effect data.
     */
    public static IVTBuffer getBuiltInEffects()
    {
        return new IVTBuffer(ImmVibe.getInstance().getBuiltInEffects());
    }

    /**
     * Initializes this IVT buffer object given a byte array containing valid
     * IVT data.
     * <p>
     * If the given array is null or contains invalid IVT data, subsequent
     * method calls on this IVT buffer object may throw exceptions.
     *
     * @param   ivt             Byte array containing valid IVT data.
     */
    public IVTBuffer(byte ivt[])
    {
        this.ivt = ivt;
    }

    /**
     * Initializes this IVT buffer object using a given buffer size.
     *
     * @param   bufferSize      Size of the buffer in bytes to allocate.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The size of the buffer is too
     *                                  small.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public IVTBuffer(int bufferSize)
    {
        this.ivt = ImmVibe.getInstance().initializeIVTBuffer(bufferSize);
    }

    /**
     * Gets the IVT data stored in this IVT buffer.
     *
     * @return                  Stored IVT data.
     */
    public byte[] getBuffer()
    {
        return ivt;
    }

    /**
     * Gets the number of effects defined in this IVT buffer.
     *
     * @return                  Number of effects defined in this IVT buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This IVT buffer contains invalid
     *                                  data.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect count.</dd>
     *                          </dl>
     */
    public int getEffectCount()
    {
        return ImmVibe.getInstance().getIVTEffectCount(ivt);
    }

    /**
     * Gets the duration of an effect defined in this IVT buffer.
     *
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getEffectCount getEffectCount}.
     * @return                  Duration of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getEffectCount getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect duration.</dd>
     *                          </dl>
     */
    public int getEffectDuration(int effectIndex)
    {
        return ImmVibe.getInstance().getIVTEffectDuration(ivt, effectIndex);
    }

    /**
     * Gets the index of an effect defined in this IVT buffer given the name of
     * the effect.
     *
     * @param   effectName      Name of the effect for which to get the index.
     * @return                  Index of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains invalid
     *                                          data.</li>
     *                                      <li>The <param>effectName</param>
     *                                          parameter is invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>An effect with the given name was not
     *                                  found in the IVT data.</dd>
     *                          </dl>
     */
    public int getEffectIndexFromName(String effectName)
    {
         return ImmVibe.getInstance().getIVTEffectIndexFromName(ivt, effectName);
    }

    /**
     * Gets the name of an effect defined in this IVT buffer.
     *
     * @param   effectIndex     Index of the effect for which to get the name.
     *                          The index of the effect must be greater than or
     *                          equal to zero and less than the number of
     *                          effects returned by
     *                          {@link #getEffectCount getEffectCount}.
     * @return                  Name of the effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getEffectCount getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect name.</dd>
     *                          </dl>
     */
    public String getEffectName(int effectIndex)
    {
         return ImmVibe.getInstance().getIVTEffectName(ivt, effectIndex);
    }

    /**
     * Gets the type of an effect defined in this IVT buffer.
     *
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getEffectCount getEffectCount}.
     * @return                  Type of the effect. The effect type is one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_EFFECT_TYPE_MAGSWEEP}</li>
     *                              <li>{@link ImmVibe#VIBE_EFFECT_TYPE_PERIODIC}</li>
     *                              <li>{@link ImmVibe#VIBE_EFFECT_TYPE_WAVEFORM}</li>
     *                              <li>{@link ImmVibe#VIBE_EFFECT_TYPE_TIMELINE}</li>
     *                          </ul>
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getEffectCount getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect type.</dd>
     *                          </dl>
     */
    public int getEffectType(int effectIndex)
    {
        return ImmVibe.getInstance().getIVTEffectType(ivt, effectIndex);
    }

    /**
     * Gets the definition of a MagSweep effect in this IVT buffer.
     *
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getEffectCount getEffectCount}.
     * @return                  The MagSweep effect definition.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getEffectCount getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect
     *                                  parameters.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectIndex</code> parameter
     *                                  does not specify a MagSweep effect.</dd>
     *                          </dl>
     */
    public MagSweepEffectDefinition getMagSweepEffectDefinitionAtIndex(int effectIndex)
    {
        int[]    duration = new int[1];
        int[]    magnitude = new int[1];
        int[]    style = new int[1];
        int[]    attackTime = new int[1];
        int[]    attackLevel = new int[1];
        int[]    fadeTime = new int[1];
        int[]    fadeLevel = new int[1];

        ImmVibe.getInstance().getIVTMagSweepEffectDefinition(ivt,
                                                             effectIndex,
                                                             duration,
                                                             magnitude,
                                                             style,
                                                             attackTime,
                                                             attackLevel,
                                                             fadeTime,
                                                             fadeLevel);

        return new MagSweepEffectDefinition(duration[0],
                                            magnitude[0],
                                            style[0],
                                            attackTime[0],
                                            attackLevel[0],
                                            fadeTime[0],
                                            fadeLevel[0],
                                            0);
    }

    /**
     * Gets the definition of a Periodic effect in this IVT buffer.
     *
     * @param   effectIndex     Index of the effect. The effect index must be
     *                          greater than or equal to zero and less than the
     *                          number of effects returned by
     *                          {@link #getEffectCount getEffectCount}.
     * @return                  The Periodic effect definition.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>effectIndex</code>
     *                                          parameter is negative, or
     *                                          greater than or equal to the
     *                                          value returned by
     *                                          {@link #getEffectCount getEffectCount}.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error getting the effect
     *                                  parameters.</dd>
     *                              <dt>"VIBE_E_INCOMPATIBLE_EFFECT_TYPE"</dt>
     *                              <dd>The <code>effectIndex</code> parameter
     *                                  does not specify a Periodic effect.</dd>
     *                          </dl>
     */
    public PeriodicEffectDefinition getPeriodicEffectDefinitionAtIndex(int effectIndex)
    {
        int[]    duration = new int[1];
        int[]    magnitude = new int[1];
        int[]    period = new int[1];
        int[]    style = new int[1];
        int[]    attackTime = new int[1];
        int[]    attackLevel = new int[1];
        int[]    fadeTime = new int[1];
        int[]    fadeLevel = new int[1];

        ImmVibe.getInstance().getIVTPeriodicEffectDefinition(ivt,
                                                             effectIndex,
                                                             duration,
                                                             magnitude,
                                                             period,
                                                             style,
                                                             attackTime,
                                                             attackLevel,
                                                             fadeTime,
                                                             fadeLevel);

        return new PeriodicEffectDefinition(duration[0],
                                            magnitude[0],
                                            period[0],
                                            style[0],
                                            attackTime[0],
                                            attackLevel[0],
                                            fadeTime[0],
                                            fadeLevel[0],
                                            0);
    }

    /**
     * Gets the size of the IVT data in this buffer.
     * <p>
     * Call this function to determine the size of the IVT data within this
     * buffer. This function may be called at any time but is normally called
     * before saving an IVT file that was changed by calling
     * {@link #insertElement insertElement} or
     * {@link #removeElement removeElement}.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link Device#getCapabilityInt32 Device.getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     * device capability type.
     *
     * @return                  Size of the IVT data in bytes. The size of the
     *                          IVT data will be less than or equal to the size
     *                          of this buffer.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>This IVT buffer contains invalid
     *                                  data.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public int getSize()
    {
        return ImmVibe.getInstance().getIVTSize(ivt);
    }

    /**
     * Inserts an element into a Timeline effect in this IVT buffer. The element
     * will be added in chronological order.
     * <p>
     * If this IVT buffer does not contain a Timeline effect, pass zero for
     * <code>timelineIndex</code> and a Timeline effect will be created to
     * house the element.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link Device#getCapabilityInt32 Device.getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     * device capability type.
     *
     * @param   timelineIndex   Index of a Timeline effect in which to insert
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     *                          If the IVT data does not contain a Timeline
     *                          effect, pass zero for this parameter and the API
     *                          will create a Timeline effect to house this
     *                          element.
     * @param   element         Parameters of a Periodic effect, MagSweep
     *                          effect, Waveform effect, or Repeat event to
     *                          insert into the Timeline effect.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error inserting the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public void insertElement(int timelineIndex, IVTElement element)
    {
        if (element.getType() == ImmVibe.VIBE_ELEMTYPE_WAVEFORM)
        {
            ivt = ImmVibe.getInstance().insertIVTElement(ivt, timelineIndex, element.getBuffer(), ((IVTWaveformElement)element).getDefinition().getData());
        }
        else
        {
            ivt = ImmVibe.getInstance().insertIVTElement(ivt, timelineIndex, element.getBuffer(), null);
        }
    }

    /**
     * Removes the element at the specified index from a Timeline effect in this
     * IVT buffer.
     * <p>
     * If the simple effect referenced by this element is not referenced by any
     * other element, the simple effect will also be removed. A simple effect is
     * a MagSweep, Periodic, or Waveform effect.
     * <p>
     * If the element being removed is the only element in a Timeline effect,
     * the Timeline effect will also be removed.
     * <p>
     * Start-times of subsequent elements will not be modified.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link Device#getCapabilityInt32 Device.getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     * device capability type.
     *
     * @param   timelineIndex   Index of a Timeline effect from which to remove
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     * @param   elementIndex    Index of the element to remove.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                      <li>The <code>elementIndex</code>
     *                                          parameter is not a valid index
     *                                          of an element in the Timeline
     *                                          effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error removing the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public void removeElement(int timelineIndex, int elementIndex)
    {
        ivt = ImmVibe.getInstance().removeIVTElement(ivt, timelineIndex, elementIndex);
    }

    /**
     * Retrieves an element of a Timeline effect in this IVT buffer. The element
     * may be a Periodic effect, or MagSweep effect, a Waveform effect, or a
     * Repeat event.
     * <p>
     * This function is supported only in the 4000 and 5000 API editions. To get
     * the API edition level, call
     * {@link Device#getCapabilityInt32 Device.getCapabilityInt32} with the
     * {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     * device capability type.
     *
     * @param   timelineIndex   Index of a Timeline effect from which to read
     *                          the element. If the IVT data only contains one
     *                          Timeline effect, pass zero for this parameter.
     * @param   elementIndex    Index of the element to retrieve.
     * @return                  Retrieved element parameters.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>timelineIndex</code>
     *                                          parameter is not a valid index
     *                                          of a Timeline effect.</li>
     *                                      <li>The <code>elementIndex</code>
     *                                          parameter is not a valid index
     *                                          of an element in the Timeline
     *                                          effect.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error reading the element.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This function is not supported in the
     *                                  3000 edition of the API. To get the API
     *                                  edition level, call
     *                                  {@link Device#getCapabilityInt32 Device.getCapabilityInt32}
     *                                  with the
     *                                  {@link ImmVibe#VIBE_DEVCAPTYPE_EDITION_LEVEL}
     *                                  device capability type.</dd>
     *                          </dl>
     */
    public IVTElement readElement(int timelineIndex, int elementIndex)
    {
        IVTElement retVal;

        retVal = IVTElement.newIVTElement(ImmVibe.getInstance().readIVTElement(ivt, timelineIndex, elementIndex));

        if (retVal != null && retVal.getType() == ImmVibe.VIBE_ELEMTYPE_WAVEFORM)
        {
            ((IVTWaveformElement)retVal).getDefinition().setData(ImmVibe.getInstance().readIVTElementData(ivt, timelineIndex, elementIndex));
        }

        return retVal;
    }

    /**
     * Saves an IVT file to persistent storage.
     *
     * @param   pathName        Path name of the file to save.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>One or more of the arguments is invalid;
     *                                  for example,
     *                                  <ul>
     *                                      <li>This IVT buffer contains
     *                                          invalid data.</li>
     *                                      <li>The <code>pathName</code>
     *                                          parameter is invalid.</li>
     *                                  </ul>
     *                              </dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error saving the IVT file.</dd>
     *                              <dt>"VIBE_E_NOT_ENOUGH_MEMORY"</dt>
     *                              <dd>The API cannot allocation memory to
     *                                  complete the request. This happens when
     *                                  the device runs low in memory.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This method is not supported in the
     *                                  present implementation.</dd>
     *                          </dl>
     */
    public void saveHapticTrack(String pathName)
    {
        ImmVibe.getInstance().saveIVTFile(ivt, pathName);
    }

    /**
     * Removes an IVT file from persistent storage.
     *
     * @param   pathName        Path name of the file to remove.
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The <code>pathName</code> parameter
     *                                  is invalid.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error removing the IVT file.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                              <dt>"VIBE_E_NOT_SUPPORTED"</dt>
     *                              <dd>This method is not supported in the
     *                                  present implementation.</dd>
     *                          </dl>
     */
    public static void deleteHapticTrack(String pathName)
    {
        ImmVibe.getInstance().deleteIVTFile(pathName);
    }
}

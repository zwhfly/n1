/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
 
#include <string.h>
#include <android/log.h>
#include "GioneeSceneDetection.h"

#define LOG_TAG "GioneeSceneDetection"

namespace android {
GioneeSceneDetection::GioneeSceneDetection()
	: mListener(NULL)
	, mSceneDetectionHandler(NULL) 
	, mInitialized(false)
	, mFrameProcessor(NULL)
{
	pthread_mutex_init(&mMutex, NULL);
}

GioneeSceneDetection::~GioneeSceneDetection()
{
	pthread_mutex_destroy(&mMutex);
}

int32_t 
GioneeSceneDetection::init() 
{
	int32_t ret = 0;

	if (mFrameProcessor == NULL) {
		mFrameProcessor = FrameProcessor::getInstance();
	}

	mSceneDetectionHandler = new SceneDetectionHandler(this);
	if (mSceneDetectionHandler == NULL) {
		PRINTE("Failed to new SceneDetectionHandler.");
	}
	
	return ret;
}

void 
GioneeSceneDetection::deinit()
{	
	if (mFrameProcessor != NULL) {
		mFrameProcessor->deinit();
	}

	if (mSceneDetectionHandler != NULL) {
		delete mSceneDetectionHandler;
		mSceneDetectionHandler = NULL;
	}

	mInitialized = false;
}

int32_t 
GioneeSceneDetection::enableSceneDetection(GNSceneDetectionParam param)
{
	int32_t res = 0; 
	
	if (param.imageWidth == 0 || param.imageHeight == 0) {
		PRINTE("Invalid parameter.");
		return -1;
	}

    if (!mInitialized) {
		res = mFrameProcessor->enableFrameProcess(param.imageWidth,
												param.imageHeight,
												param.imageFormat, 
												GN_CAMERA_FEATUER_SCENE_DETECTION);
		if (res) {
    		PRINTE("Fail to Init Frame Processor");
			return -1;
		}

		mFrameProcessor->registerSceneDetectionListener(mSceneDetectionHandler);
    }
	
	mInitialized = true;

	return res;
}

int32_t 
GioneeSceneDetection::disableSceneDetection()
{
	int32_t res = 0; 

	if (mFrameProcessor != NULL) {
		mFrameProcessor->disableFrameProcess(GN_CAMERA_FEATUER_SCENE_DETECTION);
		mFrameProcessor->unregisterSceneDetectionListener();
	}

	mInitialized = false;

	return res;
}

int32_t 
GioneeSceneDetection::setCameraListener(GNCameraFeatureListener* listener)
{
	int32_t res = 0; 

	pthread_mutex_lock(&mMutex);
	
	mListener = listener;
	
	pthread_mutex_unlock(&mMutex);

	return res;
}

int32_t 
GioneeSceneDetection::processSceneDetection(PIMGOFFSCREEN imgSrc)
{
	int32_t res = 0;
	
	if (!mInitialized) {
		PRINTD("processSceneDetection not Initialized");
		return -1;
	}
	
	if (mFrameProcessor != NULL) {
		res = mFrameProcessor->processFrame(imgSrc ,true);
		if (res != 0) {
			PRINTD("Failed to process defog detection.");
		}
	}

	return res;
}

void
GioneeSceneDetection::
SceneDetectionHandler::
onCallback(int result) 
{
	//pthread_mutex_lock(&mGioneeSceneDetection->mMutex);
	
	if (mGioneeSceneDetection != NULL && mGioneeSceneDetection->mListener != NULL) {
		mGioneeSceneDetection->mListener->notify(GN_CAMERA_MSG_TYPE_SCENE_DETECTION, getSceneType(result), 0);
	}
	
	//pthread_mutex_unlock(&mGioneeSceneDetection->mMutex);
}

int32 
GioneeSceneDetection::
SceneDetectionHandler::
getSceneType(int result)
{
	int type = GN_CAMERA_SCENE_DETECTION_TYPE_AUTO;

	switch(result) {
		case SCENE_FOG_OR_FLARE:
			type = GN_CAMERA_SCENE_DETECTION_TYPE_DEFOG;
			break;
		case SCENE_HDR:
			type = GN_CAMERA_SCENE_DETECTION_TYPE_BACKLIT;
			break;
		case SCENE_NIGNT:
			type = GN_CAMERA_SCENE_DETECTION_TYPE_NIGHT;
			break;
	}

#ifdef DEBUG_INFO
	PRINTE("result = %d, type = %d", result, type);
#endif

	return type;
}

};

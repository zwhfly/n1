/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#include "FrameProcessor.h"

#define DETECTION_MODE    	0    /*0:accurate; 1:fast*/
#define OSDMODE           	0    /*display info*/

FrameProcessor::FrameProcessor()
	: mDefogListener(NULL)
	, mDefogProcessor(NULL)
	, mMemInitParm(NULL)
	, mInitialized(false)
	, mGNFrameProcessMask(0)
	, mWidth(0)
	, mHeight(0)
{
	pthread_mutex_init(&mMutex, NULL);
	pthread_mutex_init(&mThreadMutex, NULL);
	pthread_cond_init(&mCond, NULL);

	memset(&mFogFlareDegree, 0, sizeof(mFogFlareDegree));		
	mFogFlareDegree.isDefault = 1;
	
    if(pthread_create(&mTidDefogDetection, NULL, defogDetectionRoutine, (void *)this)) {
        PRINTE("pthread_create error");
        exit(1);
    }

}

FrameProcessor::~FrameProcessor()
{
	
	pthread_mutex_lock(&mThreadMutex); 
	mMsgScene.type =  SCENE_MSG_TYPE_DESTROY ;
	pthread_cond_signal(&mCond);
	pthread_mutex_unlock(&mThreadMutex);

	PRINTD("[~FrameProcess] Wait for thread exit");
	pthread_join(mTidDefogDetection, NULL);
	
	pthread_mutex_destroy(&mMutex);
	pthread_mutex_destroy(&mThreadMutex);
	pthread_cond_destroy(&mCond);
	
	PRINTD("[~FrameProcess] X");
	
}

FrameProcessor* FrameProcessor::mFrameProcessor = NULL;

FrameProcessor*
FrameProcessor::
getInstance()
{
	if (mFrameProcessor == NULL) {
		mFrameProcessor = new FrameProcessor();	
	}

	return mFrameProcessor;
}

int32_t
FrameProcessor::
init()
{
	int32_t res = 0;

	return res;
}


void
FrameProcessor::
deinit()
{
	if (mInitialized) {
		mInitialized = false;
		deinitEngineInstance();
		mDefogListener = NULL;
	}
}

int32_t
FrameProcessor::
registerSceneDetectionListener(SceneDetectionListener* listener)
{
	int32_t res = 0;
	
	mDefogListener = listener;

	return res;
}

void
FrameProcessor::
unregisterSceneDetectionListener()
{
	mDefogListener = NULL;
}

int32_t
FrameProcessor::
enableFrameProcess(int32_t maxWidth, int32_t maxHeight, GNImgFormat format, int32_t featureMask)
{
	int32_t res = 0;

	int32_t mask = GN_CAMERA_FEATURE_DEFOG_SHOT | GN_CAMERA_FEATUER_SCENE_DETECTION;
	
	if ((featureMask & mask) == 0) {
		return -1;
	}

	mGNFrameProcessMask |= featureMask;

	uint32_t pixelFormat = getPixelFormat(format);

    if (!mInitialized || (maxWidth*maxHeight) > (mWidth*mHeight)) {
		if (mInitialized && (maxWidth*maxHeight) > (mWidth*mHeight)) {
			mInitialized = false;
			deinitEngineInstance();
		}

		initEngineInstance(maxWidth, maxHeight, pixelFormat);
		
		pthread_mutex_lock(&mThreadMutex); 	
		mMsgScene.type =  SCENE_MSG_TYPE_CLEAR;
		pthread_cond_signal(&mCond);
		pthread_mutex_unlock(&mThreadMutex);

		mWidth = maxWidth;
		mHeight = maxHeight;
		
		mInitialized = true;
	}

	return res;
}

void
FrameProcessor::
disableFrameProcess(int32_t featureMask)
{
	int32_t mask = GN_CAMERA_FEATURE_DEFOG_SHOT | GN_CAMERA_FEATUER_SCENE_DETECTION;
	
	if ((featureMask & mask) == 0) {
		return;
	}

	mGNFrameProcessMask &= ~featureMask;

	if ((mGNFrameProcessMask & mask) == 0 && mInitialized) {
		//Uninit and destory engine instance.
		deinit();
	}
}

int32_t 
FrameProcessor::
getDefogDetectionResult()
{
	int32_t result;

	if (mFogFlareDegree.isDefault == 1) {
		result = 0;//havn't processed for now
	} else {
		result = mFogFlareDegree.result;
	}

	PRINTD("%s result = %d", __func__, result);
	
    if (mDefogListener != NULL) {
		mDefogListener->onCallback(result);
	}

	return result;
}

int32_t
FrameProcessor::
processFrame(PIMGOFFSCREEN srcImg, bool isPreview)
{
	int32_t res = 0;
	
	if (!mInitialized || srcImg == NULL) {
		PRINTD("[processFrame] failed, mInitialized %d", mInitialized);
		return -1;
	}
	
	if (isPreview) {
		getDefogDetectionResult();
		if (pthread_mutex_trylock(&mThreadMutex) != 0) {
			PRINTE("Failed to trylock mThreadMutex.");
			return -1;
		}
		mMsgScene.srcImg = *srcImg;
		mMsgScene.type = SCENE_MSG_TYPE_PROC;
		pthread_cond_signal(&mCond);
	} else {
		pthread_mutex_lock(&mThreadMutex);
		if (mDefogProcessor != NULL) {
			res = mDefogProcessor->Run(srcImg);
		}
	}
	
	pthread_mutex_unlock(&mThreadMutex);
	
	return res;
}

int32_t
FrameProcessor::
initEngineInstance(int32_t width, int32_t height, uint32_t format)
{
	int32_t res = 0;

	pthread_mutex_lock(&mMutex);

	mDefogProcessor = new DefogProcessor();
	if (mDefogProcessor == NULL) {
		PRINTE("Faile to new DefogProcessor");
		return -1;
	}

	//Init engine instance.
	mMemInitParm = new AlgDefogInitParm;
	if (mMemInitParm == NULL) {
		PRINTE("Failed to new mMemInitParm");
		delete mDefogProcessor;
		mDefogProcessor = NULL;
		return -1;
	}
	
	mMemInitParm->width =  width;
	mMemInitParm->height = height;
	mMemInitParm->pixelArrayFormat = format;

	AlgDefogRunTimeParm* runtimeparm = mDefogProcessor->GetDefaltRunTimeparm(OSDMODE);
	mDefogProcessor->Ctrl(runtimeparm);//set tuning param

	mDefogProcessor->Init(mMemInitParm);

	pthread_mutex_unlock(&mMutex);

	return res;
}

void
FrameProcessor::
deinitEngineInstance()
{
	pthread_mutex_lock(&mMutex);
	
	if (mDefogProcessor != NULL) {
		mDefogProcessor->Destroy();
		delete mDefogProcessor;
		mDefogProcessor = NULL;
	}
	
	if (mMemInitParm != NULL) {
		delete mMemInitParm;
		mMemInitParm = NULL;
	}

	pthread_mutex_unlock(&mMutex);
}

void *
FrameProcessor::
defogDetectionRoutine(void *data)
{
	int ret;	
	int running = 1;
	IMGOFFSCREEN srcImg;
	FrameProcessor* thiz  = (FrameProcessor*)data;
	
	signed long long end_time;
	struct timeval r;
	struct timespec ts;

    do {		
	  	pthread_mutex_lock(&thiz->mThreadMutex);
    	pthread_cond_wait(&thiz->mCond, &thiz->mThreadMutex);
	  	pthread_mutex_unlock(&thiz->mThreadMutex);
		
		switch (thiz->mMsgScene.type) {			
			case SCENE_MSG_TYPE_PROC:
				pthread_mutex_lock(&thiz->mMutex);
				if (thiz->mInitialized && thiz->mDefogProcessor != NULL) {
					thiz->mDefogProcessor->Detection(&(thiz->mMsgScene.srcImg), 
												&thiz->mFogFlareDegree, DETECTION_MODE);
				}
				pthread_mutex_unlock(&thiz->mMutex);
				break;
			
			case SCENE_MSG_TYPE_DESTROY:
				pthread_exit(0);
				running = 0;
		        break;

			case SCENE_MSG_TYPE_CLEAR:
				break;

			default:
				PRINTE("Invalid msg type");
				break;
		}
    } while (running);

	return NULL;
}

uint32_t
FrameProcessor::
getPixelFormat(GNImgFormat format)
{
	uint32_t pixelFormat = 0;

	switch(format) {
		case GN_IMG_FORMAT_NV21:
			pixelFormat = IMG_PAF_NV21;
			break;
		case GN_IMG_FORMAT_YV12:
			pixelFormat = IMG_PAF_YV12;
			break;
		case GN_IMG_FORMAT_YUYV:
			pixelFormat = IMG_PAF_YUYV;
			break;
		default:
			break;
	}

	return pixelFormat;
}


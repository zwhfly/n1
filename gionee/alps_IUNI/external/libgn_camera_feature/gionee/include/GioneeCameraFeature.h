/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
 
#ifndef ANDROID_GIONEE_CAMERA_FEATURE_H
#define ANDROID_GIONEE_CAMERA_FEATURE_H

#include <GNCameraFeatureBase.h>
#include <android/log.h>
#include <pthread.h>

#include "GioneeDefogShot.h"
#include "GioneeSceneDetection.h"

namespace android { 

class GioneeCameraFeature : public GNCameraFeatureBase {
public:
    GioneeCameraFeature();
	~GioneeCameraFeature();
	
    static GioneeCameraFeature* createInstance();
    virtual void destroyInstance();
	
	virtual int32 init();
    virtual void  deinit();
    virtual int32 initPreviewSize(int width, int height, GNImgFormat format);
	virtual int32 setCameraListener(GNCameraFeatureListener* listener);
    virtual int32 processPreview(void* inputBuffer, int size, int mask);
    virtual int32 processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask);
	virtual int32 setDefogShot(GNDefogShot_t defogShotMode, GNImgFormat format);
	virtual int32 setSceneDetection(GNSceneDetectionParam param);
	virtual int32 processDefogShot(PIMGOFFSCREEN imgSrc);
	virtual int32 processSceneDetection(PIMGOFFSCREEN imgSrc);

private:
	static uint32_t getPixelFormat(GNImgFormat format);
	bool dumpFrame(PIMGOFFSCREEN imageData, int size, int dump_type);
private:
	int mGNCameraFeature;
	GNCameraFeatureListener* mListener;
	GNImgFormat mFormat;

    int mWidth;
    int mHeight;
	
	int mPreviewBufferSize;
	bool mDumpImage;
	pthread_mutex_t mMutex;

	GioneeDefogShot* mGioneeDefogShot;
	GioneeSceneDetection* mGioneeSceneDetection;
	
	IMGOFFSCREEN mPreviewData;
	int mDumpCnt;

};
};
#endif /* ANDROID_ARCSOFT_CAMERA_FEATURE_H */

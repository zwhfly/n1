/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#ifndef GIONEE_DEFOG_DETECTION_H_
#define GIONEE_DEFOG_DETECTION_H_

#include <stdlib.h>

#include "AlgImgDefog.h"
#include "GNCameraFeatureDefs.h"
#include "GNCameraFeatureListener.h"
#include "FrameProcessor.h"

namespace android {

class GioneeSceneDetection {
public:
	GioneeSceneDetection();
	~GioneeSceneDetection();

	int32_t init();
	void deinit();

	int32_t setCameraListener(GNCameraFeatureListener* listener);
	int32_t processSceneDetection(PIMGOFFSCREEN imgSrc);
	int32_t enableSceneDetection(GNSceneDetectionParam param);
	int32_t disableSceneDetection();	
	
private:
	class SceneDetectionHandler: public SceneDetectionListener {
		public:
			SceneDetectionHandler(GioneeSceneDetection* thiz) {
				mGioneeSceneDetection = thiz;
			}
			
			virtual void onCallback(int result);

		private:
			int32 getSceneType(int result);
			
		private:
			GioneeSceneDetection* mGioneeSceneDetection;
	};
	
public:
    SceneDetectionHandler* mSceneDetectionHandler;
    FrameProcessor* mFrameProcessor;

	pthread_mutex_t mMutex;
	bool mInitialized;
	bool mDefogDetectEnabled;
	GNCameraFeatureListener* mListener;	
};

};
#endif /*GIONEE_DEFOG_DETECTION_H_*/

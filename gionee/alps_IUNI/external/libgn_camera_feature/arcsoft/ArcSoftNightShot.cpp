/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : zhangwu
 * Email  : zhangwu@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftNightShot"
#include <android/log.h>
#include <string.h>

#include "ArcSoftNightShot.h"

#define MEM_SIZE	(15 * 1024 * 1024)	//15M

namespace android { 

ArcSoftNightShot::ArcSoftNightShot()
	: mMem(NULL)
	, mMemMgr(NULL)
	, mEnhancer(NULL)
	, mImageNum(0)
{
	
}
	
int32 
ArcSoftNightShot::init() 
{
	MRESULT res = 0; 
	
	memset(&mSrcInput, 0, sizeof(mSrcInput));
	memset(&mDstImg, 0, sizeof(mDstImg));
	memset(&mParam, 0, sizeof(mParam));
	memset(&mSrcImages, 0, sizeof(mSrcImages));

	return res;
}
void  
ArcSoftNightShot::deinit()
{
	ANS_Uninit(&mEnhancer);

	if(mMemMgr != NULL) {
		MMemMgrDestroy(mMemMgr);
		mMemMgr = NULL;
	}

	if(mMem != NULL) {
		MMemFree(NULL, mMem);
		mMem = NULL;
	}

	if(mDstImg.ppu8Plane[0] != NULL) {
		MMemFree(NULL, mDstImg.ppu8Plane[0]);
		mDstImg.ppu8Plane[0] = NULL;
	}

	for (int i = 0; i < ((MAX_INPUT_IMAGES - 1)); i ++) {
		if(mSrcImages[i].ppu8Plane[0] != NULL) {
			free(mSrcImages[i].ppu8Plane[0]);
		}
	}

	memset(&mSrcImages, 0, sizeof(mSrcImages));
	mImageNum = 0;
}

int32
ArcSoftNightShot::getBurstCnt() 
{
	return MAX_INPUT_IMAGES;
}

int32 
ArcSoftNightShot::processNightShot(LPASVLOFFSCREEN param, int width, int height)
{
	MRESULT res = 0;
	int memsize = 0;

	PRINTD("[processNightShot] mImageNum = %d", mImageNum);

	if (param != NULL && ! startEnhance(param, width, height)) {
		return res;
	}

	//should be configed with the actual size, instead of align size.
	mDstImg.i32Height 			= height;
	mDstImg.i32Width			= width;
	mDstImg.u32PixelArrayFormat	= param->u32PixelArrayFormat;
	mDstImg.pi32Pitch[0]		= param->pi32Pitch[0];
	mDstImg.pi32Pitch[1]		= param->pi32Pitch[1];
	mDstImg.pi32Pitch[2]		= param->pi32Pitch[2];

	switch (mDstImg.u32PixelArrayFormat) {
		case ASVL_PAF_NV21:
			memsize 				= param->i32Width * param->i32Height * 3 / 2;
			mDstImg.ppu8Plane[0]	= (MByte*)MMemAlloc(NULL, memsize);
			mDstImg.ppu8Plane[1] 	= mDstImg.ppu8Plane[0] + param->i32Width * param->i32Height;
			mDstImg.ppu8Plane[2] 	= NULL;
			break;
		case ASVL_PAF_YUYV:
			memsize 				= param->i32Width * param->i32Height * 2;
			mDstImg.ppu8Plane[0]	= (MByte*)MMemAlloc(NULL, memsize);
			mDstImg.ppu8Plane[1] 	= NULL;
			mDstImg.ppu8Plane[2] 	= NULL;
			break;
		default:	
			//ASVL_PAF_NV21
			memsize 				= param->i32Width * param->i32Height * 3 / 2;
			mDstImg.ppu8Plane[0]	= (MByte*)MMemAlloc(NULL, memsize);
			mDstImg.ppu8Plane[1] 	= mDstImg.ppu8Plane[0] + param->i32Width * param->i32Height;
			mDstImg.ppu8Plane[2] 	= NULL;
			break;
	}
	
	res = ANS_Enhancement(mEnhancer, &mSrcInput, &mDstImg, &mParam, NULL, NULL);
	if (res != 0) {
		PRINTE("Failed to process night shot [#%ld].", res);
	} else {
		memcpy(param->ppu8Plane[0], mDstImg.ppu8Plane[0], memsize);
	}

	for (int i = 0; i < (MAX_INPUT_IMAGES - 1); i ++) {
		if(mSrcImages[i].ppu8Plane[0] != NULL) {
			free(mSrcImages[i].ppu8Plane[0]);
		}
	}
	
	if(mDstImg.ppu8Plane[0] != NULL) {
		MMemFree(NULL, mDstImg.ppu8Plane[0]);
		mDstImg.ppu8Plane[0] = NULL;
	}

	MMemSet(&mSrcInput, 0, sizeof(mSrcInput));
	MMemSet(&mSrcImages, 0, sizeof(mSrcImages));
	mImageNum = 0;

	PRINTD("process night shot over!");

	return res;
}

int32 
ArcSoftNightShot::enableNightShot()
{
	MRESULT res = 0;

	if (mMem == NULL) {
		mMem = MMemAlloc(NULL, MEM_SIZE);
		mMemMgr = MMemMgrCreate(mMem, MEM_SIZE);

		res = ANS_Init(mMemMgr, &mEnhancer);
		if (res != 0) {
			PRINTE("Failed to initialize nightshot enginer [#%ld].", res);
			deinit();
		} else {
			ANS_GetDefaultParam(&mParam);
			
			mParam.lLightIntensity = 100;
			mParam.bNeedSharpen = true;
		}
	}

	mImageNum = 0;

	return res;
}

int32 
ArcSoftNightShot::disableNightShot()
{
	MRESULT res = 0;

	deinit();

	return res;
}

bool
ArcSoftNightShot::startEnhance(LPASVLOFFSCREEN param, int width, int height)
{
	bool res = false;
	int memsize = 0;

	memsize = getMemSize(param->u32PixelArrayFormat, param->i32Width, param->i32Height);
	memcpy(&mSrcImages[mImageNum], param, sizeof(ASVLOFFSCREEN));

	//The src images should be configed with the actual size.
	//The dst images may exist green area if configed with stride and scanline.
	mSrcImages[mImageNum].i32Width = width;
	mSrcImages[mImageNum].i32Height = height;

	if (mImageNum < (MAX_INPUT_IMAGES - 1)) {
		mSrcImages[mImageNum].ppu8Plane[0] = (MUInt8*) malloc(memsize);
		if (mSrcImages[mImageNum].ppu8Plane[0] == NULL) {
			PRINTE("Failed to malloc memory.");
		} else {
			memcpy(mSrcImages[mImageNum].ppu8Plane[0], param->ppu8Plane[0], memsize);
		}
	}

	switch (param->u32PixelArrayFormat) {
		case ASVL_PAF_NV21:
			mSrcImages[mImageNum].pi32Pitch[0] = param->i32Width;
			mSrcImages[mImageNum].pi32Pitch[1] = param->i32Width;
			mSrcImages[mImageNum].pi32Pitch[2] = 0;
			mSrcImages[mImageNum].ppu8Plane[1] = mSrcImages[mImageNum].ppu8Plane[0] + param->i32Width * param->i32Height;
			mSrcImages[mImageNum].ppu8Plane[2] = NULL;
			break;
		case ASVL_PAF_YUYV:
			mSrcImages[mImageNum].pi32Pitch[0] = param->i32Width * 2;
			mSrcImages[mImageNum].pi32Pitch[1] = 0;
			mSrcImages[mImageNum].pi32Pitch[2] = 0;
			mSrcImages[mImageNum].ppu8Plane[1] = NULL;
			mSrcImages[mImageNum].ppu8Plane[2] = NULL;
			break;
		default:
			//ASVL_PAF_NV21
			mSrcImages[mImageNum].pi32Pitch[0] = param->i32Width;
			mSrcImages[mImageNum].pi32Pitch[1] = param->i32Width;
			mSrcImages[mImageNum].pi32Pitch[2] = 0;
			mSrcImages[mImageNum].ppu8Plane[1] = mSrcImages[mImageNum].ppu8Plane[0] + param->i32Width * param->i32Height;
			mSrcImages[mImageNum].ppu8Plane[2] = NULL;
			break;
	}

	mSrcInput.pImages[mImageNum] = &mSrcImages[mImageNum];
	
	mImageNum++;
	mSrcInput.lImgNum = mImageNum;
	
	if (mImageNum >= MAX_INPUT_IMAGES) {
		res = true;
	} 

	return res;
}

int32 
ArcSoftNightShot::getMemSize(MUInt32 u32PixelArrayFormat, int width, int height) 
{
	int memSize = 0;

	switch (u32PixelArrayFormat) {
		case ASVL_PAF_NV21:
			memSize = height * width * 3 / 2;
			break;
		case ASVL_PAF_YUYV:
			memSize = height * width * 2;
			break;
		default:	
			//ASVL_PAF_NV21
			memSize = height * width * 3 / 2;
			break;
	}

	return memSize;
}

};


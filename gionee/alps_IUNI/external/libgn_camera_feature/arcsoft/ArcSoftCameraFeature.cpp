/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
#define LOG_TAG "ArcSoftCameraFeature"
#include <android/log.h>
#include <cutils/properties.h>
#include <stdlib.h>

#include "ArcSoftCameraFeature.h"
 
namespace android {

ArcSoftCameraFeature::ArcSoftCameraFeature() 
	: mGNCameraFeature(GN_CAMERA_FEATURE_NONE)
	, mListener(NULL)
	, mOrientation(0)
	, mPreviewFormat(GN_IMG_FORMAT_NONE)
	, mArcSoftNightShot(NULL)
	, mArcSoftNightVideo(NULL)
	, mArcSoftSceneDetection(NULL)
	, mArcSoftFaceBeauty(NULL)
	, mArcSoftAgeGenderDetection(NULL)
	//, mArcSoftGestureShot(NULL)
	, mArcSoftLiveEffect(NULL)
	, mArcSoftPicZoom(NULL)
	, mPreviewBufferSize(0)
	, mPrevWidth(0)
	, mPrevHeight(0)
	, mVideoWidth(0)
	, mVideoHeight(0)
	, mVideoFormat(GN_IMG_FORMAT_NONE)
	, mDumpCnt(0)
{
	char value[PROPERTY_VALUE_MAX] = {0};
	property_get("arcsoft.dumpbuffer", value, "0");
	mDumpImage = (atoi(value) != 0);
	
	//Create ArcSoftFaceBeauty object
	mArcSoftFaceBeauty = new ArcSoftFaceBeauty();
	if (mArcSoftFaceBeauty == NULL) {
		PRINTE("Failed to new ArcSoftFaceBeauty");
	}
/*
	//Create ArcSoftGestureShot object
	mArcSoftGestureShot = new ArcSoftGestureShot();
	if (mArcSoftGestureShot == NULL) {
		PRINTE("Failed to new ArcSoftGestureShot");
	}
*/
	//Create ArcSoftNightShot object
	mArcSoftNightShot = new ArcSoftNightShot();
	if (mArcSoftNightShot == NULL) {
		PRINTE("Failed to new ArcSoftNightShot");
	}

	//Create ArcSoftSceneDetection object
	mArcSoftSceneDetection = new ArcSoftSceneDetection();
	if (mArcSoftSceneDetection == NULL) {
		PRINTE("Failed to new ArcSoftSceneDetection");
	}

	//Create ArcSoftLiveEffect object
	mArcSoftLiveEffect = new ArcSoftLiveEffect();
	if (mArcSoftLiveEffect == NULL) {
		PRINTE("Failed to new ArcSoftLiveEffect");
	}

	//Create ArcSoftAgeGenderDetection object
	mArcSoftAgeGenderDetection = new ArcSoftAgeGenderDetection();
	if (mArcSoftAgeGenderDetection == NULL) {
		PRINTE("Failed to new ArcSoftAgeGenderDetection");
	}

	mArcSoftPicZoom = new ArcSoftPicZoom();
	if (mArcSoftPicZoom == NULL) {
		PRINTE("Failed to new mArcSoftPicZoom");
	}

	mArcSoftNightVideo = new ArcSoftNightVideo();
	if (mArcSoftNightVideo == NULL) {
		PRINTE("Failed to new mArcSoftNightVideo");
	}

	pthread_mutex_init(&mMutex, NULL);
}

ArcSoftCameraFeature::
~ArcSoftCameraFeature() 
{
	if (mArcSoftFaceBeauty != NULL) {
		delete mArcSoftFaceBeauty;
		mArcSoftFaceBeauty = NULL;
	}
/*
	if (mArcSoftGestureShot != NULL) {
		delete mArcSoftGestureShot;
		mArcSoftGestureShot = NULL;
	}
*/
	if (mArcSoftNightShot != NULL) {
		delete mArcSoftNightShot;
		mArcSoftNightShot = NULL;
	}

	if (mArcSoftSceneDetection != NULL) {
		delete mArcSoftSceneDetection;
		mArcSoftSceneDetection = NULL;
	}

	if (mArcSoftLiveEffect != NULL) {
		delete mArcSoftLiveEffect;
		mArcSoftLiveEffect = NULL;
	}

	if (mArcSoftAgeGenderDetection != NULL) {
		delete mArcSoftAgeGenderDetection;
		mArcSoftAgeGenderDetection = NULL;
	}

	if (mArcSoftPicZoom != NULL) {
		delete mArcSoftPicZoom;
		mArcSoftPicZoom = NULL;
	}

	if (mArcSoftNightVideo != NULL) {
		delete mArcSoftNightVideo;
		mArcSoftNightVideo = NULL;
	}

	pthread_mutex_destroy(&mMutex);  
}

ArcSoftCameraFeature*
ArcSoftCameraFeature::
createInstance() 
{
	return new ArcSoftCameraFeature();
}

void
ArcSoftCameraFeature::
destroyInstance() 
{
	delete this;
}

int32
ArcSoftCameraFeature::
init()
{
	MRESULT res = 0;

	pthread_mutex_lock(&mMutex);

	if (mArcSoftFaceBeauty != NULL) {
		res = mArcSoftFaceBeauty->init();
		if (res != 0) {
			PRINTE("Failed to initialized face beauty [%ld].", res);
		}
	}
/*
	if (mArcSoftGestureShot != NULL) {
		res = mArcSoftGestureShot->init();
		if (res != 0) {
			PRINTE("Failed to initialized gesture shot [%ld].", res);
		}
	}
*/
	if (mArcSoftNightShot != NULL) {
		res = mArcSoftNightShot->init();
		if (res != 0) {
			PRINTE("Failed to initialized night shot [%ld].", res);
		}
	}

	if (mArcSoftSceneDetection != NULL) {
		res = mArcSoftSceneDetection->init();
		if (res != 0) {
			PRINTE("Failed to initialized scene detection [%ld].", res);
		}
	}

	if (mArcSoftLiveEffect != NULL) {
		res = mArcSoftLiveEffect->init();
		if (res != 0) {
			PRINTE("Failed to initialized live effect [%ld].", res);
		}
	}

	if (mArcSoftAgeGenderDetection != NULL) {
		res = mArcSoftAgeGenderDetection->init();
		if (res != 0) {
			PRINTE("Failed to initialized age gender detection [%ld].", res);
		}
	}

	if (mArcSoftPicZoom != NULL) {
		res = mArcSoftPicZoom->init();
		if (res != 0) {
			PRINTE("Faile to initialized pic zoom [%ld].", res);
		}
	}

	if (mArcSoftNightVideo != NULL) {
		res = mArcSoftNightVideo->init();
		if (res != 0) {
			PRINTE("Failed to initialized night video [%ld].", res);
		}
	}

	pthread_mutex_unlock(&mMutex);

	return res;
}

void 
ArcSoftCameraFeature::
deinit()
{
	pthread_mutex_lock(&mMutex);

	if (mArcSoftFaceBeauty != NULL) {
		mArcSoftFaceBeauty->deinit();
	}
/*
	if (mArcSoftGestureShot != NULL) {
		mArcSoftGestureShot->deinit();
		mArcSoftGestureShot->setCameraListener(NULL);
	}
*/
	if (mArcSoftNightShot != NULL) {
		mArcSoftNightShot->deinit();
	}

	if (mArcSoftSceneDetection != NULL) {
		mArcSoftSceneDetection->deinit();
		mArcSoftSceneDetection->setCameraListener(NULL);
	}

	if (mArcSoftLiveEffect != NULL) {
		mArcSoftLiveEffect->deinit();
	}

	if (mArcSoftAgeGenderDetection != NULL) {
		mArcSoftAgeGenderDetection->deinit();
		mArcSoftAgeGenderDetection->setCameraListener(NULL);
	}

	if (mArcSoftPicZoom != NULL) {
		mArcSoftPicZoom->deinit();
	}
	
	if (mArcSoftNightVideo != NULL) {
		mArcSoftNightVideo->deinit();
	}

	mPrevWidth = 0;
	mPrevHeight = 0;
	mPreviewFormat = GN_IMG_FORMAT_NONE;
	mPreviewBufferSize = 0;
	mListener = NULL;

	pthread_mutex_unlock(&mMutex);
}

int32 
ArcSoftCameraFeature::
initPreviewSize(int width, int height, GNImgFormat format)
{
	MRESULT res = 0;

	pthread_mutex_lock(&mMutex);
/*
	if (mPrevWidth != width || mPrevHeight != height) {
		if (mArcSoftGestureShot != NULL && (mGNCameraFeature & GN_CAMERA_FEATURE_GESTURE_SHOT)) {
			mArcSoftGestureShot->disableGestureShot();
			mArcSoftGestureShot->enableGestureShot(width, height);
		}
    }
*/
	mPreviewFormat = format;
	mPrevWidth  = width;
	mPrevHeight = height;
	mPreviewData.i32Width	= width;
	mPreviewData.i32Height	= height;

	switch (format) {
		case GN_IMG_FORMAT_YV12:
			mPreviewData.u32PixelArrayFormat 	= ASVL_PAF_YV12;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width / 2;
			mPreviewData.pi32Pitch[2] 			= width / 2;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		case GN_IMG_FORMAT_YUV422:
			mPreviewData.u32PixelArrayFormat 	= ASVL_PAF_YUYV;
			mPreviewData.pi32Pitch[0] 			= width * 2;
			mPreviewData.pi32Pitch[1] 			= 0;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 2;
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.u32PixelArrayFormat 	= ASVL_PAF_NV21;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		case GN_IMG_FORMAT_YUV420:
			mPreviewData.u32PixelArrayFormat 	= ASVL_PAF_I420;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width / 2;
			mPreviewData.pi32Pitch[2] 			= width / 2;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		default:
			mPreviewData.u32PixelArrayFormat 	= ASVL_PAF_I420;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width / 2;
			mPreviewData.pi32Pitch[2] 			= width / 2;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
	}

	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
initVideoSize(int width, int height, GNImgFormat format)
{
	MRESULT res = 0;

	pthread_mutex_lock(&mMutex);

	if (mVideoWidth != width || mVideoHeight != height) {
		mVideoFormat = format;
		mVideoWidth	= width;
		mVideoHeight = height;
		mVideoData.i32Width = width;
		mVideoData.i32Height	= height;
		
		switch (format) {
			case GN_IMG_FORMAT_YV12:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_YV12;
				mVideoData.pi32Pitch[0]			= width;
				mVideoData.pi32Pitch[1]			= width / 2;
				mVideoData.pi32Pitch[2]			= width / 2;
				break;
			case GN_IMG_FORMAT_YUV422:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_YUYV;
				mVideoData.pi32Pitch[0]			= width * 2;
				mVideoData.pi32Pitch[1]			= 0;
				mVideoData.pi32Pitch[2]			= 0;
				break;
			case GN_IMG_FORMAT_NV21:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_NV21;
				mVideoData.pi32Pitch[0]			= width;
				mVideoData.pi32Pitch[1]			= width;
				mVideoData.pi32Pitch[2]			= 0;
				break;
			case GN_IMG_FORMAT_YUV420:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_I420;
				mVideoData.pi32Pitch[0]			= width;
				mVideoData.pi32Pitch[1]			= width / 2;
				mVideoData.pi32Pitch[2]			= width / 2;
				break;
			case GN_IMG_FORMAT_NV12:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_NV12;
				mVideoData.pi32Pitch[0]			= ALIGN_TO_SIZE(width, ALIGN_TO_128);
				mVideoData.pi32Pitch[1]			= ALIGN_TO_SIZE(width, ALIGN_TO_128);
				mVideoData.pi32Pitch[2]			= 0;
				break;
			default:
				mVideoData.u32PixelArrayFormat	= ASVL_PAF_I420;
				mVideoData.pi32Pitch[0]			= width;
				mVideoData.pi32Pitch[1]			= width / 2;
				mVideoData.pi32Pitch[2]			= width / 2;
				break;
		}

    }

	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
setCameraListener(GNCameraFeatureListener* listener)
{
	pthread_mutex_lock(&mMutex);
	
	mListener = listener;

	if (mArcSoftSceneDetection != NULL) {
		mArcSoftSceneDetection->setCameraListener(listener);
	}

	//if (mArcSoftGestureShot != NULL) {
	//	mArcSoftGestureShot->setCameraListener(listener);
	//}

	if (mArcSoftAgeGenderDetection!= NULL) {
		mArcSoftAgeGenderDetection->setCameraListener(listener);
	}

	if (mArcSoftFaceBeauty != NULL) {
		mArcSoftFaceBeauty->setCameraListener(listener);
	}
	
	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
setFaceBeauty(FaceBeautyParam const faceBeautyParam)
{
	pthread_mutex_lock(&mMutex);

	if (faceBeautyParam.faceBeautyState == GN_FACEBEAUTY_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_FACE_BEAUTY;

		if (mArcSoftFaceBeauty != NULL) {
			mArcSoftFaceBeauty->disableFaceBeauty();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATURE_FACE_BEAUTY;

		if (mArcSoftFaceBeauty != NULL) {
			mArcSoftFaceBeauty->enableFaceBeauty();
			mArcSoftFaceBeauty->setFaceBeautyParam(faceBeautyParam);
		}
	}

	pthread_mutex_unlock(&mMutex);
	
	return 0;
}

int32
ArcSoftCameraFeature::
setAgeGenderDetection(GNAgeGenderDetection_t const ageGenderDetection)
{
	pthread_mutex_lock(&mMutex);

	if (ageGenderDetection == GN_AGEGENDER_DETECTION_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATUER_AGEGENDER_DETECTION;

		if (mArcSoftAgeGenderDetection != NULL) {
			mArcSoftAgeGenderDetection->disableAgeGenderDetection();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATUER_AGEGENDER_DETECTION;

		if (mArcSoftAgeGenderDetection != NULL) {
			mArcSoftAgeGenderDetection->enableAgeGenderDetection();
		}
	}
	
	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
setGestureShot(GNGestureShot_t gestureShotMode)
{
	pthread_mutex_lock(&mMutex);
/*	
	if (gestureShotMode == GN_GESTURE_SHOT_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_GESTURE_SHOT;

		if (mArcSoftGestureShot != NULL) {
			mArcSoftGestureShot->disableGestureShot();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATURE_GESTURE_SHOT;

		if (mArcSoftGestureShot != NULL) {
			mArcSoftGestureShot->enableGestureShot(mPrevWidth, mPrevHeight);
		}
	}

	PRINTD("gestureShotMode = %d", gestureShotMode);
*/
	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
setSceneDetection(GNSceneDetectionParam param)
{
	pthread_mutex_lock(&mMutex);
	
	if (param.sceneDetectionMode == GN_SCENE_DETECTION_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATUER_SCENE_DETECTION;

		if (mArcSoftSceneDetection != NULL) {
			mArcSoftSceneDetection->disableSceneDetection();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATUER_SCENE_DETECTION;

		if (mArcSoftSceneDetection != NULL) {
			mArcSoftSceneDetection->enableSceneDetection();
		}
	}

	PRINTD("sceneDetectionMode = %d", param.sceneDetectionMode);

	pthread_mutex_unlock(&mMutex);
	
	return 0;
}

int32 
ArcSoftCameraFeature::
setNightShot(GNNightShot_t nightShotMode)
{
	pthread_mutex_lock(&mMutex);
	
	if (nightShotMode == GN_NIGHT_SHOT_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATUER_NIGHT_SHOT;

		if (mArcSoftNightShot != NULL) {
			mArcSoftNightShot->disableNightShot();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATUER_NIGHT_SHOT;

		if (mArcSoftNightShot != NULL) {
			mArcSoftNightShot->enableNightShot();
		}
	}

	PRINTD("nightShotMode = %d", nightShotMode);

	pthread_mutex_unlock(&mMutex);
	
	return 0;
}

int32 
ArcSoftCameraFeature::
setNightVideo(GNNightVideo_t nightVideoMode)
{
	pthread_mutex_lock(&mMutex);
	
	if (nightVideoMode == GN_NIGHT_VIDEO_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATUER_NIGHT_VIDEO;

		if (mArcSoftNightVideo != NULL) {
			mArcSoftNightVideo->disableNightVideo();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATUER_NIGHT_VIDEO;

		if (mArcSoftNightVideo != NULL) {
			mArcSoftNightVideo->enableNightVideo();
		}
	}

	PRINTD("nightVideoMode = %d", nightVideoMode);

	pthread_mutex_unlock(&mMutex);
	
	return 0;
}

int32 
ArcSoftCameraFeature::
setEffect(GNLiveEffect_t effect)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);

	if (mArcSoftLiveEffect != NULL) {
		if (mArcSoftLiveEffect->setLiveEffectType(effect) != 0) {
			mGNCameraFeature &= ~GN_CAMERA_FEATURE_EFFECT;
		} else {
			mGNCameraFeature |= GN_CAMERA_FEATURE_EFFECT;
		}
	}

	PRINTD("effect = %x", effect);

	pthread_mutex_unlock(&mMutex);

	return res;
}

int32 
ArcSoftCameraFeature::
setOrientation(int orientation) 
{
	pthread_mutex_lock(&mMutex);

	mOrientation = orientation;

	if (mArcSoftSceneDetection != NULL) {
		mArcSoftSceneDetection->setOrientation(orientation);
	}

	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32 
ArcSoftCameraFeature::
setMirror(GNMirror_t mirrorMode)
{
	int32 res = 0;

	if (mirrorMode != GN_MIRROR_OFF) {
		mGNCameraFeature |= GN_CAMERA_FEATUER_MIRROR;
	} else {
		mGNCameraFeature &= ~GN_CAMERA_FEATUER_MIRROR;
	}
	
	return res;
}

int32
ArcSoftCameraFeature::
setPicZoom(PicZoomParam const picZoomParam)
{
	pthread_mutex_lock(&mMutex);
	
	if (picZoomParam.picZoomMode == GN_PIC_ZOOM_OFF) {
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_PIC_ZOOM;

		if (mArcSoftPicZoom!= NULL) {
			mArcSoftPicZoom->disablePicZoom();
		}
	} else {
		mGNCameraFeature |= GN_CAMERA_FEATURE_PIC_ZOOM;

		if (mArcSoftPicZoom != NULL) {
			mArcSoftPicZoom->enablePicZoom(picZoomParam);
		}
	}

	PRINTD("picZoomMode = %d", picZoomParam.picZoomMode);

	pthread_mutex_unlock(&mMutex);
	
	return 0;

}

int32 
ArcSoftCameraFeature::
getBurstCnt(GNCameraFeature_t cameraFeature) 
{
	int32 ret = 0;

	pthread_mutex_lock(&mMutex);

	if (mArcSoftNightShot != NULL && (cameraFeature & GN_CAMERA_FEATUER_NIGHT_SHOT)) {
		ret = mArcSoftNightShot->getBurstCnt();
	}

	if (mArcSoftPicZoom != NULL && (cameraFeature & GN_CAMERA_FEATURE_PIC_ZOOM)) {
		ret = mArcSoftPicZoom->getBurstCnt();
	}
	
	pthread_mutex_unlock(&mMutex);

	return ret;
}


int32 
ArcSoftCameraFeature::
processPreview(void* inputBuffer, int size, int mask)
{
	MRESULT res = 0;
	int32	dumpFrameRef = 0;

	pthread_mutex_lock(&mMutex);

	if (inputBuffer == NULL || size == 0 || size < mPreviewBufferSize) {
		PRINTE("the inputBuffer is null or invalid.");
		pthread_mutex_unlock(&mMutex); 
		return -1;
	}

	if ((mGNCameraFeature & 0XFFFF) == 0) {
		pthread_mutex_unlock(&mMutex); 
		return res;
	}
	
	mPreviewData.ppu8Plane[0] = (MByte*)inputBuffer;
	
	switch (mPreviewFormat) {
		case GN_IMG_FORMAT_YV12:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = mPreviewData.ppu8Plane[1] + mPreviewData.i32Width * mPreviewData.i32Height / 4;
			break;
		case GN_IMG_FORMAT_YUV422:
			mPreviewData.ppu8Plane[1] = NULL;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_YUV420:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = mPreviewData.ppu8Plane[1] + mPreviewData.i32Width * mPreviewData.i32Height/4;
			break;
		default:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = mPreviewData.ppu8Plane[1] + mPreviewData.i32Width * mPreviewData.i32Height/4;
			break;
	}

	if (dumpFrame(&mPreviewData, size, GN_CAMERA_DUMP_FRM_PREVIEW_INPUT)) {
		dumpFrameRef ++;
	}
	
/*
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_GESTURE_SHOT) & mask) {
		res = processGestureShot(&mPreviewData);
		if (res != 0) {
			PRINTD("Failed to process gesture shot.");
		}
	}
*/
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_FACE_BEAUTY) & mask) {
		res = processFaceBeauty(&mPreviewData, GN_DATA_TYPE_PREVIEW);
		if (res != 0) {
			PRINTD("Failed to process face beauty[#%ld].", res);
		}
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATUER_SCENE_DETECTION) & mask) {
		res = processSceneDetection(&mPreviewData);
		if (res != 0) {
			PRINTD("Failed to process scene detection[#%ld].", res);
		}
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATUER_AGEGENDER_DETECTION) & mask) {
		res = processAgeGenderDetection(&mPreviewData, GN_DATA_TYPE_PREVIEW);
		if (res != 0) {
			PRINTD("Failed to process age gender detection[#%ld].", res);
		}
	}

	//process night video
	if ((mGNCameraFeature & GN_CAMERA_FEATUER_NIGHT_VIDEO) & mask) {
		res = processNightVideo(&mPreviewData);
		if (res != 0) {
			PRINTE("Failed to process night shot");
		}
	}

	if (dumpFrame(&mPreviewData, size, GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}

	pthread_mutex_unlock(&mMutex); 

	return res;
}

int32 
ArcSoftCameraFeature::
processVideo(void* inputBuffer, int size, int mask)
{
	MRESULT res = 0;
	int32 dumpFrameRef = 0;
	
	pthread_mutex_lock(&mMutex);
	
	if (inputBuffer == NULL || size == 0) {
		PRINTE("the inputBuffer is null or invalid.");
		pthread_mutex_unlock(&mMutex); 
		return -1;
	}

	if ((mGNCameraFeature & 0XFFFF) == 0) {
		pthread_mutex_unlock(&mMutex); 
		return res;
	}

	int yStride = 0;
	int yScanline = 0;
	
	mVideoData.ppu8Plane[0] = (MByte*)inputBuffer;
	
	switch (mVideoFormat) {
		case GN_IMG_FORMAT_YV12:
			mVideoData.ppu8Plane[1] = mVideoData.ppu8Plane[0] + mVideoData.i32Width * mVideoData.i32Height;
			mVideoData.ppu8Plane[2] = mVideoData.ppu8Plane[1] + mVideoData.i32Width * mVideoData.i32Height / 4;
			break;
		case GN_IMG_FORMAT_YUV422:
			mVideoData.ppu8Plane[1] = NULL;
			mVideoData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_NV21:
			mVideoData.ppu8Plane[1] = mVideoData.ppu8Plane[0] + mVideoData.i32Width * mVideoData.i32Height;
			mVideoData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_YUV420:
			mVideoData.ppu8Plane[1] = mVideoData.ppu8Plane[0] + mVideoData.i32Width * mVideoData.i32Height;
			mVideoData.ppu8Plane[2] = mVideoData.ppu8Plane[1] + mVideoData.i32Width * mVideoData.i32Height/4;
			break;
		case GN_IMG_FORMAT_NV12:
			//NV12_VENUS format in msm8974 platform
			yStride = ALIGN_TO_SIZE(mVideoData.i32Width, ALIGN_TO_128);
			yScanline = ALIGN_TO_SIZE(mVideoData.i32Height, ALIGN_TO_32);
			mVideoData.ppu8Plane[1] = mVideoData.ppu8Plane[0] + yStride * yScanline;
			mVideoData.ppu8Plane[2] = NULL;
			break;
		default:
			mVideoData.ppu8Plane[1] = mVideoData.ppu8Plane[0] + mVideoData.i32Width * mVideoData.i32Height;
			mVideoData.ppu8Plane[2] = mVideoData.ppu8Plane[1] + mVideoData.i32Width * mVideoData.i32Height/4;
			break;
	}

	if (dumpFrame(&mVideoData, size, GN_CAMERA_DUMP_FRM_VIDEO_INPUT)) {
		dumpFrameRef ++;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_FACE_BEAUTY) & mask) {
		res = processFaceBeauty(&mVideoData, GN_DATA_TYPE_VIDEO);
		if (res != 0) {
			PRINTD("Failed to process face beauty[#%ld].", res);
		}
	}
	
	if (dumpFrame(&mVideoData, size, GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}

	pthread_mutex_unlock(&mMutex); 

	return res;
}

int32 
ArcSoftCameraFeature::
processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask)
{
	MRESULT res = 0;
	int dumpFrameRef = 0;
	int stride = 0;
	int scanline = 0;
	ASVLOFFSCREEN imageData;

	if (inputBuffer == NULL || size == 0 || width == 0 || height == 0) {
		PRINTE("unvalid parameters.");
		return -1;
	}
	
	pthread_mutex_lock(&mMutex);
	
	if ((mGNCameraFeature & (GN_CAMERA_FEATURE_FACE_BEAUTY|GN_CAMERA_FEATUER_NIGHT_SHOT
		| GN_CAMERA_FEATURE_EFFECT | GN_CAMERA_FEATUER_AGEGENDER_DETECTION
		| GN_CAMERA_FEATURE_PIC_ZOOM)) == 0) {
		pthread_mutex_unlock(&mMutex); 
		return res;
	}

	//we need to align these param based on special platform in memory
	if (ALIGN_FORMAT != ALIGN_TO_32) {
		stride = ALIGN_TO_SIZE(width, ALIGN_FORMAT);
		scanline = ALIGN_TO_SIZE(height, ALIGN_FORMAT);
	} else {
		stride = width;
		scanline = height;
	}

	imageData.i32Width	= stride;
	imageData.i32Height = scanline;
	
	switch (format) {
		case GN_IMG_FORMAT_YV12:
			imageData.u32PixelArrayFormat = ASVL_PAF_YV12;
			imageData.pi32Pitch[0] = stride;
			imageData.pi32Pitch[1] = stride / 2;
			imageData.pi32Pitch[2] = stride / 2;
			imageData.ppu8Plane[0] = (MByte*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + imageData.i32Width * imageData.i32Height;
			imageData.ppu8Plane[2] = imageData.ppu8Plane[1] + imageData.i32Width * imageData.i32Height / 4;
			break;
		case GN_IMG_FORMAT_YUYV:
			imageData.u32PixelArrayFormat = ASVL_PAF_YUYV;
			imageData.pi32Pitch[0] = stride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (MByte*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_NV21:
			imageData.u32PixelArrayFormat = ASVL_PAF_NV21;
			imageData.pi32Pitch[0] = stride;
			imageData.pi32Pitch[1] = stride;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (MByte*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + imageData.i32Width * imageData.i32Height;
			imageData.ppu8Plane[2] = NULL;
			break;
		default:
			imageData.u32PixelArrayFormat = ASVL_PAF_YUYV;
			imageData.pi32Pitch[0] = stride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (MByte*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
	}

	//dump source frame if needed
	if (dumpFrame(&imageData, size, GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT)) {
		dumpFrameRef ++;
	}

	//process night shot
	if ((mGNCameraFeature & GN_CAMERA_FEATUER_NIGHT_SHOT) & mask) {
		res = processNightShot(&imageData, width, height);
		if (res != 0) {
			PRINTE("Failed to process night shot");
		}
	}

	//process age gender detection
	if ((mGNCameraFeature & GN_CAMERA_FEATUER_AGEGENDER_DETECTION) & mask) {
		res = processAgeGenderDetection(&imageData, GN_DATA_TYPE_SNAPSHOT);
		if (res != 0) {
			PRINTE("Failed to process age gender detection.");
		}
	}

	//process face beauty
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_FACE_BEAUTY) & mask) {
		res = processFaceBeauty(&imageData, GN_DATA_TYPE_SNAPSHOT);
		if (res != 0) {
			PRINTE("Failed to process face beauty.");
		}
	}

	//process live effect
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_EFFECT) & mask) {
		res = processLiveEffect(&imageData, mGNCameraFeature & GN_CAMERA_FEATUER_MIRROR);
		if (res != 0) {
			PRINTE("Failed to process live effect.");
		}
	}

	//process pic zoom
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_PIC_ZOOM) & mask) {
		res = processPicZoom(&imageData);
		if (res != 0) {
			PRINTE("Failed to process pic zoom");
		}
	}

	//dump proessed frame if needed
	if (dumpFrame(&imageData, size, GN_CAMERA_DUMP_FRM_SNAPSHOT_OUTPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}
	
	pthread_mutex_unlock(&mMutex); 
	
	return res;
}

int32 
ArcSoftCameraFeature::
processFaceBeauty(LPASVLOFFSCREEN param, GNDataType dataType)
{
	MRESULT res = 0;

	if (mArcSoftFaceBeauty != NULL) {
		res = mArcSoftFaceBeauty->processFaceBeauty(param, dataType);
	}
	
	return res;
}

int32
ArcSoftCameraFeature::
processAgeGenderDetection(LPASVLOFFSCREEN param, GNDataType dataType)
{
	MRESULT res = 0;

	if (mArcSoftAgeGenderDetection != NULL) {
		res = mArcSoftAgeGenderDetection->processAgeGenderDetection(param, dataType);
	}

	return res;
}

int32 
ArcSoftCameraFeature::
processGestureShot(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;
/*	
	if (mArcSoftGestureShot != NULL) {
		res = mArcSoftGestureShot->processGestureShot(param);
	}
*/
	return res;
}

int32 
ArcSoftCameraFeature::
processSceneDetection(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;
	
	if (mArcSoftSceneDetection != NULL) {
		res = mArcSoftSceneDetection->processSceneDetection(param);
		if (res != 0) {
			PRINTE("Failed to process scene detection [#%ld].", res);
		}
	}

	return res;
}

int32 
ArcSoftCameraFeature::
processNightShot(LPASVLOFFSCREEN param, int width, int height)
{
	MRESULT res = 0;

	if (mArcSoftNightShot != NULL) {
		res = mArcSoftNightShot->processNightShot(param, width, height);
		if (res != 0) {
			PRINTE("Failed to process night shot [#%ld].", res);
		}
	}

	return res;
}

int32 
ArcSoftCameraFeature::
processNightVideo(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;

	if (mArcSoftNightVideo != NULL) {
		res = mArcSoftNightVideo->processNightVideo(param);
		if (res != 0) {
			PRINTE("Failed to process night video [#%ld].", res);
		}
	}

	return res;
}

int32 
ArcSoftCameraFeature::
processLiveEffect(LPASVLOFFSCREEN param, bool isMirror)
{
	MRESULT res = 0;

	if (mArcSoftLiveEffect!= NULL) {
		res = mArcSoftLiveEffect->processLiveEffect(param, isMirror);
		if (res != 0) {
			PRINTE("Failed to process live effect [#%ld].", res);
		}
	}

	return res;
}

int32
ArcSoftCameraFeature::
processPicZoom(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;
	
	if (mArcSoftPicZoom!= NULL) {
		res = mArcSoftPicZoom->processPicZoom(param);
		if (res != 0) {
			PRINTE("Failed to process night shot [#%ld].", res);
		}
	}

	return res;
}

bool 
ArcSoftCameraFeature::
dumpFrame(LPASVLOFFSCREEN imageData, MUInt32 size, int dump_type)
{
	bool ret = false;
	
    char value[PROPERTY_VALUE_MAX];
	property_get("persist.arcsoft.dumpbuffer", value, "0");
	int32_t enabled = atoi(value);

	if (enabled & GN_CAMERA_DUMP_FRM_MASK_ALL) {		
		if ((enabled & dump_type) && (imageData != NULL) 
			&& (imageData->ppu8Plane[0] != NULL) && size) {
			int width = 0;
			int height = 0;
			char fileStr[64] = {0};
			char timeStr[128] = {0};
			time_t timeCurr;
			struct tm *timeInfo = NULL;;
			
			time(&timeCurr);
			timeInfo = localtime(&timeCurr);
			strftime(timeStr, sizeof(timeStr),"/data/%Y%m%d%H%M%S", timeInfo);

			String8 filePath(timeStr);

			width = imageData->i32Width;
			height = imageData->i32Height;

			switch (dump_type) {
				case GN_CAMERA_DUMP_FRM_PREVIEW_INPUT:
					sprintf(fileStr, "p_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT:
					sprintf(fileStr, "p_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT:
					sprintf(fileStr, "s_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_OUTPUT:
					sprintf(fileStr, "s_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_INPUT:
					sprintf(fileStr, "v_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_OUTPUT:
					sprintf(fileStr, "v_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				default:
					break;
			}

			filePath.append(fileStr);

			FILE* fp = fopen(filePath.string(), "w");
			if (fp == NULL) {
				PRINTE("fail to open file to save img: %s", filePath.string());
				return false;
			}

			fwrite(imageData->ppu8Plane[0], 1, size , fp);
			
			fclose(fp);

			ret = true;
		}
	}
	
    return ret;
}

};

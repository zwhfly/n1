#ifndef __AMIMAGEPROCESS_H__
#define __AMIMAGEPROCESS_H__

#include "amcomdef.h"
#include "amdisplay.h"

#if defined(__MIPSYMBIAN32__)
#include <e32def.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__MIPSYMBIAN32__)
	#define MIP_IMPORT IMPORT_C
#else
	#define MIP_IMPORT
#endif

#define MIP_IN
#define MIP_OUT
#define MIP_INOUT
#define MIP_FORMULA_SZIE (sizeof(MInt32) * 4 * 3)
#define MIP_AUTOFIX_FORMULA_SIZE (sizeof(MInt32) * 30)

//Processor Type
#define MIP_PROCESSOR_ARM7					0x001	//Reserved; ARM7 series
#define MIP_PROCESSOR_ARM9E					0x002	//ARM9 series with enhance DSP instruction, bilinear interpolation will get better performance
#define MIP_PROCESSOR_ARM11					0x003	//ARM11 series, bilinear interpolation will get better performance
#define MIP_PROCESSOR_XSCALE				0x004	//Intel XScale series, bilinear interpolation and non stretch convert will get better performance.

//error code
#define MERR_UNMERGEABLE					(MERR_BASIC_BASE+0x100)

#define MIP_EID_BASE					0x1000
#define	MIP_EID_MERGE					(MIP_EID_BASE + (1<<8))
#define	MIP_EID_FILLCOLOR				(MIP_EID_BASE + (2<<8))
#define	MIP_EID_BRIGHTNESS_CONTRAST		(MIP_EID_BASE + (3<<8))
#define	MIP_EID_HUE_SATURATION 			(MIP_EID_BASE + (4<<8))
#define	MIP_EID_GAMMA					(MIP_EID_BASE + (5<<8))
#define	MIP_EID_AUTOLEVEL				(MIP_EID_BASE + (6<<8))
#define	MIP_EID_GRAY					(MIP_EID_BASE + (7<<8))
#define	MIP_EID_NEGATIVE				(MIP_EID_BASE + (8<<8))
#define	MIP_EID_SEPIA					(MIP_EID_BASE + (9<<8))
#define	MIP_EID_SHARPEN		 			(MIP_EID_BASE + (10<<8))
#define	MIP_EID_BLUR					(MIP_EID_BASE + (11<<8))
#define	MIP_EID_DESPECKLE				(MIP_EID_BASE + (12<<8))
#define	MIP_EID_EMBOSS					(MIP_EID_BASE + (13<<8))
#define	MIP_EID_SKETCH					(MIP_EID_BASE + (14<<8))
#define	MIP_EID_COLORCHANNEL			(MIP_EID_BASE + (15<<8))
#define	MIP_EID_DITHER		 			(MIP_EID_BASE + (16<<8))
#define	MIP_EID_SOLARIZE				(MIP_EID_BASE + (17<<8))
#define MIP_EID_OILPAINTING				(MIP_EID_BASE + (18<<8))
#define MIP_EID_PINCH					(MIP_EID_BASE + (19<<8))
#define MIP_EID_DENOISE					(MIP_EID_BASE + (20<<8))
#define MIP_EID_WARPING					(MIP_EID_BASE + (21<<8))
#define MIP_EID_NOISE					(MIP_EID_BASE + (22<<8))

#define MIP_EID_OLDPHOTO				(MIP_EID_BASE + (23<<8))
#define MIP_EID_GRAYNEGATIVE			(MIP_EID_BASE + (24<<8))
#define MIP_EID_BULGE					(MIP_EID_BASE + (25<<8))
#define MIP_EID_MIRROR					(MIP_EID_BASE + (26<<8))
#define MIP_EID_SPOTLIGHT				(MIP_EID_BASE + (27<<8))
#define MIP_EID_STAMP					(MIP_EID_BASE + (28<<8))
#define MIP_EID_POSTERIZE				(MIP_EID_BASE + (29<<8))
#define MIP_EID_COLORBOOST				(MIP_EID_BASE + (30<<8))
#define MIP_EID_MOONLIGHT				(MIP_EID_BASE + (31<<8))
#define MIP_EID_FOG						(MIP_EID_BASE + (32<<8))
#define MIP_EID_FOCUS					(MIP_EID_BASE + (33<<8))
#define MIP_EID_FROSTEDGLASS			(MIP_EID_BASE + (34<<8))
#define MIP_EID_ANTIQUE					(MIP_EID_BASE + (35<<8))
#define MIP_EID_GLOW					(MIP_EID_BASE + (36<<8))
#define MIP_EID_CARTOON					(MIP_EID_BASE + (37<<8))
#define MIP_EID_GLASSFILTER				(MIP_EID_BASE + (38<<8))
#define MIP_EID_PUZZLE					(MIP_EID_BASE + (39<<8))
#define MIP_EID_3D_GRID					(MIP_EID_BASE + (40<<8))
#define MIP_EID_SURFACE					(MIP_EID_BASE + (41<<8))
#define MIP_EID_SPLASH					(MIP_EID_BASE + (42<<8))
#define MIP_EID_RAINDROP				(MIP_EID_BASE + (43<<8))
#define MIP_EID_NEON					(MIP_EID_BASE + (44<<8))
#define MIP_EID_FIRELIGHT				(MIP_EID_BASE + (45<<8))
#define MIP_EID_CHARCOAL				(MIP_EID_BASE + (46<<8))
#define MIP_EID_WATERCOLOR				(MIP_EID_BASE + (47<<8))//added bu lbzhao
#define MIP_EID_FLAME					(MIP_EID_BASE + (48<<8))
#define MIP_EID_WATERDROPS				(MIP_EID_BASE + (49<<8))
#define MIP_EID_MOSAIC					(MIP_EID_BASE + (50<<8))
#define MIP_EID_LAKE					(MIP_EID_BASE + (51<<8))

#define MIP_EID_STRAIGHTEN				(MIP_EID_BASE + (52<<8))

#define MIP_EID_AUTOCONTRAST			(MIP_EID_BASE + (53<<8))
#define MIP_EID_AUTOCOLOR				(MIP_EID_BASE + (54<<8))
#define MIP_EID_AUTOEXPOSURE			(MIP_EID_BASE + (55<<8))

#define MIP_EID_WARMCOLOR				(MIP_EID_BASE + (56<<8))
#define MIP_EID_COLDCOLOR				(MIP_EID_BASE + (57<<8))
#define MIP_EID_HIGHLIGHTSSHADOWS		(MIP_EID_BASE + (58<<8))
#define MIP_EID_WHITEBALANCE			(MIP_EID_BASE + (59<<8))
#define MIP_EID_FOCALWHITEBLACK			(MIP_EID_BASE + (60<<8))
#define MIP_EID_DYNAMICLIGHTING			(MIP_EID_BASE + (61<<8))
#define MIP_EID_MOONNIGHT               (MIP_EID_BASE + (62<<8))//added for moon night
#define MIP_EID_GPEN                    (MIP_EID_BASE + (63<<8))//added for G pen
#define MIP_EID_VIVID                   (MIP_EID_BASE + (64<<8))//added for vivid
#define MIP_EID_LOMO                    (MIP_EID_BASE + (65<<8))//added for lomo
#define MIP_EID_DIFFUSE                 (MIP_EID_BASE + (66<<8))//added for diffuse
#define MIP_EID_MILKY                   (MIP_EID_BASE + (67<<8))//added for milky
#define MIP_EID_AUTOFIX                 (MIP_EID_BASE + (68<<8))
#define MIP_EID_AUTOFIX_BRIGHTNESS      (MIP_EID_BASE + (69<<8))
#define MIP_EID_HALFTONE                (MIP_EID_BASE + (70<<8))
#define MIP_EID_PMDENOISE				(MIP_EID_BASE + (71<<8))//added for pmdenoise
#define MIP_EID_SINGLECOLOR				(MIP_EID_BASE + (72<<8))//added for singlecolor
#define MIP_EID_KALEIDOSCOPE			(MIP_EID_BASE + (73<<8))
#define MIP_EID_FISHEYE					(MIP_EID_BASE + (74<<8))
#define MIP_EID_POLAROID				(MIP_EID_BASE + (75<<8))
#define MIP_EID_MINIATURE				(MIP_EID_BASE + (76<<8))
#define MIP_EID_PARTIALCOLOR			(MIP_EID_BASE + (77<<8))
#define MIP_EID_NOSTALGIC				(MIP_EID_BASE + (78<<8))
#define MIP_EID_VINTAGE					(MIP_EID_BASE + (79<<8))
#define MIP_EID_DEEPQUITE				(MIP_EID_BASE + (80<<8))
#define MIP_EID_COLDBLUE				(MIP_EID_BASE + (81<<8))
#define MIP_EID_GOTHIC					(MIP_EID_BASE + (82<<8))
#define MIP_EID_PURPLE					(MIP_EID_BASE + (83<<8))
#define MIP_EID_VINTAGEBLACKWHITE		(MIP_EID_BASE + (84<<8))
#define MIP_EID_FISHEYEMAGNIFIER		(MIP_EID_BASE + (85<<8))
#define MIP_EID_FOURCOLORPOSTER			(MIP_EID_BASE + (86<<8))
#define MIP_EID_COLORGRADIENT			(MIP_EID_BASE + (87<<8))
#define MIP_EID_POP						(MIP_EID_BASE + (88<<8))
#define MIP_EID_SUNNY					(MIP_EID_BASE + (89<<8))
#define MIP_EID_CURVEBRIGHTNESS			(MIP_EID_BASE + (90<<8))
#define MIP_EID_NEWSKETCH				(MIP_EID_BASE + (91<<8))
#define MIP_EID_SUNSET					(MIP_EID_BASE + (92<<8))
#define MIP_EID_NEWEMBOSS				(MIP_EID_BASE + (93<<8))
#define MIP_EID_REVERSALFILM			(MIP_EID_BASE + (94<<8))
#define MIP_EID_LEVELS					(MIP_EID_BASE + (95<<8))
#define MIP_EID_CURVE					(MIP_EID_BASE + (96<<8))
#define MIP_EID_NEW_BRIGHTNESS_CONTRAST	(MIP_EID_BASE + (97<<8))
#define MIP_EID_EXPOSURE						(MIP_EID_BASE + (98<<8))
#define MIP_EID_SATURATION						(MIP_EID_BASE + (99<<8))
#define MIP_EID_DEFINITION						(MIP_EID_BASE + (100<<8))
#define MIP_EID_HIGHLIGHT_BRIGHT_DARK_SHADOW	(MIP_EID_BASE + (101<<8))
#define MIP_EID_AUTOFIX_WHITEBALANCE			(MIP_EID_BASE + (102<<8))
#define MIP_EID_HSLCOLOR						(MIP_EID_BASE + (103<<8))
#define MIP_EID_SEPARATETONE					(MIP_EID_BASE + (104<<8))
#define MIP_EID_NATURALSATURATION				(MIP_EID_BASE + (105<<8))
#define MIP_EID_NEWCARTOON						(MIP_EID_BASE + (106<<8))
#define MIP_EID_GAUSSBLUR						(MIP_EID_BASE + (107<<8))
#define MIP_EID_SELECTIVECOLOR					(MIP_EID_BASE + (108<<8))
#define MIP_EID_DISTORTION          			(MIP_EID_BASE + (109<<8))
#define MIP_EID_CLASSICALNOSTALGIA				(MIP_EID_BASE + (110<<8))
#define MIP_EID_HSBADJUST                       (MIP_EID_BASE + (111<<8))
#define MIP_EID_CHROMATICABERRATION				(MIP_EID_BASE + (112<<8))
#define MIP_EID_NOSTALGICFRESH					(MIP_EID_BASE + (113<<8))
#define MIP_EID_COOLTIME						(MIP_EID_BASE + (114<<8))
#define MIP_EID_COOLCHILDREN					(MIP_EID_BASE + (115<<8))
#define MIP_EID_DARKCORNER  					(MIP_EID_BASE + (116<<8))
#define MIP_EID_CHANNELMIXER					(MIP_EID_BASE + (117<<8))
#define MIP_EID_FEATHER							(MIP_EID_BASE + (118<<8))
#define MIP_EID_COLORBALANCE					(MIP_EID_BASE + (119<<8))
#define MIP_EID_NORDICMONOCHROME				(MIP_EID_BASE + (120<<8))
#define MIP_EID_AMMAT							(MIP_EID_BASE + (121<<8))
#define MIP_EID_RETOUCH							(MIP_EID_BASE + (122<<8))
#define MIP_EID_GIRLTIMES						(MIP_EID_BASE + (123<<8))
#define MIP_EID_GIRLSOFTLIGHT					(MIP_EID_BASE + (124<<8))
#define MIP_EID_BLACKWHITECONTRAST				(MIP_EID_BASE + (125<<8))
#define MIP_EID_BLACKWHITEDETAILS				(MIP_EID_BASE + (126<<8))
#define MIP_EID_BLACKWHITENOISE					(MIP_EID_BASE + (127<<8))
#define MIP_EID_FOREST							(MIP_EID_BASE + (128<<8))
#define MIP_EID_NEWLOMO							(MIP_EID_BASE + (129<<8))
#define MIP_EID_NEWOILPAINT						(MIP_EID_BASE + (130<<8))
#define MIP_EID_POLAROIDPURPLE					(MIP_EID_BASE + (131<<8))
#define MIP_EID_POLAROIDYELLOWGREEN				(MIP_EID_BASE + (132<<8))
#define MIP_EID_BUILDING						(MIP_EID_BASE + (133<<8))
#define MIP_EID_FRESH							(MIP_EID_BASE + (134<<8))
#define MIP_EID_SOFTLIGHT						(MIP_EID_BASE + (135<<8))
#define MIP_EID_FRESHPLANT						(MIP_EID_BASE + (136<<8))
#define MIP_EID_NEWNATURALSATURATION			(MIP_EID_BASE + (137<<8))
#define MIP_EID_CANDYCOLORS						(MIP_EID_BASE + (138<<8))
#define MIP_EID_REMOVEEDGE						(MIP_EID_BASE + (139<<8))
#define MIP_EID_NEWPARTIALCOLOR					(MIP_EID_BASE + (140<<8))
#define MIP_EID_HUDSON							(MIP_EID_BASE + (141<<8))
#define MIP_EID_BRANNAN							(MIP_EID_BASE + (142<<8))
#define MIP_EID_TOASTER							(MIP_EID_BASE + (143<<8))
#define MIP_EID_LOFI							(MIP_EID_BASE + (144<<8))
#define MIP_EID_VALENCIA						(MIP_EID_BASE + (145<<8))
#define MIP_EID_NEWSTRAIGHTEN					(MIP_EID_BASE + (146<<8))
#define MIP_EID_DARKMOOD						(MIP_EID_BASE + (147<<8))
#define MIP_EID_HDR								(MIP_EID_BASE + (148<<8))
#define MIP_EID_FLEETINGTIME					(MIP_EID_BASE + (149<<8))
#define MIP_EID_CRAYON							(MIP_EID_BASE + (150<<8))
#define MIP_EID_SNOWFLAKES						(MIP_EID_BASE + (151<<8))
#define MIP_EID_NEWREVERSAL						(MIP_EID_BASE + (152<<8))
#define MIP_EID_WARMLOMO						(MIP_EID_BASE + (153<<8))
#define MIP_EID_COLDLOMO						(MIP_EID_BASE + (154<<8))
#define MIP_EID_SOFTPINK						(MIP_EID_BASE + (155<<8))
#define MIP_EID_RIXI							(MIP_EID_BASE + (156<<8))
#define MIP_EID_COMPLEXIONOPTIMIZING			(MIP_EID_BASE + (157<<8))
#define MIP_EID_SHARPENUSM						(MIP_EID_BASE + (158<<8))
#define MIP_EID_LIGHTLEAK						(MIP_EID_BASE + (159<<8))
#define MIP_EID_PAINTOIL						(MIP_EID_BASE + (160<<8))
#define MIP_EID_BLACKWHITE						(MIP_EID_BASE + (161<<8))
#define MIP_EID_SMALLFRESH						(MIP_EID_BASE + (162<<8))
#define MIP_EID_GORGEOUS						(MIP_EID_BASE + (163<<8))
#define MIP_EID_NEWCOLORBOOTS					(MIP_EID_BASE + (164<<8))
#define MIP_EID_NEWWARMCOLOR					(MIP_EID_BASE + (165<<8))
#define MIP_EID_NATURE							(MIP_EID_BASE + (166<<8))
#define MIP_EID_REVERSELOMO						(MIP_EID_BASE + (167<<8))
#define MIP_EID_AIBAOCOLOR						(MIP_EID_BASE + (168<<8))
#define MIP_EID_RE_NEWREVERSAL					(MIP_EID_BASE + (169<<8))
#define MIP_EID_RE_FLEETINGTIME					(MIP_EID_BASE + (170<<8))
#define MIP_EID_GOLDENTIME						(MIP_EID_BASE + (171<<8))
#define MIP_EID_RE_WARMLOMO						(MIP_EID_BASE + (172<<8))
#define MIP_EID_RE_SOFTPINK						(MIP_EID_BASE + (173<<8))
#define MIP_EID_RE_RIXI							(MIP_EID_BASE + (174<<8))
#define MIP_EID_BLACKWHITE2						(MIP_EID_BASE + (175<<8))
#define MIP_EID_FINEFOOD						(MIP_EID_BASE + (176<<8))
#define MIP_EID_AMARO							(MIP_EID_BASE + (177<<8))
#define MIP_EID_WHITE							(MIP_EID_BASE + (178<<8))
#define MIP_EID_BRIGHTLY						(MIP_EID_BASE + (179<<8))

//ArcFilm video filters
#define MIP_EID_IZAKAYA     					(MIP_EID_BASE + (210<<8))
#define MIP_EID_VINTAGEBREEZE					(MIP_EID_BASE + (234<<8))
#define MIP_EID_JAEJUNGGIM						(MIP_EID_BASE + (238<<8))
#define MIP_EID_MOMENTO							(MIP_EID_BASE + (259<<8))
#define MIP_EID_GLAMOURLIFE						(MIP_EID_BASE + (260<<8))
#define MIP_EID_NEW_FASHION                     (MIP_EID_BASE + (279<<8))
#define MIP_EID_COLORS							(MIP_EID_BASE + (280<<8))

//Yunmall video filters or Themes filters
#define MIP_EID_KALEIDA_HORIZONTAL				(MIP_EID_BASE + (180<<8))
#define MIP_EID_CHALKBOARD_THEME				(MIP_EID_BASE + (181<<8))
#define MIP_EID_COLOR_ENHANCEMENT				(MIP_EID_BASE + (182<<8))
#define MIP_EID_BW_SEPIA						(MIP_EID_BASE + (183<<8))
#define MIP_EID_COLOR_OVERLAY					(MIP_EID_BASE + (184<<8))
#define MIP_EID_ICE     						(MIP_EID_BASE + (185<<8))
#define MIP_EID_GOLDEN                          (MIP_EID_BASE + (186<<8))
#define MIP_EID_DIAMOND     					(MIP_EID_BASE + (187<<8))
#define MIP_EID_WEDDINGWARM						(MIP_EID_BASE + (188<<8))
#define MIP_EID_LOMO2							(MIP_EID_BASE + (190<<8))
#define MIP_EID_HANDWRITTEN_SECTION01			(MIP_EID_BASE + (213<<8))
#define MIP_EID_HANDWRITTEN_SECTION02     		(MIP_EID_BASE + (195<<8))
#define MIP_EID_SQUARE							(MIP_EID_BASE + (236<<8))
#define MIP_EID_CAMERA							(MIP_EID_BASE + (239<<8))

//Yunmall photo filters
#define MIP_EID_BLUEGEL_A						(MIP_EID_BASE + (191<<8))
#define MIP_EID_FLARE07     					(MIP_EID_BASE + (192<<8))
#define MIP_EID_FLARE08     					(MIP_EID_BASE + (193<<8))
#define MIP_EID_SEPIA_YELLOW					(MIP_EID_BASE + (194<<8))
#define MIP_EID_FLARE06							(MIP_EID_BASE + (196<<8))
#define MIP_EID_FILTER05						(MIP_EID_BASE + (197<<8))
#define MIP_EID_FILTER1915						(MIP_EID_BASE + (198<<8))
#define MIP_EID_FILTER1995						(MIP_EID_BASE + (199<<8))
#define MIP_EID_BLACKWHITE_A     				(MIP_EID_BASE + (200<<8))
#define MIP_EID_BLACKWHITE_B    				(MIP_EID_BASE + (201<<8))
#define MIP_EID_VINTAGE_A     					(MIP_EID_BASE + (202<<8))
#define MIP_EID_VINTAGE_B     					(MIP_EID_BASE + (203<<8))
#define MIP_EID_VINTAGE_C     					(MIP_EID_BASE + (204<<8))
#define MIP_EID_VINTAGE_H     					(MIP_EID_BASE + (205<<8))
#define MIP_EID_LOMOGRAPHY_55     				(MIP_EID_BASE + (206<<8))
#define MIP_EID_LOMOGRAPHY_56     				(MIP_EID_BASE + (207<<8))
#define MIP_EID_LOMOGRAPHY_57     				(MIP_EID_BASE + (208<<8))
#define MIP_EID_LOMOGRAPHY_58     				(MIP_EID_BASE + (209<<8))
#define MIP_EID_SOFTNESS						(MIP_EID_BASE + (211<<8))
#define MIP_EID_FILTER14     					(MIP_EID_BASE + (212<<8))
#define MIP_EID_REDGELB							(MIP_EID_BASE + (214<<8))
#define MIP_EID_PRISM_MASKS						(MIP_EID_BASE + (215<<8))
#define MIP_EID_REDGELA							(MIP_EID_BASE + (216<<8))
#define MIP_EID_SKINTONE     					(MIP_EID_BASE + (217<<8))
#define MIP_EID_FILTER02     					(MIP_EID_BASE + (218<<8))
#define MIP_EID_AURORAMASTER					(MIP_EID_BASE + (219<<8))
#define MIP_EID_FOURCOLOURPANEL					(MIP_EID_BASE + (220<<8))
#define MIP_EID_GRUNGE							(MIP_EID_BASE + (221<<8))
#define MIP_EID_OLDGLASS						(MIP_EID_BASE + (222<<8))
#define MIP_EID_WATERCOLOR02					(MIP_EID_BASE + (223<<8))
#define MIP_EID_MULTIPLEEXPOSURE03				(MIP_EID_BASE + (224<<8))
#define MIP_EID_MULTIPLEEXPOSURE04				(MIP_EID_BASE + (225<<8))
#define MIP_EID_SEPIABROWN     					(MIP_EID_BASE + (226<<8))
#define MIP_EID_YELLOWGEL						(MIP_EID_BASE + (227<<8))
#define MIP_EID_SHARDSBW						(MIP_EID_BASE + (228<<8))
#define MIP_EID_SEPIARED     					(MIP_EID_BASE + (229<<8))
#define MIP_EID_GEOMETRICVERTICAL				(MIP_EID_BASE + (230<<8))
#define MIP_EID_WARMOVERLAY     			    (MIP_EID_BASE + (231<<8))
#define MIP_EID_GLITCH01	     			    (MIP_EID_BASE + (232<<8))
#define MIP_EID_PURPLEOVERLAY     			    (MIP_EID_BASE + (233<<8))
#define MIP_EID_GLITCH02						(MIP_EID_BASE + (235<<8))
#define MIP_EID_SHARDPRISM						(MIP_EID_BASE + (237<<8))
#define MIP_EID_POLARIZED						(MIP_EID_BASE + (240<<8))
#define MIP_EID_FILTER1965						(MIP_EID_BASE + (241<<8))
#define MIP_EID_AMARO_INSTAGRAM					(MIP_EID_BASE + (243<<8))
#define MIP_EID_MOTIONBLUR     					(MIP_EID_BASE + (244<<8))
#define MIP_EID_XPROIIINSTAGRAM					(MIP_EID_BASE + (245<<8))

#define MIP_EID_15_02_C1VSCOCAM					(MIP_EID_BASE + (246<<8))
#define MIP_EID_15_12_T3VSCOCAM					(MIP_EID_BASE + (247<<8))
#define MIP_EID_15_13_M1VSCOCAM					(MIP_EID_BASE + (248<<8))
#define MIP_EID_15_14_S3VSCOCAM					(MIP_EID_BASE + (249<<8))
#define MIP_EID_15_04_G3VSCOCAM					(MIP_EID_BASE + (250<<8))
#define MIP_EID_15_09_N3_VSCOCAM                (MIP_EID_BASE + (251<<8))
#define MIP_EID_15_07_N2_VSCOCAM				(MIP_EID_BASE + (252<<8))
#define MIP_EID_15_08_T1_VSCOCAM				(MIP_EID_BASE + (253<<8))
#define MIP_EID_15_11_M2_VSCOCAM                (MIP_EID_BASE + (254<<8))
#define MIP_EID_15_05_F2VSCOCAM					(MIP_EID_BASE + (255<<8))
#define MIP_EID_15_10_RISE_INSTAGRAM            (MIP_EID_BASE + (256<<8))
#define MIP_EID_15_06_N1VSCOCAM					(MIP_EID_BASE + (257<<8))
#define MIP_EID_15_15_A2VSCOCAM					(MIP_EID_BASE + (258<<8))
#define MIP_EID_COLORTEMPERATURE				(MIP_EID_BASE + (261<<8))



//Yunmall photo filters to do or to fix
#define MIP_EID_POLARIZED_MASTER				(MIP_EID_BASE + (189<<8))



//Yunmall Photo filter last 40
#define MIP_EID_SHARDSINVERT					(MIP_EID_BASE + (262<<8))
#define MIP_EID_MULTIEXPOSURE02					(MIP_EID_BASE + (263<<8))
#define MIP_EID_GEOMETRICHEXAGON				(MIP_EID_BASE + (264<<8))
#define MIP_EID_KALEIDOSCOPE_YUNMALL			(MIP_EID_BASE + (265<<8))

#define MIP_EID_50_COLOR_EFFECT					(MIP_EID_BASE + (266<<8))
#define MIP_EID_81_GEOMETRIC_SQUARE				(MIP_EID_BASE + (267<<8))
#define MIP_EID_83_NEON							(MIP_EID_BASE + (268<<8))
#define MIP_EID_A14								(MIP_EID_BASE + (269<<8))
#define MIP_EID_62_MULTIEXPOSURE01				(MIP_EID_BASE + (270<<8))
#define MIP_EID_75_GEOMETRIC					(MIP_EID_BASE + (271<<8))
#define MIP_EID_77_GEOMETRICHORIZONTAL			(MIP_EID_BASE + (272<<8))
#define MIP_EID_84_COLORFULLIGHTSTREAKS			(MIP_EID_BASE + (273<<8))
#define MIP_EID_PAINTSTROKES02					(MIP_EID_BASE + (274<<8))

#define MIP_EID_80_GEOMETRICTRIANGLES           (MIP_EID_BASE + (275<<8))
#define MIP_EID_A16                             (MIP_EID_BASE + (276<<8))
#define MIP_EID_85_WOOD                         (MIP_EID_BASE + (277<<8))
#define MIP_EID_92_PAINTSTROKES                 (MIP_EID_BASE + (278<<8))
#define MIP_EID_93_TYPE01		                (MIP_EID_BASE + (281<<8))
#define MIP_EID_94_TYPE02		                (MIP_EID_BASE + (282<<8))
#define MIP_EID_96_TYPE04		                (MIP_EID_BASE + (283<<8))

#define MIP_EID_A01								(MIP_EID_BASE + (284<<8))
#define MIP_EID_A08								(MIP_EID_BASE + (285<<8))
#define MIP_EID_A11								(MIP_EID_BASE + (286<<8))
#define MIP_EID_11_MONOCHROME1		        	(MIP_EID_BASE + (287<<8))
//#define MIP_EID_14_MONOCHROME4		        (MIP_EID_BASE + (288<<8))
#define MIP_EID_15_ORANGEGEL		            (MIP_EID_BASE + (289<<8))

#define MIP_EID_02_SOFTNESS						(MIP_EID_BASE + (290<<8))
#define MIP_EID_A10								(MIP_EID_BASE + (291<<8))
#define MIP_EID_A13								(MIP_EID_BASE + (292<<8))
#define MIP_EID_12_MONOCHROME					(MIP_EID_BASE + (293<<8))
#define MIP_EID_13_MONOCHROME					(MIP_EID_BASE + (294<<8))
#define MIP_EID_14_MONOCHROME					(MIP_EID_BASE + (295<<8))

#define MIP_EID_16_ICE							(MIP_EID_BASE + (296<<8))
#define MIP_EID_30_VINTAGE_F					(MIP_EID_BASE + (297<<8))
#define MIP_EID_A02								(MIP_EID_BASE + (298<<8))
#define MIP_EID_A05								(MIP_EID_BASE + (299<<8))
#define MIP_EID_44_LENSFLARE					(MIP_EID_BASE + (300<<8))

#define MIP_EID_40_COLORREVEAL					(MIP_EID_BASE + (301<<8))
#define MIP_EID_69_WATERCOLOR01					(MIP_EID_BASE + (302<<8))
#define MIP_EID_91_PAINTSTROKES					(MIP_EID_BASE + (303<<8))
#define MIP_EID_A04								(MIP_EID_BASE + (304<<8))

#define MIP_EID_ROCKON1							(MIP_EID_BASE + (305<<8))
#define MIP_EID_ROCKON2							(MIP_EID_BASE + (306<<8))
#define MIP_EID_LITTLEMONSTER					(MIP_EID_BASE + (307<<8))
#define MIP_EID_MOMENTO_V1						(MIP_EID_BASE + (308<<8))

#define MIP_EID_LOMO2_NOMASK					(MIP_EID_BASE + (309<<8))

//add by zxj
#define MIP_EID_TEST02							(MIP_EID_BASE + (310<<8))
#define MIP_EID_C01								(MIP_EID_BASE + (311<<8))
#define MIP_EID_C02				                (MIP_EID_BASE + (312<<8))
#define MIP_EID_W01				                (MIP_EID_BASE + (313<<8))
#define MIP_EID_W02				                (MIP_EID_BASE + (314<<8))
#define MIP_EID_W03				                (MIP_EID_BASE + (315<<8))


//straight parameter of QualityOrSpeed
#define  MIP_STRAIGHTEN_HIGHSPEED 0
#define  MIP_STRAIGHTEN_HIGHQUALITY 1

//get/set property
#define PROPERTY_GET_TOTALFRAMES          0x00
#define PROPERTY_SET_CURRENTFRAME         0x01

//typedef enum __tag_wbtype {
//	ASSHOT, AUTOWB, DAYLIGHT, CLOUDY, SHADE, TUNGSTEN, FLUORESCENT, FLASH, CUSTOMWB
//}MWBTYPE;
//for white balance
#define  ASSHOT       0x00
#define  AUTOWB       0x01
#define  DAYLIGHT     0x02
#define  CLOUDY       0x03
#define  SHADE        0x04
#define  TUNGSTEN     0x05
#define  FLUORESCENT  0x06
#define  FLASH        0x07
#define  CUSTOMWB     0x08

//typedef enum _tag_lomotype
//{
//	LOMO_WARMCOLOR,LOMO_COLDCOLOR,LOMO_DARKCORNER
//}LomoType;
//for lomo type
#define  LOMO_WARMCOLOR       0x00
#define  LOMO_COLDCOLOR       0x01
#define  LOMO_DARKCORNER      0x02
#define  LOMO_MAGIC           0x03
#define  LOMO_SOFT			  0x04

//ColorChanel for HSBPARAM
#define HSB_ALLIMAGE          		0x00
#define HSB_RED               		0x01
#define HSB_YELLOW            		0x02
#define HSB_GREEN             		0x03
#define HSB_CYAN              		0x04
#define HSB_BLUE              		0x05
#define HSB_MAGENTA           		0x06

#define AMMAT_RESERVE_LINE          0x00
#define AMMAT_REMOVE_LINE           0x01

//WaterFall
#define MIP_WF_PROP_INTENSITY		1/*An MInt32 datum greater than 1 is acceptable, default is 40*/
#define MIP_WF_PROP_WIDTH			2/*An MInt32 datum greater than 1 is acceptable, default is 20*/

typedef struct __tag_mippixel
{
	MDWord		dwSpaceID;		//ID of color space, sample: MPAF_RGB16_R5G6B5, MPAF_RGB24_R8G8B8, MPAF_I420, (MPAF_I420| MPAF_BT601_YCBCR)
	MInt32		lWidth;		//Specifies the width of the bitmap, in pixels
	MInt32		lHeight;		//Specifies the height of the bitmap, in pixels
} MIPPIXEL, *LPIPFPIXEL;

typedef struct __tag_mipdata
{
	MByte		*pPlane[MPAF_MAX_PLANES];
	MInt32		lPitch[MPAF_MAX_PLANES];
} MIPData, *LPIPData;

typedef struct __tag_complexionparam
{
	MInt32 lDermabrasionSize;//>0
	MDouble dDermabrasionStrength;//0-100
	MInt32 lWhiteningStrength;//0-100
	//MDWord dwColorType;
	//MInt32 lComplexionStrenght;
}ComplexionPARAM;

typedef struct __tag_gorgeousparam
{
	MDWord dwModle;//0-3

}GorgeousPARAM;

typedef struct __tag_mergefillcolorparam
{
	MDWord	dwTransparent;//for fillcolor and merge only 0 to 100
	MDWord	dwFillColor;//for fillcolor only
}MergeFillcolorPARAM;
typedef struct __tag_bricontrastparam
{
	MInt32	lBrightness;//Value for brightness adjust, -100~100, 0 means adjust none
	MInt32	lContrast;	//Value for contrast adjust, -100~100, 0 means adjust none
}BriContrastPARAM;
typedef struct __tag_huesaturationparam
{
	MInt32	lHue;//-180~180, 0 means adjust none
	MInt32	lSaturation;//-100~100, 0 means adjust none
}HueSaturationPARAM;

typedef struct __tag_sketchparam
{
	MInt32	lSketchThreshold;//0~255 80 for normal
	MInt32	IsGray;//0 or 1:stand for color or gray
}SketchPARAM;
typedef struct __tag_embossparam
{
	MInt32	lDepth ;//1~255 the intensity of effect
	MInt32	IsGray;//0 is color emboss
	//MInt32	lClrAction ;//set the color value
	//MInt32	lDirection ;//1~8;eight direction
}EmbossPARAM;

typedef struct __tag_diffuseparam
{
	MInt32	lSize ;//1~20 the intensity of effect
}DiffusePARAM;

typedef struct _tag_lomoparam
{
    MDWord lType;
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
	MByte bIntensity;
}LomoPARAM;
typedef struct __tag_surfaceparam
{
	MBITMAP bmpMask;
	MDWord  dwSurfaceLevel;//0, 1, 2
}SurfacePARAM;

typedef struct __tag_halftoneparam
{
	MBITMAP halftoneMask;
	MInt32  dwPatternSize;//(-10 ~ 10)
}HALFTONEPARAM;

typedef struct __tag_focusparam
{
	MInt32  lFocusRadius;//1~
	MInt32  lFocusCentX;//0 ~ ImageWidth
	MInt32  lFocusCentY;//0 ~ ImageHeight

}FOCUSPARAM;
typedef struct __tag_warpparam
{
	MDWord  IsInflate;
	MInt32	lWarpIntensity;//1~20
}WarpPARAM;
// for new
typedef struct __tag_autoprocessparam
{
	MInt32	lAutoProFormula[12];
}AutoProcessPARAM;
typedef struct __tag_colorchannelparam
{
	MInt32	lR; //-100~100
	MInt32	lG; //-100~100
	MInt32	lB; //-100~100
}ColorChannelPARAM;

typedef struct __tag_sharpenparam
{
	MInt32 SharpenALPHA; //0~100
}SharpenPARAM;

typedef struct __tag_blurparam
{
	MInt32 lBlurSize;//1~60
}BlurPARAM;

typedef struct __tag_oilpaintparam
{
	MInt32 lOilPaintDepth;//2~15
}OilPaintPARAM;

typedef struct __tag_noiseparam
{
	MInt32 lNoiseIntensity;//1~32
}NoisePARAM;

typedef struct __tag_posterizeparam
{
	MInt32 lPosterizeLevel;//2~10
}PosterizePARAM;

typedef struct __tag_m3dgridparam
{
	MInt32 l3DGridSize;//2~
}M3DGridPARAM;

typedef struct __tag_straightenparam
{
	MInt32 lStraightenAngle;//-15~15
	MDWord dwQualityOrSpeed;//MIP_STRAIGHTEN_HIGHSPEED or MIP_STRAIGHTEN_HIGHQUALITY
}StraightenPARAM;

typedef struct __tag_newstraightenparam
{
	MDouble dStraightenRadian;//
	MDWord dwQualityOrSpeed;//MIP_STRAIGHTEN_HIGHSPEED or MIP_STRAIGHTEN_HIGHQUALITY
}NewStraightenPARAM;

typedef struct __tag_mosaicparam
{
	MInt32 lMosaicSize;//2~12
}MosaicPARAM;

typedef struct __tag_cartoonparam
{
	MInt32 lCartoonDepth;//1-7
	MInt32 lCartoonThre;//2-20; 6 is best
}CartoonPARAM;

typedef struct __tag_newcartoonparam
{
	MInt32 lCartoonDepth;//1-7
	MInt32 lCartoonThre;//2-20; 6 is best
}NewCartoonPARAM;

typedef struct __tag_stampparam
{
	MInt32 lStampDepth;//1-7
	MInt32 lStampThre;//2-20; 6 is best
}StampPARAM;

typedef struct __tag_highlightshadowparam
{
	MInt32 lHighlights;//-100~100
	MInt32 lLights;//-100~100
	MInt32 lDarks;//-100~100
	MInt32 lShadows;//-100~100
}HighlightShadowPARAM;

typedef struct __tag_spotlightparam
{
	MInt32  lSpotLightRadius;//1~
	MInt32  lSpotLightCentX;//0 ~ ImageWidth
	MInt32  lSpotLightCentY;//0 ~ ImageHeight
}SpotLightPARAM;

typedef struct __tag_focalwhiteblackparam
{
	MInt32  lFocalWBRadius;//1~
	MInt32  lFocalWBCentX;//0 ~ ImageWidth
	MInt32  lFocalWBCentY;//0 ~ ImageHeight
}FocalWhiteBlackPARAM;

typedef struct  __tag_whitebalanceparam
{
	MDWord emWBtype;
	MInt32 lTemperature;//2000~12000
	MInt32 lTint;//-150~150
}WhiteBalancePARAM;

typedef struct __tag_dynamiclightparam {
	MInt32		lSceneKey;			// The destination luminance of scene (0~100)
	MInt32		lShadowInt;			// The destination intensity of shadow area (0~100)
	MByte		pGrayMap[256];      // The parameter of Outputing [256 Byte]
}DynamicLightPARAM;

typedef struct _tag_AutoFixParam
{
	MBool IsAutoLevel;

	MBool IsAutoColorBalance;
	MInt32 lColorBalanceRed;                 //-100 ~ 100
	MInt32 lColorBalanceGreen;				//-100 ~ 100
	MInt32 lColorBalanceBlue;				//-100 ~ 100

	MBool IsAutoSaturation;
	MInt32 lSaturationPara;		//-100 ~ 100

	MBool IsAutoBrightness;
	MInt32 lBriPara;				//-100 ~ 100

	MBool IsAutoContrast;
	MInt32 lConMax;					//0 ~ 511
	MInt32 lConMin;					//-256 ~ 255

	MBool IsAutoSharp;
	MInt32 lSharpPara;			//0 ~ 100

	MInt32 lShadow;				//0 ~ 100
	MInt32 lLuminance;			//-100 ~ 0

	MBool IsDeNoise;
	MVoid *ConfigAddr;
	MInt32 ConfigSize;

}AUTOFIXPARAM, *LPAUTOFIXPARAM;

typedef struct __tag_effectinfo
{
	MDWord	dwEffectID;
	MVoid	*pEffect;
	MDWord	dwParamSize;
}MEffectInfo;

typedef struct __tag_mipfx
{
	MEffectInfo	*pEffectInfo;//effect list
	MDWord		dwEffectCount;//number of effect
	MDWord		dwProcessorType; //set the processor type
}MIPFX, *LPMIPFX;

//typedef struct __tag_mipfx
//{
//	MDWord		*pEffectID;//effect list
//	MVoid		**pEffectParam;
//	MDWord		dwEffectCount;//number of effect
//	MDWord		dwProcessorType; //set the processor type
//}MIPFX, *LPMIPFX;

typedef struct __tag_miprect
{
	MRECT	rtSrc;
	MPOINT	ptFore;
	MPOINT	ptMask;
}MIPRECT, *LPMIPRECT;//set the rect witch you want process

typedef struct _tag_autofixbrightness
{
	MInt32 lbrightness;
}AutoFixBrightness;

/////////////////////////////////////added for pmdenoise
typedef struct __tag_exif
{
	MDWord dwTime[2];
	MWord wISO;
} EXIF,*LPEXIF;

typedef struct _tag_denoise
{
	EXIF    pExif;
	MInt32   lRParam;//0~100
	MInt32	lGParam;//0~100
	MInt32	lBParam;//0~100
	MInt32	lNoiseParam;//0~100

}PMDENOISE,*LPPMDENOISE;

typedef struct _tag_noiseparam
{
	MInt32   lRParam;//0~100
	MInt32	lGParam;//0~100
	MInt32	lBParam;//0~100
	MInt32	lNoiseParam;//0~100
}NOISEPARAM,*LPNOISEPARAM;

typedef struct  _tag_colorparam
{
	MByte bRYBegin;			//0-255
	MByte bGUBegin;			//0-255
	MByte bBVBegin;			//0-255
	MByte bRYEnd;			//0-255
	MByte bGUEnd;			//0-255
	MByte bBVEnd;			//0-255
	MByte bAlpha;			//0-255
}SINGLECOLORPARAM, *LPSINGLECOLORPARAM;

typedef struct  _tag_kaleidoscopeparam
{
	MInt32 lAngle;
	MDWord dwR;//>0
	MDWord dwMode;//0 无明暗,1 明暗,2原图亮
	MPOINT postion;//Area is decided by R and position, and it must be in the image range
}KALEIDOSCOPEPARAM, *LPKALEIDOSCOPEPARAM;

typedef struct  _tag_PointColor
{
	MByte RorY;			// 0 - 255
	MByte GorU;			// 0 - 255
	MByte BorV;			// 0 - 255
}POINTCOLOR, *LPPOINTCOLOR;

typedef struct  _tag_partialcolorparam
{
	POINTCOLOR*	pColor;					// preserve multi colors value: RGB or YUV
	MInt32				lColorCount;			// preserve colors count
	MInt32				lHue;						// preserve color hue range [ 0, 100 ]
}PARTIALCOLORPARAM, *LPPARTIALCOLORPARAM;

typedef struct  _tag_NewPointColor
{
	MByte RorY;			// 0 - 255
	MByte GorU;			// 0 - 255
	MByte BorV;			// 0 - 255
	MInt32 lHue;						// preserve color hue range [ 0, 100 ]
}NEWPOINTCOLOR, *LPNEWPOINTCOLOR;

typedef struct  _tag_newpartialcolorparam
{
	NEWPOINTCOLOR*	pColor;					// preserve multi colors value: RGB or YUV
	MInt32				lColorCount;			// preserve colors count
}NEWPARTIALCOLORPARAM, *LPNEWPARTIALCOLORPARAM;

typedef struct  _tag_miniatureparam
{
	MInt32 lAngle; //旋转角度,对应矩形模式
	MDWord dwMode; //模式：圆1、矩形0
	MDWord dwBlurMode; //gauss 1, rect 0
	MInt32 lSaturation;//0~60
	MInt32 lContrast;//0~100
	MDWord dwClearRange; //清晰范围
	MDWord dwTranRange;//过渡范围
	MDWord dwNoiseGrade; //0-50模糊等级
	MPOINT postion; //处理位置
}MINIATUREPARAM, *LPMINIATUREPARAM;

typedef struct  _tag_polaroidparam
{
	MDWord dwColorMode; //0 综合，1 绿色，2黄色，3红色
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}POLAROIDPARAM, *LPPOLAROIDPARAM;

typedef struct  _tag_vividaram
{
	MDWord lVividGrade;//0~80

}VIVIDPARAM, *LPVIVIDPARAM;

typedef struct  _tag_fisheyeparam
{
	MDWord lTranGrade;//0~150

}FISHEYEPARAM, *LPFISHEYEPARAM;

typedef struct  _tag_nostalgicparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}NOSTALGICPARAM, *LPNOSTALGICPARAM;

typedef struct  _tag_vintageparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}VINTAGEPARAM, *LPVINTAGEPARAM;

typedef struct  _tag_deepquiteparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}DEEPQUITEPARAM, *LPDEEPQUITEPARAM;

typedef struct  _tag_coldblueparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}COLDBLUEPARAM, *LPCOLDBLUEPARAM;

typedef struct  _tag_gothicparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}GOTHICPARAM, *LPGOTHICPARAM;

typedef struct  _tag_purpleparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}PURPLEPARAM, *LPPURPLEPARAM;

typedef struct  _tag_vintageblackwhiteparam
{
	MDWord dwCornerMode; //0不加暗角，1加暗角
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadiusW;
	MInt32 lRadiusH;
}VINTAGEBLACKWHITEPARAM, *LPVINTAGEBLACKWHITEPARAM;


typedef struct  _tag_fisheyemagnifierparam
{
	MInt32 lOriginX;
	MInt32 lOriginY;
	MInt32 lRadius;

}FISHEYEMAGNIFIERPARAM, *LPFISHEYEMAGNIFIERPARAM;

typedef struct  _tag_fourcolorposterparam
{
	MDWord dwPosterMode;	// 四色组合的模式，对应0到4，缺省为0
	MDWord dwThreshold;		// 黑色和彩色之间的分界阈值，0到255之间，缺省为128
}FOURCOLORPOSTERPARAM, *LPFOURCOLORPOSTERPARAM;

typedef struct  _tag_colorgradientparam
{
	MInt32 lColorOffset;//0~360

}COLORGRADIENTPARAM, *LPCOLORGRADIENTPARAM;

typedef struct  _tag_curvebrightnessparam
{
	MInt32 lbrightness;//-30~30

}CURVEBEIGHTNESSPARAM, *LPCURVEBRIGHTNESSPARAM;

typedef struct __tag_newsketchparam
{
	MInt32 lThreshold;//0~100
}NewSketchPARAM;

typedef struct __tag_newembossparam
{
	MInt32	lDepth ;//1~16 the intensity of effect
}NewEmbossPARAM;

typedef struct __tag_reversalfilmparam
{
	MInt32	lHighLight ;//0-100
	MInt32	lContrast;//0-100
	MInt32	lDark;//0-100
	MInt32	lSaturation;//0-100
	MInt32	lComplexion;//0-100
}ReversalFilmPARAM;

typedef struct __tag_levelsparam
{
	MByte	bInputBegin;//initialization 0
	MByte	bInputEnd;//initialization 255,binputEnd-binputBegin>=2
	MByte	bOutputBegin;//initialization 0
	MByte	bOutputEnd;//initialization 255,bOutputEnd-bOutputBegin>=2
	MDouble	dGramma;//0.05-6

	MByte	bRInputBegin;//initialization 0
	MByte	bRInputEnd;//initialization 255,bRinputEnd-bRinputBegin>=2
	MByte	bROutputBegin;//initialization 0
	MByte	bROutputEnd;//initialization 255,bROutputEnd-bROutputBegin>=2
	MDouble	dRGramma;//0.05-6

	MByte	bGInputBegin;//initialization 0
	MByte	bGInputEnd;//initialization 255,bRinputEnd-bRinputBegin>=2
	MByte	bGOutputBegin;//initialization 0
	MByte	bGOutputEnd;//initialization 255,bROutputEnd-bROutputBegin>=2
	MDouble	dGGramma;//0.05-6

	MByte	bBInputBegin;//initialization 0
	MByte	bBInputEnd;//initialization 255,bRinputEnd-bRinputBegin>=2
	MByte	bBOutputBegin;//initialization 0
	MByte	bBOutputEnd;//initialization 255,bROutputEnd-bROutputBegin>=2
	MDouble	dBGramma;//0.05-6

	MByte *bOutputTable;//output 256 levels of rgb
	MByte *bROutputTable;//output 256 levels of r
	MByte *bGOutputTable;//output 256 levels of g
	MByte *bBOutputTable;//output 256 levels of b

}LevelsPARAM;

typedef struct __tag_curvesparam
{
	MDWord dwPointNum;//>=2, initialization = 2，对应的两个初始点为(0,0)，(255,255)
	MByte *pInput;//按照input数值升序排列
	MByte *pOutput;//排序和input对应

	MDWord dwRPointNum;//>=2, initialization = 2，对应的两个初始点为(0,0)，(255,255)
	MByte *pRInput;//按照input数值升序排列
	MByte *pROutput;//排序和input对应

	MDWord dwGPointNum;//>=2, initialization = 2，对应的两个初始点为(0,0)，(255,255)
	MByte *pGInput;//按照input数值升序排列
	MByte *pGOutput;//排序和input对应

	MDWord dwBPointNum;//>=2, initialization = 2，对应的两个初始点为(0,0)，(255,255)
	MByte *pBInput;//按照input数值升序排列
	MByte *pBOutput;//排序和input对应

	MByte *bOutputTable;//output 256 curve of rgb
	MByte *bROutputTable;//output 256 curve of r
	MByte *bGOutputTable;//output 256 curve of g
	MByte *bBOutputTable;//output 256 curve of b

}CurvePARAM;

typedef struct __tag_newbrightnesscontrast
{
	MInt32 lBrightness;//-150~150
	MInt32 lContrast;// -100~100
}NewBrightnessContrastPARAM;

typedef struct __tag_exposure
{
	MDouble dExposure;//-5.0~5.0
}ExposurePARAM;

typedef struct __tag_saturation
{
	MInt32 lSaturation;//-100~100
}SaturationPARAM;

typedef struct __tag_definition
{
	MInt32 	lRadius;//0~100; change with width of image
	MInt32 lThreshold;//0~100
	MInt32 lStrength;//-100~100
}DefinitionPARAM;

typedef struct __tag_highlightbrightdarkshadow
{
	MInt32 lHighlight;//-100~100
	MInt32 lShadow;//-100~100
	MInt32 lBrightColor;//-100~100
	MInt32 lDarkColor;//-100~100
	MInt32 lRight;//reserve
	MInt32 lLeft;//reserve
	MInt32 lMid;//reserve
	MByte *bOutputTable;//output 256 curve of rgb
}HighlightBrightDarkShadowPARAM;

typedef struct __tag_autowhitebalance
{
	MInt32 	lColorBalanceRed;//-100~100
	MInt32 lColorBalanceGreen;//-100~100
	MInt32 lColorBalanceBlue;//-100~100

}AutoWhiteBalancePARAM;

typedef struct __tag_hslcolor
{
	MDWord dwColor;
	MInt32 lHue;
	MInt32 lSaturation;
	MInt32 lBrightness;

}HSLColorPARAM;

typedef struct __tag_separatetone
{
	MInt32 lHighHue;//0~360
	MInt32 lHighSatu;//0~100
	MInt32 lBalance;//-100~100
	MInt32 lShadowHue;//0~360
	MInt32 lShadowSatu;//0~100
}SeparateTonePARAM;

typedef struct __tag_naturalsaturation
{
	MInt32 lSaturation;//-100~100
}NaturalSaturationPARAM;

typedef struct __tag_newnaturalsaturation
{
	MInt32 lSaturation;
	MInt32 lNewSaturation;//-100~100
}NewNaturalSaturationPARAM;

typedef struct __tag_gaussblurparam
{
	MDWord lBlurSize;//0~50
}GaussBlurPARAM;

typedef struct __tag_sharpenusmparam
{
	MInt32 lSharpNum;//0~500
	MInt32 lThreshold;//0~255
	MInt32 lRadius;//0~250
}SharpenUSMPARAM;

typedef struct __tag_selectivecolorparam
{
	MInt32 lParamCyan[9];//-100~100, 0~8 :red、yellow、green、cyan、blue、magenta、white、mid、black
	MInt32 lParamMagenta[9];//-100~100
	MInt32 lParamYellow[9];//-100~100
	MInt32 lParamBlack[9];//-100~100
	MDWord dwtype;//reserve

}SelectiveColorPARAM;

typedef struct{
	MFloat fDistort;//-1.0~1.0
}DistortParam;

typedef struct __tag_hsbparam
{
    MInt32  lHue[7];//-180~180, 0 means adjust none
    MInt32  lSaturation[7];//-100~100, 0 means adjust none
    MInt32   lLightness[7];//-100~100
    MInt32   lMin1[7];//0~359
    MInt32   lMin2[7];//0~359
    MInt32   lMax1[7];//0~359
    MInt32   lMax2[7];//0~359
}HSBPARAM;

typedef struct __tag_chromaticaberrationparam
{
	MInt32 lRadius;//1-11
	MInt32 lStrength;//0-100
	MBITMAP *pMask;//the same size with original image,MPAF_GRAY8 ,if mask==MNull:all image process
									//if mask!=MNull :process nonzero area
}ChromaticAberrationPARAM;

typedef struct __tag_removeedgeparam
{
	MInt32 lHPurpleBegin;//0~90,initialization: 30
	MInt32 lHPurpleEnd;//10~100,initialization:80
	MInt32 lHGreenBegin;//0~90,initialization:90
	MInt32 lHGreenEnd;//10~100,initialization:100
	MInt32 lPurpleStrength;//0~20
	MInt32 lGreenStrength;//0~20
	MBITMAP *pMask;//the same size with original image,MPAF_GRAY8 ,if mask==MNull:all image process
	//if mask!=MNull :process nonzero area
}RemoveEdgePARAM;

typedef struct __tag_darkcorner
{
    MInt32 lStrength;//-100~100，默认值设为0
    MInt32 lX;     //默认为图像中点
    MInt32 lY;     //默认为图像中点
    MInt32 lRegion;//0~100,默认值设为50
}DarkCornerPARAM;

typedef struct __tag_channelmixerparam
{
	MInt32 lParamRed[3];//-200~200
	MInt32 lParamGreen[3];//-200~200
	MInt32 lParamBlue[3];//-200~200
	MInt32 lParamConstant[3];//-200~200
	MDWord dwtype;//0 color;1 gray

}ChannelMixerPARAM;

typedef struct __tag_featherparam
{
	MDWord dwRadius;//0-100
	MRECT *pRect;//output rect of nonzero
}FeatherPARAM;

typedef struct __tag_colorbalance
{
	//高光调
	MInt32 lHigh_Cyan_Red;       //-100~100
	MInt32 lHigh_Magenta_Green;  //-100~100
	MInt32 lHigh_Yello_Blue;     //-100~100
	//中间调
	MInt32 lMid_Cyan_Red;       //-100~100
	MInt32 lMid_Magenta_Green;  //-100~100
	MInt32 lMid_Yello_Blue;     //-100~100
	//阴影调
	MInt32 lShadow_Cyan_Red;       //-100~100
	MInt32 lShadow_Magenta_Green;  //-100~100
	MInt32 lShadow_Yello_Blue;     //-100~100
	//明度
	MInt32 lLuminosity;     //0:not preserve luminosity,1:preserve luminosity
}ColorBalancePARAM;

typedef struct _tag_retouch
{
	MFloat fRadius; // 像素填充半径，范围1-10，推荐4
	MFloat fAlpha; // 像素填充优先级系数，范围0-1，推荐0.8
}RETOUCHPARAM, *LPRETOUCHPARAM;

typedef struct _tag_newoilpaint
{
	MInt32 lPenNumbers;
	MDWord dwType;//reserve
}NEWOILPAINTPARAM,*LNEWOILPAINTPARAM;

typedef struct _tag_backgroundcolor
{
	MByte bR, bG, bB, bA;
}BackgroundColor;

typedef struct __tag_rotatenparam
{
	MInt32 lRotateAngle;//-360---360
	BackgroundColor *pColor;
}RotatePARAM;
//typedef struct _tag_polaroidpurple
//{
//	MBITMAP *pMaskBitmap;
//}PolaroidPurplePARAM,*LPolaroidPurplePARAM;

typedef struct __tag_hudson
{
	MInt32 lStrength;//0~150
}HudsonPARAM;
typedef struct __tag_brannan
{
	MInt32 lStrength;//0~150
}BrannanPARAM;
typedef struct __tag_toaster
{
	MInt32 lStrength;//0~150
}ToasterPARAM;
typedef struct __tag_lofi
{
	MInt32 lStrength;//0~150
}LofiPARAM;
typedef struct __tag_valencia
{
	MInt32 lStrength;//0~150
}ValenciaPARAM;
typedef struct __tag_darkmood
{
	MInt32 lStrength;//0~150
}DarkmoodPARAM;
typedef struct __tag_hdr
{
	MInt32 lStrength;//0~150
}HdrPARAM;
typedef struct __tag_newreversal
{
	MInt32 lStrength;//0~100
}NewReversalPARAM;
typedef struct __tag_warmlomo
{
	MInt32 lStrength_1;//0~100
	MInt32 lStrength_2;//0~100
}WarmLomoPARAM;
typedef struct __tag_coldlomo
{
	MInt32 lStrength_1;//0~100
	MInt32 lStrength_2;//0~100
}ColdLomoPARAM;
typedef struct __tag_softpink
{
	MInt32 lStrength_1;//0~100
	MInt32 lStrength_2;//0~100
}SoftPinkPARAM;
typedef struct __tag_rixi
{
	MInt32 lStrength;//0~100
}RiXiPARAM;

typedef struct __tag_lightleak
{
	MDWord dwAngleType;//0-3(0,90,180,270)
	MDWord dwColorType;//0-4
	MDWord dwStrength;//0-150
}LightLeakPARAM;

typedef struct __tag_paintoil
{
	MInt32 lStrength;//0~150
}PAINTOILPARAM;

typedef struct __tag_crayonparam
{
	MInt32 lScale; // >0
}CrayonParam;
//structor for parameter passing Yunmall
#define YOURMALL_MAX_BITMAP_NUM	3
typedef struct __tag_yourmallphotofilterparam
{
	MInt32 lXOffset; // -100 - 100
	MInt32 lYOffset; // -100 - 100
	MInt32 bRadius;//0~100,actual radius will be bRadius * sqrt(ImageWidth * ImageWidth + ImageHeight * ImageHeight) / 200
	MByte bIntensity;//0~100, the bigger of this value, the stronger of the effect
	MInt32 iDisStrength;//0~100, the bigger of this value, the stronger of the effect
	MByte bFlipHorizontal;//0/1, 1:flip horizontally, 0:not
	MByte bFlipVertical;//0/1, 1:flip vertically, 0:not
	MByte bOverlayIndex;//Overlay index.
	MBITMAP *pDynBmp[YOURMALL_MAX_BITMAP_NUM];	//used for bitmap parameter, storing the address of bitmap struct
	MRECT mRct;	//used for rectangle infomation
	MByte bR,bG,bB;
}YourmallPhotoFilterParam;


//////////////////////////////////////////

#define MIP_HISTOGRAM_RGB      0x01
#define MIP_HISTOGRAM_GRAY     0x02

#define MIP_HISTOGRAMPLANES    0x04

//get the Histogram for auto effect
MIP_IMPORT MRESULT MIP_GetHistogram(MBITMAP *pBitmapData,MDWord dwEffectID, MInt32* pHistogram);
//get the matrix of autolevel
MIP_IMPORT MRESULT MIP_GetAutoProcessFormula(MInt32* pHistogram,MDWord dwEffectID, MInt32* pFormula);
MIP_IMPORT MRESULT MIP_GetAutoColorFormula(MBITMAP *pBitmapData, MInt32* pHistogram,MInt32* pFormula);

MIP_IMPORT MRESULT MIP_GetAutoWBPara(MBITMAP *pBitmapData,MInt32* temperature, MInt32* tint);

MIP_IMPORT MRESULT MIP_GetSelectionWBPara(MBITMAP *pBitmapData,MRECT rect,MInt32* temperature, MInt32* tint);

MIP_IMPORT MRESULT MIP_GetDLightPara(MBITMAP *pBitmapData,MInt32 lLunminance, MInt32 lShadow, MVoid* pMap);

MIP_IMPORT MRESULT	MIP_GetAutoFixPara(MBITMAP *pBitmapData,AUTOFIXPARAM *AutoFixPara,MInt32* pFormula);

MIP_IMPORT MRESULT	MIP_GetAutoWhiteBalancePara(MBITMAP *pBitmapData,AutoWhiteBalancePARAM *autoWhiteBalancePara);

MIP_IMPORT MRESULT	MIP_GetWhiteBalancePara(MBITMAP *pBitmapData,WhiteBalancePARAM *WhiteBalancePara);

MIP_IMPORT MRESULT	MIP_GetAutoBrightnessPara(MBITMAP *pBitmapData,AutoFixBrightness* pFormula);

MIP_IMPORT MRESULT MGetMoveEdgeParam(MBITMAP *pBitmapData,MPOINT point,RemoveEdgePARAM * pParam);

//get the Histogram for all effect
MIP_IMPORT MRESULT MIP_GetCommonHistogram(MBITMAP *pBitmapData,MDWord Histogram_ID, MInt32* pHistogram[MIP_HISTOGRAMPLANES]);

// help function
//rtDst Out max rect
//trSrc Out Max rect
MIP_IMPORT MRESULT MIPCalcRect(MIP_IN MHandle HIP, MIP_INOUT MRECT *rtDst, MIP_INOUT MRECT *rtSrc);
MIP_IMPORT MRESULT MIPCalcRectReset(MIP_IN MHandle HIP);
MIP_IMPORT MBool  	MIPMergeable(MDWord dwSpaceID, MIPFX *FX);
MIP_IMPORT MBool  	MIPIsSupEffMerge(MDWord dwPreEffectID,MDWord dwNextEffectID);
MIP_IMPORT MBool  	MIPIsSupport(MDWord dwPixFormat, MDWord dwEffectID);
MIP_IMPORT MBool	MIPIsSameInOutArea(MDWord dwEffectID);

// Rotate Image
MRESULT MIP_GetRotateDstSize(MBITMAP* pSrcBmp, MInt32 lAngle, MInt32* pDstWidth, MInt32* pDstHeight);
MRESULT MIP_RotateImage(MBITMAP* pSrcBmp, MInt32 lAngle, BackgroundColor bgColor, MBITMAP* pDstBmp);

//Effect Process
MIP_IMPORT MRESULT MIPCreate(MIP_IN MIPPIXEL *pDstPixel, MIP_IN MIPPIXEL *pSrcPixel, MIPFX *FX, MIP_OUT MHandle *pHIP);
MIP_IMPORT MRESULT MIPProcess(MIP_IN MHandle HIP, MIP_IN MIPData *pDst, MIP_IN MIPData *pSrc, MIP_INOUT MRECT *pRECT);
MIP_IMPORT MRESULT MIPDestroy(MIP_IN MHandle HIP);
MIP_IMPORT MRESULT MIPGetMaxOutLine(MIP_IN MHandle HIP,MIP_IN MDWord dwMaxInputLines, MIP_OUT MDWord *pMaxLines);
MIP_IMPORT MRESULT MIPFilterPaint(MBITMAP* pSrcBitmap, MBITMAP* pDstBitmap,MInt32 filterID, MInt32 intensity, MInt32 overlaytype,
								  MInt32 x, MInt32 y, MInt32 lIsStartPoint, MInt32 brushsize, MInt32 needwholebuffer);

//for merge and fillcolor
MIP_IMPORT MRESULT MIPCreateMerge(MIP_IN MIPPIXEL *pDstPixel, MIP_IN MIPPIXEL *pSrcPixel, MIP_IN MIPPIXEL *pForePixel, MIP_IN MIPPIXEL *pMaskPixel,
				  MIP_IN MIPFX *FX, MIP_OUT MHandle *pHIP);
MIP_IMPORT MRESULT MIPMerge(MIP_IN MHandle HIP, MIP_IN MIPData *pDst, MIP_IN MIPData *pSrc, MIP_IN MIPData *pFore, MIP_IN MIPData *pMask,
				   MIP_IN MIPRECT *pRtBuffer);
MIP_IMPORT MRESULT MIPDestroyMerge(MIP_IN MHandle HIP);
MIP_IMPORT MRESULT MIPFreeMemory(MIP_IN MHandle HIP);

//get image process version information
MIP_IMPORT MRESULT ImageProcessGetVersionInfo (MDWord* pdwRelease, MDWord* pdwMajor, MDWord* pdwMinor, MTChar* pszPatch, MDWord dwLen);

//pmdenoise analyse
MIP_IMPORT MRESULT DenoiseUpdate(MIP_IN MHandle HIP, MIP_IN NOISEPARAM NoiseParam);
MIP_IMPORT MRESULT GetDenoiseRt(MIP_IN MHandle HIP, MIP_IN MBITMAP *pResampleBitmap,MIP_OUT MRECT *pSrcBitRect);
MIP_IMPORT MRESULT DenoiseAnalyse(MIP_IN MHandle HIP, MIP_IN MBITMAP *pRtBitmap, MIP_OUT NOISEPARAM *NoiseParam);

//ammat
MRESULT AMMAT_Create(MHandle* phAMMAT, MBITMAP* pSrcBmp);
MRESULT AMMAT_AddLine(MHandle hAMMAT, MDWord dwLineType, MPOINT* pptLine, MInt32 lLineCnt);
MRESULT AMMAT_ResetLine(MHandle hAMMAT);
MRESULT AMMAT_Process(MHandle hAMMAT, MBITMAP** ppMskBmp, MPOINT** ppptEdge, MInt32* pEdgeCnt);
MVoid AMMAT_Destroy(MHandle hAMMAT);

//ContourTracing
MRESULT MIP_ContourTracing(MBITMAP* pMskBmp, MPOINT** ppptContour, MInt32* pContourLength);

//Retouch
MRESULT ReTouch(MBITMAP* pSrcMap, MBITMAP* pResampleMap, MBITMAP *pMask, RETOUCHPARAM* pReTouch, MBITMAP* pDstMap);

//NewOilpaint
MRESULT MIPGetProPerty(MIP_IN MHandle HIP,MDWord dwEffectID,MDWord dwPropertyType,MVoid *pProperty,MInt32 lPropertySize);
MRESULT MIPSetProPerty(MIP_IN MHandle HIP,MDWord dwEffectID,MDWord dwPropertyType,MVoid *pProperty,MInt32 lPropertySize);

//MotionBlur
MRESULT MIP_MotionBlur(MBITMAP* pSrcBmp, MBITMAP* pMskBmp, MInt32 lAngle, MInt32 lDistance, MBITMAP* pDstBmp);

// Waterfall
MRESULT MIP_Waterfall(MBITMAP* pSrcBmp, MPOINT* pptStart, MPOINT* pptEnd, MInt32 lBrushWidth, MInt32 lIntensity, MBITMAP* pDstBmp);

//rotate free
MRESULT MIPRotateCalcDstSize(MBITMAP *pSrcBitmap,MInt32 lAngle,MInt32 *lWidth,MInt32 *lHeight);
MRESULT MIPRotateCalcInternalRect(MBITMAP *pSrcBitmap,MBITMAP *pDstBitmap,MInt32 lAngle,MRECT *pInternalRect);
MRESULT MIPRotateCreate(MHandle *handle,MBITMAP *pSrcBitmap,MBITMAP *pDstBitmap,RotatePARAM *pRotateParam);
MRESULT MIPRotateCalcRect(MHandle handle,MRECT *pDstRect,MRECT *pSrcRect);
MRESULT MIPRotateProcess(MHandle handle,MBITMAP *pSrcBitmap,MBITMAP *pDstBitmap,MRECT *pDstRect);
MRESULT MIPRotateDestroy(MHandle handle);

//WaterFall
MRESULT MIP_WaterFall_Create(MHandle* phWF, MBITMAP* pBmpIn, MBITMAP** ppBmpOut);
//pUpdatedArea can be null
MRESULT MIP_WaterFall_Process(MHandle hWF, MPOINT* pPoint, MBool bLast, MBITMAP** ppBmpOut, MRECT* pUpdatedArea);
MRESULT MIP_WaterFall_ProcessAsync(MHandle hWF, MPOINT* pPoint, MBool bLast, MBool* bNeedDostep);
MRESULT MIP_WaterFall_Dostep(MHandle hWF, MDWord dwTimeLimit, MBITMAP** ppBmpOut, MRECT* pUpdatedArea);
MRESULT MIP_WaterFall_Destroy(MHandle hWF);
MRESULT MIP_WaterFall_SetProp(MHandle hWF, MDWord dwPropID, MPVoid pData, MInt32 lDataSize);
MRESULT MIP_WaterFall_GetProp(MHandle hWF, MDWord dwPropID, MPVoid pData, MInt32 lDataSize);
//straighten
MRESULT MIP_Straighten(MBITMAP* pSrcBitmap, MBITMAP* pDstBitmap,MDouble dStraightenRadian,MDWord dwQualityOrSpeed);

#ifdef __cplusplus
}
#endif

#endif

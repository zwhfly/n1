/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/
 
#ifndef ANDROID_ARCSOFT_NIGHT_VIDEO_H
#define ANDROID_ARCSOFT_NIGHT_VIDEO_H

#include "arcsoft_nighthawk.h"
#include <pthread.h>

#include "asvloffscreen.h"
#include "ammem.h"
#include "merror.h"
#include "GNCameraFeatureDefs.h"


namespace android { 
class ArcSoftNightVideo {
public:
    ArcSoftNightVideo();
	
	int32 init();
    void  deinit();
	int32 processNightVideo(LPASVLOFFSCREEN param);
	int32 enableNightVideo();
	int32 disableNightVideo();


private:
	MVoid* mMem;
	MHandle mMemMgr;
	MHandle mEnhancer;
	pthread_mutex_t mMutex;
	ANH_PARAM mParam;
};
};
#endif /* ANDROID_ARCSOFT_NIGHT_VIDEO_H */


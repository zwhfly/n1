#ifndef TA3D_GEOMETRY_H
#define TA3D_GEOMETRY_H

namespace TouchlessA3D {
  /**
   * Represents a two-dimensional point.
   *
   * @tparam T The type of the coordinates.
   */
  template<typename T>
  struct Point {
    typedef T value_type;

    Point() {}

    Point(const T& xValue, const T& yValue) :
      x(xValue),
      y(yValue)
    {
    }

    Point(const Point& other) :
      x(other.x),
      y(other.y)
    {
    }

    ~Point() {}

    Point& operator=(const Point& other) {
      x = other.x;
      y = other.y;
      return *this;
    }

    T x;
    T y;
  };

  /**
   * Represents a rectangle in two dimensions using two opposite corners.
   * The rectangle is considered a closed, two-dimensional interval, i.e. it
   * contains both its corners.
   *
   * @tparam T The type of the coordinates.
   */
  template<typename T>
  struct Rectangle {
    typedef T value_type;

    Rectangle(const Point<T>& tl, const Point<T>& br) :
      topLeft(tl),
      bottomRight(br)
    {
    }

    Rectangle(const T& left, const T& top, const T& right, const T& bottom) :
      topLeft(left, top),
      bottomRight(right, bottom)
    {
    }

    Rectangle(const Rectangle& other) :
      topLeft(other.topLeft),
      bottomRight(other.bottomRight)
    {
    }

    ~Rectangle() {}

    Rectangle& operator=(const Rectangle& other) {
      topLeft = other.topLeft;
      bottomRight = other.bottomRight;
      return *this;
    }

    Point<T> topLeft;
    Point<T> bottomRight;
  };
}

#endif  // TA3D_GEOMETRY_H

#ifndef TA3D_MOTION_LISTENER_H
#define TA3D_MOTION_LISTENER_H

#include "common.h"
#include "motion.h"

namespace TouchlessA3D {
  /**
   * Abstract base class for motion listeners implemented by the user of this library.
   *
   * Copyright © 2014 Crunchfish AB. All rights reserved. All information herein
   * is or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT MotionListener {
  public:
    virtual ~MotionListener();

    /**
     * Called to notify the listener when a motion detection result is
     * available.
     *
     * @param motion The motion detected. Note that the motion might be empty,
     *               indicating that nothing was detected, i.e. just getting the
     *               callback is not sufficient to tell whether there was motion
     *               in the image handled.
     */
    virtual void onMotion(const Motion& motion) = 0;
  };
} // namespace TouchlessA3D

#endif  // TA3D_MOTION_LISTENER_H

/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#ifndef GN_CAMERA_DLIST_H
#define GN_CAMERA_DLIST_H

#include <stddef.h>

#define member_of(ptr, type, member) ({ \
  const typeof(((type *)0)->member) *__mptr = (ptr); \
  (type *)((char *)__mptr - offsetof(type,member));})

struct dlist {
  struct dlist *next, *prev;
};

static inline void dlist_init(struct dlist *ptr)
{
  ptr->next = ptr;
  ptr->prev = ptr;
}

static inline void dlist_add_tail_node(struct dlist *item,
  struct dlist *head)
{
  struct dlist *prev = head->prev;

  head->prev = item;
  item->next = head;
  item->prev = prev;
  prev->next = item;
}

static inline void dlist_insert_before_node(struct dlist *item,
  struct dlist *node)
{
  item->next = node;
  item->prev = node->prev;
  item->prev->next = item;
  node->prev = item;
}

static inline void dlist_del_node(struct dlist *ptr)
{
  struct dlist *prev = ptr->prev;
  struct dlist *next = ptr->next;

  next->prev = ptr->prev;
  prev->next = ptr->next;
  ptr->next = ptr;
  ptr->prev = ptr;
}

#endif /* GN_CAMERA_DLIST_H */

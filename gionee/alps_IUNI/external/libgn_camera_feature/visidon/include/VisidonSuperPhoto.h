/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/
 
#ifndef ANDROID_VISIDON_SUPER_PHOTO_CAMERA_FEATURE_H
#define ANDROID_VISIDON_SUPER_PHOTO_CAMERA_FEATURE_H

#include <GNCameraFeatureDefs.h>
#include <stdio.h>

extern "C"
{
#include "VDSuperPhotoAPI.h"
}

//five buffer for src image, one for dst image
#define MAX_INPUT_IMAGE_NUM 6

namespace android { 

class VisidonSuperPhoto {
public:
    VisidonSuperPhoto();
	~VisidonSuperPhoto();
	
	int32 init();
    void  deinit();
    int32 enableSuperPhoto(float scale, GNSuperPhotoType_t type);
	void disableSuperPhoto();
	int32 processSuperPhoto(ImageParamInfo* imageData);
	int32 getBurstCnt();
	int32 setExParameters(int32 type, void* param);
	
private:
	int32 getMemSize(int width, int height, VDDatatype format);
	
private:
	//debug
	int mColorFilterStrength;
	int mDenoiseStrength;
	int mSharpness;
	int mContrastStrength;
	int mIsoValue;
	
	int mImageNum;
	int mSuperPhotoType;
	float mScale;
	unsigned char *mSrcImg[MAX_INPUT_IMAGE_NUM - 1];
	
	bool mInitialized;

};
};
#endif /* ANDROID_TEST_CAMERA_FEATURE_H */

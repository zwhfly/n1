/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/

#define LOG_TAG "VisidonRTSuperPhoto"

#include "VisidonRTSuperPhoto.h"

#include <cutils/properties.h>
#include <stdlib.h>
#include <cutils/log.h>
#include <time.h>

#include "GNCameraFeatureDbgTime.h"

//#define DEBUG_TIME
    
namespace android {

VisidonRTSuperPhoto::VisidonRTSuperPhoto() 
	: mRTSuperPhotoEngine(NULL)
	, mPreWidth(0)
	, mPreHeight(0)
	, mInitialized(false)
	, mEnableVideoStab(0)
	, mDeghosting(1)
	, mZoomRatio(1.0)
	, mTotalZoomRatio(1.0)
	, mFormat(GN_IMG_FORMAT_YV12)
	, mRTSuperPhotoState(GN_RTSUPERPHOTO_STATE_UNINIT)
{
	char sharpness[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.realtime.sharpening", sharpness, "10");
	mSharpening = atoi(sharpness);
	
	char contrast[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.realtime.contrast", contrast, "20");
	mContrast = atoi(contrast);
	
	char denoiseStrength[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.realtime.denoise", denoiseStrength, "60");
	mDenoiseStrength = atoi(denoiseStrength);

	char colorFilterStrength[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.realtime.filter", colorFilterStrength, "10");
	mColorFilterStrength = atoi(colorFilterStrength);

	char lightMode[PROPERTY_VALUE_MAX] = {0};
	property_get("persist.lightMode", lightMode, "1");
	mLightMode = atoi(lightMode);

	mCmdThread.launch(defferedWorkRoutine, (void*)this);

}

VisidonRTSuperPhoto::
~VisidonRTSuperPhoto() 
{
	if (mRTSuperPhotoEngine != NULL) {
		mCmdThread.sendCmd(GN_CAMERA_CMD_TYPE_UNINIT, true, true);
	}

	mCmdThread.exit();
}

int32
VisidonRTSuperPhoto::
init()
{
	int32 res = 0;

	mInitialized = false;

	return res;
}

void 
VisidonRTSuperPhoto::
deinit()
{
	disableRTSuperPhoto();
}

int32 
VisidonRTSuperPhoto::
enableRTSuperPhoto(int pWidth, int pHeight, GNImgFormat format, float zoom, float totalZoom)
{
	int32 res = 0;


	mZoomRatio = zoom;
	mTotalZoomRatio = totalZoom;

	if (pWidth != mPreWidth || pHeight != mPreHeight || mRTSuperPhotoState == GN_RTSUPERPHOTO_STATE_UNINIT) {
		if (mRTSuperPhotoEngine != NULL) {
			VDRTSuperPhoto_Release(&mRTSuperPhotoEngine);
			mRTSuperPhotoEngine = NULL;
		}

		mPreWidth = pWidth;
		mPreHeight = pHeight;
		mFormat = format;

		updateRTSuperPhotoState(GN_RTSUPERPHOTO_STATE_INITING);

		mCmdThread.sendCmd(GN_CAMERA_CMD_TYPE_INIT, false, false);
		
	} else {
		PRINTD("%s zoom = %f, totalZoom = %f", __func__, zoom, totalZoom);
		
		if (mRTSuperPhotoEngine != NULL) {
			res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_ZOOM, (void*)(&zoom), mRTSuperPhotoEngine);
			if (res != VDRTSUPERPHOTO_OK) {
				PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_ZOOM error");
			}
			
			res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_TOTALZOOM, (void*)(&totalZoom), mRTSuperPhotoEngine);
			if (res != VDRTSUPERPHOTO_OK) {
				PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_ZOOM error");
			}
		}
	}
	
	return res;
}

void
VisidonRTSuperPhoto::
disableRTSuperPhoto()
{
	//if (mInitialized) {
	if (mRTSuperPhotoState != GN_RTSUPERPHOTO_STATE_UNINIT) {
		mCmdThread.sendCmd(GN_CAMERA_CMD_TYPE_UNINIT, true, true);
	}
}

int32 
VisidonRTSuperPhoto::
processRTSuperPhoto(ImageParamInfo* imageData)
{
	int32 res = 0;

#ifdef DEBUG_TIME
	GNCameraFeatureDbgTime dbgTime;
#endif

	if (imageData == NULL || imageData->buffer== NULL || !mInitialized) {
		PRINTE("the inputBuffer is null or not initialized [%d].", mInitialized);
		return -1;
	}

	if (mRTSuperPhotoEngine != NULL) {
		VDRTSuperPhoto_ProcessFrame((unsigned char*)(imageData->buffer), mRTSuperPhotoEngine);
	}

#ifdef DEBUG_TIME
	dbgTime.print(__func__);
#endif

	return res;
}

int32
VisidonRTSuperPhoto::
initEngine(int width, int height, GNImgFormat format)
{
	int32 res = 0;
	VDRTSuperPhotoImageFormat imageFormat;
	
	switch (format) {
		case GN_IMG_FORMAT_YV12:
			imageFormat = VDRTSUPERPHOTO_IMAGE_FORMAT_YV12;
			break;
		case GN_IMG_FORMAT_NV21:
			imageFormat = VDRTSUPERPHOTO_IMAGE_FORMAT_NV21;
			break;
		default:
			imageFormat = VDRTSUPERPHOTO_IMAGE_FORMAT_YV12;
			break;
	}
	
	res = VDRTSuperPhoto_Initialize(width, height, &mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_Initialize error");
		updateRTSuperPhotoState(GN_RTSUPERPHOTO_STATE_UNINIT);
		return -1;
	}

	PRINTD("%s mEnableVideoStab %d, mDenoiseStrength %d, mSharpening %d, mContrast %d, mDeghosting = %d, mLightMode = %d", 
		__func__, mEnableVideoStab, mDenoiseStrength, mSharpening, mContrast, mDeghosting, mLightMode);

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_STABILIZATION, (void*)(&mEnableVideoStab), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_STABILIZATION error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_NOISEREDUCTION_STRENGTH, (void*)(&mDenoiseStrength), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_NOISEREDUCTION_STRENGTH error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_SHARPENING, (void*)(&mSharpening), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_SHARPENING error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_CONTRAST, (void*)(&mContrast), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_CONTRAST error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_DEGHOSTING, (void*)(&mDeghosting), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_DEGHOSTING error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_LIGHTMODE, (void*) (&mLightMode),	mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_LIGHTMODE error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_IMAGEFORMAT, (void*) (&imageFormat),  mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_LIGHTMODE error");
	}

	PRINTD("%s mZoomRatio = %f, mTotalZoomRatio = %f", __func__, mZoomRatio, mTotalZoomRatio);

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_ZOOM, (void*)(&mZoomRatio), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_ZOOM error");
	}

	res = VDRTSuperPhoto_SetParameter(VDRTSUPERPHOTO_TOTALZOOM, (void*)(&mTotalZoomRatio), mRTSuperPhotoEngine);
	if (res != VDRTSUPERPHOTO_OK) {
		PRINTE("VDRTSuperPhoto_SetParameter VDRTSUPERPHOTO_ZOOM error");
	}

	mInitialized = true;
	
	updateRTSuperPhotoState(GN_RTSUPERPHOTO_STATE_INITED);

	return res;
}

void
VisidonRTSuperPhoto::
deinitEngine()
{
	if (mRTSuperPhotoEngine != NULL) {
		VDRTSuperPhoto_Release(&mRTSuperPhotoEngine);

		mRTSuperPhotoEngine = NULL;
		mInitialized = false;
	}

	updateRTSuperPhotoState(GN_RTSUPERPHOTO_STATE_UNINIT);

}

void*
VisidonRTSuperPhoto::
defferedWorkRoutine(void* data)
{
	int ret = 0;
	int running = 1;
	VisidonRTSuperPhoto* thiz = (VisidonRTSuperPhoto*)data;

	GNCameraCmdThread* cmdThread = &thiz->mCmdThread;

	do {
		do {
			ret = cam_sem_wait(&cmdThread->cmd_sem);
			if (ret != 0 && errno != EINVAL) {
				PRINTD("%s cam_sem_wait error(%s)", __func__, strerror(errno));
				return NULL;
			}
		} while(ret != 0);

		camera_cmd_type_t cmd = cmdThread->getCmd();
		PRINTD("%s cmd = %d", __func__, cmd);
		switch (cmd) {
			case GN_CAMERA_CMD_TYPE_INIT:
				thiz->initEngine(thiz->mPreWidth,
								thiz->mPreHeight, 
								thiz->mFormat);
				break;
			case GN_CAMERA_CMD_TYPE_CONFIG:
				break;
			case GN_CAMERA_CMD_TYPE_UNINIT:
				thiz->deinitEngine();
				cmdThread->cmd_queue.flush();
				cam_sem_post(&cmdThread->sync_sem);
				break;
			case GN_CAMERA_CMD_TYPE_EXIT:
				running = 0;
				break;
			default:
				break;
		}
	} while(running);

	return NULL;
}

};

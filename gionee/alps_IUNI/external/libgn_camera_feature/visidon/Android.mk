LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= \
	VisidonCameraFeature.cpp \
	VisidonRTSuperPhoto.cpp \
	VisidonSuperPhoto.cpp \
	VisidonSingleHdr.cpp \
	../util/GNCameraQueue.cpp \
	../util/GNCameraCmdThread.cpp \

LOCAL_MODULE:= libvisidon

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/bin/armeabi-v7a/libVDSuperPhotoRT.so:system/lib/libVDSuperPhotoRT.so \
	$(LOCAL_PATH)/bin/armeabi-v7a/libVDSuperPhotoRT-GLESv3.so:system/lib/libVDSuperPhotoRT-GLESv3.so \
	$(LOCAL_PATH)/bin/armeabi-v7a/libVDSuperPhotoAPI.so:system/lib/libVDSuperPhotoAPI.so \
	$(LOCAL_PATH)/bin/armeabi-v7a/libVDSingleHDRAPI.so:system/lib/libVDSingleHDRAPI.so \
	$(LOCAL_PATH)/bin/arm64-v8a/libVDSuperPhotoRT.so:system/lib64/libVDSuperPhotoRT.so \
	$(LOCAL_PATH)/bin/arm64-v8a/libVDSuperPhotoRT-GLESv3.so:system/lib64/libVDSuperPhotoRT-GLESv3.so \
	$(LOCAL_PATH)/bin/arm64-v8a/libVDSuperPhotoAPI.so:system/lib64/libVDSuperPhotoAPI.so \
	$(LOCAL_PATH)/bin/arm64-v8a/libVDSingleHDRAPI.so:system/lib64/libVDSingleHDRAPI.so 

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/include \
	$(TOP)/external/libgn_camera_feature/include \
	$(TOP)/external/libgn_camera_feature/util \
	$(TOP)/external/libgn_camera_feature/util/common \
	$(TOP)/bionic \
	$(TOP)/bionic/libstdc++/include \
	$(TOP)/external/stlport/stlport/ \

ifeq "$(strip $(GN_VISIDON_FEATURE_SUPPORT))" "yes"
include $(BUILD_STATIC_LIBRARY)
endif

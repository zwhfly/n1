/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/

#define LOG_TAG "VisidonSingleHdr"

#include <android/log.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <cutils/properties.h>

#include "GNCameraFeatureDbgTime.h"
#include "VisidonSingleHdr.h"

    
namespace android {

static int count_dump = 0;

VisidonSingleHdr::VisidonSingleHdr() 
	: mEngine(NULL)
	, mWidth(0)
	, mHeight(0)
	, mInitialized(0)
{
	char intensity[PROPERTY_VALUE_MAX] = {0};
	property_get("preview.intensityStrength", intensity, "50");
	mIntensityLevel = atoi(intensity);

	char contrast[PROPERTY_VALUE_MAX] = {0};
	property_get("preview.contrastStrength", contrast, "45");
	mContrastLevel = atoi(contrast);

	char denoise[PROPERTY_VALUE_MAX] = {0};
	property_get("preview.contrastStrength", denoise, "50");
	mDenoiseStrength = atoi(denoise);
}

VisidonSingleHdr::
~VisidonSingleHdr() 
{
	
}

VisidonSingleHdr*
VisidonSingleHdr::
createInstance() 
{
	return new VisidonSingleHdr();
}

void
VisidonSingleHdr::
destroyInstance() 
{
	delete this;
}

int32
VisidonSingleHdr::
init()
{
	int32 res = 0;

	mInitialized = false;

	return res;
}

void 
VisidonSingleHdr::
deinit()
{
	disableSingleHDR();
}

int32 
VisidonSingleHdr::
enableSingleHDR(int width, int height, GNImgFormat format)
{
	int32 res = 0;

	VDSingleShotHDRImageFormat imageFormat;

	if (width != mWidth || height != mHeight || !mInitialized) {
		if (mInitialized) {
			if (mEngine != NULL) {
				VDReleaseSingleShotHDR(&mEngine);
				mEngine = NULL;
			}
			
			mInitialized = false;
		}

		switch (format) {
			case GN_IMG_FORMAT_YV12:
				imageFormat = IMAGE_FORMAT_YV12;
				break;
			case GN_IMG_FORMAT_NV21:
				imageFormat = IMAGE_FORMAT_NV21;
				break;
			default:
				imageFormat = IMAGE_FORMAT_YUYV;
				break;
		}

		res = VDInitializeSingleShotHDR(width, height, MEMCPY_PIXELBUFFEROBJECT, &mEngine);
		if (res != VDSINGLESHOTHDR_OK) {
			PRINTD("%s Init hdr engine fail", __func__);
		}

		res = VDSetSingleShotHDRParameter(VDDRO_PREVIEW_WIDTH, (void*)(&width), mEngine);
		if (res != VDSINGLESHOTHDR_OK) {
			PRINTD("%s Faile to set VDDRO_PREVIEW_WIDTH", __func__);
		}
		
		res = VDSetSingleShotHDRParameter(VDDRO_PREVIEW_HEIGHT, (void*)(&height), mEngine);
		if (res != VDSINGLESHOTHDR_OK) {
			PRINTD("%s Faile to set VDDRO_PREVIEW_HEIGHT", __func__);
		}
		
		res = VDSetSingleShotHDRParameter(VDDRO_IMAGE_FORMAT, (void*)(&imageFormat), mEngine);
		if (res != VDSINGLESHOTHDR_OK) {
			PRINTD("%s Faile to set VDDRO_IMAGE_FORMAT", __func__);
		}

		res = VDSetSingleShotHDRParameter(VDDRO_NOISEREMOVAL_STRENGTH, (void *)(&mDenoiseStrength),mEngine);
		if (res != VDSINGLESHOTHDR_OK) {
			PRINTD("%s Faile to set VDDRO_NOISEREMOVAL_STRENGTH", __func__);
		}
		
		mWidth = width;
		mHeight = height;

		mInitialized = true;
	}

	return 0;
}

void
VisidonSingleHdr::
disableSingleHDR()
{
	if (mInitialized) {
		if (mEngine != NULL) {
			VDReleaseSingleShotHDR(&mEngine);
			mEngine = NULL;
		}
	}

	mInitialized = false;
}

int32
VisidonSingleHdr::
processSingleHDR(ImageParamInfo* imageData, bool isPreview)
{
	int32 res = 0;

	if (imageData == NULL || imageData->buffer == NULL) {
		PRINTD("%s Invalid input params", __func__);
		return -1;
	}

	if (isPreview) {
		res = processRTStream(imageData);
	} else {
		res = processImage(imageData);
	}

	return res;
}

int32 
VisidonSingleHdr::
processRTStream(ImageParamInfo* stream)
{
	int32 res = 0;
	int32 err = 0;

#ifdef DEBUG_TIME
	GNCameraFeatureDbgTime dbgTime;
#endif
	
	if (!mInitialized) {
		PRINTD("%s hdr engine is not initialized", __func__);
		return -1;
	} 
	
	err = VDProcessSingleShotHDRFrame((unsigned char*)stream->buffer, mIntensityLevel, mContrastLevel, mEngine);
	if (err != VDSINGLESHOTHDR_OK) {
		PRINTD("%s Failed to process hdr frame", __func__);
	}

#ifdef DEBUG_TIME
	dbgTime.print(__func__);
#endif

	return res;
}

int32
VisidonSingleHdr::processImage(ImageParamInfo* imageData)
{
	int32 res = 0;
	void* engine;
	GNCameraFeatureDbgTime dbgTime;

	res = VDProcessSingleShotHDRStill((unsigned char*)imageData->buffer, imageData->width, imageData->height,
				(VDSingleShotHDRImageFormat)imageData->pixelArrayFormat, mIntensityLevel, mContrastLevel);
	if (res != VDSINGLESHOTHDR_OK) {
		PRINTD("%s Faile to process hdr frame", __func__);
	}

	dbgTime.print(__func__);
	
	return res;
}

};

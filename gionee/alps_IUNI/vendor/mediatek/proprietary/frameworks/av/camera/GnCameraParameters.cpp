/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera parmeters.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/25
 *
 *************************************************************************************/

#define LOG_TAG "GnCameraParams"
#include <utils/Log.h>

#include <string.h>
#include <stdlib.h>
#include <camera/GnCameraParameters.h>

namespace android {
const char GnCameraParameters::KEY_FACE_BEAUTY_MODE[] = "face-beauty-mode";
const char GnCameraParameters::KEY_SUPPOTED_FACE_BEAUTY_MODE[] = "face-beauty-mode-supported";
const char GnCameraParameters::FACE_BEAUTY_ON[] = "on";
const char GnCameraParameters::FACE_BEAUTY_OFF[] = "off"; 
const char GnCameraParameters::KEY_FACE_BEAUTY_LEVEL[]="face-beauty-level";
const char GnCameraParameters::KEY_SLENDER_FACE_LEVEL[]	= "slender-face-level";
const char GnCameraParameters::KEY_SKIN_SOFTEN_LEVEL[] = "skin-soften-level";
const char GnCameraParameters::KEY_SKIN_BRIGHT_LEVEL[] = "skin-bright-level";
const char GnCameraParameters::KEY_EYE_ENLARGMENT_LEVEL[] = "eye-enlargment-level";

const char GnCameraParameters::KEY_FACE_BEAUTY_SUB_MODE[] = "face-beauty-sub-mode";
const char GnCameraParameters::KEY_FACE_BEAUTY_AGEGENDER_LEVEL[] = "face-beauty-agegender-level";
const char GnCameraParameters::FACE_BEAUTY_AGEGENDER_MODE[]	= "face-beauty-agegender-mode";
const char GnCameraParameters::FACE_BEAUTY_DEFAULT_MODE[] = "face-beauty-default-mode";

//ageGender-detection
const char GnCameraParameters::KEY_AGEGENDER_DETECTION_MODE[] = "agegender-detection-mode";
const char GnCameraParameters::KEY_SUPPOTED_AGEGENDER_DETECTION_MODE[] = "agegender-detection-mode-supported";
const char GnCameraParameters::AGEGENDER_DETECTION_ON[] = "on";
const char GnCameraParameters::AGEGENDER_DETECTION_OFF[] = "off";

const char GnCameraParameters::KEY_SCENE_DETECTION_MODE[] 			= "scene-detection-mode";
const char GnCameraParameters::KEY_SUPPOTED_SCENE_DETECTION_MODE[] 	= "scene-detection-mode-supported";
const char GnCameraParameters::SCENE_DETECTION_ON[] 				= "on";
const char GnCameraParameters::SCENE_DETECTION_OFF[] 				= "off";

const char GnCameraParameters::KEY_MIRROR_MODE[] = "mirror-mode";
const char GnCameraParameters::KEY_SUPPOTED_MIRROR_MODE[] = "mirror-mode-supported";
const char GnCameraParameters::KEY_MIRROR_ON[] = "on";
const char GnCameraParameters::KEY_MIRROR_OFF[] = "off";

const char GnCameraParameters::KEY_LIVE_EFFECT[] = "live-effect";
const char GnCameraParameters::KEY_SUPPORTED_LIVE_EFFECTS[] = "live-effect-values";
const char GnCameraParameters::LIVE_EFFECT_NONE[] = "none";
const char GnCameraParameters::LIVE_EFFECT_FLEETINGTIME[] = "fleetingtime";
const char GnCameraParameters::LIVE_EFFECT_CRAYON[] = "crayon";
const char GnCameraParameters::LIVE_EFFECT_SNOWFLAKES[] = "snowflakes";
const char GnCameraParameters::LIVE_EFFECT_LIGHTBEAM[] = "lightbeam";
const char GnCameraParameters::LIVE_EFFECT_REFLECTION[] = "reflection";
const char GnCameraParameters::LIVE_EFFECT_SUNSET[] = "sunset";
const char GnCameraParameters::LIVE_EFFECT_REVERSAL[] = "reversal";
const char GnCameraParameters::LIVE_EFFECT_WARMLOMO[] = "warmlomo";
const char GnCameraParameters::LIVE_EFFECT_COLDLOMO[] = "coldlomo";
const char GnCameraParameters::LIVE_EFFECT_SOFTPINK[] = "softpink";
const char GnCameraParameters::LIVE_EFFECT_JAPANBACKLIGHT[] = "japan-backlight";
const char GnCameraParameters::LIVE_EFFECT_COSMETOLOGY_BACKLIGHT[] = "cosmetology-backlight";
const char GnCameraParameters::LIVE_EFFECT_BLACK_WHITE[] = "blackwhite";
const char GnCameraParameters::LIVE_EFFECT_FINEFOOD[] = "finefood";
//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 begin
#ifdef ORIGINAL_VERSION
#else
const char GnCameraParameters::KEY_DEFOG_SHOT_MODE[] = "defogshot-mode";
const char GnCameraParameters::KEY_DEFOG_SHOT_SUPPORTED[] = "defogshot-supported";
const char GnCameraParameters::DEFOG_SHOT_MODE_ON[] = "on";
const char GnCameraParameters::DEFOG_SHOT_MODE_OFF[]= "off";

const char GnCameraParameters::KEY_SINGLE_HDR_MODE[] = "single-hdr-mode";
const char GnCameraParameters::KEY_SINGLE_HDR_SUPPORTED[] = "single-hdr-supported";
const char GnCameraParameters::SINGLE_HDR_ON[] = "on";
const char GnCameraParameters::SINGLE_HDR_OFF[] = "off";

const char GnCameraParameters::KEY_SUPER_PHOTO_SUPPORTED[] = "superphoto-supported";
const char GnCameraParameters::KEY_SUPER_PHOTO_SCALE_VALUE[] = "superphoto-scale-value";
const char GnCameraParameters::CAPTURE_MODE_SUPER_PHOTO[] = "superphoto";

const char GnCameraParameters::KEY_SUPER_ZOOM_MODE[] = "superzoom-mode";
const char GnCameraParameters::KEY_SUPER_ZOOM_SUPPORTED[] = "superzoom-supported";
const char GnCameraParameters::SUPER_ZOOM_MODE_ON[] = "on";
const char GnCameraParameters::SUPER_ZOOM_MODE_OFF[] = "off";

const char GnCameraParameters::KEY_SUPER_ZOOM[] = "super-zoom";

const char GnCameraParameters::KEY_NIGHT_VIDEO_MODE[] = "night-video-mode";
const char GnCameraParameters::KEY_NIGHT_VIDEO_SUPPORTED[] = "night-video-supported";
const char GnCameraParameters::NIGHT_VIDEO_ON[] = "on";
const char GnCameraParameters::NIGHT_VIDEO_OFF[] = "off";
#endif
//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 end
const char GnCameraParameters::CAPTURE_MODE_NIGHT_SHOT[] = "nightshot";
const char GnCameraParameters::CAPTURE_MODE_REFOCUS_SHOT[] = "refocusshot";

const char GnCameraParameters::KEY_GESTURE_SUPPORTED[] = "gesture-supported";
const char GnCameraParameters::KEY_GESTURE_MODE[] = "gesture";
const char GnCameraParameters::GESTURE_MODE_ON[] = "on";
const char GnCameraParameters::GESTURE_MODE_OFF[] = "off"; 

const char GnCameraParameters::KEY_SOUND_STATE[] = "camera-shutter-sound";
const char GnCameraParameters::KEY_SOUND_ON[] = "on";
const char GnCameraParameters::KEY_SOUND_OFF[] = "off";

//Gionee <litao> <2014-01-09> modify for CR01434897 begin
#ifdef CONFIG_GN_CAMERA_MANUAL_SHUTTER
const char GnCameraParameters:: KEY_SHUTTER_VALUE[]="shutter-value"; 
const char GnCameraParameters:: KEY_SUPPORT_SHUTTER_SPEED[]="support-shutter-speed";
const char GnCameraParameters:: KEY_SUPPORT_SHUTTER_SPEED_VALUES[]="support-shutter-speed-values";
const char GnCameraParameters:: KEY_SHOW_SUPPORT_SHUTTER_SPEED[]="show-support-shutter-speed";
const char GnCameraParameters:: KEY_SHOW_SUPPORT_SHUTTER_SPEED_VALUES[]="show-support-shutter-speed-values";
const char GnCameraParameters:: KEY_SHUTTER_SPEED[]="shutter-speed";
const char GnCameraParameters:: KEY_SHUTTER_SPEED_VALUES[]="shutter-speed-values";
#endif
//Gionee <litao> <2014-01-09> modify for CR01434897 end

const char GnCameraParameters::KEY_GESTURE_EVENT_SUPPORTED[] = "gesture-event-supported";
const char GnCameraParameters::KEY_GESTURE_EVENT_TYPE[] = "gesture-event-type";
const int  GnCameraParameters::KEY_GESTURE_EVENT_NONE = 0;
const int  GnCameraParameters::KEY_GESTURE_EVENT_OPEN_HAND_PRESENCE = 0x01;
const int  GnCameraParameters::KEY_GESTURE_EVENT_FIST_PRESENCE = 0x02;
const int  GnCameraParameters::KEY_GESTURE_EVENT_FACE_PRESENCE = 0x04;
const int  GnCameraParameters::KEY_GESTURE_EVENT_THUMBS_UP_PRESENCE = 0x08;
const int  GnCameraParameters::KEY_GESTURE_EVENT_VSIGN_PRESENCE = 0x10;

bool
GnCameraParameters::
setDefaultParameters() 
{
	bool ret = true;
	
	set(GnCameraParameters::KEY_FACE_BEAUTY_LEVEL, 0);
	set(GnCameraParameters::KEY_SUPPORTED_LIVE_EFFECTS, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_LIVE_EFFECT, GnCameraParameters::LIVE_EFFECT_NONE);
	set(GnCameraParameters::KEY_SUPPOTED_MIRROR_MODE, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_MIRROR_MODE, GnCameraParameters::KEY_MIRROR_OFF);
	set(GnCameraParameters::KEY_SUPPOTED_SCENE_DETECTION_MODE, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_SCENE_DETECTION_MODE, GnCameraParameters::SCENE_DETECTION_OFF);
	set(GnCameraParameters::KEY_GESTURE_SUPPORTED, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_GESTURE_MODE, GnCameraParameters::GESTURE_MODE_OFF);
//Gionee <litao> <2014-01-09> modify for CR01434897 begin
#ifdef CONFIG_GN_CAMERA_MANUAL_SHUTTER
	set(GnCameraParameters::KEY_SHUTTER_VALUE, 0);
#endif
//Gionee <litao> <2014-01-09> modify for CR01434897 end
	set(GnCameraParameters::KEY_GESTURE_EVENT_SUPPORTED, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_GESTURE_EVENT_TYPE, GnCameraParameters::KEY_GESTURE_EVENT_NONE);
	
//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 begin
#ifdef ORIGINAL_VERSION
#else
	#ifdef GN_VISIDON_HDR_SUPPORT
	set(GnCameraParameters::KEY_SINGLE_HDR_SUPPORTED, GnCameraParameters::TRUE);
	#else
	set(GnCameraParameters::KEY_SINGLE_HDR_SUPPORTED, GnCameraParameters::FALSE);
	#endif

	#ifdef GN_VISIDON_SUPER_PHOTO_SUPPORT
	set(GnCameraParameters::KEY_SUPER_PHOTO_SUPPORTED, GnCameraParameters::TRUE);
	set(GnCameraParameters::KEY_SUPER_PHOTO_SCALE_VALUE, "1.0");
	#else
	set(GnCameraParameters::KEY_SUPER_PHOTO_SUPPORTED, GnCameraParameters::FALSE);
	#endif

	#ifdef GN_GIONEE_DEFOG_SHOT_SUPPORT
	set(GnCameraParameters::KEY_DEFOG_SHOT_SUPPORTED, GnCameraParameters::TRUE);
	#else
	set(GnCameraParameters::KEY_DEFOG_SHOT_SUPPORTED, GnCameraParameters::FALSE);
	#endif

	#ifdef GN_VISIDON_RT_SUPER_PHOTO_SUPPORT
	set(GnCameraParameters::KEY_SUPER_ZOOM_SUPPORTED, GnCameraParameters::TRUE);
	#else
	set(GnCameraParameters::KEY_SUPER_ZOOM_SUPPORTED, GnCameraParameters::FALSE);
	#endif

	#ifdef GN_ARCSOFT_NIGHT_VIDEO_SUPPORT
	set(GnCameraParameters::KEY_NIGHT_VIDEO_SUPPORTED, GnCameraParameters::TRUE);
	#else
	set(GnCameraParameters::KEY_NIGHT_VIDEO_SUPPORTED, GnCameraParameters::FALSE);
	#endif
	
#endif
//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 end

	return ret;
}

}; // namespace android

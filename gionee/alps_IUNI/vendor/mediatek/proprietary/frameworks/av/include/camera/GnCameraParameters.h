/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera parameters.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/25
 *
 *************************************************************************************/

#ifndef _MTK_FRAMEWORKS_AV_INCLUDE_CAMERA_GNCAMERAPARAMETERS_H
#define _MTK_FRAMEWORKS_AV_INCLUDE_CAMERA_GNCAMERAPARAMETERS_H

#include <camera/MtkCameraParameters.h>
//#include <GNCameraFeature.h>

#define SUPER_ZOOM_RATIO_THRESHOLD 1.5

namespace android {

class GnCameraParameters : public MtkCameraParameters
{
public:
    GnCameraParameters() : MtkCameraParameters() {}
    GnCameraParameters(const String8 &params) { unflatten(params); }
    ~GnCameraParameters()  {}

    GnCameraParameters& operator=(CameraParameters const& params)
    {
        unflatten(params.flatten());
        return  (*this);
    }

	static const char KEY_FACE_BEAUTY_MODE[];
	static const char KEY_SUPPOTED_FACE_BEAUTY_MODE[];
	static const char FACE_BEAUTY_ON[];
	static const char FACE_BEAUTY_OFF[];
	static const char KEY_FACE_BEAUTY_LEVEL[];
	static const char KEY_SLENDER_FACE_LEVEL[];
	static const char KEY_SKIN_SOFTEN_LEVEL[];
	static const char KEY_SKIN_BRIGHT_LEVEL[];
	static const char KEY_EYE_ENLARGMENT_LEVEL[];

	static const char KEY_FACE_BEAUTY_SUB_MODE[];
	static const char FACE_BEAUTY_AGEGENDER_MODE[];
	static const char FACE_BEAUTY_DEFAULT_MODE[];
	static const char KEY_FACE_BEAUTY_AGEGENDER_LEVEL[];

	//ageGender-detection
	static const char KEY_AGEGENDER_DETECTION_MODE[];
	static const char KEY_SUPPOTED_AGEGENDER_DETECTION_MODE[];	
	static const char AGEGENDER_DETECTION_ON[];
	static const char AGEGENDER_DETECTION_OFF[];

	static const char KEY_SCENE_DETECTION_MODE[];
	static const char KEY_SUPPOTED_SCENE_DETECTION_MODE[];
	static const char SCENE_DETECTION_ON[];
	static const char SCENE_DETECTION_OFF[];

	static const char KEY_MIRROR_MODE[];
	static const char KEY_SUPPOTED_MIRROR_MODE[];
	static const char KEY_MIRROR_ON[];
	static const char KEY_MIRROR_OFF[];

	static const char KEY_LIVE_EFFECT[];
  	static const char KEY_SUPPORTED_LIVE_EFFECTS[];
	static const char LIVE_EFFECT_NONE[];	
	static const char LIVE_EFFECT_FLEETINGTIME[];
	static const char LIVE_EFFECT_CRAYON[];
	static const char LIVE_EFFECT_SNOWFLAKES[];
	static const char LIVE_EFFECT_LIGHTBEAM[];
	static const char LIVE_EFFECT_REFLECTION[];
	static const char LIVE_EFFECT_SUNSET[];
	static const char LIVE_EFFECT_REVERSAL[];
	static const char LIVE_EFFECT_WARMLOMO[];
	static const char LIVE_EFFECT_COLDLOMO[];
	static const char LIVE_EFFECT_SOFTPINK[];
	static const char LIVE_EFFECT_JAPANBACKLIGHT[];
	static const char LIVE_EFFECT_COSMETOLOGY_BACKLIGHT[];
	static const char LIVE_EFFECT_BLACK_WHITE[];
	static const char LIVE_EFFECT_FINEFOOD[];

	//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 begin
	#ifdef ORIGINAL_VERSION
	#else
	static const char KEY_DEFOG_SHOT_MODE[];
	static const char KEY_DEFOG_SHOT_SUPPORTED[];
	static const char DEFOG_SHOT_MODE_ON[];
	static const char DEFOG_SHOT_MODE_OFF[];
	
	static const char KEY_SINGLE_HDR_MODE[];
	static const char KEY_SINGLE_HDR_SUPPORTED[];
	static const char SINGLE_HDR_ON[];
	static const char SINGLE_HDR_OFF[];
	
	static const char KEY_SUPER_PHOTO_SUPPORTED[];
	static const char KEY_SUPER_PHOTO_SCALE_VALUE[];
	static const char CAPTURE_MODE_SUPER_PHOTO[];

	static const char KEY_SUPER_ZOOM_MODE[];
	static const char KEY_SUPER_ZOOM_SUPPORTED[];
	static const char SUPER_ZOOM_MODE_ON[];
	static const char SUPER_ZOOM_MODE_OFF[];

	static const char KEY_SUPER_ZOOM[];

	static const char KEY_NIGHT_VIDEO_MODE[];
	static const char KEY_NIGHT_VIDEO_SUPPORTED[];
	static const char NIGHT_VIDEO_ON[];
	static const char NIGHT_VIDEO_OFF[];
	#endif
	//Gionee <zhuangxiaojian> <2015-05-07> modify for CR01476951 end
	
	static const char CAPTURE_MODE_NIGHT_SHOT[];	
	static const char CAPTURE_MODE_REFOCUS_SHOT[];

	static const char KEY_GESTURE_SUPPORTED[];
	static const char KEY_GESTURE_MODE[];
	static const char GESTURE_MODE_ON[];
	static const char GESTURE_MODE_OFF[];	

    static const char KEY_SOUND_STATE[];
    static const char KEY_SOUND_ON[];
    static const char KEY_SOUND_OFF[];	
	//Gionee <litao> <2014-01-09> modify for CR01434897 begin
	#ifdef CONFIG_GN_CAMERA_MANUAL_SHUTTER
	static const char KEY_SHUTTER_VALUE[];
	static const char KEY_SUPPORT_SHUTTER_SPEED[];
	static const char KEY_SUPPORT_SHUTTER_SPEED_VALUES[];
	static const char KEY_SHOW_SUPPORT_SHUTTER_SPEED[];
	static const char KEY_SHOW_SUPPORT_SHUTTER_SPEED_VALUES[];
	static const char KEY_SHUTTER_SPEED[];
	static const char KEY_SHUTTER_SPEED_VALUES[];
	#endif
	//Gionee <litao> <2014-01-09> modify for CR01434897 end

	//Gesture detection defines
	static const char KEY_GESTURE_EVENT_SUPPORTED[];
	static const char KEY_GESTURE_EVENT_TYPE[];
	static const int KEY_GESTURE_EVENT_NONE;
	static const int KEY_GESTURE_EVENT_OPEN_HAND_PRESENCE;
	static const int KEY_GESTURE_EVENT_FACE_PRESENCE;
	static const int KEY_GESTURE_EVENT_FIST_PRESENCE;
	static const int KEY_GESTURE_EVENT_THUMBS_UP_PRESENCE;
	static const int KEY_GESTURE_EVENT_VSIGN_PRESENCE;

	bool			setDefaultParameters();

};

}; // namespace android

#endif

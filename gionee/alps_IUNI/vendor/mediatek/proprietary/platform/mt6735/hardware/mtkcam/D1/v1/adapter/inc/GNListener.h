/* 
 * Description:
 *		Add for CR01359934.
 * 
 * Author: zhangpj
 * Date	: 2014-08-21
 */


#include <GNCameraFeatureListener.h>
#include <mtkcam/v1/camutils/CamInfo.h>

namespace android {
	class GNListener :public GNCameraFeatureListener
	{
		public:
			GNListener() ;		
			void setMsgCbInfo(sp<CamMsgCbInfo> camMsgCbInfo);	
			void notify(GNCameraMsgType_t msgType, int32 ext1, int32 ext2, int32 ext3 = 0, void* ext4 = 0);
			void updateDisplayDimension(int width,int height);
		private:
			sp<CamMsgCbInfo>  mCamMsgCbInfo;
			int mDisplayWidth;
			int mDisplayHeight;
	};
};

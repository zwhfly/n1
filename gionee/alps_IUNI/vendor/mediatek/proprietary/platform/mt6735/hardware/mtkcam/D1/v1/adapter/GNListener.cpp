/* 
 * Description:
 *		Add for CR01359934.
 * 
 * Author: zhangpj
 * Date	: 2014-08-21
 */


#define LOG_TAG "GNListener"

#include <android/log.h>
#include "GNListener.h"
#include <system/camera.h>
#include <camera/MtkCamera.h>

using namespace android;
#define MAP_TO_DRIVER_COORDINATE(val, base, scale, offset) (val * scale / base + offset)

GNListener::GNListener() 
{
	mCamMsgCbInfo = NULL;
	mDisplayWidth = 1440;
	mDisplayHeight = 1088;
}

void GNListener::setMsgCbInfo(sp<CamMsgCbInfo> camMsgCbInfo)
{
	mCamMsgCbInfo = camMsgCbInfo;
}

void GNListener:: updateDisplayDimension(int width,int height){	
	mDisplayWidth = width;
	mDisplayHeight = height;
}			

void GNListener::notify(GNCameraMsgType_t msgType, int32 ext1, int32 ext2, int32 ext3, void* ext4)
{
#if 0
		PRINTD("msgType = %x, ext1:%d", msgType, ext1);	
		PRINTD("mCamMsgCbInfo.get(%p), mCamMsgCbInfo->getStrongCount(%d)", 
			mCamMsgCbInfo.get(), mCamMsgCbInfo->getStrongCount());
#endif
	
	if (mCamMsgCbInfo != NULL) 
	{
		switch (msgType) 
		{
			case GN_CAMERA_SMG_TYPE_GESTURE_SHOT:				
				mCamMsgCbInfo->mNotifyCb(CAMERA_MSG_GESTURE, 0, 0, mCamMsgCbInfo->mCbCookie);
				break;
				
			case GN_CAMERA_MSG_TYPE_SCENE_DETECTION:				
				mCamMsgCbInfo->mNotifyCb(CAMERA_MSG_SCENE_DETECTION, ext1, 0, mCamMsgCbInfo->mCbCookie);
				break;

			case GN_CAMERA_MSG_TYPE_GESTURE_DETECTION: {
					camera_memory* pmem = mCamMsgCbInfo->mRequestMemory(-1, 7 * sizeof(uint32_t), 1, NULL);
					uint32_t* const pCBData = reinterpret_cast<uint32_t*>(pmem->data);

					pCBData[0] = MTK_CAMERA_MSG_EXT_DATA_GESTURE;		//message type
					pCBData[1] = (uint32_t)ext1 >> 24;					//gesture type
					pCBData[2] = ((uint32_t)ext1 >> 16) & 0x000000FF;	//gesture action
					pCBData[3] = (uint32_t)ext1 & 0x0000FFFF;			//objectId
					pCBData[4] = (uint32_t)ext2;						//gesture x
					pCBData[5] = (uint32_t)ext3;						//gesture y
					pCBData[6] = (uint64_t)ext4 & 0xFFFF; 				//gesture z
					

					mCamMsgCbInfo->mDataCb(
						MTK_CAMERA_MSG_EXT_DATA,
						pmem,
						0,
						NULL,
						mCamMsgCbInfo->mCbCookie
						);

					pmem->release(pmem);
				}
			
				break;	

			case GN_CAMERA_MSG_TYPE_AGEGENDER_DETECTION: {				 				
					AgeGenderResult_t* res = (AgeGenderResult_t*)ext4;
					int i = 0;
					int faceNum = res->faceNumber;
					unsigned char *faceData = NULL;
					size_t faceResultSize = sizeof(camera_frame_metadata_t);
					
					faceResultSize += sizeof(camera_face_t) * MAX_FACE_NUM_DETECT;
					camera_memory_t *pmem = mCamMsgCbInfo->mRequestMemory(-1, sizeof(faceResultSize), 1, NULL);		
					if (pmem == NULL) {
						PRINTD("%s failed to alloc memory", __func__);
						return;
					}
					
					unsigned char* pFaceResult = reinterpret_cast<unsigned char*>(pmem->data);
					memset(pFaceResult, 0, faceResultSize);
					
					faceData = pFaceResult;			
		            camera_frame_metadata_t retFaces;				
					camera_face_t *faces = (camera_face_t *) ( faceData + sizeof(camera_frame_metadata_t) );
		            retFaces.number_of_faces = faceNum;
		            retFaces.faces = reinterpret_cast<camera_face_t*>(faces);
		                 		 			
					for (i = 0; i < faceNum; i ++) {
						faces[i].id = i;
//At present we don't need the face rect data.
#if 0
						faces[i].rect[0] = MAP_TO_DRIVER_COORDINATE(res->faceRect[i].left, mDisplayWidth, 2000, -1000);//res[i].faceRect[i].left;
						faces[i].rect[1] = MAP_TO_DRIVER_COORDINATE(res->faceRect[i].top, mDisplayHeight, 2000, -1000);;//res[i].faceRect[i].top;
						faces[i].rect[2] = faces[i].rect[0] + MAP_TO_DRIVER_COORDINATE(res->faceRect[i].right, mDisplayWidth, 2000, 0);//res[i].faceRect[i].right;
						faces[i].rect[3] = faces[i].rect[1] + MAP_TO_DRIVER_COORDINATE(res->faceRect[i].bottom, mDisplayHeight, 2000, 0);//res[i].faceRect[i].bottom;					
#endif
						faces[i].age = res->ageResult[i];
						faces[i].gender = res->genderResult[i]; 
						faces[i].flawless_level = res->flawlessLevel[i];
					} 				
					
		            mCamMsgCbInfo->mDataCb(
		                CAMERA_MSG_AGEGENDER_DETECTION, 
		                pmem,
		                0,
		                &retFaces,
		                mCamMsgCbInfo->mCbCookie
		            ); 
					
					pmem->release(pmem);
				}
			
				break;
		}
	}
}

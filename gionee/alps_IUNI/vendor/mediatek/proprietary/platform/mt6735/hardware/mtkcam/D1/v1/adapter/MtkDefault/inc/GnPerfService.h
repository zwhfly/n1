/*************************************************************************************
 * 
 * Description:
 * 	Defines API to control cpu performance.
 *    The cpu control codes need be changed according to chipset.
 *    As the cpu control is different in different chipset. 
 *    At present we only is valid for MT6795.
 *
 * Note:
 *	If enable high performance function, will enable all cpu core with highest 
 *	cpu frequence. It will increase the oppotunity of thermal throttle. If you
 *	don't wana get high performance, please disable the function in time.
 *
 * <CR01492399>
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2015/5/29
 *
 *************************************************************************************/

#ifndef _MTK_HAL_CAMADAPTER_GIONEE_INC_GN_PERF_SERVICE_H_
#define _MTK_HAL_CAMADAPTER_GIONEE_INC_GN_PERF_SERVICE_H_

#include <linux/mt_sched.h>
#include <utils/threads.h>
#include <sys/prctl.h> 
#include <dlfcn.h>

#include <mtkcam/common.h>
#include <mtkcam/utils/common.h>
#include <mtkcam/drv/res_mgr_drv.h>
#include <PerfServiceNative.h>

using android::Mutex;

namespace android {
class GnPerfService
{
public:
	GnPerfService();
	~GnPerfService();
	
	bool enableHighPerformance();
	bool disableHighPerformance();

private:
	bool initPerf();
	bool uninitPerf();
	bool enablePerfService();
	bool disablePerfService();
	bool enableForceToBigCore();
	bool disableForceToBigCore();
	
	typedef int  (*funcReg)(int, int);
	typedef int  (*funcUnreg)(int);
	typedef void (*funcEnable)(int);
	typedef void (*funcDisable)(int);
	typedef void (*funcRegScnConfig)(int, int, int, int, int, int);

	Mutex               mPerfLock;
	void*               mPerfLib ;
	funcEnable		    mPerfEnable;
	funcDisable         mPerfDisable;
	funcReg				mPerfReg;
	funcUnreg			mPerfUnreg;
	funcRegScnConfig	mPerfRegScnConfig;
	int					mScenHandle;
	bool				mPerfServiceEnabled;
};
};

#endif /* _MTK_HAL_CAMADAPTER_GIONEE_INC_GN_PERFSERVICE_H_ */

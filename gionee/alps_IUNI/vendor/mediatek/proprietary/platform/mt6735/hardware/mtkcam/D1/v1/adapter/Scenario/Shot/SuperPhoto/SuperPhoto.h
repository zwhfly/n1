/* 
 * Description:
 *		Shot mode for Super Photo.
 * 
 * Author: zhuangxiaojian
 * Date	: 2015-05-20
 */


#ifndef _MTK_CAMERA_CAMADAPTER_SCENARIO_SHOT_SUPERPHOTO_H_
#define _MTK_CAMERA_CAMADAPTER_SCENARIO_SHOT_SUPERPHOTO_H_

#include <pthread.h>
#include <semaphore.h>
#include <vector>
#include <fcntl.h>
#include <linux/cache.h>
#include <mtkcam/drv/imem_drv.h>
#include <mtkcam/utils/ImageBufferHeap.h>
#include <mtkcam/camshot/ISmartShot.h>
#include <mtkcam/iopipe/SImager/ISImager.h>
#include <mtkcam/camshot/_callbacks.h>
#include <mtkcam/camshot/_params.h>
#include "DpBlitStream.h"

#include <GnBaseCamAdapter.h>

#define Debug_Mode 0

namespace android {
namespace NSShot {
/******************************************************************************
 *
 ******************************************************************************/
#define SUPER_PHOTO_CAP_CNT 5 


/******************************************************************************
 *
 ******************************************************************************/
class SuperPhoto : public ImpShot
{
public:
	MUINT32 		mYuvWidth;
	MUINT32 		mYuvHeight;
	MUINT32 		mJpgWidth;
	MUINT32 		mJpgHeight;
	MUINT32 		mThumbWidth;
	MUINT32 		mThumbHeight;	

	static bool 	IsFinalImage;
	
	
public:  ////	 Buffers.
	//	Source.
	IImageBuffer* mpMultiYuvSource[SUPER_PHOTO_CAP_CNT];  //source multi buffer for 3rd algorithm
	IImageBuffer* mJpegBuf;
	IImageBuffer* mSrcBuf;
	IImageBuffer* mThumbBuf;
		
	IMemDrv*		mpIMemDrv;
	IImageBufferAllocator* mpIImageBufAllocator;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Instantiation.
    virtual                         ~SuperPhoto();
                                    SuperPhoto(
                                        char const*const pszShotName, 
                                        uint32_t const u4ShotMode, 
                                        int32_t const i4OpenId
                                    );

public:     ////                    Operations.

    //  This function is invoked when this object is firstly created.
    //  All resources can be allocated here.
    virtual bool                    onCreate();

    //  This function is invoked when this object is ready to destryoed in the
    //  destructor. All resources must be released before this returns.
    virtual void                    onDestroy();

    virtual bool                    sendCommand(
                                        uint32_t const  cmd, 
                                        MUINTPTR const  arg1, 
                                        uint32_t const  arg2,
                                        uint32_t const  arg3 = 0
                                    );

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////                    Operations.
    virtual bool                    onCmd_reset();
	virtual bool					onCmd_capture();
    virtual void                    onCmd_cancel();
	virtual bool					onCmd_GnSetShotParm(MUINTPTR const  arg1);


protected:  ////                    callbacks 
    static MBOOL fgCamShotNotifyCb(MVOID* user, NSCamShot::CamShotNotifyInfo const msg);
    static MBOOL fgCamShotDataCb(MVOID* user, NSCamShot::CamShotDataInfo const msg); 

protected:
    MBOOL           handlePostViewData(MUINT8* const puBuf, MUINT32 const u4Size);
    MBOOL           handleJpegData(
                        IImageBuffer* pJpeg,
                        IImageBuffer* pThumb = NULL,
                        IDbgInfoContainer* pDbg = NULL);

	MBOOL 			requestBufs();
	MBOOL 			releaseBufs();
	MBOOL 			createJpegImg(IImageBuffer const * rSrcImgBufInfo, NSCamShot::JpegParam const & rJpgParm, MUINT32 const u4Transform, IImageBuffer const * rJpgImgBufInfo, MUINT32 & u4JpegSize);
	IImageBuffer*   allocMem(MUINT32 fmt, MUINT32 w, MUINT32 h);
	void 		    deallocMem(IImageBuffer* pBuf);
	MBOOL 			handleYuvDataCallback(IImageBuffer* puBuf[SUPER_PHOTO_CAP_CNT],MUINT32 const u4Size);
	MBOOL 			handleProcessedYuvData(IImageBuffer* puBuf);

private:
    typedef struct
    {
        IImageBuffer* pImgBuf;
        IMEM_BUF_INFO memBuf;
    } ImageBufferMap;
    Vector<ImageBufferMap> mvImgBufMap;

	GnBaseCamAdapter* mpGnAdapter;
	MBOOL mCancelPicture;
};


/******************************************************************************
 *
 ******************************************************************************/
}; // namespace NSShot
}; // namespace android
#endif  //  _MTK_CAMERA_CAMADAPTER_SCENARIO_SHOT_SUPERPHOTO_H_


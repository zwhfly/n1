/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera adapter.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/28
 *
 *************************************************************************************/

#define LOG_TAG "MtkCam/GnCamAdapter"

#include <inc/GnBaseCamAdapter.h>

#define MY_LOGV(fmt, arg...)       CAM_LOGV("[%s] "fmt, __func__, ##arg)
#define MY_LOGD(fmt, arg...)       CAM_LOGD("[%s] "fmt, __func__, ##arg)
#define MY_LOGI(fmt, arg...)       CAM_LOGI("[%s] "fmt, __func__, ##arg)
#define MY_LOGW(fmt, arg...)       CAM_LOGW("[%s] "fmt, __func__, ##arg)
#define MY_LOGE(fmt, arg...)       CAM_LOGE("[%s] "fmt, __func__, ##arg)
#define MY_LOGA(fmt, arg...)       CAM_LOGA("[%s] "fmt, __func__, ##arg)
#define MY_LOGF(fmt, arg...)       CAM_LOGF("[%s] "fmt, __func__, ##arg)

/******************************************************************************
*
*******************************************************************************/
GnBaseCamAdapter::
~GnBaseCamAdapter()
{
	
}

/******************************************************************************
*
*******************************************************************************/
bool
GnBaseCamAdapter::
init()
{	
	bool ret = false;
	int previewWidth = 0;
	int previewHeight = 0;
	int videoWidth = 0;
	int videoHeight = 0;

	//MY_LOGE("%s",__func__);
	mpParamsMgr = owner->getParamsManager();

	mGNListener = GNListener();
	mpGNCameraFeature = GNCameraFeature::createInstance();
	if (mpGNCameraFeature == NULL) {
		MY_LOGE("Failed to create GNCameraFeature.");
		ret = false;
		goto lbExit;
	}

	mpGNCameraFeature->init();
	mpGNCameraFeature->setCameraType(owner->getOpenId() == 0 ? GN_CAMERA_TYPE_BACK : GN_CAMERA_TYPE_FRONT);	
	mpParamsMgr->getPreviewSize(&previewWidth, &previewHeight);
	mpGNCameraFeature->initPreviewSize(previewWidth, previewHeight, GN_IMG_FORMAT_YV12);
	mpParamsMgr->getVideoSize(&videoWidth, &videoHeight);
	mpGNCameraFeature->initVideoSize(videoWidth, videoHeight, GN_IMG_FORMAT_YV12);	

    mpExtImgProc = ExtImgProc::createInstance();
    if(mpExtImgProc != NULL)
    {
       mpExtImgProc->init();
	   mpExtImgProc->setGNCameraFeatureInstance(mpGNCameraFeature);
    }	 
	ret = true;
lbExit:
    if(!ret)
    {
        MY_LOGE("init() fail; now call uninit()");
        uninit();
    }
    return  ret;
}

/******************************************************************************
*
*******************************************************************************/
bool
GnBaseCamAdapter::
uninit()
{	
  //MY_LOGE("%s",__func__);
  if (mpGNCameraFeature != NULL) {
		mpGNCameraFeature->deinit();
		mpGNCameraFeature->destroyInstance();
		mpGNCameraFeature = NULL;
  }

  if(mpExtImgProc != NULL)
  {
  	// Gionee <litao> <2015-03-05> <CR01448914> begin
    mpExtImgProc->setGNCameraFeatureInstance(NULL);
    // Gionee <litao> <2015-03-05> <CR01448914> end
    mpExtImgProc->uninit();
    mpExtImgProc->destroyInstance();
    mpExtImgProc = NULL;
  }	
  
  return true;
}

/******************************************************************************
*
*******************************************************************************/
void
GnBaseCamAdapter::
setGnListener()
{
	int mPreviewWidth = 0;
	int mPreviewHeight = 0;
    mpParamsMgr->getPreviewSize(&mPreviewWidth, &mPreviewHeight);
	mGNListener.updateDisplayDimension(mPreviewWidth,mPreviewHeight);
	
	if (mpGNCameraFeature != NULL && owner->getCamMsgCbInfo() != NULL) 
	{
		mpGNCameraFeature->setCameraListener(&mGNListener);
		mGNListener.setMsgCbInfo(owner->getCamMsgCbInfo()); 
		MY_LOGD("set gn listener success!");
	}	
	else
	{
		MY_LOGD("fail set,mpGNCameraFeature=%p",mpGNCameraFeature);
	}
}


/******************************************************************************
*
*******************************************************************************/
void
GnBaseCamAdapter::
enableShotYuvMsg(ShotParam * shotParam,uint32_t mShotMode)
{	
			int mask = GN_CAMERA_FEATURE_FACE_BEAUTY | GN_CAMERA_FEATURE_EFFECT;	
			if(strcmp(mpParamsMgr->getStr(CameraParameters::KEY_PICTURE_FORMAT), CameraParameters::PIXEL_FORMAT_YUV422I) == 0
				||strcmp(mpParamsMgr->getStr(CameraParameters::KEY_PICTURE_FORMAT), CameraParameters::PIXEL_FORMAT_YUV420SP) == 0
				||(mpGNCameraFeature != NULL && mpGNCameraFeature->getFeatureMask()& mask))
			{
				shotParam->mu4EnableYuvMsg = 1;
			}  else {
				shotParam->mu4EnableYuvMsg = 0;				
			}	
			// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> begin
		int mBITmask = GN_CAMERA_FEATURE_FACE_BEAUTY | GN_CAMERA_FEATURE_EFFECT | GN_CAMERA_FEATUER_NIGHT_SHOT;	
		#if ORIGINAL_VERSION
		#else
			if(mpGNCameraFeature != NULL && mpGNCameraFeature->getFeatureMask()& mBITmask){
				shotParam->mb4BuildInThumbnail = false;
			}
			else {
				shotParam->mb4BuildInThumbnail = true;
			}
		#endif
		// Gionee <liu_hao> <2015-04-21> modify for <CR01449954> end
}

#ifndef EXT_IMG_PROC_H
#define EXT_IMG_PROC_H
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
#include "../../../../../../external/libgn_camera_feature/include/GNCameraFeature.h"
//#include "../../../../../../../../external/libgn_camera_feature/include/GNCameraFeature.h"

using namespace android;
#endif
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 end
//-----------------------------------------------------------------------------
class ExtImgProc : public IExtImgProc
{
    public:
        typedef enum
        {
            BufType_Display     = 0x00000001,
            BufType_PreviewCB   = 0x00000002,
            BufType_Record      = 0x00000004
        }BufTypeEnum;
    //
    protected:
        virtual ~ExtImgProc() {};
    //
    public:
        static ExtImgProc*  createInstance(void);
        virtual void        destroyInstance(void) = 0;
        //
        virtual MBOOL       init(void) = 0;
        virtual MBOOL       uninit(void) = 0;
        virtual MUINT32     getImgMask(void) = 0;
        virtual MBOOL       doImgProc(ImgInfo& img) = 0;
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
		virtual void   setGNCameraFeatureInstance(GNCameraFeature* mpGNCameraFeature) = 0;
#endif
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 end
};
//-----------------------------------------------------------------------------
#endif


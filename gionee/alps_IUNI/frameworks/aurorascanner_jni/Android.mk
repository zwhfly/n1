# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := libaurorascanner_jni
LOCAL_SRC_FILES :=  \
	native/AuroraMediaScanner.cpp \
	native/AuroraStagefrightMediaScanner.cpp  \
	native/AuroraMediaScannerClient.cpp  \
	com_aurora_mediascanner_AuroraScanner.cpp \

LOCAL_LDLIBS := -llog  -ldl -lstdc++ -lc -lm
LOCAL_SHARED_LIBRARIES := \
    libandroid_runtime \
    libdrmmtkwhitelist \
    libbinder \
    libnativehelper \
    libutils  \
    libicuuc  \
    libcutils \
    libaudioutils  \
    libmedia  \
    libsonivox \
    libdrmframework \
	libdrmmtkutil 
	
LOCAL_STATIC_LIBRARIES := libdrmframeworkcommon



LOCAL_C_INCLUDES := \
    $(TOP)/external/icu/icu4c/source/common \
    $(TOP)/external/icu/icu4c/source/i18n    \
    $(TOP)/vendor/mediatek/proprietary/frameworks/av/include \
    $(TOP)/$(MTK_ROOT)/frameworks/av/include \
        external/stlport/stlport \
        bionic
    
	
include $(BUILD_SHARED_LIBRARY)
	

 

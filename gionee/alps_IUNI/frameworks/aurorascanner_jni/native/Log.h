//#include <android/log.h>
#include <utils/Log.h>
#define XLOG_TAG "XH_Log"
#ifndef LOGV
#define LOGV(...) __android_log_print(ANDROID_LOG_INFO ,XLOG_TAG,__VA_ARGS__)
#endif
#ifndef LOGD
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG , XLOG_TAG, __VA_ARGS__)
#endif
#ifndef LOGI
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO  , XLOG_TAG, __VA_ARGS__)
#endif
#ifndef LOGW
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN  , XLOG_TAG, __VA_ARGS__)
#endif
#ifndef LOGE
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , XLOG_TAG, __VA_ARGS__)
#endif

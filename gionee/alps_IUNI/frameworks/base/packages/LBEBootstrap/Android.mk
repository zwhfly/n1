LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := LBEBootstrap
LOCAL_CERTIFICATE := platform

#### comment next line below 5.0
#LOCAL_PRIVILEGED_MODULE := true
####

LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_PACKAGE)

########################
include $(call all-makefiles-under,$(LOCAL_PATH))

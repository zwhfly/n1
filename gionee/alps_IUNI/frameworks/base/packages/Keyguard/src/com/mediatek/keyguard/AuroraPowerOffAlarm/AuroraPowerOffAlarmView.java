package com.mediatek.keyguard.AuroraPowerOffAlarm;

import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.keyguard.KeyguardSecurityCallback;
import com.android.keyguard.KeyguardSecurityView;
import com.mediatek.keyguard.AuroraPowerOffAlarm.Alarms;
import com.android.internal.widget.LockPatternUtils;
import com.android.keyguard.R; 
import com.mediatek.keyguard.AuroraPowerOffAlarm.ShakeListener.OnShakeListener;
import com.mediatek.keyguard.PowerOffAlarm.PowerOffAlarmManager;

import android.view.IWindowManager;
import android.os.ServiceManager;
import android.provider.Settings;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
public class AuroraPowerOffAlarmView extends RelativeLayout implements OnTouchListener,KeyguardSecurityView {

	private static final String TAG = "AuroraPowerOffAlarmView";
	
	private boolean showbar=false;
	 private  KeyguardManager mKeyguardManager;
	 private static final String CLOSE_FULLSCREEN_VIEW = "close.fullscreen.view";
   // These defaults must match the values in res/xml/settings.xml
   // Gionee baorui 2012-09-08 modify for CR00687666 begin
   // private static final String DEFAULT_SNOOZE = "10";
   //private static final String DEFAULT_SNOOZE = "5";
   // Gionee baorui 2012-09-08 modify for CR00687666 end
	// Gionee baorui 2012-09-28 modify for CR00705225 begin
	// private static final String DEFAULT_VOLUME_BEHAVIOR = "2";
	private static final String DEFAULT_VOLUME_BEHAVIOR = "1";
	// Gionee baorui 2012-09-28 modify for CR00705225 end
   protected static final String SCREEN_OFF = "screen_off";
   protected Alarm mAlarm;
   private int mVolumeBehavior;
   boolean mFullscreenStyle;
   private TextView  titleLabel;
   //android:hxc start
   private static final String ALARM_REQUEST_SHUTDOWN_ACTION = "android.intent.action.ACTION_ALARM_REQUEST_SHUTDOWN";
   private static final String NORMAL_SHUTDOWN_ACTION = "android.intent.action.normal.shutdown";
   private static final String DISABLE_POWER_KEY_ACTION = "android.intent.action.DISABLE_POWER_KEY";
   // delay time to finish activity after the boot anim start
	private static final int DELAY_FINISH_TIME = 2;
   // delay time to stop boot anim
   private static final int DELAY_TIME_SECONDS = 7;
   private static final String NORMAL_BOOT_DONE_ACTION = "android.intent.action.normal.boot.done";
   private static final String NORMAL_BOOT_ACTION = "android.intent.action.normal.boot";
   private static final String DEFAULT_POWER_ON_VOLUME_BEHAVIOR = "0";
   private static final String KEY_VOLUME_BEHAVIOR ="power_on_volume_behavior";
   private static final String POWER_OFF_FROM_ALARM = "isPoweroffAlarm";
   private static final String POWER_ON_VOLUME_BEHAVIOR_PREFERENCES = "PowerOnVolumeBehavior";
	private boolean mBootFromPoweroffAlarm;
   private boolean mIsPoweroffAlarm;
   private LayoutInflater mInflater;
   //android:hxc end
  
   private int mLastMoveY = 0;
   private int mYY = 0;
   //关闭闹钟后的红色背景
   private BackgroundView backgroundView;
   private RelativeLayout content_fullcreen;
   //关闭闹钟后提示“正在关闭”
   private TextView textView_bg;
   private AnimationSet as;
   private AnimationSet as2;
   private boolean downInRect = false;
   
   private LinearLayout slidetocloseLY;
   private TextView stillsleepText;
   private Rect mRect;
   
   private int distance;
   private boolean isTouchable = true;

   // Gionee <baorui><2013-03-22> modify for CR00783443 begin
   // Gionee <baorui><2013-03-22> modify for CR00783443 end
   
   //aurora add by tangjun 2013.12.22 start
   private ShakeListener shakeListener;
   private static final int SENSOR_SHAKE = 10;
   public static int alarmTimes = 1;
   //记录上一个闹钟的信息
   public static Alarms lastAlarm;
   private boolean isSensorChanged = false;
   private boolean isPowerChanged =false;
   private ScrollView scrollviewToClose;
   private LinearLayout stillsleepLinear;
   private DigitalClock digitalclock;
   private long timeCount = 0;
   Vibrator mVibrator;
   private static final String NAVI_KEY_HIDE = "navigation_key_hide";
	
   private KeyguardSecurityCallback mCallback;
   private LockPatternUtils mLockPatternUtils;
	private Context mContext;
	public AuroraPowerOffAlarmView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initView();
	}

	public AuroraPowerOffAlarmView(Context context, AttributeSet attrs) {
		this(context, attrs,-1);
		// TODO Auto-generated constructor stub
	}

	public AuroraPowerOffAlarmView(Context context) {
		this(context,null);
		// TODO Auto-generated constructor stub
	}

	private void initView(){
	}
	
	
	 Handler handler = new Handler() {  
		  
	        @Override  
	        public void handleMessage(Message msg) {  
	            super.handleMessage(msg);  
	            switch (msg.what) {  
	            case SENSOR_SHAKE:  
	                  snooze();
	                break;  
	            }  
	        }  
	  
	    }; 
	
	    private static final String DISMISS_AND_POWEROFF = "com.android.deskclock.DISMISS_ALARM";
	    private static final String DISMISS_AND_POWERON = "com.android.deskclock.POWER_ON_ALARM";
	    private static final String ACTION_KAYGUARD_SNOOZE = "com.aurora.keyguard.snooze.acton";
	    private int alarmId = -1;
	    /**
	     * 贪睡模式
	     */
	    private void snooze() {
	    	Log.e("jadon", "keyguard snooze alarmId = "+alarmId);
			Intent intent = new Intent(ACTION_KAYGUARD_SNOOZE);
			intent.putExtra(EXTRA_ALARM_ID, alarmId);
			mContext.sendBroadcast(intent);
	    }
	    private static final String ACTION_KAYGUARD_DISMISS = "com.aurora.keyguard.dissmiss.acton";
	    /**
	     * 关闭闹钟开机模式
	     */
	    // power on the device
	    private void powerOn() {
	        sendBroadCastReciver(ACTION_KAYGUARD_DISMISS);
	        sendBroadCastReciver(DISMISS_AND_POWERON);
	    }
	    
	    private void sendBroadCastReciver(String action) {
	        mContext.sendBroadcast(new Intent(action));
	    }
	    
	 private OnShakeListener mOnShakeListener = new OnShakeListener() {  
			@Override
			public void onShake() {
					
				shakeListener.stop();			
				startVibrato(); 
				new Handler().postDelayed(new Runnable(){
					@Override
					public void run(){
						if ( !isSensorChanged ) {
							isSensorChanged = true;
							Message msg = new Message();  
				            msg.what = SENSOR_SHAKE;  
				            handler.sendMessage(msg);
						}					
						//	   mVibrator.cancel();
							   shakeListener.start();
					}
				}, 500);
							
			}
	    };
	    public void startVibrato(){	
	    	
    		mVibrator.vibrate( new long[]{500,200,500,200}, -1); 
    	}
	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();
		setKeepScreenOn(true);
		scrollviewToClose = (ScrollView)findViewById(R.id.scrollviewtoclose);
		scrollviewToClose.setOnTouchListener(this);
		backgroundView=(BackgroundView)findViewById(R.id.bg);
		setFocusableInTouchMode(true);
		content_fullcreen=(RelativeLayout)findViewById(R.id.content_fullcreen);
        textView_bg=(TextView)findViewById(R.id.textView_bg);
        titleLabel = (TextView)findViewById(R.id.remarks);
        mVibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
        shakeListener = new ShakeListener(mContext);
        shakeListener.setOnShakeListener(mOnShakeListener);
        slidetocloseLY = (LinearLayout)findViewById(R.id.slider_layout);
        stillsleepText = (TextView)findViewById(R.id.stillsleeptext);
        
        stillsleepLinear = (LinearLayout)findViewById(R.id.stillsleeplinear);
        digitalclock = (DigitalClock)findViewById(R.id.digitalClock);
        enableEventDispatching(true);
	}
	
	private final static String EXTRA_ALARM_ID = "alarmid";
	private final static String EXTRA_ALARM_LABEL = "label";
	private final static String EXTRA_ALARM_TIME = "time";
	
	private final static String ACTION_ALARM_DATA = "com.aurora.boot.alarm.label.action";
	
	private long alarmTime = 0l;
	
	private BroadcastReceiver labelReciver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if(intent.getAction().equals(ACTION_ALARM_DATA))
			{
				String label = intent.getStringExtra(EXTRA_ALARM_LABEL);
				alarmTime = intent.getLongExtra(EXTRA_ALARM_TIME,0l);
				titleLabel.setText(TextUtils.isEmpty(label)?"":label);
				alarmId = intent.getIntExtra(EXTRA_ALARM_ID, -1);
			}
		}
	};
	
	
	
	@Override
	protected void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		IntentFilter intentFilter = new IntentFilter(ACTION_ALARM_DATA);
		mContext.registerReceiver(labelReciver, intentFilter);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		mContext.unregisterReceiver(labelReciver);
	}
	
	
	private void enableEventDispatching(boolean flag) {
        try {
            final IWindowManager wm = IWindowManager.Stub
                    .asInterface(ServiceManager
                            .getService(Context.WINDOW_SERVICE));
            if (wm != null) {
                wm.setEventDispatching(flag);
            }
        } catch (RemoteException e) {
            Log.w(TAG, e.toString());
        }
    }
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.e("jadon4", "keyguard onKeyDown");
        if (PowerOffAlarmManager.isAlarmBoot()) {
            switch(keyCode) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                    Log.d(TAG, "onKeyDown() - KeyEvent.KEYCODE_VOLUME_UP, do nothing.") ;
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    Log.d(TAG, "onKeyDown() - KeyEvent.KEYCODE_VOLUME_DOWN, do nothing.") ;
                    return true ;
                case KeyEvent.KEYCODE_POWER:
                	snooze();
                	break;
                default:
                    break;
            }
        }
     return super.onKeyDown(keyCode, event);
    }
	
	
	//aurora add by tangjun 2013.12.16 start 
    private boolean handleActionDownEvenet(MotionEvent event) { 
    	//矩形区域是滑动块响应区域,根据需要修改
    	mRect = new Rect(0, dip2px(mContext,465 ), dip2px(mContext,360), dip2px(mContext,615 )); 
        boolean isHit = mRect.contains((int) event.getX(), (int) event.getY()-dip2px(mContext,25));  
        return isHit;  
    }  
    private Handler mHandler =new Handler ();
	 //判断松开手指时，是否达到末尾即可以开锁了 , 是，则开锁，否则，通过一定的算法使其回退。  
    private void handleActionUpEvent(MotionEvent event){          
        int y = (int) event.getY() ;      
        
        boolean isSucess= ( y <= dip2px(mContext,400 ) || (scrollviewToClose.getScrollY() > 200 && timeCount < dip2px(mContext,67)) );           
        if(isSucess){  
        	//启动关闭闹钟时红色背景动画
        	backgroundView.startDisplayAnim();
        //	开启关闭闹钟动画
        	startBackgroundAnimation();
        	       	
           isTouchable = true;
           resetViewState();
           alarmTimes = 1;
           powerOn();
//           //关闭起床闹铃则关闭飞行模式
//           int counttime  = mAlarm.hour*60 + mAlarm.minutes;
//           if ( counttime >= 300 && counttime <= 660 && mAlarm.daysOfWeek.isRepeatSet() ) {       
//        	   setAirplaneModeOff(mContext);
//           }
        }  
        else if (scrollviewToClose.getScrollY() > 0){//没有成功解锁，以一定的算法使其回退  
            distance = scrollviewToClose.getScrollY();  
            if(distance > 0)  
                mHandler.postDelayed(BackDragImgTask, BACK_DURATION);  
            else{  //复原初始场景  
                resetViewState();  
            }  
        }  
    }  
    
  //回退动画时间间隔值   
    private static int BACK_DURATION = 15 ;   // 20ms  
    //水平方向前进速率  
    private static float VE_HORIZONTAL = 25 ;  //0.1dip/ms  
    private Runnable BackDragImgTask = new Runnable(){  
        
        public void run(){  
            //一下次Bitmap应该到达的坐标值  
            distance = distance - (int)(VE_HORIZONTAL);  
            
            //是否需要下一次动画 ？ 到达了初始位置，不在需要绘制          
            if(distance > 5)  {
            	scrollviewToClose.setScrollY(distance);
            	//Log.e("----222alpha----- = " + String.valueOf(255 - scrollviewToClose.getScrollY()));
            	stillsleepLinear.setAlpha(((float)(500 - scrollviewToClose.getScrollY())) /500 );
            	digitalclock.setAlpha(((float)(500 - scrollviewToClose.getScrollY())) /500 );
                mHandler.postDelayed(BackDragImgTask, BACK_DURATION);  
            } else { //复原初始场景  
                resetViewState();     
            }                 
        }  
    };  
  //重置初始的状态，显示tv_slider_icon图像，使bitmap不可见  
    private void resetViewState(){  
    	scrollviewToClose.setScrollY(0);
    	stillsleepLinear.setAlpha(1);
    	digitalclock.setAlpha(1);
		isTouchable = true;
    }  
    
  //关闭闹钟时背景动画
    public void startBackgroundAnimation(){
       	TranslateAnimation translateAnimation=new TranslateAnimation(0,0,0,-600);
       	translateAnimation.setDuration(500);
       	translateAnimation.setInterpolator(new DecelerateInterpolator());
       	AlphaAnimation alphaAnimation=new AlphaAnimation(1,0);
       	alphaAnimation.setDuration(500);
       	alphaAnimation.setInterpolator(new DecelerateInterpolator());
       	as2=new AnimationSet(false);
       	as2.addAnimation(translateAnimation);
       	as2.addAnimation(alphaAnimation); 
       	as2.setAnimationListener(new Animation.AnimationListener() {
    			
    			@Override
    			public void onAnimationStart(Animation animation) {
    				// TODO Auto-generated method stub
    				
    			}
    			
    			@Override
    			public void onAnimationRepeat(Animation animation) {
    				// TODO Auto-generated method stub
    				
    			}
    			
    			@Override
    			public void onAnimationEnd(Animation animation) {
    				// TODO Auto-generated method stub
    				
    				content_fullcreen.setVisibility(View.GONE);
    				slidetocloseLY.setVisibility(View.GONE);
    				textView_bg.setVisibility(View.VISIBLE);
    				startCloseTextAnimation();
    			}
    		});  
       	content_fullcreen.startAnimation(as2);
       	slidetocloseLY.startAnimation(as2);
    }
    
  //关闭闹钟动画并finish
    public void startCloseTextAnimation(){
    	 as=new AnimationSet(false);
     	TranslateAnimation translateAnimation=new TranslateAnimation(0,0,300,-120);
 	   	//va7.setFillAfter(true);
     	translateAnimation.setDuration(300);
     	translateAnimation.setInterpolator(new DecelerateInterpolator());

 	   	AlphaAnimation alphaAnimation=new AlphaAnimation(0,1);
 	   alphaAnimation.setDuration(300);
 	  alphaAnimation.setInterpolator(new DecelerateInterpolator());
     	as.setFillAfter(true);
 	   	as.addAnimation(translateAnimation);
 	   	as.addAnimation(alphaAnimation);  
 	   	as.setAnimationListener(new Animation.AnimationListener() {
 				
 				@Override
 				public void onAnimationStart(Animation animation) {
 					// TODO Auto-generated method stub
 					
 				}
 				
 				@Override
 				public void onAnimationRepeat(Animation animation) {
 					// TODO Auto-generated method stub
 					
 				}
 				
 				@Override
 				public void onAnimationEnd(Animation animation) {
 					 powerOn();
 					
 				}
 			});
 	   textView_bg.startAnimation(as);
	
    }
    
    private int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int y = (int) event.getY();  
        switch (event.getAction()) {  
        case MotionEvent.ACTION_DOWN:  
            downInRect = handleActionDownEvenet(event);
            timeCount = System.currentTimeMillis();
            break;
        case MotionEvent.ACTION_MOVE:
        	stillsleepLinear.setAlpha(((float)(500 - scrollviewToClose.getScrollY())) /500 );
        	digitalclock.setAlpha(((float)(500 - scrollviewToClose.getScrollY())) /500 );
        	break;  
        case MotionEvent.ACTION_UP:  
            //处理Action_Up事件：  判断是否解锁成功，成功则结束我们的Activity ；否则 ，缓慢回退该图片。
        	if ( isTouchable && downInRect && scrollviewToClose.getScrollY() > 0) {
        		isTouchable = false;
        		timeCount = System.currentTimeMillis() - timeCount;
        		handleActionUpEvent(event);
        	}
        	downInRect = false;
        	break; 
        }
        
        if ( downInRect ) {
        	return false;
        }
        return true;
	}

	@Override
	public void setKeyguardCallback(KeyguardSecurityCallback callback) {
		// TODO Auto-generated method stub
		this.mCallback = callback;
	}

	@Override
	public void setLockPatternUtils(LockPatternUtils utils) {
		// TODO Auto-generated method stub
		this.mLockPatternUtils = utils;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		if(shakeListener != null)
		{
			shakeListener.stop();
		}
	}

	@Override
	public void onResume(int reason) {
		// TODO Auto-generated method stub
		reset();
		if(shakeListener != null)
		{
			shakeListener.start();
		}
	}

	@Override
	public boolean needsInput() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public KeyguardSecurityCallback getCallback() {
		// TODO Auto-generated method stub
		return mCallback;
	}

	@Override
	public void showUsabilityHint() {
		// TODO Auto-generated method stub
	}

	@Override
	public void showBouncer(int duration) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hideBouncer(int duration) {
		// TODO Auto-generated method stub
	}

	@Override
	public void startAppearAnimation() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean startDisappearAnimation(Runnable finishRunnable) {
		// TODO Auto-generated method stub
		return false;
	}
	
}

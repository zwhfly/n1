/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.statusbar.policy;

import java.util.ArrayList;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.systemui.R;
import com.android.systemui.statusbar.phone.PhoneStatusBar;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;

/*
 * author:tymy 
 */

public class AuroraBatteryController extends BroadcastReceiver {
    private static final String TAG = "AuroraBatteryController";

    private int mLevel;
    private boolean mPluggedIn;
    private boolean mCharging;
    private boolean mCharged;
    private boolean mShowBatteryPercentage = true;
    private static final String ACTION_BATTERY_PERCENTAGE_SWITCH = "mediatek.intent.action.BATTERY_PERCENTAGE_SWITCH";
    private ImageView batteryBgIv;
	private TextView  percentageTV;
	private ImageView batteryIv;
	private int icon=0;
	private AnimationDrawable mAnimationDrawable;
	private Context mContext;
	private boolean pluggedBefore=false;
	private KeyguardManager mKeyguardManager;
	private boolean isKeyguard=false;
	private boolean animaEnd;

	/**M:Hazel add for battery flashing image begin*/
	private ImageView mBatteryFlashingImage;
	/**M:Hazel add for battery flashing image end*/
	
    public AuroraBatteryController(Context context) {
        mContext = context;
        mShowBatteryPercentage= (Settings.Secure.getInt(context.getContentResolver(), "battery_percentage", 0) != 0); 
        
        mKeyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        isKeyguard = mKeyguardManager.inKeyguardRestrictedInputMode();
        
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(ACTION_BATTERY_PERCENTAGE_SWITCH);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        context.registerReceiver(this, filter);
        
    }
    
    public void addLabelBg(ImageView v){
    	batteryBgIv = v;
    }

    public void addIconView(ImageView v) {
    	batteryIv=v;
    }

    public void addLabelView(TextView v) {
    	percentageTV=v;
    }
    
    
    /**M:Hazel add for instance object begin*/
    public void addLabelFlashImage(ImageView v){
    	mBatteryFlashingImage = v;
    }
    
    
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        
        if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
        	mLevel = (int)(100f
                    * intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
                    / intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100));
            mPluggedIn = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) != 0;

            final int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
                    BatteryManager.BATTERY_STATUS_UNKNOWN);
            mCharged = status == BatteryManager.BATTERY_STATUS_FULL;
            mCharging = mCharged || status == BatteryManager.BATTERY_STATUS_CHARGING;
            
            isKeyguard = mKeyguardManager.inKeyguardRestrictedInputMode();
            
        	updateBattery(); 
        	
        	pluggedBefore=mPluggedIn;
        }else if (action.equals(ACTION_BATTERY_PERCENTAGE_SWITCH)) {
        	mShowBatteryPercentage = (intent.getIntExtra("state",0) == 1);
        	updateBattery();
        }else if(action.equals(Intent.ACTION_SCREEN_ON)){
        	isKeyguard = mKeyguardManager.inKeyguardRestrictedInputMode();
        	updateBattery();
        }
       
    }
	
    public void updateBattery(){
 	   if(mShowBatteryPercentage){
    		percentageTV.setVisibility(View.VISIBLE);
    		batteryBgIv.setVisibility(View.VISIBLE);
    		batteryIv.setVisibility(View.GONE);    		   		  		   		
    		
    	   if(mCharging&& mPluggedIn){ 
    		   /**M:Hazel start to invoke that begin*/
    		   showBatteryFlashingImage();
    		   /**M:Hazel start to invoke that end*/
    		   if(pluggedBefore != mPluggedIn && mPluggedIn){
					showPercentChargeAnim(); 
    		   }else{    			 
    			   if(animaEnd){
    				   //0xFF07D17C
    			      percentageTV.setTextColor(PhoneStatusBar.getCurrentInvertColor());
    			      percentageTV.setText(mLevel+"");
    			   }else{
    				   showPercentChargeAnim(); 
    			   }
    		   }
    	   }else{
			   /**M:Hazel start to invoke that begin*/
			   hideBatteryFlashingImage();
			   /**M:Hazel start to invoke that end*/
    		   percentageTV.setBackground(null);
    		   if(isKeyguard){
    			   percentageTV.setTextColor(0xffffffff);
    		   }else{
    		       percentageTV.setTextColor(PhoneStatusBar.getCurrentInvertColor());
    		   }
    		   percentageTV.setText(mLevel+"");
    	   }
    	}else{
    		percentageTV.setVisibility(View.GONE);
    		batteryBgIv.setVisibility(View.GONE);
    		batteryIv.setVisibility(View.VISIBLE);
    		
    		if(mCharging){
    			if(mLevel>=0&&mLevel<15){
            		icon=R.drawable.aurora_battery_animation0; 
                }else if(mLevel>=15&&mLevel<35){
                	icon=R.drawable.aurora_battery_animation1;  
                }else if(mLevel>=35&&mLevel<49){
                	icon=R.drawable.aurora_battery_animation2;  
                }else if(mLevel>=49&&mLevel<60){
                	icon=R.drawable.aurora_battery_animation3; 
                }else if(mLevel>=60&&mLevel<75){
                	icon=R.drawable.aurora_battery_animation4; 
                }else if(mLevel>=75&&mLevel<90){
                	icon=R.drawable.aurora_battery_animation5; 
                }else if(mLevel>=90&&mLevel<100){
                	icon=R.drawable.aurora_battery_animation6; 
                }else if(mLevel==100){
                	icon=R.drawable.aurora_battery_animation7;  
                }
    			batteryIv.setImageResource(icon);
    			
            	mAnimationDrawable = (AnimationDrawable) batteryIv.getDrawable();
            	mAnimationDrawable.start();
     		   /**M:Hazel start to invoke that begin*/
     		   showBatteryFlashingImage();
     		   /**M:Hazel start to invoke that end*/
    		}else{   					
    			if(mLevel>=0&&mLevel<4){
            		icon=R.drawable.aurora_stat_sys_battery_0; 
                }else if(mLevel>=4&&mLevel<15){
                	if(PhoneStatusBar.getCurrentInvertColor()==0xffffffff){
                		icon=R.drawable.aurora_stat_sys_battery_15;
                	}else{
                		icon=R.drawable.aurora_stat_sys_battery_black_15;
                	}
                }else if(mLevel>=15&&mLevel<35){
                	icon=R.drawable.aurora_stat_sys_battery_28;  
                }else if(mLevel>=35&&mLevel<49){
                	icon=R.drawable.aurora_stat_sys_battery_43; 
                }else if(mLevel>=49&&mLevel<60){
                	icon=R.drawable.aurora_stat_sys_battery_57; 
                }else if(mLevel>=60&&mLevel<75){
                	icon=R.drawable.aurora_stat_sys_battery_71; 
                }else if(mLevel>=75&&mLevel<90){
                	icon=R.drawable.aurora_stat_sys_battery_85; 
                }else if(mLevel>=90&&mLevel<=100){
                	icon=R.drawable.aurora_stat_sys_battery_100;  
                }
    			    			
    			batteryIv.setImageResource(icon);
    			
    			if(mAnimationDrawable!=null){
    				  mAnimationDrawable.stop();
    			}  	
 			   /**M:Hazel start to invoke that begin*/
 			   hideBatteryFlashingImage();
 			   /**M:Hazel start to invoke that end*/
    		}
    		
    	}
 	   //M:xiejun immersion type begin 
		if(mLevel>=4&&mLevel<15&&!mCharging){
			isLowBatteryAndNotCharging = true;
		}else{
			isLowBatteryAndNotCharging = false;
		}
		//M:xiejun immersion type end
 	  immerseViews();
    }
      
    private Handler mHandler = new Handler();	

	private Runnable chargePercentAnim = new Runnable() {
		
		@Override
		public void run() {			
			percentageTV.setBackground(null);			
			
			if(mCharging){
				//0xFF07D17C
				percentageTV.setTextColor(PhoneStatusBar.getCurrentInvertColor());
			}else{
				if(isKeyguard){
	    			   percentageTV.setTextColor(0xffffffff);
	    		   }else{
	    		       percentageTV.setTextColor(PhoneStatusBar.getCurrentInvertColor());
	    		   }
			}
			percentageTV.setText(mLevel+"");
			animaEnd=true;
		}
	};

	private void showPercentChargeAnim(){
		animaEnd=false;
		
//		percentageTV.setText("");
//		percentageTV.setTextColor(0x00000000);
//		
//		if(isKeyguard){
//			percentageTV.setBackgroundResource(R.drawable.aurora_charge_flash);
//		}else{
//		    if(PhoneStatusBar.getCurrentInvertColor()==0xff000000){
//			   percentageTV.setBackgroundResource(R.drawable.aurora_charge_flash_black);
//		    }else{
//			   percentageTV.setBackgroundResource(R.drawable.aurora_charge_flash);
//		    }
//		}	
		mHandler.removeCallbacks(chargePercentAnim);
		mHandler.postDelayed(chargePercentAnim, 0);
	}
	
	
	//M:xiejun immersion type begin 
	private int mDefaultColor = 0xffffffff;
	private PorterDuffColorFilter mColorFilter = new PorterDuffColorFilter(mDefaultColor, PorterDuff.Mode.MULTIPLY);
	public static boolean isLowBatteryAndNotCharging = false;
	
	private void immerseViews(){
		mColorFilter.setColor(PhoneStatusBar.getCurrentInvertColor());
		Drawable  drawable =null;
		if(mShowBatteryPercentage){
			 drawable = batteryBgIv.getDrawable();	
		}else{
			drawable = batteryIv.getDrawable();
		}
		
		if(drawable instanceof AnimationDrawable){
			int size=((AnimationDrawable)drawable).getNumberOfFrames();
	    	for(int i=0;i<size;i++){
	    		((AnimationDrawable)drawable).getFrame(i).setColorFilter(mColorFilter);
			} 
		}else{
			if(mShowBatteryPercentage){
				drawable.setColorFilter(mColorFilter);
			}else{
				if(isLowBatteryAndNotCharging){
					
				}else{
					drawable.setColorFilter(mColorFilter);
				}
			}
		}
	}
	//M:xiejun immersion type end
    
	/**M:Hazel add for show or hide battery  image begin*/
	private void showBatteryFlashingImage(){
		mBatteryFlashingImage.setVisibility(View.VISIBLE);
		if(isKeyguard){
			mBatteryFlashingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.aurora_charge_flash));
		}else{
		    if(PhoneStatusBar.getCurrentInvertColor()==0xff000000){
		    	mBatteryFlashingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.aurora_charge_flash_black));
		    }else{
		    	mBatteryFlashingImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.aurora_charge_flash));
		    }
		}
	}
	
	private void hideBatteryFlashingImage(){
		mBatteryFlashingImage.setVisibility(View.GONE);
		mBatteryFlashingImage.setImageDrawable(null);
	}
	/**M:Hazel add for show or hide battery image end*/
}

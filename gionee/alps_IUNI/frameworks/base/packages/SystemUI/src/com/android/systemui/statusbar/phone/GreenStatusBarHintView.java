package com.android.systemui.statusbar.phone;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.notification.StatusBarNotification;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.view.MotionEvent;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.ActivityManagerNative;
import com.android.systemui.R;
public class GreenStatusBarHintView extends FrameLayout {
	public static final String TAG = "GreenStatusBarHintView";
	private Context mContext;
	private PhoneStatusBarView mPhoneStatusBarView;
	private GestureDetector mGD;
	private StatusBarNotification mSB;
	private TextView mIncallTextView;
	private TextView mAlarmTextView;
	private SimpleOnGestureListener mSGListener = new GestureDetector.SimpleOnGestureListener(){
		public boolean onSingleTapUp(MotionEvent e) {
			doAction();
			return false;
		}
	};

	public GreenStatusBarHintView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mGD = new GestureDetector(context, mSGListener);
	}
	
	public void setPhoneStatusBarView(PhoneStatusBarView view){
		mPhoneStatusBarView = view;
	}
	
	public void setStatusBarNotification(StatusBarNotification sb) {
		this.mSB = sb;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.i(TAG,"GreenStatusBarHintView :  onTouchEvent");
		mGD.onTouchEvent(event);
		return mPhoneStatusBarView.onTouchEvent(event);
	}
	
	private void doAction() {
		if(mSB != null&&mSB.getNotification()!=null&&mSB.getNotification().contentIntent!=null){
			PendingIntent pi = mSB.getNotification().contentIntent;
			try {
				ActivityManagerNative.getDefault().resumeAppSwitches();
				//ActivityManagerNative.getDefault().dismissKeyguardOnNextActivity();
				pi.send(mContext, 0, null);
				Log.i(TAG,"GreenStatusBarHintView doAction");
			} catch (Exception e) {
				Log.i(TAG,"doAction Error:"+pi);
			}
			KeyguardManager km =  (KeyguardManager)mContext.getSystemService(Context.KEYGUARD_SERVICE);
			if(km!=null){
				km.exitKeyguardSecurely(null);
			}else{
				Log.i(TAG,"doAciton error:Got keyguard manager service is null");
			}
		}
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		View v1 =this.findViewById(R.id.show_alarming);
		if(v1!=null){
			mAlarmTextView = (TextView)v1;
		}
		View v2 =this.findViewById(R.id.show_calling);
		if(v2!=null){
			mIncallTextView = (TextView)v2;
		}
	}
	
	@Override
	protected void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        getContext().registerReceiver(mLocalChangeReceiver, filter, null, null);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		getContext().unregisterReceiver(mLocalChangeReceiver);
	}
	
	private BroadcastReceiver mLocalChangeReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if(Intent.ACTION_LOCALE_CHANGED.equals(intent.getAction())){
				if(mAlarmTextView!=null){
					mAlarmTextView.setText(R.string.back_to_alarm_clock);
				}
				if(mIncallTextView!=null){
					mIncallTextView.setText(R.string.back_to_call);
				}
				
			}
			
		}
	};
}

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.recent;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.ActivityManager;
import android.app.ActivityManager.RecentTaskInfo;
import android.app.ActivityManagerNative;
import android.app.ActivityOptions;
import android.app.TaskStackBuilder;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewRootImpl;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView.ScaleType;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.systemui.R;
import com.android.systemui.qs.QSPanel;
import com.android.systemui.qs.QSPanel.QuickSettingCallback;
import com.android.systemui.recent.HandlerBar.HandlerBarCallback;
import com.android.systemui.statusbar.BaseStatusBar;
import com.android.systemui.statusbar.StatusBarPanel;
import com.android.systemui.statusbar.phone.PhoneStatusBar;
import com.android.systemui.statusbar.phone.PhoneStatusBarPolicy;

import java.util.ArrayList;
import java.util.List;

public class RecentsPanelView extends FrameLayout implements OnItemClickListener, RecentsCallback,
        StatusBarPanel, Animator.AnimatorListener {
    static final String TAG = "RecentsPanelView";
    static final boolean DEBUG = PhoneStatusBar.DEBUG || false;
    private PopupMenu mPopup;
    private View mRecentsScrim;
    private View mRecentsNoApps;
    //M: tangjun mod begin
    //private RecentsScrollView mRecentsContainer;
    private ViewGroup mRecentsContainer;
    //M: tangjun mod end
    
    //M: tangjun add fileds begin
	public static int phoneHeight;
	public static int phoneWidth;
	private HandlerBarCallback mHandlerBarCallback;
	private IntentFilter mIntentFilter;
	private static final String VOICEWAKEUP_UNLOCK = "com.qualcomm.listen.voicewakeup.unlock";
	public static final String NOTIFY_APPS_TO_UPDATEIICON = "com.aurora.action.pulbicres.update";
	private RemoveTaskPolicy mRemoveTaskPolicy;
	private View mRecentsMaskView;
	private ObjectAnimator animQuickSettingTranslation, animQuickSettingAlpha, bgAlpha;
	private AnimatorSet mEnterAnimSet = new AnimatorSet();
	private View mQuickSetting;
	private int quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start);
	private int quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end);
	private static final String RECENTS_PANEL_HIDDEN = "com.android.systemui.recent.aurora.RECENTS_PANEL_HIDDEN";
	private static final int QUIT_ANIMATION_DURATION = 300;
	private static final int MASK_VIEW_FULL_ALPHA = 210;
	private TextView mNoAppLabel;
	public boolean mAnimating = false;
	public boolean isScreenOff = false;
	private AuroraRubbishView mAuroraRubbishView;
	public static boolean isResponseHomeKey = false;
	private boolean isShowInputMethod = false;
	private static volatile int sMaskAlpha = 0; // Hold current alpha of mRecentsMaskView
	
	private QSPanel mQuickSettingPanel;
	private boolean mListening;
	//M: tangjun add fileds end

    private boolean mShowing;
    private boolean mWaitingToShow;
    private ViewHolder mItemToAnimateInWhenWindowAnimationIsFinished;
    private boolean mAnimateIconOfFirstTask;
    private boolean mWaitingForWindowAnimation;
    private long mWindowAnimationStartTime;
    private boolean mCallUiHiddenBeforeNextReload;

    private RecentTasksLoader mRecentTasksLoader;
    private ArrayList<TaskDescription> mRecentTaskDescriptions;
    private TaskDescriptionAdapter mListAdapter;
    private int mThumbnailWidth;
    private boolean mFitThumbnailToXY;
    private int mRecentItemLayoutId;
    private boolean mHighEndGfx;
    
    private static final HandlerThread sWorkerThread = new HandlerThread("task-delete");
    static {
        sWorkerThread.start();
    }
    private static final Handler sWorker = new Handler(sWorkerThread.getLooper());
    
    public void init(HandlerBarCallback hbc, int mPhoneWidth, int mPhoenHeight){
    	mHandlerBarCallback = hbc;
    	phoneWidth = mPhoneWidth;
    	phoneHeight = mPhoenHeight;
    	
    	mIntentFilter = new IntentFilter();
    	mIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);
    	
    	mIntentFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
    	mIntentFilter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
    	
    	mIntentFilter.addAction("com.aurora.deskclock.startalarm");
    	mIntentFilter.addAction("com.aurora.deskclock.stopalarm");

        mIntentFilter.addAction(VOICEWAKEUP_UNLOCK);

        mIntentFilter.addAction("hhh.hhh.hhh.hhh");
        
        //iht 2015-03-23 更新图标广播
        mIntentFilter.addAction(NOTIFY_APPS_TO_UPDATEIICON);

    	mContext.registerReceiver(mIntentReceiver, mIntentFilter);

        mIntentFilter = new IntentFilter(Intent.ACTION_PACKAGE_REMOVED);
        mIntentFilter.addDataScheme("package");
    	mContext.registerReceiver(mPackageMgrReceiver, mIntentFilter);

        mRemoveTaskPolicy = new RemoveTaskPolicy(mContext);
    }
    
    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        phoneWidth = w;
        phoneHeight = h;
    }

    public static interface RecentsScrollView {
        public int numItemsInOneScreenful();
        public void setAdapter(TaskDescriptionAdapter adapter);
        public void setCallback(RecentsCallback callback);
        public void setMinSwipeAlpha(float minAlpha);
        public View findViewForTask(int persistentTaskId);
        public void drawFadedEdges(Canvas c, int left, int right, int top, int bottom);
        public void setOnScrollListener(Runnable listener);
    }

    private final class OnLongClickDelegate implements View.OnLongClickListener {
        View mOtherView;
        OnLongClickDelegate(View other) {
            mOtherView = other;
        }
        public boolean onLongClick(View v) {
            return mOtherView.performLongClick();
        }
    }

    /* package */ final static class ViewHolder {
    	//M: tangjun mod begin
    	ImageView lockView;
        //View thumbnailView;
        //ImageView thumbnailViewImage;
        //Drawable thumbnailViewDrawable;
        ImageView iconView;
        TextView labelView;
        TextView descriptionView;
        //View calloutLine;
        TaskDescription taskDescription;
        //boolean loadedThumbnailAndIcon;
        //M: tangjun mod end
        
        //M:xiejun screen pin begin
        ImageView screenPinView;
        //M:xiejun screen pin end
    }

    /* package */ final class TaskDescriptionAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public TaskDescriptionAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return mRecentTaskDescriptions != null ? mRecentTaskDescriptions.size() : 0;
        }

        public Object getItem(int position) {
            return position; // we only need the index
        }

        public long getItemId(int position) {
            return position; // we just need something unique for this position
        }

        public View createView(ViewGroup parent) {
            View convertView = mInflater.inflate(mRecentItemLayoutId, parent, false);
            ViewHolder holder = new ViewHolder();
            //M: tangjun mod begin
            //holder.thumbnailView = convertView.findViewById(R.id.app_thumbnail);
            //holder.thumbnailViewImage =
            //        (ImageView) convertView.findViewById(R.id.app_thumbnail_image);
            // If we set the default thumbnail now, we avoid an onLayout when we update
            // the thumbnail later (if they both have the same dimensions)
            //updateThumbnail(holder, mRecentTasksLoader.getDefaultThumbnail(), false, false);
            holder.lockView = (ImageView) convertView.findViewById(R.id.app_lock);
            holder.iconView = (ImageView) convertView.findViewById(R.id.app_icon);
            holder.iconView.setImageDrawable(mRecentTasksLoader.getDefaultIcon());
            holder.labelView = (TextView) convertView.findViewById(R.id.app_label);
            //M:xiejun screen pin begin
            holder.screenPinView = (ImageView) convertView.findViewById(R.id.app_screen_pin);
            //M:xiejun screen pin end
            //holder.calloutLine = convertView.findViewById(R.id.recents_callout_line);
            holder.descriptionView = (TextView) convertView.findViewById(R.id.app_description);
            //M: tangjun mod end

            convertView.setTag(holder);
            return convertView;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = createView(parent);
            }
            final ViewHolder holder = (ViewHolder) convertView.getTag();

            // index is reverse since most recent appears at the bottom...
            final int index = mRecentTaskDescriptions.size() - position - 1;

            final TaskDescription td = mRecentTaskDescriptions.get(index);

            holder.labelView.setText(td.getLabel());
            //M: tangjun mod begin
            /*
            holder.thumbnailView.setContentDescription(td.getLabel());
            holder.loadedThumbnailAndIcon = td.isLoaded();
            */
            //M: tangjun mod end
            if (td.isLoaded()) {
                //updateThumbnail(holder, td.getThumbnail(), true, false);
                updateIcon(holder, td.getIcon(), true, false);
            }

            //modify by luolaigang for dual wechat start
            if(!td.intent.hasCategory("android.intent.category.CLONED")){
            	((AuroraRecentlItemView)convertView).updateLocked( td.getResolveInfo(),  td.getLabel(), td.getResolveInfo().activityInfo.taskAffinity);
            }
            //modify by luolaigang for dual wechat end
            
            if (index == 0) {
                if (mAnimateIconOfFirstTask) {
                    ViewHolder oldHolder = mItemToAnimateInWhenWindowAnimationIsFinished;
                    if (oldHolder != null) {
                        oldHolder.iconView.setAlpha(1f);
                        oldHolder.iconView.setTranslationX(0f);
                        oldHolder.iconView.setTranslationY(0f);
                        oldHolder.labelView.setAlpha(1f);
                        oldHolder.labelView.setTranslationX(0f);
                        oldHolder.labelView.setTranslationY(0f);
                        //M: tangjun mod begin
                        /*
                        if (oldHolder.calloutLine != null) {
                            oldHolder.calloutLine.setAlpha(1f);
                            oldHolder.calloutLine.setTranslationX(0f);
                            oldHolder.calloutLine.setTranslationY(0f);
                        }
                        */
                        //M: tangjun mod end
                    }
                    mItemToAnimateInWhenWindowAnimationIsFinished = holder;
                    int translation = -getResources().getDimensionPixelSize(
                            R.dimen.status_bar_recents_app_icon_translate_distance);
                    final Configuration config = getResources().getConfiguration();
                    if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
                        if (getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                            translation = -translation;
                        }
                        holder.iconView.setAlpha(0f);
                        holder.iconView.setTranslationX(translation);
                        holder.labelView.setAlpha(0f);
                        holder.labelView.setTranslationX(translation);
                        //M: tangjun mod begin
                        /*
                        holder.calloutLine.setAlpha(0f);
                        holder.calloutLine.setTranslationX(translation);
                        */
                        //M: tangjun mod end
                    } else {
                        holder.iconView.setAlpha(0f);
                        holder.iconView.setTranslationY(translation);
                    }
                    if (!mWaitingForWindowAnimation) {
                        animateInIconOfFirstTask();
                    }
                }
            }
            //M: tangjun mod begin
            /*
            holder.thumbnailView.setTag(td);
            holder.thumbnailView.setOnLongClickListener(new OnLongClickDelegate(convertView));
            */
            //M: tangjun mod end
            holder.taskDescription = td;
            return convertView;
        }

        public void recycleView(View v) {
            ViewHolder holder = (ViewHolder) v.getTag();
            //updateThumbnail(holder, mRecentTasksLoader.getDefaultThumbnail(), false, false);
            holder.iconView.setImageDrawable(mRecentTasksLoader.getDefaultIcon());
            holder.iconView.setVisibility(INVISIBLE);
            holder.iconView.animate().cancel();
            holder.labelView.setText(null);
            holder.labelView.animate().cancel();
            //M: tangjun mod begin
            /*
            holder.thumbnailView.setContentDescription(null);
            holder.thumbnailView.setTag(null);
            holder.thumbnailView.setOnLongClickListener(null);
            holder.thumbnailView.setVisibility(INVISIBLE);
            */
            //M: tangjun mod end
            holder.iconView.setAlpha(1f);
            holder.iconView.setTranslationX(0f);
            holder.iconView.setTranslationY(0f);
            holder.labelView.setAlpha(1f);
            holder.labelView.setTranslationX(0f);
            holder.labelView.setTranslationY(0f);
            //M: tangjun mod begin
            /*
            if (holder.calloutLine != null) {
                holder.calloutLine.setAlpha(1f);
                holder.calloutLine.setTranslationX(0f);
                holder.calloutLine.setTranslationY(0f);
                holder.calloutLine.animate().cancel();
            }
            */
            holder.lockView.setVisibility(INVISIBLE);
            //M: tangjun mod end
            holder.taskDescription = null;
            //holder.loadedThumbnailAndIcon = false;
        }
    }

    public RecentsPanelView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecentsPanelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        updateValuesFromResources();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RecentsPanelView,
                defStyle, 0);

        mRecentItemLayoutId = a.getResourceId(R.styleable.RecentsPanelView_recentItemLayout, 0);
        mRecentTasksLoader = RecentTasksLoader.getInstance(context);
        initQuickSettingsSize();
        a.recycle();
    }
    
    //M: tangjun mod begin
    private View quickSettingsView = null;
	private View recentPanelView = null;

	int last_orientation = getScreenState();
	
	private int getScreenState(){
		return getResources().getConfiguration().orientation;
	}

	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == last_orientation) return;
		last_orientation = newConfig.orientation;
		resetRecentsPanelViewOffset(false);
		initView();
		if(getScreenState() == Configuration.ORIENTATION_PORTRAIT){
			orientationPortraitLayout();
		} else {
			orientationLandLayout();
		}
		rotationChangeAnimation();
	}
	// this method needs modify
	private void rotationChangeAnimation(){
		ObjectAnimator animReset01 = null;
		ObjectAnimator animReset02 = null;

		if(isOrientationPortrait()){
			((RecentsLayout)mRecentsMaskView).startScroll(0, 0, 0, HandlerBar.mRecentsScrimHeight, HandlerBar.AUTO_ENTER_ANIMATION_DURATION);
		} else {
			((RecentsLayout)mRecentsMaskView).startScroll(0, 0, HandlerBar.mRecentsScrimHeight, 0, HandlerBar.AUTO_ENTER_ANIMATION_DURATION);
		}
		autoQuickSettingEnterAnimation();
	}

	private void resetRecentsPanelViewOffset(boolean removePanel){
		if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            animQuickSettingTranslation.cancel();
        }
        if (mEnterAnimSet != null && mEnterAnimSet.isRunning())
            mEnterAnimSet.cancel();		
		mQuickSetting.setTranslationY(0);		
		mQuickSetting.setTranslationX(0);

		mRecentsMaskView.scrollBy(-mRecentsMaskView.getScrollX(), -mRecentsMaskView.getScrollY());
	}

	private boolean isOrientationPortrait(){
		int orientation = getScreenState();
		if (orientation == Configuration.ORIENTATION_PORTRAIT) return true;
		return false;
	}


	private int quickSettingWidth;
	private int quickSettingHeight;
	private int quickSettingWidthLand;
	private int quickSettingHeightLand;
	private int quickSettingMarginTop;
	private int quickSettingMarginLeft;
	private int quickSettingMarginTopLand;
	private int quickSettingMarginLeftLand;

	private int recentHeight;
	private int recentWidthLand;


	private void initQuickSettingsSize(){
		quickSettingWidth = getResources().getDimensionPixelSize(R.dimen.quick_settings_width);
		quickSettingHeight = getResources().getDimensionPixelSize(R.dimen.quick_settings_height);
		quickSettingWidthLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_width_land);
		quickSettingHeightLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_height_land);
		quickSettingMarginTop = getResources().getDimensionPixelSize(R.dimen.quick_settings_margintop);
		quickSettingMarginLeft = getResources().getDimensionPixelSize(R.dimen.quick_settings_marginleft);
		quickSettingMarginTopLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_margintop_land);
		quickSettingMarginLeftLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_marginleft_land);
	}

	private void initView(){
		if(quickSettingsView == null || recentPanelView == null){
			quickSettingsView = findViewById(R.id.quick_setting);
			recentPanelView = findViewById(R.id.upbgview);
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	private void orientationPortraitLayout(){

		/*
		FrameLayout.LayoutParams quickParam = (FrameLayout.LayoutParams)quickSettingsView.getLayoutParams();
		quickParam.width = quickSettingWidth;
		quickParam.height = quickSettingHeight;
		quickParam.setMargins(quickSettingMarginLeft, quickSettingMarginTop, 0, 0);
		quickParam.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

		FrameLayout.LayoutParams recentParam = (FrameLayout.LayoutParams)recentPanelView.getLayoutParams();
		recentParam.width = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.height = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

		quickSettingsView.setLayoutParams(quickParam);
		recentPanelView.setLayoutParams(recentParam);
		*/
		quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start);
		quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end); 

	}

	private void orientationLandLayout(){
		/*
		FrameLayout.LayoutParams quickParam = (FrameLayout.LayoutParams)quickSettingsView.getLayoutParams();
		quickParam.width = quickSettingWidthLand;
		quickParam.height = quickSettingHeightLand;
		quickParam.setMargins(quickSettingMarginLeftLand, quickSettingMarginTopLand, 0, 0);
		quickParam.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;

		FrameLayout.LayoutParams recentParam = (FrameLayout.LayoutParams)recentPanelView.getLayoutParams();
		recentParam.width = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.height = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.gravity = Gravity.TOP | Gravity.RIGHT;

		quickSettingsView.setLayoutParams(quickParam);
		recentPanelView.setLayoutParams(recentParam);
		*/
		quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start_land);
		quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end_land);
	}
	// Aurora <Steve.Tang> 2015-01-19. supprt orientation land recent panel. end

    public int numItemsInOneScreenful() {
    	//M: tangjun mod begin
    	/*
        return mRecentsContainer.numItemsInOneScreenful();
        */
        if (mRecentsContainer instanceof RecentsScrollView){
            RecentsScrollView scrollView
                    = (RecentsScrollView) mRecentsContainer;
            return scrollView.numItemsInOneScreenful();
        }  else {
            throw new IllegalArgumentException("missing Recents[Horizontal]ScrollView");
        }
        //M: tangjun mod end
    }

    private boolean pointInside(int x, int y, View v) {
        final int l = v.getLeft();
        final int r = v.getRight();
        final int t = v.getTop();
        final int b = v.getBottom();
        return x >= l && x < r && y >= t && y < b;
    }

    public boolean isInContentArea(int x, int y) {
    	//M: tangjun mod begin
    	/*
        return pointInside(x, y, (View) mRecentsContainer);
        */
        if (pointInside(x, y, mRecentsContainer)
			|| pointInside(x, y, mRecentsNoApps)
			|| pointInside(x, y, mQuickSetting) 
												){
            return true;
        } else {
            return false;
        }
        //M: tangjun mod end
    }

    public void show(boolean show) {
        show(show, null, false, false);
    }

    public void show(boolean show, ArrayList<TaskDescription> recentTaskDescriptions,
            boolean firstScreenful, boolean animateIconOfFirstTask) {
        if (show && mCallUiHiddenBeforeNextReload) {
            onUiHidden();
            recentTaskDescriptions = null;
            mAnimateIconOfFirstTask = false;
            mWaitingForWindowAnimation = false;
        } else {
            mAnimateIconOfFirstTask = animateIconOfFirstTask;
            mWaitingForWindowAnimation = animateIconOfFirstTask;
        }
        if (show) {
        	//M: tangjun mod begin
        	updateResources();
            mWaitingToShow = true;
            //refreshRecentTasksList(recentTaskDescriptions, firstScreenful);
            showIfReady();
            //M: tangjun mod end
        } else {
            showImpl(false);
        }
    }

    private void showIfReady() {
        // mWaitingToShow => there was a touch up on the recents button
        // mRecentTaskDescriptions != null => we've created views for the first screenful of items
        if (mWaitingToShow && mRecentTaskDescriptions != null) {
            showImpl(true);
        }
    }

    static void sendCloseSystemWindows(Context context, String reason) {
        if (ActivityManagerNative.isSystemReady()) {
            try {
                ActivityManagerNative.getDefault().closeSystemDialogs(reason);
            } catch (RemoteException e) {
            }
        }
    }

    private void showImpl(boolean show) {
    	//M: tangjun mod begin
    	/*
        sendCloseSystemWindows(getContext(), BaseStatusBar.SYSTEM_DIALOG_REASON_RECENT_APPS);
        */

        mShowing = show;

        if (show) {
            // if there are no apps, bring up a "No recent apps" message
            boolean noApps = mRecentTaskDescriptions != null
                    && (mRecentTaskDescriptions.size() == 0);
            mRecentsNoApps.setAlpha(1f);
            /*
            mRecentsNoApps.setVisibility(noApps ? View.VISIBLE : View.INVISIBLE);
            */

            /*
            onAnimationEnd(null);
            */
            final TextView noRecentTextView = (TextView) mRecentsNoApps
                    .findViewById(R.id.no_app_label);
            noRecentTextView.setVisibility(noApps ? View.VISIBLE : View.INVISIBLE);
            
            setFocusable(true);
            setFocusableInTouchMode(true);
            requestFocus();
        } else {
            mWaitingToShow = false;
            // call onAnimationEnd() and clearRecentTasksList() in onUiHidden()
            mCallUiHiddenBeforeNextReload = true;
            if (mPopup != null) {
                mPopup.dismiss();
            }
        }
        //M: tangjun mod end
    }

    protected void onAttachedToWindow () {
        super.onAttachedToWindow();
        final ViewRootImpl root = getViewRootImpl();
        if (root != null) {
            root.setDrawDuringWindowsAnimating(true);
        }
    }

    public void onUiHidden() {
        mCallUiHiddenBeforeNextReload = false;
        //M: tangjun mod begin
        /*
        if (!mShowing && mRecentTaskDescriptions != null) {
            onAnimationEnd(null);
            clearRecentTasksList();
        }
        */
        
        Intent intent= new Intent(HandlerBar.ENABLE_HANDLER);
        mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));
        
        /// M: [ALPS00557795] Dismiss popup window on ui hidden condition.
        //Gionee: <guozj><2013-4-18> add for CR00793539 begin
        if (mPopup != null) {
            mPopup.dismiss();
        }
        //Gionee: <guozj><2013-4-18> add for CR00793539 end
        
        // Broadcast we are hidden, good time for Settings to update it`s data.
        mContext.sendBroadcastAsUser(new Intent(RECENTS_PANEL_HIDDEN),
            new UserHandle(UserHandle.USER_CURRENT));
        //M: tangjun mod end
    }

    public void dismiss() {
    	//M: tangjun mod begin
    	/*
        ((RecentsActivity) getContext()).dismissAndGoHome();
        */
    	removeRecentsPanelView();
    	//M: tangjun mod end
    }

    public void dismissAndGoBack() {
    	//M: tangjun mod begin
    	/*
        ((RecentsActivity) getContext()).dismissAndGoBack();
        */
    	removeRecentsPanelView();
    	//M: tangjun mod end
    }

    public void onAnimationCancel(Animator animation) {
    }

    public void onAnimationEnd(Animator animation) {
        if (mShowing) {
            final LayoutTransition transitioner = new LayoutTransition();
            ((ViewGroup)mRecentsContainer).setLayoutTransition(transitioner);
            createCustomAnimations(transitioner);
        } else {
            ((ViewGroup)mRecentsContainer).setLayoutTransition(null);
        }
    }

    public void onAnimationRepeat(Animator animation) {
    }

    public void onAnimationStart(Animator animation) {
    }

    @Override
    public boolean dispatchHoverEvent(MotionEvent event) {
        // Ignore hover events outside of this panel bounds since such events
        // generate spurious accessibility events with the panel content when
        // tapping outside of it, thus confusing the user.
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        if (x >= 0 && x < getWidth() && y >= 0 && y < getHeight()) {
            return super.dispatchHoverEvent(event);
        }
        return true;
    }

    /**
     * Whether the panel is showing, or, if it's animating, whether it will be
     * when the animation is done.
     */
    public boolean isShowing() {
        return mShowing;
    }

    public void setRecentTasksLoader(RecentTasksLoader loader) {
        mRecentTasksLoader = loader;
    }

    public void updateValuesFromResources() {
        final Resources res = getContext().getResources();
        mThumbnailWidth = Math.round(res.getDimension(R.dimen.status_bar_recents_thumbnail_width));
        mFitThumbnailToXY = res.getBoolean(R.bool.config_recents_thumbnail_image_fits_to_xy);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //M: tangjun mod begin
        mRecentsContainer = (ViewGroup) findViewById(R.id.recents_container);
        mListAdapter = new TaskDescriptionAdapter(getContext());
        if (mRecentsContainer instanceof RecentsScrollView){
            RecentsScrollView scrollView
                    = (RecentsScrollView) mRecentsContainer;
            scrollView.setOnScrollListener(new Runnable() {
                public void run() {
                    // need to redraw the faded edges
                    invalidate();
                }
            });
            scrollView.setAdapter(mListAdapter);
            scrollView.setCallback(this);
        } else {
            throw new IllegalArgumentException("missing Recents[Horizontal]ScrollView");
        }

        mRecentsScrim = findViewById(R.id.recents_bg_protect);
        mRecentsNoApps = findViewById(R.id.recents_no_apps);
        
        mNoAppLabel = (TextView) findViewById(R.id.no_app_label);
        mRecentsMaskView = findViewById(R.id.upbgview);

        if (mRecentsScrim != null) {
            mHighEndGfx = ActivityManager.isHighEndGfx();
            if (!mHighEndGfx) {
                mRecentsScrim.setBackground(null);
            } 
            /*
            else if (mRecentsScrim.getBackground() instanceof BitmapDrawable) {
                // In order to save space, we make the background texture repeat in the Y direction
                ((BitmapDrawable) mRecentsScrim.getBackground()).setTileModeY(TileMode.REPEAT);
            }
            */
        }
        
        //M: tangjun add for systemui begin
		mQuickSetting = findViewById(R.id.quick_setting);
		AuroraPageClear clearView = (AuroraPageClear) LayoutInflater.from(mContext).inflate(R.layout.aurora_recent_clearview, null);

		((AuroraRecentPage) mRecentsContainer)
            .addView(clearView, new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		//M: tangjun add for systemui end
		
		mQuickSettingPanel = (QSPanel)findViewById(R.id.quick_settings_panel);
		mQuickSettingPanel.setOnQuickSettingCallback(new QuickSettingCallback() {
			
			@Override
			public void finishQuickSetting() {
				// TODO Auto-generated method stub
				removeRecentsPanelView();
			}
		});
		
        //M: tangjun mod end
    }

    public void setMinSwipeAlpha(float minAlpha) {
    	/*
        mRecentsContainer.setMinSwipeAlpha(minAlpha);
        */
        if (mRecentsContainer instanceof RecentsScrollView){
            RecentsScrollView scrollView
                = (RecentsScrollView) mRecentsContainer;
            scrollView.setMinSwipeAlpha(minAlpha);
        }
    }

    private void createCustomAnimations(LayoutTransition transitioner) {
        transitioner.setDuration(200);
        transitioner.setStartDelay(LayoutTransition.CHANGE_DISAPPEARING, 0);
        transitioner.setAnimator(LayoutTransition.DISAPPEARING, null);
    }

    private void updateIcon(ViewHolder h, Drawable icon, boolean show, boolean anim) {
        if (icon != null) {
            h.iconView.setImageDrawable(icon);
            if (show && h.iconView.getVisibility() != View.VISIBLE) {
                if (anim) {
                    h.iconView.setAnimation(
                            AnimationUtils.loadAnimation(getContext(), R.anim.recent_appear));
                }
                h.iconView.setVisibility(View.VISIBLE);
            }
        }
    }

    //M: tangjun mod begin
    /*
    private void updateThumbnail(ViewHolder h, Drawable thumbnail, boolean show, boolean anim) {
        if (thumbnail != null) {
            // Should remove the default image in the frame
            // that this now covers, to improve scrolling speed.
            // That can't be done until the anim is complete though.
            h.thumbnailViewImage.setImageDrawable(thumbnail);

            // scale the image to fill the full width of the ImageView. do this only if
            // we haven't set a bitmap before, or if the bitmap size has changed
            if (h.thumbnailViewDrawable == null ||
                h.thumbnailViewDrawable.getIntrinsicWidth() != thumbnail.getIntrinsicWidth() ||
                h.thumbnailViewDrawable.getIntrinsicHeight() != thumbnail.getIntrinsicHeight()) {
                if (mFitThumbnailToXY) {
                    h.thumbnailViewImage.setScaleType(ScaleType.FIT_XY);
                } else {
                    Matrix scaleMatrix = new Matrix();
                    float scale = mThumbnailWidth / (float) thumbnail.getIntrinsicWidth();
                    scaleMatrix.setScale(scale, scale);
                    h.thumbnailViewImage.setScaleType(ScaleType.MATRIX);
                    h.thumbnailViewImage.setImageMatrix(scaleMatrix);
                }
            }
            if (show && h.thumbnailView.getVisibility() != View.VISIBLE) {
                if (anim) {
                    h.thumbnailView.setAnimation(
                            AnimationUtils.loadAnimation(getContext(), R.anim.recent_appear));
                }
                h.thumbnailView.setVisibility(View.VISIBLE);
            }
            h.thumbnailViewDrawable = thumbnail;
        }
    }

    void onTaskThumbnailLoaded(TaskDescription td) {
        synchronized (td) {
            if (mRecentsContainer != null) {
                ViewGroup container = (ViewGroup) mRecentsContainer;
                if (container instanceof RecentsScrollView) {
                    container = (ViewGroup) container.findViewById(
                            R.id.recents_linear_layout);
                }
                // Look for a view showing this thumbnail, to update.
                for (int i=0; i < container.getChildCount(); i++) {
                    View v = container.getChildAt(i);
                    if (v.getTag() instanceof ViewHolder) {
                        ViewHolder h = (ViewHolder)v.getTag();
                        if (!h.loadedThumbnailAndIcon && h.taskDescription == td) {
                            // only fade in the thumbnail if recents is already visible-- we
                            // show it immediately otherwise
                            //boolean animateShow = mShowing &&
                            //    mRecentsContainer.getAlpha() > ViewConfiguration.ALPHA_THRESHOLD;
                            boolean animateShow = false;
                            updateIcon(h, td.getIcon(), true, animateShow);
                            updateThumbnail(h, td.getThumbnail(), true, animateShow);
                            h.loadedThumbnailAndIcon = true;
                        }
                    }
                }
            }
        }
        showIfReady();
    }
    */
    //M: tangjun mod end

    private void animateInIconOfFirstTask() {
    	//M: tangjun mod begin
    	/*
        if (mItemToAnimateInWhenWindowAnimationIsFinished != null &&
                !mRecentTasksLoader.isFirstScreenful()) {
        */
    	if (mItemToAnimateInWhenWindowAnimationIsFinished != null) {
    	//M: tangjun mod end
            int timeSinceWindowAnimation =
                    (int) (System.currentTimeMillis() - mWindowAnimationStartTime);
            final int minStartDelay = 150;
            final int startDelay = Math.max(0, Math.min(
                    minStartDelay - timeSinceWindowAnimation, minStartDelay));
            final int duration = 250;
            final ViewHolder holder = mItemToAnimateInWhenWindowAnimationIsFinished;
            final TimeInterpolator cubic = new DecelerateInterpolator(1.5f);
            FirstFrameAnimatorHelper.initializeDrawListener(holder.iconView);
            for (View v :
            	//M: tangjun mod begin
            	new View[] { holder.iconView, holder.labelView }) {
                //new View[] { holder.iconView, holder.labelView, holder.calloutLine }) {
            	//M: tangjun mod end
                if (v != null) {
                    ViewPropertyAnimator vpa = v.animate().translationX(0).translationY(0)
                            .alpha(1f).setStartDelay(startDelay)
                            .setDuration(duration).setInterpolator(cubic);
                    FirstFrameAnimatorHelper h = new FirstFrameAnimatorHelper(vpa, v);
                }
            }
            mItemToAnimateInWhenWindowAnimationIsFinished = null;
            mAnimateIconOfFirstTask = false;
        }
    }

    public void onWindowAnimationStart() {
        mWaitingForWindowAnimation = false;
        mWindowAnimationStartTime = System.currentTimeMillis();
        animateInIconOfFirstTask();
    }

    public void clearRecentTasksList() {
        // Clear memory used by screenshots
        if (mRecentTaskDescriptions != null) {
            mRecentTasksLoader.cancelLoadingThumbnailsAndIcons(this);
            onTaskLoadingCancelled();
        }
    }

    public void onTaskLoadingCancelled() {
        // Gets called by RecentTasksLoader when it's cancelled
        if (mRecentTaskDescriptions != null) {
            mRecentTaskDescriptions = null;
            mListAdapter.notifyDataSetInvalidated();
        }
    }
    
    //M: tangjun add begin
    public void refreshViewIcons(){
    	Log.v("iht-ssui","+++++++++++++++++++++++++++++++++++++++++icons_update");
    	mListAdapter.notifyDataSetInvalidated();
    }
    //M: tangjun add end

    public void refreshViews() {
        mListAdapter.notifyDataSetInvalidated();
        updateUiElements();
        showIfReady();
    }
    
    /// M: [SystemUI][ALPS00444338]When a call come in, will clear recent tasks, but when resume, doesn't reload tasks. @{
    public void refreshRecentTasks() {
        if (mRecentTaskDescriptions == null) {
            refreshRecentTasksList();
        }
    }
    /// M: [SystemUI][ALPS00444338]When a call come in, will clear recent tasks, but when resume, doesn't reload tasks. @}

    public void refreshRecentTasksList() {
        refreshRecentTasksList(null, false);
    }

    private void refreshRecentTasksList(
            ArrayList<TaskDescription> recentTasksList, boolean firstScreenful) {
    	//M: tangjun mod begin
        //if (mRecentTaskDescriptions == null && recentTasksList != null) {
    	if (mRecentTaskDescriptions != null) {
    	//M: tangjun mod end
            onTasksLoaded(recentTasksList, firstScreenful);
        } else {
            mRecentTasksLoader.loadTasksInBackground();
        }
    }

    public void onTasksLoaded(ArrayList<TaskDescription> tasks, boolean firstScreenful) {
        if (mRecentTaskDescriptions == null) {
            mRecentTaskDescriptions = new ArrayList<TaskDescription>(tasks);
        } else {
        	//M: tangjun empty mRecentTaskDescriptions first begin
        	mRecentTaskDescriptions.clear();
        	//M: tangjun empty mRecentTaskDescriptions first end
            mRecentTaskDescriptions.addAll(tasks);
        }
        //M: tangjun mod begin
        if (mRecentTaskDescriptions.size()!=0) {
        	for(TaskDescription task: mRecentTaskDescriptions) {
                Log.d("felix", "RecentsPanelView.DEBUG  mRecentTaskDescriptions.packageName = " + task.packageName);
            }
        }
        /*
        if (((RecentsActivity) getContext()).isActivityShowing()) {
            refreshViews();
        }
        */
        refreshViews();
        //M: tangjun mod end
    }

    private void updateUiElements() {
        final int items = mRecentTaskDescriptions != null
                ? mRecentTaskDescriptions.size() : 0;

        ((View) mRecentsContainer).setVisibility(items > 0 ? View.VISIBLE : View.GONE);

        // Set description for accessibility
        int numRecentApps = mRecentTaskDescriptions != null
                ? mRecentTaskDescriptions.size() : 0;
        String recentAppsAccessibilityDescription;
        if (numRecentApps == 0) {
            recentAppsAccessibilityDescription =
                getResources().getString(R.string.status_bar_no_recent_apps);
        } else {
            recentAppsAccessibilityDescription = getResources().getQuantityString(
                R.plurals.status_bar_accessibility_recent_apps, numRecentApps, numRecentApps);
        }
        setContentDescription(recentAppsAccessibilityDescription);
    }

    public boolean simulateClick(int persistentTaskId) {
        if (mRecentsContainer instanceof RecentsScrollView){
            RecentsScrollView scrollView
                = (RecentsScrollView) mRecentsContainer;
            View v = scrollView.findViewForTask(persistentTaskId);
            if (v != null) {
                handleOnClick(v);
                return true;
            }
        }
        return false;
    }
    
    
    
    @Override
	public void handleOnClick(View view, boolean enterScreenPin) {
    	ViewHolder holder = (ViewHolder) view.getTag();
        TaskDescription ad = holder.taskDescription;
        final Context context = view.getContext();
        final ActivityManager am = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);

        //M: tangjun mod begin
        /*
        Bitmap bm = null;
        boolean usingDrawingCache = true;
        if (holder.thumbnailViewDrawable instanceof BitmapDrawable) {
            bm = ((BitmapDrawable) holder.thumbnailViewDrawable).getBitmap();
            if (bm.getWidth() == holder.thumbnailViewImage.getWidth() &&
                    bm.getHeight() == holder.thumbnailViewImage.getHeight()) {
                usingDrawingCache = false;
            }
        }
        if (usingDrawingCache) {
            holder.thumbnailViewImage.setDrawingCacheEnabled(true);
            bm = holder.thumbnailViewImage.getDrawingCache();
        }
        Bundle opts = (bm == null) ?
                null :
                ActivityOptions.makeThumbnailScaleUpAnimation(
                        holder.thumbnailViewImage, bm, 0, 0, null).toBundle();
        */
        
        Bundle opts = null;
        //M: tangjun mod end

        show(false);
        if (ad.taskId >= 0) {
            // This is an active task; it should just go to the foreground.
            am.moveTaskToFront(ad.taskId, ActivityManager.MOVE_TASK_WITH_HOME,
                    opts);
            //M: tangjun mod begin
            removeRecentsPanelView();
            //M: tangjun mod end
        } else {
            Intent intent = ad.intent;
            intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY
                    | Intent.FLAG_ACTIVITY_TASK_ON_HOME
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            if (DEBUG) Log.v(TAG, "Starting activity " + intent);
            try {
                context.startActivityAsUser(intent, opts,
                        new UserHandle(ad.userId));
            } catch (SecurityException e) {
                Log.e(TAG, "Recents does not have the permission to launch " + intent, e);
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "Error launching activity " + intent, e);
            }
            removeRecentsPanelView();
        }
        
        if(enterScreenPin&&isLockApp()){
        	try {
                ActivityManagerNative.getDefault().startLockTaskModeOnCurrent();
            } catch (RemoteException e) {}
        }
        /*
        if (usingDrawingCache) {
            holder.thumbnailViewImage.setDrawingCacheEnabled(false);
        }
        */
	}

    public void handleOnClick(View view) {
    	handleOnClick(view,false);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        handleOnClick(view);
    }

    public void handleSwipe(View view) {
        final TaskDescription ad = ((ViewHolder) view.getTag()).taskDescription;
        if (ad == null) {
            Log.v(TAG, "Not able to find activity description for swiped task; view=" + view +
                    " tag=" + view.getTag());
            return;
        }
        
        //M: tangjun Add animation of swipe up icon on recents panel begin
        // Clone a item view to perform deletion animation
        if (view instanceof AuroraRecentlItemView
            && ((AuroraRecentlItemView)view).isSingleSwipe()) {
            AuroraRecentlItemView clone = ((AuroraRecentlItemView)view).clone();
            addView(clone, new LayoutParams(view.getWidth(), view.getHeight()));
            clone.buildAnim().start();
        }
        //M: tangjun Add animation of swipe up icon on recents panel end
        
        if (DEBUG) Log.v(TAG, "Jettison " + ad.getLabel());
        if(mRecentTaskDescriptions == null){
        	return;
        }
        mRecentTaskDescriptions.remove(ad);
        mRecentTasksLoader.remove(ad);

        // Handled by widget containers to enable LayoutTransitions properly
        // mListAdapter.notifyDataSetChanged();

        if (mRecentTaskDescriptions.size() == 0) {
            dismissAndGoBack();
        }

        // Currently, either direction means the same thing, so ignore direction and remove
        // the task.
        final ActivityManager am = (ActivityManager)
                getContext().getSystemService(Context.ACTIVITY_SERVICE);
        if (am != null) {
        	sWorker.post(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
		        	am.removeTask(ad.persistentTaskId);

		            //M: tangjun mod begin
		            if (mRemoveTaskPolicy.shouldForceStopPackage(ad)) {
		            	am.forceStopPackage(ad.packageName);
		            }
		            //M: tangjun mod end
				}
			});
            
            // Accessibility feedback
            setContentDescription(
                    getContext().getString(R.string.accessibility_recents_item_dismissed, ad.getLabel()));
            sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_SELECTED);
            setContentDescription(null);
        }
    }

    private void startApplicationDetailsActivity(String packageName, int userId) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", packageName, null));
        intent.setComponent(intent.resolveActivity(getContext().getPackageManager()));
        TaskStackBuilder.create(getContext())
                .addNextIntentWithParentStack(intent).startActivities(null, new UserHandle(userId));
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mPopup != null) {
            return true;
        } else {
            return super.onInterceptTouchEvent(ev);
        }
    }

    public void handleLongPress(
            final View selectedView, final View anchorView, final View thumbnailView) {
        thumbnailView.setSelected(true);
        final PopupMenu popup =
            new PopupMenu(getContext(), anchorView == null ? selectedView : anchorView);
        mPopup = popup;
        popup.getMenuInflater().inflate(R.menu.recent_popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.recent_remove_item) {
                    ((ViewGroup) mRecentsContainer).removeViewInLayout(selectedView);
                } else if (item.getItemId() == R.id.recent_inspect_item) {
                    ViewHolder viewHolder = (ViewHolder) selectedView.getTag();
                    if (viewHolder != null) {
                        final TaskDescription ad = viewHolder.taskDescription;
                        startApplicationDetailsActivity(ad.packageName, ad.userId);
                        show(false);
                    } else {
                        throw new IllegalStateException("Oops, no tag on view " + selectedView);
                    }
                } else {
                    return false;
                }
                return true;
            }
        });
        popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
            public void onDismiss(PopupMenu menu) {
                thumbnailView.setSelected(false);
                mPopup = null;
            }
        });
        popup.show();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        int paddingLeft = getPaddingLeft();
        final boolean offsetRequired = isPaddingOffsetRequired();
        if (offsetRequired) {
            paddingLeft += getLeftPaddingOffset();
        }

        int left = getScrollX() + paddingLeft;
        int right = left + getRight() - getLeft() - getPaddingRight() - paddingLeft;
        int top = getScrollY() + getFadeTop(offsetRequired);
        int bottom = top + getFadeHeight(offsetRequired);

        if (offsetRequired) {
            right += getRightPaddingOffset();
            bottom += getBottomPaddingOffset();
        }
        if (mRecentsContainer instanceof RecentsScrollView){
            RecentsScrollView scrollView
                = (RecentsScrollView) mRecentsContainer;
            scrollView.drawFadedEdges(canvas, left, right, top, bottom);
        }
    }
    
    //M: tangjun mod begin
	public boolean isRecentActivityShowing(){
		if(null == mContext){
			return false;
		}
		return isShowing();
	}
	
	private Handler handler = new Handler();
	
    @Override
	public boolean onTouchEvent(MotionEvent event) {
    	if(inRangeOfView(mQuickSetting,event)){
    		return true;
    	}
    	
    	if (event.getAction() ==  MotionEvent.ACTION_UP){
    		
    	   removeRecentsPanelView();
    	}
		return true;
	}
    
    private boolean inRangeOfView(View view, MotionEvent ev){
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        if(ev.getX() < x || ev.getX() > (x + view.getWidth()) || ev.getY() < y || ev.getY() > (y + view.getHeight())){
            return false;
        }
        return true;
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
    	switch (event.getKeyCode()) {
    	case KeyEvent.KEYCODE_BACK:
    		if (event.getAction() == KeyEvent.ACTION_UP) {
    			// Cause pull up from BACK key fail
    			if (!event.isCanceled()) {
    				removeRecentsPanelView();
    			}
    		}
    		break;

    	}
    	return super.dispatchKeyEvent(event);
    }

    public void removeRecentsPanelView(){
    	if (mEnterAnimSet.isRunning())
    		mEnterAnimSet.cancel();

    	if (mAnimating) return;

    	if(isScreenOff){
    		scrollToOriginalLocationNoAnimation();
    		setRecentsMaskViewAlpha();
    		((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
    		mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
    		if(mAuroraRubbishView == null){
    			mAuroraRubbishView = (AuroraRubbishView)findViewById(R.id.clear_all);
    		}
    		mAuroraRubbishView.rest();
    		mHandlerBarCallback.removeRecentPanelView();
    		isScreenOff = false;
    	}else{
    		scrollToOriginalLocation();
    		autoQuickSettingQuitAnimation();
    	}
    	show(false);
    	onUiHidden();
    	
    	updateUiElements();
     }
    
    public void removeRecentsPanelView(boolean isShowHandlerBar){
    	if (mEnterAnimSet.isRunning())
    		mEnterAnimSet.cancel();

    	if (mAnimating) return;

    	if(isScreenOff){
    		scrollToOriginalLocationNoAnimation();
    		setRecentsMaskViewAlpha();
    		((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
    		mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
    		if(mAuroraRubbishView == null){
    			mAuroraRubbishView = (AuroraRubbishView)findViewById(R.id.clear_all);
    		}
    		mAuroraRubbishView.rest();
    		mHandlerBarCallback.removeRecentPanelView(false);
    		isScreenOff = false;
    	}else{
    		scrollToOriginalLocation();
    		autoQuickSettingQuitAnimation(isShowHandlerBar);
    	}
    	show(false);
    	onUiHidden();
    }
    
 	public void quickRecentPanelZMove(int scrollZ) {
        if (((AuroraRecentPage) mRecentsContainer).isScrollable()) {
            ((AuroraRecentPage) mRecentsContainer).setScrollable(false);
            handler.removeCallbacks(mDisableHandlerRunnable);
        }

 		mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(blurredBackgroundAlphaChanged(Math.min(scrollZ, 255)));
 		setMaskAlpha(backgroundAlphaChanged(scrollZ));
        if (isOrientationPortrait()) 		
            mQuickSetting.setTranslationY(-Math.max(-scrollZ, -(quickSettingAnimEnd)));
        else
            mQuickSetting.setTranslationX(-Math.max(-scrollZ, -(quickSettingAnimEnd)));
	    mQuickSetting.setAlpha(distanChangeToAlpahZeroToOne(scrollZ));
	    
	    Log.e("222222", "-----quickRecentPanelZMove mQuickSetting.getTranslationY()-------" + mQuickSetting.getTranslationY());
 	}
 	
 	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            
            //iht 更新图标
            if(action.equals(NOTIFY_APPS_TO_UPDATEIICON)){
            	mHandlerBarCallback.refreshViewIcons();
            }
            
        	if (Intent.ACTION_SCREEN_OFF.equals(action)) {
        		if(isShown()){
        			isScreenOff = true;
        			//M: tangjun mod to fix bug 17197 11.12 begin
        			mListening = false;
        			mQuickSettingPanel.setListening(mListening);
        			//M: tangjun mod to fix bug 17197 11.12 end
        			removeRecentsPanelView();
        		}
            }
        	else if(Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(action)){
                boolean shown = isShown();
                if(shown && !mAnimating) {
            		removeRecentsPanelView();
            	}
            	isResponseHomeKey = false;
            }else if(Intent.ACTION_CONFIGURATION_CHANGED.equals(action)){
            	int orientation = getScreenState();
				if(last_orientation != orientation){
					dispatchConfigurationChanged(mContext.getResources().getConfiguration());
				}

                mHandlerBarCallback.updateOrientation(orientation);
                if (InvokerService.getInstance() != null) // Avoid NullPointer Exception when not using InvokerService
                    InvokerService.getInstance().mLandscape = (orientation == Configuration.ORIENTATION_LANDSCAPE);
            }
            else if("com.aurora.deskclock.startalarm".equals(action)){

            	if(isShown()){
            		removeRecentsPanelView(false);
            	}else{
            		mHandlerBarCallback.showHandlerBarView(false);
            	}
            }else if("com.aurora.deskclock.stopalarm".equals(action)){
            	mHandlerBarCallback.showHandlerBarView(true);

            } else if (VOICEWAKEUP_UNLOCK.equals(action)) {
                removeRecentsPanelView();
            } else if ("hhh.hhh.hhh.hhh".equals(action)){
				mQuickSetting.setTranslationY(0);		
				mQuickSetting.setTranslationX(0);
			}
        }
 	};
 	
 	private BroadcastReceiver mPackageMgrReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            handler.removeCallbacks(mLoadTaskRunnable);
            handler.postDelayed(mLoadTaskRunnable, 100);
        }
    };
    
    public void setRecentsMaskViewAlpha(){
	    setMaskAlpha(0);
    }
    
    public void setRecentsMaskViewAlphaFull(){
    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);
    }
    
    public void setRecentsQuickSettingViewAlpha(){
    	mQuickSetting.setAlpha(0);
    }
    
    public float distanChangeToAlpahZeroToOne(float frac){
        final int fullAlpha = 1;
        final int H = HandlerBar.mRecentsScrimHeight;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return alpha;
    }
    
    public float distanChangeToAlpahOneToZero(float frac) {
        final int fullAlpha = 1;
        final int H = HandlerBar.mRecentsScrimHeight;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha - (fullAlpha * frac / H);
        if(alpha < 0){
        	alpha = 0;
        }
        return alpha;
    }
    
    public int backgroundAlphaChanged(float frac) {
        final int H = HandlerBar.mRecentsScrimHeight-60;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = MASK_VIEW_FULL_ALPHA * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return (int)alpha;
    }
    
    public int blurredBackgroundAlphaChanged(float frac) {
        final int fullAlpha = 255;
        final int H = HandlerBar.mRecentsScrimHeight-60;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return (int)alpha;
    }
    
    public void autoQuickSettingEnterAnimation(){
    	
    	Log.e("222222", "-----autoQuickSettingEnterAnimation mListening-------" + mListening);
    	if (!mListening) {
    		mListening = true;
    		mQuickSettingPanel.setListening(mListening);
    	}

    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
			animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimStart,quickSettingAnimEnd);
		} else {
	    	animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimStart,quickSettingAnimEnd);
		}

    	animQuickSettingTranslation.setDuration(HandlerBar.AUTO_ENTER_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){

            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", 0f,1f);
    	animQuickSettingAlpha.setDuration(HandlerBar.AUTO_ENTER_ANIMATION_DURATION);

    	mEnterAnimSet = new AnimatorSet();

        mEnterAnimSet.addListener(new AnimatorListenerAdapter() {
            boolean canceled;
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
                canceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                canceled = true;
                mAnimating = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ((AuroraRecentPage) mRecentsContainer).setScrollable(true);
                if (canceled) return;
                handler.postDelayed(mDisableHandlerRunnable, 0);
                mAnimating = false;
            }
        });

    	mEnterAnimSet.play(animQuickSettingTranslation);
    	mEnterAnimSet.play(animQuickSettingAlpha);
    	mEnterAnimSet.start();
    }
    
    public void continueAutoQuickSettingEnterAnimation(){

    	Log.e("222222", "-----continueAutoQuickSettingEnterAnimation mListening-------" + mListening);
    	if (!mListening) {
    		mListening = true;
    		mQuickSettingPanel.setListening(mListening);
    	}
    	
    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);

    	int translateZ = (int) ((isOrientationPortrait())
            ? mQuickSetting.getTranslationY()
            : mQuickSetting.getTranslationX());
    	Log.e("222222", "-----continueAutoQuickSettingEnterAnimation translateZ-------$" + translateZ);
    	if(translateZ <= quickSettingAnimStart){
    		translateZ = quickSettingAnimStart;
    	}else if(translateZ >= quickSettingAnimEnd){
    		translateZ = quickSettingAnimEnd;
    	}

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }

		String orientation = "translationY";
		if(!isOrientationPortrait()){
			orientation = "translationX";
		}

    	animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, orientation, translateZ,quickSettingAnimEnd);

    	animQuickSettingTranslation.setDuration(HandlerBar.ANIMATION_CONTINUE_ENTER_DURATION);


        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){

            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),1f);

    	animQuickSettingAlpha.setDuration(HandlerBar.ANIMATION_CONTINUE_ENTER_DURATION);

    	mEnterAnimSet = new AnimatorSet();
    	mEnterAnimSet.play(animQuickSettingTranslation);
    	mEnterAnimSet.play(animQuickSettingAlpha);
        mEnterAnimSet.addListener(new AnimatorListenerAdapter() {
            boolean canceled;
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
                canceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                canceled = true;
                mAnimating = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                ((AuroraRecentPage) mRecentsContainer).setScrollable(true);
                if (canceled) return;
                handler.postDelayed(mDisableHandlerRunnable, 0);
                mAnimating = false;
            }
        });
    	mEnterAnimSet.start();
    }
    
    public void autoQuickSettingQuitAnimation(){
    	
    	Log.e("222222", "-----autoQuickSettingQuitAnimation mListening-------" + mListening);
    	if (mListening) {
    		mListening = false;
    		mQuickSettingPanel.setListening(mListening);
    	}

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimEnd,quickSettingAnimStart);
		} else {
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimEnd,quickSettingAnimStart);
		}

    	animQuickSettingTranslation.setDuration(QUIT_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){

            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),0f);

    	animQuickSettingAlpha.setDuration(QUIT_ANIMATION_DURATION);

        mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
        if (bgAlpha != null && bgAlpha.isRunning()) {
            bgAlpha.cancel();
        }
    	bgAlpha = ObjectAnimator.ofInt(mRecentsMaskView.getBackground(), "alpha", sMaskAlpha,0);
    	bgAlpha.setDuration(QUIT_ANIMATION_DURATION);

    	animQuickSettingTranslation.addListener(new AnimatorListenerAdapter(){

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
            }

			@Override
			public void onAnimationCancel(Animator arg0) {
                mAnimating = false;
				setRecentsMaskViewAlpha();
				mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
				((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
				
			}

			@Override
			public void onAnimationEnd(Animator arg0) {

                mAnimating = false;
				setRecentsMaskViewAlpha();
				((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
				if(mAuroraRubbishView == null){
					mAuroraRubbishView = (AuroraRubbishView)findViewById(R.id.clear_all);
				}
				mAuroraRubbishView.rest();
				mHandlerBarCallback.removeRecentPanelView();
                ((AuroraRecentPage) mRecentsContainer).setScrollable(true);

			}
			
		}
    	);
    	AnimatorSet animSet = new AnimatorSet();
    	animSet.play(animQuickSettingAlpha);
    	animSet.play(animQuickSettingTranslation);
    	animSet.play(bgAlpha);
    	animSet.start();
    }
    
    public void autoQuickSettingQuitAnimation(final boolean isShowHandlerBar){

    	Log.e("222222", "-----autoQuickSettingQuitAnimation  isShowHandlerBar mListening = -------" + mListening);
    	if (mListening) {
    		mListening = false;
    		mQuickSettingPanel.setListening(mListening);
    	}
    	
        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimEnd,quickSettingAnimStart);
		} else {
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimEnd,quickSettingAnimStart);
		}

    	animQuickSettingTranslation.setDuration(QUIT_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){

            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),0f);

    	animQuickSettingAlpha.setDuration(QUIT_ANIMATION_DURATION);

        mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);

        if (bgAlpha != null && bgAlpha.isRunning()) {

            bgAlpha.cancel();
        }
        bgAlpha = ObjectAnimator.ofInt(mRecentsMaskView.getBackground(), "alpha",sMaskAlpha,0);
        bgAlpha.setDuration(QUIT_ANIMATION_DURATION);

    	animQuickSettingTranslation.addListener(new AnimatorListenerAdapter(){

			@Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
            }

            @Override
			public void onAnimationCancel(Animator arg0) {

                mAnimating = false;
				setRecentsMaskViewAlpha();
				mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
				((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
				
			}

			@Override
			public void onAnimationEnd(Animator arg0) {
                mAnimating = false;
				setRecentsMaskViewAlpha();
				((AuroraRecentPage) mRecentsContainer).setmNeedToClean(false);
				if(mAuroraRubbishView == null){
					mAuroraRubbishView = (AuroraRubbishView)findViewById(R.id.clear_all);
				}
				mAuroraRubbishView.rest();
				mHandlerBarCallback.removeRecentPanelView(isShowHandlerBar);
				((AuroraRecentPage) mRecentsContainer).setScrollable(true);
			}
			
		}
    	);
    	AnimatorSet animSet = new AnimatorSet();
    	animSet.play(animQuickSettingAlpha);
    	animSet.play(animQuickSettingTranslation);
        animSet.play(bgAlpha);
    	animSet.start();
    }
    
 	public void scrollToOriginalLocation(){
 		((RecentsLayout)mRecentsMaskView).startScroll(0, mRecentsMaskView.getScrollY(), 0,
            -mRecentsMaskView.getScrollY(), QUIT_ANIMATION_DURATION * 2);
 	}
 	
 	public View getMoveView(){
 		return mRecentsMaskView;
 	}
 	
 	public View getQuickSettingView(){
 		return mQuickSetting;
 	}
 	
 	public void autoQuickSettingEnterAnimationForZ(int scrollZ){
 		Log.e("222222", "-----autoQuickSettingEnterAnimationForZ scrollZ = -------$" + scrollZ);
 		Log.e("222222", "-----autoQuickSettingEnterAnimationForZ HandlerBar.mRecentsScrimHeight = -------$" + HandlerBar.mRecentsScrimHeight);
 		if(scrollZ>HandlerBar.mRecentsScrimHeight){
 			return;
 		}

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }
        if (isOrientationPortrait()) {
            animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY",
                    mQuickSetting.getTranslationY() < quickSettingAnimStart ? quickSettingAnimStart : mQuickSetting.getTranslationY(),
                    quickSettingAnimEnd);
        } else {
            animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX",
                    mQuickSetting.getTranslationX() < quickSettingAnimStart ? quickSettingAnimStart : mQuickSetting.getTranslationX(),
                    quickSettingAnimEnd);
        }

    	animQuickSettingTranslation.setDuration(500);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){

            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),1f);

    	animQuickSettingAlpha.setDuration(500);
    	AnimatorSet animSet = new AnimatorSet();
    	
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ((AuroraRecentPage) mRecentsContainer).setScrollable(true);
            }
        });
    	animSet.play(animQuickSettingTranslation);
    	animSet.play(animQuickSettingAlpha);
    	animSet.start();
    }
 	
 	public void closeHardAccelerate(){
 		mHandlerBarCallback.getRecentsPanelView().setLayerType(View.LAYER_TYPE_NONE,null);
    	mRecentsMaskView.setLayerType(View.LAYER_TYPE_NONE,null);
    	mQuickSetting.setLayerType(View.LAYER_TYPE_NONE,null);
    	mRecentsScrim.setLayerType(View.LAYER_TYPE_NONE,null);
    }
 	
    public void openHardAccelerate(){
    	mHandlerBarCallback.getRecentsPanelView().setLayerType(View.LAYER_TYPE_HARDWARE,null);
    	mRecentsMaskView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
    	mQuickSetting.setLayerType(View.LAYER_TYPE_HARDWARE,null);
    	mRecentsScrim.setLayerType(View.LAYER_TYPE_HARDWARE,null);
    }
    
    public void scrollToOriginalLocationNoAnimation(){
    	mRecentsMaskView.scrollTo(0, 0);
    }
    
    /**
     * Set RecentsMaskView`s background alpha and remember it.
     * Because Android doesn`t support Drawable.getAlpha() untill api-level 19
     * @param alpha to set
     * 
     * @author Felix.Duan
     * @date 2014-5-8
     */
    private void setMaskAlpha(int alpha) {
        mRecentsMaskView.getBackground().setAlpha(alpha);
        sMaskAlpha = alpha;
    }
    
    private Runnable mDisableHandlerRunnable = new Runnable() {
        @Override
        public void run() {
            if (isShown()) {
            	Log.e("666666", "---mDisableHandlerRunnable-----");
                Intent intent = new Intent(HandlerBar.DISABLE_HANDLER);
                mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));
            }
        }
    };
    
    private Runnable mLoadTaskRunnable = new Runnable() {
        @Override
        public void run() {
            mRecentTasksLoader.checkTaskSample();
        }
    };
    
    public void scheduleLoadTask() {
        Log.d(TAG, "---RecentsPanelView scheduleLoadTask()----");
        handler.removeCallbacks(mLoadTaskRunnable);
        handler.postDelayed(mLoadTaskRunnable, 100);
    }
    
    private Runnable mUpdateImeStateRunnable = new Runnable() {
        @Override
        public void run() {
            if (isShowInputMethod) {
            	if(isShown()){
            		removeRecentsPanelView(false);
            	}else{
            		mHandlerBarCallback.showHandlerBarView(false);
            	}
                if (InvokerService.getInstance() != null) // Avoid NullPointer Exception when not using InvokerService
                    InvokerService.getInstance().updateInputMethodStatus(isShowInputMethod);
            } else {
                mHandlerBarCallback.showHandlerBarView(true);
                isShowInputMethod = false;
                if (InvokerService.getInstance() != null) // Avoid NullPointer Exception when not using InvokerService
                    InvokerService.getInstance().updateInputMethodStatus(isShowInputMethod);
            }
        }
    };
    
    public void updateImeState(boolean showing) {
        Log.d(TAG, "updateImeState() showing = " + showing);
        isShowInputMethod = showing;
        handler.removeCallbacks(mUpdateImeStateRunnable);
        handler.postDelayed(mUpdateImeStateRunnable, 100);
    }
    
    public void updateResources() {
        mNoAppLabel.setText(R.string.recent_no_task);
    }
    //M: tangjun mod end
    
    //M: liuzuo fix bug  20860 begin
    private boolean isLockApp(){
    	if(Settings.System.getInt(mContext.getContentResolver(),
    	        Settings.System.LOCK_TO_APP_ENABLED, 0)==0){
    		return false;
    	}else {
			return true;
		}

    }
    //M: liuzuo fix bug  20860 end 
}

package com.android.systemui.statusbar.aurora;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.LruCache;

public class LruMemoryCache {
	private static LruMemoryCache instance;

	public static LruMemoryCache getInstance() {
		if (instance == null) {
			instance = new LruMemoryCache();
		}
		return instance;
	}
	
	public LruMemoryCache() {
		super();
	}
	
	private final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
	private final int cacheSize = maxMemory / 8;
	private LruCache<String, Drawable> mMemoryCache = new LruCache<String, Drawable>(
			cacheSize) {
		@Override
		protected int sizeOf(String key, Drawable drawable) {
			Log.i("Gicons","LruMemoryCache size = "+mMemoryCache.size());
			if(drawable instanceof BitmapDrawable){
				Bitmap b = ((BitmapDrawable)drawable).getBitmap();
				if(b!=null){
					return b.getByteCount()/1024;
				}
			}
			return 0;
		}
	};
	
	public void addToCache(String key,Drawable value){
		mMemoryCache.put(key, value);
	}
	
	public Drawable getDrawable(String key){
		return mMemoryCache.get(key);
	}
	
	public void clear() {
		 mMemoryCache.evictAll();
	}

}

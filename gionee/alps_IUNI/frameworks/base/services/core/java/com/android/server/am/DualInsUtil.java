/*
* Copyright (C) 2014 Gionee Inc.
* Module: dual_weixin
* Author:  liuran
 */

package com.android.server.am;

import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;

public class DualInsUtil {
  	public static boolean sameRace(ProcessRecord proc, TaskRecord tr){
        return proc.isClone == tr.isClone;
  	}
  	
  	public static boolean sameRace(TaskRecord tr, ActivityRecord ar){
        return tr.isClone == ar.isClone;            
  	}
  	
  	public static boolean sameRace(ActivityRecord a, ActivityRecord b){
        return a.isClone == b.isClone;            
  	}

  	public static boolean sameRace(ActivityRecord ar,ActivityInfo ati){
        return  ar.isClone == ati.applicationInfo.isClone;            
  	}
  	
  	public static boolean sameRace(ServiceRecord src, ServiceRecord dst){
        return src.isClone == dst.isClone;            
  	}
  	
  	public static boolean sameRace(ProcessRecord proc, ServiceRecord sr){
        return proc.isClone == sr.isClone;            
  	}
  	
  	public static boolean sameRace(TaskRecord src, TaskRecord dst){
        return dst.isClone == src.isClone;            
  	}
  
  	public static ApplicationInfo getProcAppInfo(ProcessRecord callerApp){
  		if(callerApp != null){
  			return callerApp.info;
  		}
  		return null;
  	}
}

//Gionee <Amigo_Server> wangym 20141225 add for AmigoServer CR01431164 begin
package com.android.server;

/** 
 *File Description: This is implementation of AmigoServerManagerService
 *Author: wangym
 *Create Date:20141226
 *Change List:
 */
 
import android.util.Log;

import android.content.pm.PackageManager;
import android.content.Context;
import java.lang.RuntimeException;
import android.os.amigoserver.IAmigoServerManager;
import android.os.amigoserver.AmigoServerManager;
//import amigo.provider.AmigoSettings;
public class AmigoServerManagerService extends IAmigoServerManager.Stub{
    private static final String TAG = "AmigoServerManagerService";
    private static native void nativeSetNodeState(int NodeType,int mValue);
    private static native int nativeGetNodeState(int NodeType);
    private static native String nativeGetNodeContent(int NodeType);
    
    private final Context mContext;
    private final Object mHalLock = new Object();
    private static int  mSettingOn = 1;
    private static int mSettingOff = 0;
    public AmigoServerManagerService(Context context){
        mContext = context;
    }
    public void onSystemReady() {
         Log.d(TAG, "onSystemReady"  );
         initNodeState(mContext);     
    }
    private void initNodeState(Context context)
    {
        /*  
        boolean mSSGSwitch = AmigoSettings.getInt(context.getContentResolver(), AmigoSettings.GN_SSG_SWITCH, mSettingOff) != mSettingOff;
        boolean mSSGQuickOpera = AmigoSettings.getInt(context.getContentResolver(), AmigoSettings.SSG_QUICK_OPERATING, mSettingOff) != mSettingOff;
        boolean mDoubleClick =  AmigoSettings.getInt(context.getContentResolver(),AmigoSettings.SSG_DOUBLECLICK_WAKE, mSettingOff) != mSettingOff;
        boolean mGloveMode =   AmigoSettings.getInt(context.getContentResolver(),AmigoSettings.GLOVE_PATTERNS, mSettingOff) != mSettingOff;
        boolean mLcmAcl =   AmigoSettings.getInt(context.getContentResolver(),AmigoSettings.AUTO_LCM_ACL, mSettingOff) != mSettingOff;
        //Gionee <Amigo_Skylight> wangym <20150409> add for  CR01462639 begin
        boolean mSkyLightEnable =   AmigoSettings.getInt(context.getContentResolver(),"skylight_switch", mSettingOn) != mSettingOff;
        //Gionee <Amigo_Skylight> wangym <20150409> add for  CR01462639 end
        if(mSSGSwitch)
        {
            if(mDoubleClick)
            {
                setNodeState(AmigoServerManager.NODE_TYPE_TPWAKESWITCH_DOUBLE_WAKE,mSettingOn);
            }
            if(mSSGQuickOpera)
            {
                setNodeState(AmigoServerManager.NODE_TYPE_TPWAKESWITCH_GESTURE_WAKE,mSettingOn);
                int mSSGQuickOperaConfig = mSettingOff;
                mSSGQuickOperaConfig = AmigoSettings.getInt(context.getContentResolver(),"black_gesture_config_value", mSettingOff);
                if(mSSGQuickOperaConfig > mSettingOff)
                {
                    setNodeState(AmigoServerManager.NODE_TYPE_TPWAKESWITCH_GESTURE_CONFIG,mSSGQuickOperaConfig);
                }
            }            
        }
        if(mGloveMode)
        {
            setNodeState(AmigoServerManager.NODE_TYPE_TPWAKESWITCH_GLOVE_ENABLE,mSettingOn);
        }
        if(mLcmAcl)
        {
            setNodeState(AmigoServerManager.NODE_TYPE_LCM_ACL_BRIGHTNESS,mSettingOn);
        }
        //Gionee <Amigo_Skylight> wangym <20150409> add for  CR01462639 begin
        if(!mSkyLightEnable)
        {
            setNodeState(AmigoServerManager.NODE_TYPE_HALL_SWITCH_ON,mSettingOff);
        }
        //Gionee <Amigo_Skylight> wangym <20150409> add for  CR01462639 end
        */
    }
    public void setNodeState(int NodeType,int mValue) {
        // TODO Auto-generated method stub
        Log.d(TAG, "setNodeState NodeType= " + NodeType + " mValue= " + mValue);
        synchronized(mHalLock){		
             nativeSetNodeState(NodeType,mValue);
        }
    }
    public int getNodeState(int NodeType)  {
        Log.d(TAG, "getNodeState NodeType" + NodeType );
        synchronized(mHalLock){			
            return nativeGetNodeState(NodeType);
        }
    }
    
    public String getNodeContent(int NodeType)  {
        Log.d(TAG, "getNodeContent NodeType" + NodeType );
        synchronized(mHalLock){         
            return nativeGetNodeContent(NodeType);
        }
    }
}
//Gionee <Amigo_Server> wangym 20141225 add for AmigoServer CR01431164 end

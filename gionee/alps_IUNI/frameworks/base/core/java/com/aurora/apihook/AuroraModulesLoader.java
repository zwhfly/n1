package com.aurora.apihook;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.aurora.apihook.XC_MethodHook.MethodHookParam;






import android.os.Build;
import android.text.GetChars;
import android.text.TextUtils;
import android.util.Log;
import dalvik.system.PathClassLoader;
public class AuroraModulesLoader {
	
	private static final int KITKAT = 19;
	
	private static final int JELLY_BEAN_MR1 = 17;
	
	private static final int JELLY_BEAN_MR2 = 18;
	
	private static final String TAG="init";
	
	public static final String BEFORE_PREFIX = "before_";
	
	public static final String AFTER_PREFIX = "after_";
	/**
	 * target apk,all of logic for aurora component are here
	 */
	private static final String APK_PATH="system/app/Aurora_Hook/Aurora_Hook.apk";
	
	private static final String MODULE_READER_NAME="com.aurora.apihook.ComponentReader";
	/*
	 * 和register.xml中的标签一一对应
	 */
	private static final String TAG_PARENT = "modules";
	private static final String TAG_MODULE = "module";
	private static final String ATTR_IMP_CLS = "impCls";
	private static final String  ATTR_ORG_CLS = "orgCls";
	private static final String ATTR_MIN_SDK = "minSdk";
	private static final String  ATTR_MAX_SDK = "maxSdk";
	private static final String ATTR_TARGET_SDK = "targetSdk";
	private static final String ATTR_METHOD = "method";
	
	private static final int INDEX_IMP_CLS = 0;
	
	private static final int INDEX_ORG_CLS = 1;
	
	private static final int INDEX_METHOD = 2;
	
	private static final String REGEX_KEY = "#";
	
	private static final boolean DEBUG_ABLE = false;
	
	private static final ClassLoader BOOTCLASSLOADER = ClassLoader.getSystemClassLoader();
	
	private static final HashMap<String, Object> mModulesClasses = new HashMap<String, Object>();
	
	private static StringBuffer mBuffer = new StringBuffer();
	/**
	 * invoked before original method
	 */
	public static final int BEFORE = 0;
	/**
	 * invoked after original method
	 */
	public static final int AFTER = 1;
	
	public static  List<String> mHookClasses ;
	private static  ClassLoader clzLoader;
	private static HashMap<String, String> methodsMap = new HashMap<String, String>();
	private static Class<?> modulesReaderClz; 
	private static List<Object> mObjects = new ArrayList<Object>();
	
	private static HashMap<String, Component> mObjectMap = new HashMap<String, Component>();
	
	private static List<String> mImpClass = new ArrayList<String>();
	
	
	private static HashMap<String, Method> mInvokeMethodMap  = new HashMap<String, Method>();
	
	
	/**
	 * parse all modules into framework
	 * @param clsLoader 
	 * @return
	 */
	private static List<Module>  parserModules(ClassLoader clsLoader){
		Module module = null;
		List<Module> modules = null;
		InputStream in = null;
		try{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		XmlPullParser parser = factory.newPullParser();
		 in = clzLoader.getResourceAsStream("assets/register.xml");
		parser.setInput(in, "UTF-8");
		
		int eventType = parser.getEventType();
		while(eventType != XmlPullParser.END_DOCUMENT){
			String nodeName = parser.getName();
			switch(eventType){
			case XmlPullParser.START_DOCUMENT:
				modules = new ArrayList<Module>();
				break;
			case XmlPullParser.START_TAG:
					if(TAG_MODULE.equals(nodeName)){
						module = new Module(getCurrentOsVersion(),getCurrentOsVersion(),getCurrentOsVersion());
						module.impCls = parser.getAttributeValue(null, ATTR_IMP_CLS);
						module.orgCls = parser.getAttributeValue(null, ATTR_ORG_CLS);
						module.method = parser.getAttributeValue(null, ATTR_METHOD);
						String maxSdk = parser.getAttributeValue(null, ATTR_MAX_SDK);
						if(!TextUtils.isEmpty(maxSdk)){
							module.maxSdk = Integer.parseInt(maxSdk);
						}
						String minSdk = parser.getAttributeValue(null, ATTR_MIN_SDK);
						if(!TextUtils.isEmpty(minSdk)){
							module.minSdk = Integer.parseInt(minSdk);
						}
						String targetSdk = parser.getAttributeValue(null, ATTR_TARGET_SDK);
						if(!TextUtils.isEmpty(targetSdk)){
							module.targetSdk = Integer.parseInt(targetSdk);
						}
						
					}
				break;
			case XmlPullParser.END_TAG:
				if(TAG_MODULE.equals(nodeName)){
					modules.add(module);
					module = null;
				}
				break;
			
			}
			
			eventType = parser.next();
			
		}
		
		}catch(Exception e){
			
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				in = null;
			}
		}
		/*
		 * if module has modified sdk version,we will fileter it 
		 */
		filterModules(modules);
		return modules;
	}
	
	private static class Module{
		String impCls;
		String orgCls;
		String method;
		int minSdk;
		int maxSdk;
		int targetSdk;
		
		Module(int maxSdk,int minSdk,int targetSdk){
		this.maxSdk = maxSdk;
		this.minSdk = minSdk;
		this.targetSdk = targetSdk;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "impCls:"+impCls+"    \n  orgCls:"+orgCls+"  \n  method:"+method;
		}
		
	}
	
	/**
	 * filter module that has target sdk version
	 * @param src
	 * @return
	 */
	private static  List<Module> filterModules( List<Module> src){
		int currentSdk = getCurrentOsVersion();
		for(int i = 0; i<src.size();i++){
			Module m = src.get(i);
			if(currentSdk < m.minSdk  || currentSdk > m.maxSdk || currentSdk != m.targetSdk){
				src.remove(m);
			}
			
		}
		
		return src;
	}
	/**
	 * load all modules into framework here,this method just called one time when Zygote forked 
	 * process
	 * all of files need to change will added here.
	 * 
	 */
	public static void init(){
		
		/**
		 * please  add invoke  infos in  Aurora_Hook/assets/register 
		 * 
		 */
		
		/**
		 * just load one time.loaded classes in Aurora_Hook.apk when zygote starting
		 */
		try{
			long start = System.currentTimeMillis();
			clzLoader = new PathClassLoader(APK_PATH,BOOTCLASSLOADER);
				List<Module> modules = parserModules(clzLoader);
				for(int i = 0;i<modules.size();i++){
					Module m = modules.get(i);
					String key = m.impCls.replaceAll(" ", "");
					
					if(!mObjectMap.containsKey(key) ){
						Class<?> clz = clzLoader.loadClass(key);
						Object instance = clz.newInstance();
						Component comp = new Component();
						comp.classObj = instance;
						comp.key = key;
						comp.name =m.orgCls;
						mObjectMap.put(key, comp);
					}
					
				}
		    long end = System.currentTimeMillis();
		    log( "totalTime:"+(end - start)+" size:"+mObjectMap.size());
			
			}catch(Exception e){
				log( "init Exception*********************** "+e);
			}
	}
	
	private static  int getCurrentOsVersion(){
		return android.os.Build.VERSION.SDK_INT;
	}
	
	@Deprecated
	private static boolean containt(String com){
		Set<String> keySet = mObjectMap.keySet();
		Iterator<String> in = keySet.iterator();
		while (in.hasNext()) {
			String key = (String) in.next();
			Component comOld = mObjectMap.get(key);
			if(comOld.name.equals(com)){
				return true;
			}
			
		}
		return false;
	}
	
	private static boolean mIsLoaded = false;
	
	/**
	 * stored Class info in Aurora_Hook.apk
	 * @author alexluo
	 *
	 */
	static class Component{
		Object classObj;
		String key;
		String name;
		
		public boolean equalsCom(Component obj) {
			// TODO Auto-generated method stubobj
			return obj.name.equals(name);
		}
	}
	
	/**
	 * create a MethodHookParam to save object's data
	 * @param obj
	 * @param args
	 * @param invokeType
	 * @return
	 */
	public static MethodHookParam createParams(Object obj,Object[] args,int invokeType){
		MethodHookParam param = new MethodHookParam();
		param.thisObject = obj;
		param.args = args;
		param.invokeType = invokeType;
		return param;
	}
	
	/**
	 * split invoke key
	 * @param key  invoke key
	 * @param regex
	 * @param index
	 * @return
	 */
	private static String getSubString(String key,String regex,int index){
		
		return key.split(regex)[index];
	}
	
	/**
	 * invoke method that defined in Aurora_Hook.apk 
	 * @param key
	 * @param param
	 */
	public static void invoke(String key,MethodHookParam param){
		long startTime = System.currentTimeMillis();
		StringBuilder targetMethodBuilder = new StringBuilder();
		Component component = null;
		try{
			
			Set<String> keys = mObjectMap.keySet();
			Iterator<String> in = keys.iterator();
			String impCls = getSubString(key, REGEX_KEY, INDEX_IMP_CLS).replaceAll(" ", "");
			
			boolean clsLoaded = mObjectMap.containsKey(impCls);
		
			if(clsLoaded){
				component = mObjectMap.get(impCls);
			}
		
			
			if(component == null){
				param.setResult(null);
				return;
			}
			String orgClsName = getSubString(key, REGEX_KEY,INDEX_ORG_CLS);
			
			Class<?> clz = component.classObj.getClass();
			
			String methodName =getSubString(key, REGEX_KEY, INDEX_METHOD);
		
//			String prefix = "";
			if(param.invokeType == BEFORE){
				targetMethodBuilder.append(BEFORE_PREFIX);
//				prefix = BEFORE_PREFIX;
			}else{
				targetMethodBuilder.append(AFTER_PREFIX);
//				prefix = AFTER_PREFIX;
			}
			if(clz != null){
//				log(  "methodName:"+clz.getName()+"#"+prefix+methodName);
				targetMethodBuilder.append(methodName);
				String cachedKey = clz.getName()+targetMethodBuilder.toString();
				Method method = mInvokeMethodMap.get(cachedKey);//clz.getDeclaredMethod(prefix+methodName, MethodHookParam.class);
				if(method == null){
					method = clz.getDeclaredMethod(targetMethodBuilder.toString(), MethodHookParam.class);
					if(!mInvokeMethodMap.containsKey(cachedKey)){
						mInvokeMethodMap.put(cachedKey, method);
					}
				}
				if(method != null){
					method.setAccessible(true);
					method.invoke(component.classObj, param);
				}
			}
			
			long endTime = System.currentTimeMillis();
			log(  "invoke Time:"+(endTime - startTime));
		}catch(Exception e){
			param.setResult(null);
			log( "invoke Exception---------->"+e);
		}
		
//		}
	}
	private static void log(String msg){
		if(DEBUG_ABLE){
			Log.i(TAG, msg);
		}
	}
	public static void loadModules(){
		init();
		mIsLoaded = true;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

package android.content.res;

import android.content.res.AuroraThemeZip.ThemeFileInfo;

/**
 * 系统资源framework-res.apk
 * @author alexluo
 *
 */
public class AuroraSystemTheme extends AuroraApplicationTheme {

	private static AuroraSystemTheme mInstance;
	
	  // Locker
    private static final Object sLock = new Object();
	
	private AuroraSystemTheme(Resources res, String packageName) {
		super(res, packageName);
		// TODO Auto-generated constructor stub
	}
	
	public static AuroraSystemTheme getInstance(Resources res, String packageName){
		synchronized (sLock) {
			if(mInstance == null){
				mInstance = new AuroraSystemTheme(res, packageName);
				
			}
			mInstance.update();
			return mInstance;
		}
	}
	
	
	@Override
	public Integer getThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeInteger(id);
	}
	
	@Override
	public Float getFloatThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeFloat(id);
	}
	

	@Override
	public ThemeFileInfo getThemeFileInfo(String themePah,int id) {

		return mThemeZipFile.getThemeFileInfo(AuroraApplicationTheme.SYSTEM_RES_ANDROID, themePah);
	}
	

}

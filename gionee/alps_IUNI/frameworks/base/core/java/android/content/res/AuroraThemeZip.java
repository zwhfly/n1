package android.content.res;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;












import com.android.internal.util.XmlUtils;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
public class AuroraThemeZip {
	
	private static final boolean DBG = true;
	
private static final String TAG = "AuroraThemeZip";
	
	private static final String ENCODING = "utf-8";
	
	private static final String TAG_DIMEN = "dimen";
	
	private static final String TAG_COLOR = "color";
	
	private static final String TAG__BOOL = "bool";
	
	private static final String TAG_INTEGER = "integer";
	
	private static final String ATTR_NAME = "name";
	
	private static final String VALUES = "values.xml";
	
	

	private ThemeZip mThemeZip;
	
	
	/**
	 * current application's theme path
	 */
	private String mThemeZipPath;
	
	
   //	com.aurora/res/drawable-xhdpi/btn.png
	
	
	/**
	 * cached theme values;
	 */
	private SparseArray mThemeValues = new SparseArray();
	
	/**
	 * map for cached ThemeZip Files
	 */
	private static HashMap<String, WeakReference<AuroraThemeZip>> mPackageThemeZipFiles = new HashMap<String, WeakReference<AuroraThemeZip>>();
	
	
	private String mPackageName;
	
	private AuroraApplicationTheme.ThemeResourceData mThemeResourcesData;
	
	
	private AuroraDensityUtils mDensityUtils;
	
	private long mThemeFileZipModifiedTime = -1L;
	
	private int mDensity;
	
	private boolean[] mHasThemeValue = {false,false,false,false};
	
	public class ThemeFileInfo {
		public int density;
		public InputStream inputStream;
		public long size;
		public String srcName;

		ThemeFileInfo(InputStream inputstream, long size) {
			this.inputStream = inputstream;
			this.size = size;
		}
	}
	
	

	
	
	public AuroraThemeZip(String packageName,AuroraApplicationTheme.ThemeResourceData themeResourcesData){
		this.mPackageName = packageName;
		this.mThemeResourcesData = themeResourcesData;
		mDensityUtils = new AuroraDensityUtils();
		mThemeValues.clear();
		mDensity = AuroraDensityUtils.getBestDensity();
	}
	
	/**
	 * if current theme file is updated ,open it
	 * @param resources
	 */
	public void updateThemeFiles(Resources resources){
			clear();
			openThemeZipFile();
			parserThemeValues(mPackageName, resources);
	}
	
	/**
	 * open theme zip file
	 */
	private void openThemeZipFile(){
		StringBuffer themePath = new StringBuffer();
		themePath.append(mThemeResourcesData.themePath);
		themePath.append(mPackageName);
		File file = new File(themePath.toString());
		//Log.d(TAG, "themefile-->"+themePath+"   hasFIle-->"+file.exists()+"  "+android.os.Debug.getCallers(5));
		if(file.exists()){
			parserThemeResFromFile(file);
		}
	}
	
	/**
	 * clear all datas that chached
	 */
	public void clear(){
		mThemeValues.clear();
		if(mThemeZip != null){
			try {
				mThemeZip.close();
				mThemeZip = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 判断主题中的values数据是否加载成功
	 * @return
	 */
	public boolean hasThemeValues(){
		return mThemeValues.size() > 0;
	}
	
	
	/**
	 * parse theme value(eg. color,dimension,integer,bool) from values.xml
	 * @param packageName
	 * @param resources
	 */
	private void parserThemeValues(String packageName,Resources resources){
		XmlPullParser xmlParser = null;
		StringBuffer valuesBuffer = new StringBuffer();
		valuesBuffer.append(packageName+File.separator+VALUES);
		InputStream input = null;
		ThemeFileInfo themeFileInfo = getThemeFileStream(valuesBuffer.toString());
		if(themeFileInfo == null){
			return;
		}
		try {
			xmlParser = XmlPullParserFactory.newInstance().newPullParser();
			input = themeFileInfo.inputStream;
			xmlParser.setInput(input, ENCODING);
			int eventType = xmlParser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT){
				  switch (eventType){
		             case XmlPullParser.START_TAG:
		                 String tag = xmlParser.getName();
		                 
		                 int attrCount = xmlParser.getAttributeCount();
		                 String resName = null;
		                 int resId = 0;
		                 for(int i = 0 ; i<attrCount;i++){
		                	 String name = xmlParser.getAttributeName(i);
		                	 if(ATTR_NAME.equals(name)){
		                		 resName = xmlParser.getAttributeValue(i);
		                		 break;
		                	 }
		                 }
		                 
		                 if(!TextUtils.isEmpty(resName)){
		                	resId = resources.getIdentifier(resName, tag, mPackageName);
		                 }
		                 if(TAG_DIMEN.equalsIgnoreCase(tag)){
		                	 if(resId != 0){
		                		 String text = xmlParser.nextText();
		                		 if(!TextUtils.isEmpty(text)){
		                			 float dimen = AuroraDensityUtils.parseDimen(text, resources);
		                			 mThemeValues.put(resId, dimen);
		                		 }
		                		 
		                	 }
		                	 
		                 }else if(TAG_COLOR.equalsIgnoreCase(tag)){
		                	 if(resId != 0){
		                		 String text = xmlParser.nextText();
		                		 if( mThemeValues.indexOfKey(resId) < 0){
		                			 mThemeValues.put(resId, AuroraDensityUtils.parseColor(text));
		                    	 }
		                	 }
		                 }else if(TAG__BOOL.equalsIgnoreCase(tag)){
		                	 if(resId != 0){
		                		 String text = xmlParser.nextText();
								if (!TextUtils.isEmpty(text)) {
									boolean bool = AuroraDensityUtils.parseBool(text);
									mThemeValues.put(resId, bool?1:0);
								}
		                	 }
		                 }else if(TAG_INTEGER.equalsIgnoreCase(tag)){
		                	 if(resId != 0){
		                		 String text = xmlParser.nextText();
								if (!TextUtils.isEmpty(text)) {
									int integer = AuroraDensityUtils.parseInteger(text);
									mThemeValues.put(resId, integer);
								}
		                	 }
		                 }
		                 break;
		             case XmlPullParser.END_TAG:
		            	 //do nothing
		                 break;
		             default:
		                 break;
		             }
	             eventType = xmlParser.next();
	         }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(input != null){
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				input = null;
			}
		}
	}
	public Integer getThemeInteger(int id){
		return (Integer) mThemeValues.get(id);
	}
	
	public Float getThemeFloat(int id){
		return (Float) mThemeValues.get(id);
	}
	
	
	
	
	public ThemeFileInfo getThemeFileInfo(String packageName,String themeFilePath){
		int density = AuroraDensityUtils.getBestDensity();
		String path = getFilePath(packageName, themeFilePath, mDensityUtils.getMatchedDrawable(density));
		if(TextUtils.isEmpty(path)){
			return null;
		}
		ThemeFileInfo themeFileInfo =  getThemeFileStream(path);
		int targetDensityIndex = AuroraDensityUtils.getBestDensityIndex(density);
		if(targetDensityIndex != -1){
		if(themeFileInfo == null){
			for(int i = targetDensityIndex + 1;i < AuroraDensityUtils.DENSITY_DIR_COUNT;i++){
					 int tmpDensity = AuroraDensityUtils.getDensityByIndex(i);
				      String tmpDir = mDensityUtils.getMatchedDrawable(tmpDensity);
				      String tmpPath = getFilePath(packageName, themeFilePath, tmpDir);
				      themeFileInfo =  getThemeFileStream(tmpPath);
				      if(themeFileInfo != null){
				    	  density = tmpDensity;
					    	  themeFileInfo.srcName = tmpPath;
					    	  break;
				      }
				    
			}
		}
		
		if(themeFileInfo == null){
			for(int i = targetDensityIndex - 1;i >= 0 ;i--){
				 int tmpDensity = AuroraDensityUtils.getDensityByIndex(i);
			      String tmpDir = mDensityUtils.getMatchedDrawable(tmpDensity);
			      String tmpPath = getFilePath(packageName, themeFilePath, tmpDir);
			      themeFileInfo =  getThemeFileStream(tmpPath);
			      if(themeFileInfo != null){
			    	  density = tmpDensity;
				    	  themeFileInfo.srcName = tmpPath;
				    	  break;
			      }
			}
		}
		}
		if(themeFileInfo != null){
			themeFileInfo.density = density;
		}
		return themeFileInfo;
	}
	
	private String getFilePath(String packageName,String themeFilePath,String densityDir){
		if(TextUtils.isEmpty(themeFilePath)){
			return null;
		}
		
		String[] paths = themeFilePath.split("/");
		StringBuffer path = new StringBuffer();
		path.append(packageName);
		path.append("/");
		try{
		path.append(paths[0]);
		path.append("/");
		path.append(densityDir);
		path.append(paths[2]);
		}catch(ArrayIndexOutOfBoundsException ex){
			Log.d("exp", "path-->"+themeFilePath+"  packageName-->"+packageName);
		}
		return path.toString();
	}
	
	/**
	 * 根据包名获取匹配的主题文件
	 * @param packageName
	 * @param id
	 * @param resources
	 * @return
	 */
	public ThemeFileInfo getThemeFileStream(String packageName,int id,Resources resources){
		ThemeFileInfo input = null;
		return input;
	}
	
	/**
	 * 获取图片的文件名
	 * @param res
	 * @param id
	 * @return
	 */
	private String getDrawableName(Resources res,int id){
		String orgFileName = res.getResourceName(id);
		if(TextUtils.isEmpty(orgFileName)){
			return "";
		}
		int index = orgFileName.indexOf("/");
		return orgFileName.substring(index+1, orgFileName.length());
	}
	
	
	/**
	 * 获取主题文件的信息，包括主题文件的大小，输入流
	 * @param themeFileName
	 * @return
	 */
	private ThemeFileInfo getThemeFileStream(String themeFileName){
		ThemeFileInfo input = null;
		if(mThemeZip == null){
			return input;
		}
		ZipEntry entry = mThemeZip.getEntry(themeFileName);
		if(entry != null){
			try {
				InputStream stream = mThemeZip.getInputStream(entry);
				
				input = new ThemeFileInfo(stream, entry.getSize());
				input.srcName = entry.getName();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "open theme  file exception-->"+e);
			}
		}
		
		return input;
	}
	
	
	public static AuroraThemeZip getThemeZipFile(String packageName,AuroraApplicationTheme.ThemeResourceData themeResourcesData){
		if(mPackageThemeZipFiles.containsKey(packageName)){
			return mPackageThemeZipFiles.get(packageName).get();
		}
		
		AuroraThemeZip themeZipFile = new AuroraThemeZip(packageName,themeResourcesData);
		mPackageThemeZipFiles.put(packageName, new WeakReference<AuroraThemeZip>(themeZipFile));
		
		return themeZipFile;
	}
	/**
	 * 打开主题zip文件，并保存到系统中
	 * @param file
	 */
	private void parserThemeResFromFile(File file) {
		try{
		mThemeZip = new ThemeZip(file);
		}catch(Exception ex){
			Log.d(TAG, "create theme file failure-->"+ex);
		}
	}
	
	
	private void log(String tag,String msg){
		if(DBG){
			Log.d(tag, msg);
		}
	}
	
	
	class ThemeZip extends ZipFile{

		HashMap<String, ZipEntry> cache = new HashMap<String, ZipEntry>();
		
		public ThemeZip(File file) throws ZipException, IOException {
			super(file);
			cache.clear();
			// TODO Auto-generated constructor stub
			try {
				ZipFile zipFile = new ZipFile(file);
				Enumeration<ZipEntry> zipEnumeration = (Enumeration<ZipEntry>) zipFile.entries();
				ZipEntry entry = null;
				while (zipEnumeration.hasMoreElements()) {
					entry = zipEnumeration.nextElement();
					if (!entry.isDirectory()) {
						cache.put(entry.getName(),entry);
					}
				}

			} catch (Exception ex) {
				Log.d(TAG, "open theme zip file exception-->" + ex);
			}
		}
		
		public ZipEntry getEntry(String key){
			return cache.get(key);
		}
		
		public int getSize(){
			return cache.size();
		}
		
		
	}





	public InputStream getAssetsFile(String packageName,String assetPath) {
		// TODO Auto-generated method stub
		String themePath = getAssetPath(packageName, assetPath);
		ThemeFileInfo info = getThemeFileStream(themePath);
		if(info != null){
			return info.inputStream;
		}
		return null;
	}
	
	private String getAssetPath(String packageName,String assetName){
		StringBuffer builder = new StringBuffer();
		builder.append(packageName);
		builder.append("/");
		builder.append("assets/");
		builder.append(assetName);
		return builder.toString();
	}
	
}

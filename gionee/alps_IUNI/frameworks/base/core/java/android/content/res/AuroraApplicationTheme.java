package android.content.res;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.res.AuroraThemeZip.ThemeFileInfo;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;

/**
 * app自己的资源
 * @author alexluo
 *
 */
public class AuroraApplicationTheme {
	/**
	 * 当前主题包存放的路径
	 */
	private static final String THEME_PATH = "/data/theme/current/";
	
	/**
	 * 系统主题包名
	 */
	public static final String SYSTEM_RES_ANDROID = "android";
	
	/**
	 * Aurora系统主题包名
	 */
	public static final String SYSTEM_RES_IUNI = "com.aurora";
	
	private static final String LAUNCHER_ICON_PACKAGE_NAME = "com.aurora.publicicon.res";
	
	private static final String THEME_ICON_PACKAGENAME = "icons";

	public static final int INDEX_COLOR = 0;

	public static final int INDEX_DIMEN = 1;

	public static final int INDEX_INTEGER = 2;

	public static final int INDEX_BOOL = 3;
	
	private Resources mRes;
	
	private String mPackageName;
	
	private ThemeResourceData mData;
	
	protected AuroraThemeZip mThemeZipFile;
	
	private List<String> mSystemResPkgs = new ArrayList<String>();
	
	
	
	private Map<String,WeakReference<AuroraApplicationTheme>> mSystemTheme = new HashMap<String,WeakReference<AuroraApplicationTheme>>();
	
	public AuroraApplicationTheme(Resources res,String packageName){
		this.mRes = res;
		this.mPackageName = packageName;
		mData = new ThemeResourceData();
		mData.themePath = THEME_PATH;
		mThemeZipFile = new AuroraThemeZip(mPackageName, mData);
	}
	
	public void update(){
		mThemeZipFile.updateThemeFiles(mRes);
		mSystemResPkgs.clear();
	}
	
	public void addSystemResources(String packageName){
		if(mSystemResPkgs.contains(packageName)){
			return;
		}
		mSystemResPkgs.add(packageName);
		addSystemResources();
	}
	
	/**
	 *初始化系统资源 
	 */
	private void addSystemResources(){
		
		int length = mSystemResPkgs.size();
		if(length > 0){
			try {
				for(int i = 0; i < length;i++){
					String pkgName = mSystemResPkgs.get(i);
					AuroraApplicationTheme systemTheme = makeSystemTheme(pkgName);
					if(systemTheme != null){
						mSystemTheme.put(pkgName, new WeakReference<AuroraApplicationTheme>(systemTheme));
					}
				}
				} catch (Exception e) {
				// do nothing
			}
			
		}
	
	}
	/**
	 * 初始化系统资源包，每个app都包含有两个系统资源包
	 * @param pkgName
	 * @return
	 */
	private AuroraApplicationTheme makeSystemTheme(String pkgName){
		if(SYSTEM_RES_ANDROID.equals(pkgName)){
			return AuroraSystemTheme.getInstance(mRes, pkgName);
		}else if(SYSTEM_RES_IUNI.equals(pkgName)){
			return AuroraFrameworkTheme.getInstance(mRes, pkgName);
		}
		return null;
	}
	
	public Drawable getThemeDrawable(int id){
		Drawable drawable = null;
		
		ThemeFileInfo themeInfo = mThemeZipFile.getThemeFileStream(mPackageName, id, mRes);
		if (themeInfo != null) {
			drawable = Drawable.createFromStream(themeInfo.inputStream,
					themeInfo.srcName);
		}
		return drawable;
	}
	
	/**
	 * 每个应用都包含了两个系统资源包，一个是android，一个是aurora
	 * @param id
	 * @return  根据目标id返回不同的系统资源包
	 */
	private AuroraApplicationTheme getSystemTheme(int id){
		AuroraApplicationTheme systemTheme = null;
		if(id != 0){
			String pkgName = mRes.getResourcePackageName(id);
			final WeakReference<AuroraApplicationTheme> wkr = mSystemTheme.get(pkgName);
			if(wkr != null){
				systemTheme = wkr.get();
			}
			if (systemTheme == null) {
				if (SYSTEM_RES_ANDROID.equals(pkgName)) {
					systemTheme = AuroraSystemTheme.getInstance(mRes,
							SYSTEM_RES_ANDROID);
				} else if (SYSTEM_RES_IUNI.equals(pkgName)) {
					systemTheme = AuroraFrameworkTheme.getInstance(mRes,
							SYSTEM_RES_IUNI);
				}
			}
		}
		
		return systemTheme;
	}
	
	
	
	public boolean hasThemeValue(){
		return mThemeZipFile.hasThemeValues();
	}
	
	/**
	 * get resource values from theme(e.g color,dimen
	 * @param id
	 * @return
	 */
	public Integer getThemeValue(int id){
		AuroraApplicationTheme systemTheme = getSystemTheme(id);
		if(systemTheme != null){
			return systemTheme.getThemeValue(id);
		}
		return mThemeZipFile.getThemeInteger(id);
	}
	
	
	/**
	 * get resource values from theme(e.g color,dimen
	 * @param id
	 * @return
	 */
	public Float getFloatThemeValue(int id){
		AuroraApplicationTheme systemTheme = getSystemTheme(id);
		if(systemTheme != null){
			return systemTheme.getFloatThemeValue(id);
		}
		return mThemeZipFile.getThemeFloat(id);
	}
	
	/**
	 * get file info from theme
	 * @param themePah
	 * @param id
	 * @return
	 */
	public ThemeFileInfo getThemeFileInfo(String themePah, int id) {
		AuroraApplicationTheme systemTheme = getSystemTheme(id);
		if (systemTheme != null) {
			return systemTheme.getThemeFileInfo(themePah, id);
		}
		if(TextUtils.isEmpty(mPackageName)){
			mPackageName = mRes.getResourcePackageName(id);
		}
		return mThemeZipFile.getThemeFileInfo(mPackageName, themePah);
	}
	
	
	private ThemeFileInfo getIcon(String themePath,int id){
		String pkgName = mRes.getResourcePackageName(id);
		if(LAUNCHER_ICON_PACKAGE_NAME.equals(pkgName)){
			
			return mThemeZipFile.getThemeFileInfo(pkgName, themePath);
		}
		return null;
	}
	
	public static  class ThemeResourceData{
		public String themePath;
	}


	public void freeCaches() {
		// TODO Auto-generated method stub
		update();
		 Collection<WeakReference<AuroraApplicationTheme>> systemThemes = mSystemTheme.values();
		 if(systemThemes != null && systemThemes.size() > 0){
			Iterator<WeakReference<AuroraApplicationTheme>> iterator = systemThemes.iterator();
			while(iterator.hasNext()){
				final WeakReference<AuroraApplicationTheme> theme = iterator.next();
				if(theme != null){
					theme.get().update();
				}
			}
		 }
	}

	public InputStream getAssetsFile(String assetPath) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getAssetsFile(mPackageName,assetPath);
	}

	
	
	
	
	
}

//Gionee <Amigo_AppCheckPermission> <liuyb> <20140704> add for CR01316056 begin
package android.util;

public class AmigoSecurityException extends RuntimeException {

    private static final long serialVersionUID = 6878364983674394168L;

    public AmigoSecurityException() {
        super();
    }

    public AmigoSecurityException(String s) {
        super(s);
    }

    public AmigoSecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public AmigoSecurityException(Throwable cause) {
        super(cause);
    }
}
//Gionee <Amigo_AppCheckPermission> <liuyb> <20140704> add for CR01316056 end
//Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

// Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 begin
import android.content.pm.PackageManager;
// Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 end
/**
 * Class that operates consumer infrared on the device.
 *
 * <p>
 * To obtain an instance of the system infrared transmitter, call
 * {link android.content.Context#getSystemService(java.lang.String)
 * Context.getSystemService()} with
 * {link android.content.Context#AMIGO_ACTIVITY_MANAGER} as the argument.
 * </p>
 */
public final class AmigoActivityManager  {
    private static final String TAG = "AmigoActivtyManager";
    private final IAmigoActivityManager mService;

    public AmigoActivityManager(Context context) {
        mService = IAmigoActivityManager.Stub.asInterface(
                ServiceManager.getService(Context.AMIGO_ACTIVITY_MANAGER));
    }
    
    public void setPackagePermEnable(String pk) {
        // TODO Auto-generated method stub
        try{				
             mService.setPackagePermEnable(pk);
        }catch (RemoteException e){
        }
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 begin
    public AmigoActivityManager() {
        mService = IAmigoActivityManager.Stub.asInterface(
                ServiceManager.getService(Context.AMIGO_ACTIVITY_MANAGER));
    }

    public int amigoCheckPermmison(String permissionName, int callingUid) {
		int checkValue = PackageManager.PERMISSION_GRANTED;
        try{				
             checkValue = mService.amigoCheckPermmison(permissionName, callingUid);
        }catch (RemoteException e){
        }
		return checkValue;
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 end
}
//Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end

package com.android.internal.policy.impl;

/**
 * 
 * 	@author xiejun
 *  add for three finger screenshot
 * 
 */
 
public interface AuroraITakeScreenShot {
	public void takeScreenShot();
}

package com.android.internal.policy.impl;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.IWindowManager;
import android.view.WindowManager;
import android.os.ServiceManager;
import android.database.ContentObserver;
import android.graphics.Point;
import static android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.provider.Settings;
import android.view.WindowManagerPolicy.PointerEventListener;
import android.widget.Toast;

/**
 * 
 * 	@author xiejun
 *  add for three finger screenshot
 * 
 */
 
public class AuroraPointerInterceptor {
    private static final String TAG = "000000";
    private static final boolean DEBUG = true;
    private static final boolean THREEFINGERS_CAPTURE_SUPPORT = true;//SystemProperties.getBoolean("ro.aurora.threefingercapture", true);
    private Context mContext;
    private WindowManagerFuncs mWindowManagerFuncs;
    private IWindowManager mWindowManager;
	private AuroraThreeFingerScreenShotPolicy mScreenShot;
	private PointerEventListener mCaptureEventListener;
    private Handler mHandler = new Handler();
    private final static String three_finger_screenshot_enable = "three_finger_screenshot_enable";
    Resources mResources;
    
    private static final int INIT_THRESHOLD = 2520; // Hot area > threshold
    private int mThreshold = INIT_THRESHOLD; // depends on screen size
    private static final int HOT_ZONE = 40;// for pull up, hot zone from TP bottom edge
    
    private static final int MSG_REGISTER_CLIENT = 1; // MUST be the same as SystemUI.InvokerService`s defination
    private static final int MSG_UNREGISTER_CLIENT = 2; // MUST be the same as SystemUI.InvokerService`s defination
    private static final int MSG_SEND_EVENT = 3; // MUST be the same as SystemUI.InvokerService`s defination
    private static final int MSG_UPDATE_MSK = 4; // MUST be the same as SystemUI.InvokerService`s defination
    private static int mMask = 0;
    private static Boolean mResetLater = false; // reset consume state later
    private static Boolean mDowningFlag = false; // Action DOWN received
    private static Boolean mKeyguardOn = false; // give way for keyguard
    private static MotionEvent mLastDownEvent; // holder
    private static Boolean mDownEventSent = false; // Action DOWN sent to target
    private ConsumeState mConsumeState = new ConsumeState();
    
    private static final int DIST_THRESHOLD = 45;// 20; // default distance
    private int mDist = DIST_THRESHOLD; // can be changed by DEBUG intent
    
    private static final String NAVI_KEY_HIDE = "navigation_key_hide";
    boolean mHasNaviBar = false;
    boolean mNavigationBarHidden = false;
    private int mOrientation  = Configuration.ORIENTATION_PORTRAIT;
    
    public static final String AURORA_BIND_INVOKER_SERVICE = "com.android.systemui.recent.AURORA_BIND_INVOKER_SERVICE";
    public static final String AURORA_RESET_PI_STATE = "com.android.systemui.recent.AURORA_RESET_PI_STATE";
    
    /** Messenger for communicating with service. */
    Messenger mService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsBound;
    
    private Handler mClientHandler = new IncomingHandler();

    /**
     * Handler of incoming messages from service.
     * for update msk
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_SEND_EVENT:
                break;
            case MSG_UPDATE_MSK:
                if (!mDowningFlag)
                    mMask = msg.arg1;
                break;
            default:
                super.handleMessage(msg);
            }
        }
    }
    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(mClientHandler);
    
    public AuroraPointerInterceptor(Context context, WindowManagerFuncs windowManagerFuncs) {
        mContext = context;
        mWindowManagerFuncs = windowManagerFuncs;
        mResources = mContext.getResources();
        IntentFilter filter = new IntentFilter(AURORA_BIND_INVOKER_SERVICE);
        filter.addAction(AURORA_RESET_PI_STATE);
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(AURORA_BIND_INVOKER_SERVICE)) {
                    enable();
                }  else if (AURORA_RESET_PI_STATE.equals(intent.getAction())) {
				    reset();
				}
            }
        }, filter);
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(three_finger_screenshot_enable), true,
                mThreeFingerScreenshotSettingObserver);
        
    }
    private final class ThreeFingerCaptureEventListener implements PointerEventListener {
        @Override
        public void onPointerEvent(MotionEvent motionEvent) {
            onMotionEvent(motionEvent);
        }
    }

    public void enable() {
    	log("enable");
    	if (mWindowManager == null)
    		mWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
    	
    	setupHotZoneThreshold();
    	
		if(THREEFINGERS_CAPTURE_SUPPORT){
			if (mScreenShot == null){
				mScreenShot = new AuroraThreeFingerScreenShotPolicy(mContext);
			} 
		}
		
		if(mCaptureEventListener == null){
			mCaptureEventListener = new ThreeFingerCaptureEventListener();
			mWindowManagerFuncs.registerPointerEventListener(mCaptureEventListener);
		}
		
		if (!mHasNaviBar) {
			log("enable---" + "doBindService()");
			doBindService();
		}
    }
    
    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                IBinder service) {
        	log("onServiceConnected()");
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mService = new Messenger(service);

            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null, MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }
        }

        public void onServiceDisconnected(ComponentName className) {
        	log("onServiceDisconnected()");
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
        }
    };
    
    void doBindService() {
        if (mHasNaviBar) { 
        	return;
        }
        log("doBindService()");
        // TODO should apply better security policy!
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Intent intent = new Intent();
        intent.setClassName("com.android.systemui", "com.android.systemui.recent.InvokerService");
        mContext.bindServiceAsUser(intent, mConnection, Context.BIND_AUTO_CREATE,new UserHandle(UserHandle.USER_CURRENT));
        mIsBound = true;
    }
    
    void doUnbindService() {
        if (mHasNaviBar) { 
        	return;
        }
        log("doUnbindService()");
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }
    
    private void setupHotZoneThreshold() {
        Point p = new Point(0,0);
        WindowManager manager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        display.getRealSize(p);
        // Typically on a phone, longer side is portrait Y axis. That`s where we apply pull up feature. 
        mThreshold = (p.x > p.y) ? p.x : p.y;
        //mThreshold -= HOT_ZONE;
        int hot_zone = dip2px(mContext, 7.5f);
        mThreshold -= hot_zone;
        if (mThreshold <= 0) {
        	mThreshold = INIT_THRESHOLD;
        }
    }
    
    private int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public void disable(){
    	log("disable");
        if(THREEFINGERS_CAPTURE_SUPPORT){
			if(mCaptureEventListener != null){
				mWindowManagerFuncs.unregisterPointerEventListener(mCaptureEventListener);
				mCaptureEventListener = null;
			}
		}
    }

    private void onMotionEvent(MotionEvent event) {
		if(THREEFINGERS_CAPTURE_SUPPORT && isThreeFingerScreenShotEnable()){
			mScreenShot.handleEvent(event);
		}

		if (mMask == 0) {
			// steve.tang, at the begin, wo only use this lisenter to capture, now not support Pull-up(recent view)
			// shield by steve.tang, it may be reopen.
			handleEvent(event);
		}
		
        if (mConsumeState.getState() && mResetLater) {
            mResetLater = false;
            mConsumeState.update(false);
        }
    }
    
    // Core logic function 
    private void handleEvent(MotionEvent event){

        if (event.getPointerCount() > 1 ) {
            // cancelTracking();
            return;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN :
                beginTracking(event);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (!mKeyguardOn)
                    cancelTracking(event);
                else {
                    mKeyguardOn = false;
                    return;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (mKeyguardOn) {
                    return;
                }
                detectTracking(event);
                break;
        }
    }
    
    // Begin track motion event
    private void beginTracking(MotionEvent event) {
        log("beginTracking()");
        mDowningFlag = true;
        mDownEventSent = false;
        mConsumeState.update(false);
        mLastDownEvent = MotionEvent.obtain(event);
        mOrientation = mResources.getConfiguration().orientation;
        // update navigation bat status
        mNavigationBarHidden = isNavigationBarHidden();

        mKeyguardOn = false;
        try {
			if(mWindowManager != null)
            	mKeyguardOn = mWindowManager.isKeyguardLocked();
        } catch (RemoteException re) {
            /* ignore, good luck with no WindowManager */
        }
        if (mKeyguardOn || !mIsBound) {
            log("mKeyguardOn = true DOWN || !mIsBound");
            return;
        }
    }
    
    /**
     * Detect finger movement
     * If ACTION_DOWN point is at touch panel bottom & dragged up for certain distance. Consume!
     * TODO only comparing with initial down point. Not good enough!
     */
    private void detectTracking(MotionEvent event){
        log("detectTracking()");
        if (mConsumeState.getState()) {
            consumeEvent(event);
            return;
        }
        if (mLastDownEvent == null) {
            return;
        }
        double currentX = event.getX();
        double currentY = event.getY();
        double downX = mLastDownEvent.getX();
        double downY = mLastDownEvent.getY();
        double absX = Math.abs(currentX - downX);
        double absY = Math.abs(currentY - downY);
        double distance = Math.hypot(absX, absY);
        long duration = event.getEventTime() - event.getDownTime();

        //log("detectTracking() mOrientation = " + mOrientation);
        //log("detectTracking() currentY = " + currentY + "  downY = " + downY);
        //log("detectTracking() currentX = " + currentX + "  downX = " + downX);
        //log("detectTracking() absX = " + absX + "  absY = " + absY);
        //log("detectTracking() distance = " + distance + "  duration = " + duration);

        if (((mOrientation == Configuration.ORIENTATION_PORTRAIT?downY:downX) > mThreshold)
                && distance > mDist) {
            // consume when navigation bar showing & draging long enough from hot zone
            if (mHasNaviBar && mNavigationBarHidden) {
                consumeEvent(event);
            }
            else if (!mHasNaviBar) {
                consumeEvent(event);
            }
        }
    }
    
    // Cancel on ACTION_UP or ACTION_CANCEL, reset state
    private void cancelTracking(MotionEvent event) {
        log("cancelTracking()");
        mDowningFlag = false;
        mDownEventSent = false;
        mResetLater = true;

        if (mLastDownEvent != null)
            mLastDownEvent = null;

        // finish with handOver Up event
        if (mConsumeState.getState())
            sendMsg(event); 
    }
    
    // Pass MotionEvent to SystemUI
    private void consumeEvent(MotionEvent event){
        log("consumeEvent() mHasNaviBar = " + mHasNaviBar );
        mConsumeState.update(true);

        // tell the world, we have ate the motion
        MotionEvent cancelEvent = MotionEvent.obtain(event);
        cancelEvent.setAction(MotionEvent.ACTION_CANCEL);
        //mNext.onMotionEvent(cancelEvent,rawEvent,policyFlags);

		// currently this feature disabled on U3 kitkat with navi bar

        if (mIsBound) {
        	if (!mDownEventSent) {
        		log("consumeEvent() handOverMotionEvent()  mLastDownEvent");
        		// hand over touch down event
        		sendMsg(mLastDownEvent); 
        		mDownEventSent = true;
        	} else {
        		log("consumeEvent() handOverMotionEvent");
        		//sLastMoveTime = event.getEventTime();
        		sendMsg(event);
        	}
        }
	}
    
    // Send IPC message
    private void sendMsg(MotionEvent event) {
        if (mHasNaviBar) return;
        log("sendMsg()");
        Message msg = Message.obtain(null, MSG_SEND_EVENT, event);
        msg.replyTo = mMessenger;
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            log("sendMsg() caught RemoteException " + e);
        } catch (NullPointerException e) {
            log("sendMsg() caught NullPointerException " + e);
            // rebind service
            doUnbindService();
            doBindService();
        }
    }
    
	private boolean isThreeFingerScreenShotEnable(){
		return Settings.System.getInt(mContext.getContentResolver(), three_finger_screenshot_enable, 1) == 1;
	}
	
	protected final ContentObserver mThreeFingerScreenshotSettingObserver = new ContentObserver(mHandler) {
        @Override
        public void onChange(boolean selfChange) {
        	/*
            final int three_finger_setting_value = Settings.System.getInt(mContext.getContentResolver(), "three_finger_screenshot_enable", 1);
            if(isThreeFingerScreenShotEnable()){
				enable();
			}else{
				disable();
			}
			*/
        }
    };
    
    private class ConsumeState {
        private boolean mConsumed;

        public ConsumeState() {
            mConsumed = false;
        }

        public void update(boolean newState){
            mConsumed = newState;
            if (mConsumed)
                SystemProperties.set("sys.aurora.input.intercept", "1");
            else
                SystemProperties.set("sys.aurora.input.intercept", "0");

            log("update() " + mConsumed + "  " + SystemProperties.getBoolean("sys.aurora.input.intercept", false));
        }

        public boolean getState() {
            return mConsumed;
        }
    }

    private boolean isNavigationBarHidden() {
        int status = Settings.System.getInt(
                mContext.getContentResolver(),
                NAVI_KEY_HIDE, 0 /*default */);
        return status != 0;
    }
    
    private void reset() {
        log("reset()");
        mDowningFlag = false;
        mConsumeState.update(false);
        mResetLater = false; 
        mKeyguardOn = false;
        mDownEventSent = false; 
        mLastDownEvent = null; 
        mMask = 0;
    }
    
    private void log(String msg) {
    	if (DEBUG) {
    		Log.e(TAG, "auroraPointerInterceptor: " + msg);
    	}
    }
}

#include <platform/mt_i2c.h>     
#include <platform/mt_pmic.h> 
#include "gn_ti_drv2604l_custom.h"



struct mt_i2c_t vibrator_i2c;

static unsigned int drv2604l_i2c_read(char data)
{
    int ret_code = I2C_OK;
	char read_data;
	int len =1;

	vibrator_i2c.id = DRV2604L_I2C_BUS_ID;
	vibrator_i2c.mode = ST_MODE;
	vibrator_i2c.addr = DRV2604L_I2C_ADDR;
	vibrator_i2c.speed = 100;

	read_data = data;
    ret_code = i2c_read(&vibrator_i2c, &read_data ,len);

    if (ret_code != I2C_OK)
    {
		printf("i2c read reg[0x%x] failed.\n",data);
        return ret_code;	
    }

    return read_data;
}

static unsigned int drv2604l_i2c_write(char addr,char data)
{
	unsigned int ret_code = I2C_OK;
   	unsigned char write_data[2];
	int transfer_len = 2;

	vibrator_i2c.id = DRV2604L_I2C_BUS_ID;
	vibrator_i2c.mode = ST_MODE;
	vibrator_i2c.addr = DRV2604L_I2C_ADDR;
	vibrator_i2c.speed = 100;
	
	write_data[0] = addr;
	write_data[1] = data;

	ret_code = i2c_write(&vibrator_i2c, write_data, transfer_len);
	if(ret_code != I2C_OK)
		printf("i2c write reg[0x%x] failed.\n",addr);
    	return ret_code;
}


static int drv2604l_set_bits( unsigned char reg, unsigned char mask, unsigned char val)
{
	int result = 1;
	unsigned char temp = 0;
	
	unsigned char regval = drv2604l_i2c_read(reg);

	temp = regval & ~mask;
	temp |= val & mask;

	if(temp != regval){
		result = drv2604l_i2c_write(reg, temp);
	}

	return result;
}


void gn_lk_vibrate(void)
{    
	printf("%s.\n",__func__);
	
	unsigned int result_tmp;
	char reg_address;
	char reg_data;

    //set gpio
	mt_set_gpio_out(DRV2604L_GPIO_ENABLE_PIN,1);
	mdelay(2);

	drv2604l_i2c_write(MODE_REG,DEV_IDLE);
	//init regs
	drv2604l_i2c_write(RATED_VOLTAGE_REG, 0x30);

  	drv2604l_i2c_write(OVERDRIVE_CLAMP_VOLTAGE_REG,0x53);

	/*drv2604l_set_bits(FEEDBACK_CONTROL_REG,	
						FEEDBACK_CONTROL_DEVICE_TYPE_MASK|FEEDBACK_CONTROL_FB_BRAKE_MASK|FEEDBACK_CONTROL_LOOP_GAIN_MASK,
						FEEDBACK_CONTROL_MODE_LRA|FB_BRAKE_FACTOR|LOOP_GAIN);

	drv2604l_set_bits(Control3_REG,	
						Control3_REG_LOOP_MASK|Control3_REG_FORMAT_MASK,
						BIDIR_INPUT_BIDIRECTIONAL|ERM_OpenLoop_Disable|LRA_OpenLoop_Disable|RTP_FORMAT_SIGNED);
	*/

	drv2604l_i2c_write(REAL_TIME_PLAYBACK_REG, 0x5F);

	drv2604l_i2c_write(MODE_REG,MODE_REAL_TIME_PLAYBACK);
	mdelay(100);

	drv2604l_i2c_write(MODE_REG,DEV_IDLE);

    //set gpio
	mt_set_gpio_out(DRV2604L_GPIO_ENABLE_PIN,0);
}

#ifndef __CUST_USB_H__
#define __CUST_USB_H__

#define CONFIG_USBD_LANG	"0409"

//xiaoqian, 20150409, modified usb vid, begin
//#define USB_VENDORID		(0x0BB4)
#define USB_VENDORID		(0x271D)
//xiaoqian, 20150409, modified usb vid, end
#define USB_PRODUCTID		(0x0C01)
#define USB_VERSIONID		(0x0100)
#define USB_MANUFACTURER	"MediaTek"
#define USB_PRODUCT_NAME	"Android"
#define FASTBOOT_DEVNAME	"mt6752_device"
#define SN_BUF_LEN		19

#endif /* __CUST_USB_H__ */

#include <linux/module.h>
//#include <linux/config.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include <asm/io.h>
#include <linux/input.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/random.h>
#include <asm/uaccess.h>
#include <asm/hardirq.h>
#include <linux/miscdevice.h>

#include <linux/init.h>
#include <asm/irq.h>
#include <linux/moduleparam.h>

#include "gn_des.h"

//static int dbg = 0;

//module_param(dbg, int, 0444);

#define GO_LOCK_IOCTL_MAGIC	'G'
#define GO_LOCK_IOCTL_GET_SEED	_IOW(GO_LOCK_IOCTL_MAGIC, 1, int)
#define GO_LOCK_IOCTL_UNLOCK	_IOW(GO_LOCK_IOCTL_MAGIC, 2, int)

//#define DEBUG_X

//#ifdef DEBUG_X

/*#define dbg(x...) \ 
#define dbg(fmt, args...) \
	do {\
		if(dbg) \
			printk(KERN_INFO "%s: " fmt, __func__, ##args);\
	} while(0)
#else
#define dbg(x...)
#endif
*/

static int rm_locked = 1;

/*
 * return 0 when rom is locked, means can't pass verify
 * return 1 when rom is ourself, means thing going well
 *
 * TODO: it's easy to be hack
 */
int go_rm_verified(void)
{
	//printk("[xxx] %d %s rmlocked:%d \n", __LINE__, __func__, rm_locked);

	return !rm_locked;
}
EXPORT_SYMBOL(go_rm_verified);

static int  go_rm_open(struct inode * inode,struct file * filp)
{
	//dbg(); 
	return 0;
}

static int go_rm_release(struct inode * inode,struct file * filp)
{
	//dbg();
	return 0;
}

static int  go_rm_read(struct file * filp,char * buf,size_t size,loff_t * poff)
{
	//dbg();
	return 0;
}

static int  go_rm_write(struct file * filp,const char * buf,size_t size,loff_t * poff)
{
	//dbg();
	return 0;
}

static unsigned int seed;
static unsigned int key;
static void go_rm_get_seed(void)
{
	key = 0;
	seed = prandom_u32();
	key = encryptInt(seed);
	//if (dbg) printk("[xxx] %u:%u\n", seed, key);

}

//static int  go_rm_ioctl(struct inode * inode,struct file * filp, unsigned int cmd,unsigned long arg)
static long  go_rm_ioctl(struct file * filp, unsigned int cmd,unsigned long arg)
{
	int ret = 0;
	unsigned int tmp_key;

	//dbg();

	if (_IOC_TYPE(cmd) != GO_LOCK_IOCTL_MAGIC)
		return -ENOTTY;

	switch (cmd) {
		case GO_LOCK_IOCTL_GET_SEED:
			go_rm_get_seed();
			if (copy_to_user((void __user *)arg, &seed, sizeof(seed))) {
				//if (dbg) printk("[xxx] %d %s copy to failed\n", __LINE__, __func__);
				ret = -EFAULT;
			}

			break;
		case GO_LOCK_IOCTL_UNLOCK:
			if (copy_from_user(&tmp_key, (void __user *)arg, sizeof (tmp_key))) {
				//if (dbg) printk("[xxx] %d %s copy from failed\n", __LINE__, __func__);
				ret = -EFAULT;
			}
			//if (dbg) printk("[xxx] %u\n", tmp_key);
			if (key == tmp_key) {
				rm_locked = 0;

				//if (dbg)
				//	printk("[xxx] rm is released\n");
			}

			break;
		default:
			break;
	}

	return ret;
}


static struct file_operations go_rm_fops={
	.owner 		= THIS_MODULE,
	.open		= go_rm_open,
	.release	= go_rm_release,
	.write		= go_rm_write,
	.read		= go_rm_read,
	.unlocked_ioctl	= go_rm_ioctl,
};

static struct miscdevice go_rm_device=
{
	.minor 	= MISC_DYNAMIC_MINOR,
	.name	= "x_misc",
	.fops	= &go_rm_fops,
};

//static long status;
static ssize_t go_rm_get_status_value(struct device *dev, struct device_attribute *attr, char *buf)
{
	//dbg();
	//dev_dbg(go_rm_device.this_device, "%s status = %ld\n", __func__, status);
	//return sprintf(buf, "%ld\n", status);
	return sprintf(buf, "%d\n", rm_locked);
}

/*
static ssize_t go_rm_set_status_value(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int ret;
	dbg("count : %zu\n", count);
	ret = strict_strtoul(buf, 10, &status);

	//door for tmp debug 
	if (dbg) {
		if (status == 1866)
			rm_locked = 0;
	}

	return count;
}
*/

static DEVICE_ATTR(status, S_IRUGO | S_IWUSR, go_rm_get_status_value, NULL);
//static DEVICE_ATTR(status, S_IRUGO | S_IWUSR, go_rm_get_status_value, go_rm_set_status_value);

static struct attribute *go_rm_attr[] = {
	&dev_attr_status.attr,
	NULL
};

static struct attribute_group go_rm_attr_gp = {
	.name = "x_misc",
	.attrs = go_rm_attr,
};

static int go_rm_init(void)
{
	int ret;

	//dbg();
	ret = misc_register(&go_rm_device);
	if(ret)
	{
		printk("[xxx] Error to register misc device\n");
		return -EINVAL;
	}

	ret = sysfs_create_group(&go_rm_device.this_device->kobj, &go_rm_attr_gp);
	if (ret)
		return -EINVAL;

	return 0;
}

static void go_rm_exit(void)
{
	//dbg();
	sysfs_remove_group(&go_rm_device.this_device->kobj, &go_rm_attr_gp);
	misc_deregister(&go_rm_device);
}

module_init(go_rm_init);
module_exit(go_rm_exit);
MODULE_AUTHOR("Go...");
MODULE_DESCRIPTION("XRXMX");
MODULE_LICENSE("GPL");

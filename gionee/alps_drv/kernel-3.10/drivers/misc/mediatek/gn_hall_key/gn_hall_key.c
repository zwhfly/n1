/*
 * Copyright (C) 2010 MediaTek, Inc.
 *
 * Author: Terry Chang <terry.chang@mediatek.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */


/*kpd.h file path: ALPS/mediatek/kernel/include/linux */
#include <linux/kpd.h>

#define HALL_KEY_NAME	"gn_hall_key"

#include <linux/switch.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/rtpm_prio.h>
#include <cust_eint.h>
#include <cust_eint_md1.h>
#include <cust_gpio_usage.h>
#include <mach/eint.h>
#include <mach/mt_gpio.h>

#define OPEN_HALL_KEY	_IO('k', 32)
#define CLOSE_HALL_KEY	_IO('k', 33)

#define HALL_TAG "gn_hall_key"

#define GN_HALL_KEY_OPEN 202
#define GN_HALL_KEY_CLOSE 203

enum hall_state
{
    HALL_CLOSE = 0,
    HALL_OPEN = 1
};

enum hall_key
{
    key_close = 0,
    key_open = 1
};

struct input_dev *hkey_input_dev;
static struct switch_dev hall_switch_dev;

static int key_on = key_close;
static void gn_hall_eint_handler(void );
static int  gn_hall_flag = 0;
struct task_struct *gn_thread_hall = NULL;
static DECLARE_WAIT_QUEUE_HEAD(waiter_hall);

//Gionee BSP1 yaoyc 20150211 add for CR01444290 begin
#ifdef CONFIG_TOUCHSCREEN_SYNAPTICS_S3508
extern void synaptics_rmi4_hall_key_callback(int state);
//Gionee BSP1 yaoyc 20150706 add for CR01510590 begin
#elif defined(CONFIG_TOUCHSCREEN_MTK_GT1151)
extern void gt1x_hall_key_callback(int state);
//Gionee BSP1 yaoyc 20150706 add for CR01510590 end
#endif
//Gionee BSP1 yaoyc 20150211 add for CR01444290 end

/*********************************************************************/
static ssize_t hall_show_state(struct device *dev,	struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", key_on);
}

static ssize_t hall_store_state(struct device *dev, struct device_attribute *attr, const char *buf, ssize_t size)
{
	if (buf != NULL && size != 0) {
		if (buf[0] == '0') {
			key_on = key_close;
			switch_set_state((struct switch_dev *)&hall_switch_dev, HALL_OPEN);
		} else {
			key_on = key_open;
		}
	}
	return size;
}
static DEVICE_ATTR(state, 0664, hall_show_state, hall_store_state);

void gn_hall_eint_handler(void)
{
	u8 kpd_hallkey_state;
	u8 old_state;
	struct sched_param param = { .sched_priority = RTPM_PRIO_TPD };
    sched_setscheduler(current, SCHED_RR, &param);

	do{
	    set_current_state(TASK_INTERRUPTIBLE);

        wait_event_interruptible(waiter_hall,gn_hall_flag!=0);

        gn_hall_flag = 0;


        set_current_state(TASK_RUNNING);
	    kpd_hallkey_state = mt_get_gpio_in(GPIO_MHALL_EINT_PIN);
	    old_state = !kpd_hallkey_state;

	    // printk( "%s: kpd_hallkey_state = %d\n",__func__,kpd_hallkey_state);

		if(key_on == key_open)
		{
		    if(kpd_hallkey_state == 1) //hall open
		    {
		    	input_report_key(hkey_input_dev, GN_HALL_KEY_OPEN, 1);
		    	input_report_key(hkey_input_dev, GN_HALL_KEY_OPEN, 0);
				input_sync(hkey_input_dev);
		    	switch_set_state((struct switch_dev *)&hall_switch_dev, HALL_OPEN);
		    }
		    else	//hall close
		    {
		    	input_report_key(hkey_input_dev, GN_HALL_KEY_CLOSE, 1);
	    		input_report_key(hkey_input_dev, GN_HALL_KEY_CLOSE, 0);
				input_sync(hkey_input_dev);
	    		switch_set_state((struct switch_dev *)&hall_switch_dev, HALL_CLOSE);
	    	}
		}
		//Gionee BSP1 yaoyc 20150211 add for CR01444290 begin
#ifdef CONFIG_TOUCHSCREEN_SYNAPTICS_S3508
		synaptics_rmi4_hall_key_callback(kpd_hallkey_state);
#elif defined(CONFIG_TOUCHSCREEN_MTK_GT1151)
		gt1x_hall_key_callback(kpd_hallkey_state);
#endif
		//Gionee BSP1 yaoyc 20150211 add for CR01444290 end
	    // for detecting the return to old_state
    	mt_eint_set_polarity(CUST_EINT_MHALL_NUM, old_state);
    	mt_eint_unmask(CUST_EINT_MHALL_NUM);
	}while(!kthread_should_stop());
}

static void kpd_hall_eint_handler(void)
{
	mt_eint_mask(CUST_EINT_MHALL_NUM);
	gn_hall_flag = 1;
	wake_up_interruptible(&waiter_hall);
}

/*****************************************************************************************/

static int hall_key_probe(struct platform_device *pdev)
{
	int error;

	hkey_input_dev = input_allocate_device();

	hkey_input_dev->name = HALL_KEY_NAME;


	error = input_register_device(hkey_input_dev);
	if (error) {
		printk(HALL_TAG "register input device failed (%d)\n",error);
		input_free_device(hkey_input_dev);
		return error;
	}

	__set_bit(EV_KEY, hkey_input_dev->evbit);

	__set_bit(GN_HALL_KEY_OPEN, hkey_input_dev->keybit);
	__set_bit(GN_HALL_KEY_CLOSE, hkey_input_dev->keybit);

    hall_switch_dev.name = HALL_KEY_NAME;
    hall_switch_dev.index = 0;
    hall_switch_dev.state = HALL_OPEN;

    error = switch_dev_register(&hall_switch_dev);
    if(error)
	{
		printk(HALL_TAG "register switch failed (%d)\n", error);
		input_unregister_device(hkey_input_dev);
		input_free_device(hkey_input_dev);
		return error;
	}

	key_on = key_open;

	/*configure to GPIO function, external interrupt*/

	mt_set_gpio_mode(GPIO_MHALL_EINT_PIN, GPIO_MHALL_EINT_PIN_M_EINT);
    mt_set_gpio_dir(GPIO_MHALL_EINT_PIN, GPIO_DIR_IN);
	mt_set_gpio_pull_enable(GPIO_MHALL_EINT_PIN, GPIO_PULL_ENABLE);
	mt_set_gpio_pull_select(GPIO_MHALL_EINT_PIN, GPIO_PULL_UP);

	mt_eint_set_sens(CUST_EINT_MHALL_NUM, CUST_EINT_LEVEL_SENSITIVE);
	mt_eint_set_hw_debounce(CUST_EINT_MHALL_NUM, CUST_EINT_MHALL_DEBOUNCE_CN);

	gn_thread_hall = kthread_run(gn_hall_eint_handler, 0, hall_switch_dev.name);
    if (IS_ERR(gn_thread_hall)) {
		printk(HALL_TAG "register kthread_run failed (%d)\n", error);
		free_irq(MT_KP_IRQ_ID,NULL);
		switch_dev_unregister(&hall_switch_dev);
		input_unregister_device(hkey_input_dev);
		input_free_device(hkey_input_dev);
		return error;
    }

	switch_set_state((struct switch_dev *)&hall_switch_dev, HALL_CLOSE);
	mt_eint_registration(CUST_EINT_MHALL_NUM, CUST_EINT_MHALL_TYPE, kpd_hall_eint_handler, 0);
	mt_eint_unmask(CUST_EINT_MHALL_NUM);

	return 0;
}
static int hall_key_remove(struct platform_device *pdev)
{
	return 0;
}

static struct platform_device hall_key_pdev = {
	.name	   = HALL_KEY_NAME,
	.id	       = -1,
};

static struct platform_driver hall_key_pdrv = {
	.probe = hall_key_probe,
	.remove = hall_key_remove,
	.driver = {
		.name = HALL_KEY_NAME,
		.owner = THIS_MODULE,

	},
};

static int __init hall_key_mod_init(void)
{
	int error;
	error = platform_device_register(&hall_key_pdev);
	if (error) {
		printk(HALL_TAG "register device failed (%d)\n", error);
		return error;
	}

	error = device_create_file(&hall_key_pdev.dev, &dev_attr_state);
	if (error)
	{
		platform_device_unregister(&hall_key_pdev);
		printk(HALL_TAG "gn_hall_key_on_attr failed(%d)\n",error);
	}

	error = platform_driver_register(&hall_key_pdrv);
	if (error) {
		sysfs_remove_file(&hall_key_pdev.dev, &dev_attr_state);
		platform_device_unregister(&hall_key_pdev);
		printk(HALL_TAG "register driver failed (%d)\n", error);
		return error;
	}

	return 0;
}

/* should never be called */
static void __exit hall_key_mod_exit(void)
{
}
late_initcall(hall_key_mod_init);
module_exit(hall_key_mod_exit);

MODULE_AUTHOR("yangqb@gionee.com");
MODULE_DESCRIPTION("Gionee Hall Key v1.1");
MODULE_LICENSE("GPL");

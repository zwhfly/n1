/*
 *  apds9960.c - Linux kernel modules for Gesture + RGB + ambient light + proximity sensor
 *
 *  Copyright (C) 2013 Lee Kai Koon <kai-koon.lee@avagotech.com>
 *  Copyright (C) 2013 Avago Technologies
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/mutex.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/ioctl.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/time.h>
#include <linux/earlysuspend.h>
#include <linux/platform_device.h>
#include <linux/hwmsensor.h>
#include <linux/hwmsen_dev.h>
#include <linux/sensors_io.h>
#include <mach/mt_gpio.h>
#include <cust_eint.h>
#include <cust_alsps_apds9960.h>
#include <mach/mt_typedefs.h>
#include <mach/mt_pm_ldo.h>
#include <linux/kthread.h>
#include <linux/dma-mapping.h>
#include "gn_avago_apds9960.h"
// Gionee BSP1 chengx 20140826 modify for CR01371160 begin
#ifdef GN_MTK_BSP_DEVICECHECK
#include <linux/gn_device_check.h>
extern int gn_set_device_info(struct gn_device_info gn_dev_info);
#endif
// Gionee BSP1 chengx 20140826 modify for CR01371160 end

#define POWER_NONE_MACRO MT65XX_POWER_NONE
extern void mt_eint_unmask(unsigned int line);
extern void mt_eint_mask(unsigned int line);
extern void mt_eint_set_polarity(kal_uint8 eintno, kal_bool ACT_Polarity);
extern void mt_eint_set_hw_debounce(kal_uint8 eintno, kal_uint32 ms);
extern kal_uint32 mt_eint_set_sens(kal_uint8 eintno, kal_bool sens);
extern void mt_eint_registration(unsigned int eint_num, unsigned int flow, void (EINT_FUNC_PTR)(void), unsigned int is_auto_umask);
extern struct alsps_hw *get_cust_alsps_hw(void);

/************************************************
  Change history

  Ver		When			Who		Why
  ---		----			---		---
  1.0.0	19-Aug-2013		KK		Initial draft
  1.0.1	26-Aug-2013		KK		Revise gesture algorithm
  1.0.2	29-Aug-2013		KK		Change GTHR_IN and GTHR_OUT
  1.0.3	03-Sep-2013		KK		Correct divide by zero error in AveragingRawData()
  1.0.4	05-Sep-2013		KK		Accept old and latest ID value
  1.0.5	17-Sep-2013		KK		Return if sample size is less than or equal to 4 in GestureDataProcessing();
  Correct error in AveragingRawData()
  1.0.6	27-Sep-2013		KK		Simplify GestureDataProcessing() and revise Gesture Calibration
  Added Up/Down/Left/Right U-Turn gesture detection
 ************************************************/

/*****************************************************************************/
#define APS_TAG                 "[ALS/PS] "
#define APDS9960_DEV_NAME       "APDS9960"

//#define APDS9960_DEBUG
#if defined(APDS9960_DEBUG)
#define APS_FUN(f)              printk(KERN_INFO APS_TAG"%s\n", __FUNCTION__)
#define APS_ERR(fmt, args...)   printk(KERN_ERR  APS_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define APS_LOG(fmt, args...)   printk(KERN_INFO APS_TAG fmt, ##args)
#define APS_DBG(fmt, args...)   printk(KERN_INFO  APS_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#else
#define APS_FUN(f)              printk(KERN_INFO APS_TAG"%s\n", __FUNCTION__)
#define APS_ERR(fmt, args...)   printk(KERN_ERR  APS_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define APS_LOG(fmt, args...)   printk(KERN_INFO APS_TAG fmt, ##args)
#define APS_DBG(fmt, args...)
#endif

typedef enum
{
    CMC_BIT_ALS    = 1,
    CMC_BIT_PS     = 2,
} CMC_BIT;

/*****************************************************************************/
// Global data
static struct i2c_board_info __initdata i2c_apds9960 = {I2C_BOARD_INFO(APDS9960_DEV_NAME, APDS9960_I2C_SLAVE_ADDR)};

#if defined(CONFIG_OF)
static const struct of_device_id apds9960_of_match[] = {
    {.compatible = "mediatek,als_ps",},
    {},
};
#endif

static int apds9960_probe(struct platform_device *pdev);
static int apds9960_remove(struct platform_device *pdev);
static struct platform_driver apds9960_alsps_driver = {
    .probe      = apds9960_probe,
    .remove     = apds9960_remove,    
    .driver     = {
        .name       = "als_ps",
#if defined(CONFIG_OF)
        .of_match_table = apds9960_of_match,
#endif
    }
};

static const struct i2c_device_id apds9960_i2c_id[] = {{APDS9960_DEV_NAME,0},{}};
static int  apds9960_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int  apds9960_i2c_remove(struct i2c_client *client);
static int apds9960_i2c_detect(struct i2c_client *client, struct i2c_board_info *info);

#if !defined(CONFIG_HAS_EARLYSUSPEND)
static int apds9960_i2c_suspend(struct i2c_client *client, pm_message_t mesg);
static int apds9960_i2c_resume(struct i2c_client *client);
#else
static void apds9960_early_suspend(struct early_suspend *h);
static void apds9960_late_resume(struct early_suspend *h);
#endif

static struct i2c_driver apds9960_i2c_driver = {	
    .probe      = apds9960_i2c_probe,
    .remove     = apds9960_i2c_remove,
    .detect     = apds9960_i2c_detect,
#if !defined(CONFIG_HAS_EARLYSUSPEND)
    .suspend    = apds9960_i2c_suspend,
    .resume     = apds9960_i2c_resume,
#endif
    .id_table   = apds9960_i2c_id,
    .driver = {
        //.owner          = THIS_MODULE,
        .name           = APDS9960_DEV_NAME,
    },
};

static struct workqueue_struct *apds_workqueue;
static struct i2c_client *apds9960_i2c_client;
struct apds9960_data *apds9960_obj = NULL;

int apds9960_setup_eint(struct i2c_client *client);
static int apds9960_get_ps_value(struct apds9960_data *obj, unsigned int ps);

/*****************************************************/
static unsigned char apds9960_als_atime_tb[] = { 0xF6, 0xEB, 0xD6 };
static unsigned short apds9960_als_integration_tb[] = {2400,5040, 10080}; // DO NOT use beyond 100.8ms
static unsigned short apds9960_als_res_tb[] = { 10240, 21504, 43008 };
static unsigned char apds9960_als_again_tb[] = { 1, 4, 16, 64 };
static unsigned char apds9960_als_again_bit_tb[] = { 0x00, 0x01, 0x02, 0x03 };

// Ensure the coefficients do not exceed 9999
static int RGB_COE_X[3] = {-1882, 10240, -8173};    // {-1.8816, 10.24, -8.173};
static int RGB_COE_Y[3] = {-2100, 10130, -7708};    // {-2.0998, 10.13, -7.708};
static int RGB_COE_Z[3] = {-1937, 5201, -2435};     // {-1.937, 5.201, -2.435};

static int RGB_CIE_N1 = 332;    // 0.332;
static int RGB_CIE_N2 = 186;    // 0.1858;

static int RGB_CIE_CCT1 = 449;  // 449.0;
static int RGB_CIE_CCT2 = 3525; // 3525.0;
static int RGB_CIE_CCT3 = 6823; // 6823.3;
static int RGB_CIE_CCT4 = 5520; // 5520.33;

// Gesture data storage
static GESTURE_DATA_TYPE gesture_data;

static int gesture_motion = DIR_NONE;
static int gesture_prev_motion = DIR_NONE;
static int fMotionMapped = 1;  // 0;

int gesture_ud_delta = 0;
int gesture_lr_delta = 0;
int gesture_state	 = 0;

int gesture_ud_count = 0;
int gesture_lr_count = 0;

int gesture_distance = 0;
int gesture_prev_distance = 0;
int gesture_far_near_flag = 0;
int gesture_far_near_started = 0;

int gesture_fundamentals = 0;	// 0 = fundamentals, 1 = extra

unsigned int ps_data;			// to store PS data
unsigned int last_ps_data;      // to store previous pdata for gesture

// Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
//static unsigned int xtalk_pdata[10];
//static int xtalk_pdata_index = 0;
// Gionee BSP1 yang_yang 20140814 modify for CR01357229 end

//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
bool need_incall_cali = false;   
bool is_hand_answer = false;
static DECLARE_WAIT_QUEUE_HEAD(psensor_cali_head_wq);
static bool bool_cali_flag = false;
#endif

//Gionee BSP chengx 20141122 modify for CR01406932 begin
static int overtake_count=0;
//Gionee BSP chengx 20141122 modify for CR01406932 end

#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
static int apds9960_dynamic_calibrate(void);
#endif
//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end

int FilterGestureRawData(GESTURE_DATA_TYPE *, GESTURE_DATA_TYPE *);
int GestureDataProcessing(void);
int DecodeGesture(int gesture_mode);
void ResetGestureParameters(void); 
static int apds9960_runtime_calibration(struct i2c_client *client);
static int apds9960_enable_gesture_sensor(struct i2c_client *client, int val);

// Gionee BSP1 yang_yang 20140802 modify for CR01341808 begin
#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
struct PS_CALI_DATA_STRUCT
{
    int close;
    int far_away;
    int valid;
};
#endif
// Gionee BSP1 yang_yang 20140802 modify for CR01341808 end

/*
 * Management functions
 */
#define _DMA_RW_MODE_
#if defined(_DMA_RW_MODE_)
static uint8_t *g_pDMABuf_va = NULL;
static uint32_t *g_pDMABuf_pa = NULL;

static void dma_buffer_alloct()
{	
    g_pDMABuf_va = (u8 *)dma_alloc_coherent(NULL, 4096, &g_pDMABuf_pa, GFP_KERNEL);
    if (!g_pDMABuf_va)
    {
        APS_DBG("[DMA][Error] Allocate DMA I2C Buffer failed!\n");    
    }
}

static void dma_buffer_release()
{
    if (g_pDMABuf_va)
    {
        dma_free_coherent(NULL, 1024, g_pDMABuf_va, g_pDMABuf_pa);  
        g_pDMABuf_va = NULL;
        g_pDMABuf_pa = NULL;
        APS_DBG("[DMA][release] Allocate DMA I2C Buffer release!\n");
    }
}
#endif

#define I2C_MASTER_CLOCK    100
static void I2C_dma_write_Readdata(u8 cmd, u8* data, u16 size)
{    
    int err;
    struct i2c_msg msg[2];

#if defined(_DMA_RW_MODE_)
    if (!g_pDMABuf_va)		
    {
        APS_ERR("g_pDMABuf_va is NULL!\n");
	    return;
    }
#endif		

    msg[0].addr = apds9960_i2c_client->addr;
    msg[0].flags = 0;
    msg[0].len = 1;
    msg[0].buf = &cmd;
    msg[0].ext_flag = apds9960_i2c_client->ext_flag;
    msg[0].timing = I2C_MASTER_CLOCK;
	
#if defined(_DMA_RW_MODE_)
    msg[1].addr = apds9960_i2c_client->addr & I2C_MASK_FLAG | I2C_DMA_FLAG,			
    msg[1].buf = g_pDMABuf_pa,			
#else
    msg[1].addr = apds9960_i2c_client->addr;
    msg[1].buf =data;
#endif
    msg[1].flags = I2C_M_RD;
    msg[1].len = size;
    msg[1].ext_flag = apds9960_i2c_client->ext_flag;
    msg[1].timing = I2C_MASTER_CLOCK;

    err = i2c_transfer(apds9960_i2c_client->adapter, msg, 2);	
    if (err < 0)
    {
        APS_ERR("I2C_dma_write_Readdata error %d,addr = %d\n", err,apds9960_i2c_client->addr);
    }
#if defined(_DMA_RW_MODE_)
    else
    {
        memcpy(data, g_pDMABuf_va, size);
    }
#endif
}

/*----------------------------------------------------------------------------*/
static int apds9960_clear_interrupt(struct i2c_client *client, int command)
{
    int ret;

    ret = i2c_smbus_write_byte(client, command);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_enable(struct i2c_client *client, int enable)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_ENABLE_REG, enable);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }
    data->enable = enable;

    return ret;
}

static int apds9960_set_atime(struct i2c_client *client, int atime)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_ATIME_REG, atime);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_wtime(struct i2c_client *client, int wtime)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_WTIME_REG, wtime);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_ailt(struct i2c_client *client, int threshold)
{
    int ret;

    ret = i2c_smbus_write_word_data(client, APDS9960_AILTL_REG, threshold);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_aiht(struct i2c_client *client, int threshold)
{
    int ret;

    ret = i2c_smbus_write_word_data(client, APDS9960_AIHTL_REG, threshold);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_pilt(struct i2c_client *client, int threshold)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_PITLO_REG, threshold);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_piht(struct i2c_client *client, int threshold)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_PITHI_REG, threshold);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_pers(struct i2c_client *client, int pers)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_PERS_REG, pers);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_config(struct i2c_client *client, int config)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_CONFIG_REG, config);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_ppulse(struct i2c_client *client, int ppulse)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_PPULSE_REG, ppulse);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_control(struct i2c_client *client, int control)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_CONTROL_REG, control);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_aux(struct i2c_client *client, int aux)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_AUX_REG, aux);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_poffset_ur(struct i2c_client *client, int poffset_ur)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_POFFSET_UR_REG, poffset_ur);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }
    data->ps_poffset_ur = poffset_ur;

    return ret;
}

static int apds9960_set_poffset_dl(struct i2c_client *client, int poffset_dl)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_POFFSET_DL_REG, poffset_dl);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }
    data->ps_poffset_dl = poffset_dl;

    return ret;
}

/****************** Gesture related registers ************************/
static int apds9960_set_config2(struct i2c_client *client, int config2)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_CONFIG2_REG, config2);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gthr_in(struct i2c_client *client, int gthr_in)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GTHR_IN_REG, gthr_in);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gthr_out(struct i2c_client *client, int gthr_out)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GTHR_OUT_REG, gthr_out);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gconf1(struct i2c_client *client, int gconf1)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GCONF1_REG, gconf1);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gconf2(struct i2c_client *client, int gconf2)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GCONF2_REG, gconf2);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_goffset_u(struct i2c_client *client, int goffset_u)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GOFFSET_U_REG, goffset_u);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_goffset_d(struct i2c_client *client, int goffset_d)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GOFFSET_D_REG, goffset_d);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gpulse(struct i2c_client *client, int gpulse)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GPULSE_REG, gpulse);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_goffset_l(struct i2c_client *client, int goffset_l)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GOFFSET_L_REG, goffset_l);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_goffset_r(struct i2c_client *client, int goffset_r)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GOFFSET_R_REG, goffset_r);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gconf3(struct i2c_client *client, int gconf3)
{
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GCONF3_REG, gconf3);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }

    return ret;
}

static int apds9960_set_gctrl(struct i2c_client *client, int gctrl)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;

    ret = i2c_smbus_write_byte_data(client, APDS9960_GCTRL_REG, gctrl);
    if (ret)
    {
        APS_ERR("%s failed!\n", __func__);
    }
    data->gctrl = gctrl;

    return ret;
}

/*********************************************************************/
static int LuxCalculation(struct i2c_client *client)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int X1, Y1, Z1;
    int x1, y1, z1;
    int n;
    unsigned int cct;
    int sum_XYZ=0;

    APS_DBG("phase 0 :: cdata = %d, rdata = %d, gdata = %d, bdata = %d\n", data->cdata, data->rdata, data->gdata, data->bdata);
    APS_DBG("phase 0.5 :: atime = %d, again = %d\n", apds9960_als_integration_tb[data->als_atime_index],
            apds9960_als_again_tb[data->als_again_index]);

    X1 = (data->RGB_COE_X[0]*data->rdata) + (data->RGB_COE_X[1]*data->gdata) + (data->RGB_COE_X[2]*data->bdata);
    Y1 = (data->RGB_COE_Y[0]*data->rdata) + (data->RGB_COE_Y[1]*data->gdata) + (data->RGB_COE_Y[2]*data->bdata);
    Z1 = (data->RGB_COE_Z[0]*data->rdata) + (data->RGB_COE_Z[1]*data->gdata) + (data->RGB_COE_Z[2]*data->bdata);

    if ((X1 == 0) && (Y1 == 0) && (Z1 == 0))
    {
        x1 = y1 = z1 = 0;
    }
    else
    {	
        sum_XYZ = (X1 + Y1 + Z1)/1000;	// scale down
        if (sum_XYZ > 0)
        {
            if (((X1+Y1+Z1)%1000) >= 500)
                sum_XYZ++;	
        }
        else
        {
            if (((X1+Y1+Z1)%1000) <= -500)
                sum_XYZ--;
        }

        x1 = X1/sum_XYZ;
        y1 = Y1/sum_XYZ;
        z1 = Z1/sum_XYZ;
    }

    if (data->cdata > 10)
    {
        n = ((x1 - RGB_CIE_N1)*1000)/(RGB_CIE_N2 - y1);
        cct = (((RGB_CIE_CCT1*(n*n*n))/1000000000) + ((RGB_CIE_CCT2*(n*n))/1000000) + ((RGB_CIE_CCT3*n)/1000) + RGB_CIE_CCT4);
    }
    else
    {
        n = 0;
        cct = 0;
    }

    data->lux = (data->cdata*10080)/(apds9960_als_integration_tb[data->als_atime_index]*apds9960_als_again_tb[data->als_again_index]);
    data->cct = cct;

    if (data->cdata > 0)
    {
        if(((data->rdata * 100)/data->cdata) >= 70)     // Incandescent 2600K
        {
            data->cct=(data->cct * APDS9960_CCT_GA2)/1000;
            data->lux=(data->lux * APDS9960_LUX_GA2)/1000;
        }
        else if (((data->rdata * 100)/data->cdata) >= 45)   // Fluorescent Warm White 2700K
        {
            data->cct=(data->cct * APDS9960_CCT_GA3)/1000;
            data->lux=(data->lux * APDS9960_LUX_GA3)/1000;
        }
        else if (((data->rdata * 100)/data->cdata) >= 35)   // Fluorescent Warm White 2700K
        {
            data->cct=(data->cct * APDS9960_CCT_GA4)/1000;
            data->lux=(data->lux * APDS9960_LUX_GA4)/1000;
        }
        else    // Fluorescent Daylight 6500K
        {
            data->cct=(data->cct * APDS9960_CCT_GA1)/1000;
            data->lux=(data->lux * APDS9960_LUX_GA1)/1000;
        }
    }

    APS_DBG("%s phase 4 :: cct = %d, data->cct = %d, data->lux = %d\n", __func__, cct, data->cct, data->lux);

    return SUCCESS;
}

// Gionee BSP1 yang_yang 20140802 modify for CR01341808 begin
#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
static void apds9960_WriteCalibration(struct PS_CALI_DATA_STRUCT *data_cali)
{
    APS_DBG("apds9930_WriteCalibration  %d,%d,%d\n",data_cali->close,data_cali->far_away,data_cali->valid);
    if (NULL == apds9960_obj)
    {
        APS_ERR("apds9960_obj is NULL!\n");
        return;
    }

    if (CALIBRATION_DATA_VALID == data_cali->valid)
    {
        apds9960_obj->ps_threshold_low = data_cali->far_away;
        apds9960_obj->ps_threshold_high = data_cali->close;
        apds9960_set_piht(apds9960_obj->client, apds9960_obj->ps_threshold_low);
        apds9960_set_pilt(apds9960_obj->client, apds9960_obj->ps_threshold_high);
    }
}
#endif
// Gionee BSP1 yang_yang 20140802 modify for CR01341808 end

static void apds9960_change_ps_threshold(struct i2c_client *client)
{
    struct apds9960_data *obj = i2c_get_clientdata(client);
    hwm_sensor_data sensor_data;
	int idx_table = 0;
	int temp_noise = 0;
    int err;

    APS_FUN();
    obj->ps_data =	i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
    sensor_data.values[0] = apds9960_get_ps_value(obj, obj->ps_data);
    if (CLOSE == sensor_data.values[0])
    {
        apds9960_set_pilt(client, obj->ps_threshold_low);
        apds9960_set_piht(client, APDS9960_THRESHOLD_MAX);
        APS_LOG("CLOSE\n");
    }
    else if (FAR == sensor_data.values[0])
    {
#ifdef GN_MTK_BSP_PS_DYNAMIC_CALIBRATION
		if ((obj->cali_noise > 20) && (obj->ps_data < (obj->cali_noise - 20)))
        {
			obj->cali_noise = obj->ps_data;
			for (idx_table = 0; idx_table < obj->ps_cali_noise_num; idx_table++)
            {
				if (obj->ps_data <= obj->ps_cali_noise[idx_table])
					break;
			}

			if (idx_table >= obj->ps_cali_noise_num)
            {
				APS_ERR("%s: the cali_offset_table is error", __func__);
			}
            else
            {
			    obj->ps_threshold_high = obj->ps_cali_offset_high[idx_table] + obj->ps_data;
			    obj->ps_threshold_low = obj->ps_cali_offset_low[idx_table] + obj->ps_data;
			    if (obj->ps_threshold_high >= APDS9960_THRESHOLD_MAX)
				    obj->ps_threshold_high = APDS9960_THRESHOLD_MAX;
			    if (obj->ps_threshold_low >= APDS9960_THRESHOLD_MAX)
				    obj->ps_threshold_low = APDS9960_THRESHOLD_MAX;
            }
		}

		if (obj->ps_data >= 20)
        {
			temp_noise = obj->ps_data - 20; 
		}
        else
        {
			temp_noise = APDS9960_THRESHOLD_MIN;
		}
		APS_DBG("cali_noise: %u, temp_noise:%u, high:%u, low:%u\n", obj->cali_noise, temp_noise, obj->ps_threshold_high, obj->ps_threshold_low);
        apds9960_set_pilt(client, temp_noise);
        apds9960_set_piht(client, obj->ps_threshold_high);
#else
        apds9960_set_pilt(client, APDS9960_THRESHOLD_MIN);
        apds9960_set_piht(client, obj->ps_threshold_high);
#endif
        APS_LOG("FAR\n");
    }
    else
    {
        APS_ERR("valuse is ERR!\n");
        return;
    }

    sensor_data.value_divide = 1;
    sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
    if ((err = hwmsen_get_interrupt_data(ID_PROXIMITY, &sensor_data)))
    {
        APS_ERR("call hwmsen_get_interrupt_data fail = %d\n", err);
    }
}

static void apds9960_change_als_threshold(struct i2c_client *client)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    unsigned char change_again=0;
    unsigned char control_data=0;
    unsigned char i2c_data[16];
    int status;

    APS_FUN();
    status = i2c_smbus_read_i2c_block_data(client, APDS9960_CDATAL_REG, 8, (unsigned char*)i2c_data);
    if ((status < 0) || (status != 8))
    {
        APS_ERR("I2C APDS9960_CDATAL_REG failed!\n");
        return;
    }

    data->cdata = (i2c_data[1]<<8)|i2c_data[0];
    data->rdata = (i2c_data[3]<<8)|i2c_data[2];
    data->gdata = (i2c_data[5]<<8)|i2c_data[4];
    data->bdata = (i2c_data[7]<<8)|i2c_data[6];

    LuxCalculation(client);

    if (data->lux >= 0)
    {
        data->lux = data->lux<30000 ? data->lux : 30000;
    }

    if (data->cct >= 0)
    {
        data->cct = data->cct<10000 ? data->cct : 10000;
    }

    APS_DBG("cct=%d, lux=%d cdata=%d rdata=%d gdata=%d bdata=%d atime=%x again=%d\n", 
        data->cct, data->lux, data->cdata, data->rdata, data->gdata, 
        data->bdata, apds9960_als_atime_tb[data->als_atime_index], apds9960_als_again_tb[data->als_again_index]);
	
    data->als_data = data->cdata;

    data->als_low_threshold = (data->als_data * (100-APDS9960_ALS_THRESHOLD_HSYTERESIS) ) /100;
    data->als_high_threshold = (data->als_data * (100+APDS9960_ALS_THRESHOLD_HSYTERESIS) ) /100;

    if (data->als_high_threshold >= apds9960_als_res_tb[data->als_atime_index])
    {
        data->als_high_threshold = apds9960_als_res_tb[data->als_atime_index];
    }

    if (data->als_data >= (apds9960_als_res_tb[data->als_atime_index]*99)/100)
    {
        // lower AGAIN if possible
        if (APDS9960_ALS_GAIN_1X != data->als_again_index)
        {
            data->als_again_index--;
            change_again = 1;
		}
    }
    else if (data->als_data <= (apds9960_als_res_tb[data->als_atime_index]*1)/100)
    {
        // increase AGAIN if possible
        if (APDS9960_ALS_GAIN_64X != data->als_again_index)
        {
            data->als_again_index++;
            change_again = 1;
		}
	}

    if (change_again)
    {
        control_data = i2c_smbus_read_byte_data(client, APDS9960_CONTROL_REG);
        control_data = control_data & 0xFC;

        control_data = control_data | apds9960_als_again_bit_tb[data->als_again_index];
        i2c_smbus_write_byte_data(client, APDS9960_CONTROL_REG, control_data);
    }

    i2c_smbus_write_word_data(client, APDS9960_AILTL_REG, data->als_low_threshold);
    i2c_smbus_write_word_data(client, APDS9960_AIHTL_REG, data->als_high_threshold);
}

void ResetGestureParameters()
{
    gesture_data.index = 0;
    gesture_data.total_gestures = NO_VALID_GESTURE;

    gesture_ud_delta = 0;
    gesture_lr_delta = 0;
    gesture_state = 0;
    gesture_ud_count = 0;
    gesture_lr_count = 0;
    gesture_motion = DIR_NONE;
    gesture_prev_motion = DIR_NONE;
    gesture_distance = 0;
    gesture_prev_distance = 0;
    gesture_far_near_flag = 0;
    gesture_far_near_started = 0;

    last_ps_data = 0;
}

int FilterGestureRawData(GESTURE_DATA_TYPE *gesture_in_data, GESTURE_DATA_TYPE *gesture_out_data)
{
    int i;

    if (gesture_in_data->total_gestures > MAX_GESTURE || gesture_in_data->total_gestures <= NO_VALID_GESTURE)
    {
        APS_ERR("total_gestures>32 or total_gestures<=0\n");
        return -EINVAL;
    }

    gesture_out_data->total_gestures = NO_VALID_GESTURE;

    for (i = 0; i < gesture_in_data->total_gestures; i++)
    {
        if ((gesture_in_data->u_data[i] > gesture_in_data->out_threshold) &&
            (gesture_in_data->d_data[i] > gesture_in_data->out_threshold) &&
            (gesture_in_data->l_data[i] > gesture_in_data->out_threshold) &&
            (gesture_in_data->r_data[i] > gesture_in_data->out_threshold))
        {
            gesture_out_data->u_data[gesture_out_data->total_gestures] = gesture_in_data->u_data[i];
            gesture_out_data->d_data[gesture_out_data->total_gestures] = gesture_in_data->d_data[i];
            gesture_out_data->l_data[gesture_out_data->total_gestures] = gesture_in_data->l_data[i];
            gesture_out_data->r_data[gesture_out_data->total_gestures] = gesture_in_data->r_data[i];

            gesture_out_data->total_gestures++;
        }
    }

    if (NO_VALID_GESTURE == gesture_out_data->total_gestures)
    {
        APS_ERR("total_gestures==0\n");
        return -EINVAL;
    }
	
    for (i = 1; i < gesture_out_data->total_gestures-1; i++)
    {
        gesture_out_data->u_data[i] = (gesture_out_data->u_data[i]+(gesture_out_data->u_data[i-1])+gesture_out_data->u_data[i+1])/3;
        gesture_out_data->d_data[i] = (gesture_out_data->d_data[i]+(gesture_out_data->d_data[i-1])+gesture_out_data->d_data[i+1])/3;
        gesture_out_data->l_data[i] = (gesture_out_data->l_data[i]+(gesture_out_data->l_data[i-1])+gesture_out_data->l_data[i+1])/3;
        gesture_out_data->r_data[i] = (gesture_out_data->r_data[i]+(gesture_out_data->r_data[i-1])+gesture_out_data->r_data[i+1])/3;
    }

    return GESTURE_VALID;
}

int GetZoomState(int zoom_value, int last_zoom_value, int prev_zoom_distance)
{
    if (zoom_value >= last_zoom_value)
    {
        if ((zoom_value - last_zoom_value) < 20)
        {
            return prev_zoom_distance;
        }

        if (zoom_value < 50)
        {
            return ZOOM_1;
        }
        else if (zoom_value < 90)
        {
            return ZOOM_2;
        }
        else if (zoom_value < 130)
        {
            return ZOOM_3;
        }
        else if (zoom_value < 170)
        {
            return ZOOM_4;
        }
        else if (zoom_value < 210)
        {
            return ZOOM_5;
        }
        else if (zoom_value <= 255)
        {
            return ZOOM_6;
        }
        else
        {
            return ZOOM_NA;
        }
    }
    else
    {
        if ((last_zoom_value - zoom_value) < 20)
        {
            return prev_zoom_distance;
        }

        if (zoom_value >= 240)
        {
            return ZOOM_6;
        }
        else if (zoom_value > 200)
        {
            return ZOOM_5;
        }
        else if (zoom_value > 160)
        {
            return ZOOM_4;
        }
        else if (zoom_value > 120)
        {
            return ZOOM_3;
        }
        else if (zoom_value > 80)
        {
            return ZOOM_2;
        }
        else if (zoom_value > 40)
        {
            return ZOOM_1;
        }
        else
        {
            return ZOOM_1;
        }
    }
}

int DecodeMappedGesture(int mapped, int motion) 
{
    APS_DBG("enter mapped:%d,motion:%d\n", mapped, motion);
    if (!mapped)
    {
        return motion;
    }
    else
    {
        switch (motion)
        {
            case DIR_UP:
                return DIR_LEFT;
            case DIR_DOWN:
                return DIR_RIGHT;
            case DIR_LEFT:
                return DIR_DOWN;
            case DIR_RIGHT:
                return DIR_UP;
            default:
                return DIR_NONE;
        }
    }
}

int DecodeGesture(int gesture_mode)
{
    if (NEAR_STATE == gesture_state)
    {
        gesture_motion = DIR_FORWARD;
        return GESTURE_VALID;
    }
    else if (FAR_STATE == gesture_state)
    {
        gesture_motion = DIR_BACKWARD;
        return GESTURE_VALID;
    }

    if ((gesture_ud_count == -1) && (gesture_lr_count == 0))
    {
        if ((DIR_BACKWARD != gesture_prev_motion) && (DIR_FORWARD != gesture_prev_motion))
        {
            gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_UP); 
        }
    }
    else if ((gesture_ud_count == 1) && (gesture_lr_count == 0))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_DOWN); 
        }
    }
    else if ((gesture_ud_count == 0) && (gesture_lr_count == 1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_RIGHT); 
        }
    }
    else if ((gesture_ud_count == 0) && (gesture_lr_count == -1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_LEFT); 
        }
    }
    else if ((gesture_ud_count == -1) && (gesture_lr_count == 1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            if (abs(gesture_ud_delta) > abs(gesture_lr_delta))
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_UP); 
            }
            else
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_RIGHT); 
            }
        }
    }
    else if ((gesture_ud_count == 1) && (gesture_lr_count == -1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            if (abs(gesture_ud_delta) > abs(gesture_lr_delta))
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_DOWN); 
            }
            else
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_LEFT); 
            }
        }
    }
    else if ((gesture_ud_count == -1) && (gesture_lr_count == -1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            if (abs(gesture_ud_delta) > abs(gesture_lr_delta))
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_UP); 
            }
            else
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_LEFT); 
            }
        }
    }
    else if ((gesture_ud_count == 1) && (gesture_lr_count == 1))
    {
        if ((gesture_prev_motion != DIR_BACKWARD) && (gesture_prev_motion != DIR_FORWARD))
        {
            if (abs(gesture_ud_delta) > abs(gesture_lr_delta))
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_DOWN); 
            }
            else
            {
                gesture_motion = DecodeMappedGesture(fMotionMapped, DIR_RIGHT); 
            }
        }
    }
    else
    {
        gesture_motion = DIR_NONE;
        return -EINVAL;
    }

    return GESTURE_VALID;
}

int GestureDataProcessing()
{
    GESTURE_DATA_TYPE gesture_out_data;
    int ud_delta, lr_delta;
    int sensitivity1_threshold = GESTURE_SENSITIVITY_LEVEL2;
    int gesture_u_d_ratio_first, gesture_u_d_ratio_last;
    int gesture_l_r_ratio_first, gesture_l_r_ratio_last;

    if (gesture_data.total_gestures <= MIN_VALID_GESTURE)
    {
        APS_ERR("total_gestures<=6!\n");
        return -EINVAL;
    }

    /************** This is to detect fundamentals gesture ****************************/
    gesture_data.in_threshold = GESTURE_GTHR_IN;   
    gesture_data.out_threshold = GESTURE_GTHR_OUT-10;

    FilterGestureRawData(&gesture_data, &gesture_out_data);	// for fundamental 

    if ((NO_VALID_GESTURE == gesture_out_data.total_gestures) || (gesture_out_data.total_gestures < MIN_VALID_GESTURE))
    {
        APS_ERR("total_gestures==0 or total_gestures<6!\n");
        return -EINVAL;
    }

    if (gesture_out_data.u_data[0] == 0)
        gesture_out_data.u_data[0] = 1;
    if (gesture_out_data.d_data[0] == 0)
        gesture_out_data.d_data[0] = 1;
    if (gesture_out_data.l_data[0] == 0)
        gesture_out_data.l_data[0] = 1;
    if (gesture_out_data.r_data[0] == 0)
        gesture_out_data.r_data[0] = 1;

    if (gesture_out_data.u_data[gesture_out_data.total_gestures-1] == 0)
        gesture_out_data.u_data[gesture_out_data.total_gestures-1] = 1;
    if (gesture_out_data.d_data[gesture_out_data.total_gestures-1] == 0)
        gesture_out_data.d_data[gesture_out_data.total_gestures-1] = 1;
    if (gesture_out_data.l_data[gesture_out_data.total_gestures-1] == 0)
        gesture_out_data.l_data[gesture_out_data.total_gestures-1] = 1;
    if (gesture_out_data.r_data[gesture_out_data.total_gestures-1] == 0)
        gesture_out_data.r_data[gesture_out_data.total_gestures-1] = 1;

    gesture_u_d_ratio_first = (gesture_out_data.u_data[0] 
        - gesture_out_data.d_data[0])*100/(gesture_out_data.u_data[0]+gesture_out_data.d_data[0]);

    gesture_l_r_ratio_first = (gesture_out_data.l_data[0] 
        - gesture_out_data.r_data[0])*100/(gesture_out_data.l_data[0]+gesture_out_data.r_data[0]);

    gesture_u_d_ratio_last = (gesture_out_data.u_data[gesture_out_data.total_gestures-1]
        - gesture_out_data.d_data[gesture_out_data.total_gestures-1])*100/(gesture_out_data.u_data[gesture_out_data.total_gestures-1]
        + gesture_out_data.d_data[gesture_out_data.total_gestures-1]);

    gesture_l_r_ratio_last = (gesture_out_data.l_data[gesture_out_data.total_gestures-1] 
        - gesture_out_data.r_data[gesture_out_data.total_gestures-1])*100/(gesture_out_data.l_data[gesture_out_data.total_gestures-1]
        + gesture_out_data.r_data[gesture_out_data.total_gestures-1]);

    ud_delta = (gesture_u_d_ratio_last - gesture_u_d_ratio_first);
    lr_delta = (gesture_l_r_ratio_last - gesture_l_r_ratio_first);

    gesture_ud_delta = ud_delta + gesture_ud_delta;
    gesture_lr_delta = lr_delta + gesture_lr_delta;

    /**************** for Left/Right/Up/Down ****************/
    if (gesture_ud_delta >= sensitivity1_threshold)
    {
        gesture_ud_count = 1;
    }
    else if (gesture_ud_delta <= -sensitivity1_threshold)
    {
        gesture_ud_count = -1;
    }
    else
    {
        gesture_ud_count = 0;
    }

    if (gesture_lr_delta >= sensitivity1_threshold)
    {
        gesture_lr_count = 1;
    }
    else if (gesture_lr_delta <= -sensitivity1_threshold)
    {
        gesture_lr_count = -1;
    }
    else
    {
        gesture_lr_count = 0;
    }

    if ((gesture_lr_count != 0) && (gesture_ud_count != 0))
    {
        if (abs(gesture_lr_delta) > abs(gesture_ud_delta))
        {
            if (abs(gesture_lr_delta) > (2*abs(gesture_ud_delta)))
            {
                gesture_ud_count = 0;
            }
        }
        else
        {
            if (abs(gesture_ud_delta) > (2*abs(gesture_lr_delta)))
            {
                gesture_lr_count = 0;
            }     
        } 
    }

    /**************** for Left/Right/Up/Down ****************/
    if (last_ps_data == 0)
    {
        last_ps_data = ps_data;
        return GESTURE_VALID;
    }
    else
    {
        if ((gesture_ud_count == 0) && (gesture_lr_count == 0))
        {
            gesture_far_near_flag++;
            if (gesture_far_near_flag == GESTURE_FAR_NEAR_FLAG)
                gesture_far_near_started = GESTURE_FAR_NEAR_STARTED;
        }
        else if (ps_data >= APDS9960_THRESHOLD_MAX)
        {
            gesture_far_near_flag++;
            if (gesture_far_near_flag == GESTURE_FAR_NEAR_FLAG)
                gesture_far_near_started = GESTURE_FAR_NEAR_STARTED;
        }
        else if ((NA_STATE == gesture_state) && (!gesture_far_near_started))
        {
            return GESTURE_VALID;
        }
    }

    if ((GESTURE_FAR_NEAR_STARTED == gesture_far_near_started) || ((FAR_STATE == gesture_state) || (NEAR_STATE == gesture_state)))
    {
        gesture_distance = GetZoomState(ps_data, last_ps_data, gesture_prev_distance);
		
        if (gesture_prev_distance != gesture_distance)
        {
            last_ps_data = ps_data;

            if (gesture_distance > gesture_prev_distance)
            {
                gesture_prev_distance = gesture_distance;
                gesture_state = NEAR_STATE;
                return GESTURE_VALID;
            }
            else
            {
                gesture_prev_distance = gesture_distance;
                gesture_state = FAR_STATE;
                return GESTURE_VALID;
            }
        }
        else if (ps_data >= APDS9960_THRESHOLD_MAX)
        {
            last_ps_data = ps_data; 
            gesture_ud_count = 0;
            gesture_lr_count = 0;
            gesture_ud_delta = 0;
            gesture_lr_delta = 0;
            gesture_prev_motion = DIR_NONE;
            gesture_motion = DIR_NONE;
            gesture_state = NA_STATE;
        }
    }
  	
    return GESTURE_VALID;
}

static void apds9960_gesture_processing(struct i2c_client *client)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int gstatus;
    int gfifo_level;
    int gfifo_read;
    unsigned char gfifo_data[FIFO_MAX_BYTE];
    int i, j;
    unsigned char overflow_flag = 0;
    int value;
    hwm_sensor_data sensor_data;
    int gfifo0, gfifo1, gfifo2, gfifo3;
    int temp_motion;

    APS_FUN();
    cancel_delayed_work(&data->gesture_dwork);
    flush_delayed_work(&data->gesture_dwork);

    // need to loop gstatus until fifo is empty
    dma_buffer_alloct();

    ps_data = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
    APS_DBG("[chengx] psensor:%d, overtake:%d, gesture_psensor_noise:%d\n", ps_data, overtake_count, data->gesture_psensor_noise);

    // Gionee BSP chengx 20141122 modify for CR01406932 begin
    if ((ps_data > data->gesture_psensor_noise + 50) && (ps_data < 200))
    {
        overtake_count++;
    }
    else
    {
        overtake_count=0;
    }

	if (((data->gesture_psensor_noise > 20) && (ps_data < (data->gesture_psensor_noise - 20))) || overtake_count > 10)
    {
        msleep(100);
        apds9960_runtime_calibration(client);
        overtake_count=0;
        ResetGestureParameters();
    }
    // Gionee BSP chengx 20141122 modify for CR01406932 end

    // Gionee BSP1 yaoyc 20140919 add for CR01390339 begin	 
    mdelay(50); 
    // Gionee BSP1 yaoyc 20140919 add for CR01390339 end  

    gstatus = i2c_smbus_read_byte_data(client, APDS9960_GSTATUS_REG);
    if (gstatus < 0)
    {
        APS_ERR("I2C err! gstatus:%d\n", gstatus);
        dma_buffer_release();
        return;
    }

    if ((gstatus & APDS9960_GFIFO_OV) == APDS9960_GFIFO_OV)
        overflow_flag = 1;

    if ((gstatus & APDS9960_GVALID) == APDS9960_GVALID)
    {
        apds9960_set_gctrl(client,0x02);
        gfifo_level = i2c_smbus_read_byte_data(client, APDS9960_GFIFO_LVL_REG);	
        APS_DBG("gfifo_level = %d\n", gfifo_level);
        gfifo_read = gfifo_level*4;
        if (gfifo_level > 0)
        {
            I2C_dma_write_Readdata(APDS9960_GFIFO0_REG, (u8*)gfifo_data, gfifo_read);
            APS_DBG("gfifo_read = %d\n", gfifo_read);

            if (gfifo_read >= 4)
            {
                if (overflow_flag && (FIFO_MAX_BYTE == gfifo_read))
                    j = 8;
                else
                    j = 0;

                for (i=j; i<gfifo_read; i+=4)
                {
                    gesture_data.u_data[gesture_data.index] = gfifo_data[i+0];
                    gesture_data.d_data[gesture_data.index] = gfifo_data[i+1];
                    gesture_data.l_data[gesture_data.index] = gfifo_data[i+2];
                    gesture_data.r_data[gesture_data.index] = gfifo_data[i+3];
                    gesture_data.index++;
                    gesture_data.total_gestures++;
                }
					
                if (GestureDataProcessing() > 0)
                {
                    if (DecodeGesture(gesture_fundamentals) > 0)
                    {
					    // this is to detect far/near/tilt/circle gestures
						if ((DIR_BACKWARD == gesture_motion) || (DIR_FORWARD == gesture_motion))
						{/*
                            if (data->enable_ps_sensor)
                            {
                                if (DIR_BACKWARD == gesture_motion)
                                {
                                    sensor_data.values[0] = CLOSE;
                                }
                                else
                                {
                                    sensor_data.values[0] = FAR;
                                }
						        APS_LOG("%s %d values:%d\n", __func__, __LINE__, sensor_data.values[0]);
						        sensor_data.value_divide = 1;
						        sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
						        hwmsen_get_interrupt_data(ID_PROXIMITY, &sensor_data);
                            }
                        */
                            temp_motion = gesture_motion;
                            gesture_motion = ((DIR_ZOOM_1-1)+gesture_distance); 

                            APS_LOG("%s %d gesture_motion:%d\n", __func__, __LINE__, gesture_motion);
                            sensor_data.values[0] = gesture_motion;
                            sensor_data.value_divide = 1;
                            sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
                            //hwmsen_get_interrupt_data(ID_GESTURE, &sensor_data);

                            gesture_motion = temp_motion;
                        }

                        if (DIR_NONE != gesture_motion)
                        {
                            gesture_prev_motion = gesture_motion;
                        }
                    }
                }

                gesture_data.index = 0;
                gesture_data.total_gestures = NO_VALID_GESTURE;

                // set timer - to report gesture only after hand is over
                cancel_delayed_work(&data->gesture_dwork);
                flush_delayed_work(&data->gesture_dwork);
                queue_delayed_work(apds_workqueue, &data->gesture_dwork, msecs_to_jiffies(GESTURE_REPORTING_TIMER_VALUE));
            }
        }
    }

    dma_buffer_release();
}
/*
// Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
static void apds9960_xtalk_monitoring_handler(struct work_struct *work)
{
    struct apds9960_data *data = container_of(work, struct apds9960_data, xtalk_dwork.work);
    struct i2c_client *client = data->client;
    unsigned int pdata;
	unsigned int poffset_ur=0;
	unsigned int poffset_dl=0;
    unsigned int delta_max, delta_min, delta_ava;
    int i;

    if (APDS_ENABLE_GESTURE != data->enable_gesture_sensor)
    {
        APS_ERR("Gesture sensor is not actived!\n");
        return;
    }

    pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
    xtalk_pdata[xtalk_pdata_index++] = pdata;
    if (xtalk_pdata_index >= XTALK_PDATA_COUNT)
    {
        delta_ava = xtalk_pdata[0];
        delta_max = delta_min = xtalk_pdata[0];
        for (i = 1; i < XTALK_PDATA_COUNT; i++)
        {
            APS_DBG("[%d]=0x%x\n", i, xtalk_pdata[i]);
            if (xtalk_pdata[i] > delta_max)
	             delta_max = xtalk_pdata[i];
            if (xtalk_pdata[i] < delta_min)
	             delta_min = xtalk_pdata[i];

            delta_ava += xtalk_pdata[i];
        }
        delta_ava /= XTALK_PDATA_COUNT;
        APS_DBG("delta_ava=%d, delta_max=%d, delta_min=%d\n", delta_ava, delta_max, delta_min);
        if ((((delta_ava - delta_min) < 10) && ((delta_max - delta_ava) < 10) &&
                (delta_ava > GESTURE_GTHR_OUT) && 
                ((abs(delta_ava-APDS9960_PS_CALIBRATED_XTALK)) < APDS9960_CAL_THRESHOLD)) ||
                ((delta_ava == 0) && (delta_min==0) && (delta_max == 0)))
        {
            apds9960_set_aux(client, APDS9960_GESTURE_LED_BOOST|0x01);
            apds9960_runtime_calibration(client);

			poffset_ur = i2c_smbus_read_byte_data(client, APDS9960_POFFSET_UR_REG);
            poffset_dl = i2c_smbus_read_byte_data(client, APDS9960_POFFSET_DL_REG);
            APS_DBG("poffset_ur:%d, poffset_dl:%d\n", poffset_ur, poffset_dl);
			if ((poffset_ur>=80) && (poffset_dl>=80))
            {
			    apds9960_set_aux(client, APDS9960_REDUCE_POWER_BOOST|0x01);
			    apds9960_runtime_calibration(client);
			}

			APS_DBG("%s %d xtalk_monitoring:%d\n", __func__, __LINE__, delta_ava);
        }

        xtalk_pdata_index = 0;
    }

    // restart timer
    queue_delayed_work(apds_workqueue, &data->xtalk_dwork, msecs_to_jiffies(XTALK_MONITORING_TIMER_VALUE));
	APS_DBG("%s exit!\n", __func__);
}
// Gionee BSP1 yang_yang 20140814 modify for CR01357229 end
*/
// Gesture Reporting routine
static void apds9960_gesture_reporting_handler(struct work_struct *work)
{
    struct apds9960_data *data = container_of(work, struct apds9960_data, gesture_dwork.work);
    struct i2c_client *client=data->client;
    hwm_sensor_data sensor_data;

    if (APDS_ENABLE_GESTURE != data->enable_gesture_sensor)
    {
        APS_ERR("Gesture sensor is not actived!\n");
        return;
    }

    // TODO - DO NOT need this, this is executed in Timer
    if ((DIR_LEFT == gesture_motion) || (DIR_RIGHT == gesture_motion) || (DIR_UP == gesture_motion) || (DIR_DOWN == gesture_motion))
    {
        if ((DIR_FORWARD != gesture_prev_motion) || (DIR_BACKWARD != gesture_prev_motion))
        {
            APS_LOG("%s %d gesture_motion:%d\n", __func__, __LINE__, gesture_motion);
            sensor_data.values[0] = gesture_motion;
            sensor_data.value_divide = 1;
            sensor_data.status = SENSOR_STATUS_ACCURACY_MEDIUM;
            //hwmsen_get_interrupt_data(ID_GESTURE, &sensor_data);
        }
    }
	
    ResetGestureParameters();
    apds9960_set_gctrl(client, 0x06);
}

// ALS_PS interrupt routine
static void apds9960_work_handler(struct work_struct *work)
{
    struct apds9960_data *data = container_of(work, struct apds9960_data, psensor_dwork.work);
    struct i2c_client *client = data->client;
    int status;

    status = i2c_smbus_read_byte_data(client, APDS9960_STATUS_REG);
    APS_DBG("==>isr : status=%x, enable=%x\n", status, data->enable);

    if ((status & APDS9960_STATUS_GINT) && 
			(data->enable & (APDS9960_GESTURE_ENABLE|APDS9960_PWR_ON)) && (data->gctrl & APDS9960_GIEN))
    {
		// Gesture is enabled with interrupte
        apds9960_gesture_processing(client);
    }
	
    if ((status & APDS9960_STATUS_PINT) && 
            ((data->enable & (APDS9960_PS_INT_ENABLE|APDS9960_PS_ENABLE|APDS9960_PWR_ON)) == 
            (APDS9960_PS_INT_ENABLE|APDS9960_PS_ENABLE|APDS9960_PWR_ON)) )
    {
		// PS is interrupted
		// check if this is triggered by background ambient noise
        /* if (status & APDS9960_STATUS_PSAT)
        {
            APS_DBG("PS is triggered by background ambient noise\n");
        }
        else*/ if (status & APDS9960_STATUS_PVALID)
        {
            apds9960_change_ps_threshold(client);
        }
    }
	
    if ((status & APDS9960_STATUS_AINT) && 
            ((data->enable & (APDS9960_ALS_INT_ENABLE|APDS9960_ALS_ENABLE|APDS9960_PWR_ON)) == 
            (APDS9960_ALS_INT_ENABLE|APDS9960_ALS_ENABLE|APDS9960_PWR_ON)))
    {
		// ALS is interrupted
		// check if this is triggered by background ambient noise
        if (status & APDS9960_STATUS_ASAT)
        {
            APS_DBG("ALS is saturated\n");
        }
        else if (status & APDS9960_STATUS_AVALID)
        {
            apds9960_change_als_threshold(client);
        }
    }

    apds9960_clear_interrupt(client, CMD_CLR_ALL_INT);
    mt_eint_unmask(CUST_EINT_ALS_NUM); 
}

// assume this is ISR
void apds9960_interrupt()
{
    APS_FUN();
    struct apds9960_data *data = apds9960_obj;
    if (!data)
    {
        APS_ERR("data is NULL!\n");
        return;
    }

    cancel_delayed_work(&data->psensor_dwork);
    queue_delayed_work(apds_workqueue, &data->psensor_dwork, 0);
}

/*
 * IOCTL support
 */
static int apds9960_enable_als_sensor(struct i2c_client *client, int val)
{
    APS_LOG("%s: enable als sensor (%d)\n", __func__, val);
    struct apds9960_data *data = i2c_get_clientdata(client);
    if (!data)
    {
        APS_ERR("data is null!\n");
        return -EINVAL;
    }

    if ((APDS_ENABLE_ALS_WITH_INT == val) || (APDS_ENABLE_ALS_NO_INT == val))
    {
        // turn on light  sensor
        if (APDS_DISABLE_ALS == data->enable_als_sensor)
        {
            data->enable_als_sensor = val;
            if (APDS_ENABLE_ALS_NO_INT == data->enable_als_sensor)      // als polling
            {	
				if (!data->enable_gesture_sensor && !data->enable_ps_sensor)
                {
					// als polling
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE);
				}
				else if (data->enable_gesture_sensor && data->enable_ps_sensor)
                {
					// gesture interrupt, ps interrupt, als polling
					apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
					apds9960_set_gctrl(client, 0x06);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
							APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else if (data->enable_gesture_sensor)
                {
					// gesture interrupt, als polling
					apds9960_set_gctrl(client, 0x06);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
							APDS9960_GESTURE_ENABLE);
				}
				else
                {
					// ps interrupt, als polling
					apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
							APDS9960_PS_INT_ENABLE);
				}
            }
            else    // als interrupt
            {
                // force first ALS interrupt in order to get environment reading
                apds9960_set_ailt(client, APDS9960_THRESHOLD_MAX);
                apds9960_set_aiht(client, APDS9960_THRESHOLD_MIN);
	
				if (!data->enable_gesture_sensor && !data->enable_ps_sensor)
                {
					// als interrupt
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_ALS_INT_ENABLE);
					//mt_eint_unmask(CUST_EINT_ALS_NUM);
				}
				else if (data->enable_gesture_sensor && data->enable_ps_sensor)
                {
					// gesture interrupt, ps interrupt, als interrupt
					apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
					apds9960_set_gctrl(client, 0x06);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
							APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else if (data->enable_gesture_sensor)
                {
					// gesture interrupt, als interrupt
					apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
					apds9960_set_gctrl(client, 0x06);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else
                {
					// ps interrupt, als interrupt
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE);
				}
            }		
			mt_eint_unmask(CUST_EINT_ALS_NUM);
        }
    }
    else if (APDS_DISABLE_ALS == val)
    {
		// turn off light sensor
        data->enable_als_sensor = val;
		if (!data->enable_gesture_sensor && !data->enable_ps_sensor)
        {
			// Power Off
			apds9960_set_enable(client,APDS9960_PWR_DOWN);
            mt_eint_mask(CUST_EINT_ALS_NUM);		
		}
		else if (data->enable_ps_sensor && data->enable_gesture_sensor)
        {
			// gesture interrupt, ps interrupt
			apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
			apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
			apds9960_set_gctrl(client, 0x06);
			apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
					APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
		}
		else if (data->enable_gesture_sensor)
        {
			// gesture interrupt
			apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
			apds9960_set_gctrl(client, 0x06);		
			apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                    APDS9960_GESTURE_ENABLE);
		}
		else
        {
			// ps interrupt
			apds9960_set_wtime(client, APDS9960_246_WAIT_TIME);
			apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
			apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                    APDS9960_PS_INT_ENABLE);
		}				
    }
    else
    {
        APS_ERR("enable als sensor=%d\n", val);
        return -EINVAL;
    }

    return SUCCESS;
}

static int apds9960_set_als_poll_delay(struct i2c_client *client, unsigned int val)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int ret;
    int atime_index=0;

    APS_DBG("%d\n", val);
    if ((APDS_ALS_POLL_SLOW != val) && (APDS_ALS_POLL_MEDIUM != val) && (APDS_ALS_POLL_FAST != val))
    {
        APS_ERR("invalid value=%d\n", val);
        return -EINVAL;
    }

    if (APDS_ALS_POLL_FAST == val)
    {
        atime_index = APDS9960_ALS_RES_24MS;
    }
    else if (APDS_ALS_POLL_MEDIUM == val)
    {
        atime_index = APDS9960_ALS_RES_50MS;
    }
    else	// APDS_ALS_POLL_SLOW
    {
        atime_index = APDS9960_ALS_RES_100MS;
    }

    ret = apds9960_set_atime(client, apds9960_als_atime_tb[atime_index]);
    if (ret >= 0)
    {
        data->als_atime_index = atime_index;
        APS_DBG("atime_index %d\n", data->als_atime_index);
    }
    else
        return -EIO;
		
    return SUCCESS;
}

static int apds9960_enable_ps_sensor(struct i2c_client *client, int val)
{
    APS_LOG("enable ps senosr (%d)\n", val);
    struct apds9960_data *data = i2c_get_clientdata(client);
    if (!data)
    {
        APS_ERR("data is null!\n");
        return -EINVAL;
    }

	if (data->enable_gesture_sensor)
	{
        apds9960_enable_gesture_sensor(client, APDS_DISABLE_GESTURE);
	}

    if (APDS_ENABLE_PS == val)      // interrupt mode only
    {
        apds9960_setup_eint(client);

		//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
		if (need_incall_cali && !is_hand_answer)
		{
			APS_LOG("enable ps incall calibrate \n ");
#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
			uint8_t temp;
			temp = i2c_smbus_read_byte_data(client, APDS9960_ENABLE_REG);
			apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE|temp);
			APS_LOG("temp=%d\n", temp);
			apds9960_dynamic_calibrate();
			apds9960_set_enable(client , temp);
#endif
			need_incall_cali = false;
		}
		is_hand_answer = false;
#endif
		//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end

        // turn on p sensor
        if (APDS_DISABLE_PS == data->enable_ps_sensor)
        {
            data->enable_ps_sensor = val;

			apds9960_set_ppulse(client, data->ps_ppulse);
			apds9960_set_control(client, APDS9960_PDRVIE_FOR_PS|APDS9960_PGAIN_FOR_PS|
                    apds9960_als_again_bit_tb[data->als_again_index]);

			apds9960_set_poffset_ur(client, data->ps_poffset_ur);
			apds9960_set_poffset_dl(client, data->ps_poffset_dl);

			apds9960_set_aux(client, APDS9960_PS_LED_BOOST|0x01);

			if (!data->enable_gesture_sensor && !data->enable_als_sensor)
			{
				// PS interrupt
				apds9960_set_wtime(client, APDS9960_246_WAIT_TIME);
				apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|
                        APDS9960_WAIT_ENABLE|APDS9960_PS_INT_ENABLE);
				//mt_eint_unmask(CUST_EINT_ALS_NUM);
			}
			else if (data->enable_gesture_sensor && data->enable_als_sensor)
            {
				apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
				apds9960_set_gctrl(client, 0x06);
				if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
                {
					// PS interrupt, gesture interrupt, als interrupt
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else
                {
					// PS interrupt, gesture interrupt, als polling
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
			}
			else if (data->enable_gesture_sensor)
            {
				// PS interrupt, gesture interrupt, no als
				apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
				apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
				apds9960_set_gctrl(client, 0x06);
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                            APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
			}
			else
            {
				apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
				if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
                {
					// ps interrupt, als interrupt
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE);
				}
				else
                {
					// ps interrupt, als polling
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_PS_INT_ENABLE);
					//mt_eint_unmask(CUST_EINT_ALS_NUM);
				}
			}
			mdelay(1);
			apds9960_change_ps_threshold(client);
			mt_eint_unmask(CUST_EINT_ALS_NUM);
        }
    } 
    else if (APDS_DISABLE_PS == val)
    {
		//turn off p sensor - can't turn off the entire sensor, the light sensor may be needed by HAL
		data->enable_ps_sensor = val;

		if (!data->enable_als_sensor && !data->enable_gesture_sensor)
        {
			apds9960_set_enable(client, APDS9960_PWR_DOWN);
            mt_eint_mask(CUST_EINT_ALS_NUM);	
		}
		else if (data->enable_als_sensor && data->enable_gesture_sensor)
        {
			apds9960_set_gctrl(client, 0x06);
			if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
            {
				// gesture interrupt, als interrupt
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
			}
			else
            {
				// gesture interrupt, als polling
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_GESTURE_ENABLE);
			}
		}
		else if (data->enable_gesture_sensor)
        {
			// no ps, no als, gesture interrupt
			apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
			apds9960_set_gctrl(client, 0x06);
			apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                            APDS9960_GESTURE_ENABLE);
		}
		else
        {			
			if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
            {				
				// als interrupt
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_ALS_INT_ENABLE);
			}
			else
            {
				// als polling
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE);
                mt_eint_mask(CUST_EINT_ALS_NUM);
			}
		}
    }
    else
    {
        APS_ERR("invalid value=%d\n", val);
        return -EINVAL;
    }
	
    return SUCCESS;
}

static int apds9960_enable_gesture_sensor(struct i2c_client *client, int val)
{
    APS_LOG("enable gesture senosr (%d)\n", val);
    struct apds9960_data *data = i2c_get_clientdata(client);
    if (!data)
    {
        APS_ERR("data is null!\n");
        return -EINVAL;
    }

    if (APDS_ENABLE_GESTURE == val)     // interrupt mode only
    {
// Gionee BSP1 chengx 20141017 modify for CR01396687 begin
        if (data->enable_ps_sensor)
        {
            APS_LOG("P-sensor is enable!\n");
            return -EINVAL;
        }
// Gionee BSP1 chengx 20141017 modify for CR01396687 end

        apds9960_setup_eint(client);
        apds9960_runtime_calibration(client);    //cal gesture sensor
        if (APDS_DISABLE_GESTURE == data->enable_gesture_sensor)
        {
            data->enable_gesture_sensor= val;

            ResetGestureParameters();
            apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
            apds9960_set_ppulse(client, data->gesture_ppulse);
            apds9960_set_control(client, APDS9960_PDRVIE_FOR_GESTURE|
					APDS9960_PGAIN_FOR_GESTURE|apds9960_als_again_bit_tb[data->als_again_index]);
            apds9960_set_aux(client, APDS9960_GESTURE_LED_BOOST|0x01);
            apds9960_set_gctrl(client, 0x06);       // gesture registers

            if (!data->enable_ps_sensor && !data->enable_als_sensor)
            {
				// gesture interrupt
				apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                            APDS9960_GESTURE_ENABLE);
				//mt_eint_unmask(CUST_EINT_ALS_NUM);
			}
			else if (data->enable_ps_sensor && data->enable_als_sensor)
            {
				apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);

				if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
                {
					// gesture interrupt, ps interrupt, als interrupt
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else
                {
					// gesture interrupt, ps interrupt, als polling
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
			}
			else if (data->enable_als_sensor)
            {
				if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
                {
					// gesture interrupt, als interrupt
					apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
				}
				else
                {
					// gesture interrupt, als polling
					apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_GESTURE_ENABLE);
					//mt_eint_unmask(CUST_EINT_ALS_NUM);
				}
			}
			else // data->enable_ps_sensor
            {
				// gesture interrupt, ps interrupt
				apds9960_set_wtime(client, APDS9960_255_WAIT_TIME);
				apds9960_set_pers(client, APDS9960_PPERS_1|APDS9960_APERS_1);
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                            APDS9960_PS_INT_ENABLE|APDS9960_GESTURE_ENABLE);
			}
			mt_eint_unmask(CUST_EINT_ALS_NUM);
			
            // Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
            //xtalk_pdata_index = 0;
            //cancel_delayed_work(&data->xtalk_dwork);
            //flush_delayed_work(&data->xtalk_dwork);
            //queue_delayed_work(apds_workqueue, &data->xtalk_dwork, msecs_to_jiffies(XTALK_MONITORING_TIMER_VALUE));
            // Gionee BSP1 yang_yang 20140814 modify for CR01357229 end
        }
    } 
    else if (APDS_DISABLE_GESTURE == val)
    {
        // turn off gesture sensor - can't turn off the entire sensor, the light/proximity sensor may be needed by HAL
        data->enable_gesture_sensor = val;

		// disable gesture interrup
		apds9960_set_gctrl(client, 0x04);

		if (!data->enable_ps_sensor && !data->enable_als_sensor)
        {
			apds9960_set_enable(client, APDS9960_PWR_DOWN);
            mt_eint_mask(CUST_EINT_ALS_NUM);
		}
		else if (data->enable_ps_sensor && data->enable_als_sensor)
        {
			apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
			if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
            {
				// ps interrupt, als interrupt
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_ALS_INT_ENABLE|APDS9960_PS_INT_ENABLE);
			}
			else
            {
				// ps interrupt, als polling
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_PS_ENABLE|
                            APDS9960_PS_INT_ENABLE);
			}
		}
		else if (data->enable_als_sensor)
        {
			if (APDS_ENABLE_ALS_WITH_INT == data->enable_als_sensor)
            {
				// als interrupt
				apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE|APDS9960_ALS_INT_ENABLE);
			}
			else
            {
				// als polling
				apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_ALS_ENABLE);
                mt_eint_mask(CUST_EINT_ALS_NUM);
			}
		}
		else
        {
			// ps interrupt
			apds9960_set_wtime(client, APDS9960_246_WAIT_TIME);
			apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);
			apds9960_set_enable(client, APDS9960_PWR_ON|APDS9960_PS_ENABLE|APDS9960_WAIT_ENABLE|
                            APDS9960_PS_INT_ENABLE);
		}

        // Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
        //cancel_delayed_work(&data->xtalk_dwork);
        //flush_delayed_work(&data->xtalk_dwork);
        // Gionee BSP1 yang_yang 20140814 modify for CR01357229 end
    }
	else
    {
        APS_ERR("invalid value=%d\n", val);
        return -EINVAL;
    }

    return SUCCESS;
}

/*********************run-time-calibration_2014_06_27_lgc*********************************/
static int apds9960_offset_calibration(uint8_t data, uint8_t *offset, uint8_t *loop)
{
    if (data > APDS9960_PS_CALIBRATED_XTALK)
    {
        // reduce 
        if (DEFAULT_OFFSET == *offset)
            *offset = NO_OFFSET;
        else if (OFFSET_REDUCE_MAX == *offset)
            return OFFSET_CALIBRATION_COMPLETE;

        *offset += *loop;
        return OFFSET_CALIBRATION_UNCOMPLETED;
    }		
    else if (data < APDS9960_PS_CALIBRATED_XTALK_BASELINE)
    {
        // increase
        if (DEFAULT_OFFSET == *offset)
            *offset == OFFSET_INCREASE_MAX;
        else if (OFFSET_INCREASE_MAX == *offset)
            return OFFSET_CALIBRATION_COMPLETE;

        *offset -= *loop;
        return OFFSET_CALIBRATION_UNCOMPLETED;
    }
    else
    {
        if (DEFAULT_OFFSET == *offset)
            *offset = NO_OFFSET;
        return OFFSET_CALIBRATION_COMPLETE;
    }
}

static int apds9960_runtime_calibration(struct i2c_client *client)
{
   	struct apds9960_data *data = i2c_get_clientdata(client);
   	
    uint8_t gthr_in, gthr_out;
    uint8_t gconf, gconf1;
    uint8_t config2;
    uint8_t enable;

    uint8_t pdata;
    uint8_t status;
    uint8_t temp_offset;

    uint8_t gstatus;
    uint8_t temp_goffset_u, temp_goffset_d, temp_goffset_l, temp_goffset_r;
    unsigned char i2c_data[4];
    uint8_t gesture_u, gesture_d, gesture_l, gesture_r;
    uint8_t gesture_u_cal_done = 0;
    uint8_t gesture_d_cal_done = 0;
    uint8_t gesture_l_cal_done = 0;
    uint8_t gesture_r_cal_done = 0;

    uint8_t i, j;
    uint8_t Loop_ur = 0;

    gconf= i2c_smbus_read_byte_data(client, APDS9960_GCTRL_REG);
    config2 = i2c_smbus_read_byte_data(client, APDS9960_CONFIG2_REG);
    enable = i2c_smbus_read_byte_data(client, APDS9960_ENABLE_REG);

    apds9960_set_enable(client , APDS9960_PWR_DOWN);
    apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);

    for (j=0; j<8; j++)
    {
        status = i2c_smbus_read_byte_data(client, APDS9960_STATUS_REG);

        if (APDS9960_STATUS_PVALID == (status & APDS9960_STATUS_PVALID)) 
            break;

        mdelay(5);
    }
    pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
    
    gthr_in = i2c_smbus_read_byte_data(client, APDS9960_GTHR_IN_REG);
    apds9960_set_gthr_in(client , 0x00);

    gthr_out = i2c_smbus_read_byte_data(client, APDS9960_GTHR_OUT_REG);
    apds9960_set_gthr_out(client , 0x00);

    gconf1 = i2c_smbus_read_byte_data(client, APDS9960_GCONF1_REG);
    apds9960_set_gconf1(client , APDS9960_GFIFO_1_LEVEL);

    /* 2in1 */
    apds9960_set_poffset_ur(client, NO_OFFSET);
    apds9960_set_poffset_dl(client, NO_OFFSET);
    apds9960_set_goffset_u(client, NO_OFFSET);
    apds9960_set_goffset_d(client, NO_OFFSET);
    apds9960_set_goffset_l(client, NO_OFFSET);
    apds9960_set_goffset_r(client, NO_OFFSET);

	apds9960_set_control(client, APDS9960_PDRVIE_FOR_GESTURE|APDS9960_PGAIN_FOR_GESTURE|apds9960_als_again_bit_tb[data->als_again_index]);
	apds9960_set_ppulse(client, data->gesture_ppulse);

    /* poffset_ur */
    apds9960_set_enable(client, APDS9960_PWR_DOWN);
    apds9960_set_config2(client , 0x26);    // D and L are masked	
    apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);     // PEN and PON

    temp_offset = DEFAULT_OFFSET;
    Loop_ur = DEFAULT_OFFSET;
    for (i=0; i<8; i++)
    {
        Loop_ur = Loop_ur/2;      // 2014_04_26 Lgc Modified
        for (j=0; j<8; j++) 
        {
            status = i2c_smbus_read_byte_data(client, APDS9960_STATUS_REG);

            if (APDS9960_STATUS_PVALID == (status & APDS9960_STATUS_PVALID)) 
	            break;

            mdelay(5);
        }

        pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
        if (apds9960_offset_calibration(pdata, &temp_offset, &Loop_ur))
            break;

        APS_DBG("ur: offset=%d\n", temp_offset);
        apds9960_set_poffset_ur(client , temp_offset);
        apds9960_set_enable(client , APDS9960_PWR_DOWN);
        apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);
    }

    // poffset_dl
    apds9960_set_enable(client , APDS9960_PWR_DOWN);
    apds9960_set_config2(client , 0x29);        // U and R are masked
    apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);

    temp_offset = DEFAULT_OFFSET;
    Loop_ur = DEFAULT_OFFSET;
    for (i=0; i<8; i++)
    {   
        Loop_ur = Loop_ur/2;

        for (j=0; j<8; j++)
        {
            status = i2c_smbus_read_byte_data(client, APDS9960_STATUS_REG);

            if (APDS9960_STATUS_PVALID == (status & APDS9960_STATUS_PVALID)) 
                break;

            mdelay(5);
        }

        pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
        if (apds9960_offset_calibration(pdata, &temp_offset, &Loop_ur))
            break;
        
        APS_DBG("dl: offset=%d\n", temp_offset);
        apds9960_set_poffset_dl(client , temp_offset);
		apds9960_set_enable(client , APDS9960_PWR_DOWN);
        apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);
    }

    apds9960_set_config2(client , 0);
    apds9960_set_enable(client , APDS9960_PWR_DOWN);
    apds9960_set_gctrl(client , APDS9960_GFIFO_CLR|APDS9960_GMODE);
    apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_GESTURE_ENABLE);

    temp_goffset_u = DEFAULT_OFFSET;
    temp_goffset_d = DEFAULT_OFFSET;
    temp_goffset_l = DEFAULT_OFFSET;
    temp_goffset_r = DEFAULT_OFFSET;
    Loop_ur = DEFAULT_OFFSET;
    for (i=0; i<8; i++)
    {
        Loop_ur = Loop_ur/2;

        for (j=0; j<8; j++)
        {
            gstatus = i2c_smbus_read_byte_data(client, APDS9960_GSTATUS_REG);

            if (APDS9960_GVALID == (gstatus & APDS9960_GVALID)) 
                break;

            mdelay(5);
        }

        i2c_smbus_read_i2c_block_data(client, APDS9960_GFIFO0_REG, 4, (unsigned char*)i2c_data);

        gesture_u = i2c_data[0];
        gesture_d = i2c_data[1];
        gesture_l = i2c_data[2];
        gesture_r = i2c_data[3];

        APS_DBG("u:%d, d:%d, l:%d, r:%d\n", gesture_u, gesture_d, gesture_l, gesture_r);
        gesture_u_cal_done = apds9960_offset_calibration(gesture_u, &temp_goffset_u, &Loop_ur);
        if (!gesture_u_cal_done)
        {
            APS_DBG("u: offset=%d\n", temp_goffset_u);
            apds9960_set_goffset_u(client , temp_goffset_u);
        }

        gesture_d_cal_done = apds9960_offset_calibration(gesture_d, &temp_goffset_d, &Loop_ur);
        if (!gesture_d_cal_done)
        {
            APS_DBG("d: offset=%d\n", temp_goffset_d);
            apds9960_set_goffset_d(client , temp_goffset_d);
        }

        gesture_l_cal_done = apds9960_offset_calibration(gesture_l, &temp_goffset_l, &Loop_ur);
        if (!gesture_l_cal_done)
        {
            APS_DBG("l: offset=%d\n", temp_goffset_l);
            apds9960_set_goffset_l(client , temp_goffset_l);
        }

        gesture_r_cal_done = apds9960_offset_calibration(gesture_r, &temp_goffset_r, &Loop_ur);
        if (!gesture_r_cal_done)
        {
            APS_DBG("r: offset=%d\n", temp_goffset_l);
            apds9960_set_goffset_r(client , temp_goffset_r);
        }

        if (gesture_u_cal_done && gesture_d_cal_done && gesture_l_cal_done && gesture_r_cal_done)
            break;
			
        apds9960_set_enable(client , APDS9960_PWR_DOWN);
        apds9960_set_gctrl(client , APDS9960_GFIFO_CLR|APDS9960_GMODE);
        apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_GESTURE_ENABLE);
    }

// Gionee BSP chengx 2011122 modify for CR01406932 begin
    apds9960_set_enable(client , APDS9960_PWR_DOWN);
    apds9960_set_enable(client , APDS9960_PWR_ON|APDS9960_PS_ENABLE);

    for (j=0; j<8; j++)
    {
        status = i2c_smbus_read_byte_data(client, APDS9960_STATUS_REG);

        if (APDS9960_STATUS_PVALID == (status & APDS9960_STATUS_PVALID)) 
            break;

        mdelay(5);
    }
    data->gesture_psensor_noise = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
// Gionee BSP chengx 2011122 modify for CR01406932 end

    apds9960_set_enable(client , APDS9960_PWR_DOWN);
    apds9960_set_gthr_in(client , gthr_in);
    apds9960_set_gthr_out(client , gthr_out);
    apds9960_set_gctrl(client , gconf);
    apds9960_set_gconf1(client , gconf1);
    apds9960_set_config2(client ,config2);
    apds9960_set_enable(client , enable);

    APS_DBG("[chengx] gesture_psensor_noise=%d\n", data->gesture_psensor_noise);
    APS_DBG("[chengx] %s exit!\n", __func__);   
	return SUCCESS;
}

static int apds9960_ps_open(struct inode *inode, struct file *file)
{
    APS_FUN();
    return SUCCESS; 
}

static int apds9960_ps_release(struct inode *inode, struct file *file)
{
    APS_FUN();
    return SUCCESS;
}

static long apds9960_ps_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct apds9960_data *data;
    struct i2c_client *client;
    int enable;
    int ret = -1;

    if (NONE_ARG == arg)
    {
        APS_ERR("arg = 0\n");
        return -EFAULT;
    }

    if (!apds9960_i2c_client)
    {
        APS_ERR("error: i2c driver not installed\n");
        return -EFAULT;
    }

    client = apds9960_i2c_client;   
    data = i2c_get_clientdata(apds9960_i2c_client);

    switch (cmd)
    {
        case APDS_IOCTL_PS_ENABLE:
            if (copy_from_user(&enable, (void __user *)arg, sizeof(enable)))
            {
                APS_ERR("APDS_IOCTL_PS_ENABLE: copy_from_user failed\n");
                return -EFAULT;
            }

            ret = apds9960_enable_ps_sensor(client, enable);        
            if (ret < 0)
            {
                APS_ERR("apds9960_enable_ps_sensor() failed!\n");
                return ret;
            }
            break;

        case APDS_IOCTL_PS_GET_ENABLE:
            if (copy_to_user((void __user *)arg, &data->enable_ps_sensor, sizeof(data->enable_ps_sensor)))
            {
                APS_ERR("APDS_IOCTL_PS_GET_ENABLE: copy_to_user failed\n");
                return -EFAULT;
            }

            break;

        case APDS_IOCTL_PS_GET_PDATA:
            data->ps_data =	i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
            if (copy_to_user((void __user *)arg, &data->ps_data, sizeof(data->ps_data)))
            {
                APS_ERR("APDS_IOCTL_PS_GET_PDATA: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_GESTURE_ENABLE:              
            if (copy_from_user(&enable, (void __user *)arg, sizeof(enable)))
            {
                APS_ERR("APDS_IOCTL_GESTURE_ENABLE: copy_from_user failed\n");
                return -EFAULT;
            }

            ret = apds9960_enable_gesture_sensor(client, enable);        
            if(ret < 0)
            {
                APS_ERR("apds9960_enable_gesture_sensor() failed!\n");
                return ret;
            }
            break;

        case APDS_IOCTL_GESTURE_GET_ENABLE:
            if (copy_to_user((void __user *)arg, &data->enable_gesture_sensor, sizeof(data->enable_gesture_sensor)))
            {
                APS_ERR("APDS_IOCTL_GESTURE_GET_ENABLE: copy_to_user failed\n");
                return -EFAULT;
            }

            break;

        default:
            break;
    }

    return SUCCESS;
}

static int apds9960_als_open(struct inode *inode, struct file *file)
{
    APS_FUN();
    return SUCCESS;
}

static int apds9960_als_release(struct inode *inode, struct file *file)
{
    APS_FUN();
    return SUCCESS;
}

static long apds9960_als_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct apds9960_data *data;
	struct i2c_client *client;
	int enable;
	int ret = -1;
	unsigned int delay;

	if (NONE_ARG == arg)
    {
        APS_ERR("arg = 0\n");
        return -EFAULT;
    }

    if (!apds9960_i2c_client)
    {    
        APS_ERR("i2c driver not installed\n");
        return -EFAULT;
    }

    client = apds9960_i2c_client;   
    data = i2c_get_clientdata(apds9960_i2c_client);

    switch (cmd)
    {
        case APDS_IOCTL_ALS_ENABLE:
            if (copy_from_user(&enable, (void __user *)arg, sizeof(enable)))
            {
                APS_ERR("APDS_IOCTL_ALS_ENABLE: copy_from_user failed\n");
                return -EFAULT;
            }

            ret = apds9960_enable_als_sensor(client, enable); 
            if (ret < 0)
            {
                APS_ERR("apds9960_enable_als_sensor() failed!\n");
                return ret;
            }
            break;

        case APDS_IOCTL_ALS_POLL_DELAY:
            if (APDS_ENABLE_ALS_NO_INT == data->enable_als_sensor)
            {	
                if (copy_from_user(&delay, (void __user *)arg, sizeof(delay)))
                {
                    APS_ERR("APDS_IOCTL_ALS_POLL_DELAY: copy_to_user failed\n");
                    return -EFAULT;
                }

                ret = apds9960_set_als_poll_delay(client, delay); 
                if (ret < 0)
                {
                    APS_ERR("apds9960_set_als_poll_delay() failed!\n");
                    return ret;
                }
            }
            else
            {
                APS_ERR("APDS_IOCTL_ALS_POLL_DELAY: als is not in polling mode!\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_ALS_GET_ENABLE:
            if (copy_to_user((void __user *)arg, &data->enable_als_sensor, sizeof(data->enable_als_sensor)))
            {
                APS_ERR("APDS_IOCTL_ALS_GET_ENABLE: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_ALS_GET_CDATA:
            data->als_data = i2c_smbus_read_word_data(client, APDS9960_CDATAL_REG);
            if (copy_to_user((void __user *)arg, &data->als_data, sizeof(data->als_data)))
            {
                APS_ERR("APDS_IOCTL_ALS_GET_CDATA: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_ALS_GET_RDATA:
            data->als_data = i2c_smbus_read_word_data(client, APDS9960_RDATAL_REG);
            if (copy_to_user((void __user *)arg, &data->als_data, sizeof(data->als_data)))
            {
                APS_ERR("APDS_IOCTL_ALS_GET_RDATA: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_ALS_GET_GDATA:
            data->als_data = i2c_smbus_read_word_data(client, APDS9960_GDATAL_REG);
            if (copy_to_user((void __user *)arg, &data->als_data, sizeof(data->als_data)))
            {
                APS_ERR("APDS_IOCTL_ALS_GET_GDATA: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        case APDS_IOCTL_ALS_GET_BDATA:
            data->als_data = i2c_smbus_read_word_data(client, APDS9960_BDATAL_REG);
            if (copy_to_user((void __user *)arg, &data->als_data, sizeof(data->als_data)))
            {
                APS_ERR("APDS_IOCTL_ALS_GET_BDATA: copy_to_user failed\n");
                return -EFAULT;
            }
            break;

        default:
            break;
    }

    return SUCCESS;
}

/*****************************SysFS support***********************************/
static ssize_t apds9960_show_cdata(struct device_driver *ddri, char *buf)
{
    int cdata;

    cdata = i2c_smbus_read_word_data(apds9960_i2c_client, APDS9960_CDATAL_REG);

    return sprintf(buf, "%d\n", cdata);
}

static ssize_t apds9960_show_rdata(struct device_driver *ddri, char *buf)
{
    int rdata;

    rdata = i2c_smbus_read_word_data(apds9960_i2c_client, APDS9960_RDATAL_REG);

    return sprintf(buf, "%d\n", rdata);
}

static ssize_t apds9960_show_gdata(struct device_driver *ddri, char *buf)
{
    int gdata;

    gdata = i2c_smbus_read_word_data(apds9960_i2c_client, APDS9960_GDATAL_REG);

    return sprintf(buf, "%d\n", gdata);
}

static ssize_t apds9960_show_bdata(struct device_driver *ddri, char *buf)
{
    int bdata;

    bdata = i2c_smbus_read_word_data(apds9960_i2c_client, APDS9960_BDATAL_REG);

    return sprintf(buf, "%d\n", bdata);
}

static ssize_t apds9960_show_pdata(struct device_driver *ddri, char *buf)
{
    int pdata;

    pdata = i2c_smbus_read_byte_data(apds9960_i2c_client, APDS9960_PDATA_REG);

    return sprintf(buf, "%d\n", pdata);
}

static ssize_t apds9960_show_alsps_cal(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d,%d,%d,%d,%d,%d,%d,%d,%d\n", data->cal.l_cal,
                        data->cal.p_cal_ur, data->cal.p_cal_dl, data->cal.g_cal_u, data->cal.g_cal_d,
                        data->cal.g_cal_l, data->cal.g_cal_r, data->cal.g_cal_ur, data->cal.g_cal_dl);
}

static ssize_t apds9960_store_alsps_cal(struct device_driver *ddri, const char *buf, size_t count)
{
    struct i2c_client *client = apds9960_i2c_client;
    struct apds9960_data *data = i2c_get_clientdata(client);
    struct alsps_cal cali;
    int rc = 0;

    rc = sscanf(buf, "%d %d %d %d %d %d %d %d %d", &cali.l_cal, &cali.p_cal_ur, &cali.p_cal_dl,
                    &cali.g_cal_u, &cali.g_cal_d, &cali.g_cal_l, &cali.g_cal_r, &cali.g_cal_ur,
                    &cali.g_cal_dl);
    if (9 != rc)
    {
        APS_ERR("Invalid arguments\n");
        rc = -EINVAL;
        return rc;
    }
    APS_DBG("GO l_cal(%d)p_ur(%d)p_dl(%d)g_ur(%d)g_dl(%d)gu(%d)gd(%d)gl(%d)gr(%d)\n", cali.l_cal,
                cali.p_cal_ur, cali.p_cal_dl, cali.g_cal_ur, cali.g_cal_dl, cali.g_cal_u, cali.g_cal_d,
                cali.g_cal_l, cali.g_cal_r);

    data->ps_poffset_ur = cali.p_cal_ur;
    data->ps_poffset_dl = cali.p_cal_dl;

    apds9960_set_goffset_u(client, cali.g_cal_u);
    apds9960_set_goffset_d(client, cali.g_cal_d);
    apds9960_set_goffset_l(client, cali.g_cal_l);
    apds9960_set_goffset_r(client, cali.g_cal_r);

    return count;
}

static ssize_t apds9960_show_proximity_enable(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d\n", data->enable_ps_sensor);
}

static ssize_t apds9960_store_proximity_enable(struct device_driver *ddri, const char *buf, size_t count)
{
    unsigned long val = simple_strtoul(buf, NULL, 10);

    APS_DBG("enable ps senosr (%ld)\n", val);

    if ((APDS_DISABLE_PS != val) && (APDS_ENABLE_PS != val))
    {
        APS_ERR("store invalid value=%ld\n", val);
        return count;
    }

    apds9960_enable_ps_sensor(apds9960_i2c_client, val);	

    return count;
}

static ssize_t apds9960_show_light_enable(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d\n", data->enable_als_sensor);
}

static ssize_t apds9960_store_light_enable(struct device_driver *ddri, const char *buf, size_t count)
{
    unsigned long val = simple_strtoul(buf, NULL, 10);

    APS_DBG("enable als sensor (%ld)\n", val);

    if ((APDS_DISABLE_ALS != val) && (APDS_ENABLE_ALS_WITH_INT != val) && (APDS_ENABLE_ALS_NO_INT != val))
    {
        APS_ERR("store invalid valeu=%ld\n", val);
        return count;
    }

    apds9960_enable_als_sensor(apds9960_i2c_client, val); 

    return count;
}

static ssize_t apds9960_show_gesture_enable(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d\n", data->enable_gesture_sensor);
}

static ssize_t apds9960_store_gesture_enable(struct device_driver *ddri, const char *buf, size_t count)
{
    unsigned long val = simple_strtoul(buf, NULL, 10);

    APS_DBG("enable gesture sensor (%ld)\n", val);

    if ((APDS_DISABLE_GESTURE != val) && (APDS_ENABLE_GESTURE != val))
    {
        APS_ERR("store invalid valeu=%ld\n", val);
        return count;
    }

    apds9960_enable_gesture_sensor(apds9960_i2c_client, val); 

    return count;
}

static ssize_t apds9960_show_reg(struct device_driver *ddri, char *buf)
{
    int i = 0;
    u8 	addr = 0x80;
    u8 	dat = 0;
    int len = 0;

    if (!apds9960_obj)
    {
        APS_ERR("apds9960_obj is null!!\n");
        return 0;
    }

    for (i = 0; i < 30; i++)
    {
        dat = i2c_smbus_read_byte_data(apds9960_i2c_client, addr);
        len += snprintf(buf+len,16,"Reg addr=%x ",addr);
        len += snprintf(buf+len,16,"Reg regdata=%x\n",dat);
        addr++;
        if (addr == 0x84)
        {
            addr += 0x04;
        }
        if (addr == 0x90)
        {
            addr += 0x03;
        }
        if (addr > 0x9F)
        {
            break;
        }
    }

    return len;
}

#define IS_SPACE(CH) (((CH) == ' ') || ((CH) == '\n'))
static int read_int_from_buf(struct apds9960_priv *obj, const char* buf, size_t count, u32 data[], int len)
{
    int idx = 0;
    char *cur = (char*)buf, *end = (char*)(buf+count);
    while (idx < len)
    {
        while ((cur < end) && IS_SPACE(*cur))
        {
            cur++;
        }
        if (1 != sscanf(cur, "%d", &data[idx]))
        {
            break;
        }
        idx++;
        while ((cur < end) && !IS_SPACE(*cur))
        {
            cur++;
        }
    }
    return idx;
}

static ssize_t apds9960_store_reg(struct device_driver *ddri, char *buf, ssize_t count)
{
    u32 data[2];
    if (read_int_from_buf(apds9960_obj, buf, count, data, 2) != 2)
    {
        APS_ERR("%s the buff is not available", __func__);
    }    
    APS_LOG("the data[0] =  %d, data[1] = %d ", data[0], data[1]);
    i2c_smbus_write_byte_data(apds9960_obj->client, data[0], data[1]);

    return count;
}

static ssize_t apds9960_show_high_threshold(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d\n", data->ps_threshold_high);
}

static ssize_t apds9960_show_low_threshold(struct device_driver *ddri, char *buf)
{
    struct apds9960_data *data = i2c_get_clientdata(apds9960_i2c_client);

    return sprintf(buf, "%d\n", data->ps_threshold_low);
}

//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
static ssize_t apds9960_store_incall_cali(struct device_driver *ddri, const char *buf , size_t count)
{
	APS_FUN();
	struct i2c_client *client = apds9960_i2c_client;
	struct apds9960_data *data = i2c_get_clientdata(client);
	int password = 0;
	int ret;
	
	ret = sscanf(buf, "%d", &password);
	switch (password) {
		case 17:
			need_incall_cali = true;
			if (APDS_ENABLE_PS == data->enable_ps_sensor)
			{
				bool_cali_flag = true;
    			wake_up_interruptible(&psensor_cali_head_wq);
			}
			break;

		case 18:
			need_incall_cali = false;
			break;
			
		case 27:
			is_hand_answer = true;
			break;

		case 28:
			is_hand_answer = false;

		default:
			need_incall_cali = false;
			is_hand_answer = false;
			break;	
	}
	APS_LOG("%s, ret : %d; password : %d ;\n", __func__, ret , password);
	return count;
}
static DRIVER_ATTR(incall_cali, S_IWUGO|S_IRUGO, NULL, apds9960_store_incall_cali);
#endif
//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end
static DRIVER_ATTR(cdata, S_IRUGO, apds9960_show_cdata, NULL);
static DRIVER_ATTR(rdata, S_IRUGO, apds9960_show_rdata, NULL);
static DRIVER_ATTR(gdata, S_IRUGO, apds9960_show_gdata, NULL);
static DRIVER_ATTR(bdata, S_IRUGO, apds9960_show_bdata, NULL);
static DRIVER_ATTR(pdata, S_IRUGO, apds9960_show_pdata, NULL);
static DRIVER_ATTR(alsps_cal, S_IWUGO|S_IRUGO, apds9960_show_alsps_cal, apds9960_store_alsps_cal);
static DRIVER_ATTR(proximity_enable, S_IWUGO|S_IRUGO, apds9960_show_proximity_enable, apds9960_store_proximity_enable);
static DRIVER_ATTR(light_enable, S_IWUGO|S_IRUGO, apds9960_show_light_enable, apds9960_store_light_enable);
static DRIVER_ATTR(gesture_enable, S_IWUGO|S_IRUGO, apds9960_show_gesture_enable, apds9960_store_gesture_enable);
static DRIVER_ATTR(reg, S_IWUSR|S_IRUGO, apds9960_show_reg, apds9960_store_reg);
static DRIVER_ATTR(high_threshold, S_IWUSR|S_IRUGO, apds9960_show_high_threshold, NULL);
static DRIVER_ATTR(low_threshold, S_IWUSR|S_IRUGO, apds9960_show_low_threshold, NULL);

static struct device_attribute *apds9960_attr_list[] = {
    &driver_attr_cdata,
    &driver_attr_rdata,
    &driver_attr_gdata,
    &driver_attr_bdata,
    &driver_attr_pdata,
    &driver_attr_alsps_cal,
    &driver_attr_proximity_enable,
    &driver_attr_light_enable,
    &driver_attr_gesture_enable,
    &driver_attr_reg,
    &driver_attr_high_threshold,
    &driver_attr_low_threshold,
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
	&driver_attr_incall_cali,
#endif
};

static int apds9960_create_attr(struct device_driver *driver)
{
    int idx, err = 0;
    int num = (int)(sizeof(apds9960_attr_list)/sizeof(apds9960_attr_list[0]));

    if (!driver)
    {
        APS_ERR("driver is NULL!\n");
        return -EINVAL;
    }

    for (idx = 0; idx < num; idx++)
    {
        if (err = driver_create_file(driver, apds9960_attr_list[idx]))
        {
            APS_ERR("driver_create_file (%s) = %d\n", apds9960_attr_list[idx]->attr.name, err);
            break;
        }
    }
    return err;
}

static int apds9960_delete_attr(struct device_driver *driver)
{
    int idx ,err = 0;
    int num = (int)(sizeof(apds9960_attr_list)/sizeof(apds9960_attr_list[0]));

    if (!driver)
    {
        APS_ERR("driver is NULL!\n");
        return -EINVAL;
    }

    for (idx = 0; idx < num; idx++)
    {
        driver_remove_file(driver, apds9960_attr_list[idx]);
    }

    return err;
}

/************************************************************/
static struct file_operations apds9960_ps_fops = {
    .owner = THIS_MODULE,
    .open = apds9960_ps_open,
    .release = apds9960_ps_release,
    .unlocked_ioctl = apds9960_ps_ioctl,
};

static struct miscdevice apds9960_ps_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "apds_ps_dev",
    .fops = &apds9960_ps_fops,
};

static struct file_operations apds9960_als_fops = {
    .owner = THIS_MODULE,
    .open = apds9960_als_open,
    .release = apds9960_als_release,
    .unlocked_ioctl = apds9960_als_ioctl,
};

static struct miscdevice apds9960_als_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "apds_als_dev",
    .fops = &apds9960_als_fops,
};

int apds9960_setup_eint(struct i2c_client *client)
{
    mt_set_gpio_dir(GPIO_ALS_EINT_PIN, GPIO_DIR_IN);
    mt_set_gpio_mode(GPIO_ALS_EINT_PIN, GPIO_ALS_EINT_PIN_M_EINT);
    mt_set_gpio_pull_enable(GPIO_ALS_EINT_PIN, TRUE);
    mt_set_gpio_pull_select(GPIO_ALS_EINT_PIN, GPIO_PULL_UP);

    mt_eint_set_hw_debounce(CUST_EINT_ALS_NUM, CUST_EINT_ALS_DEBOUNCE_CN);
    mt_eint_registration(CUST_EINT_ALS_NUM, CUST_EINT_ALS_TYPE, apds9960_interrupt, 0);

    return SUCCESS;
}

/*
 * Initialization function
 */
static int apds9960_init_client(struct i2c_client *client)
{
    struct apds9960_data *data = i2c_get_clientdata(client);
    int err;
    int id;

    err = apds9960_set_enable(client, APDS9960_PWR_DOWN);
    if (err < 0)
    {
        APS_ERR("apds9960_set_enable error!\n");
        return err;
    }

    id = i2c_smbus_read_byte_data(client, APDS9960_ID_REG);
    if (0x9F==id || 0x9C==id || 0xAB==id)
    {
        APS_LOG("It is APDS-9960\n");
    }
    else
    {
        APS_ERR("It is not APDS-9960\n");
        return -EIO;
    }

    err = apds9960_set_atime(client, apds9960_als_atime_tb[data->als_atime_index]);	
    if (err < 0)
        return err;

    err = apds9960_set_wtime(client, APDS9960_246_WAIT_TIME);	// 27ms Wait time
    if (err < 0)
        return err;

    err = apds9960_set_ppulse(client, data->ps_ppulse);	
    if (err < 0)
        return err;

    err = apds9960_set_poffset_ur(client, data->ps_poffset_ur);	
    if (err < 0)
        return err;

    err = apds9960_set_poffset_dl(client, data->ps_poffset_dl);	
    if (err < 0)
        return err;

    err = apds9960_set_config(client, 0x60);		// no long wait
    if (err < 0)
        return err;

    err = apds9960_set_control(client, APDS9960_PDRVIE_FOR_PS|APDS9960_PGAIN_FOR_PS|apds9960_als_again_bit_tb[data->als_again_index]);
    if (err < 0)
        return err;

    err = apds9960_set_pilt(client, data->ps_threshold_low);		// init threshold for proximity
    if (err < 0)
        return err;

    err = apds9960_set_piht(client, data->ps_threshold_high);
    if (err < 0)
        return err;

    err = apds9960_set_ailt(client, APDS9960_THRESHOLD_MAX);	// force first ALS interrupt to get the environment reading
    if (err < 0)
        return err;

    err = apds9960_set_aiht(client, APDS9960_THRESHOLD_MIN);
    if (err < 0)
        return err;

    err = apds9960_set_pers(client, APDS9960_PPERS_2|APDS9960_APERS_2);	// 2 consecutive persistence
    if (err < 0)
        return err;

    // gesture register
    err = apds9960_set_aux(client, APDS9960_GESTURE_LED_BOOST|0x01); // modify 20141008 chengx
    if (err < 0)
        return err;

    err = apds9960_set_config2(client, 0);
    if (err < 0)
        return err;

    err = apds9960_set_gthr_in(client, GESTURE_GTHR_IN);
    if (err < 0)
        return err;

    err = apds9960_set_gthr_out(client, GESTURE_GTHR_OUT);
    if (err < 0)
        return err;

    err = apds9960_set_gconf1(client, APDS9960_GESTURE_FIFO);
    if (err < 0)
        return err;

    err = apds9960_set_gconf2(client, APDS9960_GDRIVE|APDS9960_GGAIN|APDS9960_GTIME);
    if (err < 0)
        return err;

    err = apds9960_set_goffset_u(client, NO_OFFSET);
    if (err < 0)
        return err;

    err = apds9960_set_goffset_d(client, NO_OFFSET);
    if (err < 0)
        return err;

    err = apds9960_set_gpulse(client, (APDS9960_GPULSE-1)|APDS9960_GPULSE_LEN);
    if (err < 0)
        return err;

    err = apds9960_set_goffset_l(client, NO_OFFSET);
    if (err < 0)
        return err;

    err = apds9960_set_goffset_r(client, NO_OFFSET);
    if (err < 0)
        return err;

    err = apds9960_set_gconf3(client, 0);
    if (err < 0)
        return err;

    err = apds9960_set_gctrl(client, 0x04);
    if (err < 0)
        return err;

    // sensor is in disabled mode but all the configurations are preset
    err = apds9960_setup_eint(client);
    if(err != 0)
        return err;

    return SUCCESS;
}

/*----------------------------------------------------------------------------*/
static int apds9960_get_als_value(struct apds9960_data *obj, u16 als)
{
    int idx;

    for (idx = 0; idx < obj->als_level_num; idx++)
    {
        if (als < obj->hw->als_level[idx])
        {
            break;
        }
    }

    if (idx >= obj->als_value_num)
    {
        APS_ERR("APDS9930_get_als_value exceed range\n"); 
        idx = obj->als_value_num - 1;
    }
    APS_DBG("ALS: %05d => %05d， idx=%d\n", als, obj->hw->als_value[idx], idx);	
    return obj->hw->als_value[idx];

}

// Gionee BSP1 yang_yang modify for 20140802 CR01341808 begin
static int apds9960_read_ps(struct apds9960_data *data, u16 *value)
{
    struct i2c_client *client = data->client;
    int pdata;
    int tmp_data1, tmp_data2;
    APS_FUN();
    while(1)
    {
        pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
        tmp_data1 = pdata;

        pdata = i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);	
        tmp_data2 = pdata;

        if ((tmp_data2 - tmp_data1 > 27) || (tmp_data1 - tmp_data2 > 27))
            continue;

        break;
	}

    if (pdata < 0)
    {
        APS_ERR("I2C failed!\n");
        return -EFAULT;
    }
    *value = pdata;
    
    return SUCCESS;
}
// Gionee BSP1 yang_yang modify for 20140802 CR01341808 end

int apds9960_read_als(struct apds9960_data *data, u16 *value)
{
    struct i2c_client *client;
    unsigned char i2c_data[8];
    int status;

    APS_FUN();
    client = data->client;
    if (APDS_ENABLE_ALS_NO_INT != data->enable_als_sensor)
    {
        APS_ERR("enable_als_sensor != APDS_ENABLE_ALS_NO_INT!\n");
        return -EFAULT;
    }

    status = i2c_smbus_read_i2c_block_data(client, APDS9960_CDATAL_REG, 8, (unsigned char*)i2c_data);
    if ((status < 0) || (status != 8))
    {
        APS_ERR("I2C APDS9960_CDATAL_REG failed!\n");
        return -EFAULT;
    }

    data->cdata = (i2c_data[1]<<8)|i2c_data[0];
    data->rdata = (i2c_data[3]<<8)|i2c_data[2];
    data->gdata = (i2c_data[5]<<8)|i2c_data[4];
    data->bdata = (i2c_data[7]<<8)|i2c_data[6];
    APS_DBG("c:%d,r:%d,g:%d,b:%d", data->cdata, data->rdata, data->gdata, data->bdata);
    LuxCalculation(client);
    APS_DBG("lux:%d",data->lux);
    if (data->lux >= 0)
    {
        data->lux = data->lux<30000 ? data->lux : 30000;
    }
    *value = data->lux;
    return SUCCESS;
}

// Gionee BSP1 yang_yang 20140802 modify for CR01341808 begin
#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
static int apds9960_dynamic_calibrate(void)
{
    int ret = 0;
    int i = 0;
    u16 data = 0;
    unsigned int noise = 0;
    int max = 0;
    int idx_table = 0;
    unsigned int data_total = 0;
    struct apds9960_data *obj = apds9960_obj;

    APS_FUN();
    if (!obj)
    {
        APS_ERR("obj is NULL!\n");
		goto err;
    }

    for (i = 0; i < COUNT; i++)
    {
		if (max++ > 10)
        {
			obj->ps_threshold_high = APDS9960_THRESHOLD_MAX;
			obj->ps_threshold_low = APDS9960_THRESHOLD_MAX;

			obj->cali_noise = PS_MAX_VALUE;
			goto err;
		}
        mdelay(50);
        ret = apds9960_read_ps(obj, &data);
		if (ret != 0)
        {
			if (i > 0)
                i--;
			continue;
		}
        data_total += data;
    }
    noise = data_total/COUNT;

    for (idx_table = 0; idx_table < obj->ps_cali_noise_num; idx_table++)
    {
        if (noise <= obj->ps_cali_noise[idx_table])
            break;
    }

	if (idx_table >= obj->ps_cali_noise_num)
        goto err;

    obj->ps_threshold_high = obj->ps_cali_offset_high[idx_table] + noise;
    if (obj->ps_threshold_high >= APDS9960_THRESHOLD_MAX)
        obj->ps_threshold_high = APDS9960_THRESHOLD_MAX;

    obj->ps_threshold_low = obj->ps_cali_offset_low[idx_table] + noise;
    if (obj->ps_threshold_low >= APDS9960_THRESHOLD_MAX)
        obj->ps_threshold_low = APDS9960_THRESHOLD_MAX;

	obj->cali_noise = noise;
    if (i2c_smbus_write_byte_data(obj->client, APDS9960_PITHI_REG, obj->ps_threshold_high))
    {
        goto err;
    }

    if (i2c_smbus_write_byte_data(obj->client, APDS9960_PITLO_REG, obj->ps_threshold_low))
    {
        goto err;
    }

    APS_LOG("apds9960_dynamic_calibrate end:noise = %d, obj->ps_threshold_low = %d , obj->ps_threshold_high = %d\n",
                noise, obj->ps_threshold_low, obj->ps_threshold_high);

    return SUCCESS;

err:
    APS_ERR("apds9930_dynamic_calibrate fail!!!\n");
    return -EFAULT;
}
#endif
//Gionee BSP1 yang_yang 20140802 modify for CR01341808 end */

int apds9960_gesture_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
    int value;
    int err = 0;
    hwm_sensor_data* sensor_data;
    struct apds9960_data *obj = (struct apds9960_data *)self;
    struct i2c_client *client = apds9960_i2c_client;

    APS_FUN();
    if (!client)
    {
        APS_ERR("apds9960_ps_ioctl error: i2c driver not installed\n");
        return -EFAULT;
    }

    switch (command)
    {
        case SENSOR_DELAY:
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Set delay parameter error!\n");
                err = -EINVAL;
            }
            // Do nothing
            break;

        case SENSOR_ENABLE:
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Enable sensor parameter error!\n");
                err = -EINVAL;
            }
            else
            {	
                value = *(int *)buff_in;
                if (value)
                {
                    if ((err = apds9960_enable_gesture_sensor(client, APDS_ENABLE_GESTURE)))
                    {
                        APS_ERR("enable ps fail: %d\n", err); 
                        return -EIO;
                    }
                }
                else
                {
                    if ((err = apds9960_enable_gesture_sensor(client, APDS_DISABLE_GESTURE)))
                    {
                        APS_ERR("disable ps fail: %d\n", err); 
                        return -EIO;
                    }
                }
            }
            break;

        case SENSOR_GET_DATA:
            if ((NULL == buff_out) || (size_out< sizeof(hwm_sensor_data)))
            {
                APS_ERR("get sensor data parameter error!\n");
                err = -EINVAL;
            }
            break;

        default:
            APS_ERR("proxmy sensor operate function no this parameter %d!\n", command);
            err = -EINVAL;
            break;
    }

    return err;
}

int apds9960_ps_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
    int value;
    int err = 0;
    struct i2c_client *client = apds9960_i2c_client;
    hwm_sensor_data* sensor_data;
    struct apds9960_data *obj = (struct apds9960_data *)self;

    APS_FUN();
    if (!client)
    {
        APS_ERR("apds9960_ps_ioctl error: i2c driver not installed\n");
        return -EFAULT;
    }

    switch (command)
    {
        case SENSOR_DELAY:
			APS_DBG("SENSOR_DELAY\n");
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Set delay parameter error!\n");
                err = -EINVAL;
            }
            // Do nothing
            break;

        case SENSOR_ENABLE:
			APS_DBG("SENSOR_ENABLE\n");
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Enable sensor parameter error!\n");
                err = -EINVAL;
            }
            else
            {	
                value = *(int *)buff_in;
                APS_LOG("%s enable value:%d\n", __func__, value);
                if (value)
                {
                    if ((err = apds9960_enable_ps_sensor(client, APDS_ENABLE_PS)))
                    {
                        APS_ERR("enable ps fail: %d\n", err); 
                        return -EIO;
                    }
                }
                else
                {
                    if ((err = apds9960_enable_ps_sensor(client, APDS_DISABLE_PS)))
                    {
                        APS_ERR("disable ps fail: %d\n", err); 
                        return -EIO;
                    }
                }
            }
            break;

        case SENSOR_GET_DATA:
            if ((NULL == buff_out) || (size_out< sizeof(hwm_sensor_data)))
            {
                APS_ERR("get sensor data parameter error!\n");
                err = -EINVAL;
            }
            else
            {
                sensor_data = (hwm_sensor_data *)buff_out;	
				obj->ps_data =	i2c_smbus_read_byte_data(client, APDS9960_PDATA_REG);
				sensor_data->values[0] = apds9960_get_ps_value(obj, obj->ps_data);
                APS_LOG("%s value:%d", __func__, sensor_data->values[0]);
                sensor_data->value_divide = 1;
                sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;			
            }
            break;

        default:
            APS_ERR("proxmy sensor operate function no this parameter %d!\n", command);
            err = -EINVAL;
            break;
    }

    return err;
}

int apds9960_als_operate(void* self, uint32_t command, void* buff_in, int size_in,
		void* buff_out, int size_out, int* actualout)
{
    int err = 0;
    int value;
    hwm_sensor_data* sensor_data;
    struct apds9960_data *obj = (struct apds9960_data *)self;
    struct i2c_client *client = apds9960_i2c_client;

    if (!client)
    {
        APS_ERR("apds9960_ps_ioctl error: i2c driver not installed\n");
        return -EFAULT;
    }

    switch (command)
    {
        case SENSOR_DELAY:
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Set delay parameter error!\n");
                err = -EINVAL;
            }
            // Do nothing
            break;

        case SENSOR_ENABLE:
            if ((NULL == buff_in) || (size_in < sizeof(int)))
            {
                APS_ERR("Enable sensor parameter error!\n");
                err = -EINVAL;
            }
            else
            {
                value = *(int *)buff_in;	
                APS_LOG("%s, value:%d", __func__, value);
                if (value)
                {
                    if ((err = apds9960_enable_als_sensor(client, APDS_ENABLE_ALS_NO_INT)))
                    {
                        APS_ERR("enable als fail: %d\n", err); 
                        return -EIO;
                    }
                }
                else
                {
                    if ((err = apds9960_enable_als_sensor(client, APDS_DISABLE_ALS)))
                    {
                        APS_ERR("disable als fail: %d\n", err); 
                        return -EIO;
                    }
                }
            }
            break;

        case SENSOR_GET_DATA:
            if ((NULL == buff_out) || (size_out< sizeof(hwm_sensor_data)))
            {
                APS_ERR("get sensor data parameter error!\n");
                err = -EINVAL;
            }
            else
            {
                sensor_data = (hwm_sensor_data *)buff_out;
                apds9960_read_als(obj, &obj->als_data);

                //Gionee BSP1 yangqb 20140912 modify for CR01380122 LCM MODULE AAL SUPPORT start
                #ifdef MTK_AAL_SUPPORT
                sensor_data->values[0] = obj->als_data;
                #else
                sensor_data->values[0] = apds9960_get_als_value(obj, obj->als_data);
                #endif
                //Gionee BSP1 yangqb 20140912 modify for CR01380122 LCM MODULE AAL SUPPORT end

                APS_LOG("%s,sensor_data->values[0]:%d", __func__, sensor_data->values[0]);
                sensor_data->value_divide = 1;
                sensor_data->status = SENSOR_STATUS_ACCURACY_MEDIUM;
            }
            break;

        default:
            APS_ERR("light sensor operate function no this parameter %d!\n", command);
            err = -EINVAL;
            break;
    }

    return err;
}

/****************************************************************************** 
 * Function Configuration
 ******************************************************************************/
static int apds9960_get_ps_value(struct apds9960_data *obj, unsigned int ps)
{
    int val;
    static int val_temp = FAR;

    if (ps > obj->ps_threshold_high)
    {
        val = CLOSE;
        val_temp = CLOSE;
    }
    else if (ps < obj->ps_threshold_low)
    {
        val = FAR;
        val_temp = FAR;
    }
    else
    {
        val = val_temp;
    }

    return val;
}

//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
static int thread_psensor_cali(void *client)
{
	struct apds9960_data *data = i2c_get_clientdata((struct i2c_client *)client);
	int err = 0;

    while (1) {
        wait_event_interruptible(psensor_cali_head_wq, (bool_cali_flag == true));
		if ((APDS_ENABLE_PS == data->enable_ps_sensor) && need_incall_cali)
		{
			if (!is_hand_answer)
			{
				APS_LOG("incall calibrate, ps is enable \n");
#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
				apds9960_dynamic_calibrate();
#endif
				need_incall_cali = false;
			}
			else
			{
				APS_LOG("incall calibrate, is_hand_answer, ignore \n");
				is_hand_answer = false;
			}
		}	
		bool_cali_flag = false;
    }

    return 0;
}

#endif
//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end
static int apds9960_open(struct inode *inode, struct file *file)
{
    file->private_data = apds9960_i2c_client;

    if (!file->private_data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    return nonseekable_open(inode, file);
}

static int apds9960_release(struct inode *inode, struct file *file)
{
    file->private_data = NULL;

    return SUCCESS;
}

static long apds9960_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct i2c_client *client = (struct i2c_client*)file->private_data;
    struct apds9960_data *obj = i2c_get_clientdata(client);  
    long err = 0;
    void __user *ptr = (void __user*) arg;
    int dat;
    uint32_t enable;
    int ps_result;
    int alsps_cali;
    int threshold[2];
    int i;
    int ps_noise[8];

#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
    struct PS_CALI_DATA_STRUCT ps_cali_temp = {{0,0,0},};
#endif
    struct alsps_cal cal_data = {0};

    switch (cmd)
    {
        case ALSPS_SET_PS_MODE:
            if (copy_from_user(&enable, ptr, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            if (enable)
            {
                if ((err = apds9960_enable_ps_sensor(obj->client, APDS_ENABLE_PS)))
                {
                    APS_ERR("enable ps fail: %ld\n", err); 
                    goto err_out;
                }

            }
            else
            {
                if ((err = apds9960_enable_ps_sensor(obj->client, APDS_DISABLE_PS)))
                {
                    APS_ERR("disable ps fail: %ld\n", err); 
                    goto err_out;
                }

            }
            break;

        case ALSPS_GET_PS_MODE:
            enable = obj->enable_ps_sensor;
            if (copy_to_user(ptr, &enable, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            break;

        case ALSPS_GET_PS_DATA:    
            obj->ps_data =	i2c_smbus_read_byte_data(obj->client, APDS9960_PDATA_REG);
            dat = apds9960_get_ps_value(obj, obj->ps_data);
            if (copy_to_user(ptr, &dat, sizeof(dat)))
            {
                err = -EFAULT;
                goto err_out;
            }  
            break;

        case ALSPS_GET_PS_RAW_DATA:    		
            dat = i2c_smbus_read_byte_data(obj->client, APDS9960_PDATA_REG);
            if (copy_to_user(ptr, &dat, sizeof(dat)))
            {
                err = -EFAULT;
                goto err_out;
            }  
            break;              

        case ALSPS_SET_ALS_MODE:
            if (copy_from_user(&enable, ptr, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            if (enable)
            {
                if (err = apds9960_enable_als_sensor(client, APDS_ENABLE_ALS_NO_INT))
                {
                    APS_ERR("enable als fail: %ld\n", err); 
                    goto err_out;
                }
            }
            else
            {
                if (err = apds9960_enable_als_sensor(client, APDS_DISABLE_ALS))
                {
                    APS_ERR("disable als fail: %ld\n", err); 
                    goto err_out;
                }
            }
            break;

        case ALSPS_GET_ALS_MODE:
            enable = obj->enable_als_sensor;
            if (copy_to_user(ptr, &enable, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            break;

        case ALSPS_GET_ALS_DATA: 
            apds9960_read_als(obj, &obj->als_data);
            dat = apds9960_get_als_value(obj, obj->als_data);
            if (copy_to_user(ptr, &dat, sizeof(dat)))
            {
                err = -EFAULT;
                goto err_out;
            }              
            break;

        case ALSPS_GET_ALS_RAW_DATA:    
            apds9960_read_als(obj, &obj->als_data);
            dat = obj->als_data;
            if (copy_to_user(ptr, &dat, sizeof(dat)))
            {
                err = -EFAULT;
                goto err_out;
            }              
            break;

        // for factory mode test
        case ALSPS_GET_PS_TEST_RESULT:
            dat = i2c_smbus_read_byte_data(obj->client, APDS9960_PDATA_REG);
            if (dat > obj->ps_threshold_high)
            {
                ps_result = 0;
            }
            else
                ps_result = 1;

            if (copy_to_user(ptr, &ps_result, sizeof(ps_result)))
            {
                err = -EFAULT;
                goto err_out;
            }			   
            break;

        case ALSPS_IOCTL_CLR_CALI:
            break;

        case ALSPS_IOCTL_GET_CALI:
            cal_data = obj->cal ;
            if (copy_to_user(ptr, &cal_data, sizeof(cal_data)))
            {
                err = -EFAULT;
                goto err_out;
            }

            break;

        case ALSPS_IOCTL_SET_CALI:
            if (copy_from_user(&alsps_cali, ptr, sizeof(alsps_cali)))
            {
                err = -EFAULT;
                goto err_out;
            }

            break;

        case ALSPS_SET_PS_THRESHOLD:
            break;

        case ALSPS_GET_PS_THRESHOLD_HIGH:
            threshold[0] = obj->ps_threshold_high;
            APS_LOG("%s get threshold high: 0x%x\n", __func__, threshold[0]); 
            if (copy_to_user(ptr, &threshold[0], sizeof(threshold[0])))
            {
                err = -EFAULT;
                goto err_out;
            }

            break;

        case ALSPS_GET_PS_THRESHOLD_LOW:
            threshold[0] = obj->ps_threshold_low;
            APS_LOG("%s get threshold low: 0x%x\n", __func__, threshold[0]); 
            if (copy_to_user(ptr, &threshold[0], sizeof(threshold[0])))
            {
                err = -EFAULT;
                goto err_out;
            }

            break;
#if 0

        case ALSPS_SET_GESTURE_MODE:
            if (copy_from_user(&enable, ptr, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            if (enable)
            {
                if (err = apds9960_enable_gesture_sensor(client, APDS_ENABLE_GESTURE))
                {
                    APS_ERR("enable ps fail: %ld\n", err); 
                    goto err_out;
                }
            }
            else
            {
                if (err = apds9960_enable_gesture_sensor(client, APDS_DISABLE_GESTURE))
                {
                    APS_ERR("disable ps fail: %ld\n", err); 
                    goto err_out;
                }
            }
            break;

        case ALSPS_GET_GESTURE_MODE:
            enable = obj->enable_gesture_sensor;
            if (copy_to_user(ptr, &enable, sizeof(enable)))
            {
                err = -EFAULT;
                goto err_out;
            }
            break;

        case ALSPS_GET_GESTURE_DATA:
            dat = gesture_motion;
            if (copy_to_user(ptr, &dat, sizeof(dat)))
            {
                err = -EFAULT;
                goto err_out;
            }  
            break;

        // Gionee BSP1 yang_yang 20140802 modify for CR01341808 begin
        case ALSPS_GET_PS_NOISE:
            APS_LOG("%s command ALSPS_GET_PS_NOISE = 0x%0x4\n", __FUNCTION__, ALSPS_GET_PS_NOISE);
            if (err = apds9960_enable_ps_sensor(obj->client, APDS_ENABLE_PS))
            {
                APS_ERR("ads9960 ioctl enable ps fail: %ld\n", err);
            }
            for (i=0; i<8; i++)
            {
                apds9960_read_ps(obj, &ps_noise[i]);
                msleep(50);
                APS_LOG("apds9960_read_ps noise[i]= %d\n", ps_noise[i]);
            }

            if (err = apds9960_enable_ps_sensor(obj->client, APDS_DISABLE_PS))
            {
                APS_ERR("ads9960 ioctl enable ps fail: %ld\n", err);
            }

            if (copy_to_user(ptr, ps_noise, sizeof(ps_noise)))
            {	
                err = -EFAULT;
                goto err_out;
            }	 
            break;

#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
        case ALSPS_SET_PS_CALI:
            dat = (void __user*)arg;
            if (NULL == dat)
            {
                APS_ERR("dat is NULL\n");
                return -EINVAL;
            }
            if (copy_from_user(&ps_cali_temp, dat, sizeof(ps_cali_temp)))
            {
                APS_ERR("alsps set ps cali_copy from user fail\n");
                return -EFAULT;
            }

            apds9960_WriteCalibration(&ps_cali_temp);
            APS_LOG(" ALSPS_SET_PS_CALI %d,%d,%d\t", ps_cali_temp.close, ps_cali_temp.far_away, ps_cali_temp.valid);
            break;

		case ALSPS_GET_PS_CALI:
            if (err = apds9960_enable_ps_sensor(client, APDS_ENABLE_PS))
            {
                APS_ERR("ads9930 ioctl enable ps fail: %d\n", err);
            }
            else
            {
                err =apds9960_dynamic_calibrate();
                if (err < 0)
                {
                    ps_cali_temp.valid = CALIBRATION_DATA_INVALID;
                }
                else
                {
                    ps_cali_temp.valid = CALIBRATION_DATA_VALID;
                }
                ps_cali_temp.close = obj->ps_threshold_high;
                ps_cali_temp.far_away = obj->ps_threshold_low;
            }

            if (err = apds9960_enable_ps_sensor(client, APDS_DISABLE_PS))
            {
                APS_ERR("apds9930 ioctl disable ps fail: %d\n", err);
            }

            if (copy_to_user((void __user *)arg, &ps_cali_temp, sizeof(ps_cali_temp)))
            {
                APS_ERR("alsps get ps cali_copy from user fail\n");
                return -EFAULT;
            }
            APS_LOG("apds9960 ALSPS_GET_PS_CALI %d,%d,%d\t",ps_cali_temp.close, ps_cali_temp.far_away,ps_cali_temp.valid);
            break;
#endif
        // Gionee BSP1 yang_yang 20140802 modify for CR01341808 end
#endif
        default:
            APS_ERR("%s not supported = 0x%04x", __FUNCTION__, cmd);
            err = -ENOIOCTLCMD;
            break;
    }

err_out:
    return err;    
}

/*----------------------------------------------------------------------------*/
static struct file_operations apds9960_fops = {
    .owner = THIS_MODULE,
    .open = apds9960_open,
    .release = apds9960_release,
    .unlocked_ioctl = apds9960_unlocked_ioctl,
};

static struct miscdevice apds9960_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "als_ps",
    .fops = &apds9960_fops,
};

/*----------------------------------------------------------------------------*/
static int apds9960_i2c_detect(struct i2c_client *client, struct i2c_board_info *info) 
{    
    strcpy(info->type, APDS9960_DEV_NAME);
    return SUCCESS;
}

/*----------------------------------------------------------------------------*/
static int  apds9960_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct i2c_adapter *adapter = to_i2c_adapter(client->dev.parent);
    struct apds9960_data *data;
    struct hwmsen_object obj_ps, obj_als, obj_gesture;
    int err = 0;

    if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE))
    {
        err = -EIO;
		APS_ERR("i2c_check_functionality() failed!\n");
        goto exit;
    }

	// Gionee BSP1 chengx 20140826 modify for CR01371160 begin
#ifdef GN_MTK_BSP_DEVICECHECK
    struct gn_device_info gn_dev_info_lsensor = {0};
    gn_dev_info_lsensor.gn_dev_type = GN_DEVICE_TYPE_LIGHT;
    strcpy(gn_dev_info_lsensor.name, APDS9960_DEV_NAME);
    gn_set_device_info(gn_dev_info_lsensor);
#endif
    // Gionee BSP1 chengx 20140826 modify for CR01371160 end

    data = kzalloc(sizeof(struct apds9960_data), GFP_KERNEL);
    if (!data)
    {
        err = -ENOMEM;
		APS_ERR("kzalloc memory failed!\n");
        goto exit;
    }
    apds9960_obj = data;
    data->client = client;
    apds9960_i2c_client = client;

    i2c_set_clientdata(client, data);

    data->hw = get_cust_alsps_hw();
    data->enable = 0;                   // default mode is standard
    data->enable_als_sensor = 0;        // default to 0
    data->enable_ps_sensor = 0;         // default to 0
    data->enable_gesture_sensor = 0;	// default to 0

    data->als_atime_index = APDS9960_ALS_RES_24MS;  // APDS9960_ALS_RES_100MS;	// 100ms ATIME
    data->als_again_index = APDS9960_ALS_GAIN_1X;	// 1x AGAIN
    data->enable_suspended_value = 0;

    data->ps_threshold_high = data->hw->ps_threshold_high;
    data->ps_threshold_low = data->hw->ps_threshold_low;
    data->gesture_ppulse = (APDS9960_PPULSE_FOR_GESTURE-1)|APDS9960_PPULSE_LEN_FOR_GESTURE;
    data->ps_ppulse = (APDS9960_PPULSE_FOR_PS-1)|APDS9960_PPULSE_LEN_FOR_PS;
    data->ps_poffset_ur = NO_OFFSET;
    data->ps_poffset_dl = NO_OFFSET;

    data->RGB_COE_X[0] = RGB_COE_X[0];
    data->RGB_COE_X[1] = RGB_COE_X[1];
    data->RGB_COE_X[2] = RGB_COE_X[2];

    data->RGB_COE_Y[0] = RGB_COE_Y[0];
    data->RGB_COE_Y[1] = RGB_COE_Y[1];
    data->RGB_COE_Y[2] = RGB_COE_Y[2];

    data->RGB_COE_Z[0] = RGB_COE_Z[0];
    data->RGB_COE_Z[1] = RGB_COE_Z[1];
    data->RGB_COE_Z[2] = RGB_COE_Z[2];

    data->als_level_num = sizeof(data->hw->als_level)/sizeof(data->hw->als_level[0]);
    data->als_value_num = sizeof(data->hw->als_value)/sizeof(data->hw->als_value[0]);
    BUG_ON(sizeof(data->als_level) != sizeof(data->hw->als_level));
    memcpy(data->als_level, data->hw->als_level, sizeof(data->als_level));
    BUG_ON(sizeof(data->als_value) != sizeof(data->hw->als_value));
    memcpy(data->als_value, data->hw->als_value, sizeof(data->als_value));

    // Gionee BSP1 yang_yang 20140802 modify for CR01341808 begin
#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
	data->cali_noise = 0;
    data->ps_cali_noise_num =  sizeof(data->hw->ps_cali_noise)/sizeof(data->hw->ps_cali_noise[0]);
    data->ps_cali_offset_high_num =	sizeof(data->hw->ps_cali_offset_high)/sizeof(data->hw->ps_cali_offset_high[0]);
    data->ps_cali_offset_low_num =  sizeof(data->hw->ps_cali_offset_low)/sizeof(data->hw->ps_cali_offset_low[0]);

    BUG_ON(sizeof(data->ps_cali_noise) != sizeof(data->hw->ps_cali_noise));
    memcpy(data->ps_cali_noise, data->hw->ps_cali_noise, sizeof(data->ps_cali_noise));

    BUG_ON(sizeof(data->ps_cali_offset_high) != sizeof(data->hw->ps_cali_offset_high));
    memcpy(data->ps_cali_offset_high, data->hw->ps_cali_offset_high, sizeof(data->ps_cali_offset_high));

    BUG_ON(sizeof(data->ps_cali_offset_low) != sizeof(data->hw->ps_cali_offset_low));
    memcpy(data->ps_cali_offset_low, data->hw->ps_cali_offset_low, sizeof(data->ps_cali_offset_low));
#endif
	// Gionee BSP1 yang_yang 20140802 modify for CR01341808 end

    // Gionee BSP chengx 20141122 modify for CR01406932 begin
    data->gesture_psensor_noise = 0;
    // Gionee BSP chengx 20141122 modify for CR01406932 end

    obj_ps.self = apds9960_obj;
    obj_ps.polling = 0;
    obj_ps.sensor_operate = apds9960_ps_operate;
    if ((err = hwmsen_attach(ID_PROXIMITY, &obj_ps)))
    {
        APS_ERR("attach fail = %d\n", err);
        goto exit_unregister_sensor_operate;
    }

    obj_als.self = apds9960_obj;
    obj_als.polling = 1;
    obj_als.sensor_operate = apds9960_als_operate;
    if ((err = hwmsen_attach(ID_LIGHT, &obj_als)))
    {
        APS_ERR("attach fail = %d\n", err);
        goto exit_unregister_sensor_operate;
    }
#if 0
    obj_gesture.self = apds9960_obj;
    obj_gesture.polling = 0;
    obj_gesture.sensor_operate = apds9960_gesture_operate;
    if ((err = hwmsen_attach(ID_GESTURE, &obj_gesture)))
    {
        APS_ERR("attach fail = %d\n", err);
        goto exit_unregister_sensor_operate;
    }
#endif
    INIT_DELAYED_WORK(&data->psensor_dwork, apds9960_work_handler);
    INIT_DELAYED_WORK(&data->gesture_dwork, apds9960_gesture_reporting_handler); 

    // Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
    //INIT_DELAYED_WORK(&data->xtalk_dwork, apds9960_xtalk_monitoring_handler); 
    // Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin

//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
#if defined(GN_MTK_BSP_PS_CALIBRATE_INCALL)
	kthread_run(thread_psensor_cali, client, "psensor_cali_thread");
#endif
//Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end

    // Initialize the APDS9960 chip
    err = apds9960_init_client(client);
    if (err)
    {
        APS_ERR("apds9960_init_client error!\n");
        goto exit_init_client_failed;
    }

    // Register sysfs hooks 
    if (err = apds9960_create_attr(&apds9960_alsps_driver.driver))
    {
        APS_ERR("create attribute err = %d\n", err);
        goto exit_create_attr_failed;
    }
    APS_LOG("%s before misc_register \n", __func__);

    // Register for sensor ioctl
    if ((err = misc_register(&apds9960_device)))
    {
        APS_ERR("APDS9930_device register failed\n");
        goto exit_misc_device_register_failed;
    }

    if ((err = misc_register(&apds9960_ps_device)))
    {
        APS_ERR("Unalbe to register ps ioctl: %d", err);
        goto exit_misc_ps_device_register_failed;
    }

    if ((err = misc_register(&apds9960_als_device)))
    {
        APS_ERR("Unalbe to register als ioctl: %d", err);
        goto exit_misc_als_device_register_failed;
    }

#if defined(CONFIG_HAS_EARLYSUSPEND)
    data->early_drv.level    = EARLY_SUSPEND_LEVEL_DISABLE_FB - 1,
    data->early_drv.suspend  = apds9960_early_suspend,
    data->early_drv.resume   = apds9960_late_resume,    
    register_early_suspend(&data->early_drv);
#endif
    APS_LOG("%s support ver. %s enabled\n", __func__, DRIVER_VERSION);

    return SUCCESS;

exit_misc_als_device_register_failed:
    misc_deregister(&apds9960_ps_device);
exit_misc_ps_device_register_failed:
    misc_deregister(&apds9960_device);
exit_misc_device_register_failed:
    apds9960_delete_attr(&apds9960_alsps_driver.driver);
exit_create_attr_failed:
exit_init_client_failed:
exit_unregister_sensor_operate:
    kfree(data);
    apds9960_obj = NULL;
    apds9960_i2c_client = NULL;
exit:
    return err;
}

static int  apds9960_i2c_remove(struct i2c_client *client)
{
    APS_FUN();
    struct apds9960_data *data = i2c_get_clientdata(client);
    if (!data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    cancel_delayed_work(&data->psensor_dwork);
    cancel_delayed_work(&data->gesture_dwork);

    // Gionee BSP1 yang_yang 20140814 modify for CR01357229 begin
    //cancel_delayed_work(&data->xtalk_dwork);
    // Gionee BSP1 yang_yang 20140814 modify for CR01357229 end

    // Power down the device
    apds9960_set_enable(client, APDS9960_PWR_DOWN);

    misc_deregister(&apds9960_als_device);
    misc_deregister(&apds9960_ps_device);	

    apds9960_delete_attr(&apds9960_alsps_driver.driver);

    apds9960_i2c_client = NULL;

    kfree(data);

    return SUCCESS;
}

#if !defined(CONFIG_HAS_EARLYSUSPEND)
static int apds9960_i2c_suspend(struct i2c_client *client, pm_message_t mesg)
{
    APS_FUN(); 
    struct apds9960_data *data = i2c_get_clientdata(client);
    if (!data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    if (PM_EVENT_SUSPEND == msg.event)
    {
        // Do nothing as p-sensor is in active
        if (data->enable_ps_sensor || data->enable_gesture_sensor)
        {
            APS_LOG("ps-sensor is enable!\n");
            return SUCCESS;
        }
        else if (data->enable_als_sensor)
        {
	        data->enable_suspended_value = data->enable_als_sensor;
        }

		apds9960_enable_als_sensor(client, APDS_DISABLE_ALS);
        apds9960_clear_interrupt(client, CMD_CLR_ALL_INT);
    }

    APS_DBG("enable_suspended_value=%d\n", data->enable_suspended_value);
    return SUCCESS;
}

static int apds9960_i2c_resume(struct i2c_client *client)
{
    APS_FUN();
    int err = 0;
    struct apds9960_data *data = i2c_get_clientdata(client);;
    if (!data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    // Do nothing as it was not suspended
    APS_LOG("apds9960_late_resume (enable=%d)\n", data->enable_suspended_value);
    if (data->enable_ps_sensor || data->enable_gesture_sensor)
    {
        APS_LOG("ps-sensor is enable!\n");
        return SUCCESS;
    }
	else if (!data->enable_als_sensor)
	{
		err = apds9960_enable_als_sensor(client, data->enable_suspended_value);
		if (err < 0)
		{
			APS_ERR("enable set Fail\n");
        	return -EINVAL;
		}
	}

    apds9960_clear_interrupt(client, CMD_CLR_ALL_INT);	// clear pending interrupt

    return SUCCESS;
}

#else

static void apds9960_early_suspend(struct early_suspend *h) 
{
    APS_FUN(); 
    struct apds9960_data *data = container_of(h, struct apds9960_data, early_drv);
    if (!data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    struct i2c_client *client = data->client;
    if (!client)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

	if (data->enable_als_sensor)
	{
		data->enable_suspended_value = data->enable_als_sensor;
		apds9960_enable_als_sensor(client, APDS_DISABLE_ALS);
	}

    // Do nothing as p-sensor is in active
    if (data->enable_ps_sensor || data->enable_gesture_sensor)
    {
        APS_LOG("ps-sensor is enable!\n");
    }
	else
	{
		apds9960_clear_interrupt(client, CMD_CLR_ALL_INT);
	}

    APS_DBG("enable_suspended_value=%d\n", data->enable_suspended_value);
    return;
}

static void apds9960_late_resume(struct early_suspend *h)
{
    APS_FUN();
    int err = 0;
    struct apds9960_data *data = container_of(h, struct apds9960_data, early_drv);
    if (!data)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    struct i2c_client *client = data->client;
    if (!client)
    {
        APS_ERR("null pointer!!\n");
        return -EINVAL;
    }

    APS_LOG("apds9960_late_resume (enable=%d)\n", data->enable_suspended_value);
	if (!data->enable_als_sensor)
	{
		err = apds9960_enable_als_sensor(client, data->enable_suspended_value);
		if (err < 0)
		{
			APS_ERR("enable set Fail\n");
		}
	}

    // Do nothing as it was not suspended
    if (data->enable_ps_sensor || data->enable_gesture_sensor)
    {
        APS_LOG("ps-sensor is enable!\n");
    }
	else
	{
		apds9960_clear_interrupt(client, CMD_CLR_ALL_INT);	// clear pending interrupt
	}

	return;
}
#endif

/*----------------------------------------------------------------------------*/
static void apds9960_power(struct alsps_hw *hw, unsigned int on) 
{
    static unsigned int power_on = POWER_OFF;

    APS_DBG("power %s\n", on ? "on" : "off");
    if (POWER_NONE_MACRO != hw->power_id)
    {
        if (power_on == on)
        {
            APS_DBG("ignore power control: %d\n", on);
			return;
        }
        else if (on)
        {
            APS_DBG("hw->power_id:%d, hw->power_vol:%d\n", hw->power_id, hw->power_vol);
            if (!hwPowerOn(hw->power_id, hw->power_vol, APDS9960_DEV_NAME)) 
            {
                APS_ERR("power on fails!!\n");
            }
        }
        else
        {
            if (!hwPowerDown(hw->power_id, APDS9960_DEV_NAME)) 
            {
                APS_ERR("power off fail!!\n");   
            }
        }
    }
    power_on = on;
}

/*----------------------------------------------------------------------------*/
static int apds9960_probe(struct platform_device *pdev) 
{
    APS_FUN();
    struct alsps_hw *hw = get_cust_alsps_hw();
    if (!hw)
    {
        APS_ERR("get_cust_alsps_hw() failed!\n");
        return -ENODEV;
    }

    apds_workqueue = create_workqueue("proximity_als");
    if (!apds_workqueue)
    {
        APS_ERR("create_workqueue proximity_als failed!\n");
        return -ENOMEM;
    }

    apds9960_power(hw, POWER_ON);
	// when power on, allow 5.7ms for device initialization before issuing I2C commands to the APDS-9960
	msleep(6);
    if (i2c_add_driver(&apds9960_i2c_driver))
    {
        APS_ERR("add driver error!\n");
        return -ENODEV;
    } 

    APS_LOG("%s exit!\n", __func__);
    return SUCCESS;
}

static int apds9960_remove(struct platform_device *pdev)
{
    APS_FUN(); 
    struct alsps_hw *hw = get_cust_alsps_hw();
    if (!hw)
    {
        APS_ERR("get_cust_alsps_hw() failed!\n");
        return -ENODEV;
    }

    if (apds_workqueue)
    {
        destroy_workqueue(apds_workqueue);
    }

    apds_workqueue = NULL;
    apds9960_power(hw, POWER_OFF);    
    i2c_del_driver(&apds9960_i2c_driver);

    return SUCCESS;
}

static int __init apds9960_init(void)
{
    APS_FUN();
    struct alsps_hw *hw = get_cust_alsps_hw();
    if (!hw)
    {
        APS_ERR("get_cust_alsps_hw() failed!\n");
        return -ENODEV;
    }
    APS_DBG("i2c_number=%d\n", hw->i2c_num);

    i2c_register_board_info(hw->i2c_num, &i2c_apds9960, 1);

    if (platform_driver_register(&apds9960_alsps_driver))
    {
        APS_ERR("failed to register driver!\n");
        return -ENODEV;
    }

    return SUCCESS;
}

static void __exit apds9960_exit(void)
{
    APS_FUN();
    platform_driver_unregister(&apds9960_alsps_driver);
}

MODULE_AUTHOR("Lee Kai Koon <kai-koon.lee@avagotech.com>");
MODULE_DESCRIPTION("apds9960 gesture + RGB + ambient light + proximity sensor driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRIVER_VERSION);

module_init(apds9960_init);
module_exit(apds9960_exit);

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/input.h> /* BUS_I2C */
#include <linux/input-polldev.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>
#include <linux/string.h>
#include <linux/gpio.h>
#include <linux/regulator/consumer.h>
#include <linux/of_gpio.h>
#include <linux/workqueue.h>

#include <linux/iio/buffer.h>
#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>
#include <linux/iio/trigger.h>
#include <linux/iio/trigger_consumer.h>
#include <linux/iio/kfifo_buf.h>
#include <linux/irq_work.h>

#include "CwMcuSensor.h"
#include <mach/mt_typedefs.h>
#include <mach/mt_gpio.h>
#include <mach/mt_pm_ldo.h>
#include <mach/eint.h>
#include <cust_eint.h>
#include <linux/dma-mapping.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/sensors_io.h>
#include <linux/platform_device.h>
#include <linux/delay.h>

// Gionee chengx 20150429 add for CR01461753 begin
#include <linux/earlysuspend.h>
// Gionee chengx 20150429 add for CR01461753 begin

#undef CONFIG_HAS_EARLYSUSPEND

#define SENSOR_HUB_TAG                  "[SensorHub] "
#define DEBUG                           1
#if defined(DEBUG)
#define SH_FUN(f)                       printk(KERN_INFO SENSOR_HUB_TAG"%s\n", __FUNCTION__)
#define SH_ERR(fmt, args...)            printk(KERN_ERR  SENSOR_HUB_TAG"%s %d ERROR: "fmt, __FUNCTION__, __LINE__, ##args)
#define SH_LOG(fmt, args...)            printk(KERN_ERR  SENSOR_HUB_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define SH_DBG(fmt, args...)            printk(KERN_INFO SENSOR_HUB_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#else
#define SH_FUN(f)                       printk(KERN_INFO SENSOR_HUB_TAG"%s\n", __FUNCTION__)
#define SH_ERR(fmt, args...)            printk(KERN_ERR  SENSOR_HUB_TAG"%s %d ERROR: "fmt, __FUNCTION__, __LINE__, ##args)
#define SH_LOG(fmt, args...)            printk(KERN_ERR  SENSOR_HUB_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define SH_DBG(fmt, args...)
#endif

#define CWMCU_MUTEX
#define QueueSystemInfoMsgSize  30
#define QueueWarningMsgSize		30
#define ACK		0x79
#define NACK    0x1F

#define CWMCU_POLL_MAX      2000

/* Input poll interval in milliseconds */
#define FT_VTG_MIN_UV		2600000
#define FT_VTG_MAX_UV		3300000
#define FT_I2C_VTG_MIN_UV	1800000
#define FT_I2C_VTG_MAX_UV	1800000

/* turn on gpio interrupt if defined */
#define CWMCU_INTERRUPT

#define CWMCU_CALIB_SAVE_IN_FLASH

//#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
struct PS_CALI_DATA_STRUCT
{
	int close;
	int far_away;
	int valid;
};

static struct PS_CALI_DATA_STRUCT ps_cali={{0,0,0},};
//#endif

#define AVAGO9960_PS_MAX 255
static int ps_cali_noise[]       = { 100, 200, 255};
static int ps_cali_offset_high[] = {  70,  40,  10};
static int ps_cali_offset_low[]  = {  60,  30,   5};
static int ps_cali_noise_num = sizeof(ps_cali_noise)/sizeof(ps_cali_noise[0]);
static int gionee_psensor_calibration(void);

static bool is_handanswer = false;
static bool need_incall_cali = false;
static void gionee_incall_ps_calibration(void);

static int skip_mag_data = 0;

struct CWMCU_T {
	struct i2c_client *client;
	struct regulator *vdd;
	struct regulator *vcc_i2c;
	struct workqueue_struct *driver_wq;
	struct work_struct work;
    struct delayed_work delay_work;
	struct CWMCU_SENSORS_INFO sensors_info[HANDLE_ID_END][SENSORS_ID_END];
	SensorsInit_T	hw_info[DRIVER_ID_END];
    RegInformation *pReadRegInfo;
    RegInformation *pWriteRegInfo;
	u8 m_cReadRegCount;
	u8 m_cWriteRegCount;
    uint8_t initial_hw_config;
	
	int mcu_mode;
	uint8_t kernel_status;

	/* enable & batch list */
	uint32_t enabled_list[HANDLE_ID_END];
	uint32_t interrupt_status;
    uint8_t calibratordata[DRIVER_ID_END][30];
    uint8_t calibratorUpdate[DRIVER_ID_END];
    
	/* Mcu site enable list*/

	/* power status */
    volatile uint32_t power_on_list;

	/* Calibrator status */
	int cal_cmd;
	int cal_type;
	int cal_id;

	uint32_t debug_log;
	
	int cmd;
	uint32_t addr;
	int len;
	int mcu_slave_addr;
	int firmwave_update_status;
	int cw_i2c_rw;	/* r = 0 , w = 1 */
	int cw_i2c_len;
	uint8_t cw_i2c_data[300];

    s32 iio_data[6];
	struct iio_dev *indio_dev;
	struct irq_work iio_irq_work;
	struct iio_trigger  *trig;
	atomic_t pseudo_irq_enable;

	struct class *sensor_class;
	struct device *sensor_dev;
	atomic_t delay;
    int supend_flag;

    int wq_polling_time;
#ifdef CWMCU_MUTEX
	struct mutex mutex_lock;
    struct mutex mutex_lock_i2c;
    struct mutex mutex_wakeup_gpio;
#endif

    unsigned char loge_buff[QueueSystemInfoMsgSize*2];
    unsigned char loge_buff_count;
    
    unsigned char logw_buff[QueueWarningMsgSize*2];
    unsigned char logw_buff_count;
	
    uint8_t logcat_cmd;
	
    int ps_threshold_high;
    int ps_threshold_low;
    u16 ps_raw_data;

    int ps_noise;
};

struct CWMCU_T *sensors;
static DEFINE_MUTEX(multi_thread);
static int proximity_calib_en(struct CWMCU_T *sensor, int en);
static int proximity_calib_data(struct CWMCU_T *sensor, int *data);
static int proximity_set_threshold(struct CWMCU_T *sensor, int near_th, int far_th);
static int CWMCU_I2C_R(struct CWMCU_T *sensor, u8 reg_addr, u8 *data, u8 len);
static int CWMCU_I2C_W(struct CWMCU_T *sensor, u8 reg_addr, u8 *data, u8 len);

/*dma*/
static u8 *firmware_update_dma_va_read = NULL;
static u8 *firmware_update_dma_va_write = NULL;
static dma_addr_t firmware_update_dma_pa_read = 0;
static dma_addr_t firmware_update_dma_pa_write = 0;
static DEFINE_MUTEX(i2c_access_mutex);

//for geater than 32 bytes read
static int CWMCU_Object_read(struct CWMCU_T *sensor, u8 reg_addr, u8 *data, u8 len)
{
    return CWMCU_I2C_R(sensor, reg_addr, data, len);
}

static void wakeup_pin_reset(void)
{
	mt_set_gpio_out(GPIO_CW_MCU_WAKE_UP, 0);
    usleep_range(200, 200);
	mt_set_gpio_out(GPIO_CW_MCU_WAKE_UP, 1);
    usleep_range(200, 200);
}

static int CWMCU_reg_read(struct CWMCU_T *sensor, u8 reg_addr, u8 *data)
{
    RegInformation *pReadRegInfoInx = sensor->pReadRegInfo;
    int i;
	u8 cReadRegCount = sensor->m_cReadRegCount;
    int wRetVal = 0;

    if (NULL == pReadRegInfoInx || 0 == cReadRegCount)
    {
        SH_ERR("pReadRegInfoInx==NULL or cReadRegCount==0\n");
        wRetVal = -1;
        return wRetVal;
    }

    for(i = 0; i < cReadRegCount; i++)
    {
        if(pReadRegInfoInx[i].cIndex == reg_addr)
            break;
    }

    if(i >= cReadRegCount)
    {
        wRetVal = -1;
    }
    else
    {
        if(pReadRegInfoInx[i].cObjLen != 0)
            wRetVal = CWMCU_Object_read(sensor, pReadRegInfoInx[i].cIndex, data, pReadRegInfoInx[i].cObjLen);
    }
    return wRetVal;
}

static int CWMCU_I2C_R(struct CWMCU_T *sensor, u8 reg_addr, u8 *data, u8 len)
{
	int ret;
    int rty = 0;

	struct i2c_client *client = sensor->client;
    
    if (!firmware_update_dma_va_read)
    {
        SH_ERR("dma_alloc_coherent failed!\n");
        return -1;
    }

	mutex_lock(&i2c_access_mutex);
    if (0 == reg_addr)
    {
        struct i2c_msg msg;
        msg.addr = client->addr;
        msg.flags |= I2C_M_RD;
        msg.len	= len;
        msg.buf	= firmware_update_dma_pa_read;
        msg.ext_flag = client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG;
        msg.timing = 400;

retry0:
        ret = i2c_transfer(client->adapter, &msg, 1);
        if (ret < 0)
        {
            if (rty < 3)
            {
                rty++; 
            	wakeup_pin_reset();
                goto retry0;
            }
            SH_LOG("i2c transfer! reg_addr=%u\n", reg_addr);
            mutex_unlock(&i2c_access_mutex);
            return ret;
        }

        memcpy(data, firmware_update_dma_va_read, len);
	    mutex_unlock(&i2c_access_mutex);
	    return (ret == 1) ? len : ret;
    }
    else
    {
        struct i2c_msg msgs[2];
        msgs[0].addr = client->addr;
        msgs[0].flags = client->flags;
        msgs[0].len	= 1;
        msgs[0].buf	= &reg_addr;
        msgs[0].ext_flag = client->ext_flag;
        msgs[0].timing = 400;

        msgs[1].addr = client->addr;
        msgs[1].flags = client->flags | I2C_M_RD;
        msgs[1].len	= len;
        msgs[1].buf	= firmware_update_dma_pa_read;
        msgs[1].ext_flag = client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG;
        msgs[1].timing = 400;

retry1:
        ret = i2c_transfer(client->adapter, msgs, 2);
        if (ret < 0)
        {
            if (rty < 3)
            {
                rty++; 
                wakeup_pin_reset();
                goto retry1;
            }
            SH_LOG("i2c transfer! reg_addr=%u\n", reg_addr);
            mutex_unlock(&i2c_access_mutex);
            return ret;
        }

        memcpy(data, firmware_update_dma_va_read, len);
	    mutex_unlock(&i2c_access_mutex);
	    return (ret == 2) ? len : ret;
    }
}

static int CWMCU_I2C_W(struct CWMCU_T *sensor, u8 reg_addr, u8 *data, u8 len)
{
	int ret;
    int rty=0;
	struct i2c_client *client = sensor->client;
    struct i2c_msg msg;

    if(!firmware_update_dma_va_write)
    {
        SH_ERR("dma_alloc_coherent failed!\n");
        return -1;
    }

	mutex_lock(&i2c_access_mutex);
    if (0 == reg_addr)
    {
        memcpy(firmware_update_dma_va_write, data, len);
        msg.addr = client->addr;
	    msg.flags	= 0;
        msg.len	= len;
        msg.buf	= firmware_update_dma_pa_write;
        msg.ext_flag = client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG;
        msg.timing = 400;

retry2:
        ret = i2c_transfer(client->adapter, &msg, 1);
        if (ret < 0)
        {
            if (rty < 3)
            {
                rty++; 
                wakeup_pin_reset();
                goto retry2;
            }
            SH_LOG("i2c transfer! reg_addr=%u\n", reg_addr);
        }
	    mutex_unlock(&i2c_access_mutex);
	    return (ret == 1) ? len : ret;
    }
    else
    {
        firmware_update_dma_va_write[0] = reg_addr;
        memcpy(&firmware_update_dma_va_write[1], &data[0], len);
		msg.addr	= client->addr;
		msg.flags	= 0;
		msg.len	= len+1;
		msg.buf	= firmware_update_dma_pa_write;
        msg.ext_flag = client->ext_flag | I2C_ENEXT_FLAG | I2C_DMA_FLAG;
        msg.timing = 400;

retry3:
        ret = i2c_transfer(client->adapter, &msg, 1);
        if (ret < 0)
        {
            if (rty < 3)
            {
                rty++; 
                wakeup_pin_reset();
                goto retry3;
            }
            SH_LOG("i2c transfer! reg_addr=%u\n", reg_addr);
        }
	    mutex_unlock(&i2c_access_mutex);
	    return (ret == 1) ? len : ret;
    }
}

static int CWMCU_I2C_W_SERIAL(struct CWMCU_T *sensor, u8 *data, int len)
{
    return CWMCU_I2C_W(sensor, 0, data, len);
}

static int CWMCU_I2C_R_SERIAL(struct CWMCU_T *sensor, u8 *data, int len)
{
    return CWMCU_I2C_R(sensor, 0, data, len);
}

static int cw_send_event(struct CWMCU_T *sensor, u8 handle, u8 id, u8 *data)
{
	u8 event[21];/* Sensor HAL uses fixed 21 bytes */
    int idx_table;
    static int flag = 1;

    if (CWMCU_NODATA == id)
        return FAIL;

	event[0] = handle;
	event[1] = id;
    memcpy(&event[2], data, 19);

    if ((sensor->debug_log & (1<<D_IIO_DATA)) || (id == PROXIMITY)) 
        SH_LOG("id%u,data:%u,%u,%u\n", id, data[1]<<8 | data[0], data[3]<<8 | data[2], data[5]<<8 | data[4]);

	if (sensor->indio_dev->active_scan_mask && 
        (!bitmap_empty(sensor->indio_dev->active_scan_mask,
            sensor->indio_dev->masklength)))
    {
	    if ((id == MAGNETIC) && (skip_mag_data>0))
	    {
			skip_mag_data--;
			return 0;
	    }
		
		iio_push_to_buffers(sensor->indio_dev, event);
		//iio_push_to_buffer(sensor->indio_dev->buffer, event, 0);
        if (PROXIMITY == id)
        {
            sensor->ps_raw_data = data[5]<<8 | data[4];

            if (((data[1]<<8|data[0])==1) && (sensor->ps_noise > 20) && (sensor->ps_raw_data < (sensor->ps_noise - 20)))
            {
	            sensor->ps_noise = sensor->ps_raw_data;
	            for (idx_table = 0; idx_table < ps_cali_noise_num; idx_table++)
                {
		            if (sensor->ps_raw_data <= ps_cali_noise[idx_table])
			            break;
	            }

	            if (idx_table >= ps_cali_noise_num)
                {
		            SH_ERR("%s: the noise is too large!", __func__);
	            }
                else
                {
	                sensor->ps_threshold_high = ps_cali_offset_high[idx_table] + sensor->ps_raw_data;
                    if (sensor->ps_threshold_high >= AVAGO9960_PS_MAX)
                        sensor->ps_threshold_high = AVAGO9960_PS_MAX;

                    sensor->ps_threshold_low = ps_cali_offset_low[idx_table] + sensor->ps_raw_data;
                    if (sensor->ps_threshold_low >= AVAGO9960_PS_MAX)
                        sensor->ps_threshold_low = AVAGO9960_PS_MAX;

                    proximity_calib_en(sensor, 1);
                    proximity_set_threshold(sensor, sensor->ps_threshold_high, sensor->ps_threshold_low);
                    proximity_calib_en(sensor, 0);
                    SH_LOG("ps_noise: %d, close:%d, far:%d\n", sensor->ps_noise,
                            sensor->ps_threshold_high, sensor->ps_threshold_low);
                }
            }
        }


		return 0;
	}
	else if (NULL == sensor->indio_dev->active_scan_mask)
	{
		SH_ERR("active_scan_mask = NULL, event might be missing\n");
	}

	return -EIO;
}

static void power_pin_sw(struct CWMCU_T *sensor,SWITCH_POWER_ID id, int onoff)
{
	int value = 0;
	mutex_lock(&i2c_access_mutex);
	value = mt_get_gpio_out(GPIO_CW_MCU_WAKE_UP);
	if (onoff)
	{
		sensor->power_on_list |= ((uint32_t)(1) << id);
		if(value == 0)
			mt_set_gpio_out(GPIO_CW_MCU_WAKE_UP, onoff);
			
		if (0 == sensor->power_on_list || value == 0)
		{
			usleep_range(200, 200);
		}   
	}
    else
    {
		sensor->power_on_list &= ~(1 << id);
		if (0 == sensor->power_on_list && value)
		{
			mt_set_gpio_out(GPIO_CW_MCU_WAKE_UP, onoff);
			usleep_range(200, 200);
		}
	}
	mutex_unlock(&i2c_access_mutex);
}

static void cwmcu_kernel_status(struct CWMCU_T *sensor,uint8_t status)
{
	if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
		return ;
	}
	sensor->kernel_status = status;
	CWMCU_I2C_W(sensor, RegMapW_SetHostStatus, &sensor->kernel_status, 1);
}

static int check_enable_list(struct CWMCU_T *sensor)
{
	int i = 0,j=0;
	int count = 0;
	int handle = 0;
	uint8_t data[10];
	int error_msg = 0;
	uint32_t enabled_list[HANDLE_ID_END] = {0};
	uint32_t enabled_list_temp = 0;

    SH_FUN();

	if (CWMCU_I2C_R(sensor, RegMapR_GetHostEnableList, data, 8) >= 0)
	{
		enabled_list[NonWakeUpHandle] = (uint32_t)data[3]<<24 |(uint32_t)data[2]<<16 |(uint32_t)data[1]<<8 |(uint32_t)data[0];
		enabled_list[WakeUpHandle] = (uint32_t)data[7]<<24 |(uint32_t)data[6]<<16 |(uint32_t)data[5]<<8 |(uint32_t)data[4];
		enabled_list[InternalHandle] = 0;

		if ((enabled_list[NonWakeUpHandle] != sensor->enabled_list[NonWakeUpHandle]) 
	            || (enabled_list[WakeUpHandle] != sensor->enabled_list[WakeUpHandle]))
	    {
			SH_LOG("Enable List Check AP0:%d,MCU0:%d;AP1:%d,MCU1:%d\n",
	                    sensor->enabled_list[NonWakeUpHandle],enabled_list[NonWakeUpHandle],
	                    sensor->enabled_list[WakeUpHandle],enabled_list[WakeUpHandle]);

			for (j = 0; j < InternalHandle; j++)
	        {
				handle = j;
				enabled_list_temp = sensor->enabled_list[handle]^enabled_list[handle];
				for (i = 0; i < SENSORS_ID_END; i++)
	            {
					if (enabled_list_temp & (1<<i))
	                {
						data[0] = handle;
						data[1] = i;
						if (sensor->sensors_info[handle][i].en)
	                    {
							sensor->sensors_info[handle][i].rate = (sensor->sensors_info[handle][i].rate ==0)?200:sensor->sensors_info[handle][i].rate;
							data[2] = sensor->sensors_info[handle][i].rate;
							data[3] = (uint8_t)sensor->sensors_info[handle][i].timeout;
							data[4] = (uint8_t)(sensor->sensors_info[handle][i].timeout >>8);
							error_msg = CWMCU_I2C_W(sensor, RegMapW_SetEnable, data, 5);
						}
	                    else
	                    {
							data[2] = 0;
							data[3] = 0;
							data[4] = 0;
							error_msg = CWMCU_I2C_W(sensor, RegMapW_SetDisable, data, 5);
						}
						if (error_msg < 0)
							SH_ERR("I2c Write Fail;%d,%d\n", handle, i);
                     
						count++;
	                    if (count > 15)
						{
							count = 0;
							msleep(20);
						}
					}
				}
			}
		}
        else {
			SH_LOG("enabled_list is the same sensor->enabled_list\n");
        }
	}
    else
    {
        SH_ERR("read RegMapR_GetHostEnableList failed!\n");
    }
	return 0;
}

static int cwmcu_read_buff(struct CWMCU_T *sensor , u8 handle)
{
    uint8_t count_reg;
    uint8_t data_reg;
	uint8_t data[24] = {0};
	uint16_t count = 0;
	int i = 0;

    //SH_FUN();
    if (handle == NonWakeUpHandle)
    {
        count_reg = RegMapR_StreamCount;
        data_reg = RegMapR_StreamEvent;
    }
    else if (handle == WakeUpHandle)
    {
        count_reg = RegMapR_BatchCount;
        data_reg = RegMapR_BatchEvent;
    }
    else
    {
        return FAIL;
    }

    if (CWMCU_I2C_R(sensor, count_reg, data, 2) >= 0) 
    {
	    count = ((uint16_t)data[1] << 8) | (uint16_t)data[0];
		//if (count != 0)
		//	SH_LOG("count  = %d handle = %d\n", count, handle);
	}
    else 
    {
        SH_ERR("check count failed)\n");
        return FAIL;
	}
	
    if ((data[0] == 0xFF) && (data[1] == 0xFF))
        return NO_ERROR;

	for (i = 0; i < count; i++)
    {
        if (CWMCU_I2C_R(sensor, data_reg, data, 9) >= 0)
        {
            cw_send_event(sensor, handle, data[0], &data[1]);
		}
	}

    return NO_ERROR;
}

static int cwmcu_read_gesture(struct CWMCU_T *sensor )
{
	uint8_t data[24] = {0};
	uint8_t count = 0;
	int i = 0;

    SH_FUN();
    if (CWMCU_I2C_R(sensor, RegMapR_GestureCount, &count, 1) < 0)
    {
        SH_ERR("check count failed)\n");
        return FAIL;
	}
	
    if (count == 0xFF)
        return NO_ERROR;        

	for (i = 0; i < count; i++)
    {
        if (CWMCU_I2C_R(sensor, RegMapR_GestureEvent, data, 9) >= 0)
        {
			cw_send_event(sensor, NonWakeUpHandle, data[0], &data[1]);

			if (data[0] == BRUSH)
        	{
				msleep(10);
            	SH_LOG("event BRUSH!\n");
            	data[1] = 0;
				data[2] = 0;
            	cw_send_event(sensor, NonWakeUpHandle, data[0], &data[1]);
        	}
		}
	}

    return NO_ERROR;
}

#define QueueSystemInfoMsgBuffSize      QueueSystemInfoMsgSize*5
static void parser_mcu_info(char *data)
{
    static unsigned char loge_bufftemp[QueueSystemInfoMsgBuffSize];
    static int buff_counttemp = 0;
	int i;
    
    SH_FUN();
	for (i = 0; i < QueueSystemInfoMsgSize; i++)
    {
		loge_bufftemp[buff_counttemp] = (unsigned char)data[i];
		buff_counttemp++;
        if (data[i] == '\n' || (buff_counttemp >= QueueSystemInfoMsgBuffSize))
        {
			SH_LOG("%s:%s", "MSG",loge_bufftemp);
            memset(loge_bufftemp,0x00,QueueSystemInfoMsgBuffSize);
			buff_counttemp = 0;
		}
	}
}

static void read_mcu_info(struct CWMCU_T *sensor)
{
	uint8_t data[40];
	uint16_t count = 0;
	int i = 0;

    SH_FUN();
	if (CWMCU_I2C_R(sensor, RegMapR_SystemInfoMsgCount, data, 1) >= 0)
    {
		count = (uint16_t)data[0];
	}
    else
    {
		SH_ERR("check count fail!\n");
		return;
	}
	
    if (0xFF == count)
        return ;

	for (i = 0; i < count; i++)
    {
		if (CWMCU_I2C_R(sensor, RegMapR_SystemInfoMsgEvent, data, 30) >= 0)
        {
			parser_mcu_info(data);
		}
	}
}

static int CWMCU_POLLING(struct CWMCU_T *sensor)
{
	if (sensor->debug_log & (1<<D_DELAY_WQ)) 
		SH_LOG("Polling: debug_log =>0x%x\n", (int)sensor->enabled_list[NonWakeUpHandle]);

	if (sensor->enabled_list[NonWakeUpHandle])
	{
		cwmcu_read_buff(sensor, NonWakeUpHandle);
		cwmcu_read_buff(sensor, WakeUpHandle);
	}

    return 0;
}

/*==========sysfs node=====================*/
static int cwmcu_find_mindelay(struct CWMCU_T *sensor, int handle)
{
	int i;
	int min_delay = 30;
	for (i = 0; i < SENSORS_ID_END; i++)
    {	
		if (sensor->sensors_info[handle][i].en
				&& (sensor->sensors_info[handle][i].rate >= 10)
				&& (sensor->sensors_info[handle][i].rate < min_delay)
		  )
		{
			min_delay = sensor->sensors_info[handle][i].rate;
		}
	}
    min_delay = (min_delay<=10)? 10: min_delay;
	return min_delay;
}

static ssize_t active_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	int enabled = 0;
	int id = 0;
	int handle = 0;
	int error_msg = 0;
	uint8_t data[10];

    SH_FUN();
	if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode = CW_BOOT\n");
		return FAIL;
	}
	
	sscanf(buf, "%d %d %d\n", &handle, &id, &enabled);
	
    if (PROXIMITY == id && enabled == 1 && need_incall_cali)
        gionee_incall_ps_calibration();

	power_pin_sw(sensor,SWITCH_POWER_ENABLE, 1);
	
	sensor->sensors_info[handle][id].en = enabled;
	data[0] = handle;
	data[1] = id;
	if (enabled)
    {
		sensor->enabled_list[handle] |= 1<<id;
		data[2] = (sensor->sensors_info[handle][id].rate == 0) ? 200 : sensor->sensors_info[handle][id].rate;
		data[3] = (uint8_t)sensor->sensors_info[handle][id].timeout;
		data[4] = (uint8_t)(sensor->sensors_info[handle][id].timeout >>8);
        error_msg = CWMCU_I2C_W(sensor, RegMapW_SetEnable, data, 5);

		if (id == MAGNETIC)
			skip_mag_data = 2;
	}
    else
    {
		sensor->enabled_list[handle] &= ~(1<<id);
		sensor->sensors_info[handle][id].rate = 0;
		sensor->sensors_info[handle][id].timeout = 0;
		data[2] = 0;
		data[3] = 0;
		data[4] = 0;
        error_msg = CWMCU_I2C_W(sensor, RegMapW_SetDisable, data, 5);
		
		if (id == MAGNETIC)
			skip_mag_data = 0;
	}
		
    if (error_msg < 0)
        SH_ERR("I2c Write Fail\n");

    msleep(5);
	check_enable_list(sensor);
	power_pin_sw(sensor,SWITCH_POWER_ENABLE, 0);

    if (NonWakeUpHandle == handle)
    {
        SH_LOG("NonWakeUpHandle == handle\n");
        sensor->wq_polling_time = cwmcu_find_mindelay(sensor, NonWakeUpHandle);
        if (sensor->wq_polling_time != atomic_read(&sensor->delay))
		{
            SH_LOG("sensor->wq_polling_time != atomic_read(&sensor->delay)\n");
            cancel_delayed_work(&sensor->delay_work);
        	if (sensor->enabled_list[NonWakeUpHandle])
        	{
                SH_LOG("sensor->enabled_list[NonWakeUpHandle] == 1\n");
                atomic_set(&sensor->delay, sensor->wq_polling_time);
            	queue_delayed_work(sensor->driver_wq, &sensor->delay_work,
            	msecs_to_jiffies(atomic_read(&sensor->delay)));
        	}
        	else
			{
        		atomic_set(&sensor->delay, CWMCU_POLL_MAX);
			}
    	}
    }
	if (sensor->debug_log & (1<<D_EN)) 
		SH_LOG("%d,%d,%d,%d,%d\n", handle, id, enabled, (int)sensor->sensors_info[handle][id].rate, (int)sensor->sensors_info[handle][id].timeout);

	return error_msg;
}

static ssize_t active_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[10] = {0};
    uint32_t enabled_list[2] ={0, 0};
    int err = 0;

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode = CW_BOOT\n");
        return sprintf(buf, "In Boot Mode\n");
    }

    power_pin_sw(sensor,SWITCH_POWER_ENABLE, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetHostEnableList, data, 8);
    power_pin_sw(sensor,SWITCH_POWER_ENABLE, 0);
    if (err >= 0)
    {
		enabled_list[NonWakeUpHandle] = (uint32_t)data[3]<<24 |(uint32_t)data[2]<<16 |(uint32_t)data[1]<<8 |(uint32_t)data[0];
        enabled_list[WakeUpHandle] = (uint32_t)data[7]<<24 |(uint32_t)data[6]<<16 |(uint32_t)data[5]<<8 |(uint32_t)data[4];
        if (sensor->debug_log & (1<<D_EN)) 
            SH_LOG("MCU En Status:%d,%d\n", enabled_list[NonWakeUpHandle], enabled_list[WakeUpHandle]);

        return sprintf(buf, "%d %d %d %d\n", sensor->enabled_list[NonWakeUpHandle],
                sensor->enabled_list[WakeUpHandle], enabled_list[NonWakeUpHandle], enabled_list[WakeUpHandle]);
    }
    else
    {
        SH_ERR("check MCU En Status failed!\n");
        return sprintf(buf, "read RegMapR_GetHostEnableList failed!\n");
    }
}

static ssize_t batch_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	uint32_t id = 0;
	uint32_t handle = 0;	
	uint32_t mode = -1;
	uint32_t rate = 0;
	uint32_t timeout = 0;
	uint8_t data[5];
    int err = 0;

    SH_FUN();
	if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode = CW_BOOT\n");
		return FAIL;
	}

	sscanf(buf, "%d %d %d %d %d\n", &handle, &id, &mode, &rate, &timeout);
	if (0 == mode)
	{
		sensor->sensors_info[handle][id].rate = (uint8_t)rate;
		sensor->sensors_info[handle][id].timeout = (uint16_t)timeout;
		data[0] = handle;
		data[1] = id;
		data[2] = sensor->sensors_info[handle][id].rate;
		data[3] = (uint8_t)sensor->sensors_info[handle][id].timeout;
		data[4] = (uint8_t)(sensor->sensors_info[handle][id].timeout >> 8);
        if (sensor->sensors_info[handle][id].en)
        {
        	power_pin_sw(sensor,SWITCH_POWER_BATCH, 1);
            err = CWMCU_I2C_W(sensor, RegMapW_SetEnable, data, 5);
        	power_pin_sw(sensor,SWITCH_POWER_BATCH, 0);
			if (err < 0)
			{
				SH_ERR("Write Fail:id:%d, mode:%d, rate:%d, timeout:%d)\n", id, mode, rate, timeout);
            }
		}

		if (sensor->debug_log & (1<<D_EN)) 
			SH_LOG("id:%d, mode:%d, rate:%d, timeout:%d\n", id, mode, rate, timeout);
	}	

	return err;
}

static ssize_t flush_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    int id = 0;
    int handle = 0; 
    uint8_t data[2];
    int err = 0;

	if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
		return FAIL;
	}

    sscanf(buf, "%d %d\n", &handle, &id);
	SH_LOG("flush:id:%d\n", id);
    data[0] = (uint8_t)handle;
    data[1] = (uint8_t)id;
	power_pin_sw(sensor,SWITCH_POWER_FLUSH, 1);
    err = CWMCU_I2C_W(sensor, RegMapW_SetFlush, data, 2);
	power_pin_sw(sensor,SWITCH_POWER_FLUSH, 0);
    if (err < 0)
	{
		SH_ERR("H:%d, Id:%d\n", handle, id);
    }

    return err;
}

static int CWMCU_Write_Mcu_Memory(struct CWMCU_T *sensor,const char *buf)
{
	uint8_t WriteMemoryCommand[2];
	uint8_t data[300];
	uint8_t received[10];
	uint8_t XOR = 0;
	uint16_t i = 0;

    SH_FUN();
	WriteMemoryCommand[0] = 0x31;
	WriteMemoryCommand[1] = 0xCE;
	if (CWMCU_I2C_W_SERIAL(sensor,(uint8_t *)WriteMemoryCommand, 2) < 0)
    {
		return -EAGAIN;
	}
	
    if (CWMCU_I2C_R_SERIAL(sensor,(uint8_t *)received, 1) < 0)
    {
		return -EAGAIN;
	}

	if (received[0] != ACK)
    {
		return -EAGAIN;
	}
	
	data[0] = (uint8_t) (sensor->addr >> 24);
	data[1] = (uint8_t) (sensor->addr >> 16);
	data[2] = (uint8_t) (sensor->addr >> 8);
	data[3] = (uint8_t) sensor->addr;
	data[4] = data[0] ^ data[1] ^ data[2] ^ data[3];
	if (CWMCU_I2C_W_SERIAL(sensor,(uint8_t *)data, 5) < 0)
    {
		return -EAGAIN;
	}

	if (CWMCU_I2C_R_SERIAL(sensor,(uint8_t *)received, 1) < 0)
    {
		return -EAGAIN;
	}

	if (received[0] != ACK)
    {
		return -EAGAIN;
	}

	data[0] = sensor->len - 1;
	XOR = sensor->len - 1;
	for (i = 0; i < sensor->len; i++)
    {
		data[i+1] = buf[i];
		XOR ^= buf[i];
	}
	data[sensor->len+1] = XOR;

	if (CWMCU_I2C_W_SERIAL(sensor,(uint8_t *)data, (sensor->len + 2)) < 0)
    {
		return -EAGAIN;
	}

	return 0;
}

static int set_calib_cmd(struct CWMCU_T *sensor, uint8_t cmd, uint8_t id, uint8_t type)
{
    uint8_t data[4];
    int err;

    if (CW_BOOT == sensor->mcu_mode) 
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return -1;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    data[0] = cmd;
    data[1] = id;
    data[2] = type;
    err = CWMCU_I2C_W(sensor, RegMapW_CalibratorCmd, data, 4);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);

    return err;
}

/*
    sensors default calibrator flow:
        sensors_calib_start(sensors, id);
        do{
            sensors_calib_status(sensors, id,&status);
        }while(status ==CALIB_STATUS_INPROCESS)
        if(status ==CALIB_STATUS_PASS)
            sensors_calib_data_read(sensors, id,data);
        save data    
*/
static int sensors_calib_start(struct CWMCU_T *sensor, uint8_t id)
{
    int err;
    err = set_calib_cmd(sensor, CALIB_EN, id, CALIB_TYPE_DEFAULT);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }

    err = set_calib_cmd(sensor, CALIB_CHECK_STATUS, id, CALIB_TYPE_NON);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }

    return err;
}

static int sensors_calib_status(struct CWMCU_T *sensor, uint8_t id, int *status)
{
    int err;
    uint8_t i2c_data[31] = {0};

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return FAIL;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_CalibratorData, i2c_data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("I2c Read Fail!\n");
        return I2C_FAIL;
    }
    status[0] = (int)((int8_t)i2c_data[0]);

    return NO_ERROR;
}

static int sensors_calib_data_read(struct CWMCU_T*sensor, uint8_t id, uint8_t *data)
{
    int err;

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return FAIL;
    }

    err = set_calib_cmd(sensor, CALIB_DATA_READ, id, CALIB_TYPE_NON);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_CalibratorData, data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("I2c Read Fail!\n");
        return err;
    }

    return NO_ERROR;
}

static int sensors_calib_data_write(struct CWMCU_T *sensor, uint8_t id, uint8_t *data)
{
    int err;

    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return FAIL;
    }

    err = set_calib_cmd(sensor, CALIB_DATA_WRITE, id, CALIB_TYPE_NON);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_W(sensor, RegMapW_CalibratorData, data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }

    return NO_ERROR;
}

static int proximity_calib_en(struct CWMCU_T *sensor, int en)
{
    int err;
    
    SH_LOG("en=%d\n", en);
    if (en)
        err = set_calib_cmd(sensor, CALIB_EN, PROXIMITY, CALIB_TYPE_SENSORS_ENABLE);
    else
        err = set_calib_cmd(sensor, CALIB_EN, PROXIMITY, CALIB_TYPE_SENSORS_DISABLE);

    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return err;
    }
    
    return NO_ERROR;
}

/*
    FUN: proximity_calib_data
    |data[0]: Proximity sensors raw data
    |data[1] is Hight threshold to check sensors is near 
    |data[2] is low threshold to check sensors is far 
*/
static int proximity_calib_data(struct CWMCU_T *sensor, int *data)
{
    int err;
    uint8_t i2c_data[31] = {0};
    int *ptr;
    ptr = (int *)i2c_data;
    
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return FAIL;
    }

    err = set_calib_cmd(sensor, CALIB_DATA_READ, PROXIMITY, CALIB_TYPE_NON);
    if (err < 0)
    {
        SH_ERR("set_calib_cmd Fail!\n");
        return err;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_CalibratorData, i2c_data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("I2c Read Fail!\n");
        return I2C_FAIL;
    }

    data[0] = ptr[3];
    data[1] = ptr[1];
    data[2] = ptr[2];

    SH_LOG("raw:%d, close:%d, far:%d\n", data[0], data[1], data[2]);
    return NO_ERROR;
}

static int proximity_set_threshold(struct CWMCU_T *sensor, int near_th, int far_th)
{
    int err;
    uint8_t i2c_data[31] = {0};
    int *ptr;
    ptr = (int *)i2c_data;
    
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT\n");
        return FAIL;
    }

    err = set_calib_cmd(sensor, CALIB_DATA_WRITE, PROXIMITY, CALIB_TYPE_NON);
    if (err < 0)
    {
        SH_ERR("set_calib_cmd Fail!\n");
        return err;
    }

    ptr[0] = 0;
    ptr[1] = near_th;
    ptr[2] = far_th;
    
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_W(sensor, RegMapW_CalibratorData, i2c_data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("I2c Write Fail!\n");
        return -1;
    }

    SH_LOG("close:%d, far:%d\n", near_th, far_th);
    return NO_ERROR;
}

static ssize_t set_firmware_update_cmd(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	u8 data[300] = {0};
	int i = 0;
    int status = 0;
    int proximity_data[3] = {0};
	u8 cReadRegCount = sensor->m_cReadRegCount;
	u8 cWriteRegCount = sensor->m_cWriteRegCount;
    RegInformation *pReadRegInfoInx = sensor->pReadRegInfo;
    RegInformation *pWriteRegInfoInx = sensor->pWriteRegInfo;
    
	sscanf(buf, "%d %d %d\n", &sensor->cmd, &sensor->addr, &sensor->len);
	
	SH_LOG("cmd=%d addr=%d len=%d\n", sensor->cmd, sensor->addr, sensor->len);

	power_pin_sw(sensor,SWITCH_POWER_FIRMWARE_COMMAND, 1);
	switch (sensor->cmd)
    {
	    case CHANGE_TO_BOOTLOADER_MODE:
			SH_LOG("CHANGE_TO_BOOTLOADER_MODE\n");
	        mt_eint_mask(CUST_EINT_SENSOR_HUB_NUM);
			
		    sensor->mcu_mode = CW_BOOT;
			sensor->client->addr = 0x72 >> 1;
			sensor->mcu_slave_addr = sensor->client->addr;

	    	/* need to config irq to output for AP */
	        mt_set_gpio_dir(GPIO_CW_MCU_INTERRUPT, GPIO_DIR_OUT);
			mt_set_gpio_dir(GPIO_CW_MCU_RESET, GPIO_DIR_OUT);
			mt_set_gpio_dir(GPIO_CW_MCU_BOOT, GPIO_DIR_OUT);
			
	        mt_set_gpio_out(GPIO_CW_MCU_INTERRUPT, 0);

		    /* reset mcu */
	        
	        mt_set_gpio_out(GPIO_CW_MCU_BOOT, 1);        
	        mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);

            msleep(100);
			mt_set_gpio_out(GPIO_CW_MCU_RESET, 0);
			msleep(100);
	        mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);
            msleep(100);
			
	        mt_set_gpio_dir(GPIO_CW_MCU_INTERRUPT, GPIO_DIR_IN);
            break;

	    case CHANGE_TO_NORMAL_MODE:
			SH_LOG("CHANGE_TO_NORMAL_MODE\n");

	        mt_set_gpio_dir(GPIO_CW_MCU_RESET, GPIO_DIR_OUT);

            /* boot low  reset high */
	        mt_set_gpio_out(GPIO_CW_MCU_BOOT, 0);
	        mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);

			msleep(100);
	        mt_set_gpio_out(GPIO_CW_MCU_RESET, 0);
		    msleep(100);
	        mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);
		    msleep(100);

			sensor->firmwave_update_status = 1;
			sensor->client->addr = 0x74 >> 1;

			sensor->mcu_mode = CW_NORMAL;
            mt_eint_unmask(CUST_EINT_SENSOR_HUB_NUM);
			break;

    	case CHECK_FIRMWAVE_VERSION:
			if (CWMCU_I2C_R(sensor, RegMapR_GetFWVersion, data, 4) >= 0)
            {
			    SH_LOG("CHECK_FIRMWAVE_VERSION:%u,%u,%u,%u)\n", data[0], data[1], data[2], data[3]);
			}
			break;

    	case GET_FWPROJECT_ID:
			if (CWMCU_reg_read(sensor, RegMapR_GetProjectID, data) >= 0)
            {
				SH_LOG("PROJECT ID:%s\n", data);
			}
			break;

       	case SHOW_THE_REG_INFO:
			if (pWriteRegInfoInx != NULL && pReadRegInfoInx != NULL)
            {
               SH_LOG("number of read reg:%u number of write reg:%u\n", cReadRegCount, cWriteRegCount);
               SH_LOG("--------------------READ REGISTER INFORMATION------------------------\n");
               for (i = 0; i < cReadRegCount; i++)
               {
                   SH_LOG("read tag number:%u and lengh:%u\n", pReadRegInfoInx->cIndex, pReadRegInfoInx->cObjLen);
                   pReadRegInfoInx++;
               }

               SH_LOG("--------------------WRITE REGISTER INFORMATION-----------------------\n");
               for (i = 0; i < cWriteRegCount; i++)
               {
                   SH_LOG("write tag number:%u and lengh:%u) \n", pWriteRegInfoInx->cIndex, pWriteRegInfoInx->cObjLen);
                   pWriteRegInfoInx++;
               }
			}
			break;

    	case SET_DEBUG_LOG:
            if (sensor->len)
                sensor->debug_log  |= (1<< sensor->addr);
            else
                sensor->debug_log  &= ~(1<< sensor->addr);

			SH_LOG("SET_DEBUG_LOG:%u\n", sensor->debug_log);

			break;

    	case SET_SYSTEM_COMMAND:
			SH_LOG("SET_SYSTEM_COMMAND\n");
			data[0] = sensor->addr;
			data[1] = sensor->len;
			CWMCU_I2C_W(sensor, RegMapW_SetSystemCommand, data, 2);
			break;

    	case GET_SYSTEM_TIMESTAMP:
			if (CWMCU_I2C_R(sensor, RegMapR_GetSystemTimestamp, data, 4) >= 0)
            {
				SH_LOG("Timestamp:%u\n", (((uint32_t)data[3])<<24) | (((uint32_t)data[2])<<16) |
                            (((uint32_t)data[1])<<8) | ((uint32_t)data[0]));
			}
			break;

    	case SET_HW_INITIAL_CONFIG_FLAG:
			sensor->initial_hw_config = sensor->addr;
			break;

    	case SET_SENSORS_POSITION:
			data[0] = sensor->addr;
			data[1] = sensor->len;
			CWMCU_I2C_W(sensor, RegMapW_SetSensorAxisReference, data, 2);
			break;

        case CMD_CALIBRATOR_START:
            sensors_calib_start(sensor,sensor->addr);
            break;

        case CMD_CALIBRATOR_STATUS:
            sensors_calib_status(sensor,sensor->addr,&status);
            break;

        case CMD_CALIBRATOR_READ:
            sensors_calib_data_read(sensor,sensor->addr,sensor->cw_i2c_data);
            break;

        case CMD_CALIBRATOR_WRITE:
            sensors_calib_data_write(sensor,sensor->addr,sensor->cw_i2c_data);
            break;

        case CMD_PROXIMITY_EN:
            proximity_calib_en(sensor,sensor->addr);
            break;

        case CMD_PROXIMITY_DATA:
            proximity_calib_data(sensor,proximity_data);
            SH_LOG("Proximity data:%d,%d,%d\n", proximity_data[0], proximity_data[1], proximity_data[2]);
            break;

        case CMD_PROXIMITY_TH:
            proximity_set_threshold(sensor,sensor->addr,sensor->len);
            SH_LOG("Proximity th:%d,%d\n", sensor->addr, sensor->len);
            break;

    	default:
			break;
	}
	power_pin_sw(sensor,SWITCH_POWER_FIRMWARE_COMMAND, 0);

	return count;
}

static ssize_t set_firmware_update_data(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    SH_LOG("%s\n", buf);
	sensor->firmwave_update_status = 1;
	sensor->firmwave_update_status = CWMCU_Write_Mcu_Memory(sensor,buf);
	return count;
}

static ssize_t get_firmware_update_status(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	SH_LOG("firmwave_update_status = %d\n", sensor->firmwave_update_status);
	return sprintf(buf, "%d\n", sensor->firmwave_update_status);
}

static ssize_t set_firmware_update_i2(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	int intsize = sizeof(int);

    SH_FUN();
	memcpy(&sensor->cw_i2c_rw, buf, intsize);
	memcpy(&sensor->cw_i2c_len, &buf[4], intsize);
	memcpy(sensor->cw_i2c_data, &buf[8], sensor->cw_i2c_len);
	return count;
}

static ssize_t get_firmware_update_i2(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	int status = 0;

    SH_FUN();
	if (sensor->cw_i2c_rw)
    {
		if (CWMCU_I2C_W_SERIAL(sensor,sensor->cw_i2c_data, sensor->cw_i2c_len) < 0)
        {
			status = -1;
		}
		memcpy(buf, &status, sizeof(int));
		return 4;
	}
    else
    {
		if (CWMCU_I2C_R_SERIAL(sensor,sensor->cw_i2c_data, sensor->cw_i2c_len) < 0)
        {
			status = -1;
			memcpy(buf, &status, sizeof(int));
			return 4;
		}
		memcpy(buf, &status, sizeof(int));
		memcpy(&buf[4], sensor->cw_i2c_data, sensor->cw_i2c_len);
		return 4+sensor->cw_i2c_len;
	}
	return  0;
}

static ssize_t mcu_mode_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", sensor->mcu_mode);
}

static ssize_t mcu_model_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	int mode = 0;
	sscanf(buf, "%d\n", &mode);
    sensor->mcu_mode = mode;
	return count;
}

static ssize_t set_calibrator_cmd(struct device *dev,struct device_attribute *attr,const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    int err;

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_ERR("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }

    sscanf(buf, "%d %d %d\n", &sensor->cal_cmd, &sensor->cal_id, &sensor->cal_type);
    err = set_calib_cmd(sensor, sensor->cal_cmd, sensor->cal_id, sensor->cal_type);
    if (sensor->debug_log & (1<<D_CALIB)) 
        SH_LOG("cmd:%d,id:%d,type:%d\n", sensor->cal_cmd, sensor->cal_id, sensor->cal_type);
    if (err < 0)
        SH_ERR("I2c Write Fail!\n");

    return err;
}

static ssize_t get_calibrator_cmd(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    return sprintf(buf, "Cmd:%d,Id:%d,Type:%d\n", sensor->cal_cmd, sensor->cal_id, sensor->cal_type);
}

static ssize_t get_calibrator_data(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t Cal_data[31] = {0};
    int err = 0;
    
    SH_FUN();
    if (sensor->mcu_mode == CW_BOOT)
    {
        SH_ERR("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_CalibratorData, Cal_data, 30);
    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    if (err < 0)
    {
        SH_ERR("read RegMapR_CalibratorData failed!\n");
        return err;
    }

    if (sensor->cal_cmd == CALIB_DATA_READ)
    {
        memcpy(sensor->calibratordata[sensor->cal_id], Cal_data, 30);
        sensor->calibratorUpdate[sensor->cal_id] = 1;
    }

    return sprintf(buf, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", 
        Cal_data[0],  Cal_data[1],  Cal_data[2],  Cal_data[3],  Cal_data[4],  Cal_data[5],  Cal_data[6],  Cal_data[7],  Cal_data[8], 
        Cal_data[9],  Cal_data[10], Cal_data[11], Cal_data[12], Cal_data[13], Cal_data[14], Cal_data[15], Cal_data[16], Cal_data[17],
        Cal_data[18], Cal_data[19], Cal_data[20], Cal_data[21], Cal_data[22], Cal_data[23], Cal_data[24], Cal_data[25], Cal_data[26], 
        Cal_data[27], Cal_data[28], Cal_data[29]);
}

static ssize_t set_calibrator_data(struct device *dev,struct device_attribute *attr,const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[30];
    int temp[33] = {0};
    int i,err;

    if (sensor->mcu_mode == CW_BOOT) 
    {
        SH_ERR("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }

    sscanf(buf, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
        &temp[0],  &temp[1],  &temp[2],  &temp[3],  &temp[4],  &temp[5],  &temp[6],  &temp[7],  &temp[8],  &temp[9], 
        &temp[10], &temp[11], &temp[12], &temp[13], &temp[14], &temp[15], &temp[16], &temp[17], &temp[18], &temp[19],
        &temp[20], &temp[21], &temp[22], &temp[23], &temp[24], &temp[25], &temp[26], &temp[27], &temp[28], &temp[29]);

    for (i = 0 ; i < 30; i++)
        data[i] = (uint8_t)temp[i];

    if (CALIB_DATA_WRITE == sensor->cal_cmd)
    {
        memcpy(sensor->calibratordata[sensor->cal_id],data,30);
        sensor->calibratorUpdate[sensor->cal_id] = 1;
    }

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 1);
    err = CWMCU_I2C_W(sensor, RegMapW_CalibratorData, data, 30);
    if (err < 0)
        SH_ERR("I2c Write Fail!\n");

    power_pin_sw(sensor,SWITCH_POWER_CALIB, 0);
    return err;
}

static ssize_t version_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	uint8_t data[4];
	int16_t version = -1;

    SH_FUN();
	if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
		return FAIL;
	}

	power_pin_sw(sensor,SWITCH_POWER_VERSION, 1);
	if (CWMCU_I2C_R(sensor, RegMapR_GetFWVersion, data, 4) >= 0)
    {
		version = (int16_t)( ((uint16_t)data[1])<<8 | (uint16_t)data[0]);
		SH_LOG("CHECK_FIRMWAVE_VERSION : M:%u,D:%u,V:%u,SV:%u\n", data[3], data[2], data[1], data[0]);
	}
    else
    {
		SH_ERR("i2c read fail)\n");
	}
	power_pin_sw(sensor,SWITCH_POWER_VERSION, 0);
	return sprintf(buf, "%d\n", version);
}

static ssize_t library_version_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[4] = {0, 0, 0, 0};

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    
    power_pin_sw(sensor,SWITCH_POWER_VERSION, 1);
    if (CWMCU_I2C_R(sensor, RegMapR_GetLibVersion, data, 4) >= 0)
    {
        SH_LOG("check_library_version:%u,%u,%u,%u\n", data[3], data[2], data[1], data[0]);
    }
    else
    {
        SH_ERR("i2c read fail)\n");
    }
    power_pin_sw(sensor,SWITCH_POWER_VERSION, 0);
    return sprintf(buf, "%d %d %d %d\n", data[3], data[2], data[1], data[0]);
}

static ssize_t timestamp_show(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	uint8_t data[4];
    uint32_t *ptr;
    int err;
    ptr = (uint32_t *)data;

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode || sensor->supend_flag == 1)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_TIME, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetSystemTimestamp, data, 4);
    power_pin_sw(sensor,SWITCH_POWER_TIME, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetSystemTimestamp failed!\n");
        return err;
    }
    SH_LOG("Time:%u\n", ptr[0]);
    return sprintf(buf, "%u\n", ptr[0]);
}

static ssize_t set_sys_cmd(struct device *dev,struct device_attribute *attr,const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[8];
    int temp[8] = {0};
    int i,err;

    if (CW_BOOT == sensor->mcu_mode) 
    {
        SH_ERR("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }

    sscanf(buf, "%d %d %d %d %d %d %d %d\n",
        &temp[0], &temp[1], &temp[2],
        &temp[3], &temp[4], &temp[5], &temp[6], &temp[7]);

    for (i = 0 ; i < 8; i++)
        data[i] = (uint8_t)temp[i];

    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_W(sensor, RegMapW_SetSystemCommand, data, 8);
    if (err < 0)
        SH_ERR("I2c Write Fail!\n");
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    return err;
}

static void read_calib_info(struct CWMCU_T *sensor)
{
    uint8_t data[24] = {0};
    int status = 0;
    uint16_t *ptr;
    ptr = (uint16_t *)data;
    
    if (set_calib_cmd(sensor, CALIB_CHECK_STATUS, sensor->cal_id, sensor->cal_type)<0)
	{
        SH_ERR("I2c Write Fail!\n");
        return;
    }

    if (sensors_calib_status(sensor,  sensor->cal_id, &status) >= 0)
	{
        SH_LOG("Calib id:%d:status:%d\n", sensor->cal_id , status);
        if (CALIB_STATUS_PASS == status)
        {
            ptr[0] =  (uint16_t)sensor->cal_id;
            cw_send_event(sensor,NonWakeUpHandle,CALIBRATOR_UPDATE,data);
        }
    }

    return ;
}

static void read_error_code(struct CWMCU_T *sensor)
{
    uint8_t data[4] = {0};
    int8_t *ptr;
    int err;
    ptr = (int8_t *)data;
    err = CWMCU_I2C_R(sensor, RegMapR_ErrorCode, data, 4);
    if (err < 0)
        SH_ERR("I2c Write Fail!\n");
    if (ptr[0] == ERR_TASK_BLOCK)
	{
        SH_LOG("ERR_TASK_BLOCK\n");
    }
    SH_LOG("%s:%d,%d,%d,%d)\n",__FUNCTION__ , ptr[0], ptr[1], ptr[2], ptr[3]);
}

static ssize_t get_raw_data0(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[6];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
    
	SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
	
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetAccelerationRawData, data, 6);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetAccelerationRawData failed!\n");
        return FAIL;
    }
    SH_LOG("RawData0:%u,%u,%u)\n", ptr[0], ptr[1], ptr[2]);
    return sprintf(buf, "%d %u %u %u\n", err, ptr[0], ptr[1], ptr[2]);
}

static ssize_t get_raw_data1(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[6];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
	
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetMagneticRawData, data, 6);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetMagneticRawData failed!\n");
        return err;
    }
    SH_LOG("RawData1:%u,%u,%u)\n", ptr[0], ptr[1], ptr[2]);
    return sprintf(buf, "%d %u %u %u\n", err, ptr[0], ptr[1], ptr[2]);
}

static ssize_t get_raw_data2(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[6];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
	
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetGyroRawData, data, 6);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetGyroRawData failed!\n");
        return err;
    }
    SH_LOG("RawData2:%u,%u,%u)\n", ptr[0], ptr[1], ptr[2]);
    return sprintf(buf, "%d %u %u %u\n", err, ptr[0], ptr[1], ptr[2]);
}

static ssize_t get_raw_data3(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[6];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
    
	SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
	
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetLightRawData, data, 6);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetLightRawData failed!\n");
        return err;
    }
    SH_LOG("RawData3:%u,%u,%u)\n", ptr[0], ptr[1], ptr[2]);
    return sprintf(buf, "%d %u %u %u\n", err, ptr[0], ptr[1], ptr[2]);
}

static ssize_t get_raw_data4(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[6];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
	
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_GetProximityRawData, data, 6);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_GetProximityRawData failed!\n");
        return err;
    }
    SH_LOG("RawData4:%u,%u,%u)\n", ptr[0], ptr[1], ptr[2]);
    return sprintf(buf, "%d %u %u %u\n", err, ptr[0], ptr[1], ptr[2]);
}

static ssize_t get_mag_special_data(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    uint8_t data[64];
    uint16_t *ptr;
    int err;
    ptr = (uint16_t *)data;
    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_SYS, 1);
    err = CWMCU_I2C_R(sensor, RegMapR_MagSpecialData, data, 64);
    power_pin_sw(sensor,SWITCH_POWER_SYS, 0);
    if (err < 0) 
    {
        SH_ERR("read RegMapR_MagSpecialData failed!\n");
        return err;
    }
    memcpy(buf,data,64);
    return 64;
}

static ssize_t set_mag_special_data(struct device *dev,struct device_attribute *attr,const char *buf, size_t count)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);

    SH_FUN();
    if (CW_BOOT == sensor->mcu_mode)
    {
        SH_LOG("mcu_mode == CW_BOOT!\n");
        return FAIL;
    }
    power_pin_sw(sensor,SWITCH_POWER_PROBE, 1);
    cwmcu_kernel_status(sensor,KERNEL_SHUTDOWN);
    power_pin_sw(sensor,SWITCH_POWER_PROBE, 0);
    return count;
}

#ifndef CWMCU_CALIB_SAVE_IN_FLASH
static void reload_calib_data(struct CWMCU_T *sensor)
{
    int i;
    for (i = 0;i < DRIVER_ID_END ; i ++)
    {
        if (sensor->calibratorUpdate[i])
        {
            sensors_calib_data_write(sensor, i, sensor->calibratordata[i]);
            msleep(10);
        }
    }
}
#endif

static void cwmcu_reinit(struct CWMCU_T *sensor)
{
#ifndef CWMCU_CALIB_SAVE_IN_FLASH
    reload_calib_data(sensor);
#endif
    check_enable_list(sensor);
    cwmcu_kernel_status(sensor,KERNEL_RESUND);
}

static struct device_attribute attributes[] = {
	__ATTR(enable, 0664,  active_show, active_set),
	__ATTR(batch, 0660, NULL, batch_set),
	__ATTR(flush, 0660, NULL, flush_set),
	__ATTR(mcu_mode, 0664, mcu_mode_show, mcu_model_set),
	__ATTR(calibrator_cmd, 0664,  get_calibrator_cmd, set_calibrator_cmd),
	__ATTR(calibrator_data, 0664, get_calibrator_data, set_calibrator_data),
	__ATTR(firmware_update_i2c, 0664, get_firmware_update_i2, set_firmware_update_i2),
    __ATTR(firmware_update_cmd, 0660, NULL, set_firmware_update_cmd),
    __ATTR(firmware_update_data, 0660, NULL, set_firmware_update_data),
    __ATTR(firmware_update_status, 0664, get_firmware_update_status, NULL),
    __ATTR(version, 0664,  version_show, NULL),
    __ATTR(library_version, 0664,  library_version_show, NULL),
    __ATTR(timestamp, 0664, timestamp_show, NULL),
    __ATTR(sys_cmd, 0660,  NULL, set_sys_cmd),
    __ATTR(raw_data0, 0664, get_raw_data0, NULL),
    __ATTR(raw_data1, 0664, get_raw_data1, NULL),
    __ATTR(raw_data2, 0664, get_raw_data2, NULL),
    __ATTR(raw_data3, 0664, get_raw_data3, NULL),
    __ATTR(raw_data4, 0664, get_raw_data4, NULL),
    __ATTR(mag_special_data, 0664, get_mag_special_data, set_mag_special_data),
};

static void CWMCU_IRQ(struct CWMCU_T *sensor)
{
    uint8_t temp[2] = {0};
	uint8_t data_event[24] = {0};
    
    SH_FUN();
    if (CWMCU_I2C_R(sensor, RegMapR_InterruptStatus, temp, 2) >= 0)
    {
        sensor->interrupt_status = (u32)temp[1] << 8 | (u32)temp[0];
        if (sensor->debug_log & (1<<D_IRQ)) 
            SH_LOG("interrupt_status:%d\n", sensor->interrupt_status);
    }
    else
    {
        SH_ERR("check interrupt_status failed\n");
        return;
    }
               
    if (sensor->interrupt_status & (1<<IRQ_INIT))
    {
        cwmcu_reinit(sensor);
        cw_send_event(sensor, NonWakeUpHandle, MCU_REINITIAL, data_event);
    }

    if (sensor->interrupt_status & (1<<IRQ_GESTURE))
    {
        cwmcu_read_gesture(sensor);
    }

    if ((sensor->interrupt_status & (1<<IRQ_BATCH_TIMEOUT)) ||(sensor->interrupt_status & (1<<IRQ_BATCH_FULL)) ) 
    {
        cwmcu_read_buff(sensor,WakeUpHandle);
    }

    if (sensor->interrupt_status & (1<<IRQ_INFO))
    {
        read_mcu_info(sensor);
    }
    if (sensor->interrupt_status & (1<<IRQ_CALIB)) 
    {
        read_calib_info(sensor);
    }
	if (sensor->interrupt_status & (1<<IRQ_ERROR))
	{
		read_error_code(sensor);
	}
}

// Gionee chengx 20150429 add for CR01461753 begin
#ifdef CONFIG_HAS_EARLYSUSPEND
static void mtk_cwmcu_suspend(struct early_suspend *h)
{
    SH_FUN();

// Gionee chengx 20150529 add for CR01488451 begin
    if ((sensors->enabled_list[NonWakeUpHandle] && 1<<ACCELERATION) && 
            (sensors->enabled_list[NonWakeUpHandle] && 1<<GYRO) && 
            (sensors->enabled_list[NonWakeUpHandle] && 1<<PROXIMITY))
    {
        msleep(500);   
    }
// Gionee chengx 20150529 add for CR01488451 end

    cancel_delayed_work(&sensors->delay_work);
	power_pin_sw(sensors, SWITCH_POWER_PROBE, 1);
	cwmcu_kernel_status(sensors, KERNEL_SUPEND);
	power_pin_sw(sensors, SWITCH_POWER_PROBE, 0);
    sensors->supend_flag = 1;
}

static void mtk_cwmcu_resume(struct early_suspend *h)
{
	SH_FUN();
	
    power_pin_sw(sensors, SWITCH_POWER_PROBE, 1);
    cwmcu_kernel_status(sensors, KERNEL_RESUND);
    power_pin_sw(sensors, SWITCH_POWER_PROBE, 0);
    queue_delayed_work(sensors->driver_wq, &sensors->delay_work,
    	msecs_to_jiffies(atomic_read(&sensors->delay)));
    sensors->supend_flag = 0;
}

static struct early_suspend mtk_cwmcu_early_suspend_handler = {
	.level = EARLY_SUSPEND_LEVEL_STOP_DRAWING - 2,
	.suspend = mtk_cwmcu_suspend,
	.resume = mtk_cwmcu_resume,
};

#else

static int CWMCU_suspend(struct device *dev)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
	
    SH_FUN();
    cancel_delayed_work(&sensor->delay_work);
	power_pin_sw(sensor,SWITCH_POWER_PROBE, 1);
	cwmcu_kernel_status(sensor,KERNEL_SUPEND);
	power_pin_sw(sensor,SWITCH_POWER_PROBE, 0);
	return 0;
}

static int CWMCU_resume(struct device *dev)
{
    struct CWMCU_T *sensor = dev_get_drvdata(dev);
    
    SH_FUN();
    power_pin_sw(sensor,SWITCH_POWER_PROBE, 1);
    cwmcu_kernel_status(sensor,KERNEL_RESUND);
    power_pin_sw(sensor,SWITCH_POWER_PROBE, 0);
    queue_delayed_work(sensor->driver_wq, &sensor->delay_work,
    	msecs_to_jiffies(atomic_read(&sensor->delay)));
    return 0;
}
#endif
// Gionee chengx 20150429 add for CR01461753 end

/*=======iio device reg=========*/
static void iio_trigger_work(struct irq_work *work)
{
	struct CWMCU_T *mcu_data = container_of((struct irq_work *)work, struct CWMCU_T, iio_irq_work);

	iio_trigger_poll(mcu_data->trig, iio_get_time_ns());
}

static irqreturn_t cw_trigger_handler(int irq, void *p)
{
	struct iio_poll_func *pf = p;
	struct iio_dev *indio_dev = pf->indio_dev;
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);

#ifdef CWMCU_MUTEX
	mutex_lock(&mcu_data->mutex_lock);
#endif
	iio_trigger_notify_done(mcu_data->indio_dev->trig);
#ifdef CWMCU_MUTEX
	mutex_unlock(&mcu_data->mutex_lock);
#endif

	return IRQ_HANDLED;
}

static const struct iio_buffer_setup_ops cw_buffer_setup_ops = {
	.preenable = &iio_sw_buffer_preenable,
	.postenable = &iio_triggered_buffer_postenable,
	.predisable = &iio_triggered_buffer_predisable,
};

static int cw_pseudo_irq_enable(struct iio_dev *indio_dev)
{
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);

	if (!atomic_cmpxchg(&mcu_data->pseudo_irq_enable, 0, 1))
    {
        SH_FUN();
		cancel_work_sync(&mcu_data->work);
		queue_work(mcu_data->driver_wq, &mcu_data->work);
        cancel_delayed_work(&mcu_data->delay_work);
        queue_delayed_work(mcu_data->driver_wq, &mcu_data->delay_work, 0);
	}

	return 0;
}

static int cw_pseudo_irq_disable(struct iio_dev *indio_dev)
{
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);

	if (atomic_cmpxchg(&mcu_data->pseudo_irq_enable, 1, 0))
    {
		cancel_work_sync(&mcu_data->work);
        cancel_delayed_work(&mcu_data->delay_work);
        SH_FUN();
	}
	return 0;
}

static int cw_set_pseudo_irq(struct iio_dev *indio_dev, int enable)
{
	if (enable)
		cw_pseudo_irq_enable(indio_dev);
	else
		cw_pseudo_irq_disable(indio_dev);
	return 0;
}

static int cw_data_rdy_trigger_set_state(struct iio_trigger *trig, bool state)
{
	struct iio_dev *indio_dev = (struct iio_dev *)iio_trigger_get_drvdata(trig);
	//struct iio_dev *indio_dev = trig->private_data;
#ifdef CWMCU_MUTEX
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);
    mutex_lock(&mcu_data->mutex_lock);
#endif
	cw_set_pseudo_irq(indio_dev, state);
#ifdef CWMCU_MUTEX
    mutex_unlock(&mcu_data->mutex_lock);
#endif
	
	return 0;
}

static const struct iio_trigger_ops cw_trigger_ops = {
	.owner = THIS_MODULE,
	.set_trigger_state = &cw_data_rdy_trigger_set_state,
};

static int cw_probe_trigger(struct iio_dev *iio_dev)
{
	struct CWMCU_T *mcu_data = iio_priv(iio_dev);
	int ret;

	iio_dev->pollfunc = iio_alloc_pollfunc(&iio_pollfunc_store_time, &cw_trigger_handler, 
                            IRQF_ONESHOT, iio_dev, "%s_consumer%d", iio_dev->name, iio_dev->id);
	if (NULL == iio_dev->pollfunc)
    {
		ret = -ENOMEM;
		goto error_ret;
	}

	mcu_data->trig = iio_trigger_alloc("%s-dev%d", iio_dev->name, iio_dev->id);
	//mcu_data->trig = iio_allocate_trigger("%s-dev%d", iio_dev->name, iio_dev->id);
	if (!mcu_data->trig)
    {
		ret = -ENOMEM;
		goto error_dealloc_pollfunc;
	}

	mcu_data->trig->dev.parent = &mcu_data->client->dev;
	mcu_data->trig->ops = &cw_trigger_ops;
	iio_trigger_set_drvdata(mcu_data->trig, iio_dev);
	//mcu_data->trig->private_data = iio_dev;

	ret = iio_trigger_register(mcu_data->trig);
	if (ret)
		goto error_free_trig;

	return 0;

error_free_trig:
	iio_trigger_free(mcu_data->trig);
	//iio_free_trigger(mcu_data->trig);
error_dealloc_pollfunc:
	iio_dealloc_pollfunc(iio_dev->pollfunc);
error_ret:
	return ret;
}

static int cw_probe_buffer(struct iio_dev *iio_dev)
{
	int ret;
	struct iio_buffer *buffer;

	buffer = iio_kfifo_allocate(iio_dev);
	if (!buffer)
    {
		ret = -ENOMEM;
		goto error_ret;
	}

	buffer->scan_timestamp = true;
	iio_dev->buffer = buffer;
	iio_dev->setup_ops = &cw_buffer_setup_ops;
	iio_dev->modes |= INDIO_BUFFER_TRIGGERED;
	ret = iio_buffer_register(iio_dev, iio_dev->channels, iio_dev->num_channels);
	if (ret)
		goto error_free_buf;

	iio_scan_mask_set(iio_dev, iio_dev->buffer, CW_SCAN_ID);
	iio_scan_mask_set(iio_dev, iio_dev->buffer, CW_SCAN_X);
	iio_scan_mask_set(iio_dev, iio_dev->buffer, CW_SCAN_Y);
	iio_scan_mask_set(iio_dev, iio_dev->buffer, CW_SCAN_Z);
	return 0;

error_free_buf:
	iio_kfifo_free(iio_dev->buffer);
error_ret:
	return ret;
}

static int cw_read_raw(struct iio_dev *indio_dev, struct iio_chan_spec const *chan,
                            int *val, int *val2, long mask)
{
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);
	int ret = -EINVAL;

	if (chan->type != IIO_ACCEL)
		return ret;

#ifdef CWMCU_MUTEX
	mutex_lock(&mcu_data->mutex_lock);
#endif
	switch (mask)
    {
	    case 0:
		    *val = mcu_data->iio_data[chan->channel2 - IIO_MOD_X];
		    ret = IIO_VAL_INT;
		    break;

	    case IIO_CHAN_INFO_SCALE:
		    /* Gain : counts / uT = 1000 [nT] */
		    /* Scaling factor : 1000000 / Gain = 1000 */
		    *val = 0;
		    *val2 = 1000;
		    ret = IIO_VAL_INT_PLUS_MICRO;
		    break;

        default:
            break;
	}
#ifdef CWMCU_MUTEX
    mutex_unlock(&mcu_data->mutex_lock);
#endif
	return ret;
}

#define CW_CHANNEL(axis)			\
{						\
	.type = IIO_ACCEL,			\
	.modified = 1,				\
	.channel2 = axis+1,			\
	.info_mask = BIT(IIO_CHAN_INFO_SCALE),	\
	.scan_index = axis,			\
	.scan_type = IIO_ST('u', 32, 32, 0)	\
}

static const struct iio_chan_spec cw_channels[] = {
	CW_CHANNEL(CW_SCAN_ID),
	CW_CHANNEL(CW_SCAN_X),
	CW_CHANNEL(CW_SCAN_Y),
	CW_CHANNEL(CW_SCAN_Z),
	IIO_CHAN_SOFT_TIMESTAMP(CW_SCAN_TIMESTAMP)
};

static const struct iio_info cw_info = {
	.read_raw = &cw_read_raw,
	.driver_module = THIS_MODULE,
};

static void cwmcu_delwork_report(struct work_struct *work)
{
    struct CWMCU_T *sensor = container_of((struct delayed_work *)work,
        struct CWMCU_T, delay_work);
    
    if (atomic_read(&sensor->pseudo_irq_enable))
	{
        if (CW_BOOT == sensor->mcu_mode)
		{
            SH_ERR("sensor->mcu_mode = CW_BOOT\n");
        }
		else
		{
            power_pin_sw(sensor,SWITCH_POWER_POLLING, 1);
            CWMCU_POLLING(sensor);
            power_pin_sw(sensor,SWITCH_POWER_POLLING, 0);
        }
	    queue_delayed_work(sensor->driver_wq, &sensor->delay_work,
	    	msecs_to_jiffies(atomic_read(&sensor->delay)));
    }
}

static int create_sysfs_interfaces(struct CWMCU_T *mcu_data)
{
	int i;
	int res;
    
    SH_FUN();
	mcu_data->sensor_class = class_create(THIS_MODULE, "cywee_sensorhub");
	if (IS_ERR(mcu_data->sensor_class))
		return PTR_ERR(mcu_data->sensor_class);

	mcu_data->sensor_dev = device_create(mcu_data->sensor_class, NULL, 0, "%s", "sensor_hub");
	if (IS_ERR(mcu_data->sensor_dev))
    {
		res = PTR_ERR(mcu_data->sensor_dev);
		goto err_device_create;
	}

	res = dev_set_drvdata(mcu_data->sensor_dev, mcu_data);
	if (res)
		goto err_set_drvdata;

	for (i = 0; i < ARRAY_SIZE(attributes); i++)
		if (device_create_file(mcu_data->sensor_dev, attributes + i))
			goto error;

	res = sysfs_create_link(&mcu_data->sensor_dev->kobj, &mcu_data->indio_dev->dev.kobj, "iio");
	if (res < 0)
		goto error;

	return 0;

error:
	while (--i >= 0)
		device_remove_file(mcu_data->sensor_dev, attributes + i);
err_set_drvdata:
	put_device(mcu_data->sensor_dev);
	device_unregister(mcu_data->sensor_dev);
err_device_create:
	class_destroy(mcu_data->sensor_class);
	return res;
}

#ifdef CWMCU_INTERRUPT
static void CWMCU_interrupt_thread(void)
{
	if (CW_BOOT == sensors->mcu_mode)
    {
		SH_ERR("sensors->mcu_mode = CW_BOOT\n");
        return;
	}

	schedule_work(&sensors->work);
    return;
}

static void cwmcu_work_report(struct work_struct *work)
{
    struct CWMCU_T *sensor = container_of((struct work_struct *)work, struct CWMCU_T, work);

    if (CW_BOOT == sensor->mcu_mode)
    {
	    SH_ERR("sensor->mcu_mode = CW_BOOT)\n");
        mt_eint_unmask(CUST_EINT_SENSOR_HUB_NUM);
	    return;
    }

	power_pin_sw(sensor,SWITCH_POWER_INTERRUPT, 1);
	CWMCU_IRQ(sensor);
	power_pin_sw(sensor,SWITCH_POWER_INTERRUPT, 0);
    mt_eint_unmask(CUST_EINT_SENSOR_HUB_NUM);
}
#endif

static void cwmcu_remove_trigger(struct iio_dev *indio_dev)
{
	struct CWMCU_T *mcu_data = iio_priv(indio_dev);

	iio_trigger_unregister(mcu_data->trig);
	iio_trigger_free(mcu_data->trig);
	//iio_free_trigger(mcu_data->trig);
	iio_dealloc_pollfunc(indio_dev->pollfunc);
}

static void cwmcu_remove_buffer(struct iio_dev *indio_dev)
{
	iio_buffer_unregister(indio_dev);
	iio_kfifo_free(indio_dev->buffer);
}

static int cwstm_power_on(struct CWMCU_T *sensor,bool on)
{
	int rc;
return 0;
	if (!on)
		goto power_off;

	rc = regulator_enable(sensor->vdd);
	if (rc)
    {
		SH_ERR("Regulator vdd enable failed rc=%d\n", rc);
		return rc;
	}

	rc = regulator_enable(sensor->vcc_i2c);
	if (rc)
    {
		SH_ERR("Regulator vcc_i2c enable failed rc=%d\n", rc);
		regulator_disable(sensor->vdd);
	}

	return rc;

power_off:
	rc = regulator_disable(sensor->vdd);
	if (rc)
    {
		SH_ERR("Regulator vdd disable failed rc=%d\n", rc);
		return rc;
	}

	rc = regulator_disable(sensor->vcc_i2c);
	if (rc)
    {
		SH_ERR("Regulator vcc_i2c disable failed rc=%d\n", rc);
		regulator_enable(sensor->vdd);
	}

	return rc;
}

static int cwstm_power_init(struct CWMCU_T *sensor,bool on)
{
	int rc;
return 0;
	if (!on)
		goto pwr_deinit;

	sensor->vdd = regulator_get(&sensor->client->dev, "cwstm,vdd_ana");
	if (IS_ERR(sensor->vdd))
    {
		rc = PTR_ERR(sensor->vdd);
		SH_ERR("Regulator get failed vdd rc=%d\n", rc);
		return rc;
	}

	if (regulator_count_voltages(sensor->vdd) > 0)
    {
		rc = regulator_set_voltage(sensor->vdd, FT_VTG_MIN_UV, FT_VTG_MAX_UV);
		if (rc)
        {
			SH_ERR("Regulator set_vtg failed vdd rc=%d\n", rc);
			goto reg_vdd_put;
		}
	}

	sensor->vcc_i2c = regulator_get(&sensor->client->dev, "cwstm,vcc_i2c");
	if (IS_ERR(sensor->vcc_i2c))
    {
		rc = PTR_ERR(sensor->vcc_i2c);
		SH_ERR("Regulator get failed vcc_i2c rc=%d\n", rc);
		goto reg_vdd_set_vtg;
	}

	if (regulator_count_voltages(sensor->vcc_i2c) > 0)
    {
		rc = regulator_set_voltage(sensor->vcc_i2c, FT_I2C_VTG_MIN_UV, FT_I2C_VTG_MAX_UV);
		if (rc) {
			SH_ERR("Regulator set_vtg failed vcc_i2c rc=%d\n", rc);
			goto reg_vcc_i2c_put;
		}
	}

	return 0;

reg_vcc_i2c_put:
	regulator_put(sensor->vcc_i2c);
reg_vdd_set_vtg:
	if (regulator_count_voltages(sensor->vdd) > 0)
		regulator_set_voltage(sensor->vdd, 0, FT_VTG_MAX_UV);
reg_vdd_put:
	regulator_put(sensor->vdd);
	return rc;

pwr_deinit:
	if (regulator_count_voltages(sensor->vdd) > 0)
		regulator_set_voltage(sensor->vdd, 0, FT_VTG_MAX_UV);

	regulator_put(sensor->vdd);

	if (regulator_count_voltages(sensor->vcc_i2c) > 0)
		regulator_set_voltage(sensor->vcc_i2c, 0, FT_I2C_VTG_MAX_UV);

	regulator_put(sensor->vcc_i2c);
	return 0;
}

static void cwmcu_hw_config_init(struct CWMCU_T *sensor)
{
	int i = 0;
    int j = 0;
	
	sensor->initial_hw_config = 0;
	for (i = 0; i < HANDLE_ID_END; i++)
    {
		sensor->enabled_list[i] = 0;
        for (j = 0;j<SENSORS_ID_END;j++)
        {
            sensor->sensors_info[i][j].en = 0;
            sensor->sensors_info[i][j].mode= 0;
            sensor->sensors_info[i][j].rate = 0;
            sensor->sensors_info[i][j].timeout= 0;
        }
	}

	sensor->interrupt_status = 0;
	sensor->power_on_list = 0;
	sensor->cal_cmd = 0;
	sensor->cal_type = 0;
	sensor->cal_id = 0;
	sensor->debug_log = 12;
	for (i = 0; i < DRIVER_ID_END; i++)
    {
		sensor->hw_info[i].hw_id=0;
        sensor->calibratorUpdate[i]=0;
        for (j = 0;j<30;j++)
        {
            sensor->calibratordata[i][j]=0;
        }
	}
}

//#if defined(GN_MTK_BSP_PS_DYNAMIC_CALIBRATION)
static int gionee_psensor_calibration(void)
{
    struct CWMCU_T *obj = sensors;
    int error = 0;
    int i = 0;
    int max = 0;
    int data[3];
    int data_total = 0;
    int noise = 0;
    int idx_table = 0;

    SH_FUN();
    if (!obj)
    {
        SH_ERR("obj is NULL!\n");
		goto err;
    }

    error = proximity_calib_en(sensors, 1);
    if (error < 0)
    {
        SH_ERR("proximity_calib_en() failed!\n");
        goto err;
    }
    msleep(100);

    for (i = 0; i < 5; i++)
    {
		if (max++ > 10)
        {
			goto err;
		}
        mdelay(50);
        error = proximity_calib_data(obj, data);
		if (error < 0)
        {
			if (i > 0)
                i--;
			continue;
		}
        data_total += data[0];
    }
    noise = data_total/5;

    for (idx_table = 0; idx_table < ps_cali_noise_num; idx_table++)
    {
        if (noise <= ps_cali_noise[idx_table])
            break;
    }

	if (idx_table >= ps_cali_noise_num)
        goto err;

    obj->ps_threshold_high = ps_cali_offset_high[idx_table] + noise;
    if (obj->ps_threshold_high >= AVAGO9960_PS_MAX)
        obj->ps_threshold_high = AVAGO9960_PS_MAX;

    obj->ps_threshold_low = ps_cali_offset_low[idx_table] + noise;
    if (obj->ps_threshold_low >= AVAGO9960_PS_MAX)
        obj->ps_threshold_low = AVAGO9960_PS_MAX;

	obj->ps_noise = noise;

    error = proximity_set_threshold(obj, obj->ps_threshold_high, obj->ps_threshold_low);
    if (error < 0)
    {
        SH_ERR("proximity_set_threshold() failed!\n");
        goto err;
    }
    SH_LOG("noise:%d, close:%d, far:%d\n", noise, obj->ps_threshold_high, obj->ps_threshold_low);

    proximity_calib_en(sensors, 0);
    msleep(100);

//#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
    ps_cali.close = obj->ps_threshold_high;
    ps_cali.far_away = obj->ps_threshold_low;
    if (ps_cali.far_away == AVAGO9960_PS_MAX)
    {
        ps_cali.valid = 0;
    }
    else
    {
        ps_cali.valid = 1;
    }
//#endif

    return 0;

err:
    obj->ps_threshold_high = AVAGO9960_PS_MAX;
    obj->ps_threshold_low = AVAGO9960_PS_MAX;
    obj->ps_noise = AVAGO9960_PS_MAX;
    ps_cali.close = obj->ps_threshold_high;
    ps_cali.far_away = obj->ps_threshold_low;
    ps_cali.valid = 0;
    proximity_set_threshold(obj, obj->ps_threshold_high, obj->ps_threshold_low);
    proximity_calib_en(sensors, 0);
    msleep(100);
    SH_ERR("gionee_psensor_calibration fail!!!\n");
    return -EFAULT;
}
//#endif

static ssize_t gionee_ps_show_raw_data(struct device_driver *ddri, char *buf)
{
	int error = 0;
    int data[3];

    error = proximity_calib_data(sensors, data);
	if (error < 0)
    {
		SH_ERR("proximity_calib_data() failed!\n");
		return 0;
	}


    return sprintf(buf, "%u\n", data[0]);
}

static ssize_t gionee_ps_show_high_threshold(struct device_driver *ddri, char *buf)
{
    return sprintf(buf, "%d\n", sensors->ps_threshold_high);
}

static ssize_t gionee_ps_show_low_threshold(struct device_driver *ddri, char *buf)
{
    return sprintf(buf, "%d\n", sensors->ps_threshold_low);
}

static void gionee_incall_ps_calibration(void)
{

    SH_FUN();
	if (CW_BOOT == sensors->mcu_mode)
    {
		SH_ERR("in boot mode\n");
		return ;
	}

	if (is_handanswer)
    {
		SH_ERR("user is hand answer \n");
		is_handanswer = false;
		need_incall_cali = false;
		return;
	}

	if (need_incall_cali)
    {
		gionee_psensor_calibration();
        SH_LOG("close:%d, far:%d\n", sensors->ps_threshold_high, sensors->ps_threshold_low);
		need_incall_cali = false;
	}
}

static ssize_t gionee_incall_cali_show(struct device_driver *ddri,char *buf)
{
	return sprintf(buf,"%d,%d\n", is_handanswer, need_incall_cali);
}

static ssize_t gionee_incall_cali_store(struct device_driver *ddri,char *buf,size_t count)
{
	int rt;
	int password = 0;

	rt = sscanf(buf, "%d", &password);
	SH_LOG("password=%d", password);
	switch (password) {
		case 17:
			need_incall_cali = true;
			if (sensors && ( (sensors->enabled_list[NonWakeUpHandle] & (1<<PROXIMITY)) || (sensors->enabled_list[WakeUpHandle] & (1<<PROXIMITY)) ))
			{
				gionee_incall_ps_calibration();
			}
			break;

		case 18:
			need_incall_cali = false;
			break;
			
		case 27:
			is_handanswer = true;
			break;

		case 28:
			is_handanswer = false;

		default:
			need_incall_cali = false;
			is_handanswer = false;
			break;	
	}

	return count;
}

static DRIVER_ATTR(pdata, 0664, gionee_ps_show_raw_data, NULL);
static DRIVER_ATTR(high_threshold, 0664, gionee_ps_show_high_threshold, NULL);
static DRIVER_ATTR(low_threshold, 0664, gionee_ps_show_low_threshold, NULL);
static DRIVER_ATTR(incall_cali, 0660, gionee_incall_cali_show, gionee_incall_cali_store);

static struct driver_attribute *gionee_alsps_attr_list[] = {
    &driver_attr_pdata,
    &driver_attr_high_threshold,
    &driver_attr_low_threshold,
	&driver_attr_incall_cali,
};

static int gionee_alsps_create_attr(struct device_driver *driver)
{
    int idx, err = 0;
    int num = (int)(sizeof(gionee_alsps_attr_list)/sizeof(gionee_alsps_attr_list[0]));

    if (NULL == driver)
    {
        SH_ERR("driver is NULL!\n");
        return -EINVAL;
    }

    for (idx = 0; idx < num; idx++)
    {
        if (err = driver_create_file(driver, gionee_alsps_attr_list[idx]))
        {
            SH_ERR("driver_create_file (%s) = %d\n", gionee_alsps_attr_list[idx]->attr.name, err);
            break;
        }
    }
    return err;
}

static int gionee_alsps_delete_attr(struct device_driver *driver)
{
    int idx ,err = 0;
    int num = (int)(sizeof(gionee_alsps_attr_list)/sizeof(gionee_alsps_attr_list[0]));

    if (NULL == driver)
    {
        SH_ERR("driver is NULL!\n");
        return -EINVAL;
    }

    for (idx = 0; idx < num; idx++)
    {
        driver_remove_file(driver, gionee_alsps_attr_list[idx]);
    }

    return err;
}

static int gionee_alsps_probe(struct platform_device *pdev)
{
    SH_FUN();
    return 0;
}

static int gionee_alsps_remove(struct platform_device *pdev)
{
    SH_FUN();
    return 0;
}

#if defined(CONFIG_OF)
static const struct of_device_id gionee_alsps_of_match[] = {
    {.compatible = "mediatek,als_ps",},
    {},
};
#endif

static struct platform_driver gionee_alsps_driver = {
    .probe      = gionee_alsps_probe,
    .remove     = gionee_alsps_remove,
    .driver 	= {
        .name   = "als_ps",
#if defined(CONFIG_OF)
        .of_match_table = gionee_alsps_of_match,
#endif
    }
};

static int gionee_alsps_open(struct inode *inode, struct file *file)
{
    file->private_data = sensors;

    if (!file->private_data)
    {
        SH_ERR("null pointer!!\n");
        return -EINVAL;
    }

    return nonseekable_open(inode, file);
}

static int gionee_alsps_release(struct inode *inode, struct file *file)
{
    file->private_data = NULL;

    return 0;
}

static long gionee_alsps_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int ps_noise[8];
    int err = 0;
    int i;
    void __user *ptr = (void __user*) arg;
//#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
    struct PS_CALI_DATA_STRUCT ps_cali_temp = {{0,0,0},};
//#endif

    switch (cmd)
    {
        case ALSPS_GET_PS_NOISE:
            SH_LOG("command ALSPS_GET_PS_NOISE\n");
            if (NULL == ptr || CW_BOOT == sensors->mcu_mode)
            {
                SH_ERR("ptr is NULL or mcu in boot mode\n");
                return -EINVAL;
            }

            err = proximity_calib_en(sensors, 1);
            if (err < 0) 
            {
                SH_ERR("P-sensor enable failed!\n");
                return err;
            }
            
            for (i=0; i<8; i++)
            {
                proximity_calib_data(sensors, &(ps_noise[i]));
                msleep(50);
            }

            err = proximity_calib_en(sensors, 0);
            if (err < 0) 
            {
                SH_ERR("P-sensor enable failed!\n");
                return err;
            }

            if (copy_to_user(ptr, ps_noise, sizeof(ps_noise)))
            {
                SH_ERR("copy_to_user failed!\n");
                err = -EFAULT;
            }
            break;

//#if defined(GN_MTK_BSP_PS_STATIC_CALIBRATION)
        case ALSPS_SET_PS_CALI:
            SH_LOG("command ALSPS_SET_PS_CALI\n");
            if (NULL == ptr || CW_BOOT == sensors->mcu_mode)
            {
                SH_ERR("ptr is NULL or mcu in boot mode\n");
                return -EINVAL;
            }

            if (copy_from_user(&ps_cali_temp, ptr, sizeof(ps_cali_temp)))
            {
                SH_ERR("alsps set ps cali_copy from user fail\n");
                return -EFAULT;
            }

            if (ps_cali_temp.valid != 0)
            {
                err = proximity_calib_en(sensors, 1);
                if (err < 0) 
                {
                    SH_ERR("P-sensor enable failed!\n");
                    return err;
                }

                proximity_set_threshold(sensors, (int)ps_cali_temp.close, (int)ps_cali_temp.far_away);

                err = proximity_calib_en(sensors, 0);
                if (err < 0) 
                {
                    SH_ERR("P-sensor enable failed!\n");
                    return err;
                }

                ps_cali.close = ps_cali_temp.close;
                ps_cali.far_away = ps_cali_temp.far_away;
                ps_cali.valid = ps_cali_temp.valid;
            }

            SH_LOG("ALSPS_SET_PS_CALI %d,%d,%d\t", ps_cali_temp.close, ps_cali_temp.far_away, ps_cali_temp.valid);
            break;

        case ALSPS_GET_PS_CALI:
            SH_LOG("command ALSPS_GET_PS_CALI\n");
            if (NULL == ptr || CW_BOOT == sensors->mcu_mode)
            {
                SH_ERR("ptr is NULL or mcu in boot mode\n");
                return -EINVAL;
            }

            gionee_psensor_calibration();

            ps_cali_temp.close  = ps_cali.close;
            ps_cali_temp.far_away = ps_cali.far_away;
            ps_cali_temp.valid = ps_cali.valid;

            if (copy_to_user(ptr, &ps_cali_temp, sizeof(ps_cali_temp)))
            {
                SH_ERR("alsps get ps cali_copy from user fail\n");
                return -EFAULT;
            }
            SH_LOG("ALSPS_GET_PS_CALI %d,%d,%d\t", ps_cali_temp.close, ps_cali_temp.far_away,ps_cali_temp.valid);
            break;
//#endif

        default:
            SH_ERR("unknown IOCTL: 0x%08x\n", cmd);
            err = -ENOIOCTLCMD;
            break;
    }
    return err;
}


static struct file_operations gionee_alsps_fops = {
    .owner = THIS_MODULE,
    .open = gionee_alsps_open,
    .release = gionee_alsps_release,
    .unlocked_ioctl = gionee_alsps_unlocked_ioctl,
};

static struct miscdevice gionee_alsps_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "als_ps",
    .fops = &gionee_alsps_fops,
};

static int gionee_gsensor_calibration(void)
{
    int status = 0;
    uint8_t data[30];
    int error = 0;
	int loop = 5;

    mutex_lock(&multi_thread);
    error = sensors_calib_start(sensors, ACCELERATION);
    msleep(1000);

    if (error < 0)
    {
        SH_ERR("sensors_calib_start() Fail!\n");
        mutex_unlock(&multi_thread);
        return error;
    }

    do {
        msleep(200);
		loop--;
        sensors_calib_status(sensors, ACCELERATION, &status);
    } while ((CALIB_STATUS_INPROCESS == status) && (loop>0));

    if (CALIB_STATUS_PASS == status)
    {
    	//msleep(100);
        error = sensors_calib_data_read(sensors, ACCELERATION, data);
		SH_ERR("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
        data[0],  data[1],  data[2],  data[3],  data[4],  data[5],  data[6],  data[7],  data[8],  data[9], 
        data[10], data[11], data[12], data[13], data[14], data[15], data[16], data[17], data[18], data[19],
        data[20], data[21], data[22], data[23], data[24], data[25], data[26], data[27], data[28], data[29]);
        if (error < 0)
        {
            SH_ERR("sensors_calib_data_read() Fail!\n");
        }
        else
            sensors_calib_data_write(sensors, ACCELERATION, data);
    }

    mutex_unlock(&multi_thread);
    return status;
}


static int gionee_gyroscope_calibration(void)
{
    int status = 0;
    uint8_t data[30];
    int error = 0;
	int loop = 5;

    mutex_lock(&multi_thread);
    error = sensors_calib_start(sensors, GYRO);
    msleep(1000);
	SH_ERR("error = %d\n", error);
    if (error < 0)
    {
        SH_ERR("sensors_calib_start() Fail!\n");
        mutex_unlock(&multi_thread);
        return error;
    }

    do {
        msleep(200);
		loop--;
        sensors_calib_status(sensors, GYRO, &status);
    } while ((CALIB_STATUS_INPROCESS == status) && (loop>0));

    if (CALIB_STATUS_PASS == status)
    {
        error = sensors_calib_data_read(sensors, GYRO, data);
		SH_ERR("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
        data[0],  data[1],  data[2],  data[3],  data[4],  data[5],  data[6],  data[7],  data[8],  data[9], 
        data[10], data[11], data[12], data[13], data[14], data[15], data[16], data[17], data[18], data[19],
        data[20], data[21], data[22], data[23], data[24], data[25], data[26], data[27], data[28], data[29]);
        if (error < 0)
        {
            SH_ERR("sensors_calib_data_read() Fail!\n");
        }
        else
            sensors_calib_data_write(sensors, GYRO, data);
    }

    mutex_unlock(&multi_thread);
    return status;
}

static int gionee_gsensor_open(struct inode *inode, struct file *file)
{
    file->private_data = sensors;

    if (!file->private_data)
    {
        SH_ERR("null pointer!!\n");
        return -EINVAL;
    }

    return nonseekable_open(inode, file);
}

static int gionee_gsensor_release(struct inode *inode, struct file *file)
{
    file->private_data = NULL;

    return 1;
}

static long gionee_gsensor_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    void __user *user_data;
    char strbuf[256];
    int err = 0;
    int x=0, y=0, z=9800;
    SENSOR_DATA sensor_data;
    SH_FUN();

    switch(cmd) {
        case GSENSOR_IOCTL_READ_SENSORDATA:
            SH_LOG("GSENSOR_IOCTL_READ_SENSORDATA\n");
            user_data = (void __user *) arg;
            if(NULL == user_data)
            {
                SH_ERR("arg is null!\n");
                err = -EINVAL;
                break;
            }

            SH_LOG("X Y Z = %d %d %d\n", x, y, z);
            sprintf(strbuf, "%d %d %d", x, y, z);
            if(copy_to_user(user_data, strbuf, strlen(strbuf)+1))
            {
                SH_ERR("arg is null!\n");
                err = -EFAULT;
            }

            break;

        case GSENSOR_IOCTL_SET_CALI:
            break;

        case GSENSOR_IOCTL_GET_CALI:
            SH_LOG("GSENSOR_IOCTL_GET_CALI\n");
            user_data = (void __user*)arg;
            if (NULL == user_data)
            {
                SH_ERR("arg is null!\n");
                break;
            }
			
            err = gionee_gsensor_calibration();
            if (err < 0)
            {
                SH_ERR("gsensor calibration failed!\n");
                break;
            }
			msleep(100);
            err = gionee_gyroscope_calibration();
			if (err < 0)
            {
                SH_ERR("gyro calibration failed!\n");
                break;
            }

            sensor_data.x = 0;
            sensor_data.y = 0;
            sensor_data.z = 0;

            if ((err = copy_to_user(user_data, &sensor_data, sizeof(sensor_data))))
            {
                SH_ERR("copy_to_user() failed!\n");
                err = -EFAULT;
            }
            break;

        default:
            SH_ERR("unknown IOCTL: 0x%08x\n", cmd);
            err = -ENOIOCTLCMD;
            break;
    }
	SH_ERR("err = %d\n", err);
    return err;
}

static struct file_operations gionee_gsensor_fops = {
    .owner = THIS_MODULE,
    .open = gionee_gsensor_open,
    .release = gionee_gsensor_release,
    .unlocked_ioctl = gionee_gsensor_unlocked_ioctl,
};

static struct miscdevice gionee_gsensor_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "gsensor",
    .fops = &gionee_gsensor_fops,
};

static int gionee_msensor_open(struct inode *inode, struct file *file)
{
    file->private_data = sensors;

    if (!file->private_data)
    {
        SH_ERR("null pointer!!\n");
        return -EINVAL;
    }

    return nonseekable_open(inode, file);
}

static int gionee_msensor_release(struct inode *inode, struct file *file)
{
    file->private_data = NULL;
    return 1;
}

static long gionee_msensor_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    void __user *user_data;
    char buf[64];
    int x = 0;
    int y = 0;
    int z = 0;
    int err = 0;

    switch (cmd)
    {
        case MSENSOR_IOCTL_INIT:
            user_data = (void __user *) arg;
            break;

        case MSENSOR_IOCTL_SET_MODE:
            break;

        case MSENSOR_IOCTL_READ_SENSORDATA:
            user_data = (void __user*)arg;
            if (NULL == user_data)
            {
                SH_ERR("arg is null!\n");
                break;
            }

            SH_LOG("X Y Z = %d %d %d\n", x, y, z);
            memset(buf, 0, 64);
            sprintf(buf,"%d %d %d", x, y, z);

            if (copy_to_user(user_data, buf, strlen(buf)+1))
            {
                return -EFAULT;
            }

            break;

        default:
            SH_ERR("unknown IOCTL: 0x%08x\n", cmd);
            err = -ENOIOCTLCMD;
            break;
    }
    return err;

}

static struct file_operations gionee_msensor_fops = {
    .owner = THIS_MODULE,
    .open = gionee_msensor_open,
    .release = gionee_msensor_release,
    .unlocked_ioctl = gionee_msensor_unlocked_ioctl,
};

static struct miscdevice gionee_msensor_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "msensor",
    .fops = &gionee_msensor_fops,
};

static int CWMCU_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct CWMCU_T *mcu;
    struct iio_dev *indio_dev;
	int error;
	
    SH_FUN();

	/* mcu reset */
	mt_set_gpio_mode(GPIO_CW_MCU_RESET, GPIO_CW_MCU_RESET_M_GPIO);
	mt_set_gpio_dir(GPIO_CW_MCU_RESET, GPIO_DIR_OUT);
    mt_set_gpio_pull_enable(GPIO_CW_MCU_RESET, true);
	mt_set_gpio_pull_select(GPIO_CW_MCU_RESET, 0);

	mt_set_gpio_mode(GPIO_CW_MCU_BOOT, GPIO_CW_MCU_BOOT_M_GPIO);
	mt_set_gpio_dir(GPIO_CW_MCU_BOOT, GPIO_DIR_OUT);
    mt_set_gpio_pull_enable(GPIO_CW_MCU_BOOT, true);
	mt_set_gpio_pull_select(GPIO_CW_MCU_BOOT, 0);

	msleep(1);
	mt_set_gpio_out(GPIO_CW_MCU_BOOT, 0);
	//mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);

	mt_set_gpio_mode(GPIO_CW_MCU_WAKE_UP, GPIO_CW_MCU_WAKE_UP_M_GPIO);
	mt_set_gpio_dir(GPIO_CW_MCU_WAKE_UP, GPIO_DIR_OUT);
    mt_set_gpio_pull_enable(GPIO_CW_MCU_WAKE_UP, true);
	mt_set_gpio_pull_select(GPIO_CW_MCU_WAKE_UP, GPIO_PULL_UP);
    msleep(1);
	mt_set_gpio_out(GPIO_CW_MCU_WAKE_UP, 0);

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
		SH_ERR("CwMcu: i2c_check_functionality error\n");
		return -EIO;
	}

#ifdef CONFIG_64BIT
    firmware_update_dma_va_read = (u8 *)dma_alloc_coherent(&client->dev, 1024, &firmware_update_dma_pa_read, GFP_KERNEL);
    firmware_update_dma_va_write = (u8 *)dma_alloc_coherent(&client->dev, 1024, &firmware_update_dma_pa_write, GFP_KERNEL);
#else
    firmware_update_dma_va_read = (u8 *)dma_alloc_coherent(NULL, 1024, &firmware_update_dma_pa_read, GFP_KERNEL);
    firmware_update_dma_va_write = (u8 *)dma_alloc_coherent(NULL, 1024, &firmware_update_dma_pa_write, GFP_KERNEL);
#endif

    if(!firmware_update_dma_va_read || !firmware_update_dma_va_write)
    {
        SH_ERR("Allocate DMA I2C Buffer failed\n");
        return -EIO;
    }

	indio_dev = iio_device_alloc(sizeof(*mcu));
	//indio_dev = iio_allocate_device(sizeof(*mcu));
	if (!indio_dev)
    {
		SH_ERR("iio_device_alloc failed\n");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, indio_dev);

	indio_dev->name = CWMCU_I2C_NAME;
	indio_dev->dev.parent = &client->dev;
	indio_dev->info = &cw_info;
	indio_dev->channels = cw_channels;
	indio_dev->num_channels = ARRAY_SIZE(cw_channels);
	indio_dev->modes |= INDIO_BUFFER_TRIGGERED;

	mcu = iio_priv(indio_dev);
	mcu->client = client;
	mcu->indio_dev = indio_dev;

    mcu->ps_threshold_high = 0;
    mcu->ps_threshold_low = 0;
    mcu->ps_raw_data = 0;
    mcu->ps_noise = AVAGO9960_PS_MAX;

    sensors = mcu;

#ifdef CWMCU_INTERRUPT
	mcu->client->irq = CUST_EINT_SENSOR_HUB_NUM;

	SH_LOG("sensor->client->irq  = %d\n", mcu->client->irq);

	if (mcu->client->irq > 0) {
	    mt_eint_mask(CUST_EINT_SENSOR_HUB_NUM);
        mt_eint_registration(CUST_EINT_SENSOR_HUB_NUM, CUST_EINT_SENSOR_HUB_TYPE, CWMCU_interrupt_thread, 0);
	    mt_set_gpio_dir(GPIO_CW_MCU_INTERRUPT, GPIO_DIR_IN);
	    mt_set_gpio_mode(GPIO_CW_MCU_INTERRUPT, GPIO_CW_MCU_INTERRUPT_M_EINT);
	    mt_set_gpio_pull_enable(GPIO_CW_MCU_INTERRUPT, TRUE);
	    mt_set_gpio_pull_select(GPIO_CW_MCU_INTERRUPT, GPIO_PULL_UP);
	    mt_eint_set_hw_debounce(CUST_EINT_SENSOR_HUB_NUM, CUST_EINT_SENSOR_HUB_DEBOUNCE_CN);

        INIT_WORK(&mcu->work, cwmcu_work_report);
	    //mt_eint_unmask(CUST_EINT_SENSOR_HUB_NUM);
	}
#endif

	error = cwstm_power_init(mcu,true);
	if (error)
    {
		SH_ERR("power init failed\n");
	}

	error = cwstm_power_on(mcu,true);
	if (error)
    {
		SH_ERR("power on failed\n");
	}
    atomic_set(&mcu->delay, 20);
    INIT_DELAYED_WORK(&mcu->delay_work, cwmcu_delwork_report);

#ifdef CWMCU_MUTEX
	mutex_init(&mcu->mutex_lock);
    mutex_init(&mcu->mutex_lock_i2c);
#endif
    mcu->supend_flag = 0;
	mcu->mcu_mode = CW_NORMAL;

	error = cw_probe_buffer(indio_dev);
	if (error)
    {
		SH_ERR("iio yas_probe_buffer failed\n");
		goto error_free_dev;
	}

	error = cw_probe_trigger(indio_dev);
	if (error)
    {
		SH_ERR("iio yas_probe_trigger failed\n");
		goto error_remove_buffer;
	}

	error = iio_device_register(indio_dev);
	if (error)
    {
		SH_ERR("iio iio_device_register failed\n");
		goto error_remove_trigger;
	}

	error = create_sysfs_interfaces(mcu);
	if (error)
    {
		SH_ERR("create_sysfs_interfaces\n");
		goto err_free_mem;
    }

    if ((error = misc_register(&gionee_alsps_device))) 
    {
	    SH_ERR("misc register failed\n");
        goto err_free_mem;
    }

    if (error = gionee_alsps_create_attr(&gionee_alsps_driver.driver))
    {
        SH_ERR("create attribute err = %d\n", error);
        goto err_free_alsps_device;
    }

    if ((error = misc_register(&gionee_gsensor_device))) 
    {
	    SH_ERR("misc register failed\n");
        goto err_free_alsps_sysfs;
    }

    if ((error = misc_register(&gionee_msensor_device))) 
    {
	    SH_ERR("misc register failed\n");
        goto err_free_gsensor_device;
    }

	atomic_set(&mcu->delay, 20);
	init_irq_work(&mcu->iio_irq_work, iio_trigger_work);

	cwmcu_hw_config_init(mcu);
	
    mcu->driver_wq = create_singlethread_workqueue("cywee_mcu");
	i2c_set_clientdata(client, mcu);
	pm_runtime_enable(&client->dev);

// Gionee chengx 20150429 add for CR01461753 begin
#ifdef CONFIG_HAS_EARLYSUSPEND
	register_early_suspend(&mtk_cwmcu_early_suspend_handler);
#endif
// Gionee chengx 20150429 add for CR01461753 end

#ifdef CWMCU_INTERRUPT
    mt_eint_unmask(CUST_EINT_SENSOR_HUB_NUM);
#endif

	mt_set_gpio_out(GPIO_CW_MCU_RESET, 1);
	
	SH_LOG("probe success\n");
	return 0;

err_free_gsensor_device:
    misc_deregister(&gionee_gsensor_device);
err_free_alsps_sysfs:
    gionee_alsps_delete_attr(&gionee_alsps_driver.driver);
err_free_alsps_device:
    misc_deregister(&gionee_alsps_device);
err_free_mem:
	iio_device_unregister(indio_dev);
error_remove_trigger:
	cwmcu_remove_trigger(indio_dev);
error_remove_buffer:
	cwmcu_remove_buffer(indio_dev);
error_free_dev:
	iio_device_free(indio_dev);
	//iio_free_device(indio_dev);
	i2c_set_clientdata(client, NULL);
#ifdef CONFIG_64BIT
    dma_free_coherent(&client->dev, 1024, firmware_update_dma_va_read, firmware_update_dma_pa_read);
    dma_free_coherent(&client->dev, 1024, firmware_update_dma_va_write, firmware_update_dma_pa_write);
#else
    dma_free_coherent(NULL, 1024, firmware_update_dma_va_read, firmware_update_dma_pa_read);
    dma_free_coherent(NULL, 1024, firmware_update_dma_va_write, firmware_update_dma_pa_write);
#endif
    firmware_update_dma_va_read = NULL;
    firmware_update_dma_va_write = NULL;
    firmware_update_dma_pa_read = 0;
    firmware_update_dma_pa_write = 0;
    sensors = NULL;

	return error;
}

static int CWMCU_i2c_remove(struct i2c_client *client)
{
	struct CWMCU_T *sensor = i2c_get_clientdata(client);
	kfree(sensor);

    if (firmware_update_dma_va_read || firmware_update_dma_va_write)
    {
#ifdef CONFIG_64BIT
        dma_free_coherent(&client->dev, 1024, firmware_update_dma_va_read, firmware_update_dma_pa_read);
        dma_free_coherent(&client->dev, 1024, firmware_update_dma_va_write, firmware_update_dma_pa_write);
#else
        dma_free_coherent(NULL, 1024, firmware_update_dma_va_read, firmware_update_dma_pa_read);
        dma_free_coherent(NULL, 1024, firmware_update_dma_va_write, firmware_update_dma_pa_write);
#endif
        firmware_update_dma_va_read = NULL;
        firmware_update_dma_va_write = NULL;
        firmware_update_dma_pa_read = 0;
        firmware_update_dma_pa_write = 0;
    }

// Gionee chengx 20150429 add for CR01461753 begin
#ifdef CONFIG_HAS_EARLYSUSPEND
    unregister_early_suspend(&mtk_cwmcu_early_suspend_handler);
#endif
// Gionee chengx 20150429 add for CR01461753 end

    misc_deregister(&gionee_msensor_device);
    misc_deregister(&gionee_gsensor_device);
    gionee_alsps_delete_attr(&gionee_alsps_driver.driver);
    misc_deregister(&gionee_alsps_device);
    sensors = NULL;
	return 0;
}

// Gionee chengx 20150429 add for CR01461753 begin
#ifndef CONFIG_HAS_EARLYSUSPEND
static const struct dev_pm_ops CWMCU_pm_ops = {
	.suspend = CWMCU_suspend,
	.resume = CWMCU_resume
};
#endif
// Gionee chengx 20150429 add for CR01461753 end

static const struct i2c_device_id CWMCU_id[] = {
	{ CWMCU_I2C_NAME, 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, CWMCU_id);

static struct i2c_driver CWMCU_driver = {
	.driver = {
		.name = CWMCU_I2C_NAME,
		.owner = THIS_MODULE,
// Gionee chengx 20150429 add for CR01461753 begin
#ifndef CONFIG_HAS_EARLYSUSPEND
		.pm = &CWMCU_pm_ops,
#endif
// Gionee chengx 20150429 add for CR01461753 end
	},
	.probe    = CWMCU_i2c_probe,
	.remove   = CWMCU_i2c_remove,
	.id_table = CWMCU_id,
};

static struct i2c_board_info __initdata CWMCU_board_info = {I2C_BOARD_INFO(CWMCU_I2C_NAME, CWMCU_ADDR)};

static int __init CWMCU_i2c_init(void)
{
    SH_FUN();
    i2c_register_board_info(2, &CWMCU_board_info, 1);

    if (platform_driver_register(&gionee_alsps_driver))
    {
        SH_ERR("failed to register driver!\n");
        return -ENODEV;
    }

	return i2c_add_driver(&CWMCU_driver);
}

static void __exit CWMCU_i2c_exit(void)
{
	i2c_del_driver(&CWMCU_driver);
    platform_driver_unregister(&gionee_alsps_driver);
}

module_init(CWMCU_i2c_init);
module_exit(CWMCU_i2c_exit);

MODULE_DESCRIPTION("CWMCU I2C Bus Driver");
MODULE_AUTHOR("CyWee Group Ltd.");
MODULE_LICENSE("GPL");

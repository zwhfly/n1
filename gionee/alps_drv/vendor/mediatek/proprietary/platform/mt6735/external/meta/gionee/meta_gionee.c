/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/sensors_io.h>
#include "meta_gionee.h"
#include "meta_gionee_para.h"

#define VERSION_FILE_PATH           "/system/build.prop"
#define RELEASE_SW_VERSION           "ro.gn.gnvernumber"
#define GN_VERNUMBERREL             "ro.gn.gnvernumberrel"
#define BATT_VOLT_FILE_PATH         "/dev/MT_pmic_adc_cali"
#define PS_NOISE_DATA_PATH          "/dev/als_ps"

#define MAX_BUFFER_SIZE 128

#define GIONEE_PREFIX   "GIONEE: "
#define GIONEE_LOG(fmt, arg ...) printf(GIONEE_PREFIX fmt, ##arg)


/* IOCTO */
#define ADC_CHANNEL_READ 		_IOW('k', 4, int)

#define HWM_ALSPS_DEV          "/dev/als_ps"

//mali get ps noise end
/* ADC CHannel */
#define ADC_BAT_SEN 1

#define ADC_COUNT 5

//extern int WriteDataToPC(void *Local_buf,unsigned short Local_len,void *Peer_buf,unsigned short Peer_len);

/* The string of ro.gn.gnvernumber is like "S101_GNGBW122A_T2012_B", 
 * And factory not need "T2012" in string  but the string of ro.gn.gnvernumberrel, which is like "12" 
 * So the function changes "S101_GNGBW122A_T2012_B" to "S101_GNGBW122A_T12_B"
 */


int str_handle(char *src, char *string, char s)
{
    char *src_tmp;
	          
    src_tmp = strchr(src, s);//find the first '_�
    if( NULL == src_tmp)
    		return -1;

    src_tmp++;//pass '_'
    src_tmp = strchr(src_tmp, s);//find the second '_' 
    if( NULL == src_tmp)
    		return -1;

    src_tmp += 2; // /pass '_' && 'T'
    *src_tmp++ = '\0';
    
    src_tmp = strchr(src_tmp, s); //find the third '_'
    if( NULL == src_tmp)
    		return -1;

    strcat(string,src_tmp);
    strcat(src,string);  

   return 0;  
}

//int to char*
static void itoa(int n,char* string)
{
	char tmp[33];
	int yu;
	char *p = tmp;

	int num;

	if(n < 0)
		num = -n;
	else
	    num=n;

	while(num){
	   yu=num%10;
	   num=num/10;
	   *p++=yu+'0';
	}

	char *q=string;

	if(n < 0)
		*q++='-';

	while(p > tmp){
	   *q++=*--p;
	}

	*q=0;
}



/*Input : ChannelNUm, Counts*/ 
/*Output : Sum, Result (0:success, 1:failed)*/
static int get_ADC_channel(int adc_channel, int adc_count)
{
	int fd = -1;
	int ret = 0;
	int adc_in_data[2] = {1,1}; 
	
	fd = open(BATT_VOLT_FILE_PATH,O_RDONLY, 0);
	if (fd == -1) {
		GIONEE_LOG(TAG "get_ADC_channel - Can't open /dev/MT_pmic_adc_cali\n");
 		return -1;
	}

	adc_in_data[0] = adc_channel;
	adc_in_data[1] = adc_count;
	ret = ioctl(fd, ADC_CHANNEL_READ, adc_in_data);
	close(fd);

	if (adc_in_data[1]==0) { 
		GIONEE_LOG(TAG "read channel[%d] %d times : %d\n", adc_channel, adc_count, adc_in_data[0]);		
		return adc_in_data[0];
	}
	
	return -1;
}


void Meta_GIONEE_OP_R_ADC_VBAT_VOLT(GIONEE_CNF *cnf, char *buf, unsigned short buf_len)
{
	int temp = 0;
	int vbat = 0;
	int ret_str_len = MAX_BUFFER_SIZE;
	char voltage[33];

	temp = get_ADC_channel(ADC_BAT_SEN, ADC_COUNT); 
	if (temp != -1) {		
		vbat = (temp/ADC_COUNT);
        cnf->status = META_SUCCESS;
	}
    else
	{
		GIONEE_LOG(" et_ADC_channel fail! \n");
		cnf->status = META_FAILED;
	}
    
    GIONEE_LOG("Meta vbat =  %d \n",vbat);
	itoa(vbat,voltage);
    GIONEE_LOG("Meta voltage =  %s \n",voltage);

	ret_str_len = strlen(voltage);
	WriteDataToPC(cnf, sizeof(GIONEE_CNF),voltage,ret_str_len);  
}
//mali read ps cali data begin

void  Meta_GIONEE_R_PS_NOISE(GIONEE_CNF *cnf, char *buff, unsigned short len)
{
    int ret_str_len = MAX_BUFFER_SIZE;
    int fd = -1;
    int tmp =0;
    int i;
    
    //char ps_data[20]= {0};
    //char ps_data[2][4] = {{0,0}};
    int ps_noise[8]={0} ;
    
    fd = open(HWM_ALSPS_DEV, O_RDONLY);
	GIONEE_LOG("yang_yang open als_ps\n");
    if(fd < 0)
    {
        GIONEE_LOG("mali open alsps_dev error\n");
        return ;
    }
	GIONEE_LOG("yang_yang before ioctl als_ps\n");

    tmp = ioctl(fd, ALSPS_GET_PS_NOISE, ps_noise);
	
	GIONEE_LOG("yang_yang after ioctl als_ps\n");
    if(tmp)
    {
        GIONEE_LOG("mali ioctl ALSPS_GET_PS_NOISE error \n");
        return ;
    }
    GIONEE_LOG("mali ioctl ALSPS_GET_PS_NOISE ps_data[1] = %d, ps_data[2] = %d ps_data[3] = %d ps_data[4] = %d ps_data[5] = %d\n",ps_noise[0],ps_noise[1],ps_noise[2],ps_noise[3],ps_noise[4]);
#if 0
    for(i=0;i<2;i++)
    {
        itoa(ps_noise[i], ps_data[i]);
    }
    //itoa(ps_noise, ps_data);
#endif
    close(fd);
    
    ret_str_len = sizeof(ps_noise);
    
    if(WriteDataToPC(cnf, sizeof(GIONEE_CNF), ps_noise, ret_str_len))
    {
        GIONEE_LOG("mali meta read ps_noise is OK !!\n");
    }
    else
    {
        GIONEE_LOG("mali meta read ps_noise is ERROR !!\n");
    }

}

//mali read ps cali data end

void Meta_GIONEE_OP_R_SW_RELEASE(GIONEE_CNF *cnf, char *buf, unsigned short buf_len)
{
    FILE *fd = 0;
    int i = 0;
    char str[256] = {0};
    char *loc,*temp,read_char;
    char ret_str[MAX_BUFFER_SIZE],vernum[10];
    int ret_str_len = MAX_BUFFER_SIZE;

    if((fd = fopen(VERSION_FILE_PATH,"r")) == NULL)
    {
        GIONEE_LOG("%s can't open file : %s \n",__func__,VERSION_FILE_PATH);
    }
    else
    {
        while(!feof(fd))
        {
            if(fgets(str, 256, fd)!=NULL)
            {
                temp = str;
                loc = strsep(&temp, "=");
                if(!strcmp(loc, RELEASE_SW_VERSION))
                {
                    GIONEE_LOG("[FT_GetVersionInfo] SW Version = %s!\n", temp);
                    strcpy((char*)ret_str, temp);  
                }
                if(!strcmp(loc, GN_VERNUMBERREL))
                {
                    GIONEE_LOG("[FT_GN_VERNUMBERREL] Compile Version = %s!\n", temp);
                    strcpy((char*)vernum, temp);
                    break;  
                }                 
            }
        }
        fclose(fd);
        
        if(strlen(vernum) != 0)
        {
            for(i = 0 ;i < 10;i++)
            {
                if((vernum[i]  == '\r') || (vernum[i] == '\n' ))
                {
                    vernum[i] = '\0';
                    break;
                }
            }

            if(str_handle(ret_str,vernum,'-')){
				if(str_handle(ret_str,vernum,'_')){
				   GIONEE_LOG("str_handle fail !\n");
                   cnf->status = META_SUCCESS;
				}  
                else
                   cnf->status = META_FAILED;
			} 
            
        }
         
        GIONEE_LOG("[FT_GN_VERNUMBER + REL] %s \n",ret_str);
      
        ret_str_len = strlen(ret_str);
        
        WriteDataToPC(cnf, sizeof(GIONEE_CNF),ret_str,ret_str_len);        
    }
}
//Gionee BSP1 yaoyc 20140910 add for remote ir board test CR01376011 begin
#ifdef REMOTE_IR_SUPPORT
#ifdef LATTICE_ICE40_IR
#define SPI_IOC_MAGIC			'k'
#define ICE40_IOCTL_RD_META_TEST  _IOR(SPI_IOC_MAGIC, 11, __u8)
#define REMOTE_IR_NAME	"/dev/spidev0.0"
#define REMOTE_IR_CHECK_HW ICE40_IOCTL_RD_META_TEST
#else
#define REMOTE_IR_NAME " "
#define REMOTE_IR_CHECK_HW 0
#endif
void Meta_GIONEE_CHECK_REMOTE_IR_HW(GIONEE_CNF *cnf, char *buff, unsigned short len)
{
	
    int fd = -1;
    int ret = 0;
    unsigned char result = 0;
    
    fd = open(REMOTE_IR_NAME, O_RDONLY);
	GIONEE_LOG("open remote ir device : %s\n",REMOTE_IR_NAME);
    if(fd < 0)
    {
        GIONEE_LOG("open remote ir device error\n");
        return ;
    }

    ret = ioctl(fd, REMOTE_IR_CHECK_HW, &result);
	
    if(ret)
    {
        GIONEE_LOG("ioctl ICE40_IOCTL_RD_META_TEST error \n");
		close(fd);
        return ;
    }
    GIONEE_LOG("ioctl ICE40_IOCTL_RD_META_TEST  result = %d\n",result);

    close(fd);
    
    
    if(result == 1)
    {
        GIONEE_LOG("meta remote ir board test is OK !!\n");
		cnf->status = META_SUCCESS;
    }
    else
    {
        GIONEE_LOG("meta remote ir board test is ERROR !!\n");
		cnf->status = META_FAILED;
    }
	WriteDataToPC(cnf, sizeof(GIONEE_CNF), NULL, 0);
	return;
}
#endif
//Gionee BSP1 yaoyc 20140910 add for remote ir board test CR01376011 end

void Meta_GIONEE_OP(GIONEE_REQ *req, char *peer_buff, unsigned short peer_len)
{
	GIONEE_CNF cnf;

	memset(&cnf, 0, sizeof(GIONEE_CNF));

	cnf.header.id = req->header.id+1;
	cnf.header.token = req->header.token;
	cnf.type = req->type;
	cnf.status = META_SUCCESS;

	GIONEE_LOG("%s : req->type: %d, req->cmd.m_u1Dummy: %d, peer_len: %d\n", __FUNCTION__, req->type, req->cmd.m_u1Dummy, peer_len);
	switch (req->cmd.m_u1Dummy) 
	{
		case GIONEE_W_VIA_SN:
		case GIONEE_R_VIA_SN:
		case GIONEE_W_VIA_MEID:
		case GIONEE_R_VIA_MEID:
		case GIONEE_R_VIA_SW:
		case GIONEE_W_VIA_TESTFLAG:
		case GIONEE_R_VIA_TESTFLAG:
             GIONEE_LOG("Request Type: %d is not supported now\n", req->type);
			break;
        case GIONEE_R_SW_RELEASE:
            Meta_GIONEE_OP_R_SW_RELEASE(&cnf, peer_buff, peer_len);
            break;
		case GIONEE_R_ADC_VBAT_VOLT:
			Meta_GIONEE_OP_R_ADC_VBAT_VOLT(&cnf, peer_buff, peer_len);
            break;
//mali read ps cali data begin
        case GIONEE_R_PS_NOISE:
			GIONEE_LOG("yang_yang read ps noise\n");
            Meta_GIONEE_R_PS_NOISE(&cnf, peer_buff, peer_len);
			GIONEE_LOG("after yang_yang read ps noise\n");
            break;
//mali read ps cali data end
//Gionee BSP1 yaoyc 20140910 add for remote ir board test CR01376011 begin
#ifdef REMOTE_IR_SUPPORT
		case GIONEE_CHECK_IR_HW:
			GIONEE_LOG("check remote ir hw \n");
			Meta_GIONEE_CHECK_REMOTE_IR_HW(&cnf, peer_buff, peer_len);
			break;
#endif
//Gionee BSP1 yaoyc 20140910 add for remote ir board test CR01376011 end
		default:
			GIONEE_LOG("Unknow Request Type: %d\n", req->type);
			cnf.status = META_FAILED;
			WriteDataToPC(&cnf, sizeof(GIONEE_CNF), NULL, 0);
			break;
	}
	GIONEE_LOG("%s : Finish !\n", __FUNCTION__);
}

BOOL Meta_GIONEE_Init(void)
{
	return true;
}

BOOL Meta_GIONEE_Deinit(void)
{
	return true;
}

package aurora.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;

class ScrollIconLinearLayout extends LinearLayout implements View.OnClickListener{
	//Picture of a cursor
	private Bitmap cursorBitmap;
	//Cursor color
	private int cursorColor = -1;
	//Cursor drawing area
	private Rect colorRect = new  Rect();
	//Paint of cursor
	private Paint colorPaint = new  Paint();
	private OnItemClickListener mOnItemClickListener;
	//Color when the element is selected
	private int textColorSelected;
	//Color when the element is  selected
	private int textColorNormal;
	//tab item text size
	private float textSize = -1;
	//The cursor at the top of the view
	private boolean cursorTop = false;
	//The height space to draw cursor
	private int curserWidth;
	private int curserHeight = 20;
	private String[] stringsTexts;
	private int[] mIconsNormal;
	private int[] mIconsFocus;
	
	
	public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
		this.mOnItemClickListener = mOnItemClickListener;
	}
	public interface OnItemClickListener{
		public abstract void onClick(int index);
	}
	public ScrollIconLinearLayout(Context context) {
		this(context, null);
	}

	public ScrollIconLinearLayout(Context context, AttributeSet attrs) {
		this(context, attrs, -1);
	}
	
	public ScrollIconLinearLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);		
		setWillNotDraw(false);  
		init();		
		//setElevation(20);
	}
	
	private void init(){
		DisplayMetrics metric = new DisplayMetrics();
        ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metric);
        windowWidth = metric.widthPixels; 
	}
	
	/**
	 * let tab items to refresh
	 * @param foucusIndex
	 */
	private void invalidateAll(int foucusIndex){
		for(int i=0; i<getChildCount(); i++){
			TextView textView = (TextView)getChildAt(i);
			textView.setTextColor(getResources().getColor(foucusIndex==i?textColorSelected:textColorNormal));
			if(mIconsNormal!=null && mIconsNormal.length>0){
				textView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(foucusIndex==i?mIconsFocus[i]:mIconsNormal[i], getContext().getTheme())	, null, null);
			}
		}
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		init();
	}
	
	private int windowWidth;
	private int childCount;
	private int eachWidth;
	
	private int scrollLeft; 
	private int colorLeft; 
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		if(childCount==0){
			return;
		}
		eachWidth = windowWidth/childCount;
		int realPosition = Math.round(position+positionOffset);		
    	if(childCount>0){
    		colorLeft = (int)((position+positionOffset)*eachWidth)+(curserWidth==0?0:((eachWidth-curserWidth)/2));
    		if(cursorBitmap!=null){
    			scrollLeft = colorLeft+(eachWidth-cursorBitmap.getWidth())/2;
    		}
    		invalidateAll(realPosition);
    		invalidate();
    	}
    	moveView();
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	if(scrollLeft >= 0){
    		//draw bitmap
        	if(childCount>0 && cursorBitmap!=null){
        		canvas.drawBitmap(cursorBitmap, scrollLeft, cursorTop?0:(getHeight()-cursorBitmap.getHeight()), null);
        	}
        	//draw color rect
        	if(childCount>0 && cursorColor>0){
        		colorRect.left = colorLeft;
        		colorRect.top = cursorTop?0:(getHeight()-curserHeight);
        		colorRect.right = colorLeft+(curserWidth==0?eachWidth:curserWidth);
        		colorRect.bottom = cursorTop?curserHeight:getHeight();
        		canvas.drawRect(colorRect, colorPaint);
        	}
    	}else{
    		onPageScrolled(0, 0, 0);
    	}
    }

	@Override
	public void onClick(View v) {
		int count = getChildCount();
		for(int i=0; i<count; i++){
			if(v==getChildAt(i)){
				mOnItemClickListener.onClick(i);
				return;
			}
		}
	}
	
	private ViewGroup mViewPager;
	private int bottomRoot = 0;
	public void setViewPager(ViewGroup mViewPager, int bottomRoot) {
		this.mViewPager = mViewPager;
		this.bottomRoot = bottomRoot;
	}
	
	public void setViewPager(ViewGroup mViewPager) {
		this.mViewPager = mViewPager;
	}
	
	int[] location = new int[2] ; 	
	private View moveView(){
		if(mViewPager!=null && bottomRoot!=0){
			int count = mViewPager.getChildCount();
			for(int i=0; i<count; i++){
				View movePage = mViewPager.getChildAt(i);
				movePage.getLocationInWindow(location);
				View moveRootView = movePage.findViewById(bottomRoot);
				if(moveRootView!=null && location[0]>=-windowWidth && location[0]<=windowWidth){
					if(moveRootView!=null && location[0]>=-windowWidth/2 && location[0]<=windowWidth/2){
						moveRootView.setVisibility(View.VISIBLE);
						int moveOffer = (int)(Math.abs(((float)location[0])/windowWidth)*((View)moveRootView.getParent()).getHeight());
						moveRootView.setLeft(-location[0]);
						moveRootView.setTop((int)(moveOffer*3));
						moveRootView.invalidate();
					}else{
						moveRootView.setTop(((View)moveRootView.getParent()).getHeight());
						moveRootView.invalidate();
					}
				}
			}
		}
		return null;
	}

	/**
	 * set cursor color
	 * @param cursorColor
	 */
	public void setCursorColor(int cursorColor) {
		this.cursorColor = cursorColor;
		if(cursorColor>0){
			colorPaint.setColor(getResources().getColor(cursorColor));
		}
	}
	
	/**
	 * set item textSize
	 * @param textSize
	 */
	public void setTextSize(float textSize) {
		this.textSize = textSize;
		if(isAttachedToWindow()){
			resetChildViews();
		}
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		resetChildViews();
	}
	
	/**
	 * set cursorBitmap
	 * @param cursorBitmap		// TODO Auto-generated method stub
		
	 */
	public void setCursorBitmap(int cursorBitmap){
		this.cursorBitmap = BitmapFactory.decodeResource(getResources(), cursorBitmap);
	}
	
	/**
	 * set tab item title textIds
	 * @param strings
	 */
	public void setTitles(int[] strings) {
		if(strings!=null && strings.length>0){
			this.stringsTexts = new String[strings.length];
			for(int i=0; i<stringsTexts.length; i++){
				stringsTexts[i] = getResources().getString(strings[i]);
			}
		}else{
			stringsTexts = null;
		}
		if(isAttachedToWindow()){
			resetChildViews();
		}
	}
	
	public void setTitles(String[] strings) {
		this.stringsTexts = strings;
		if(isAttachedToWindow()){
			resetChildViews();
		}
	}
	
	/**
	 * 刷新view
	 */
	private void resetChildViews(){
		int textLength = stringsTexts!=null?stringsTexts.length:0;
		int iconLength = mIconsNormal!=null?mIconsNormal.length:0;
		//如果只单独设了文字或者图片,那么长度为设置了的属性长度
		if(textLength==0 || iconLength==0){
			childCount = Math.max(textLength, iconLength);
		}else{//否则是较短的数组长度
			childCount = Math.min(textLength, iconLength);
		}
		int needAddCount = childCount- getChildCount();
		if(needAddCount>0){//增加TextView
			for(int i=0; i<needAddCount; i++){
				TextView textView = new TextView(getContext());
				textView.setGravity(Gravity.CENTER);
				textView.setTextColor(getResources().getColor(textColorNormal));
				textView.setOnClickListener(this);
				LayoutParams mLayoutParams = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
				textView.setLayoutParams(mLayoutParams);
				addView(textView);
			}
		}else{//减少TextView
			for(int i=0; i<-needAddCount; i++){
				removeViewAt(0);
			}
		}
		if(childCount>0){
			for(int i=0; i<childCount; i++){
				TextView textView = (TextView)getChildAt(i);
				if(textSize>=0){
					textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
				}
				if(stringsTexts!=null){
					textView.setText(stringsTexts[i]);
				}
				if(mIconsNormal!=null){
					textView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(mIconsNormal[i], getContext().getTheme()) , null, null);
				}
			}
		}
	}
	
	/**
	 * set tab item iconIds
	 * @param normalIconIds 
	 * @param focusIconIds
	 */
	public void setIcons(int[] normalIconIds, int[] focusIconIds) {
		mIconsNormal = normalIconIds;
		mIconsFocus = focusIconIds;
		if(isAttachedToWindow()){
			resetChildViews();
		}
	}
	
	/*
	 * if the cursorTop at top
	 */
	public void setCursorAtTop(boolean cursorTop){
		if(this.cursorTop!=cursorTop){
			this.cursorTop = cursorTop;
			invalidate();
		}
	}
	
	/**
	 * 设置游标高度
	 * @param curserHeight 像素高度
	 */
	public void setCurserHeight(int curserHeight) {
		this.curserHeight = curserHeight;
	}

	/**
	 * 设置游标宽度
	 * @param curserWidth 像素宽度
	 */
	public void setCurserWidth(int curserWidth) {
		this.curserWidth = curserWidth;
	}
	
	public void seTextColorNormal(int normalTextColor) {
		textColorNormal = normalTextColor;
	}

	public void seTextColorFocus(int selectedTextColor) {
		textColorSelected = selectedTextColor;
	}
}

package aurora.widget;

import java.util.ArrayList;

import android.app.usage.UsageEvents.Event;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.aurora.utils.Constants;

import aurora.app.AuroraDialog;

/**
 * @author leftaven
 * @2013年9月12日 aurora menu
 */
public class AuroraSystemMenu extends AuroraMenuBase {

	public ListView menuListView;
	public LinearLayout layout;

	private OnAuroraMenuItemClickListener auroraSystemMenuCallBack;
	

	
	public void setSystemMenuItemClickListener(OnAuroraMenuItemClickListener auroraSystemMenuCallBack){
	    this.auroraSystemMenuCallBack = auroraSystemMenuCallBack;
	}
	
	private Handler mMenuActionHandler;
	private Runnable mMenuAciton = new Runnable() {
        
        @Override
        public void run() {
            // TODO Auto-generated method stub
            
        }
    };
    
    public void setMenuWidth(int width) {
    	ViewGroup.LayoutParams params = menuListView.getLayoutParams();
		params.width = width;
		menuListView.setLayoutParams(params);
    }
    
    public void setMenuHeight(int height) {
    	ViewGroup.LayoutParams params = menuListView.getLayoutParams();
		params.height = height;
		menuListView.setLayoutParams(params);
    }
    
    public void setMenuWidthToMin() {
    	setMenuWidth(mContext.getResources().getDimensionPixelSize(com.aurora.R.dimen.aurora_system_menu_width_min));
    }
    
    public void setMenuWidthToNormal() {
    	setMenuWidth(mContext.getResources().getDimensionPixelSize(com.aurora.R.dimen.aurora_system_menu_width_normal));
    }
    
	/**
	 * 计算listview高度
	 * 
	 * @param pull
	 */
	public void setPullLvHeight(ListView pull) {
		int totalHeight = 0;
		ListAdapter adapter = pull.getAdapter();
		for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, pull);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = pull.getLayoutParams();
		params.height = totalHeight
				+ (pull.getDividerHeight() * (pull.getCount() - 1));
		pull.setLayoutParams(params);
	}
	private void setCallBack(OnAuroraMenuItemClickListener auroraMenuCallBack,int position){
	    auroraMenuItem = (AuroraMenuItem) menuAdapter
                .getItem(position);
        auroraMenuCallBack.auroraMenuItemClick(auroraMenuItem
                .getId());
	}

	public AuroraSystemMenu(final Context context,
			final OnAuroraMenuItemClickListener auroraMenuCallBack,
			final AuroraMenuAdapterBase auroraMenuAdapter, int aniTabMenu,
			int resId) {
		super(context);
		mMenuActionHandler = new Handler();
		menuAdapter = auroraMenuAdapter;
		mContext = context;
		layout = (LinearLayout) LayoutInflater.from(context).inflate(resId,
				null);
		layout.setFocusableInTouchMode(true);// 重点，再次点击menu键，窗口消失
		
		auroraSystemMenuCallBack = auroraMenuCallBack;

		menuListView = (ListView) layout.findViewById(com.aurora.internal.R.id.auroraMenuContentLv);
		menuListView.setAdapter(menuAdapter);
		menuListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View itemView,
					int position, long arg3) {
				boolean itemEnable = menuAdapter.isItemEnable(position);
				if(!itemEnable){
					return;
				}
				if (auroraSystemMenuCallBack != null) {
				    AuroraDialog.showFromPopupWindow();
					if (isShowing() ) {// 菜单项目触发，隐藏menu
						dismissMenu();
					}
					if(Looper.myLooper() == mMenuActionHandler.getLooper()){
					    setCallBack(auroraSystemMenuCallBack,position);
					}else {
					    mMenuActionHandler.postDelayed(mMenuAciton, 50);
                    }
				}
			}
		});
		layout.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// 单击menu菜单时拦截事件
				if (keyCode == KeyEvent.KEYCODE_MENU && isShowing() && event.getAction() == KeyEvent.ACTION_DOWN) {
					dismissMenu();
				} else if (keyCode == KeyEvent.KEYCODE_BACK && isShowing() && event.getAction() == KeyEvent.ACTION_DOWN) {
					dismissMenu();
					// dismiss();
				}
				return false;
			}
		});

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismissMenu();
			}
		});

		setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				dismissMenu();
			}
		});

		defaultMenu(layout, aniTabMenu);
		
		if (resId == com.aurora.R.layout.aurora_menu_fillparent) {
			((AuroraSystemMenuAdapter)menuAdapter).setAdapterLayoutItemMatchParent(true);
			this.setWidth(LayoutParams.MATCH_PARENT);
		} else {
			this.setAnimationStyle(com.aurora.R.style.AuroraMenuRightTopAnimation);
		}
	}

	
	/**
	 * 增加菜单项
	 * 
	 * @param itemId
	 */
	public void addMenuItemById(int itemId) {
		menuAdapter.addMenuItemById(itemId);
		setPullLvHeight(menuListView);
		menuAdapter.notifyDataSetChanged();
	}

	/**
	 * 删除菜单项
	 * 
	 * @param itemId
	 */
	public void removeMenuItemById(int itemId) {
		menuAdapter.removeMenuItemById(itemId);
		setPullLvHeight(menuListView);
		menuAdapter.notifyDataSetChanged();
	}

	// Aurora <leftaven> <2013年9月13日> modify for OTA begin
	/**
	 * 单个menu项设置是否可用
	 * 
	 * @param itemId
	 * @param isEnable
	 */
	public void setMenuItemEnable(int itemId, boolean isEnable) {
		menuAdapter.setMenuItemEnable(itemId, isEnable);
	}
	
	//Aurora add by tangjun 2014.7.7 start
	public void addMenu(int itemId,int titleRes,int iconRes) {
		menuAdapter.addMenu(itemId, titleRes, iconRes);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}

	public void addMenu(int itemId,int titleRes,int iconRes, int position) {
		menuAdapter.addMenu(itemId, titleRes, iconRes, position);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}
	
	public void addMenu(int itemId,CharSequence menuText,int iconRes) {
		menuAdapter.addMenu(itemId, menuText, iconRes);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}

	public void addMenu(int itemId,CharSequence menuText,int iconRes, int position) {
		menuAdapter.addMenu(itemId, menuText, iconRes, position);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}
	
	public void removeMenuByItemId(int itemId) {
		menuAdapter.removeMenuByItemId(itemId);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}
	
	public void removeMenuByPosition(int position) {
		menuAdapter.removeMenuByPosition(position);
		menuAdapter.notifyDataSetChanged();
		setPullLvHeight(menuListView);
		update();
	}
	
	public void setMenuTextByItemId(int menuTextResId, int itemId) {
		menuAdapter.setMenuTextByItemId(menuTextResId, itemId);
		menuAdapter.notifyDataSetChanged();
		update();
	}
	
	public void setMenuTextByItemId(CharSequence menuText, int itemId) {
		menuAdapter.setMenuTextByItemId(menuText, itemId);
		menuAdapter.notifyDataSetChanged();
		update();
	}
	
	public void setMenuTextByPosition(int menuTextResId, int position) {
		menuAdapter.setMenuTextByPosition(menuTextResId, position);
		menuAdapter.notifyDataSetChanged();
		update();
	}
	
	public void setMenuTextByPosition(CharSequence menuText, int position) {
		menuAdapter.setMenuTextByPosition(menuText, position);
		menuAdapter.notifyDataSetChanged();
		update();
	}
	//Aurora add by tangjun 2014.7.7 end
}

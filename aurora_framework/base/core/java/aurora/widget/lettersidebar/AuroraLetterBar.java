package aurora.widget.lettersidebar;


import java.lang.reflect.Array;
import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import aurora.widget.lettersidebar.AuroraLetterSideBar.OnTouchingLetterChangedListener;

public class AuroraLetterBar extends LinearLayout implements OnTouchingLetterChangedListener{

	 private LinearLayout mToastLayout;
	 private TextView mOverlayText;
	 private Handler mHandler = new Handler() ;
	 private OverlayThread mOverlayThread = new OverlayThread();
	
	private ListView mListview;
	private ArrayList<String> mStringArray;

	public AuroraLetterBar(Context context) {
		this(context, null);
	}

	public AuroraLetterBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public AuroraLetterBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		mContext = context;

		LayoutInflater.from(context).inflate(com.aurora.R.layout.aurora_lettersidebar, this, true);

		initViews();
	}
	
 	private class OverlayThread implements Runnable {
		public void run() {
			if(mToastLayout != null){
				mToastLayout.setVisibility(View.GONE);
			}			
		}
	}
	
	public void setListViewAndString(ListView listview, ArrayList<String> stringArray) {
		
		mListview = listview;
		mStringArray = stringArray;
	}

	private void initViews() {
		mToastLayout = (LinearLayout)findViewById(com.aurora.R.id.toastLayout);
		mOverlayText = (TextView)findViewById(com.aurora.R.id.tvLetter);
		
		AuroraLetterSideBar letterSideBar = (AuroraLetterSideBar)findViewById(com.aurora.R.id.letterSideBar);
		letterSideBar.setOnTouchingLetterChangedListener(this);
	}

	@Override
	public void onTouchingLetterChanged(String s, float positionOfY) {
		// TODO Auto-generated method stub
		if(mOverlayText == null || 
				mToastLayout == null ||
				mHandler == null || 
				mOverlayThread == null ||
				mListview == null ||
				mStringArray == null){
			return ;
		}
		int position;
		updateOverlayPosition(positionOfY);
		mOverlayText.setText(s);
		mToastLayout.setVisibility(View.VISIBLE);	
		mHandler.removeCallbacks(mOverlayThread);
		mHandler.postDelayed(mOverlayThread, 1000);
        if(AuroraLetterSideBar.b[0].equals(s) && 
        		mStringArray.size() > 0){
        	mListview.setSelection(0);
        }else if ((position = alphaIndexer(mStringArray,s)) >= 0) {
        	mListview.setSelection(position);
		}
	}
	
	/**
	 * 更新显示选中字母的位置
	 * @param downPositionOfY 点击点在Y轴的位置
	 */
	private void updateOverlayPosition(float downPositionOfY){
		
	}

	private int alphaIndexer(ArrayList<String> stringArray, String s) {		
		int position = -1;
		if(TextUtils.isEmpty(s) || stringArray == null){
			return 0;
		}

		for (int i = 0; i < stringArray.size(); i++) {
			if(HanziToPinyin.getSpell(stringArray.get(i)).startsWith(s)){
				position = i;
				break;
			}
		}
		return position;
	}
}

package aurora.widget;

import com.aurora.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 创建一个类似Toast对象.可以添加点击事件执行回调
 * @author luolaigang
 *
 */
public class AuroraToast extends LinearLayout {
	/**
	 * 显示时长2秒
	 */
	public static final int LENGTH_SHORT = 2000;

	/**
	 * 显示时长3.5秒
	 */
	public static final int LENGTH_LONG = 3500;

	private int duration = 0;
	private static Activity activity;
	private static AuroraToast auroraToast;
	private TextView infoText;
	private TextView deleteText;
	public AuroraToast(Activity activity) {
		this(activity, null);
		setBackgroundColor(getResources().getColor(R.color.toast_bg_color));
		int pandding = getResources().getDimensionPixelSize(R.dimen.toast_pandding);		
		int panddingTop = getResources().getDimensionPixelSize(R.dimen.toast_pandding_top);				
		setPadding(pandding, panddingTop, pandding, panddingTop);
	}

	private void addInFrame(){
		removeFromFrame();
		FrameLayout frameLayout = (FrameLayout)activity.getWindow().getDecorView();
		FrameLayout.LayoutParams layoutParamsInfo = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParamsInfo.gravity = Gravity.BOTTOM;
		int leftRightMarging = getResources().getDimensionPixelSize(R.dimen.toast_marging_left_right);
		int bottomMarging = getResources().getDimensionPixelSize(R.dimen.toast_marging_bottom);
		layoutParamsInfo.setMargins(leftRightMarging, 0, leftRightMarging, bottomMarging);
		frameLayout.addView(this, layoutParamsInfo);
	}
	
	private void removeFromFrame(){
		if(auroraToast.getParent()!=null){
			((ViewGroup)auroraToast.getParent()).removeView(auroraToast);
		}
	}
	
	public AuroraToast(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public AuroraToast(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setOrientation(LinearLayout.HORIZONTAL);
		initView();
	}
	
	private void initView() {
		float textSize = getResources().getDimension(R.dimen.toast_textsize);
		infoText = new TextView(getContext());
		infoText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		infoText.setBackground(null);
		infoText.setPadding(0, 0, 0, 0);
		infoText.setEnabled(false);
		infoText.setTextColor(Color.WHITE);
		LayoutParams layoutParamsInfo = new LayoutParams(0, LayoutParams.WRAP_CONTENT);
		layoutParamsInfo.weight = 1;
		layoutParamsInfo.gravity = Gravity.CENTER_VERTICAL;
		addView(infoText, layoutParamsInfo);

		deleteText = new TextView(getContext());
		deleteText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		deleteText.setText(R.string.toast_delete);
		deleteText.setTextColor(getResources().getColor(R.color.toast_delete_color));
		deleteText.setBackground(null);
		deleteText.setPadding(0, 0, 0, 0);
		deleteText.setClickable(true);
		LayoutParams layoutParamsDelete = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParamsDelete.gravity = Gravity.CENTER_VERTICAL;
		addView(deleteText, layoutParamsDelete);
	}

	/**
	 * 创建Toast
	 * @param activity Toast所在Activity
	 * @param text 需要显示的文本
	 * @param duration 显示时长(毫秒)
	 * @param onClickListener (点击撤销执行的OnClickListener)
	 * @return
	 */
	public static AuroraToast makeText(Activity activity, String text, int duration, OnClickListener onClickListener) {
		if(AuroraToast.activity != activity){
			AuroraToast.activity = activity;
			if(auroraToast!=null){
				auroraToast.removeFromFrame();
				auroraToast.showHandle.removeMessages(SHOW);
				auroraToast.showHandle.removeMessages(HIDE);
			}
		}
		if (auroraToast == null) {
			auroraToast = new AuroraToast(activity);
		}
		auroraToast.infoText.setText(text);
		auroraToast.duration = duration;
		auroraToast.deleteText.setOnClickListener(onClickListener);
		return auroraToast;
	}

	private static final int SHOW = 100;
	private static final int HIDE = 200;
	private Handler showHandle = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(auroraToast==null){
				return;
			}			
			if ((msg.what == SHOW)&&auroraToast.getParent()==null) {
				auroraToast.addInFrame();
				showHandle.sendEmptyMessageDelayed(HIDE, auroraToast.duration);
			}else if(( msg.what == HIDE)&&auroraToast.getParent()!=null){
				auroraToast.removeFromFrame();
			}
		};
	};

	/**
	 * 显示Toast
	 */
	public void show() {
		if(showHandle!=null){
			showHandle.sendEmptyMessage(SHOW);
		}
	}

	/**
	 * 创建Toast
	 * @param activity Toast所在Activity
	 * @param resId 需要显示的文本id
	 * @param duration 显示时长(毫秒)
	 * @param onClickListener (点击撤销执行的OnClickListener)
	 * @return
	 */
	public static AuroraToast makeText(Activity activity, int resId, int duration, OnClickListener onClickListener) {
		return makeText(activity, activity.getResources().getString(resId), duration, onClickListener);
	}

}

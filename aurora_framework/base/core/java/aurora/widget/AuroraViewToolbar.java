package aurora.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.util.Log;

import com.aurora.R;
import com.aurora.utils.DensityUtil;

/**
 * The base class is used to support toolbar popped up form views.
 *
 */
public abstract class AuroraViewToolbar {
    private static final int TOLERANCE_TOUCH = 3;
    private static final int TOOLBAR_ITEM_PADDING_LEFT_AND_RIGHT = 12;
    private static final int TOOLBAR_ITEM_PADDING_BOTTOM = 3;
    private static final int TOOLBAR_POSITION_OFFSET_TO_SCREEN_LEFT = 14;
    private static final int TOOLBAR_POSITION_OFFSET_TO_SCREEN_RIGHT = 14;
    private static final int TOOLBAR_ARROW_OFFSET_TO_EDGE = 15;
    protected View mHostView;
    protected Context mContext;

    protected WindowManager mWindowManager;
    protected WindowManager.LayoutParams mLayoutParams = null;
    protected LayoutInflater mLayoutInflater;
    protected ViewGroup mToolbarGroup;
    protected View mToolbarView;
    protected ImageView mToolbarPositionArrowView;

    protected boolean mShowing = false;
    private int mLeftDrawableResId;
    private int mCenterDrawableResId;
    private int mRightDrawableResId;
    private Drawable mSingleDrawable;
    private Drawable mArrowAboveDrawable;
    private Drawable mArrowBelowDrawable;

    private int mStatusBarHeight;
    private int mToolbarPositionArrowWidth;
    private int mToolbarPositionArrowHeight;

    private int mPositionX, mPositionY;

    protected int mToleranceTouch;
    protected int mToolbarItemPaddingLeftAndRight;
    protected int mToolbarItemPaddingBottom;

    private final int TOOLBAR_TOP_OFFSET = 30;
    private float mScale;
    
    private int mEditBarArrowPaddingLeft;
    private int mEditBarArrowPaddingRight;
    
    /**
     * adjust the toolbar show position when touch on the contact edit area.
     */
    private int mOffsetToolbar = TOOLBAR_TOP_OFFSET;

    public AuroraViewToolbar(View hostView) {
        this.mHostView = hostView;
        this.mContext = mHostView.getContext();
        mScale = mContext.getResources().getDisplayMetrics().density;

        // initial resources
        Resources resources = mHostView.getResources();
        mLeftDrawableResId = R.drawable.aurora_text_toolbar_left;
        mCenterDrawableResId = R.drawable.aurora_text_toolbar_center;
        mRightDrawableResId = R.drawable.aurora_text_toolbar_right;
        mSingleDrawable = resources.getDrawable(R.drawable.aurora_text_toolbar_single);
        mArrowAboveDrawable = resources.getDrawable(R.drawable.aurora_text_toolbar_position_arrow_above);
        mArrowBelowDrawable = resources.getDrawable(R.drawable.aurora_text_toolbar_position_arrow_below);
        // initial window manager
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        calculateTolerance();
        mStatusBarHeight = getStatusBarHeight();
        // initial tool bar and it's items.
        mLayoutInflater = LayoutInflater.from(mContext);
        mToolbarView = mLayoutInflater.inflate(R.layout.aurora_text_toolbar, null);
        mToolbarGroup = (ViewGroup) mToolbarView.findViewById(com.aurora.internal.R.id.aurora_toolbar_group);
        mToolbarPositionArrowView = (ImageView) mToolbarView.findViewById(com.aurora.internal.R.id.aurora_toolbar_position_arrow);
       
        // calculate initial size of tool bar.
        mToolbarView.measure(0, 0);
        mToolbarPositionArrowWidth = mToolbarPositionArrowView.getMeasuredWidth();
        mToolbarPositionArrowHeight = mToolbarPositionArrowView.getMeasuredHeight();
        
        mEditBarArrowPaddingLeft = mToolbarPositionArrowView.getPaddingLeft();
        mEditBarArrowPaddingRight = mToolbarPositionArrowView.getPaddingRight();
    }

    /**
     * @return Whether the toolbar is showing.
     */
    public boolean isShowing() {
        return mShowing;
    }

    /**
     * Show toolbar at assigned position relative to left-top of screen.
     * @param screenX
     * @param screenY
     * @param selected
     */
    public void show(int screenX, int screenY, boolean selected) {
        if (!mShowing) {
            showInternal(screenX, screenY, 0, selected);
        }
    }

    /**
     * Move toolbar to assigned position relative to left-top of screen.
     * @param screenX
     * @param screenY
     */
    public void move(int screenX, int screenY, boolean selected) {
        if (mShowing) {
            moveInternal(screenX, screenY, 0, selected);
        }
    }

    /**
     * Hide the toolbar.
     */
    public void hide() {
        if (mShowing) {
            try {
                mToolbarPositionArrowView.setPadding(0, 0, 0, 0);
                mWindowManager.removeViewImmediate(mToolbarView);
            } finally {
                // set showing flag whether hiding view is successful.
                mShowing = false;
            }
        }
    }

    /**
     * Update items of toolbar.
     */
    protected abstract void updateToolbarItems();

    protected void showInternal(int screenX, int screenY, int cursorLineHeight, boolean selected) {
        // update tool bar.
        update();
        if (mToolbarGroup.getChildCount() < 1) {
            hide();
            return;
        }
        prepare(screenX, screenY, cursorLineHeight, selected);
        // reposition the toolbar.
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.token = mHostView.getApplicationWindowToken();
        lp.x = mPositionX;
        lp.y = mPositionY;
        lp.width = LayoutParams.WRAP_CONTENT;
        lp.height = LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.LEFT | Gravity.TOP;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_PANEL;
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            lp.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
            lp.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE;
        } else {
            lp.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
            lp.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
        }
        lp.packageName = mContext.getPackageName();
        mLayoutParams = lp;
        mWindowManager.addView(mToolbarView, lp);
        // set showing flag
        mShowing = true;
    }

    protected void moveInternal(int screenX, int screenY, int cursorLineHeight, boolean selected) {
        if (mToolbarGroup.getChildCount() < 1) {
            hide();
            return;
        }
        prepare(screenX, screenY, cursorLineHeight, selected);
        // reposition the toolbar.
        WindowManager.LayoutParams lp = mLayoutParams;
        lp.x = mPositionX;
        lp.y = mPositionY;
        mWindowManager.updateViewLayout(mToolbarView, lp);
    }
    
    private boolean mInInDialog;
    protected void aboveCursor(boolean inInDialog){
    	mInInDialog = inInDialog;
    }

    private void prepare(int screenX, int screenY, int cursorLineHeight, boolean selected) {
        // calculate the size of tool bar.
        mToolbarView.measure(0, 0);
        // calculate the position of tool bar.
        boolean aboveCursor = calculatePosition(screenX, screenY, cursorLineHeight, selected);
        if(mInInDialog){
        	aboveCursor = true;
        }
        if (aboveCursor) {
            mToolbarPositionArrowView.setImageDrawable(mArrowBelowDrawable);
            mToolbarPositionArrowView.setPadding(mEditBarArrowPaddingLeft, mToolbarGroup.getMeasuredHeight(),
                    mEditBarArrowPaddingRight, 0);
        } else {
            mToolbarPositionArrowView.setImageDrawable(mArrowAboveDrawable);
            mToolbarGroup.setPadding(0, mOffsetToolbar, 0, 0);
            mToolbarPositionArrowView.setPadding(mEditBarArrowPaddingLeft, mOffsetToolbar-mArrowBelowDrawable.getIntrinsicHeight(), mEditBarArrowPaddingRight, 0);
        }
        
    }

    private void update() {
        updateToolbarItems();
        // set drawable of items.
        int childCount = mToolbarGroup.getChildCount();
        if (childCount >= 2) {
            mOffsetToolbar = (int)(TOOLBAR_TOP_OFFSET * mScale);
            for (int i = 0; i < childCount; i++) {
                View view = mToolbarGroup.getChildAt(i);
                if (i == 0) {
                    view.setBackgroundResource(mLeftDrawableResId);
                } else if (i == childCount - 1) {
                    view.setBackgroundResource(mRightDrawableResId);
                } else {
                    view.setBackgroundResource(mCenterDrawableResId);
                }
            }
        } else if (childCount == 1) {
            mOffsetToolbar = 0;
            View view = mToolbarGroup.getChildAt(0);
            view.setBackgroundDrawable(mSingleDrawable);
        }
    }

    private boolean calculatePosition(int screenX, int screenY, int cursorLineHeight, boolean selected) {
        boolean aboveCursor = true;
        // calculate x
        int x;
        int px = screenX - mHostView.getRootView().getScrollX();
        int half = mToolbarGroup.getMeasuredWidth() / 2;
        int displayWidth = mWindowManager.getDefaultDisplay().getWidth();
        if (px + half < displayWidth) {
            x = px - half;
        } else {
            x = displayWidth - mToolbarGroup.getMeasuredWidth();
        }
        mPositionX = Math.max(0, x);
        // calculate y
        int y;
        int py = screenY - mHostView.getRootView().getScrollY();
        int th = mToolbarGroup.getMeasuredHeight() + mToolbarPositionArrowHeight;
        int lh = cursorLineHeight / 2;
        if (py - th - lh < mStatusBarHeight) {
            y = py + lh + (selected ? mToleranceTouch : 0) + 2;
            mStatusBarHeight = y;//Luofu modified
            aboveCursor = false;
        } else {
            y = py - th - lh - (selected ? mToleranceTouch : 0) + 6;
            aboveCursor = true;
        }
        
        mPositionY = Math.max(mStatusBarHeight, y);
        Log.e("luofu", "mPositionY:"+mPositionY);
        Log.e("luofu", "mPositionY:"+mPositionY);
        
//        Log.e("luofu", "mStatusBarHeight:"+mStatusBarHeight);
//        Log.e("luofu", "mPositionY:"+mPositionY);
//        Log.e("luofu", "mPositionY:"+mPositionY);
        return aboveCursor;
    }
    
 

    private void calculateTolerance() {
        DisplayMetrics dm = new DisplayMetrics();
        this.mWindowManager.getDefaultDisplay().getMetrics(dm);
        float ratio = 1.0f * dm.densityDpi / DisplayMetrics.DENSITY_MEDIUM;
        mToleranceTouch = Math.round(TOLERANCE_TOUCH * ratio);
        mToolbarItemPaddingLeftAndRight = Math.round(TOOLBAR_ITEM_PADDING_LEFT_AND_RIGHT * ratio);
        mToolbarItemPaddingBottom = Math.round(TOOLBAR_ITEM_PADDING_BOTTOM * ratio);
    }
    
    private int getStatusBarHeight () {
        /*Rect rect = new Rect();
        Context context = mHostView.getContext();
        if (context instanceof Activity) {
            Window window = ((Activity)context).getWindow();
            if (window != null) {
              window.getDecorView().getWindowVisibleDisplayFrame(rect);
              android.view.View v = window.findViewById(Window.ID_ANDROID_CONTENT);
    
              android.view.Display display = ((android.view.WindowManager) mHostView.getContext()
                      .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    
              //return result title bar height
              return display.getHeight() - v.getBottom() + rect.top;   
        }
        }
        return 0;*/
        return (int)DensityUtil.getStatusBarHeight(mContext);
    }
    
    
}


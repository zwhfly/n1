package com.android.internal.app;

import java.util.List;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import aurora.view.PagerAdapter;

public class AuroraResolverViewPagerGridViewAdapter extends PagerAdapter {

	
	private List<View > mViewList ;
	
	public AuroraResolverViewPagerGridViewAdapter( List<View > viewList) {
		// TODO Auto-generated constructor stub
		this.mViewList = viewList;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mViewList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0 == arg1;
	}
	
	
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TODO Auto-generated method stub
		View tmpView = mViewList.get(position);
		if(tmpView.getParent() != null){
			ViewGroup parent = (ViewGroup) tmpView.getParent();
			parent.removeView(tmpView);
		}
		container.addView(tmpView,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		return tmpView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		container.removeView((View)object);
	}
	

}

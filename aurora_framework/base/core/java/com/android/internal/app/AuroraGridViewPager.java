package com.android.internal.app;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import aurora.view.ViewPager;

public class AuroraGridViewPager extends ViewPager {
	
	private int mRowNumber = 3;
	
	public AuroraGridViewPager(Context context) {
		super(context);
	}

	public AuroraGridViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void setGridRowNumber(int rowNumber){
		mRowNumber = rowNumber;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int height = 0;
		// 下面遍历所有child的高度
		for (int i = 0; i < getChildCount(); i++) {
			View child = getChildAt(i);
			child.measure(widthMeasureSpec,
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
			int h = child.getMeasuredHeight();
			if (h > height) // 采用最大的view的高度。
				height = h;
		}
	
		
		height = mRowNumber*height;
		heightMeasureSpec = MeasureSpec.makeMeasureSpec(height,
				MeasureSpec.EXACTLY);

		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}

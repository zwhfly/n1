package com.aurora.utils;

public class Log {
	public static String TAG_HEAD = "";
	public static final int VERBOSE = 2;
	public static final int DEBUG = 3;
	public static final int INFO = 4;
	public static final int WARN = 5;
	public static final int ERROR = 6;
	public static final int ASSERT = 7;

	public static void i(String tag, String msg) {
		android.util.Log.i(TAG_HEAD + tag, msg);
	}

	public static void i(String tag, String msg, Throwable e) {
		android.util.Log.i(TAG_HEAD + tag, msg, e);
	}

	public static void d(String tag, String msg) {
		android.util.Log.d(TAG_HEAD + tag, msg);
	}

	public static void d(String tag, String msg, Throwable e) {
		android.util.Log.d(TAG_HEAD + tag, msg, e);
	}

	public static void w(String tag, String msg) {
		android.util.Log.w(TAG_HEAD + TAG_HEAD + tag, msg);
	}

	public static void w(String tag, String msg, Throwable e) {
		android.util.Log.w(TAG_HEAD + tag, msg, e);
	}

	public static int w(String tag, Throwable tr) {
		return android.util.Log.w(tag, tr);
	}

	public static void e(String tag, String msg) {
		android.util.Log.e(TAG_HEAD + tag, msg);
	}

	public static void e(String tag, String msg, Throwable e) {
		android.util.Log.e(TAG_HEAD + tag, msg, e);
	}

	public static void v(String tag, String msg) {
		android.util.Log.v(TAG_HEAD + tag, msg);
	}

	public static void v(String tag, String msg, Throwable e) {
		android.util.Log.v(TAG_HEAD + tag, msg, e);
	}

	public static void wtf(String tag, String msg) {
		android.util.Log.wtf(TAG_HEAD + tag, msg);
	}

	public static void wtf(String tag, String msg, Throwable e) {
		android.util.Log.wtf(TAG_HEAD + tag, msg, e);
	}

	public static boolean isLoggable(String tag, int level) {
		return android.util.Log.isLoggable(TAG_HEAD + tag, level);
	}

}

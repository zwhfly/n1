package com.aurora.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;

public class PhotoUtils {
	private static final Canvas sCanvas = new Canvas();
	private static final Rect sOldBounds = new Rect();
	private static final int sIconDefaultSize=192;
	static {
        sCanvas.setDrawFilter(new PaintFlagsDrawFilter(Paint.DITHER_FLAG,
                Paint.FILTER_BITMAP_FLAG));
    }
	
	@SuppressLint("NewApi")
	public static Drawable getIconDrawable(String packageName, String resourceName,
            Context context){
		Drawable drawable =null;
        Resources resources = context.getResources();
		if (resources != null) {
			int resId = resources.getIdentifier(resourceName, "drawable",
					packageName);
			if (resId == 0) {
				return null;
			}
			drawable = resources.getDrawableForDensity(resId,AbstractIconGetter.mIconDpi);
		}
		return drawable;
	}
	
	public static int getDimen(String packageName, String resourceName,
            Context context,int defaultValue){
		int dimen =1;
		int resId =0;
        Resources resources = context.getResources();
		if (resources != null) {
			resId = resources.getIdentifier(resourceName, "dimen",
					packageName);
		}
		if(resId==0){
			return defaultValue;
		}
		dimen = resources.getDimensionPixelSize(resId);
		Log.i("GetDimen","getDimen="+dimen);
		return dimen;
	}
	
	public static Bitmap createIconBitmap(String packageName, String resourceName,
            Context context) {
		   Drawable drawable = getIconDrawable(packageName,resourceName,context);
           return drawable==null?null:createIconBitmap(drawable, context);
	}
	
	public static Bitmap createIconBitmap(Drawable icon, Context context) {
		synchronized (sCanvas) {
			 final int iconBitmapSize = getIconBitmapSize();
	            int width = iconBitmapSize;
	            int height = iconBitmapSize;
	            if (icon instanceof PaintDrawable) {
	                PaintDrawable painter = (PaintDrawable) icon;
	                painter.setIntrinsicWidth(width);
	                painter.setIntrinsicHeight(height);
	            } else if (icon instanceof BitmapDrawable) {
	                // Ensure the bitmap has a density.
	                BitmapDrawable bitmapDrawable = (BitmapDrawable) icon;
	                Bitmap bitmap = bitmapDrawable.getBitmap();
	                if (bitmap.getDensity() == Bitmap.DENSITY_NONE) {
	                    bitmapDrawable.setTargetDensity(context.getResources().getDisplayMetrics());
	                }
	            }
	            int sourceWidth = icon.getIntrinsicWidth();
	            int sourceHeight = icon.getIntrinsicHeight();
	            if (sourceWidth > 0 && sourceHeight > 0) {
	                // Scale the icon proportionally to the icon dimensions
	                final float ratio = (float) sourceWidth / sourceHeight;
	                if (sourceWidth > sourceHeight) {
	                    height = (int) (width / ratio);
	                } else if (sourceHeight > sourceWidth) {
	                    width = (int) (height * ratio);
	                }
	            }
	            // no intrinsic size --> use default size
	            int textureWidth = iconBitmapSize;
	            int textureHeight = iconBitmapSize;
	            final Bitmap bitmap = Bitmap.createBitmap(textureWidth, textureHeight,
	                    Bitmap.Config.ARGB_8888);
	            final Canvas canvas = sCanvas;
	            canvas.setBitmap(bitmap);
	            final int left = (textureWidth-width) / 2;
	            final int top = (textureHeight-height) / 2;
	            sOldBounds.set(icon.getBounds());
	            icon.setBounds(left, top, left+width, top+height);
	            icon.draw(canvas);
	            icon.setBounds(sOldBounds);
	            canvas.setBitmap(null);
	            return bitmap;
		}
	}

	public static Bitmap customizeIcon(Bitmap source, Drawable bg_scale ,Drawable background,Drawable shadow, Drawable mask) {
		Bitmap sourceBmp = source;
		if(sourceBmp==null){
			return null;
		}
		if(true){
			Log.i("CustomizeIcon","1:sourceBmp :("+sourceBmp.getWidth()+" , "+sourceBmp.getHeight()+")");
		}
		Bitmap bg_scaleBmp = drawable2bitmap(background);
		//first scale the source
		if(bg_scaleBmp!=null){
			//sourceBmp = composite(sourceBmp,bg_scaleBmp);
		}
		
		//add mask to source
		Bitmap maskBmp = drawable2bitmap(mask);
		if(mask!=null){
			//sourceBmp=maskBitmap(sourceBmp,maskBmp);
		}
		
		//add shadow to source
		Bitmap shadowBmp = drawable2bitmap(shadow);
		if(shadowBmp!=null){
			//sourceBmp = composite(shadowBmp,sourceBmp);
		}
		
		//draw it to the final background
		Bitmap backgroundBmp = drawable2bitmap(background);
		if(backgroundBmp!=null){
			sourceBmp = composite(sourceBmp,backgroundBmp);
		}
		
		if(true){
			int witdh = 0;
			int height=0;
			if(sourceBmp!=null){
				witdh = sourceBmp.getWidth();
				height = sourceBmp.getHeight();
			}
			Log.i("CustomizeIcon","2:sourceBmp:("+witdh+" , "+height+")");
			Log.i("CustomizeIcon","-------------------------------------------------------------------");
		}
		
		return sourceBmp;
	}
	public static Bitmap customizeIcon(Drawable source,Drawable bg_scale,Drawable background,Drawable shadow, Drawable mask) {
		return customizeIcon(drawable2bitmap(source), bg_scale ,background, shadow, mask);
	}
	
	public static Bitmap maskBitmap(Bitmap source, Bitmap mask) {
		Bitmap src = source;
		Bitmap result = mask;
		// 创建画板
		Canvas canvas = new Canvas(mask);
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		canvas.save();
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.translate((mask.getWidth() - src.getWidth()) / 2,
				(mask.getHeight() - src.getHeight()) / 2);
		canvas.drawBitmap(source, 0.0f, 0.0f, paint);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(Paint.DITHER_FLAG,
                Paint.FILTER_BITMAP_FLAG));
		canvas.restore();
		paint.setXfermode(null);
		canvas.setBitmap(null);
		return result;
	}
	
	public static Bitmap  composite(Bitmap source, Bitmap bg) {
		Bitmap b1 = bg;
		Bitmap b2 = source;
		//zoom(b2,b1);
		if (!b1.isMutable()) {
			// 设置图片为背景为透明
			b1 = b1.copy(Bitmap.Config.ARGB_8888, true);
		}
		Log.i("zoom","b2:("+b2.getWidth()+"  ,  "+b2.getHeight()+")");
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		Canvas canvas = new Canvas(b1);
		int b1w = b1.getWidth();
		int b1h = b1.getHeight();
		int b2w = b2.getWidth();
		int b2h = b2.getHeight();
		int bx = (b1w - b2w) / 2;
		int by = (b1h - b2h) / 2;
		canvas.drawBitmap(b2, bx, by, paint);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(Paint.DITHER_FLAG,
                Paint.FILTER_BITMAP_FLAG));
		// 叠加新图b2 并且居中
		canvas.save(Canvas.ALL_SAVE_FLAG);
		canvas.restore();
		canvas.setBitmap(null);
		return b1;
	}
	
	public static Bitmap zoom(Bitmap bmpBg,Bitmap bg){
		//if(bg.getWidth()<=bmpBg.getWidth()){
		//	return bmpBg;
	    //}
		//Bitmap bmp = Bitmap.createBitmap(bg.getWidth(),
		//		bg.getHeight(), Config.ARGB_8888);
		//Canvas canvas = new Canvas(bmp);
		/*float scaleW = 1.0f;
		float scaleH = 1.0f;
		if(bg.getWidth()!=0&&bg.getHeight()!=0){
			scaleW=(float)(((double)bg.getWidth())/bmpBg.getWidth());
			scaleH=(float)(((double)bg.getHeight())/bmpBg.getHeight());
		}
		//Log.i("zoom","1:scale = "+scale+" ,  bmpBg:("+bmpBg.getWidth()+" , "+bmpBg.getHeight()+")");
		//matrix.postScale(scale, 
		//		scale, 
		//		bmpBg.getWidth() / 2, bmpBg.getHeight() / 2);
		//matrix.postTranslate( (canvas.getWidth()-bmpBg.getWidth()) / 2, (canvas.getHeight()-bmpBg.getHeight()) / 2);
		Matrix matrix = new Matrix();
		matrix.postScale(scaleW,scaleH); 
		Log.i("zoom","22222222222222222:bmpBg ;  ("+bg.getWidth()+" , "+bg.getHeight()+")  scale = ( "+scaleW+" ,  "+scaleH+" )");
		Bitmap bm =Bitmap.createBitmap(bmpBg, 0, 0, bmpBg.getWidth(), bmpBg.getHeight(), matrix,
				true);
		//canvas.drawBitmap(bmpBg, matrix, new Paint()); 
		Log.i("zoom","22222222222222222:bm ;  ("+bm.getWidth()+" , "+bm.getHeight()+")");*/
		if(bg.getWidth()<=bmpBg.getWidth()){
			return bmpBg;
		}
		float scale = 1.0f;
		if(bg.getWidth()!=0&&bg.getHeight()!=0){
			scale=(float)(((float)bg.getHeight())/Math.max(bmpBg.getWidth(), bmpBg.getHeight()));
		}
		return zoom(bmpBg,scale);
	}
	
	public static Bitmap zoom(Bitmap bmpBg,float scale){
		Matrix matrix = new Matrix();
		matrix.postScale(scale,scale); 
		Bitmap bm =Bitmap.createBitmap(bmpBg, 0, 0, bmpBg.getWidth(), bmpBg.getHeight(), matrix,
				true);
		return bm;
	}
	
	public static Bitmap drawable2bitmap(Drawable dw) {
		//TODO:???? is best?
		if(dw==null)return null;
		// 创建新的位图
		Bitmap bg = Bitmap.createBitmap(dw.getIntrinsicWidth(),
				dw.getIntrinsicHeight(), Config.ARGB_8888);
		// 创建位图画板
		Canvas canvas = new Canvas(bg);
		// 绘制图形
		dw.setBounds(0, 0, dw.getIntrinsicWidth(), dw.getIntrinsicHeight());
		dw.draw(canvas);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(Paint.DITHER_FLAG,
                Paint.FILTER_BITMAP_FLAG));
		// 释放资源
		canvas.setBitmap(null);
		return bg;
	}

	public  static int getIconBitmapSize() {
		return sIconDefaultSize;
	}
	/*
	 * Normalize Icon to google android icon std.
	 */
	public static Drawable normalizeIcon(Drawable d,Context context) {
		//TODO:xiejun
		Bitmap bmp = drawable2bitmap(d);//((BitmapDrawable) d).getBitmap();
		if (null == bmp||bmp.getWidth()==0||bmp.getHeight()==0)
			return null;
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		float DEST_SIZE = 48f; // TODO magic number is good enough for now, should go with display info
		Bitmap scaleBmp = null;
        //if (width > DEST_SIZE || height > DEST_SIZE) {
            // Resize bitmap
		//scale the bitmap to ^
		float scale = DEST_SIZE / Math.max(width, height);
		int destWidth = (int) (width * scale);
		int destHeight = (int) (height * scale);
		scaleBmp = Bitmap.createScaledBitmap(bmp, destWidth, destHeight, false);
		width = scaleBmp.getWidth();
		height = scaleBmp.getHeight();
        //}
		Drawable finalDrawable =null;
		int l = -1;
		int r = -1;
		int t = -1;
		int b = -1;
		int i, j;
		int s=0;
		for (i = 0; i < height; ++i) {
			for (j = 0; j < width; ++j) {
				if ((scaleBmp.getPixel(j, i) >>> 24) > 10) {
					if (l < 0 || l > j)
						l = j;
					if (r < 0 || r < j)
						r = j;
					if (t < 0 || t > i)
						t = i;
					if (b < 0 || b < i)
						b = i;
					++s;
				}
			}
		}
		if(scaleBmp!=null&&!scaleBmp.isRecycled()){
			scaleBmp.recycle();
			scaleBmp=null;
		}
		
		float w = r - l;
		float h = b - t;

		int[] bounds = calcBounds(r,l,t,b,width,height);
		// left top right bottom
		Drawable preClipedDrawable = null;
		Log.i("realScale","bounds ( "+bounds[0]+" , "+bounds[1]+" , "+bounds[2]+"  ,  "+bounds[3]+" )"+"  scale = "+scale);
		if(!(bounds[0]==0&&bounds[1]==0&&bounds[2]==0&&bounds[3]==0)){
			preClipedDrawable = clipDrawable(new BitmapDrawable(context.getResources(), bmp), 
					context.getResources(), (int)(bounds[2]/scale), (int)(bounds[0]/scale), (int)(bounds[3]/scale),(int)(bounds[1]/scale));
			bmp = drawable2bitmap(preClipedDrawable);
		}
		
		float recSize = getDimen(AbstractIconGetter.RES_PKG, IconConst.VISIBLE_REC_ICONSIZE, context,156);
		float circleSize = getDimen(AbstractIconGetter.RES_PKG, IconConst.VISIBLE_CIRCLE_ICONSIZE, context,176);
		float finalSize = getDimen(AbstractIconGetter.RES_PKG, IconConst.FINALICONSZIE, context,192);
		Log.d("realScale"," recSize = "+recSize+"  ,  circleSize = "+circleSize+"  , finalSize = "+finalSize);
		Log.d("realScale","s = "+s+" ,  "+0.91f * w * h+" ,  "+w+"  ,  "+h);
		if (s > 0.91f * w * h&&w > 0.97f * h && h>0.97*w) {
			Log.d("realScale","  deal rectangle ");
			scale = ((recSize))/(Math.max(w, h)*(finalSize/DEST_SIZE));
		}else{
			Log.d("realScale"," deal circle ");
			scale = ((circleSize))/(Math.max(w, h)*(finalSize/DEST_SIZE));
		}
		float realeScale = scale*finalSize/Math.max(bmp.getWidth(), bmp.getHeight());
		Log.d("realScale","1:realeScale = "+realeScale+" bmp : ("+bmp.getWidth()+" ,  "+bmp.getHeight()+" )"
				+" (recSize,circleSize,finalSize) =( "+recSize+" , "+circleSize+" , "+finalSize+" )");
		if(realeScale!=1.0f){
			bmp = zoom(bmp, realeScale);
		}
		Log.d("realScale","2:realeScale = "+realeScale+" bmp : ("+bmp.getWidth()+" ,  "+bmp.getHeight()+" )");
		if(bmp.getWidth()>finalSize||bmp.getHeight()>finalSize){
			int left = (bmp.getWidth()-(int)finalSize)/2;
			int top = (bmp.getHeight()-(int)finalSize)/2;
			float extraRight = bmp.getWidth()>finalSize?finalSize: bmp.getWidth();
			float extraBottom = bmp.getHeight()>finalSize?finalSize: bmp.getHeight();
			int right = left+(int)extraRight;
			int bottom = top+(int)extraBottom;
			finalDrawable = clipDrawable(new BitmapDrawable(context.getResources(), bmp), context.getResources(), right, left, bottom, top);
		}
		return finalDrawable==null?new BitmapDrawable(context.getResources(),bmp):finalDrawable;
	}
	
	private static int[] calcBounds(int r,int l,int t,int b,int bmpW,int bmpH){
		Log.i("realScale","r = "+r+" l = "+l+"  t  =  "+t+"  b  = "+b+"  bmpW = "+bmpW+"  bmpH = "+bmpH);
		int rr = bmpW - r;
		int bb = bmpH - b;
		int[]  rec = new int[]{// left top right bottom
			0,0,0,0
		};
		 
		boolean exactW = Math.abs(l-rr)>5?true:false;
		boolean exactH = Math.abs(l-rr)>5?true:false;
		if(exactW&&exactH){
			if(l<rr&&t<bb){
				rec[0]=0;
				rec[1]=0;
				rec[2]=r+l;
				rec[3]=b+t;
			}else if(l<rr&&t>bb){
				rec[0]=0;
				rec[1]=t-bb;
				rec[2]=(r+l);
				rec[3]=(bmpH-1);
			}else if(l>rr&&t<bb){
				rec[0]=(int)(l-rr);
				rec[1]=0;
				rec[2]=(bmpW-1);
				rec[3]=(b+t);
			}else if(l>rr&&t>bb){
				rec[0]=(l-rr);;
				rec[1]=(t-bb);
				rec[2]=(bmpW);
				rec[3]=(bmpH);
			}
		}
		
		return rec;
	}
	
	private static Drawable clipDrawable(Drawable drawable,Resources res,int r,int l,int b,int t){
		//TODO:Here will be make some misstakes.
		Bitmap bg = Bitmap.createBitmap(Math.abs(r -l)+1,
				Math.abs(t - b)+1, Config.ARGB_8888);
		Canvas canvas = new Canvas(bg);
		canvas.save();
		Bitmap oldBitmap = drawable2bitmap(drawable);
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		canvas.drawBitmap(oldBitmap, -l, -t,paint);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(Paint.DITHER_FLAG,
                Paint.FILTER_BITMAP_FLAG));
		canvas.restore();
		return new BitmapDrawable(res,bg);
	}

}

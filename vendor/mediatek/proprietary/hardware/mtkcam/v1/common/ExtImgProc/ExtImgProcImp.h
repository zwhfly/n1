#ifndef EXT_IMG_PROC_IMP_H
#define EXT_IMG_PROC_IMP_H
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else
#include <GNCameraFeature.h>
using namespace android;
#endif
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 end
//-----------------------------------------------------------------------------
class ExtImgProcImp : public ExtImgProc
{
    protected:
        ExtImgProcImp();
        virtual ~ExtImgProcImp();
    //
    public:
        static ExtImgProc*  getInstance(void);
        virtual void        destroyInstance(void);
        //
        virtual MBOOL       init(void);
        virtual MBOOL       uninit(void);
        virtual MUINT32     getImgMask(void);
        virtual MBOOL       doImgProc(ImgInfo& img);
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else 
		virtual void        setGNCameraFeatureInstance(GNCameraFeature* mpGN);		
#endif
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 end
   //
   private:
        mutable Mutex   mLock;
        volatile MINT32 mUser;
        MUINT32         mImgMask;
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 begin
#if ORIGINAL_VERSION
#else		
		GNCameraFeature* mpGNCameraFeature;
#endif
// Gionee <zhangpj> <2014-08-21> modify for CR01359934 end
};
//-----------------------------------------------------------------------------
#endif


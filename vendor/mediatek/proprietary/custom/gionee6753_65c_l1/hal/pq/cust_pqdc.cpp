extern "C" {

// --------------------------------------------------------------------------
//  Behavior configuration
// --------------------------------------------------------------------------
//Gionee BSP1 yangqb 20150422 modify for CR01468997 start
#ifdef CONFIG_GN_BSP_MTK_GAMMA_FUNCTION_SUPPORT
	int MiddleRegionGain = 160;
	int BlackRegionGain  = 100;
	int BlackRegionRange = 1;
	int BlackEffectLevel = 128;
	int WhiteRegionGain  = 100;
	int WhiteRegionRange = 1;
	int WhiteEffectLevel = 128;
#endif
//Gionee BSP1 yangqb 20150422 modify for CR01468997 end
}
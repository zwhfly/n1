# =============================================================================
#
# Module: libMcTeeKeymaster.so - Client library handling key operations
# with TEE Keymaster trustlet
#
# =============================================================================

LOCAL_PATH    := $(call my-dir)

ifeq ($(MTK_K64_SUPPORT), yes)
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../mobicore/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../mobicore/common/MobiCore/inc
else
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../mobicore/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../mobicore/common/MobiCore/inc
endif

include $(CLEAR_VARS)

LOCAL_MODULE    := libMcTeeKeymaster
LOCAL_MODULE_TAGS := debug eng optional

# Add new source files here
LOCAL_SRC_FILES +=\
    tlcTeeKeymaster_if.c

LOCAL_C_INCLUDES +=\
    $(LOCAL_PATH)/public \
    $(LOCAL_PATH)/inc \
    $(MOBICORE_LIB_PATH)

# Need the MobiCore client library
LOCAL_SHARED_LIBRARIES := libMcClient

LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_SHARED_LIBRARY)


# =============================================================================
ifeq ($(strip $(TARGET_BUILD_VARIANT)), eng)

include $(CLEAR_VARS)

# Module name (sets name of output binary / library)
LOCAL_MODULE	:= testTeeKeymaster
LOCAL_MODULE_TAGS := debug eng optional

# Add your source files here (relative paths)
LOCAL_SRC_FILES +=\
    testTeeKeymaster.c

LOCAL_C_INCLUDES +=\
    $(LOCAL_PATH)/public \
    $(LOCAL_PATH)/inc \
    $(MOBICORE_LIB_PATH) \
    external/openssl/include \

LOCAL_SHARED_LIBRARIES := \
    libMcTeeKeymaster \
    libMcClient \
    libcrypto \
    liblog \
    libssl

include $(BUILD_EXECUTABLE)

endif

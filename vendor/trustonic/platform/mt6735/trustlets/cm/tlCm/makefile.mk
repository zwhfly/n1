################################################################################
#
# Trustonic Content Management TA
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := tlCm

#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------

TRUSTLET_UUID := 07010000000000000000000000000000
TRUSTLET_SERVICE_TYPE := 3
TRUSTLET_KEYFILE := pairVendorTltSig.pem
TRUSTLET_KPHFILE := MobiConfig_GuD_KPH_public_key.pem
TRUSTLET_KID := 1

#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)

#-------------------------------------------------------------------------------
# standalone build
OUTPUT_ROOT := $(TEE_TRUSTLET_OUTPUT_PATH)/$(OUTPUT_NAME)
OUTPUT_PATH := $(OUTPUT_ROOT)/$(TEE_MODE)
TA_UUID ?= $(TRUSTLET_UUID)
TA_UUID := $(shell echo $(TA_UUID) | tr A-Z a-z)
TA_SERVICE_TYPE ?= $(TRUSTLET_SERVICE_TYPE)
TA_KEYFILE ?= $(TRUSTLET_KEYFILE)
TA_KPHFILE ?= $(TRUSTLET_KPHFILE)
TASDK_DIR_SRC ?= $(TLSDK_DIR_SRC)
TA_BIN := $(OUTPUT_PATH)/$(TA_UUID).tlbin
TA_AXF := $(OUTPUT_PATH)/$(OUTPUT_NAME).axf
TA_AXF_CONF := $(OUTPUT_PATH)/$(OUTPUT_NAME).axf.conf
TA_LST2 := $(OUTPUT_PATH)/$(OUTPUT_NAME).lst2
TA_KID := $(TRUSTLET_KID)
CROSS=arm-none-eabi
READELF=$(CROSS_GCC_PATH_BIN)/$(CROSS)-readelf
READ_OPT=-a $(TA_AXF) > $(TA_LST2)
TA_INJECT_KEY_PARAM := -c \
            -i $(TA_AXF) \
            -o $(TA_AXF_CONF) \
            -k $(TA_KPHFILE) \
            --kid $(TA_KID)
						
TA_PARAM := -b $(TA_AXF_CONF) \
            -servicetype $(TA_SERVICE_TYPE) \
            -output $(TA_BIN) \
            -k $(TA_KEYFILE)            

all : $(TA_BIN)

$(TA_BIN) : $(TA_AXF)
	$(info ******************************************)
	$(info ** READELF & MobiConvert *****************)
	$(info ******************************************)
	$(READELF) $(READ_OPT)
	$(JAVA_HOME)/bin/java -jar $(MOBICORE_TOOL_DIR)/Bin/MobiConfig.jar $(TA_INJECT_KEY_PARAM) >$(TA_BIN).inject.log
	$(JAVA_HOME)/bin/java -jar $(TASDK_DIR_SRC)/Bin/MobiConvert/MobiConvert.jar $(TA_PARAM) >>$(TA_BIN).log

#ifndef _H_DDP_LOG_
#define _H_DDP_LOG_

#ifndef LOG_TAG
#define LOG_TAG
#endif

#include "drStd.h"
#include "DrApi/DrApi.h"

#define DISP_LOG_RAW(fmt, args...) drApiLogPrintf(fmt, ##args)

#define DISP_LOG_D( fmt, args...)   drDbgPrintLnf("[TDDP/"LOG_TAG"]"fmt, ##args)
#define DISP_LOG_I( fmt, args...)   drApiLogPrintf("[TDDP/"LOG_TAG"]"fmt, ##args)
#define DISP_LOG_W( fmt, args...)   drApiLogPrintf("[TDDP/"LOG_TAG"]"fmt, ##args)
#define DISP_LOG_E( fmt, args...)   drApiLogPrintf("[TDDP/"LOG_TAG"]error:"fmt, ##args);

extern void ddp_debug_set_log_level(int level);
extern int ddp_debug_get_log_level();

#define DISP_LOG_V(fmt, args...)               \
	do {                                       \
        if(ddp_debug_get_log_level()>=3)       \
		    DISP_LOG_I(fmt,##args);            \
	}while (0) 

#define DDPIRQ(fmt, args...)                   \
	do {                                       \
        if(ddp_debug_get_log_level()>=2)          \
		    DISP_LOG_I(fmt,##args);            \
	}while (0)

#define DDPDBG(fmt, args...)                   \
	do {                                       \
        if(ddp_debug_get_log_level()>=1)       \
		    DISP_LOG_I(fmt,##args);            \
	}while (0)

#define DDPDEBUG_D(fmt, args...)               \
    do {                                       \
        DISP_LOG_D(fmt,##args);                \
    }while (0)

#define DDPMSG(fmt, args...)                   \
	do {                                       \
		DISP_LOG_I(fmt,##args);                \
	}while (0)

#define DDPERR(fmt, args...)                   \
    do {                                       \
        DISP_LOG_E(fmt,##args);                \
    }while (0)

#define DDPDUMP(fmt, args...)                        \
    do {                                       \
        DISP_LOG_I(fmt,##args);                \
    }while (0)

#ifndef ASSERT
#define ASSERT(expr)                             \
    do {                                         \
        if(expr) break;                          \
		while(1) {								\
			DISP_LOG_E("DDP ASSERT FAILED %s, %d\n", __FILE__, __LINE__);\
		}\
    }while(0)
#endif

#define DDPAEE(fmt, args...)                        \
    do {\
		DISP_LOG_E(fmt, ##args);\
		DISP_LOG_E("AEE %s, %d\n", __FILE__, __LINE__);\
    }while(1)


#define DISPFUNC() DISP_LOG_I("[DISP]func|%s\n", __func__)  //default on, err msg


	
#endif

#define LOG_TAG "ddp_drv"

#include "ddp_drv.h"
#include "ddp_reg.h"
#include "ddp_hal.h"
#include "ddp_log.h"


static int nr_dispsys_dev = 0;
unsigned int dispsys_irq[DISP_REG_NUM] = {0};
unsigned long dispsys_reg[DISP_REG_NUM] = {0};

extern int ddp_path_init();

int disp_reg_init()
{
	int ret;
	int i;
    static unsigned int disp_probe_cnt = 0;

    if(disp_probe_cnt!=0)
    {
		disp_probe_cnt = 1;
        return 0;
    }

    /* iomap registers and irq*/
    for(i=0;i<DISP_REG_NUM;i++)
    {
		drApiResult_t ret;
		/* remap registers */

		ret = drApiMapPhysicalBuffer(ddp_reg_pa_base[i], 0x1000, 
				MAP_HARDWARE, &dispsys_reg[i]);

		if(ret != DRAPI_OK)
			DDPAEE("map reg fail: pa=0x%x, size=0x%x, flag=0x%x, ret=%d(0x%x)\n",
				ddp_reg_pa_base[i], 0x1000, MAP_HARDWARE, ret, ret);
		
        DDPMSG("reg_map%d module=%s, map_addr=%p, map_irq=%d, reg_pa=0x%x\n", 
            i, ddp_get_reg_module_name(i), dispsys_reg[i], dispsys_irq[i], 
            ddp_reg_pa_base[i]);
    }    

	ddp_path_init();
	ddp_dsi_reg_init();
	DDPMSG("dispsys probe done.\n");	
	return 0;
}




#define LOG_TAG "debug"

static int ddp_debug_log_level = 2;

void ddp_debug_set_log_level(int level)
{
	ddp_debug_log_level = level;
}

int ddp_debug_get_log_level()
{
	return ddp_debug_log_level;
}




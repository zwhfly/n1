#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}

DRV_NAME=drtplay

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Debug
  OPTIM=debug
fi

source ${t_sdk_root}/setup.sh

cd $(dirname $(readlink -f $0))

if [ ! -d "${OPTIM}" ]; then
  exit 1
fi

#rm -rf ${OUT_DIR}/
mkdir -p ${OUT_DIR}/
cp -f ${OPTIM}/drtplay.lib  ${OUT_DIR}/
cp -f ${OPTIM}/drtplay.axf  ${OUT_DIR}/

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export DRSDK_DIR_SRC=${COMP_PATH_DrSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
export DRSDK_DIR=${COMP_PATH_DrSdk}
echo "Running make..."	
make -f makefile.mk "$@"

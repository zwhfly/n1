/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
 
#ifndef __TL_M_TPLAY_API_H__
#define __TL_M_TPLAY_API_H__

#include "TlApi/TlApiCommon.h"
#include "TlApi/TlApiError.h"

/* Marshaled function parameters.
 * structs and union of marshaling parameters via TlApi.
 *
 * @note The structs can NEVER be packed !
 * @note The structs can NOT used via sizeof(..) !
 */
 
 /*
  * Function id definitions
  */
#define FID_DR_TPLAY_OPEN_SESSION           1   /* T-play API */
#define FID_DR_TPLAY_CLOSE_SESSION          2   /* T-play API */
#define FID_DR_TPLAY_PROCESS_DRM_CONTENT    3   /* T-play API */
#define FID_DR_TPLAY_CHECK_LINK             4   /* T-play API */
#define FID_DR_TPLAY_DEBUG_CONTENT          5   /* T-play DEBUG API */
#define FID_DRV_HACC_CTR_INIT               12  /* for DRM & HDCP */
#define FID_DEV_HACC_CTR_FINISH             13  /* for DRM & HDCP */
#define FID_DRV_HACC_CTR_DECRYPT            14  /* for DRM */
#define FID_DRV_HACC_CTR_ENCRYPT            15  /* for HDCP */
#define FID_DRV_TPLAY_SET_HANDLE_ADDR       17  /* for NWD to set t-play handle addr */
#define FID_DRV_HACC_COPY_DATA              18  /* for HDCP */
#define FID_DRV_TPLAY_DUMP_PHYSICAL_ADDR    19  /* for NWD to dump physcial address from handle */
#define FID_DR_TPLAY_SET_LINK               20
#define FID_DR_TPLAY_CLEAR_LINK             21
#define FID_DR_TPLAY_TEST_LINK              22
#define FID_DRV_GCPU_CBC_INIT               23 
#define FID_DEV_GCPU_CBC_FINISH             24 
#define FID_DRV_GCPU_CBC_DECRYPT            25 
#define FID_DRV_GCPU_CBC_ENCRYPT            26 


typedef enum {
    MEM_TA_VIRTUAL = 1,
    MEM_PHYSICAL = 2,
    MEM_NULL = 3,
    MEM_TPLAY_MAPPING = 4,
    MEM_HANDLE_PHY_MAPPING = 5,
    MEM_RESERVED = 0x7FFFFFFF  /* force enum to use 32 bits */
} EX_MEM_t;

/* .. add more when needed */

/* Marshaled function parameters.
 * structs and union of marshaling parameters via TlApi.
 *
 * @note The structs can NEVER be packed !
 * @note The structs can NOT used via sizeof(..) !
 */

typedef struct {
    uint32_t    commandId;
	uint32_t    index;
    uint32_t    result;
    uint32_t    module_index;
    uint32_t    domain_num;
    uint32_t    permission_control;
    
} tlApiDapc_t, *tlApiDapc_ptr;


/** Encode cleartext with rot13.
 *
 * @param encodeData Pointer to the tlApiEncodeRot13_t structure 
 * @return TLAPI_OK if data has successfully been encrypted.
 */

//_TLAPI_EXTERN_C tlApiResult_t tlApiAddDapc(tlApiDapc_ptr DapcData);

#endif // __TLAPIDAPC_H__


#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}
export DRUTILS_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/drutils
export DRUTILS_DIR=${COMP_PATH_ROOT}/../../../Drutils/Out

TL_NAME=TlKeyInstall

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Debug
  OPTIM=debug
fi

export DRUTILS_LIB_PATH=${DRUTILS_OUT_DIR}/${TEE_MODE}/drutils.lib
# For customer release - mode check
if [ "$TEE_MODE" == "Debug" ] && [ ! -f ${DRUTILS_OUT_DIR}/${TEE_MODE}/drutils.lib ] ; then
    echo "${DRUTILS_OUT_DIR}/${TEE_MODE}/drutils.lib is not existed in 'Debug' mode, changing to 'Release' mode"
    DRUTILS_LIB_PATH=${DRUTILS_OUT_DIR}/Release/drutils.lib
elif [ "$TEE_MODE" == "Release" ] && [ ! -f ${DRUTILS_OUT_DIR}/${TEE_MODE}/drutils.lib ] ; then
    echo "${DRUTILS_OUT_DIR}/${TEE_MODE}/drutils.lib is not existed in 'Release' mode, chaning to 'Debug' mode"
    DRUTILS_LIB_PATH=${DRUTILS_OUT_DIR}/Debug/drutils.lib
fi

export TLKEYINSTALL_LIB_PATH=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/${TEE_MODE}/${TL_NAME}.lib
# For customer release - mode check
if [ "$TEE_MODE" == "Debug" ] && [ ! -f ${TLKEYINSTALL_LIB_PATH} ] ; then
    echo "${TLKEYINSTALL_LIB_PATH} is not existed in 'Debug' mode, changing to 'Release' mode"
    TLKEYINSTALL_LIB_PATH=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Release/${TL_NAME}.lib
elif [ "$TEE_MODE" == "Release" ] && [ ! -f ${TLKEYINSTALL_LIB_PATH} ] ; then
    echo "${TLKEYINSTALL_LIB_PATH} is not existed in 'Release' mode, chaning to 'Debug' mode"
    TLKEYINSTALL_LIB_PATH=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Debug/${TL_NAME}.lib
fi

export DRKEYINSTALL_LIB_PATH=${TEE_DRIVER_OUTPUT_PATH}/DrKeyInstall/${TEE_MODE}/DrKeyInstall.lib
# For customer release - mode check
if [ "$TEE_MODE" == "Debug" ] && [ ! -f ${DRKEYINSTALL_LIB_PATH} ] ; then
    echo "${DRKEYINSTALL_LIB_PATH} is not existed in 'Debug' mode, changing to 'Release' mode"
    DRKEYINSTALL_LIB_PATH=${TEE_DRIVER_OUTPUT_PATH}/DrKeyInstall/Release/DrKeyInstall.lib
elif [ "$TEE_MODE" == "Release" ] && [ ! -f ${DRKEYINSTALL_LIB_PATH} ] ; then
    echo "${DRKEYINSTALL_LIB_PATH} is not existed in 'Release' mode, chaning to 'Debug' mode"
    DRKEYINSTALL_LIB_PATH=${TEE_DRIVER_OUTPUT_PATH}/DrKeyInstall/Debug/DrKeyInstall.lib
fi

# For customer release - file check
if [ ! -f ${DRUTILS_LIB_PATH} ] ; then
    echo "[ERROR] '${DRUTILS_LIB_PATH}' is still not existed! Please check it!"
    exit 0
fi

if [ ! -f ${TLKEYINSTALL_LIB_PATH} ] ; then
    echo "[ERROR] '${TLKEYINSTALL_LIB_PATH}' is still not existed! Please check it!"
    exit 0
fi

if [ ! -f ${DRKEYINSTALL_LIB_PATH} ] ; then
    echo "[ERROR] '${DRKEYINSTALL_LIB_PATH}' is still not existed! Please check it!"
    exit 0
fi
source ${t_sdk_root}/setup.sh

cd $(dirname $(readlink -f $0))

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
echo "Running make..."	
make -f makefile.mk "$@"
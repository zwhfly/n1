#!/bin/bash
#
# This file sets the path variables and resolves the dependencies for the
# different Makefiles included in the release.
#
# Set the paths to your ARM DS-5 and Android NDK toolchains here
#
################### EDIT HERE ####################
# Edit these paths according to your environment #

CUR_DIR=`pwd`

export COMP_PATH_AndroidNdk=/mtkoss/android-ndk/r10c/x86_64/android-ndk-r10c
#export COMP_PATH_AndroidNdk=/mtkoss/android-ndk/r9d/x86_64
export ARM_RVCT_PATH=                                               # ARM DS-5      - e.g.: /usr/local/DS-5
export LM_LICENSE_FILE=                                             # DS-5 license  - e.g.: /home/user/DS5PE-*.dat

# GNU Toolchain from the following website:
# https://launchpad.net/gcc-arm-embedded/4.7/4.7-2012-q4-major

export CROSS_GCC_PATH=${MKTOPDIR}/prebuilts/gcc/linux-x86/arm/gcc-arm-none-eabi-4_8-2014q3
#export CROSS_GCC_PATH=${MKTOPDIR}/prebuilts/gcc/linux-x86/arm/gcc-arm-none-eabi-4_7-2012q4
#export CROSS_GCC_PATH=${MKTOPDIR}/prebuilts/gcc/linux-x86/aarch64/linaro-aarch64-linux-gnu-4.8
# Linaro GCC 4.7.3 - e.g.: /opt/gcc-arm-none-eabi-4_7-2012q4

#export JAVA_HOME

######################################################
#### fixed part - do not change the following lines! #

export ARM_RVCT_PATH_INC=${ARM_RVCT_PATH}/include   # sub path DS-5 includes
export ARM_RVCT_PATH_LIB=${ARM_RVCT_PATH}/lib       # sub path DS-5 library
export ARM_RVCT_PATH_BIN=${ARM_RVCT_PATH}/bin       # sub path DS-5 compiler binary

export CROSS_GCC_PATH_INC=${CROSS_GCC_PATH}/arm-none-eabi/include
export CROSS_GCC_PATH_LIB=${CROSS_GCC_PATH}/arm-none-eabi/lib
export CROSS_GCC_PATH_LGCC=${CROSS_GCC_PATH}/lib/gcc/arm-none-eabi/4.8.4
export CROSS_GCC_PATH_BIN=${CROSS_GCC_PATH}/bin
#export CROSS_GCC_PATH_INC=${CROSS_GCC_PATH}/aarch64-linux-gnu/include
#export CROSS_GCC_PATH_LIB=${CROSS_GCC_PATH}/lib
#export CROSS_GCC_PATH_LIB_LOCAL=${CROSS_GCC_PATH}/lib/gcc/aarch64-linux-gnu/4.8.3/libgcc.a
#export CROSS_GCC_PATH_BIN=${CROSS_GCC_PATH}/bin

export NDK_BUILD=${COMP_PATH_AndroidNdk}/ndk-build

SDK_ROOT=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustzone/t-sdk
MOBICORE_TOOL_ROOT=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustzone/tools

export COMP_PATH_Logwrapper=${SDK_ROOT}/TlcSdk/Out/Logwrapper
export COMP_PATH_MobiCore=${SDK_ROOT}/TlSdk/Out/Public/MobiCore
export COMP_PATH_MobiCoreDriverLib=${SDK_ROOT}/TlcSdk/Out
export COMP_PATH_TlSdk=${SDK_ROOT}/TlSdk/Out
export COMP_PATH_DrSdk=${SDK_ROOT}/DrSdk/Out
export COMP_PATH_MOBICONFIG=${MOBICORE_TOOL_ROOT}/MobiConfig

############## Below Path are for internal reference only ########################################
export COMP_PATH_Drmem=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/utils/Drutils/Out
export COMP_PATH_Tlmem=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/utils/Tlutils/Out
export COMP_PATH_Drsec=${MKTOPDIR}/vendor/mediatek/proprietary/protect-private/security/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/sec/Drsec/Out
export COMP_PATH_Tlsec=${MKTOPDIR}/vendor/mediatek/proprietary/protect-private/security/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/sec/Tlsec/Out

# m4u
export COMP_PATH_M4U_ROOT=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/m4u
export COMP_PATH_M4U_CROSS_INC=${MKTOPDIR}/kernel-3.10/drivers/misc/mediatek/mach/${ARCH_MTK_PLATFORM}/include/trustzone/m4u

# tplay
export COMP_PATH_Drtplay=${MKTOPDIR}/vendor/mediatek/proprietary/protect-private/security/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/tplay/Drtplay/Out
export COMP_PATH_Tltplay=${MKTOPDIR}/vendor/mediatek/proprietary/protect-private/security/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/tplay/Tltplay/Out

######### Below Path are for external reference only (customer release ) ##################################
export COMP_PATH_Tlcmem=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/export/util/sec_mem
export COMP_PATH_Drmem_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/utils/Drutils
# H.264 decoder
export COMP_PATH_SECVDECDR_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/MtkH264SecVdecDrv
export COMP_PATH_MtkH264SecVdecDRV=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/video/MtkH264Vdec/MtkH264SecVdecDrv/Out
export COMP_PATH_MtkH264SecVdec=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/video/MtkH264Vdec/MtkH264SecVdecTL/Out

# H.264 encoder
export COMP_PATH_MtkH264SecVencDRV=${MKTOPDIR}/mediatek/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/video/MtkH264Venc/MtkH264SecVencDrv/Out
export COMP_PATH_MtkH264SecVenc=${MKTOPDIR}/mediatek/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/video/MtkH264Venc/MtkH264SecVencTL/Out
export COMP_PATH_SECVENCDR_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/MtkH264SecVencDrv

export COMP_PATH_Tlmem_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/utils/Tlutils
export COMP_PATH_Drspi_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/spi/Drspi
export COMP_PATH_Tlspi_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/spi/Tlspi
export COMP_PATH_Drsec_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/sec/Drsec
export COMP_PATH_Tlsec_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/sec/Tlsec
# tplay
export COMP_PATH_Drtplay_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/tplay/Drtplay
export COMP_PATH_Tltplay_Export=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/tplay/Tltplay

#CMDQ
export COMP_PATH_CmdqSecDr=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/cmdq/cmdq_sec_dr/Out
export COMP_PATH_CmdqSecTl=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/cmdq/cmdq_sec_tl/Out
export COMP_PATH_CmdqSecDr_PUBLIC=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/cmdq/cmdq_sec_dr
export COMP_PATH_CmdqSecTl_PUBLIC=${MKTOPDIR}/vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets/cmdq/cmdq_sec_tl

#############################
#Widevine
export AOSP_MTK_SOURCE_PATH=${MKTOPDIR}/vendor/mediatek/proprietary

#Widevine Classis DRM
export COMP_PATH_TlWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/classic_drm/TlWidevineClassicDrm/Locals/Code
export COMP_PATH_TlWVDecrypt=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/classic_drm/TlWidevineClassicDrmDecrypt/Locals/Code
export COMP_PATH_DrWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/classic_drm/DrWidevineClassicDrm/Locals/Code

#Widevine Modular DRM
#export COMP_PATH_TlModularDRMWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/modular_drm/TlWidevineModularDrm/Out
export COMP_PATH_TlModularDRMWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/modular_drm/TlWidevineModularDrm/Locals/Code/public/
#export COMP_PATH_DrModularDRMWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/modular_drm/DrWidevineModularDrm/Out
export COMP_PATH_DrModularDRMWidevine=${AOSP_MTK_SOURCE_PATH}/protect-bsp/trustzone/trustonic/trustlets/widevine/modular_drm/DrWidevineModularDrm/Locals/Code/public/
#############################


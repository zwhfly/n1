################################################################################
#
# <t-sdk Sample <t-play Trusted Application
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := tlPlay


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------
TRUSTLET_UUID := 07120000000000000000000000000000
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1


ifeq ($(SP_SYS),SP)
TRUSTLET_SERVICE_TYPE := 2 # 2: SP trustlet
TRUSTLET_KEYFILE := Locals/Build/keySpTl.xml
else
TRUSTLET_SERVICE_TYPE := 3 # 3: system trustlet
TRUSTLET_KEYFILE := Locals/Build/pairVendorTltSig.pem
endif

TRUSTLET_FLAGS := 0 
TRUSTLET_INSTANCES := 16 # min: 1; max: 16
ifeq ($(TOOLCHAIN),GNU)
    TRUSTLET_OPTS += -Werror
endif
ifeq ($(TOOLCHAIN),ARM)
   TRUSTLET_OPTS += --diag_error=warning
endif

#-------------------------------------------------------------------------------

TBASE_API_LEVEL := 3

#-------------------------------------------------------------------------------
# Files and include paths - Add your files here
#-------------------------------------------------------------------------------

### A simple scatter file and set SCATTER_FILE.
### Uncomment this to use a custom scatter file $(OUTPUT_NAME).sct
# SCATTER_FILE := $(TRUSTLET_DIR)/$(OUTPUT_NAME).sct

### Add include path here
INCLUDE_DIRS += \
    Locals/Code/inc \
    Locals/Code/public \

### Add source code files for C++ compiler here
SRC_CPP += # nothing


### Add source code files for C compiler here
SRC_C += \
    Locals/Code/tlTciHandler.c \
    Locals/Code/tlMain.c



### Add source code files for assembler here
SRC_S += # nothing

CUSTOMER_DRIVER_LIBS += \

#-------------------------------------------------------------------------------
# use generic make file
#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)
include $(TLSDK_DIR)/trustlet.mk



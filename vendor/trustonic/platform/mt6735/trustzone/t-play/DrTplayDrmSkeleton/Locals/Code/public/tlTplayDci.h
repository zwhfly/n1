/*
 * Copyright (c) 2013-2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/**
 * @file   dci.h
 * @brief  Contains DCI (Driver Control
 * Interface) definitions and data structures
 *
 */

#ifndef __DCI_H__
#define __DCI_H__


typedef uint32_t dciCommandId_t;
typedef uint32_t dciResponseId_t;
typedef uint32_t dciReturnCode_t;

/**< Responses have bit 31 set */
#define RSP_ID_MASK (1U << 31)
#define RSP_ID(cmdId) (((uint32_t)(cmdId)) | RSP_ID_MASK)
#define IS_CMD(cmdId) ((((uint32_t)(cmdId)) & RSP_ID_MASK) == 0)
#define IS_RSP(cmdId) ((((uint32_t)(cmdId)) & RSP_ID_MASK) == RSP_ID_MASK)

/*
 * For implementation, further commands should be added here to
 * manage functionality such as decode and display, this is up to
 * the SoC to define and out of the scope of this skeleton code.
 *
 * But for example purposes :
 */

#define FID_DR_DECODE_INITIALIZE     0
#define FID_DR_DECODE_PROCESS        1
#define FID_DR_DISPLAY_CONTENT       2
#define FID_DR_MANAGE_HANDLE         3


/**
 * DCI command header.
 */
typedef struct{
    dciCommandId_t commandId; /**< Command ID */
} dciCommandHeader_t;

/**
 * DCI response header.
 */
typedef struct{
    dciResponseId_t     responseId; /**< Response ID (must be command ID | RSP_ID_MASK )*/
    dciReturnCode_t     returnCode; /**< Return code of command */
} dciResponseHeader_t;

/** DCI simple command data */
typedef struct {
    /**< Length of data to process */
    uint32_t            len;
    /**< data to process */
    uint64_t            data;
} dciSimpleData_t;

/** DCI manage handle command data */
typedef struct {
    /** physical address of output handle */
    uint64_t            handlePhys;
    /** size(bytes) of handle2phys table */
    uint32_t            handleToPhysTableSize;
    /** physical address of handle2phys table */
    uint64_t            handleToPhysTablePhys;
} dciManageHandleCommandData_t;

/**
 * command message.
 *
 * @param len Lenght of the data to process.
 * @param data Data to be processed
 */
typedef struct {
    dciCommandHeader_t  header;     /**< Command header */
    union {
        dciSimpleData_t                 simpleData;
        dciManageHandleCommandData_t    manageHandleData;
    };
} cmd_t;


/**
 * Response structure
 */
typedef struct {
    /**< Response header */
    dciResponseHeader_t header;
    /**< Response data */
    union {
        dciSimpleData_t simpleData;
    };
} rsp_t;

/**
 * DCI message data.
 */
typedef struct {
    union {
        cmd_t     command;
        rsp_t     response;
    };
} dciMessage_t;


/**
 * TPlay DRM Driver UUID
 */
#define DRM_DRIVER_UUID { { 7, 0xb, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } }

#endif // __DCI_H__

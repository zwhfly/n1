/*
 * Copyright (c) 2013-2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef __DRDRMUTILS_H__
#define __DRDRMUTILS_H__

#include "DrApi/DrApi.h"

#include "drTplayCommon.h"

/**
 * Exchange registers of current thread or another thread.
 *
 * @param  threadNo   Thread no
 * @param  ip         ip value to be set
 * @param  sp         sp value to be set
 *
 * @retval E_OK or relevant error code.
 */
drApiResult_t drUtilsRestartThread(
    threadno_t threadNo,
    addr_t ip,
    addr_t sp );


/**
 * Makes request to RTM for updating notification thread
 *
 * @param  threadNo   Thread no
 *
 * @retval E_OK or relevant error code.
 */
drApiResult_t drUtilsUpdateNotificationThread( threadno_t threadNo );


#define DRM_HARDCODED_MAP
#define DRM_USE_HANDLE2PHYS_TABLE


/*
 * This is the structure of the gHandle2PhysTable
 */
typedef struct
{
    uint32_t    handle;
    phys_addr_t phys_addr;
    uint32_t    size;
} handleToPhys_t;

/* Declaration of globals shared between IPC and DCI thread */
extern phys_addr_t  gOutputHandlePhysAddr;
extern phys_addr_t  gHandle2PhysTablePhysAddr;
extern uint32_t     gHandle2PhysTableSize;

/** Initialization of default Handle2Phys Table */
drApiResult_t initHandle2PhysTable(void);

/** */
drApiResult_t drMapAndGetOutputAddress(addr_t *bufVirtAddr, addr_t *targetVirtAddr, phys_addr_t *pPhysAddr);

/** */
drApiResult_t drUnMap(addr_t bufVirtAddr);

/** */
drApiResult_t drMap(phys_addr_t physAddr, uint32_t len, addr_t *bufVirtAddr, addr_t *targetVirtAddr);


#endif // __DRDRMUTILS_H__

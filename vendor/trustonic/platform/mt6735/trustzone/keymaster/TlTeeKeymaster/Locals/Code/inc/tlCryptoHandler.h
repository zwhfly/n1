/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef __TLCRYPTOHANDLER_H__
#define __TLCRYPTOHANDLER_H__

#include "tlTeeKeymaster_Api.h"


/* Key types */
#define KEY_TYPE_RSA           1
#define KEY_TYPE_DSA           2
#define KEY_TYPE_ECDSA         3


/* RSA key types */
#define KEY_PAIR_TYPE_RSA      1
#define KEY_PAIR_TYPE_RSACRT   2


/* Key sizes (in bits) */
#define RSA_KEY_SIZE_512       512
#define RSA_KEY_SIZE_1024      1024
#define RSA_KEY_SIZE_2048      2048
#define RSA_KEY_SIZE_3072      3072
#define RSA_KEY_SIZE_4096      4096


/* Digest algoritms */
#define RSA_DIGEST_SHA1        0
#define RSA_DIGEST_SHA256      1


/* Padding types */
#define RSA_PADDING_TYPE_ISO9796     0
#define RSA_PADDING_TYPE_ISO9796_MR  1
#define RSA_PADDING_TYPE_PKCS1       2
#define RSA_PADDING_TYPE_PSS         3


/* RSA signature algorithms */
#define SIG_ALG_RSA_NODIGEST_NOPADDING    1  /**< No digest and padding */

/**
 * Key structure buffer size (in bytes)
 * For public and private key data in total
 */
#define RSA_KEY_STRUCT_SIZE   4096


/**
 * Big enough to hold key data for max 2048 bit RSA key (1024 byes for modulus and  
 * exponent data + some extra data for headers, etc..) */
#define KEY_BUFFER_SIZE       4096
#define RSA_MAX_SIG_SIZE      512
#define SHA256_HASH_LENGTH    32 /* sha256 hash length in bytes */

/**
 * Big enough data to hold max 3072 bit DSA key data
 * (3072/8) * 5 => p, q, g, x, y
 */
#define DSA_KEY_BUFFER_SIZE   1920
#define DSA_MAX_KEY_SIZE      384 /* (3072/8) */

/* Min and max P and Q sizes */
#define DSA_P_BYTES_MIN   64
#define DSA_P_BYTES_MAX   384
#define DSA_Q_BYTES_MIN   20
#define DSA_Q_BYTES_MAX   32

/**
 * Big enough buffer to hold ECDSA key pair data (x, y and private key)
 */
#define ECDSA_KEY_BUFFER_SIZE  1024
/**
  * The following is more than enough. Typically
  * NIST P-521 key data (private key, public key data x and y )
  * each is 66 bytes
  */
#define ECDSA_KEY_DATA_SIZE    128

/*Maximum modulus length */
#define MAX_MOD_LENGTH   (RSA_KEY_SIZE_4096/8)

/**
 * HMAC key size in bytes
 */
#define HMAC_KEY_SIZE   16 /* 16 bytes, 128 bits */

/**
 * HMAC key data buffer size.
 */
#define HMAC_KEY_BUFFER_SIZE   256


/* Macros for handling RSA key data */
#define STRUCT_M(data,type,buf)  ({ data = (type*) buf; buf=buf+sizeof(type); })
#define STRUCT_D(data,buf,lenth) ({ data = (uint8_t *) buf; buf=buf+lenth; })
#define COPY_KEY_DATA(dst,src,length) ({ memcpy(dst, src, length); dst=dst+length; })

/* Macro for converting bits size to byte size */
#define BYTES_PER_BITS(bits) (((bits-1) >> 3)+1)

/**
 * Generate RSA key pair
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_RSAGenerateKeyPair( tci_t* pTci );


/**
 * RSA sign plain data and return signature data to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_RSASign( tci_t* pTci );


/**
 * RSA verify signature and return validity status to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_RSAVerify( tci_t* pTci );


/**
 * Generate random HMAC key
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_HMACGenerateKey( tci_t* pTci );


/**
 * HMAC sign plain data and return signature data to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_HMACSign( tci_t* pTci );


/**
 * HMAC verify signature and return validity status to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_HMACVerify( tci_t* pTci );


/**
 * Wrap and return key data to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_KeyImport( tci_t* pTci );


/**
 * Return public key data to to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_GetPubKey( tci_t* pTci );


/**
 * Generate DSA key pair
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_DSAGenerateKeyPair( tci_t* pTci );


/**
 * DSA sign plain data and return signature data to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_DSASign( tci_t* pTci );


/**
 * DSA verify signature and return validity status to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_DSAVerify( tci_t* pTci );


/**
 * Generate ECDSA key pair
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_ECDSAGenerateKeyPair( tci_t* pTci );


/**
 * ECDSA sign plain data and return signature data to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_ECDSASign( tci_t* pTci );


/**
 * ECDSA verify signature and return validity status to TLC
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_ECDSAVerify( tci_t* pTci );


/**
 * Get key info from key blob that is a secure object and
 * return key info to the NWd
 *
 * @return RET_OK upon success or relevant error code
 */
tciReturnCode_t tlCryptoHandler_GetKeyInfo( tci_t* pTci );

#endif // __TLCRYPTOHANDLER_H__


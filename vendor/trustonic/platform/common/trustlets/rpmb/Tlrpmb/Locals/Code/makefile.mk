################################################################################
#
# <t-sdk Sample RPMB Trustlet
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := tlrpmb


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------

TRUSTLET_UUID := 06040000000000000000000000000000
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1
TRUSTLET_SERVICE_TYPE := 3
TRUSTLET_KEYFILE := Locals/Build/pairVendorTltSig.pem
TRUSTLET_FLAGS := 0
TRUSTLET_INSTANCES := 10
TBASE_API_LEVEL := 5

HEAP_SIZE_INIT := 4096
HEAP_SIZE_MAX := 65536


#-------------------------------------------------------------------------------
# Files and include paths - Add your files here
#-------------------------------------------------------------------------------

### Add include path here
INCLUDE_DIRS += \
    Locals/Code/public \
    $(DRRPMB_DIR)/public/

### Add source code files for C++ compiler here
SRC_CPP += \
    Locals/Code/tlrpmb.c

### Add source code files for C compiler here
SRC_C += # nothing

### Add source code files for assembler here
SRC_S += # nothing

ifeq ($(TOOLCHAIN),GNU)
    CUSTOMER_DRIVER_LIBS += \
        $(TEE_DRIVER_OUTPUT_PATH)/drrpmb/$(TEE_MODE)/drrpmb.lib
else
   CUSTOMER_DRIVER_LIBS += \
        $(TEE_DRIVER_OUTPUT_PATH)/drrpmb/$(TEE_MODE)/drrpmb.lib
endif

#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)
include $(TLSDK_DIR)/trustlet.mk

